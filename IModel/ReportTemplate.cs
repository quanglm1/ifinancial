//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportTemplate
    {
        public int ReportTemplateID { get; set; }
        public string ReportName { get; set; }
        public string FileName { get; set; }
        public byte[] Template { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
}
