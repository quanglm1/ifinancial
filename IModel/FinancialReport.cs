//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class FinancialReport
    {
        public int FinancialReportID { get; set; }
        public int PlanID { get; set; }
        public int Year { get; set; }
        public Nullable<int> Quarter { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string Creator { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public Nullable<double> MoneyInQuarter { get; set; }
        public Nullable<double> MoneyToQuarter { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<System.DateTime> SendDate { get; set; }
        public Nullable<int> StepNow { get; set; }
    }
}
