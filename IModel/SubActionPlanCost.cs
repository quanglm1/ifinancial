//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SubActionPlanCost
    {
        public int SubActionPlanCostID { get; set; }
        public Nullable<int> PlanTargetActionID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public Nullable<int> PlanTargetActionSpendItemID { get; set; }
        public Nullable<int> SubActionSpendItemID { get; set; }
        public Nullable<double> Quantity { get; set; }
        public string Unit { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public Nullable<double> SumTotalBudget { get; set; }
        public Nullable<int> PlanYear { get; set; }
        public Nullable<int> ExpenseItemID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> CreateUserID { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string ExpenseName { get; set; }
        public Nullable<int> ModifierID { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
    
        public virtual PlanTargetAction PlanTargetAction { get; set; }
        public virtual SubActionPlan SubActionPlan { get; set; }
        public virtual SubActionSpendItem SubActionSpendItem { get; set; }
    }
}
