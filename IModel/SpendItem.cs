//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpendItem
    {
        public int SpendItemID { get; set; }
        public string SpenContent { get; set; }
        public Nullable<double> Price { get; set; }
        public string Description { get; set; }
        public Nullable<int> Status { get; set; }
        public string PathTraversal { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<int> Level { get; set; }
        public Nullable<int> SpendItemType { get; set; }
        public Nullable<int> MainActionID { get; set; }
    }
}
