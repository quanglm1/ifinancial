//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChangeRequest
    {
        public int ChangeRequestID { get; set; }
        public int PlanID { get; set; }
        public int DepartmentID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string Description { get; set; }
        public int CreateUserID { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<int> LastUpdateUserID { get; set; }
        public Nullable<int> StepNow { get; set; }
        public Nullable<int> IsFinish { get; set; }
    }
}
