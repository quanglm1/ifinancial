﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using IModel;
using IInterface.Common;

namespace IInterface.Filter
{
    public class ActionLogFilter : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var descriptor = filterContext.ActionDescriptor;
            string controller = descriptor.ControllerDescriptor.ControllerName;
            string actionName = descriptor.ActionName;
            UserInfo user = SessionManager.GetUserInfo();
            if (user != null)
            {
                using (Entities context = new Entities())
                {
                    ActionLog actionLog = new ActionLog();
                    actionLog.UserID = user.UserID;
                    actionLog.UserName = user.UserName;
                    actionLog.ActionName = actionName;
                    actionLog.ActionTime = DateTime.Now;
                    actionLog.ControllerName = controller;
                    context.ActionLog.Add(actionLog);
                    context.SaveChanges();
                }
            }
        }
    }
}