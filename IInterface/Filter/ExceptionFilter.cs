﻿using IInterface.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IInterface.Filter
{
    public class ExceptionFilter : FilterAttribute, IExceptionFilter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void OnException(ExceptionContext filterContext)
        {
            ////Write log
            logger.Error(filterContext.Exception);
            logger.Error(filterContext.Exception.StackTrace);
            logger.Error(filterContext.Exception.Message);
            if (!filterContext.ExceptionHandled && filterContext.Exception is BusinessException)
            {
                BusinessException ex = (BusinessException)filterContext.Exception;
                filterContext.Result = ex.Result != null ? (ActionResult)ex.Result :
                new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { Type = "ERROR", Message = ex.Message }
                };
                //var result = new ViewResult { ViewName = "ERROR" };
                filterContext.ExceptionHandled = true;
                SessionManager.SetValue("LAST_ERROR", filterContext.Result);
                filterContext.HttpContext.Response.StatusCode = 450;
            }
            else if (!filterContext.ExceptionHandled &&
            filterContext.Exception is HttpRequestValidationException)
            {
                BusinessException ex = new BusinessException("Bạn không được phép nhập các ký tự dạng thẻ html");
                filterContext.Result =
                new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { Type = "ERROR", Message = ex.Message }
                };
                SessionManager.SetValue("LAST_ERROR", filterContext.Result);
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 450;
            }
            else if (!filterContext.ExceptionHandled &&
                filterContext.Exception is UnauthorizedAccessException)
            {
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 401;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    BusinessException ex = new BusinessException("Bạn không được quyền truy cập chức năng này");
                    filterContext.Result =
                    new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { Type = "ERROR", Message = ex.Message }
                    };
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                                    {
                                        { "controller", "Home" },
                                        { "action", "RoleError" }
                                    });
                }
            }
            else
            {
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 500;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    BusinessException ex = new BusinessException("Hệ thống đang nâng cấp vui lòng quay lại sau");
                    filterContext.Result =
                    new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { Type = "ERROR", Message = ex.Message }
                    };
                }
                else
                {
                    if (SessionManager.GetUserInfo() == null)
                    {
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                            {
                                { "controller", "Home" },
                                { "action", "LogOn" }
                            });
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                            {
                                { "controller", "Home" },
                                { "action", "Error" }
                            });
                    }
                }
            }
        }
    }
}