﻿using IInterface.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace IInterface.Filter
{
    public class AuthorizeFilter : AuthorizeAttribute
    {
        private bool baseValidate;
        private bool customValidate;
        public const string TOKEN_KEY = "AuthenticationToken";
        public AuthorizeFilter() : base()
        {

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            baseValidate = base.AuthorizeCore(httpContext);
            if (baseValidate)
            {
                customValidate = Authorize.ValidateRequest(httpContext);
            }
            return baseValidate && customValidate;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                return;
            }
            else
            {
                UserInfo objUser = SessionManager.GetUserInfo();
                if (objUser == null)
                {
                    SessionManager.Clear();
                    FormsAuthentication.SignOut();
                    //throw new BusinessException("Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại");
                }
                //    if (filterContext.HttpContext.Request.IsAjaxRequest())
                //{
                //    throw new BusinessException("Bạn không được cấp quyền thao tác chức năng");
                //}
                //if (baseValidate && !customValidate)
                //{
                //    throw new UnauthorizedAccessException();
                //}
            }
        }
    }
}