﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace IInterface.Filter
{
    public class BreadCrumbAttribute : ActionFilterAttribute
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string AreaName { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controller = ControllerName;
            string area = AreaName;
            string action = ActionName;

            if (controller == "Hoạt động")
            {
                object obj;
                 filterContext.ActionParameters.TryGetValue("type", out obj);
                if (obj!=null)
                {
                    var type = Convert.ToInt32(obj);

                    if (type == Common.Constants.MAIN_TYPE)
                    {
                        action = "Hoạt động quỹ";
                    }
                    else
                    {
                        action = "Hoạt động đơn vị";
                    }
                }
            }

            if (controller == "Mục tiêu")
            {
                object obj;
                filterContext.ActionParameters.TryGetValue("type", out obj);
                if (obj != null)
                {
                    var type = Convert.ToInt32(obj);

                    if (type == Common.Constants.MAIN_TYPE)
                    {
                        action = "Mục tiêu quỹ";
                    }
                    else
                    {
                        action = "Mục tiêu đơn vị";
                    }
                }
            }


            if (controller== "Mô hình tổ chức")
            {
                if (action== "Cập nhật")
                {
                    object obj;
                    filterContext.ActionParameters.TryGetValue("id", out obj);
                    if (obj == null)
                    {
                        action = "Thêm mới đơn vị";
                    }
                    else
                    {
                        action = "Cập nhật đơn vị";
                    }
                }
            }
            filterContext.Controller.ViewBag.displayActionName = action;
            filterContext.Controller.ViewBag.displayControllerName = controller;
            filterContext.Controller.ViewBag.displayAreaName = area;
        }
    }
}