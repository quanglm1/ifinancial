﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class MenuModel
    {
        public int MenuID { get; set; }

        [DisplayName("Trạng thái")]
        public int Status { get; set; }

        [DisplayName("Chức năng cha")]
        public int? ParentID { get; set; }

        public string ParentName { get; set; }

        [DisplayName("Tên chức năng")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 1000 ký tự")]
        public string MenuName { get; set; }
        [DisplayName("Icon class")]
        public string IconClass { get; set; }
        [DisplayName("Đường dẫn")]
        public string ActionPath { get; set; }
        public bool IsSelected { get; set; }
        public List<MenuModel> ListChildMenu { get; set; }
        public int Type { get; set; }
        [DisplayName("Thứ tự")]
        public int? OrderNumber { get; set; }
    }
}