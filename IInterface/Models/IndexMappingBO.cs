﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class IndexMappingBO
    {
        public int MainIndexID { get; set; }
        public double? ValueIndex { get; set; }
        public double? ValueTotal { get; set; }
    }
}