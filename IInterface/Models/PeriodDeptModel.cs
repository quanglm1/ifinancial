﻿using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Models
{
    public class PeriodDeptModel
    {
        public int PeriodID { get; set; }
        public int? DepartmentID { get; set; }
        public int? PlanID { get; set; }
        public int? IsChange { get; set; }
        public long? PlanHisID { get; set; }
        public string DepartmentName { get; set; }
        public string PeriodName { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public int Role { get; set; }
        public int? PL { get; set; }
        public IEnumerable<SelectListItem> DepartmentItems { get; set; }
        public bool HasAdCost { get; set; }

        public PeriodDepartment ToEntity(string creator)
        {
            PeriodDepartment entity = new PeriodDepartment();
            entity.DepartmentID = DepartmentID == null ? 0 : DepartmentID.Value;
            entity.PeriodID = PeriodID;
            entity.Status = Status;
            return entity;
        }

        public void UpdateEntity(PeriodDepartment entity)
        {
            entity.DepartmentID = DepartmentID == null ? 0 : DepartmentID.Value;
            entity.PeriodID = PeriodID;
            entity.Status = Status;
        }
    }
}