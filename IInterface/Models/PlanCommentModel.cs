﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PlanCommentModel
    {
        public long CommentID { get; set; }
        public string Content { get; set; }
        public int CommentUserID { get; set; }
        public string UserName { get; set; }
        public System.DateTime CommentDate { get; set; }
        public int PlanTargetID { get; set; }
        public int PlanActionID { get; set; }
        public int Type { get; set; }
    }
}