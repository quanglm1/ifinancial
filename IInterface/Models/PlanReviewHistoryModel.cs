﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PlanReviewHistoryModel
    {
        public int PlanReviewHistoryID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string Comment { get; set; }
        public int PlanID { get; set; }
        public long PlanHisID { get; set; }
        public int? IsFirstApprove { get; set; }
    }
}