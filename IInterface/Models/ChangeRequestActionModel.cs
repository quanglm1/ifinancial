﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ChangeRequestActionModel
    {
        public long ChangeRequestActionID { get; set; }
        public long? ChangeRequestID { get; set; }
        public int? PlanID { get; set; }
        public int PlanTargetID { get; set; }
        public int PlanTargetActionID { get; set; }
        public int? ApproveStatus { get; set; }
    }
}