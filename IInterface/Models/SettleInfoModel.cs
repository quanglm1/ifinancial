﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SettleInfoModel
    {
        public string PlanName { get; set; }
        public string DeptName { get; set; }
        public int DeptID { get; set; }
        public int PeriodID { get; set; }
        public int PlanID { get; set; }
        public int SettleSynthesisID { get; set; }
        public int Year { get; set; }
        public Nullable<int> Quarter { get; set; }
        public Nullable<double> TotalReceivedMoney { get; set; }
        public Nullable<double> TottalDisbMoney { get; set; }
        public Nullable<double> TotalSettleMoney { get; set; }
        public Nullable<double> TotalPlanMoney { get; set; }
        public System.DateTime? CreateDate { get; set; }
        public DateTime? SendDate { get; set; }
        public string Creator { get; set; }
        public int Status { get; set; }
        public int Role { get; set; }
        public int Type { get; set; }
        public bool IsHasReview { get; set; }
    }
}