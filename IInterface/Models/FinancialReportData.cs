﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class FinancialReportData
    {
        public int FinancialReportID { get; set; }
        public int PlanID { get; set; }
        public int PeriodID { get; set; }
        public int Year { get; set; }
        public int? Quarter { get; set; }
        public int[] ArrPlanTargetActionID { get; set; }
        public string[] ArrPlanTargetActionMoney { get; set; }
        public int[] ArrSubActionPlanID { get; set; }
        public string[] ArrSubActionPlanMoney { get; set; }
    }
}