﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubTargetIndexModel
    {
        public int MainIndexID { get; set; }
        public string ContentIndex { get; set; }
        public Nullable<int> TypeIndex { get; set; }
        public Nullable<int> ValuePosition { get; set; }
        public Nullable<int> ValueIndex { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Creator { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> Choosen { get; set; }
    }
}