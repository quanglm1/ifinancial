﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionCost
    {
        public int TargetID { get; set; }
        public int MainActionID { get; set; }
        public string ActionCode { get; set; }
        public string ActionContent { get; set; }
        public double Cost { get; set; }
        public int PlanYear { get; set; }
    }
}