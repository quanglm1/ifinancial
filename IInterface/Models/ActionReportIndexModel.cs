﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportIndexModel
    {
        public int PlanTargetID { get; set; }
        public int PlanTargetIndexID { get; set; }
        public int? TypeIndex { get; set; }
        public double? ValueIndex { get; set; }
        public double? PlanValueIndex { get; set; }
        public string ContentIndex { get; set; }
        public bool Choosen { get; set; }
    }
}