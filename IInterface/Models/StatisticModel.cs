﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class StatisticModel
    {
        public int ActionNum { get; set; }
        public int IndexNum { get; set; }
        public int TargetNum { get; set; }
    }
}