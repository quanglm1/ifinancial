﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class DepartmentModel
    {
        public int DepartmentID { get; set; }
        [DisplayName("Mã đơn vị")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string DepartmentCode { get; set; }
        [DisplayName("Tên đơn vị")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string DepartmentName { get; set; }
        [DisplayName("Địa chỉ")]
        [MaxLength(1000, ErrorMessage = "{0} không được vượt quá 1000 ký tự")]
        public string Address { get; set; }
        [DisplayName("Số điện thoại")]
        [MaxLength(20, ErrorMessage = "{0} không được vượt quá 20 ký tự")]
        public string PhoneNumber { get; set; }
        [DisplayName("Fax")]
        [MaxLength(20, ErrorMessage = "{0} không được vượt quá 20 ký tự")]
        public string Fax { get; set; }
        [DisplayName("Số tài khoản")]
        [MaxLength(50, ErrorMessage = "{0} không được vượt quá 50 ký tự")]
        public string AccountNumber { get; set; }
        [DisplayName("Ngân hàng")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string BankName { get; set; }
        [DisplayName("Họ tên")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string RepresentPersonName { get; set; }
        [DisplayName("Chức vụ")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string RepresentPosition { get; set; }
        [DisplayName("Địa chỉ")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string RepresentAddress { get; set; }
        [DisplayName("Số điện thoại")]
        [MaxLength(20, ErrorMessage = "{0} không được vượt quá 20 ký tự")]
        public string RepresentPhone { get; set; }
        [DisplayName("Email")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string RepresentEmail { get; set; }
        [DisplayName("Họ tên")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string ContactPersonName { get; set; }
        [DisplayName("Chức vụ")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string ContactPosition { get; set; }
        [DisplayName("Địa chỉ")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string ContactAddress { get; set; }
        [DisplayName("Số điện thoại")]
        [MaxLength(20, ErrorMessage = "{0} không được vượt quá 20 ký tự")]
        public string ContactPhone { get; set; }
        [DisplayName("Email")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string ContactEmail { get; set; }
        public int? ParentID { get; set; }
        public bool IsUpDept { get; set; }
        public bool IsHasAdiction { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string Creator { get; set; }
        [DisplayName("Loại đơn vị")]
        public int? DepartmentType { get; set; }

        [DisplayName("Mục tiêu quỹ")]
        public int? TargetID { get; set; }
        [DisplayName("Hoạt động quỹ")]
        public int? MainActionID { get; set; }
        public string ListTargetAction { get; set; }
        public Department ToEntity()
        {
            Department entity = new Department
            {
                DepartmentCode = DepartmentCode,
                DepartmentName = DepartmentName,
                Address = Address,
                PhoneNumber = PhoneNumber,
                Fax = Fax,
                AccountNumber = AccountNumber,
                BankName = BankName,
                RepresentPersonName = RepresentPersonName,
                RepresentPosition = RepresentPosition,
                RepresentAddress = RepresentAddress,
                RepresentPhone = RepresentPhone,
                RepresentEmail = RepresentEmail,
                ContactPersonName = ContactPersonName,
                ContactPosition = ContactPosition,
                ContactAddress = ContactAddress,
                ContactPhone = ContactPhone,
                ContactEmail = ContactEmail,
                IsUpDept = (IsUpDept ? Constants.IS_ACTIVE : Constants.IS_NOT_ACTIVE),
                IsHasAdiction = (IsUpDept ? Constants.IS_ACTIVE : Constants.IS_NOT_ACTIVE),
                ParentID = ParentID,
                CreateDate = DateTime.Now,
                Creator = Creator,
                Status = Constants.IS_ACTIVE,
                DepartmentType = DepartmentType
            };
            return entity;
        }

        public void UpdateEntity(Department entity)
        {
            entity.DepartmentCode = DepartmentCode;
            entity.DepartmentName = DepartmentName;
            entity.Address = Address;
            entity.PhoneNumber = PhoneNumber;
            entity.Fax = Fax;
            entity.AccountNumber = AccountNumber;
            entity.BankName = BankName;
            entity.RepresentPersonName = RepresentPersonName;
            entity.RepresentPosition = RepresentPosition;
            entity.RepresentAddress = RepresentAddress;
            entity.RepresentPhone = RepresentPhone;
            entity.RepresentEmail = RepresentEmail;
            entity.ContactPersonName = ContactPersonName;
            entity.ContactPosition = ContactPosition;
            entity.ContactAddress = ContactAddress;
            entity.ContactPhone = ContactPhone;
            entity.ContactEmail = ContactEmail;
            entity.Status = Constants.IS_ACTIVE;
            entity.DepartmentType = DepartmentType;
            entity.IsUpDept = (IsUpDept ? Constants.IS_ACTIVE : Constants.IS_NOT_ACTIVE);
            entity.ParentID = ParentID;
            entity.IsHasAdiction = (IsUpDept ? Constants.IS_ACTIVE : Constants.IS_NOT_ACTIVE);
        }

        public void EntityTo(Department entity)
        {
            DepartmentID = entity.DepartmentID;
            DepartmentCode = entity.DepartmentCode;
            DepartmentName = entity.DepartmentName;
            Address = entity.Address;
            PhoneNumber = entity.PhoneNumber;
            Fax = entity.Fax;
            AccountNumber = entity.AccountNumber;
            BankName = entity.BankName;
            RepresentPersonName = entity.RepresentPersonName;
            RepresentPosition = entity.RepresentPosition;
            RepresentAddress = entity.RepresentAddress;
            RepresentPhone = entity.RepresentPhone;
            RepresentEmail = entity.RepresentEmail;
            ContactPersonName = entity.ContactPersonName;
            ContactPosition = entity.ContactPosition;
            ContactAddress = entity.ContactAddress;
            ContactPhone = entity.ContactPhone;
            ContactEmail = entity.ContactEmail;
            IsUpDept = (entity.IsUpDept == Constants.IS_ACTIVE ? true : false);
            IsHasAdiction = (entity.IsHasAdiction == Constants.IS_ACTIVE ? true : false);
            ParentID = entity.ParentID;
            DepartmentType = entity.DepartmentType;
        }
    }
}