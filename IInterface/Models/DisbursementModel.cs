﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IInterface.Models
{
    public class DisbursementModel
    {
        public long DisbursementID { get; set; }
        public int PlanID { get; set; }
        [DisplayName("Lần")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int Number { get; set; }
        
        public double? Money { get; set; }
        [DisplayName("Số tiền")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string SMoney { get; set; }
    }
}