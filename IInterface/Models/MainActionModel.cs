﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using IInterface.Common;
using IModel;

namespace IInterface.Models
{
    public class MainActionModel
    {
        public int MainActionID { get; set; }

        [DisplayName("Mã hoạt động")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(50, ErrorMessage = "{0} không được vượt quá 50 ký tự")]
        public string ActionCode { get; set; }
        [DisplayName("Nội dung")]
        public string ActionContent { get; set; }

        [DisplayName("Hoạt động cha")]
        public int? ParentID { get; set; }

        [DisplayName("Trạng thái")]
        public int? Status { get; set; }
        public string CrateUser { get; set; }
        public string TargetName { get; set; }
        public string ParentName { get; set; }
        public DateTime CreatDate { get; set; }
        [DisplayName("Mục tiêu")]
        public int? TargetID { get; set; }
        public int? MissionID { get; set; }
        public int? TypeFor { get; set; }
        public int[] DeptType { get; set; }
        public int[] listDeptID { get; set; }
        public int[] listMappingMainActionID { get; set; }
        public int? ActionType { get; set; }
        public int? Level { get; set; }
        public bool IsOther { get; set; }
        public bool IsSelected { get; set; }
        public List<MainActionModel> ListChildAction { get; set; }
        public List<ReportMissionModel> ListSpendItem { get; set; }
        public List<ActionIndexModel> ListActionIndex { get; set; }
        public double? ReportCount { get; set; }
        public void UpdateEntity(MainAction entity)
        {
            entity.MainActionID = MainActionID;
            entity.MissionID = MissionID;
            entity.ParentID = ParentID;
            entity.TargetID = TargetID;
            entity.ActionCode = ActionCode;
            entity.ActionContent = ActionContent;
            entity.IsOther = null;
            if (entity.Type == Constants.SUB_TYPE && IsOther)
            {
                entity.IsOther = Constants.IS_ACTIVE;
            }
        }

        public MainAction ToEntity(string userName)
        {
            MainAction entity = new MainAction
            {
                Type = TypeFor,
                MainActionID = MainActionID,
                MissionID = MissionID,
                ParentID = ParentID,
                Status = Status,
                TargetID = TargetID,
                ActionCode = ActionCode,
                ActionContent = ActionContent,
                CrateUser = userName,
                CreatDate = CreatDate,
                Level = Level
            };
            if (entity.Type == Constants.SUB_TYPE && IsOther)
            {
                entity.IsOther = Constants.IS_ACTIVE;
            }
            return entity;
        }
    }
}