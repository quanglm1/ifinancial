﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using IInterface.Common;

namespace IInterface.Models
{
    public class TargetModel
    {
        public int TargetID { get; set; }
        [DisplayName("Tiêu đề")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(100, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string TargetTitle { get; set; }
        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string TargetContent { get; set; }
        [DisplayName("Đơn vị")]
        public int? Type { get; set; }

        public int? Status { get; set; }
        [DisplayName("Thứ tự")]
        public int? TargetOrder { get; set; }
        [DisplayName("Mục tiêu quỹ")]
        public int? ParentID { get; set; }
        [DisplayName("Hoạt động quỹ")]
        public int? MainActionID { get; set; }
        [DisplayName("Loại đơn vị")]
        public int? DepartmentType { get; set; }
        public string ParentName { get; set; }
        public int? Choosen { get; set; }

        internal void UpdateEntity(Target entity)
        {
            entity.TargetID = TargetID;
            entity.Type = Type;
            entity.TargetTitle = TargetTitle;
            entity.TargetOrder = TargetOrder;
            entity.TargetContent = TargetContent;
            entity.ParentID = ParentID;
            entity.MainActionID = MainActionID;
            entity.DepartmentType = DepartmentType;
        }

        internal Target ToEntity(string userName)
        {
            Target entity = new Target
            {
                TargetID = TargetID,
                Type = Type,
                TargetTitle = TargetTitle,
                TargetOrder = TargetOrder,
                TargetContent = TargetContent,
                ParentID = ParentID,
                Status = Constants.IS_ACTIVE,
                MainActionID = MainActionID,
                DepartmentType = DepartmentType
            };
            return entity;
        }
    }
}