﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class CostEstimateDetailModel
    {
        public long CostEstimateDetailID { get; set; }
        public int? PlanTargetID { get; set; }
        public int? PlanTargetActionID { get; set; }
        public int? SubActionPlanID { get; set; }
        public long? CostEstimateSpendID { get; set; }
        public bool IsTarget { get; set; }
        public double? TotalMoney { get; set; }
        public int PlanYear { get; set; }
        public string Description { get; set; }
        public bool HasChild { get; set; }
        public int? MainActionID { get; set; }
    }
}