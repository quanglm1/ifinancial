﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubActionPlanCostModel
    {
        public int SubActionPlanCostID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public Nullable<double> Quantity { get; set; }
        public string Unit { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public Nullable<double> SumTotalBudget { get; set; }
        public Nullable<int> PlanYear { get; set; }
        public Nullable<int> ExpenseItemID { get; set; }
        public Nullable<int> CreateUserID { get; set; }
        public Nullable<int> ModifierID { get; set; }
        public Nullable<int> Status { get; set; }
        public string ExpenseName { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
        public Nullable<int> PlanTargetAcionID { get; set; }
        public Nullable<int> PlanTargetAcionSpendItemID { get; set; }
        public Nullable<int> SubActionSpendItemID { get; set; }
        public int CountChild { get; set; }
        public int? SumCostType { get; set; }
        public bool IsQuantityChange { get; set; }
        public bool IsPriceChange { get; set; }
        public bool IsTotalPriceChange { get; set; }
    }
}