﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class FinancialReportDetailModel
    {
        public long FinancialReportDetailID { get; set; }
        public int PlanTargetID { get; set; }
        public string TargetName { get; set; }
        public string TargetCode { get; set; }
        public int? PlanTargetActionID { get; set; }
        public string PlanTargetActionCode { get; set; }
        public string PlanTargetActionName { get; set; }
        public List<SubActionPlanModel> ListSubAction { get; set; }
        public List<SettleSynCostModel> ListSettleSynCost { get; set; }
        public bool IsTarget { get; set; }
        public double? DisbMoney { get; set; }
        public double? DisbMoneyToQuarter { get; set; }
        public double? PlanMoney { get; set; }
    }
}