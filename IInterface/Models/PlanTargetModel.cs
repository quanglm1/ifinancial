﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PlanTargetModel : SettleModel
    {
        public int PlanID { get; set; }
        public int? TargetID { get; set; }
        public string TargetCode { get; set; }
        public string Description { get; set; }
        public List<PlanTargetActionModel> ListPlanTargetActionModel { get; set; }
        public List<TargetIndexModel> ListPlanTargetIndex { get; set; }
        public List<SubActionPlanCostModel> ListSubActionPlanCost { get; set; }
        public int PlanTargetID { get; set; }
        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string PlanTargetContent { get; set; }
        [DisplayName("Tiêu đề")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string PlanTargetTitle { get; set; }
        public int? DepartmentID { get; set; }
        public int? DepartmentType { get; set; }
        public double? TotalBudget { get; set; }
        public List<CostEstimateDetailModel> ListCostEstimateDetail { get; set; }
        public List<CostEstimateSpendModel> ListCostEstimateSpend { get; set; }
        public bool IsChosen { get; set; }
        public int? ApproveStatus { get; set; }        
    }
}