﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PlanTargetActionModel : SettleModel
    {
        public int PlanTargetActionID { get; set; }
        public int? SubActionPlanID { get; set; }
        public string ActionCode { get; set; }
        public string ActionContent { get; set; }
        public string ActionTime { get; set; }
        public string ContentPropaganda { get; set; }
        public string ObjectScope { get; set; }
        public string ActionMethod { get; set; }
        public string ActionCodeChange { get; set; }
        public string ActionContentChange { get; set; }
        public string ContentPropagandaChange { get; set; }
        public string ObjectScopeChange { get; set; }
        public string ActionMethodChange { get; set; }
        public string Result { get; set; }
        public Nullable<int> ActionOrder { get; set; }
        public Nullable<int> MainActionID { get; set; }
        public Nullable<int> PlanID { get; set; }
        public Nullable<int> TargetID { get; set; }
        public int? PlanTargetID { get; set; }
        public List<SubActionPlanTimeModel> ListSubActionPlanTime { get; set; }
        public List<SubActionPlanCostModel> ListSubActionPlanCost { get; set; }
        public List<SubActionPlanModel> ListSubActionPlan { get; set; }
        public List<SubActionSpendItemModel> ListSubActionSpendItem { get; set; }
        public List<SettleSynCostModel> ListSettleCost { get; set; }
        public string Exucuter { get; set; }
        public string Supporter { get; set; }
        public string Note { get; set; }
        public Nullable<double> TotalBudget { get; set; }
        public Nullable<double> RemainMoney { get; set; }
        public Nullable<int> SumCostType { get; set; }

        public List<CostEstimateDetailModel> ListCostEstimateDetail { get; set; }
        public List<CostEstimateSpendModel> ListCostEstimateSpend { get; set; }
        public string DeptName { get; set; }
        public int DeptID { get; set; }
        public int? DeptType { get; set; }
        public string TargetContent { get; set; }
        public bool IsChosen { get; set; }
        public int? ApproveStatus { get; set; }
        public int? MissionID { get; set; }
        public int? IsNextPlan { get; set; }
    }
}