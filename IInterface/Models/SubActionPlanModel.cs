﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubActionPlanModel : SettleModel
    {
        // SubActionPlan
        public Nullable<int> PlanID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public Nullable<int> SubTargetID { get; set; }
        public Nullable<int> TargetID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public string SubActionCode { get; set; }
        public string SubActionContent { get; set; }
        public string ContentPropaganda { get; set; }
        public string Result { get; set; }
        public string SubActionObject { get; set; }
        public string SubActionMethod { get; set; }
        public List<SubActionPlanTimeModel> ListSubActionPlanTime { get; set; }
        public List<SubActionPlanCostModel> ListSubActionPlanCost { get; set; }
        public string Exucuter { get; set; }
        public string Supporter { get; set; }
        public string Note { get; set; }
        public Nullable<double> DisbMoney { get; set; }
        public Nullable<double> DisbMoneyToQuarter { get; set; }
        public Nullable<double> TotalBudget { get; set; }
        public string ActionReportDescription { get; set; }
        public string ActionReportResult { get; set; }
        public string ActionReportPlanContent { get; set; }
        public string ActionTime { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> CreateUserID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> SubActionOrder { get; set; }

        public Nullable<int> PlanTargetActionID { get; set; }
        public string ParentPlanTagetActionCode { get; set; }
        public string ParentPlanTagetActionContent { get; set; }

        public Nullable<int> PeriodID { get; set; }
        public Nullable<int> SpendItemID { get; set; }
        public Nullable<int> SumCostType { get; set; }
        public bool SumType { get; set; }
        public List<SubActionSpendItemModel> ListSubActionSpendItem { get; set; }
        public List<SubActionPlanModel> ListChildSubActionPlan { get; set; }
        public List<SettleSynCostModel> ListSettleCost { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
        public List<CostEstimateDetailModel> ListCostEstimateDetail { get; set; }
        public List<CostEstimateSpendModel> ListCostEstimateSpend { get; set; }

        public string GenPlanContent()
        {
            string content = string.Empty;
            if (!string.IsNullOrWhiteSpace(this.ContentPropaganda))
            {
                content += "Nội dung: \n";
                content += this.ContentPropaganda + "\n";
                if (!string.IsNullOrWhiteSpace(this.SubActionObject))
                {
                    content += "Đối tượng, phạm vi: \n";
                    content += this.SubActionObject + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.SubActionMethod))
                {
                    content += "Phương thức hoạt động: \n";
                    content += this.SubActionMethod + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.Result))
                {
                    content += "Kết quả dự kiến: \n";
                    content += this.Result + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.ActionTime))
                {
                    content += "Thời gian thực hiện: \n";
                    content += this.ActionTime + "\n";
                }
            }
            return content;
        }

    }
}