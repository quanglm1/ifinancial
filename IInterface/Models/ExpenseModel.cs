﻿using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ExpenseModel
    {
        public int SpendItemID { get; set; }

        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string SpenContent { get; set; }

        [DisplayName("Giá")]
        public double? Price { get; set; }

        [DisplayName("Chi tiết")]
        public string Description { get; set; }

        [DisplayName("Trạng thái")]
        public int? Status { get; set; }

        [DisplayName("Đường dẫn")]
        public string PathTraversal { get; set; }

        [DisplayName("Khoản chi cha")]
        public int? ParentID { get; set; }
        public string ParentName { get; set; }
        public int? Level { get; set; }
        public int? ActionType { get; set; }

        public void EntityTo(SpendItem entity)
        {
            this.Description = entity.Description;
            this.Level = entity.Level;
            this.ParentID = entity.ParentID;
            this.PathTraversal = entity.PathTraversal;
            this.Price = entity.Price;
            this.SpenContent = entity.SpenContent;
            this.SpendItemID = entity.SpendItemID;
            this.Status = entity.Status;
        }

        internal void UpdateEntity(SpendItem entity)
        {
            entity.Description = Description;
            entity.Level = Level;
            entity.ParentID = ParentID;
            entity.PathTraversal = PathTraversal;
            entity.Price = Price;
            entity.SpenContent = SpenContent;
            entity.SpendItemID = SpendItemID;
            entity.Status = Status;
        }

        internal SpendItem ToEntity(string userName)
        {
            SpendItem entity = new SpendItem();
            entity.Description = Description;
            entity.Level = Level;
            entity.ParentID = ParentID;
            entity.PathTraversal = PathTraversal;
            entity.Price = Price;
            entity.SpenContent = SpenContent;
            entity.SpendItemID = SpendItemID;
            entity.Status = Status;
            return entity;
        }
    }
}