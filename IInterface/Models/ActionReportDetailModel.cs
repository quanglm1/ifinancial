﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportDetailModel
    {
        public int PlanTargetID { get; set; }
        public string TargetName { get; set; }
        public string TargetCode { get; set; }
        public int? PlanTargetActionID { get; set; }
        public string PlanTargetActionCode { get; set; }
        public string PlanTargetActionName { get; set; }
        public List<SubActionPlanModel> ListSubAction { get; set; }
        public List<SettleSynCostModel> ListSettleSynCost { get; set; }
        public bool IsTarget { get; set; }
        public string Result { get; set; }
        public string Description { get; set; }
        public string PlanContent { get; set; }
        public string ContentPropaganda { get; set; }
        public string ObjectScope { get; set; }
        public string ActionMethod { get; set; }
        public string PlanResult { get; set; }
        public string ActionTime { get; set; }
        public string GenPlanContent()
        {
            string content = string.Empty;
            if (!string.IsNullOrWhiteSpace(this.ContentPropaganda))
            {
                content += "Nội dung: \n";
                content += this.ContentPropaganda + "\n";
                if (!string.IsNullOrWhiteSpace(this.ObjectScope))
                {
                    content += "Đối tượng, phạm vi: \n";
                    content += this.ObjectScope + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.ActionMethod))
                {
                    content += "Phương thức hoạt động: \n";
                    content += this.ActionMethod + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.PlanResult))
                {
                    content += "Kết quả dự kiến: \n";
                    content += this.PlanResult + "\n";
                }
                if (!string.IsNullOrWhiteSpace(this.ActionTime))
                {
                    content += "Thời gian thực hiện: \n";
                    content += this.ActionTime + "\n";
                }
            }
            return content;
        }
    }
}