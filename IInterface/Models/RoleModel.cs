﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class RoleModel
    {
        public int RoleID { get; set; }
        [DisplayName("Tên nhóm")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string RoleName { get; set; }
        public int Status { get; set; }
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
    }
}