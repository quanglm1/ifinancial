﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportActionIndexModel
    {
        public long ActionReportActionIndexID { get; set; }
        public int ActionReportID { get; set; }
        public int PlanTargetID { get; set; }
        public int PlanTargetActionID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public int PlanActionIdexID { get; set; }
        public double IndexValue { get; set; }
        public string UnitName { get; set; }
        public string IndexName { get; set; }
        public string PrefixName { get; set; }
        public bool Choosen { get; set; }
        public string FullIndexName { get; set; }
    }
}