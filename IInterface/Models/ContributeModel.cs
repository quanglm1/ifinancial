﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ContributeModel
    {
        public int ContributeID { get; set; }
        [DisplayName("Năm")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int Year { get; set; }
        [DisplayName("Kế hoạch và dư năm trước")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string PlanMoney { get; set; }
        public string PlanMoneyFormat
        {
            get
            {
                string s = string.Empty;
                if (!string.IsNullOrWhiteSpace(this.PlanMoney))
                {
                    double m;
                    if (double.TryParse(this.PlanMoney, out m))
                    {
                        s = Utils.FormatMoney(m, true);
                    }
                }
                return s;
            }
        }
        [DisplayName("Thực thu")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ActualMoney { get; set; }
        public double DBPlanMoney { get; set; }
        public double DBActualMoney { get; set; }
        public string ActualMoneyFormat
        {
            get
            {
                string s = string.Empty;
                if (!string.IsNullOrWhiteSpace(this.ActualMoney))
                {
                    double m;
                    if (double.TryParse(this.ActualMoney, out m))
                    {
                        s = Utils.FormatMoney(m, true);
                    }
                }
                return s;
            }
        }
        
        public void EntityTo(Contribute entity)
        {
            this.ContributeID = entity.ContributeID;
            this.PlanMoney = Utils.FormatMoney(entity.PlanMoney);
            this.ActualMoney = Utils.FormatMoney(entity.ActualMoney);
            this.DBPlanMoney = entity.PlanMoney;
            this.DBActualMoney = entity.ActualMoney;
            this.Year = entity.Year;
        }

        internal void UpdateEntity(Contribute entity)
        {
            entity.Year = this.Year;
            if (!string.IsNullOrWhiteSpace(this.PlanMoney))
            {
                double m;
                if (double.TryParse(this.PlanMoney, out m))
                {
                    entity.PlanMoney = m;
                }
            }
            if (!string.IsNullOrWhiteSpace(this.ActualMoney))
            {
                double m;
                if (double.TryParse(this.ActualMoney, out m))
                {
                    entity.ActualMoney = m;
                }
            }
        }

        internal Contribute ToEntity()
        {
            Contribute entity = new Contribute();
            entity.Year = this.Year;
            if (!string.IsNullOrWhiteSpace(this.PlanMoney))
            {
                double m;
                if (double.TryParse(this.PlanMoney, out m))
                {
                    entity.PlanMoney = m;
                }
            }
            if (!string.IsNullOrWhiteSpace(this.ActualMoney))
            {
                double m;
                if (double.TryParse(this.ActualMoney, out m))
                {
                    entity.ActualMoney = m;
                }
            }
            return entity;
        }
    }
}