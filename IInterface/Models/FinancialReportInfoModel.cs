﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class FinancialReportInfoModel
    {
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public string PlanName { get; set; }
        public int PeriodID { get; set; }
        public int PlanID { get; set; }
        public int FinancialReportID { get; set; }
        public int Year { get; set; }
        public Nullable<int> Quarter { get; set; }
        public System.DateTime? CreateDate { get; set; }
        public DateTime? SendDate { get; set; }
        public string Creator { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public double PlanMoney { get; set; }
        public double MoneyInQuarter { get; set; }
        public Nullable<double> MoneyToQuarter { get; set; }
        public bool IsHasReview { get; set; }
        public int Role { get; set; }
    }
}