﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ModelLogOn
    {
        [DisplayName("Tên đăng nhập")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(100, ErrorMessage = "{0} không được vượt quá 100 ký tự")]
        public string UserName { get; set; }
        [DisplayName("Mật khẩu")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Password { get; set; }

        public CaptchaForm Captcha { get; set; }
    }
}