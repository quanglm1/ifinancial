﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PlanIndexModel
    {
        public int PlanTargetIndexID { get; set; }
        public int? MainIndexID { get; set; }
        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ContentIndex { get; set; }
        public int? PlanID { get; set; }
        [DisplayName("Loại chỉ số")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? TypeIndex { get; set; }
        [DisplayName("Giá trị")]
        [Range(0, float.MaxValue, ErrorMessage = "{0} là kiểu số")]
        public double? ValueIndex { get; set; }
        [DisplayName("Tổng số")]
        [Range(0, float.MaxValue, ErrorMessage = "{0} là kiểu số")]
        public double? ValueTotal { get; set; }
        public int? CreateUserID { get; set; }
        public int? UpdateUser { get; set; }
        public int? PlanTargetID { get; set; }

    }
}