﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Models
{
    public class PeriodModel
    {
        public int PeriodID { get; set; }
        [DisplayName("Tên giai đoạn")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string PeriodName { get; set; }
        [DisplayName("Thời gian bắt đầu")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public DateTime FromDate { get; set; }
        [DisplayName("Thời gian kết thúc")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [DateGreaterThan("FromDate", "Ngày kết thúc không được nhỏ thua ngày bắt đầu")]
        public DateTime ToDate { get; set; }
        public int Status { get; set; }
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        public string Creator { get; set; }
        public DateTime CreateDate { get; set; }
        //public int DepartmentID { get; set; }
        //public string DepartmentName { get; set; }
        //public IEnumerable<SelectListItem> DepartmentItems { get; set; }
        public Period ToEntity(string creator)
        {
            Period entity = new Period();
            entity.PeriodName = this.PeriodName;
            entity.FromDate = this.FromDate;
            entity.ToDate = this.ToDate;
            entity.Description = this.Description;
            entity.Status = Constants.IS_ACTIVE;
            entity.CreateDate = DateTime.Now;
            entity.Creator = creator;
            //entity.DepartmentID = DepartmentID;
            return entity;
        }
        public PeriodModel FromEntity(Period entity)
        {
            PeriodModel dto = new PeriodModel();
            dto.PeriodName = entity.PeriodName;
            dto.FromDate = entity.FromDate;
            dto.ToDate = entity.ToDate;
            dto.Description = entity.Description;
            dto.Status = entity.Status;
            dto.CreateDate = entity.CreateDate;
            dto.Creator = entity.Creator;
            return dto;
        }
        public void UpdateEntity(Period entity)
        {
            entity.PeriodID = this.PeriodID;
            entity.PeriodName = this.PeriodName;
            entity.FromDate = this.FromDate;
            entity.ToDate = this.ToDate;
            entity.Description = this.Description;
            //entity.DepartmentID = this.DepartmentID;
        }
    }
}