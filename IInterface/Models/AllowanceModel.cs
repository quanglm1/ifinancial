﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class AllowanceModel
    {
        public int AllowanceId { get; set; }
        public int? NumOfEmployee { get; set; }
        public double? Coefficient { get; set; }
        public double? BasicSalary { get; set; }
        public double? AllowancePerMonth { get; set; }
        public double? TotalPerYear { get; set; }
        public int Year { get; set; }
        public int Type { get; set; }
    }
}