﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SettleCostData
    {
        public int SettleSynthesisID { get; set; }
        public int PeriodID { get; set; }
        public int Year { get; set; }
        public int? Quarter { get; set; }
        public string Comment { get; set; }
        public string[] Types { get; set; }
        public int[] IDs { get; set; }
        public long?[] SyncCostIDs { get; set; }
        public string[] ArrValue { get; set; }
        public string[] ArrDesc { get; set; }
    }
}