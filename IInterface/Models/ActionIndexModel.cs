﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IInterface.Models
{
    public class ActionIndexModel
    {
        public int ActionIndexID { get; set; }
        public int ActionID { get; set; }
        public string ActionName { get; set; }
        public int TargetID { get; set; }
        public string TargetName { get; set; }
        [DisplayName("Tên chỉ số")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string IndexName { get; set; }
        [DisplayName("Đơn vị tính")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string UnitName { get; set; }
        public DateTime CreateDate { get; set; }
        public int IndexValue { get; set; }
        public int PlanActionID { get; set; }
        public int? Choosen { get; set; }
    }
}