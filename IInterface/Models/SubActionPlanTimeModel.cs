﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubActionPlanTimeModel
    {

        public Nullable<int> SubActionPlanTimeID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public string PlanTime { get; set; }
        public Nullable<int> PlanYear { get; set; }
        public string Executer { get; set; }
        public string Supporter { get; set; }
        public string Note { get; set; }
        public Nullable<int> PlanTargetAcionID { get; set; }
        public Nullable<int> CreateUserID { get; set; }
        public Nullable<int> ModifierID { get; set; }

    }
}