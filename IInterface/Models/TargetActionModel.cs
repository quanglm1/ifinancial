﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using IInterface.Common;

namespace IInterface.Models
{
    public class TargetActionModel
    {
        public int PlanTargetActionID { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
        public Nullable<int> TargetID { get; set; }
        //[DisplayName("Mã hoạt động")]
        //[Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ActionCode { get; set; }
        [DisplayName("Tiêu đề hoạt động")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ActionContent { get; set; }
        [DisplayName("Nội dung tuyên truyền")]
        public string ContentPropaganda { get; set; }
        [DisplayName("Đối tượng, phạm vi")]
        public string ObjectScope { get; set; }
        [DisplayName("Phương thức thực hiện")]
        public string ActionMethod { get; set; }
        public string Years { get; set; }
        public Nullable<int> ActionOrder { get; set; }
        [DisplayName("Hoạt động gợi ý")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public Nullable<int> MainActionID { get; set; }
        public Nullable<int> PlanID { get; set; }
        public int? UpdateUserID { get; set; }
        public bool IsActionContentChange { get; set; }
        public bool IsContentPropagandaChange { get; set; }
        public bool IsActionMethodChange { get; set; }
        public bool IsObjectScopeChange { get; set; }
        public string ActionTimes { get; set; }
        public string Result { get; set; }
        public PlanTargetAction ToEntity()
        {
            PlanTargetAction entity = new PlanTargetAction();
            entity.PlanID = PlanID;
            if (MainActionID != null && MainActionID > 0)
            {
                entity.MainActionID = MainActionID;
                entity.IsOtherAction = null;
            }
            else if (MainActionID != null && MainActionID == 0)
            {
                entity.MainActionID = null;
                entity.IsOtherAction = Constants.IS_ACTIVE;
            }
            else
            {
                entity.MainActionID = null;
                entity.IsOtherAction = null;
            }
            entity.PlanTargetID = PlanTargetID;
            entity.ObjectScope = ObjectScope;
            entity.ActionContent = ActionContent;
            entity.ContentPropaganda = ContentPropaganda;
            entity.ActionMethod = ActionMethod;
            entity.ActionOrder = ActionOrder;
            return entity;
        }
        public void UpdateEntity(PlanTargetAction entity)
        {
            entity.PlanID = PlanID;
            if (MainActionID != null && MainActionID > 0)
            {
                entity.MainActionID = MainActionID;
                entity.IsOtherAction = null;
            }
            else if (MainActionID != null && MainActionID == 0)
            {
                entity.MainActionID = null;
                entity.IsOtherAction = Constants.IS_ACTIVE;
            }
            else
            {
                entity.MainActionID = null;
                entity.IsOtherAction = null;
            }
            entity.PlanTargetID = PlanTargetID;
            entity.ObjectScope = ObjectScope;
            entity.ActionContent = ActionContent;
            entity.ContentPropaganda = ContentPropaganda;
            entity.ActionMethod = ActionMethod;
            entity.ActionOrder = ActionOrder;
        }
    }
}