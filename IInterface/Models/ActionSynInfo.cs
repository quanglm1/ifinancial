﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionSynInfo
    {
        public int TargetID { get; set; }
        public int MainActionID { get; set; }
        public int? DeptType { get; set; }
        public int? DeptID { get; set; }
        public string ActionCode { get; set; }
        public string ActionContent { get; set; }
    }
}