﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class CostEstimateData
    {
        public int CostEstimateID { get; set; }
        public int PlanID { get; set; }
        public int[] ArrPlanTargetActionID { get; set; }
        public string[][] ArrPlanTargetActionMoney { get; set; }
        public int[] ArrSubActionPlanID { get; set; }
        public string[][] ArrSubMoney { get; set; }
        public string[] ArrDescription { get; set; }
    }
}