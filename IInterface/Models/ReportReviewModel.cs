﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ReportReviewModel
    {
        public long ReportReviewID { get; set; }
        public int ReportID { get; set; }
        public int ReportType { get; set; }
        public int? ActionType { get; set; }
        public System.DateTime ProcessDate { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string DeptName { get; set; }
        public int Status { get; set; }
    }
}