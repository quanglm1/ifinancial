﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class CommentModel
    {
        public long CommentID { get; set; }
        public string Content { get; set; }
        public int Type { get; set; }
        public long ObjectID { get; set; }
        public DateTime CommentDate { get; set; }
        public int CommentUserID { get; set; }
        public string CommentUserName { get; set; }
        public string CommentFullName { get; set; }
    }
}