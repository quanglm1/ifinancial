﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubActionSpendItemModel : SettleModel
    {
        public int SubActionSpendItemID { get; set; }
        public int? SpendItemID { get; set; }
        public string SpendItemName { get; set; }
        public string SpendItemTitle { get; set; }
        public Nullable<int> PlanTargetActionID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public Nullable<int> Status { get; set; }
        public List<SubActionPlanCostModel> ListSubActionPlanCost { get; set; }
        public List<SubActionSpendItemModel> ListChild { get; set; }
        public Nullable<int> PlanTargetSpendItemID { get; set; }
        public List<SettleSynCostModel> ListSettleCost { get; set; }
        public int Level { get; set; }
        public int? SumCostType { get; set; }
        public List<SubActionSpendItemModel> ItemChild { get; set; }
        public bool IsChangeName { get; set; }
        public bool IsParent { get; set; }
        public string Description { get; set; }
    }
}