﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IInterface.Models
{
    public class SubActionModel
    {
        public DateTime? CreateDate { get; set; }
        public int? CreateUserID { get; set; }
        public int? Type { get; set; }
        [DisplayName("Hoạt động gợi ý")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? MainActionID { get; set; }
        public int? Status { get; set; }
        public string SubActionCode { get; set; }
        [DisplayName("Tiêu đề")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string SubActionContent { get; set; }
        public int? SubActionID { get; set; }
        public int? TargetID { get; set; }
        public int PlanActionID { get; set; }
        public int SubActionPlanID { get; set; }
        public int? SubActionOrder { get; set; }
        public string PlanActionContent { get; set; }
        public string PlanTargetTitle { get; set; }
        public string ContentPropaganda { get; set; }
        public string Result { get; set; }
        public string SubActionObject { get; set; }
        public string SubActionMethod { get; set; }
    }
}