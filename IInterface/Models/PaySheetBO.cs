﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PaySheetBO
    {
        public PaySheetModel PaySheet { get; set; }
        public AllowanceModel Allowance { get; set; }
    }
}