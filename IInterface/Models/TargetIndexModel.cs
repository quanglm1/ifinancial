﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class TargetIndexModel
    {
        public int PlanTargetIndexID { get; set; }
        public int? MainIndexID { get; set; }
        public string ContentIndex { get; set; }
        public Nullable<int> TypeIndex { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public int CreateUserID { get; set; }
        public int? UpdateUserID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> Choosen { get; set; }
        public double? ValueIndex { get; set; }
        public double? ValueTotal { get; set; }
        public double? ValueIndexChange { get; set; }
        public double? ValueTotalChange { get; set; }
        public bool IsNew { get; set; }
        public int TargetID { get; set; }
        public int PlanTargetID { get; set; }
    }
}