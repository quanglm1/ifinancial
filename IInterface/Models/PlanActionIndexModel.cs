﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IInterface.Models
{
    public class PlanActionIndexModel
    {
        public int PlanActionIndexID { get; set; }
        public int? ActionIndexID { get; set; }
        public int PlanTargetActionID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        [DisplayName("Giá trị chỉ số")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? IndexValue { get; set; }
        public string IndexUnitName { get; set; }
        [DisplayName("Tên chỉ số")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string IndexName { get; set; }
        public int PlanTargetID { get; set; }
        public int Choosen { get; set; }

        public string GetFullInfo()
        {
            string s = string.Empty;
            s += IndexValue;
            if (!string.IsNullOrWhiteSpace(this.IndexUnitName))
            {
                s += " " + this.IndexUnitName;
            }
            if (!string.IsNullOrWhiteSpace(this.IndexName))
            {
                s += " " + this.IndexName;
            }
            return s.Trim();
        }
    }
}