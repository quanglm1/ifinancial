﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ReportMissionModel
    {
        public long ReportMissionID { get; set; }
        public int PeriodID { get; set; }
        public string PeriodName { get; set; }
        public int MissionID { get; set; }
        public string MissionName { get; set; }
        public int ActionID { get; set; }
        public string ActionName { get; set; }
        public Nullable<int> SpendItemID { get; set; }
        public string SpendItemName { get; set; }
        public int? ParentID { get; set; }
        public double? ReportCount { get; set; }
        public List<ReportMissionModel> ListChildAction { get; set; }
    }
}