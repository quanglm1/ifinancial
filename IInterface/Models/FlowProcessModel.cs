﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using IModel;
using IInterface.Common;

namespace IInterface.Models
{
    public class FlowProcessModel
    {
        public int FlowProcessID { get; set; }
        public int ObjectType { get; set; }
        [DisplayName("Bước")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int Step { get; set; }
        public int ProcessorType { get; set; }
        public int ProcessorID { get; set; }
        public string ProcessorName { get; set; }
        public int WorkType { get; set; }
        public Nullable<int> PLNumber { get; set; }
        public Nullable<int> IsHeadOffice { get; set; }
        public Nullable<int> PeriodID { get; set; }
        public bool IsRepresent { get; set; }

        public void EntityTo(FlowProcess entity)
        {
            this.FlowProcessID = entity.FlowProcessID;
            this.IsHeadOffice = entity.IsHeadOffice;
            this.ObjectType = entity.ObjectType;
            this.Step = entity.Step;
            this.ProcessorType = entity.ProcessorType;
            this.ProcessorID = entity.ProcessorID;
            this.WorkType = entity.WorkType;
            this.PLNumber = entity.PLNumber;
            if (entity.IsRepresent.GetValueOrDefault() == Constants.IS_ACTIVE)
            {
                this.IsRepresent = true;
            }
            this.PeriodID = PeriodID;
        }

        public FlowProcess ToEntity()
        {
            FlowProcess entity = new FlowProcess();
            entity.IsHeadOffice = IsHeadOffice;
            entity.ObjectType = ObjectType;
            entity.Step = Step;
            entity.ProcessorType = ProcessorType;
            entity.ProcessorID = ProcessorID;
            entity.WorkType = WorkType;
            entity.PeriodID = PeriodID;
            if (entity.ObjectType == 1)
            {
                entity.PLNumber = PLNumber;
            }
            else
            {
                entity.PLNumber = null;
            }
            if (entity.PLNumber.GetValueOrDefault() == 0)
            {
                entity.PLNumber = null;
            }
            if (this.IsRepresent)
            {
                entity.IsRepresent = Constants.IS_ACTIVE;
            }
            else
            {
                entity.IsRepresent = null;
            }
            return entity;
        }

        public void UpdateEntity(FlowProcess entity)
        {
            entity.IsHeadOffice = IsHeadOffice;
            entity.ObjectType = ObjectType;
            entity.Step = Step;
            entity.ProcessorType = ProcessorType;
            entity.ProcessorID = ProcessorID;
            entity.WorkType = WorkType;
            entity.PeriodID = PeriodID;
            if (entity.ObjectType == 1)
            {
                entity.PLNumber = PLNumber;
            }
            else
            {
                entity.PLNumber = null;
            }
            if (entity.PLNumber.GetValueOrDefault() == 0)
            {
                entity.PLNumber = null;
            }
            if (this.IsRepresent)
            {
                entity.IsRepresent = Constants.IS_ACTIVE;
            }
            else
            {
                entity.IsRepresent = null;
            }
        }
    }
}