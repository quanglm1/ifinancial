﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SettleModel
    {
        public long SettleSynthesisCostID { get; set; }
        public double? TotalSettleMoney { get; set; }
        public double? TotalSettleMoneyChange { get; set; }
        public string SettleDescription { get; set; }
        public string SettleDescriptionChange { get; set; }
        public int? CostUpdateUserID { get; set; }
        public int? CostUpdateType { get; set; }
    }
}