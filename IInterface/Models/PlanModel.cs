﻿using IModel;
using IInterface.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Models
{
    public class PlanModel
    {
        public int PeriodID { get; set; }
        public long PlanHisID { get; set; }
        public string PeriodName { get; set; }
        public int? PlanID { get; set; }
        public int DepartmentID { get; set; }
        public int? Type { get; set; }
        public int? StepNow { get; set; }
        public int Status { get; set; }
        public string PlanContent { get; set; }
        public string PlanTarget { get; set; }
        public string Monitor { get; set; }
        public string CostManagement { get; set; }
        public int? ParentPlanID { get; set; }
        [DisplayName("Thời gian bắt đầu")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public DateTime FromDate { get; set; }
        [DisplayName("Thời gian kết thúc")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [DateGreaterThan("FromDate", "Ngày kết thúc không được nhỏ thua ngày bắt đầu")]
        public DateTime ToDate { get; set; }

        //Department
        public string DepartmentName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string RepresentPersonName { get; set; }
        public string RepresentPosition { get; set; }
        public string RepresentAddress { get; set; }
        public string RepresentPhone { get; set; }
        public string RepresentEmail { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPosition { get; set; }
        public string ContactAddress { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Implementation { get; set; }

        // Tong kinh phi duoc duyet
        public double TotalPlanMoney { get; set; }

        public IEnumerable<SelectListItem> PeriodItems { get; set; }
        public IEnumerable<SelectListItem> DepartmentItems { get; set; }
        public int Role { get; set; }
        public int? PL { get; set; }
        public int? IsChange { get; set; }
        public DateTime? LastOpsDate { get; set; }
        public FlowProcess LastFlowProcess { get; set; }
        public string LastReviewInfo { get; set; }
        public Plan ToEntity()
        {
            Plan entity = new Plan();
            entity.PeriodID = PeriodID;
            entity.DepartmentID = DepartmentID;
            entity.ParentPlanID = ParentPlanID;
            entity.PlanContent = PlanContent;
            entity.PlanTarget = PlanTarget;
            entity.Status = Constants.PLAN_NEW;
            entity.Type = Type;
            entity.RepresentPersonName = RepresentPersonName;
            entity.RepresentPosition = RepresentPosition;
            entity.RepresentAddress = RepresentAddress;
            entity.RepresentEmail = RepresentEmail;
            entity.RepresentPhone = RepresentPhone;
            entity.ContactPersonName = ContactPersonName;
            entity.ContactPosition = ContactPosition;
            entity.ContactAddress = ContactAddress;
            entity.ContactEmail = ContactEmail;
            entity.ContactPhone = ContactPhone;
            entity.FromDate = FromDate;
            entity.ToDate = ToDate;
            return entity;
        }
        public void ToUpdateEntity(Plan entity)
        {
            entity.PeriodID = PeriodID;
            entity.DepartmentID = DepartmentID;
            entity.ParentPlanID = ParentPlanID;
            entity.PlanContent = PlanContent;
            entity.PlanTarget = PlanTarget;
            entity.RepresentPersonName = RepresentPersonName;
            entity.RepresentPosition = RepresentPosition;
            entity.RepresentAddress = RepresentAddress;
            entity.RepresentEmail = RepresentEmail;
            entity.RepresentPhone = RepresentPhone;
            entity.ContactPersonName = ContactPersonName;
            entity.ContactPosition = ContactPosition;
            entity.ContactAddress = ContactAddress;
            entity.ContactEmail = ContactEmail;
            entity.ContactPhone = ContactPhone;
            entity.ContactPhone = ContactPhone;
            entity.FromDate = FromDate;
            entity.ToDate = ToDate;
        }
    }
}