﻿using IInterface.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportFileModel
    {
        public int ActionReportFileID { get; set; }
        public string DocName { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public long FileSize { get; set; }
        public string MegaBytes
        {
            get
            {
                return Utils.BytesToSize(this.FileSize);
            }
        }
        public int ActionReportID { get; set; }
    }
}