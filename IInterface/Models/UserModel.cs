﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        [DisplayName("Tên đăng nhập")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(100, ErrorMessage = "{0} không được vượt quá 100 ký tự")]
        public string UserName { get; set; }
        [DisplayName("Mật khẩu")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string Password { get; set; }
        [DisplayName("Họ tên")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string FullName { get; set; }
        [DisplayName("Chức vụ")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 500 ký tự")]
        public string Position { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        [DisplayName("Email")]
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        [MaxLength(200, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string Email { get; set; }
        [DisplayName("Số điện thoại")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Số điện thoại không hợp lệ")]
        [MaxLength(20, ErrorMessage = "{0} không được vượt quá 20 ký tự")]
        public string PhoneNumber { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string Creator { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public byte[] Avatar { get; set; }
        public string RoleIDs { get; set; }
        public string ManageDeptIDs { get; set; }
        public int[] listRoleID { get; set; }
        public int[] listManageDeptID { get; set; }
        public bool IsAutoPass { get; set; }
        public bool IsDelAvatar { get; set; }
        public bool IsHeadOffice { get; set; }
        public HttpPostedFileWrapper UserAvatar { get; set; }
        public void EntityTo(User entity)
        {
            this.UserID = entity.UserID;
            this.UserName = entity.UserName.Trim().ToLower();
            this.FullName = entity.FullName;
            this.Position = entity.Position;
            this.CreateDate = entity.CreateDate;
            this.Creator = entity.Creator;
            this.LastLoginDate = entity.LastLoginDate;
            this.Status = entity.Status;
            this.PhoneNumber = entity.PhoneNumber;
            this.Email = entity.Email;
            this.DepartmentID = entity.DepartmentID;
            this.Password = StringCipher.Decrypt(entity.Password);
            if (entity.IsHeadOffice.GetValueOrDefault() == Constants.IS_ACTIVE)
            {
                this.IsHeadOffice = true;
            }
            else
            {
                this.IsHeadOffice = false;
            }
        }

        public User ToEntity()
        {
            User entity = new User();
            entity.UserName = this.UserName.Trim().ToLower();
            entity.FullName = this.FullName;
            List<string> listName = this.FullName.Split(' ').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
            entity.LastName = listName[listName.Count - 1];
            entity.Position = this.Position;
            entity.CreateDate = this.CreateDate;
            entity.Creator = this.Creator;
            entity.LastLoginDate = this.LastLoginDate;
            entity.Status = this.Status;
            entity.PhoneNumber = this.PhoneNumber;
            entity.Email = this.Email;
            entity.DepartmentID = this.DepartmentID;
            if (this.IsAutoPass)
            {
                entity.Password = StringCipher.Encrypt(StringCipher.GetRandomString());
            }
            else
            {
                entity.Password = StringCipher.Encrypt(this.Password);
            }
            entity.Avatar = this.Avatar;
            if (this.IsHeadOffice)
            {
                entity.IsHeadOffice = Constants.IS_ACTIVE;
            }
            else
            {
                entity.IsHeadOffice = Constants.IS_NOT_ACTIVE;
            }
            return entity;
        }
        public void UpdateEntity(User entity)
        {
            if (entity == null)
            {
                return;
            }
            entity.UserName = this.UserName.Trim().ToLower();
            entity.FullName = this.FullName;
            List<string> listName = this.FullName.Split(' ').Where(v => !string.IsNullOrWhiteSpace(v)).ToList();
            entity.LastName = listName[listName.Count - 1];
            entity.Position = this.Position;
            entity.PhoneNumber = this.PhoneNumber;
            entity.Email = this.Email;
            entity.DepartmentID = this.DepartmentID;
            if (this.IsDelAvatar)
            {
                entity.Avatar = null;
            }
            else
            {
                if (this.Avatar != null && this.Avatar.LongLength > 0)
                {
                    entity.Avatar = this.Avatar;
                }
            }
            if (this.IsHeadOffice)
            {
                entity.IsHeadOffice = Constants.IS_ACTIVE;
            }
            else
            {
                entity.IsHeadOffice = Constants.IS_NOT_ACTIVE;
            }
        }
    }
}