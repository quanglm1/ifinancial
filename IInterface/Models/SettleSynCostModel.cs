﻿using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SettleSynCostModel
    {
        public long SettleSynthesisCostID { get; set; }
        public int SettleSynthesisID { get; set; }
        public string ActionCode { get; set; }
        public string ActionTitle { get; set; }
        public double PlanSetttleMoney { get; set; }
        public double SetttleMoney { get; set; }
        public double? SetttleMoneyChange { get; set; }
        public string Description { get; set; }
        public string DescriptionChange { get; set; }
        public Nullable<long> ParentID { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<int> PlanTargetActionID { get; set; }
        public Nullable<int> SubPlanActionID { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
        public string ExpenseName { get; set; }
        public int? CostUpdateUserID { get; set; }
        public int? CostUpdateType { get; set; }
        public int? PlanTargetActionSpendItemID { get; set; }
        public int? SubActionSpendItemID { get; set; }
        public List<SettleSynCostModel> ListChildCost { get; set; }
        public string ParentPath { get; set; }
        public string ParentType { get; set; }
    }
}