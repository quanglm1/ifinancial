﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ReportTemplateModel
    {
        public int ReportTemplateID { get; set; }
        public string ReportName { get; set; }
        public string FileName { get; set; }
        public byte[] Template { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public bool IsDelTemplate { get; set; }
        public HttpPostedFileWrapper ReportTemplateAvatar { get; set; }

        public ReportTemplate ToEntity()
        {
            ReportTemplate entity = new ReportTemplate();
            entity.ReportName = this.ReportName;
            entity.Status = Constants.IS_ACTIVE;
            entity.CreateDate = DateTime.Now;
            entity.Template = this.Template;
            entity.FileName = this.FileName;
            entity.Description = this.Description;
            if(entity.Template == null)
            {
                entity.FileName = null;
            }
            return entity;
        }
        public void UpdateEntity(ReportTemplate entity)
        {
            if (entity == null)
            {
                return;
            }
            entity.ReportName = this.ReportName;
            entity.Template = this.Template;
            entity.FileName = this.FileName;
            entity.Description = this.Description;
            if (this.IsDelTemplate)
            {
                entity.Template = null;
                entity.FileName = null;
            }
            else
            {
                if (this.Template != null && this.Template.LongLength > 0)
                {
                    entity.Template = this.Template;
                }
            }
        }
    }
}