﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class DeptCostInfo
    {
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public int DeptType { get; set; }
        public int Year { get; set; }
        public int TargetID { get; set; }
        public string TargetTitle { get; set; }
        public string TargetName { get; set; }
        public string ActionCode { get; set; }
        public int ActionID { get; set; }
        public string ActionName { get; set; }
        public double Cost { get; set; }
    }
}