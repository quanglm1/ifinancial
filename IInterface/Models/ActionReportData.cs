﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportData
    {
        public int ActionReportID { get; set; }
        public int PeriodID { get; set; }
        public int Year { get; set; }
        public int? Quarter { get; set; }
        public string Advantage { get; set; }
        public string Difficulty { get; set; }
        public string Propose { get; set; }
        public int[] ArrPlanTargetID { get; set; }
        public string[] ArrPlanTargetResult { get; set; }
        public string[] ArrPlanTargetDesc { get; set; }
        public string[] ArrPlanTargetPlanContent { get; set; }
        public int[] ArrPlanTargetActionID { get; set; }
        public string[] ArrPlanTargetActionResult { get; set; }
        public string[] ArrPlanTargetActionDes { get; set; }
        public string[] ArrPlanTargetActionPlanContent { get; set; }
        public int[] ArrSubActionPlanID { get; set; }
        public string[] ArrSubActionPlanResult { get; set; }
        public string[] ArrSubActionPlanDes { get; set; }
        public string[] ArrSubActionPlanPlanContent { get; set; }
    }
}