﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class CostEstimateSpendModel
    {
        public long CostEstimateSpendID { get; set; }
        public int PlanID { get; set; }
        public int? MainActionID { get; set; }
        public double? TotalPrice { get; set; }
        public Nullable<int> PlanTargetID { get; set; }
        public Nullable<int> PlanTargetActionID { get; set; }
        public Nullable<int> SubActionPlanID { get; set; }
        public string SpendContent { get; set; }
        public string SpendItemTitle { get; set; }
        public Nullable<long> ParentID { get; set; }
        public Nullable<int> IsSalary { get; set; }
        public Nullable<int> OtherType { get; set; }
        public Nullable<int> SalaryType { get; set; }
        public List<CostEstimateSpendModel> ListChildSpend { get; set; }
        public List<CostEstimateDetailModel> ListDetail { get; set; }
    }
}