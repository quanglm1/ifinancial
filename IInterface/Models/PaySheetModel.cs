﻿using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class PaySheetModel
    {
        public int PaySheetId { get; set; }
        public int? NumOfEmployee { get; set; }
        public double? Coefficient { get; set; }
        public double? BasicSalary { get; set; }
        public double? IncreasedSalary { get; set; }
        public double? TotalPerMonth { get; set; }
        public double? TotalPerYear { get; set; }
        public double? InsurranceDepartmentPay { get; set; }
        public double? InsurranceEmplyeePay { get; set; }
        public double? ActuallyReceived { get; set; }
        public double? TotalSalary { get; set; }
        public int Year { get; set; }
        public int Type { get; set; }
    }
}