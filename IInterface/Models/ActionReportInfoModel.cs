﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ActionReportInfoModel
    {
        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public string PlanName { get; set; }
        public string Advantage { get; set; }
        public string Difficulty { get; set; }
        public string Propose { get; set; }
        public int PeriodID { get; set; }
        public int PlanID { get; set; }
        public int ActionReportID { get; set; }
        public int Year { get; set; }
        public Nullable<int> Quarter { get; set; }
        public System.DateTime? CreateDate { get; set; }
        public System.DateTime? SendDate { get; set; }
        public string Creator { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
        public bool IsHasReview { get; set; }
        public int Role { get; set; }
    }
}