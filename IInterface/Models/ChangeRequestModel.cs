﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class ChangeRequestModel
    {
        public int ChangeRequestID { get; set; }
        public int PlanID { get; set; }
        public int PeriodID { get; set; }
        public string PeriodName { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CreateDate { get; set; }
        public string Description { get; set; }
        public int CreateUserID { get; set; }
        public int Status { get; set; }
        public Nullable<DateTime> LastUpdateDate { get; set; }
        public Nullable<int> LastUpdateUserID { get; set; }
        public Nullable<int> StepNow { get; set; }
        public int Role { get; set; }
        public List<int> PlanTargetActionIDs { get; set; }
    }
}