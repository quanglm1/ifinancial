﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class FileDownloadForm
    {
        public string Id { get; set; }
        public string EncodedData { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}