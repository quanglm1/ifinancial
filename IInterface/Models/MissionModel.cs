﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IInterface.Models
{
    public class MissionModel
    {
        public int MissionID { get; set; }
        public System.DateTime CreateTime { get; set; }

        [DisplayName("Mã nhiệm vụ")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        [MaxLength(500, ErrorMessage = "{0} không được vượt quá 200 ký tự")]
        public string MissionCode { get; set; }

        [DisplayName("Tên nhiệm vụ")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string MissionName { get; set; }
        public string Creator { get; set; }

        [DisplayName("Trạng thái")]
        public int Status { get; set; }

        [DisplayName("Nội dung")]
        public string MissionContent { get; set; }
        public double PlanMoney { get; set; }
        public double ActualMoney { get; set; }
        public string Color { get; set; }
        public List<MainActionModel> ListAction { get; set; }

        internal void UpdateEntity(Mission entity)
        {
            entity.MissionID = this.MissionID;
            //entity.CreateTime = CreateTime;
            entity.Creator = Creator;
            entity.MissionCode = MissionCode;
            entity.MissionContent = MissionContent;
            entity.MissionName = MissionName;
            entity.Status = Status;
        }

        internal Mission ToEntity(string userName)
        {
            Mission entity = new Mission();
            entity.MissionID = this.MissionID;
            entity.CreateTime = CreateTime;
            entity.Creator = userName;
            entity.MissionCode = MissionCode;
            entity.MissionContent = MissionContent;
            entity.MissionName = MissionName;
            entity.Status = Status;
            return entity;
        }
    }
}