﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubActionPlanBO
    {
        public int PlanTargetID { get; set; }
        public int PlanTargetActionID { get; set; }
        public int? SubActionPlanID { get; set; }
        public string ChangeContent { get; set; }
    }
}