﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using IModel;
using IInterface.Common;

namespace IInterface.Models
{
    public class MainIndexModel
    {
        public int MainIndexID { get; set; }
        [DisplayName("Nội dung")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public string ContentIndex { get; set; }
        [DisplayName("Loại chỉ số")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? TypeIndex { get; set; }
        public System.DateTime? CreateTime { get; set; }
        public string Creator { get; set; }
        public int? Status { get; set; }
        [DisplayName("Cấp áp dụng")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? Type { get; set; }
        [DisplayName("Mục tiêu")]
        [Required(ErrorMessage = "{0} bắt buộc nhập")]
        public int? TargetID { get; set; }
        [DisplayName("Ánh xạ chỉ số quỹ")]
        public int? MappingIndexID { get; set; }
        [DisplayName("Số thứ tự")]
        public int? OrderNumber { get; set; }
        [DisplayName("Giá trị")]
        public int? IndexValue { get; set; }
        public string TargetName { get; set; }


        public void UpdateEntity(MainIndex entity)
        {
            entity.Type = Type;
            entity.MainIndexID = MainIndexID;
            entity.ContentIndex = ContentIndex;
            entity.TypeIndex = TypeIndex;
            entity.IndexValue = IndexValue;
            entity.TargetID = TargetID;
            entity.Creator = Creator;
            entity.OrderNumber = OrderNumber;
            entity.MappingIndexID = MappingIndexID;
            if (entity.Type == Constants.MAIN_TYPE)
            {
                entity.MappingIndexID = null;
            }
            if (TypeIndex == Constants.TYPE_INDEX_CHOICE)
            {
                entity.IndexValue = null;
            }
            else if (TypeIndex == Constants.TYPE_INDEX_INPUT)
            {
                if (entity.IndexValue != null)
                {
                    if (entity.IndexValue <= 0)
                    {
                        throw new BusinessException("Giá trị phải là số nguyên dương");
                    }
                }
            }
            else
            {
                if (entity.IndexValue != null)
                {
                    if (entity.IndexValue < 0 || entity.IndexValue > 100)
                    {
                        throw new BusinessException("Giá trị phải là số nguyên dương từ 0 đến 100");
                    }
                }
            }
        }

        public MainIndex ToEntity(string userName)
        {
            MainIndex entity = new MainIndex
            {
                Type = Type,
                MainIndexID = MainIndexID,
                ContentIndex = ContentIndex,
                TypeIndex = TypeIndex,
                IndexValue = IndexValue,
                Status = Status,
                TargetID = TargetID,
                Creator = userName,
                CreateTime = CreateTime,
                OrderNumber = OrderNumber,
                MappingIndexID = MappingIndexID,
            };
            if (entity.Type == Constants.MAIN_TYPE)
            {
                entity.MappingIndexID = null;
            }
            if (entity.TypeIndex == Constants.TYPE_INDEX_CHOICE)
            {
                entity.IndexValue = null;
            }
            else if (entity.TypeIndex == Constants.TYPE_INDEX_INPUT)
            {
                if (entity.IndexValue != null)
                {
                    if (entity.IndexValue <= 0)
                    {
                        throw new BusinessException("Giá trị phải là số nguyên dương");
                    }
                }
            }
            else
            {
                if (entity.IndexValue != null)
                {
                    if (entity.IndexValue < 0 || entity.IndexValue > 100)
                    {
                        throw new BusinessException("Giá trị phải là số nguyên dương từ 0 đến 100");
                    }
                }
            }
            return entity;
        }
    }
}
