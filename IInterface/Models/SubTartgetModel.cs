﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class SubTartgetModel
    {
        public int SubTargetID { get; set; }
        public int TargetID { get; set; }
        public string SubTargetTitle { get; set; }
        public string SubTargetContent { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> TargetOrder { get; set; }
        public List<SubActionPlanModel> ListSubActionPlanModel { get; set; }
    }
}