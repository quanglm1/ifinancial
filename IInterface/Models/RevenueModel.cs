﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IInterface.Models
{
    public class RevenueModel
    {
        public int RevenueID { get; set; }
        [DisplayName("Giai đoạn")]
        public int PeriodID { get; set; }
        public string PeriodName { get; set; }
        [DisplayName("Ngày thu")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> RevenueDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        [DisplayName("Nguồn thu")]
        public string Source { get; set; }
        [DisplayName("Tổng tiền")]
        public string Money { get; set; }
        public string MoneyFormat
        {
            get
            {
                string s = string.Empty;
                if (!string.IsNullOrWhiteSpace(this.Money))
                {
                    double m;
                    if (double.TryParse(this.Money, out m))
                    {
                        s = Utils.FormatMoney(m, true);
                    }
                }
                return s;
            }
        }
        [DisplayName("Ghi chú")]
        public string Description { get; set; }

        public void EntityTo(Revenue entity)
        {
            this.Description = entity.Description;
            this.RevenueID = entity.RevenueID;
            this.PeriodID = entity.PeriodID;
            this.RevenueDate = entity.RevenueDate;
            this.CreateDate = entity.CreateDate;
            this.Source = entity.Source;
            if (entity.Money != null)
            {
                this.Money = entity.Money.ToString();
            }
        }

        internal void UpdateEntity(Revenue entity)
        {
            entity.Description = this.Description;
            entity.PeriodID = this.PeriodID;
            entity.RevenueDate = this.RevenueDate;
            entity.Source = this.Source;
            if (!string.IsNullOrWhiteSpace(this.Money))
            {
                double m;
                if (double.TryParse(this.Money, out m))
                {
                    entity.Money = m;
                }
            }
        }

        internal Revenue ToEntity()
        {
            Revenue entity = new Revenue();
            entity.Description = this.Description;
            entity.PeriodID = this.PeriodID;
            entity.RevenueDate = this.RevenueDate;
            entity.Source = this.Source;
            if (!string.IsNullOrWhiteSpace(this.Money))
            {
                double m;
                if (double.TryParse(this.Money, out m))
                {
                    entity.Money = m;
                }
            }
            entity.CreateDate = DateTime.Now;
            return entity;
        }
    }
}