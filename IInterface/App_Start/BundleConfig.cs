﻿using System.Web;
using System.Web.Optimization;

namespace IInterface
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //---------------Script-------------------
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.contextMenu.*",
                        "~/Scripts/jquery.maskMoney.*"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/Chart.*",
                        "~/Scripts/CommonJS.*",
                        "~/admin-lte/js/*.js",
                        "~/Content/summernote-0.8.7-dist/dist/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.*",
                      "~/Scripts/respond.js",
                      "~/Scripts/tinymce/*.js",
                      "~/Scripts/bootstrap-date*",
                      "~/Scripts/moment.js",
                      "~/Scripts/select2.js",
                      "~/Scripts/Pace.js",
                      "~/Scripts/Chart.PieceLabel.js",
                      "~/Scripts/jquery.smartWizard.*"));

            //------------------Style-------------------
            bundles.Add(new StyleBundle("~/Content/css/styles").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/admin-lte/css/*.css",
                      "~/admin-lte/css/skins/_all-skins.*",
                      "~/Content/css/font-awesome.*",
                      "~/Content/bootstrap-datepicker*",
                      "~/Content/select2.css",
                      "~/Content/smart_wizard.css",
                      "~/Content/pace-theme-center-atom.css",
                      "~/Content/smart_wizard_theme_arrows.*",
                      "~/Content/summernote-0.8.7-dist/dist/summernote.css"));
        }
    }
}
