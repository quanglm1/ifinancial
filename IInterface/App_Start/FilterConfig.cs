﻿using IInterface.Filter;
using System.Web;
using System.Web.Mvc;

namespace IInterface
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeFilter());
            filters.Add(new ActionLogFilter());
            filters.Add(new ExceptionFilter());
        }
    }
}
