﻿
var CommonJS = {
    HEADER_HIGHT: 271,

    baseAlert: function (msg) {
        if (msg.Message != null) {
            alert(msg.Message);
            return msg;
        }
        json = {};
        try {
            json = $.parseJSON(msg);
        } catch (e) {
            alert(msg);
            return;
        }

        alert(json.Message);
        return json;
    },

    alert: function (msg, type) {
        $("#_GlobalMessage").attr("class", "");
        if (type == null || type == undefined || type == "") {
            $("#_GlobalMessage").addClass("msg-type-SUCCESS");
        }
        if (type == false) {
            $("#_GlobalMessage").addClass("msg-type-ERROR");
        }

        if (msg.Message != null) {
            $("#_GlobalMessage").html(msg.Message);
            $("#_GlobalMessage").fadeIn();
            setTimeout('$("#_GlobalMessage").fadeOut();', 4000);
            return msg;
        }
        json = {};
        try {
            json = $.parseJSON(msg);
        } catch (e) {
            $("#_GlobalMessage").html(msg);
            $("#_GlobalMessage").fadeIn();
            setTimeout('$("#_GlobalMessage").fadeOut();', 4000);
            return;
        }

        if (json == null) {
            $("#_GlobalMessage").addClass("msg-type-ERROR");
            $("#_GlobalMessage").html("Lỗi: Mất kết nối tới máy chủ vui lòng đợi trong giây lát");
        } else {
            $("#_GlobalMessage").addClass("msg-type-" + json.Type);
            $("#_GlobalMessage").html(json.Message);
        }
        $("#_GlobalMessage").fadeIn();
        setTimeout('$("#_GlobalMessage").fadeOut();', 4000);
        return json;
    },

    back: function () {
        history.back(1);
    },

    checkDate: function checkDate(date) {
        var minYear = 1902;
        var errorMsg = "";
        // regular expression to match required date format 
        re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

        if (date != '') {
            if (regs = date.match(re)) {
                if (regs[1] < 1 || regs[1] > 31) {
                    errorMsg = "Invalid value for day: " + regs[1];
                    return false;
                }
                else if (regs[2] < 1 || regs[2] > 12) {
                    errorMsg = "Invalid value for month: " + regs[2];
                    return false;
                }
                else if (regs[3] < minYear) {
                    errorMsg = "Invalid value for year: " + regs[3] + " - must be between " + minYear;
                    return false;
                }
            }
            else {
                errorMsg = "Invalid date format: " + date;
                return false;
            }
        }
        else {
            errorMsg = "Empty date not allowed!";
            return false;
        }

        return true;
    },
    ReloadPage: function () {
        window.location.href = window.location.href;
    },
    ClearAllCache: function () {
        sessionStorage.clear();
    }
}