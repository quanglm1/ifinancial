﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Encoder = System.Drawing.Imaging.Encoder;

namespace IInterface.Common
{
    public class Utils
    {
        public static MemoryStream GetJpeg(Image img, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new BusinessException("Dữ liệu không hợp lệ");

            EncoderParameter qualityParam =
                new EncoderParameter(Encoder.Quality, quality);
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo jpegCodec = codecs.FirstOrDefault(x => x.MimeType.Equals("image/jpeg"));

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            using (MemoryStream s = new MemoryStream())
            {
                img.Save(s, jpegCodec, encoderParams);
                return s;
            }
        }

        public static string BytesToSize(double? byteCount)
        {
            if (!byteCount.HasValue)
            {
                return "0B";
            }
            string[] units = new string[] { "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            double thresh = 1024;
            if (Math.Abs(byteCount.Value) < thresh)
            {
                return byteCount + " B";
            }
            var u = -1;
            do
            {
                byteCount /= thresh;
                ++u;
            } while (Math.Abs(byteCount.Value) >= thresh && u < units.Length - 1);
            return Math.Round(byteCount.Value, 1) + " " + units[u];
        }

        public static string ToRoman(int number)
        {
            String[] roman = new String[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
            int[] decimals = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

            string romanvalue = String.Empty;

            for (int i = 0; i < 13; i++)
            {
                while (number >= decimals[i])
                {
                    number -= decimals[i];
                    romanvalue += roman[i];
                }
            }
            return romanvalue;
        }

        public static string ToQuarter(int? number)
        {
            string quarter = "";
            switch (number)
            {
                case 1:
                    quarter = "I";
                    break;
                case 2:
                    quarter = "II";
                    break;
                case 3:
                    quarter = "III";
                    break;
                case 4:
                    return "IV";
                default:
                    break;
            }
            return quarter;
        }

        public static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
        public static string ChuyenSo(string number)
        {
            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "lẻ ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if (i + j == len - 1)
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += dv[n - j - 1] + " ";
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];
            doc = doc.First().ToString().ToUpper() + doc.Substring(1);
            List<string> listDoc = doc.Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            doc = string.Join(" ", listDoc);
            return doc + " đồng";
        }

        public static IEnumerable<SelectListItem> GetDepartmentType()
        {
            List<SelectListItem> res = new List<SelectListItem>
            {
                new SelectListItem {Text = "-- Chọn --", Value = ""},
                new SelectListItem {Text = "Phòng ban quỹ", Value = Constants.DEPT_TYPE_PBQUY.ToString()},
                new SelectListItem {Text = "Đơn vị cấp bộ ngành", Value = Constants.DEPT_TYPE_BO_NGANH.ToString()},
                new SelectListItem {Text = "Đơn vị cấp tỉnh/thành phố", Value = Constants.DEPT_TYPE_TINH_TP.ToString()},
                new SelectListItem {Text = "Đơn vị có thành phố du lịch", Value = Constants.DEPT_TYPE_TP_DU_LICH.ToString()},
                new SelectListItem {Text = "Đơn vị bệnh viện cai nghiện", Value = Constants.DEPT_TYPE_BV_CAI_NGHIEN.ToString()},
                new SelectListItem {Text = "Đơn vị sáng kiến", Value = Constants.DEPT_TYPE_SANG_KIEN.ToString()}
            };

            return res;
        }

        public static string GetDepartmentType(int type)
        {
            string deptName = string.Empty;
            switch (type)
            {
                case Constants.DEPT_TYPE_PBQUY:
                    deptName = "Phòng ban quỹ";
                    break;
                case Constants.DEPT_TYPE_BO_NGANH:
                    deptName = "Đơn vị cấp bộ ngành";
                    break;
                case Constants.DEPT_TYPE_TINH_TP:
                    deptName = "Đơn vị cấp tỉnh/thành phố";
                    break;
                case Constants.DEPT_TYPE_TP_DU_LICH:
                    deptName = "Đơn vị có thành phố du lịch";
                    break;
                case Constants.DEPT_TYPE_BV_CAI_NGHIEN:
                    deptName = "Đơn vị bệnh viện cai nghiện";
                    break;
                case Constants.DEPT_TYPE_SANG_KIEN:
                    deptName = "Đơn vị sáng kiến";
                    break;
            }
            return deptName;
        }

        public static string GetReviewType(int type)
        {
            string typeName = string.Empty;
            switch (type)
            {
                case Constants.PLAN_TYPE_SEND:
                    typeName = "Gửi kế hoạch";
                    break;
                case Constants.PLAN_TYPE_AGREE:
                    typeName = "Đồng ý kế hoạch và gửi lên cấp tiếp theo";
                    break;
                case Constants.PLAN_TYPE_APPROVE:
                    typeName = "Phê duyệt kế hoạch";
                    break;
                case Constants.PLAN_TYPE_UNAPPROVE:
                    typeName = "Từ chối kế hoạch";
                    break;
                case Constants.PLAN_TYPE_UNLOCK:
                    typeName = "Mở khóa kế hoạch";
                    break;
            }
            return typeName;
        }

        public static IEnumerable<SelectListItem> GetStatusLst()
        {
            List<SelectListItem> res = new List<SelectListItem>();
            res.Add(new SelectListItem { Text = "Hiệu lực", Value = "1" });
            res.Add(new SelectListItem { Text = "Hết hiệu lực", Value = "0" });

            return res;
        }

        public static bool IsStrongPassword(string password, out string ErrorMessage)

        {
            ErrorMessage = string.Empty;
            // Minimum and Maximum Length of field - 6 to 12 Characters
            if (password.Length < 6 || password.Length > 18)

            {
                ErrorMessage = "Mật khẩu không được nhỏ hơn 6 ký tự hoặc không được lớn hơn 18 ký tự";
                return false;
            }
            // Check white space in pass
            if (password.Any(c => char.IsWhiteSpace(c)))
            {
                ErrorMessage = "Mật khẩu không được chứa dấu cách";
                return false;
            }
            // Numeric Character - At least one character
            if (!password.Any(c => char.IsLetter(c)))
            {
                ErrorMessage = "Mật khẩu phải chứa ít nhất một ký tự chữ";
                return false;
            }
            if (!password.Any(c => char.IsNumber(c)))
            {
                ErrorMessage = "Mật khẩu phải bao gồm số";
                return false;
            }
            return true;
        }

        public static string RandomCaptcha(int length)
        {
            Random random = new Random();
            //string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder captcha = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                captcha.Append(combination[random.Next(combination.Length)]);
            }
            return captcha.ToString();
        }

        public static void Copy(object src, object des)
        {
            var parentProperties = src.GetType().GetProperties();
            var childProperties = des.GetType().GetProperties();
            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                    {
                        childProperty.SetValue(des, parentProperty.GetValue(src));
                        break;
                    }
                }
            }
        }

        public static string FormatMoney(double? num, bool isDot = false)
        {
            if (num == null)
            {
                return string.Empty;
            }
            string mid = ",";
            if (isDot)
            {
                mid = ".";
            }
            string start = string.Empty;
            if (num < 0)
            {
                num = 0 - num;
                start = "-";
            }
            string str = num.ToString().Replace("$", "");
            List<string> parts = new List<string>();
            var output = new List<string>();
            var i = 1;
            string formatted = null;
            if (str.IndexOf(".") > 0)
            {
                parts = str.Split('.').ToList();
                str = parts[0];
            }
            str = new string(str.Reverse().ToArray());
            int len = str.Length;
            for (int j = 0; j < len; j++)
            {
                if (str[j].ToString() != mid)
                {
                    output.Add(str[j].ToString());
                    if (i % 3 == 0 && j < (len - 1))
                    {
                        output.Add(mid);
                    }
                    i++;
                }
            }
            formatted = new string(string.Join("", output).Reverse().ToArray());
            return start + (formatted + ((parts.Any()) ? "." + (parts[1].Length > 2 ? parts[1].Substring(0, 2) : parts[1]) : ""));
        }

        public static bool StringEqual(string a, string b)
        {
            if (string.IsNullOrEmpty(a))
            {
                return string.IsNullOrEmpty(b);
            }
            else
            {
                return string.Equals(a, b);
            }
        }

        public static Dictionary<int, string> GetColorCode()
        {
            Dictionary<int, string> dicColor = new Dictionary<int, string>();
            dicColor[0] = "#FF9800";
            dicColor[1] = "#F44336";
            dicColor[2] = "#2196F3";
            dicColor[3] = "#FFEB3B";
            dicColor[4] = "#9C27B0";
            dicColor[5] = "#607D8B";
            dicColor[6] = "#8BC34A";
            dicColor[7] = "#00C853";
            dicColor[8] = "#009688";
            dicColor[9] = "#3F51B5";
            dicColor[10] = "#3E2723";
            dicColor[11] = "#4A148C";
            return dicColor;
        }

        public static string GetStandardString(string s)
        {
            if (s == null)
            {
                return string.Empty;
            }
            return s.Trim();
        }

        public static string GenSpace(int n)
        {
            string s = string.Empty;
            for (int i = 0; i < n; i++)
            {
                s += "..";
            }
            return n == 0 ? s : s + " ";
        }
    }
}