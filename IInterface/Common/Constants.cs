﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace IInterface.Common
{
    public class Constants
    {
        public const int IS_ACTIVE = 1;
        public const int IS_NOT_ACTIVE = 0;
        public const int IS_DELETED_USER = 2;
        public const int PAGE_SIZE = 10;
        public const int PAGE_SIZE_MAX = 20;
        public const int PAGE_SIZE_SUPER_MAX = 100;
        public const int PAGE_SIZE_LITE = 5;

        public const int PERIOD_DEACTIVED = 0;
        public const int PERIOD_NEW = 1;
        public const int PERIOD_PLANNED = 2;
        public const int PERIOD_NEXT_LEVER = 3;
        public const int PERIOD_APPROVED = 4;

        public const int PLAN_DEACTIVED = 0;
        public const int PLAN_NEW = 1;
        public const int PLAN_PLANNED = 2;
        public const int PLAN_NEXT_LEVER = 3;
        public const int PLAN_APPROVED = 4;

        public const int REPORT_NEW = 1;
        public const int REPORT_SENT = 2;

        public const int ACTION_ADD_NEW = 1;
        public const int ACTION_ADD_CHILD = 2;
        public const int ACTION_EDIT = 3;

        public const int OTHER_TYPE_DU_PHONG = 1;
        public const int OTHER_TYPE_HC = 2;

        public const int TYPE_BDH = 1;
        public const int TYPE_BKS = 2;

        //Tinh vao tong gia
        public const int SUM_COST_TYPE_TOTAL = 1;
        //Tinh vao don gia
        public const int SUM_COST_TYPE_PRICE = 2;

        //Bao cao quy
        public const int REPORT_TYPE_QUARTER = 1;
        //Bao cao nam
        public const int REPORT_TYPE_YEAR = 2;

        //Don vi quy
        public const int MAIN_TYPE = 0;
        //Don vi cap duoi
        public const int SUB_TYPE = 1;
        //Don vi quy
        public const string DEPT_MAIN_TYPE_NAME = "Đơn vị quỹ";
        //Don vi cap duoi
        public const string DEPT_SUB_TYPE_NAME = "Đơn vị thuộc quỹ";

        public const int TYPE_INDEX_CHOICE = 1;
        public const int TYPE_INDEX_INPUT = 2;
        public const int TYPE_INDEX_INPUT_AVERAGE = 3;

        public const string TYPE_NAME_INDEX_CHOICE = "Lựa chọn";
        public const string TYPE_NAME_INDEX_INPUT = "Tuyệt đối";
        public const string TYPE_NAME_INDEX_AVERAGE = "Tỷ lệ";

        public const string PLAN_ID = "PlanID";
        public const string TARGET_ID = "TargetID";
        public const string PLAN_MARKED_COMMENT = "Kế hoạch đã được thông qua";
        public const string PLAN_SENT_COMMENT = "Hoàn thành kế hoạch và yêu cầu xem xét";
        public const string PLAN_AGREE_SENT_COMMENT = "Đã xem xét kế hoạch và gửi lên cấp trên";
        public const string PLAN_UNLOCK_COMMENT = "Kế hoạch được mở khóa để cấp dưới chỉnh sửa";
        public const string PLAN_UNAPPROVE_COMMENT = "Kế hoạch bị từ chối chuyển lại đầu mối để chỉnh sửa";
        public const string PLAN_APPROVE_COMMENT = "Kế hoạch được phê duyệt";

        public const int DEPT_TYPE_PBQUY = 1;
        public const int DEPT_TYPE_BO_NGANH = 2;
        public const int DEPT_TYPE_TINH_TP = 3;
        public const int DEPT_TYPE_TP_DU_LICH = 4;
        public const int DEPT_TYPE_BV_CAI_NGHIEN = 5;
        public const int DEPT_TYPE_SANG_KIEN = 6;

        public const int UPDATE_TYPE_TAO = 1;
        public const int UPDATE_TYPE_REVIEW = 2;

        public const int COMMENT_TYPE_PLAN_TARGET = 1;
        public const int COMMENT_TYPE_SETTLE = 2;
        public const int COMMENT_TYPE_FINANCIAL = 3;
        public const int COMMENT_TYPE_ACTION = 4;
        public const int COMMENT_TYPE_INDEX = 5;
        public const int COMMENT_TYPE_CHANGE_REQUEST = 6;
        public const int COMMENT_TYPE_INDEX_HIS = 7;

        public const int REPORT_TYPE_SETTLE = 1;
        public const int REPORT_TYPE_FINANCIAL = 2;
        public const int REPORT_TYPE_ACTION = 3;
        public const int REPORT_TYPE_CHANGE_REQUEST = 4;


        public const int REPORT_ACTION_TYPE_SEND = 1;
        public const int REPORT_ACTION_TYPE_AGREE = 2;
        public const int REPORT_ACTION_TYPE_UNLOCK = 3;
        public const int REPORT_ACTION_TYPE_UNAPPROVE = 4;
        public const int REPORT_ACTION_TYPE_APPROVE = 5;

        public const int PLAN_TYPE_SEND = 1;
        public const int PLAN_TYPE_AGREE = 2;
        public const int PLAN_TYPE_UNLOCK = 3;
        public const int PLAN_TYPE_UNAPPROVE = 4;
        public const int PLAN_TYPE_APPROVE = 5;

        public const int PARAGRAPH_TARGET_ACTION_TITLE = 1;
        public const int PARAGRAPH_TARGET_ACTION_CONTENT = 2;
        public const int PARAGRAPH_TARGET_ACTION_OBJECT = 3;
        public const int PARAGRAPH_TARGET_ACTION_METHOD = 4;
        public const int PARAGRAPH_TARGET_ACTION_RESULT = 5;
        public const int PARAGRAPH_TARGET_ACTION_TIME = 6;

        public const int FLOW_ROLE = 1;
        public const int FLOW_DEPT = 2;

        public const int FLOW_WORK_REVIEW = 1;
        public const int FLOW_WORK_APPROVE = 2;

        public const int ROLE_NO_VIEW = 0;
        public const int ROLE_VIEW = 1;
        public const int ROLE_EDIT = 2;
        public const int ROLE_APPROVE = 3;
        public const int ROLE_AFTER_APPROVE = -1;

        public static int HEAD_DEPT_ID = int.Parse(ConfigurationManager.AppSettings["ID_QUY"]);
        public static int REPORT_DAY = int.Parse(ConfigurationManager.AppSettings["REPORT_DAY"]);
        public static int REPORT_WARNING_BEFORE_DAY = int.Parse(ConfigurationManager.AppSettings["REPORT_WARNING_BEFORE_DAY"]);
        public static int LOGIN_FAIL_COUNT = 5;
        public const string OTHER_NAME_INDEX = "[Chỉ số khác]";
        public const string OTHER_CHOICE_ACTION = "---Chọn hoạt động gợi ý---";
        public const string OTHER_NAME_ACTION = "[Hoạt động khác]";
        public const string OTHER_NAME_TARGET = "[Mục tiêu khác]";

        public const string QUY_SPEND_OTHER = "Kinh phí dự phòng cho các hoạt động phát sinh";
        public const string QUY_SPEND_HC = "Chi phí hành chính";
        public const string QUY_SPEND_HC_LUONG = "Tiền lương, phụ cấp";
        public const string QUY_SPEND_HC_LUONG_BDH = "Ban điều hành";
        public const string QUY_SPEND_HC_LUONG_BKS = "Ban kiểm soát";
        public const string QUY_SPEND_HC_LUONG_PHU_CAP = "Phụ cấp";
        public const string QUY_SPEND_HC_LUONG_CHINH = "Tiền lương";

        public const int QUY_SPEND_HC_LUONG_PHU_CAP_TYPE = 1;
        public const int QUY_SPEND_HC_LUONG_CHINH_TYPE = 2;
    }
}