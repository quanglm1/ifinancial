﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Collections.Generic;

namespace IInterface.Common.OpenXML
{
    public static class ParagraphExt
    {
        public static void AddAlign(this Paragraph p, int pos = 7930)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            Tabs tabs = new Tabs();
            TabStop tabStop = new TabStop() { Val = TabStopValues.Left, Position = pos };
            tabs.Append(tabStop);
            pr.Append(tabs);
        }

        public static void AddRun(this Paragraph p, List<Run> listRun)
        {
            if (listRun != null)
            {
                listRun.ForEach(x =>
                {
                    p.Append(x);
                });
            }
        }

        public static void AddSpacing(this Paragraph p, string line, string before = "0", string after = "0")
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            SpacingBetweenLines s = new SpacingBetweenLines { After = after, Line = line, Before = before, LineRule = LineSpacingRuleValues.Auto };
            pr.Append(s);
        }


        public static void AddIndentation(this Paragraph p, string firstLine = "720")
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            Indentation indentation = new Indentation() { FirstLine = firstLine };
            pr.Append(indentation);
        }

        public static void AddBasicSpacing(this Paragraph p, string left = null)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            SpacingBetweenLines s = new SpacingBetweenLines { After = "0", Line = "240", Before = "60", LineRule = LineSpacingRuleValues.Auto };
            pr.Append(s);
            Justification just = new Justification() { Val = JustificationValues.Both };
            pr.Append(just);
            if (!string.IsNullOrWhiteSpace(left))
            {
                Indentation indentation = new Indentation() { Left = left };
                pr.Append(indentation);
            }
        }

        public static void AddBasicBullet(this Paragraph p, int val = 1)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            NumberingProperties numberingProperties = new NumberingProperties();
            NumberingLevelReference numberingLevelReference = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId = new NumberingId() { Val = val };
            numberingProperties.Append(numberingLevelReference);
            numberingProperties.Append(numberingId);
            pr.Append(numberingProperties);
        }

        public static void AddBoth(this Paragraph p)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            Justification just = new Justification() { Val = JustificationValues.Both };
            pr.Append(just);
        }

        public static void AddCenter(this Paragraph p)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            Justification center = new Justification() { Val = JustificationValues.Center };
            pr.Append(center);
        }

        public static void AddLeft(this Paragraph p)
        {
            ParagraphProperties pr = p.ParagraphProperties;
            if (pr == null)
            {
                pr = p.AppendChild(new ParagraphProperties());
            }
            Justification center = new Justification() { Val = JustificationValues.Left };
            pr.Append(center);
        }

        public static void AddRight(this Paragraph p)
        {
            ParagraphProperties centerProperty = new ParagraphProperties();
            Justification center = new Justification() { Val = JustificationValues.Right };
            centerProperty.Append(center);
            p.Append(centerProperty);
        }

        public static void AddText(this Paragraph p, string text, string fontName = "Times New Roman", string fontSize = "26", bool isBold = false, bool isNewLine = false, bool isUnderline = false, bool isItalic = false)
        {
            Run r = new Run();
            r.AddProperties(fontName, fontSize, isBold, isUnderline, isItalic);
            r.AddText(text);
            if (isNewLine)
            {
                r.AddNewLine();
            }
            p.Append(r);
        }

        public static void AddBreakPage(this Paragraph p)
        {
            p.Append(new Run(new Break { Type = BreakValues.Page }));
        }

        public static void AddNewLine(this Paragraph p, int line = 1)
        {
            for (int i = 0; i < line; i++)
            {
                p.Append(new Run(new Break()));
            }
        }

        public static void AddTab(this Paragraph p, int numTab = 1)
        {
            for (int i = 0; i < numTab; i++)
            {
                p.Append(new Run(new TabChar()));
            }
        }
    }
}