﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;

namespace IInterface.Common.OpenXML
{
    public static class TableExt
    {

        public static void AddBasicProperty(this Table t, bool isBorder = true)
        {
            TableProperties tableProp = new TableProperties();
            TableStyle tableStyle = new TableStyle() { Val = "TableGrid" };
            TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };
            tableProp.Append(tableStyle, tableWidth);
            if (isBorder)
            {
                TableBorders tableBorder = new TableBorders()
                {
                    BottomBorder = new BottomBorder { Val = BorderValues.BasicThinLines },
                    TopBorder = new TopBorder { Val = BorderValues.BasicThinLines },
                    LeftBorder = new LeftBorder { Val = BorderValues.BasicThinLines },
                    RightBorder = new RightBorder { Val = BorderValues.BasicThinLines },
                    InsideHorizontalBorder = new InsideHorizontalBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicThinLines) },
                    InsideVerticalBorder = new InsideVerticalBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicThinLines) }
                };
                tableProp.Append(tableBorder);
            }
            t.Append(tableProp);
        }

        public static void AddBigTable(this Table t)
        {
            TableProperties tableProp = new TableProperties();
            TableStyle tableStyle = new TableStyle() { Val = "TableGrid" };
            TableWidth tableWidth = new TableWidth() { Width = "5500", Type = TableWidthUnitValues.Pct };
            tableProp.Append(tableStyle, tableWidth);
            t.Append(tableProp);
        }

        public static void AddGridSpan(this TableCell c, int span)
        {
            GridSpan gridSpan = new GridSpan() { Val = span };
            TableCellProperties pro = c.TableCellProperties;
            if (pro == null)
            {
                pro = c.AppendChild(new TableCellProperties());
            }
            pro.Append(gridSpan);
        }

        /// <summary>
        /// Gan dong dai cho cell
        /// </summary>
        /// <param name="t"></param>
        /// <param name="percent">Ty le phan tram</param>
        public static void AddColWidth(this TableCell t, int percent)
        {
            TableCellWidth tableCellWidth = new TableCellWidth()
            {
                Width = (percent * 50).ToString(),
                Type = TableWidthUnitValues.Pct
            };
            t.Append(tableCellWidth);
        }

        public static void AddVerticalMerge(this TableCell c, bool isRestart = false)
        {
            TableCellProperties pro = c.TableCellProperties;
            if (pro == null)
            {
                pro = c.AppendChild(new TableCellProperties());
            }
            VerticalMerge verticalMerge = new VerticalMerge();
            if (isRestart)
            {
                verticalMerge.Val = MergedCellValues.Restart;
            }
            pro.Append(verticalMerge);
        }
    }
}