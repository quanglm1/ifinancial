﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;

namespace IInterface.Common.OpenXML
{
    public static class RunExt
    {
        public static void AddText(this Run r, string text)
        {
            if(r.RunProperties == null)
            {
                r.AddProperties();
            }
            r.Append(new Text() { Text = text, Space = SpaceProcessingModeValues.Preserve });
        }

        public static void AddNewLine(this Run r, int line = 1)
        {
            for (int i = 0; i < line; i++)
            {
                r.Append(new Break());
            }
        }
        public static void AddTab(this Run r, int numTab = 1)
        {
            for (int i = 0; i < numTab; i++)
            {
                r.Append(new TabChar());
            }
        }
        public static void AddSpace(this Run r, int numSpace = 0)
        {
            string s = "";
            for (int i = 0; i < numSpace; i++)
            {
                s += " ";
            }
            r.AddText(s);
        }
        public static void AddFont(this RunProperties r, string fontName = "Times New Roman", string fontSize = "26")
        {
            RunFonts runFont = new RunFonts() { Ascii = fontName, HighAnsi = fontName };
            FontSize font = new FontSize() { Val = fontSize };
            r.Append(runFont);
            r.Append(font);
        }

        public static void AddCheckBox(this Run r, string fontSize = "26", bool isCheck = false)
        {
            RunProperties runPro = new RunProperties();
            runPro.AddFont("Wingdings", fontSize);
            r.Append(runPro);
            if (isCheck)
            {
                r.Append(new SymbolChar() { Char = "00FE", Font = "Wingdings" });
            }
            else
            {
                r.Append(new SymbolChar() { Char = "00A8", Font = "Wingdings" });
            }
        }

        public static void AddProperties(this Run r, string fontName = "Times New Roman", string fontSize = "26", bool isBold = false, bool isUnderline = false, bool isItalic = false)
        {
            RunProperties p = new RunProperties();
            RunFonts runFont = new RunFonts() { Ascii = fontName, HighAnsi = fontName };
            FontSize font = new FontSize() { Val = fontSize };
            p.Append(runFont);
            p.Append(font);
            if (isBold)
            {
                Bold b = new Bold();
                p.Append(b);
            }
            if (isUnderline)
            {
                Underline underline = new Underline() { Val = UnderlineValues.Single };
                p.Append(underline);
            }
            if (isItalic)
            {
                Italic italic = new Italic();
                p.Append(italic);
            }
            r.Append(p);
        }
    }
}