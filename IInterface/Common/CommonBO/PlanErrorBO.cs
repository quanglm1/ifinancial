﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Common.CommonBO
{
    public class PlanErrorBO
    {
        public int Type { get; set; }
        public string Name { get; set; }
    }
}