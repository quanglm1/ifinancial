﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Common.CommonBO
{
    public class WarningBO
    {
        public int Type { get; set; }
        public int Status { get; set; }
        public int Count { get; set; }
        public int? Quarter { get; set; }
        public int Year { get; set; }
        public string ReportName
        {
            get
            {
                string name = string.Empty;
                if (Type == Constants.REPORT_TYPE_ACTION)
                {
                    name = "<a href=\"/ActionReport/IndexDept\">Báo cáo hoạt động";
                }
                else if (Type == Constants.REPORT_TYPE_FINANCIAL)
                {
                    name = "<a href=\"/FinancialReport/IndexDept\">Báo cáo tài chính";
                }
                else
                {
                    name = "<a href=\"/SettleSynthesis/IndexDept\">Báo cáo quyết toán";
                }
                if (Quarter != null)
                {
                    name += " quý " + Quarter + ", năm " + Year;
                }
                else
                {
                    name += " năm " + Year;
                }
                return name + "</a>";
            }
        }
        public string StatusName
        {
            get
            {
                if (Status == Constants.PLAN_DEACTIVED)
                {
                    return "<span class=\"label label-danger\">Chưa lập báo cáo</span>";
                }
                else if (Status == Constants.PLAN_NEW)
                {
                    return "<span class=\"label label-warning\">Chưa gửi báo cáo</span>";
                }
                else
                {
                    return "<span class=\"label label-success\">Đã gửi báo cáo</span>";
                }
            }
        }
    }
}