﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Common.CommonBO
{
    public class ObjectRoleBO
    {
        public int ObjectID { get; set; }
        public int Role { get; set; }
        public int? PL { get; set; }
    }
}