﻿using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Viettel.Business.BusinessUtil
{
    public class Mail
    {
        #region Send email
        public static string MAIL_ADDRESS = ConfigurationManager.AppSettings["Mail:Address"];
        public static string MAIL_NAME = ConfigurationManager.AppSettings["Mail:Name"];
        public static string MAIL_PASSWORD = ConfigurationManager.AppSettings["Mail:Password"];
        public static string MAIL_HOST = ConfigurationManager.AppSettings["Mail:Host"];
        public static string MAIL_PORT = ConfigurationManager.AppSettings["Mail:Port"];
        public static string WEB_URL = ConfigurationManager.AppSettings["WEB_URL"];
        #endregion
        public static void SendMail(string mailTo, string subject, string content)
        {
            var fromAddress = new MailAddress(MAIL_ADDRESS, MAIL_NAME);
            var toAddress = new MailAddress(mailTo);

            var smtp = new SmtpClient
            {
                Host = MAIL_HOST,
                Port = int.Parse(MAIL_PORT),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, MAIL_PASSWORD)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = content,
                BodyEncoding = System.Text.Encoding.UTF8,
                SubjectEncoding = System.Text.Encoding.UTF8,
                HeadersEncoding = System.Text.Encoding.UTF8,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }
        }
    }
}
