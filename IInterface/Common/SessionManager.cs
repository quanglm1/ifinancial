﻿
using IInterface.Common.CommonBO;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace IInterface.Common
{
    public class SessionManager
    {
        public const string USER_INFO = "USER_INFO";
        public const string LIST_ACTION = "LIST_ACTION";
        public const string PAGE_INDEX = "PAGE_INDEX";
        public static void SetValue(string Key, object Value)
        {
            HttpContext context = HttpContext.Current;
            context.Session[Key] = Value;
        }
        public static object GetValue(string Key)
        {
            HttpContext context = HttpContext.Current;
            return context.Session[Key];
        }
        public static void Remove(string Key)
        {
            HttpContext context = HttpContext.Current;
            context.Session.Remove(Key);
        }
        public static void Clear()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Session != null)
            {
                context.Session.RemoveAll();
            }
        }
        public static bool HasValue(string Key)
        {
            HttpContext context = HttpContext.Current;
            return context.Session[Key] != null;
        }
        public static UserInfo GetUserInfo()
        {
            HttpContext context = HttpContext.Current;
            if (context.Session[USER_INFO] == null)
            {
                context.Session[USER_INFO] = context.Cache[context.Session.SessionID];
                if (context.Session[USER_INFO] == null
                    && context.User.Identity.IsAuthenticated && !string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    SessionManager.TransferSession(context.User.Identity.Name);
                }
            }
            return context.Session[USER_INFO] as UserInfo;
        }
        public static void SetUserInfo(UserInfo userInfo)
        {
            HttpContext context = HttpContext.Current;
            if (context.Session[USER_INFO] == null)
            {
                context.Session[USER_INFO] = userInfo;
            }
        }
        public static List<AuthorizeBO> GetListPermistion()
        {
            HttpContext context = HttpContext.Current;
            return context.Session[LIST_ACTION] as List<AuthorizeBO>;
        }

        public static bool TransferSession(string userName)
        {
            User obj = null;
            using (Entities context = new Entities())
            {
                obj = context.User.FirstOrDefault(a => a.UserName.Equals(userName));
                if (obj != null)
                {
                    var objUser = new UserInfo
                    {
                        UserID = obj.UserID,
                        UserName = obj.UserName,
                        Avatar = obj.Avatar,
                        FullName = obj.FullName,
                        DepartmentID = obj.DepartmentID
                    };
                    var role = context.UserRole.Where(o => o.UserID == obj.UserID).Select(o => o.RoleID);
                    if (role.Any())
                    {
                        objUser.Role = role.ToList();
                    }
                    var dept = context.Department.Find(obj.DepartmentID);
                    objUser.DepartmentName = dept != null ? dept.DepartmentName : "";
                    if (dept != null)
                    {
                        if (dept.ParentID == null || dept.DepartmentType == Constants.DEPT_TYPE_PBQUY)
                        {
                            objUser.DeptType = Constants.MAIN_TYPE;
                        }
                        else
                        {
                            objUser.DeptType = Constants.SUB_TYPE;
                        }
                    }
                    objUser.ListMenu = GetListMenu(objUser.Role, context);
                    objUser.IsHeadOffice = objUser.IsHeadOffice;
                    SetUserInfo(objUser);
                    //Set lai gia tri dang nhap
                    obj.LastLoginDate = DateTime.Now;
                    context.SaveChanges();
                }
            }
            return obj != null;
        }

        public static List<MenuModel> GetListMenu(List<int> listMenuRole, Entities context)
        {
            if (listMenuRole == null)
            {
                return null;
            }

            var lstMenuforRole = context.MenuRole.Where(o => listMenuRole.Contains(o.RoleID)).Select(o => o.MenuID);
            List<MenuModel> listMenu = context.Menu.Where(x => x.Status == Constants.IS_ACTIVE && x.ParentID == null && lstMenuforRole.Contains(x.MenuID))
                .Select(x => new MenuModel
                {
                    MenuID = x.MenuID,
                    MenuName = x.MenuName,
                    OrderNumber = x.OrderNumber,
                    IconClass = x.IconClass,
                    IsSelected = listMenuRole.Contains(x.MenuID)
                }).ToList();
            // Lay danh sach menu con
            List<int> listMenuId = listMenu.Select(x => x.MenuID).ToList();

            List<Menu> listChildMenu = context.Menu.Where(x => x.Status == Constants.IS_ACTIVE && listMenuId.Contains(x.ParentID.Value) && lstMenuforRole.Contains(x.MenuID)).ToList();

            listMenu.ForEach(x =>
            {
                x.ListChildMenu = listChildMenu.Where(c => c.ParentID.Value == x.MenuID)
                .Select(c => new MenuModel
                {
                    MenuID = c.MenuID,
                    MenuName = c.MenuName,
                    OrderNumber = c.OrderNumber,
                    IsSelected = listMenuRole.Contains(c.MenuID),
                    ActionPath = c.ActionPath,
                    IconClass = c.IconClass
                }).OrderBy(o => o.MenuName).ToList();
            });

            return listMenu;
        }
    }
}