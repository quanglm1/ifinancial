﻿using Microsoft.Extensions.Caching.Memory;

namespace IInterface.Common
{
    public class CacheManagement
    {
        public static IMemoryCache MemoryCache = new MemoryCache(new MemoryCacheOptions());
    }
}