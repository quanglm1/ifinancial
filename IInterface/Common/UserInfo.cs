﻿using IInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Common
{
    public class UserInfo
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public List<int> Role { get; set; }
        public int? MemberID { get; set; }
        public int? DepartmentID { get; set; }
        public int DeptType { get; set; }
        public byte[] Avatar { get; set; }
        public string DepartmentName { get; set; }
        public List<MenuModel> ListMenu { get; set; }
        public int IsHeadOffice { get; set; }
    }
}