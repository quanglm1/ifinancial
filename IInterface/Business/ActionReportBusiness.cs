﻿using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Business
{
    public class ActionReportBusiness
    {
        private Entities context;
        public ActionReportBusiness(Entities context)
        {
            this.context = context;
        }
        public List<ActionReportIndexModel> GetActionReportTargetIndex(int actionReportID, int planTargetID = 0)
        {
            return (from ai in this.context.ActionReportIndex
                    join pi in this.context.PlanTargetIndex on ai.PlanTargetIndexID equals pi.PlanTargetIndexID
                    join pt in this.context.PlanTarget on pi.PlanTargetID equals pt.PlanTargetID
                    where ai.ActionReportID == actionReportID && (planTargetID == 0 || pi.PlanTargetID == planTargetID)
                    orderby pt.TargetOrder, pi.MainIndex.OrderNumber
                    select new ActionReportIndexModel
                    {
                        PlanTargetID = pt.PlanTargetID,
                        PlanTargetIndexID = ai.PlanTargetIndexID,
                        ValueIndex = ai.IndexValue,
                        ContentIndex = pi.ContentIndex,
                        PlanValueIndex = pi.ValueIndex,
                        TypeIndex = pi.TypeIndex
                    }).ToList();
        }

        public List<ActionReportActionIndexModel> GetActionReportActionIndex(int actionReportID, int planTargetID = 0)
        {
            return (from ai in this.context.ActionReportActionIndex
                    join pi in this.context.PlanActionIndex on ai.PlanActionIdexID equals pi.PlanActionIndexID
                    join pt in this.context.PlanTargetAction on pi.PlanTargetActionID equals pt.PlanTargetActionID
                    where ai.ActionReportID == actionReportID && (planTargetID == 0 || pt.PlanTargetID == planTargetID)
                    orderby pi.PlanActionIndexID
                    select new ActionReportActionIndexModel
                    {
                        PlanTargetID = pt.PlanTargetID.Value,
                        ActionReportActionIndexID = ai.ActionReportActionIndexID,
                        ActionReportID = ai.ActionReportID,
                        IndexName = pi.IndexName,
                        IndexValue = ai.IndexValue,
                        PlanActionIdexID = pi.PlanActionIndexID,
                        PlanTargetActionID = pi.PlanTargetActionID,
                        UnitName = pi.UnitName,
                        FullIndexName = pi.UnitName == null ? pi.IndexValue + " " + pi.IndexName : pi.IndexValue + " " + pi.UnitName + " " + pi.IndexName
                    }).ToList();
        }

        public List<ActionReportDetailModel> GetListAction(int planTargetActionID, int planTargetID, long actionReportID)
        {
            ActionReport entityActionReport = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entityActionReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            PlanTarget entityPlanTarget = this.context.PlanTarget.Where(x => x.PlanID == entityActionReport.PlanID && x.PlanTargetID == planTargetID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entityPlanTarget == null)
            {
                throw new BusinessException("Không tồn tại hoạt động, vui lòng kiểm tra lại");
            }
            List<ActionReportDetailModel> listAction = (from pt in this.context.PlanTarget
                                                        join pta in this.context.PlanTargetAction.Where(x => planTargetActionID == 0 || x.PlanTargetActionID == planTargetActionID)
                                                        on pt.PlanTargetID equals pta.PlanTargetID
                                                        where pt.PlanTargetID == entityPlanTarget.PlanTargetID
                                                        orderby pta.ActionOrder
                                                        select new ActionReportDetailModel
                                                        {
                                                            PlanTargetActionID = pta.PlanTargetActionID,
                                                            PlanTargetActionName = pta.ActionContent,
                                                            PlanTargetActionCode = pta.ActionCode,
                                                            PlanTargetID = pt.PlanTargetID,
                                                            TargetName = pt.PlanTargetContent,
                                                            ContentPropaganda = pta.ContentPropaganda,
                                                            ObjectScope = pta.ObjectScope,
                                                            ActionMethod = pta.ActionMethod,
                                                            PlanResult = pta.Result
                                                        }).ToList();
            List<int> listPlanTargetActionID = listAction.Where(x => x.PlanTargetActionID != null).Select(x => x.PlanTargetActionID.Value).ToList();
            PlanHistoryBusiness hisBus = new PlanHistoryBusiness(context);
            PlanBusiness planBus = new PlanBusiness(context);
            Dictionary<int, string> dicPlanTargetActionTime = hisBus.ActionTimes(listPlanTargetActionID, entityActionReport.PlanID);
            // Lay du lieu da nhap
            List<ActionReportDetail> listDetail = (from a in this.context.ActionReportDetail
                                                   join t in this.context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                                                   where a.ActionReportID == actionReportID && t.PlanTargetID == planTargetID
                                                   select a).ToList();
            // Lay subaction muc con
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlan.Where(x => this.context.PlanTargetAction.Any(p => p.PlanTargetID == planTargetID
            && p.PlanTargetActionID == x.PlanTargetActionID && (planTargetActionID == 0 || p.PlanTargetActionID == planTargetActionID)))
            .Select(x => new SubActionPlanModel
            {
                PlanTargetID = planTargetID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                SubActionOrder = x.SubActionOrder,
                ParentID = x.ParentID,
                TotalBudget = x.TotalBudget,
                ContentPropaganda = x.ContentPropaganda,
                SubActionMethod = x.SubActionMethod,
                SubActionObject = x.SubActionObject,
                Result = x.Result
            }).OrderBy(x => x.SubActionPlanID).ToList();

            List<int> listSubActionID = listSubAction.Where(x => x.SubActionPlanID != null).Select(x => x.SubActionPlanID.Value).ToList();
            Dictionary<int, string> dicSubActionTime = hisBus.ActionTimes(listSubActionID, entityActionReport.PlanID);
            foreach (SubActionPlanModel subAction in listSubAction)
            {
                if (subAction.SubActionPlanID != null && dicSubActionTime.ContainsKey(subAction.SubActionPlanID.Value))
                {
                    string time = dicSubActionTime[subAction.SubActionPlanID.Value];
                    if (!string.IsNullOrWhiteSpace(time) && !time.ToLower().StartsWith("chưa"))
                    {
                        subAction.ActionTime = time;
                    }
                }
            }

            foreach (ActionReportDetailModel action in listAction)
            {
                if (action.PlanTargetActionID != null && dicPlanTargetActionTime.ContainsKey(action.PlanTargetActionID.Value))
                {
                    string time = dicPlanTargetActionTime[action.PlanTargetActionID.Value];
                    if (!string.IsNullOrWhiteSpace(time) && !time.ToLower().StartsWith("chưa"))
                    {
                        action.ActionTime = time;
                    }
                }
                ActionReportDetail detail = listDetail.Where(x => x.PlanTargetActionID == action.PlanTargetActionID
                && x.SubPlanActionID == null).FirstOrDefault();
                if (detail != null)
                {
                    action.Result = detail.Result;
                    action.Description = detail.Description;
                    action.PlanContent = detail.PlanContent;
                }
                else
                {
                    action.PlanContent = action.GenPlanContent();
                }
                action.ListSubAction = listSubAction.Where(x => x.PlanTargetActionID == action.PlanTargetActionID && x.ParentID == null).OrderBy(x => x.SubActionOrder).ToList();
                if (action.ListSubAction != null && action.ListSubAction.Any())
                {
                    foreach (SubActionPlanModel sub in action.ListSubAction)
                    {
                        this.AddSubAction(sub, listSubAction, listDetail);
                    }
                }
            }
            ActionReportDetail targetDetail = listDetail.Where(x => x.PlanTargetID == entityPlanTarget.PlanTargetID
                                               && x.PlanTargetActionID == null
                                               && x.SubPlanActionID == null).FirstOrDefault();
            ActionReportDetailModel target = new ActionReportDetailModel();
            target.IsTarget = true;
            target.PlanTargetID = entityPlanTarget.PlanTargetID;
            target.TargetCode = entityPlanTarget.TargetCode;
            target.TargetName = entityPlanTarget.PlanTargetContent;
            if (targetDetail != null)
            {
                target.Description = targetDetail.Description;
                target.Result = targetDetail.Result;
                target.PlanContent = targetDetail.PlanContent;
            }
            listAction.Insert(0, target);
            return listAction;
        }

        public void AddSubAction(SubActionPlanModel subAction, List<SubActionPlanModel> listAllSubAction,
            List<ActionReportDetail> listDetail)
        {
            ActionReportDetail detail = listDetail.Where(x => x.PlanTargetActionID == subAction.PlanTargetActionID
                                               && x.SubPlanActionID == subAction.SubActionPlanID).FirstOrDefault();
            if (detail != null)
            {
                subAction.ActionReportResult = detail.Result;
                subAction.ActionReportDescription = detail.Description;
                subAction.ActionReportPlanContent = detail.PlanContent;
            }
            else
            {
                subAction.ActionReportPlanContent = subAction.GenPlanContent();
            }
            List<SubActionPlanModel> listChild = listAllSubAction.Where(x => x.ParentID == subAction.SubActionPlanID).OrderBy(x => x.SubActionOrder).ToList();
            if (listAllSubAction != null && listChild.Any())
            {
                subAction.ListChildSubActionPlan = listChild;
                foreach (SubActionPlanModel child in listChild)
                {
                    this.AddSubAction(child, listAllSubAction, listDetail);
                }
            }
        }

        public void Save(ActionReportData data)
        {
            // Target
            if (data.ArrPlanTargetID != null && data.ArrPlanTargetID.Length > 0)
            {
                string sPlanTargetID = string.Join(",", data.ArrPlanTargetID);
                this.context.Database.ExecuteSqlCommand("delete from ActionReportDetail where ActionReportID = " + data.ActionReportID
                    + " and PlanTargetID in (" + sPlanTargetID + ")");

                var listPlanTarget = (from pt in this.context.PlanTarget
                                      where data.ArrPlanTargetID.Contains(pt.PlanTargetID)
                                      select new
                                      {
                                          pt.PlanTargetID,
                                          pt.PlanTargetContent
                                      }).ToList();
                int orderNumer = 0;
                foreach (int planTargetID in data.ArrPlanTargetID)
                {
                    var pt = listPlanTarget.Where(x => x.PlanTargetID == planTargetID).FirstOrDefault();
                    if (pt == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    ActionReportDetail detail = new ActionReportDetail();
                    detail.ActionReportID = data.ActionReportID;
                    detail.PlanTargetID = planTargetID;
                    detail.Description = string.IsNullOrWhiteSpace(data.ArrPlanTargetDesc[orderNumer]) ? null : data.ArrPlanTargetDesc[orderNumer].Trim();
                    detail.Result = string.IsNullOrWhiteSpace(data.ArrPlanTargetResult[orderNumer]) ? null : data.ArrPlanTargetResult[orderNumer].Trim();
                    detail.PlanContent = string.IsNullOrWhiteSpace(data.ArrPlanTargetPlanContent[orderNumer]) ? null : data.ArrPlanTargetPlanContent[orderNumer].Trim();
                    this.context.ActionReportDetail.Add(detail);
                    orderNumer++;
                }
            }
            // PlanTargetAction
            if (data.ArrPlanTargetActionID != null && data.ArrPlanTargetActionID.Length > 0)
            {
                var listPlanTargetAction = (from pta in this.context.PlanTargetAction
                                            join pt in this.context.PlanTarget on pta.PlanTargetID equals pt.PlanTargetID
                                            where data.ArrPlanTargetActionID.Contains(pta.PlanTargetActionID)
                                            select new
                                            {
                                                pt.PlanTargetID,
                                                pta.PlanTargetActionID,
                                                pta.ActionCode,
                                                pta.ActionContent
                                            }).ToList();
                int orderNumer = 0;
                foreach (int planTargetActionID in data.ArrPlanTargetActionID)
                {
                    var pt = listPlanTargetAction.Where(x => x.PlanTargetActionID == planTargetActionID).FirstOrDefault();
                    if (pt == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    ActionReportDetail detail = new ActionReportDetail();
                    detail.ActionReportID = data.ActionReportID;
                    detail.PlanTargetID = pt.PlanTargetID;
                    detail.PlanTargetActionID = pt.PlanTargetActionID;
                    detail.Description = string.IsNullOrWhiteSpace(data.ArrPlanTargetActionDes[orderNumer]) ? null : data.ArrPlanTargetActionDes[orderNumer].Trim();
                    detail.Result = string.IsNullOrWhiteSpace(data.ArrPlanTargetActionResult[orderNumer]) ? null : data.ArrPlanTargetActionResult[orderNumer].Trim();
                    detail.PlanContent = string.IsNullOrWhiteSpace(data.ArrPlanTargetActionPlanContent[orderNumer]) ? null : data.ArrPlanTargetActionPlanContent[orderNumer].Trim();
                    this.context.ActionReportDetail.Add(detail);
                    orderNumer++;
                }
            }
            // SubPlanAction
            if (data.ArrSubActionPlanID != null && data.ArrSubActionPlanID.Length > 0)
            {
                var listSubActionPlan = (from s in this.context.SubActionPlan
                                         join pta in this.context.PlanTargetAction on s.PlanTargetActionID equals pta.PlanTargetActionID
                                         join pt in this.context.PlanTarget on pta.PlanTargetID equals pt.PlanTargetID
                                         where data.ArrSubActionPlanID.Contains(s.SubActionPlanID)
                                         select new
                                         {
                                             pt.PlanTargetID,
                                             pta.PlanTargetActionID,
                                             s.SubActionPlanID,
                                             s.SubActionCode,
                                             s.SubActionContent
                                         }).ToList();
                int orderNumer = 0;
                foreach (int subActionPlanID in data.ArrSubActionPlanID)
                {
                    var sp = listSubActionPlan.Where(x => x.SubActionPlanID == subActionPlanID).FirstOrDefault();
                    if (sp == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    ActionReportDetail detail = new ActionReportDetail();
                    detail.ActionReportID = data.ActionReportID;
                    detail.PlanTargetID = sp.PlanTargetID;
                    detail.PlanTargetActionID = sp.PlanTargetActionID;
                    detail.SubPlanActionID = sp.SubActionPlanID;
                    detail.Description = string.IsNullOrWhiteSpace(data.ArrSubActionPlanDes[orderNumer]) ? null : data.ArrSubActionPlanDes[orderNumer].Trim();
                    detail.Result = string.IsNullOrWhiteSpace(data.ArrSubActionPlanResult[orderNumer]) ? null : data.ArrSubActionPlanResult[orderNumer].Trim();
                    detail.PlanContent = string.IsNullOrWhiteSpace(data.ArrSubActionPlanPlanContent[orderNumer]) ? null : data.ArrSubActionPlanPlanContent[orderNumer].Trim();
                    this.context.ActionReportDetail.Add(detail);
                    orderNumer++;
                }
            }
            this.context.SaveChanges();
        }

        public void SaveReportTargetIndex(List<IndexMappingBO> TargetIndexIDs, int PlanTargetID, int ActionReportID, int Page)
        {
            List<TargetIndexModel> ChoosingTargetIndex = new List<TargetIndexModel>();
            UserInfo objUser = SessionManager.GetUserInfo();
            if (PlanTargetID == 0 || ActionReportID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm tiêu chí");
            }
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == ActionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget planTarget = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (planTarget == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            //get main list
            var xQuery = from pi in this.context.PlanTargetIndex
                         join mi in this.context.MainIndex on pi.MainIndexID equals mi.MainIndexID into ii
                         from gi in ii.DefaultIfEmpty()
                         where pi.PlanID == entity.PlanID && pi.PlanTargetID == PlanTargetID
                         orderby gi.OrderNumber
                         select pi.PlanTargetIndexID;
            List<int> listPlanTargetIndexID = xQuery.Skip((Page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            List<ActionReportIndex> listReportIndex = this.context.ActionReportIndex.Where(x => x.ActionReportID == ActionReportID
                && listPlanTargetIndexID.Contains(x.PlanTargetIndexID)).ToList();
            if (TargetIndexIDs != null && TargetIndexIDs.Any())
            {
                List<PlanTargetIndex> listPlanTargetIndex = this.context.PlanTargetIndex.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == PlanTargetID).ToList();
                TargetIndexIDs.ForEach(x =>
                {
                    if (listPlanTargetIndex.Any(i => i.PlanTargetIndexID == x.MainIndexID))
                    {
                        ActionReportIndex item = listReportIndex.Where(ri => ri.PlanTargetIndexID == x.MainIndexID).FirstOrDefault();
                        if (item == null)
                        {
                            item = new ActionReportIndex();
                            item.ActionReportID = ActionReportID;
                            item.IndexValue = x.ValueIndex;
                            item.PlanTargetIndexID = x.MainIndexID;
                            this.context.ActionReportIndex.Add(item);
                        }
                        else
                        {
                            item.IndexValue = x.ValueIndex;
                        }
                    }
                });
                listReportIndex.RemoveAll(i => TargetIndexIDs.Any(x => x.MainIndexID == i.PlanTargetIndexID));
                this.context.ActionReportIndex.RemoveRange(listReportIndex);
            }
            else
            {
                this.context.ActionReportIndex.RemoveRange(listReportIndex);
            }
            this.context.SaveChanges();
        }

        public void SaveReportActionIndex(List<IndexMappingBO> ActionIndexIDs, int PlanTargetID, int ActionReportID, int Page)
        {
            List<ActionIndexModel> ChoosingActionIndex = new List<ActionIndexModel>();
            UserInfo objUser = SessionManager.GetUserInfo();
            if (PlanTargetID == 0 || ActionReportID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm chỉ số");
            }
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == ActionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget planTarget = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (planTarget == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            //get main list
            var xQuery = (from pi in this.context.PlanActionIndex
                          join pt in this.context.PlanTarget on pi.PlanTargetID equals pt.PlanTargetID
                          where pt.PlanID == entity.PlanID && pi.PlanTargetID == PlanTargetID
                          orderby pi.PlanActionIndexID
                          select pi.PlanActionIndexID);
            List<int> listPlanActionIndexID = xQuery.Skip((Page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            List<ActionReportActionIndex> listReportIndex = this.context.ActionReportActionIndex.Where(x => x.ActionReportID == ActionReportID
                && listPlanActionIndexID.Contains(x.PlanActionIdexID)).ToList();

            if (ActionIndexIDs != null && ActionIndexIDs.Any())
            {
                List<PlanActionIndex> listActionIndex = this.context.PlanActionIndex.Where(x => x.PlanTargetID == PlanTargetID).ToList();
                ActionIndexIDs.ForEach(x =>
                {
                    if (listActionIndex.Any(i => i.PlanActionIndexID == x.MainIndexID))
                    {
                        ActionReportActionIndex item = listReportIndex.Where(ri => ri.PlanActionIdexID == x.MainIndexID).FirstOrDefault();
                        if (item == null)
                        {
                            item = new ActionReportActionIndex();
                            item.ActionReportID = ActionReportID;
                            item.IndexValue = x.ValueIndex.GetValueOrDefault();
                            item.PlanActionIdexID = x.MainIndexID;
                            this.context.ActionReportActionIndex.Add(item);
                        }
                        else
                        {
                            item.IndexValue = x.ValueIndex.GetValueOrDefault();
                        }
                    }
                });
                listReportIndex.RemoveAll(i => ActionIndexIDs.Any(x => x.MainIndexID == i.PlanActionIdexID));
                this.context.ActionReportActionIndex.RemoveRange(listReportIndex);
            }
            else
            {
                this.context.ActionReportActionIndex.RemoveRange(listReportIndex);
            }
            this.context.SaveChanges();
        }

        public void RemoveReportActionIndex(int PlanTargetID, int ActionReportID, long ActionReportActionIndexID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            if (PlanTargetID == 0 || ActionReportID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm chỉ số");
            }
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == ActionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            ActionReportActionIndex item = this.context.ActionReportActionIndex.Where(x => x.ActionReportID == ActionReportID
                && this.context.PlanActionIndex.Any(pi => pi.PlanActionIndexID == x.PlanActionIdexID
                && pi.PlanTargetID == PlanTargetID) && x.ActionReportActionIndexID == ActionReportActionIndexID).FirstOrDefault();
            if (item == null)
            {
                throw new BusinessException("Không tồn tại chỉ số hoạt động");
            }
            this.context.ActionReportActionIndex.Remove(item);
            this.context.SaveChanges();
        }

        public void RemoveReportTargetIndex(int PlanTargetID, int ActionReportID, long PlanTargetIndexID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            if (PlanTargetID == 0 || ActionReportID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm chỉ số");
            }
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == ActionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            ActionReportIndex item = this.context.ActionReportIndex.Where(x => x.ActionReportID == ActionReportID
                && this.context.PlanTargetIndex.Any(pi => pi.PlanTargetIndexID == x.PlanTargetIndexID
                && pi.PlanTargetID == PlanTargetID) && x.PlanTargetIndexID == PlanTargetIndexID).FirstOrDefault();
            if (item == null)
            {
                throw new BusinessException("Không tồn tại chỉ số mục tiêu");
            }
            this.context.ActionReportIndex.Remove(item);
            this.context.SaveChanges();
        }


        public Paginate<ActionReportIndexModel> GetReportIndex(int actionReportID, int planTargetID, int page)
        {
            List<ActionReportIndexModel> listActionReportIndex = this.GetActionReportTargetIndex(actionReportID, planTargetID);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            IQueryable<ActionReportIndexModel> qIndex = (from pi in this.context.PlanTargetIndex
                                                         join mi in this.context.MainIndex on pi.MainIndexID equals mi.MainIndexID into ii
                                                         from gi in ii.DefaultIfEmpty()
                                                         where pi.PlanID == entity.PlanID && pi.PlanTargetID == planTargetID
                                                         orderby gi.OrderNumber, pi.PlanTargetIndexID
                                                         select new ActionReportIndexModel
                                                         {
                                                             PlanTargetIndexID = pi.PlanTargetIndexID,
                                                             ContentIndex = pi.ContentIndex,
                                                             TypeIndex = pi.TypeIndex,
                                                             PlanValueIndex = pi.ValueIndex
                                                         });
            Paginate<ActionReportIndexModel> paginate = new Paginate<ActionReportIndexModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.Count = qIndex.Count();
            List<ActionReportIndexModel> listTargetIndex = qIndex.Skip((page - 1) * paginate.PageSize).Take(paginate.PageSize).ToList();
            listTargetIndex.ForEach(x =>
            {
                string middle = string.Empty;
                if (x.TypeIndex.GetValueOrDefault() != Constants.TYPE_INDEX_CHOICE)
                {
                    if (x.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_INPUT_AVERAGE)
                    {
                        middle = "% ";
                        if (x.ContentIndex.StartsWith("%"))
                        {
                            x.ContentIndex = x.ContentIndex.Substring(1);
                        }
                    }
                    else
                    {
                        middle = " ";
                    }
                }
                x.ContentIndex = x.PlanValueIndex.GetValueOrDefault() + middle + x.ContentIndex;
                ActionReportIndexModel reportIndex = listActionReportIndex.Where(ix => ix.PlanTargetIndexID == x.PlanTargetIndexID).FirstOrDefault();
                if (reportIndex != null)
                {
                    x.ValueIndex = reportIndex.ValueIndex;
                    x.Choosen = true;
                }
            });
            paginate.List = listTargetIndex;
            return paginate;
        }

        public Paginate<ActionReportActionIndexModel> GetReportActionIndex(int actionReportID, int planTargetID, int page)
        {
            List<ActionReportActionIndexModel> listActionReportIndex = this.GetActionReportActionIndex(actionReportID, planTargetID);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            IQueryable<ActionReportActionIndexModel> qIndex = (from pi in this.context.PlanActionIndex
                                                               join pt in this.context.PlanTarget on pi.PlanTargetID equals pt.PlanTargetID
                                                               where pt.PlanID == entity.PlanID && pi.PlanTargetID == planTargetID
                                                               orderby pi.PlanActionIndexID
                                                               select new ActionReportActionIndexModel
                                                               {
                                                                   PlanTargetID = pt.PlanTargetID,
                                                                   IndexName = pi.IndexName,
                                                                   PlanActionIdexID = pi.PlanActionIndexID,
                                                                   PlanTargetActionID = pi.PlanTargetActionID,
                                                                   SubActionPlanID = pi.SubActionPlanID,
                                                                   UnitName = pi.UnitName,
                                                                   IndexValue = pi.IndexValue
                                                               });
            Paginate<ActionReportActionIndexModel> paginate = new Paginate<ActionReportActionIndexModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.Count = qIndex.Count();
            List<ActionReportActionIndexModel> listActionIndex = qIndex.Skip((page - 1) * paginate.PageSize).Take(paginate.PageSize).ToList();
            listActionIndex.ForEach(x =>
            {
                string FullIndexName = x.UnitName == null ? x.IndexValue + " " + x.IndexName : x.IndexValue + " " + x.UnitName + " " + x.IndexName;
                x.FullIndexName = FullIndexName;
                ActionReportActionIndexModel reportIndex = listActionReportIndex.Where(ix => ix.PlanActionIdexID == x.PlanActionIdexID).FirstOrDefault();
                if (reportIndex != null)
                {
                    x.IndexValue = reportIndex.IndexValue;
                    x.Choosen = true;
                }
            });
            paginate.List = listActionIndex;
            return paginate;
        }

        public Paginate<ActionReportInfoModel> GetListActionReport(int userID, int periodID, int deptID, int Status, int quarter, int year, int page)
        {
            List<int> listYear = new List<int>();
            List<ActionReportInfoModel> listReport =
                (from s in this.context.ActionReport
                 join pl in this.context.Plan on s.PlanID equals pl.PlanID
                 join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                 join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                 where (periodID == 0 || pr.PeriodID == periodID) && (deptID == 0 || d.DepartmentID == deptID) && d.Status != Constants.IS_NOT_ACTIVE
                 && pl.Status != Constants.IS_NOT_ACTIVE && pr.Status != Constants.IS_NOT_ACTIVE && s.Status != Constants.IS_NOT_ACTIVE
                 && (year == 0 || s.Year == year) && (quarter == 0 || s.Quarter == quarter)
                && (Status == 0 || (Status == Constants.REPORT_NEW)
                || (Status == Constants.REPORT_SENT && s.Status >= Constants.PLAN_NEXT_LEVER))
                 select new ActionReportInfoModel
                 {
                     CreateDate = s.CreateDate,
                     Creator = s.Creator,
                     PlanID = s.PlanID,
                     PeriodID = pl.PeriodID,
                     PlanName = pr.PeriodName,
                     Quarter = s.Quarter,
                     Year = s.Year,
                     ActionReportID = s.ActionReportID,
                     Type = s.Type,
                     Status = s.Status,
                     DeptID = d.DepartmentID,
                     DeptName = d.DepartmentName,
                     SendDate = s.SendDate,
                     IsHasReview = this.context.ReportReview.Any(x => x.ReportID == s.ActionReportID && x.ReportType == Constants.REPORT_TYPE_ACTION)
                 }).ToList();

            Period period = context.Period.Find(periodID);
            if (year == 0)
            {
                for (int i = period.FromDate.Year; i <= period.ToDate.Year; i++)
                {
                    listYear.Add(i);
                }
            }
            else
            {
                listYear.Add(year);
            }

            List<int> listQuarter = new List<int> { 0, 1, 2, 3, 4 };
            if (quarter > 0)
            {
                listQuarter.Clear();
                listQuarter.Add(quarter);
            }
            DateTime now = DateTime.Now;
            int quarterNow = (now.Month % 3 == 0) ? now.Month / 3 : (now.Month / 3) + 1;

            List<Department> listDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE && (deptID == 0 || x.DepartmentID == deptID)).ToList();
            Dictionary<int, ObjectRoleBO> dicRoleDept = new FlowProcessBusiness(this.context).GetRoleForDept(userID, listDept, Constants.COMMENT_TYPE_ACTION);
            listDept.RemoveAll(x => !dicRoleDept.ContainsKey(x.DepartmentID) || dicRoleDept[x.DepartmentID].Role == Constants.ROLE_NO_VIEW);
            listReport = (from q in listQuarter
                          join y in listYear on 0 equals 0
                          join d in listDept on 0 equals 0
                          join l in listReport on new { DeptID = d.DepartmentID, Quarter = q, Year = y } equals new { DeptID = l.DeptID, Quarter = l.Quarter.GetValueOrDefault(), Year = l.Year } into lj
                          from s in lj.DefaultIfEmpty()
                          where (Status == 0 || (Status == Constants.REPORT_NEW && (s == null || s.Status == Constants.PLAN_NEW))
                || (Status == Constants.REPORT_SENT && s != null && s.Status >= Constants.PLAN_NEXT_LEVER))
                && (y < now.Year || (y == now.Year && q <= quarterNow))
                          select new ActionReportInfoModel
                          {
                              CreateDate = s == null ? null : s.CreateDate,
                              PeriodID = periodID,
                              PlanID = s == null ? 0 : s.PlanID,
                              PlanName = s == null ? period.PeriodName : s.PlanName,
                              Quarter = q == 0 ? null : (int?)q,
                              Year = y,
                              ActionReportID = s == null ? 0 : s.ActionReportID,
                              Type = s == null ? 0 : s.Type,
                              Status = s == null ? 0 : s.Status,
                              DeptID = s == null ? d.DepartmentID : s.DeptID,
                              DeptName = s == null ? d.DepartmentName : s.DeptName,
                              SendDate = s == null ? null : s.SendDate,
                              IsHasReview = s == null ? false : s.IsHasReview
                          }).ToList();
            // Lay quyen
            List<int> listReportID = listReport.Where(x => x.ActionReportID > 0).Select(x => x.ActionReportID).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.context).GetRoleForObject(userID, listReportID, Constants.COMMENT_TYPE_ACTION);

            listReport.RemoveAll(x => x.ActionReportID > 0 && dicRole[x.ActionReportID].Role == Constants.ROLE_NO_VIEW);
            listReport.ForEach(x =>
            {
                if (x.ActionReportID > 0)
                {
                    x.Role = dicRole[x.ActionReportID].Role;
                }
                else
                {
                    x.Role = dicRoleDept[x.DeptID].Role;
                }
            });
            Paginate<ActionReportInfoModel> paginate = new Paginate<ActionReportInfoModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = listReport.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter == null || x.Quarter == 0 ? 100 : x.Quarter.Value)
                .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = listReport.Count;
            return paginate;
        }
    }
}