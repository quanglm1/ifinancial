﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Business
{
    public class ReportReviewBusiness
    {
        private Entities context;
        public ReportReviewBusiness(Entities context)
        {
            this.context = context;
        }

        private void ValidateRole(int synID, int objectType, int minRole, bool includeAfterAprove = false)
        {
            if (objectType < 0)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var user = SessionManager.GetUserInfo();
            var busRole = new FlowProcessBusiness(this.context);
            var role = busRole.GetRoleForObject(user.UserID, synID, objectType);
            if (role == null)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            if (includeAfterAprove && role.Role == Constants.ROLE_AFTER_APPROVE)
            {
                return;
            }
            if (role.Role < minRole)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
        }

        private int GetObjectType(int reportType)
        {
            if (reportType == Constants.REPORT_TYPE_ACTION)
            {
                return Constants.COMMENT_TYPE_ACTION;
            }
            else if (reportType == Constants.REPORT_TYPE_SETTLE)
            {
                return Constants.COMMENT_TYPE_SETTLE;
            }
            else if (reportType == Constants.REPORT_TYPE_FINANCIAL)
            {
                return Constants.COMMENT_TYPE_FINANCIAL;
            }
            return -1;
        }

        public void ActionApprove(int reportID, int reportType, int status)
        {
            int reportStatus;
            int acionType;
            int objectType = GetObjectType(reportType);
            ValidateRole(reportID, objectType, Constants.ROLE_APPROVE, true);
            if (status == Constants.IS_ACTIVE)
            {
                reportStatus = Constants.PLAN_APPROVED;
                acionType = Constants.REPORT_ACTION_TYPE_APPROVE;
            }
            else
            {
                reportStatus = Constants.PLAN_NEXT_LEVER;
                acionType = Constants.REPORT_ACTION_TYPE_UNAPPROVE;
            }
            if (reportType == Constants.REPORT_TYPE_ACTION)
            {
                ActionReport report = this.context.ActionReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.Status = reportStatus;
            }
            else if (reportType == Constants.REPORT_TYPE_SETTLE)
            {
                SettleSynthesis report = this.context.SettleSynthesis.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.Status = reportStatus;
            }
            else if (reportType == Constants.REPORT_TYPE_FINANCIAL)
            {
                FinancialReport report = this.context.FinancialReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.Status = reportStatus;
            }
            ReportReview r = new ReportReview
            {
                ActionType = acionType,
                ReportID = reportID,
                ReportType = reportType,
                ProcessDate = DateTime.Now,
                Status = Constants.IS_ACTIVE,
                UserID = SessionManager.GetUserInfo().UserID
            };
            this.context.ReportReview.Add(r);
            this.context.SaveChanges();
        }

        public void ActionAgree(int reportID, int reportType, int status)
        {
            ReportReview r = new ReportReview();
            UserInfo user = SessionManager.GetUserInfo();
            int objectType = GetObjectType(reportType);
            ValidateRole(reportID, objectType, Constants.ROLE_EDIT);
            if (status == Constants.IS_ACTIVE)
            {
                r.ActionType = Constants.REPORT_ACTION_TYPE_AGREE;
            }
            else
            {
                r.ActionType = Constants.REPORT_ACTION_TYPE_UNLOCK;
            }
            if (reportType == Constants.REPORT_TYPE_ACTION)
            {
                ActionReport report = this.context.ActionReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = status == Constants.IS_ACTIVE ? report.StepNow.GetValueOrDefault() + 1 : report.StepNow.GetValueOrDefault() - 1;
                if (report.StepNow <= 0)
                {
                    report.StepNow = null;
                }
                if (status == Constants.IS_ACTIVE)
                {
                    report.SendDate = DateTime.Now;
                }
                report.Status = Constants.PLAN_NEXT_LEVER;
                if (report.DepartmentID == user.DepartmentID)
                {
                    r.ActionType = Constants.REPORT_ACTION_TYPE_SEND;
                }
            }
            else if (reportType == Constants.REPORT_TYPE_SETTLE)
            {
                SettleSynthesis report = this.context.SettleSynthesis.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = status == Constants.IS_ACTIVE ? report.StepNow.GetValueOrDefault() + 1 : report.StepNow.GetValueOrDefault() - 1;
                if (report.StepNow <= 0)
                {
                    report.StepNow = null;
                }
                if (status == Constants.IS_ACTIVE)
                {
                    report.SendDate = DateTime.Now;
                }
                report.Status = Constants.PLAN_NEXT_LEVER;
                if (report.DepartmentID == user.DepartmentID)
                {
                    r.ActionType = Constants.REPORT_ACTION_TYPE_SEND;
                }
            }
            else if (reportType == Constants.REPORT_TYPE_FINANCIAL)
            {
                FinancialReport report = this.context.FinancialReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = status == Constants.IS_ACTIVE ? report.StepNow.GetValueOrDefault() + 1 : report.StepNow.GetValueOrDefault() - 1;
                if (report.StepNow <= 0)
                {
                    report.StepNow = null;
                }
                if (status == Constants.IS_ACTIVE)
                {
                    report.SendDate = DateTime.Now;
                }
                report.Status = Constants.PLAN_NEXT_LEVER;
                if (report.DepartmentID == user.DepartmentID)
                {
                    r.ActionType = Constants.REPORT_ACTION_TYPE_SEND;
                }
            }
            r.ReportID = reportID;
            r.ReportType = reportType;
            r.ProcessDate = DateTime.Now;
            r.Status = Constants.IS_ACTIVE;
            r.UserID = SessionManager.GetUserInfo().UserID;
            this.context.ReportReview.Add(r);
            this.context.SaveChanges();
        }

        public void SendReport(int reportID, int reportType)
        {
            int objectType = GetObjectType(reportType);
            ValidateRole(reportID, objectType, Constants.ROLE_EDIT);
            if (reportType == Constants.REPORT_TYPE_ACTION)
            {
                ActionReport report = this.context.ActionReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = report.StepNow.GetValueOrDefault() + 1;
                report.SendDate = DateTime.Now;
                report.Status = Constants.PLAN_NEXT_LEVER;
            }
            else if (reportType == Constants.REPORT_TYPE_SETTLE)
            {
                SettleSynthesis report = this.context.SettleSynthesis.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = report.StepNow.GetValueOrDefault() + 1;
                report.SendDate = DateTime.Now;
                report.Status = Constants.PLAN_NEXT_LEVER;
            }
            else if (reportType == Constants.REPORT_TYPE_FINANCIAL)
            {
                FinancialReport report = this.context.FinancialReport.Find(reportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                report.StepNow = report.StepNow.GetValueOrDefault() + 1;
                report.SendDate = DateTime.Now;
                report.Status = Constants.PLAN_NEXT_LEVER;
            }
            ReportReview r = new ReportReview
            {
                ReportID = reportID,
                ReportType = reportType,
                ProcessDate = DateTime.Now,
                Status = Constants.IS_ACTIVE,
                UserID = SessionManager.GetUserInfo().UserID,
                ActionType = Constants.REPORT_ACTION_TYPE_SEND
            };
            this.context.ReportReview.Add(r);
            this.context.SaveChanges();
        }

        public List<SelectListItem> GetListPeriod(int deptID = 0, bool isAll = true)
        {
            List<PeriodModel> listPlan;
            if (deptID > 0)
            {
                listPlan = (from pl in this.context.Plan
                            join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                            where pl.DepartmentID == deptID && pl.Status == Constants.PLAN_APPROVED
                            && pr.Status == Constants.IS_ACTIVE
                            orderby pr.FromDate descending
                            select new PeriodModel
                            {
                                PeriodID = pr.PeriodID,
                                PeriodName = pr.PeriodName,
                                FromDate = pr.FromDate,
                                ToDate = pr.ToDate
                            }).ToList();
            }
            else
            {
                listPlan = (from pr in this.context.Period
                            where pr.Status == Constants.IS_ACTIVE
                            orderby pr.FromDate descending
                            select new PeriodModel
                            {
                                PeriodID = pr.PeriodID,
                                PeriodName = pr.PeriodName,
                                FromDate = pr.FromDate,
                                ToDate = pr.ToDate
                            }).ToList();
            }
            List<SelectListItem> listPlanItem = new List<SelectListItem>();
            if (isAll)
            {
                listPlanItem.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            }
            DateTime now = DateTime.Now;
            listPlan.ForEach(x =>
            {
                SelectListItem selectedItem = new SelectListItem { Value = x.PeriodID.ToString(), Text = x.PeriodName };
                if (x.FromDate.Year <= now.Year && x.ToDate.Year >= now.Year)
                {
                    selectedItem.Selected = true;
                }
                listPlanItem.Add(selectedItem);
            });
            return listPlanItem;
        }

        public List<SelectListItem> GetListYearByPeriod(int periodID = 0, bool isAll = true, bool isNow = false)
        {
            var listPlan = (from pr in this.context.Period
                            where pr.Status == Constants.IS_ACTIVE
                            && (periodID == 0 || pr.PeriodID == periodID)
                            orderby pr.FromDate descending
                            select new
                            {
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listYear = new List<SelectListItem>();
            if (isAll)
            {
                listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            }
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i <= maxYear; i++)
                {
                    bool isSelected = false;
                    if (isNow)
                    {
                        isSelected = (i == DateTime.Now.Year);
                    }
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString(), Selected = isSelected });
                }
            }
            return listYear;
        }

        public List<ReportReviewModel> GetListReportReview(int reportID, int reportType)
        {
            List<ReportReviewModel> listReview = (from r in this.context.ReportReview
                                                  join u in this.context.User on r.UserID equals u.UserID
                                                  join d in this.context.Department on u.DepartmentID equals d.DepartmentID
                                                  where r.ReportID == reportID && r.ReportType == reportType && r.Status == Constants.IS_ACTIVE
                                                  select new ReportReviewModel
                                                  {
                                                      UserName = u.UserName,
                                                      FullName = u.FullName,
                                                      DeptName = d.DepartmentName,
                                                      ProcessDate = r.ProcessDate,
                                                      ActionType = r.ActionType
                                                  }).ToList();
            return listReview;
        }
    }
}