﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Business
{
    public class PlanCostBusiness
    {
        private Entities context;
        int userID = SessionManager.GetUserInfo().UserID;
        DateTime now = DateTime.Now;
        public PlanCostBusiness(Entities context)
        {
            this.context = context;
        }

        public double GetTotalPlanCost(int planID)
        {
            double totalCost = context.PlanTarget.Where(x => x.PlanID == planID)
                .Select(x => x.TotalBuget == null ? 0 : x.TotalBuget.Value).DefaultIfEmpty(0).Sum();
            return totalCost;
        }

        public List<PlanTargetModel> GetListCostHis(long planHisID)
        {
            PlanHis entity = this.context.PlanHis.Find(planHisID);
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetModel> listPlanTarget = this.context.PlanTargetHis.Where(x => x.PlanID == entity.PlanID && x.PlanHisID == planHisID && x.Status == Constants.IS_ACTIVE)
                .Select(x => new PlanTargetModel
                {
                    PlanID = x.PlanID,
                    TargetID = x.TargetID,
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetCode = x.TargetCode,
                    TotalBudget = x.TotalBuget
                }).ToList();
            List<PlanTargetActionModel> listPlanTargetAction = (from pta in this.context.PlanTargetActionHis
                                                                join pt in this.context.PlanTargetHis
                                                                on pta.PlanTargetID equals pt.PlanTargetID
                                                                join s in this.context.SubActionPlanHis on pta.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                                from sj in sg.DefaultIfEmpty()
                                                                where pt.PlanID == entity.PlanID && pt.PlanHisID == planHisID && pta.PlanHisID == planHisID
                                                                group sj by new { pt.PlanTargetID, pt.PlanTargetContent, pta.PlanTargetActionID, pta.TotalBudget, pta.ActionContent, pta.ActionCode, pta.ActionOrder } into g
                                                                orderby g.Key.PlanTargetID, g.Key.PlanTargetID
                                                                select new PlanTargetActionModel
                                                                {
                                                                    PlanTargetActionID = g.Key.PlanTargetActionID,
                                                                    ActionContent = g.Key.ActionContent,
                                                                    ActionCode = g.Key.ActionCode,
                                                                    PlanTargetID = g.Key.PlanTargetID,
                                                                    TotalBudget = g.Key.TotalBudget,
                                                                    ListSubActionPlan = g.Where(x => x != null && x.ParentID == null).Select(x => new SubActionPlanModel
                                                                    {
                                                                        SubActionPlanID = x.SubActionPlanID,
                                                                        SubActionCode = x.SubActionCode,
                                                                        SubActionContent = x.SubActionContent,
                                                                        PlanTargetActionID = x.PlanTargetActionID,
                                                                        TotalBudget = x.TotalBudget
                                                                    }).OrderBy(x => x.SubActionCode).ToList()
                                                                }).ToList();
            List<int> listPlanTargetID = listPlanTarget.Select(x => x.PlanTargetID).ToList();
            List<SubActionPlanCostModel> listPlanCost = this.context.SubActionPlanCostHis
                .Where(x => x.PlanHisID == planHisID && x.PlanTargetID != null && listPlanTargetID.Contains(x.PlanTargetID.Value))
            .Select(x => new SubActionPlanCostModel
            {
                PlanTargetID = x.PlanTargetID,
                PlanTargetAcionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                SubActionPlanCostID = x.SubActionPlanCostID,
                PlanYear = x.PlanYear,
                PlanTargetAcionSpendItemID = x.PlanTargetActionSpendItemID,
                SubActionSpendItemID = x.SubActionSpendItemID,
                TotalPrice = x.TotalPrice,
                Unit = x.Unit,
                Quantity = x.Quantity,
                Price = x.Price,
                CreateUserID = x.CreateUserID,
                ModifierID = x.ModifierID
            }).ToList();

            // Lay subaction muc con
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlanHis.Where(x =>
            this.context.PlanTargetActionHis.Any(p => p.PlanID == entity.PlanID && p.PlanHisID == planHisID
            && p.PlanTargetActionID == x.PlanTargetActionID) && x.PlanHisID == planHisID)
            .Select(x => new SubActionPlanModel
            {
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                ParentID = x.ParentID,
                TotalBudget = x.TotalBudget
            }).OrderBy(x => x.SubActionCode).ToList();

            List<SubActionSpendItemModel> listSubSpendItem = this.context.SubActionSpendItemHis.Where(x => this.context.PlanTargetActionHis
            .Any(p => p.PlanHisID == planHisID && p.PlanTargetActionID == x.PlanTargetActionID && p.PlanTargetID != null
            && listPlanTargetID.Contains(p.PlanTargetID.Value)) && x.PlanHisID == planHisID)
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemID = x.SpendItemID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice
            }).ToList();

            foreach (PlanTargetActionModel planTargetAction in listPlanTargetAction)
            {
                if (planTargetAction.ListSubActionPlan != null && planTargetAction.ListSubActionPlan.Any())
                {
                    foreach (SubActionPlanModel sub in planTargetAction.ListSubActionPlan)
                    {
                        this.AddSubAction(sub, listSubAction, listSubSpendItem, listPlanCost);
                    }
                }
                else
                {
                    planTargetAction.ListSubActionSpendItem = listSubSpendItem.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                    && x.SubActionPlanID == null && x.ParentID == null).ToList();
                    foreach (SubActionSpendItemModel item in planTargetAction.ListSubActionSpendItem)
                    {
                        this.AddSubCost(item, listSubSpendItem, listPlanCost);
                    }
                }
                // Lay thong tin chi tiet chi phi theo tung hoat dong con
                planTargetAction.ListSubActionPlanCost = listPlanCost.Where(x => x.PlanTargetAcionID == planTargetAction.PlanTargetActionID
                && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            }
            foreach (PlanTargetModel pt in listPlanTarget)
            {
                pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                pt.ListSubActionPlanCost = listPlanCost.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetAcionID == null
                && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            }
            return listPlanTarget;
        }

        public List<SubActionSpendItemModel> GetListSubItem(int SubActionSpendItemID)
        {
            SubActionSpendItem item = context.SubActionSpendItem.Find(SubActionSpendItemID);
            if (item == null)
            {
                return new List<SubActionSpendItemModel>();
            }
            List<SubActionSpendItemModel> listSubSpendItem = this.context.SubActionSpendItem.Where(x => x.PlanTargetActionID == item.PlanTargetActionID
            && x.SubActionPlanID == item.SubActionPlanID && x.SubActionSpendItemID != SubActionSpendItemID)
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemID = x.SpendItemID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice,
                SumCostType = x.SumCostType
            }).ToList();
            List<SubActionSpendItemModel> listRoot = listSubSpendItem.Where(x => x.ParentID == null).ToList();
            foreach (var root in listRoot)
            {
                GenSubItem(root, listSubSpendItem);
            }
            return listRoot;
        }

        public List<PlanTargetModel> GetListCost(int planID)
        {
            Plan entity = this.context.Plan.Where(x => x.PlanID == planID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetModel> listPlanTarget = this.context.PlanTarget.Where(x => x.PlanID == planID && x.Status == Constants.IS_ACTIVE)
                .OrderBy(x => x.TargetOrder)
                .Select(x => new PlanTargetModel
                {
                    PlanID = x.PlanID,
                    TargetID = x.TargetID,
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetCode = x.TargetCode,
                    TotalBudget = x.TotalBuget
                }).ToList();
            List<PlanTargetActionModel> listPlanTargetAction = (from pta in this.context.PlanTargetAction
                                                                join pt in this.context.PlanTarget
                                                                on pta.PlanTargetID equals pt.PlanTargetID
                                                                join s in this.context.SubActionPlan on pta.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                                from sj in sg.DefaultIfEmpty()
                                                                where pt.PlanID == planID
                                                                group sj by new { pt.PlanTargetID, pt.PlanTargetContent, pta.PlanTargetActionID, pta.TotalBudget, pta.ActionContent, pta.ActionCode, pta.ActionOrder, pta.SumCostType } into g
                                                                orderby g.Key.PlanTargetID, g.Key.PlanTargetID
                                                                select new PlanTargetActionModel
                                                                {
                                                                    PlanTargetActionID = g.Key.PlanTargetActionID,
                                                                    ActionContent = g.Key.ActionContent,
                                                                    ActionCode = g.Key.ActionCode,
                                                                    PlanTargetID = g.Key.PlanTargetID,
                                                                    TotalBudget = g.Key.TotalBudget,
                                                                    SumCostType = g.Key.SumCostType,
                                                                    ListSubActionPlan = g.Where(x => x != null && x.ParentID == null).OrderBy(x => x.SubActionOrder).Select(x => new SubActionPlanModel
                                                                    {
                                                                        SubActionPlanID = x.SubActionPlanID,
                                                                        SubActionCode = x.SubActionCode,
                                                                        SubActionContent = x.SubActionContent,
                                                                        PlanTargetActionID = x.PlanTargetActionID,
                                                                        TotalBudget = x.TotalBudget,
                                                                        SubActionOrder = x.SubActionOrder,
                                                                        SumCostType = x.SumCostType
                                                                    }).ToList()
                                                                }).ToList();
            List<int> listPlanTargetID = listPlanTarget.Select(x => x.PlanTargetID).ToList();

            CompareBusiness cb = new CompareBusiness(context);
            List<SubActionPlanCostModel> listPlanCost = this.context.SubActionPlanCost
                .Where(x => x.PlanTargetID != null && listPlanTargetID.Contains(x.PlanTargetID.Value))
            .Select(x => new SubActionPlanCostModel
            {
                PlanTargetID = x.PlanTargetID,
                PlanTargetAcionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                SubActionPlanCostID = x.SubActionPlanCostID,
                PlanYear = x.PlanYear,
                PlanTargetAcionSpendItemID = x.PlanTargetActionSpendItemID,
                SubActionSpendItemID = x.SubActionSpendItemID,
                TotalPrice = x.TotalPrice,
                Unit = x.Unit,
                Quantity = x.Quantity,
                Price = x.Price,
                CreateUserID = x.CreateUserID,
                ModifierID = x.ModifierID
            }).ToList();
            cb.UpdateSubActionCost(planID, userID, 0, listPlanCost);

            // Lay subaction muc con
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlan.Where(x =>
            this.context.PlanTargetAction.Any(p => p.PlanID == planID
            && p.PlanTargetActionID == x.PlanTargetActionID))
            .Select(x => new SubActionPlanModel
            {
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                ParentID = x.ParentID,
                TotalBudget = x.TotalBudget,
                SumCostType = x.SumCostType
            }).OrderBy(x => x.SubActionCode).ToList();

            List<SubActionSpendItemModel> listSubSpendItem = this.context.SubActionSpendItem.Where(x => this.context.PlanTargetAction
            .Any(p => p.PlanTargetActionID == x.PlanTargetActionID && p.PlanTargetID != null && listPlanTargetID.Contains(p.PlanTargetID.Value)))
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemID = x.SpendItemID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice,
                SumCostType = x.SumCostType,
                Description = x.Description
            }).ToList();
            // Update thay doi
            cb.UpdateSubActionSpendItem(planID, userID, 0, null, listSubSpendItem);

            foreach (PlanTargetActionModel planTargetAction in listPlanTargetAction)
            {
                if (planTargetAction.ListSubActionPlan != null && planTargetAction.ListSubActionPlan.Any())
                {
                    foreach (SubActionPlanModel sub in planTargetAction.ListSubActionPlan)
                    {
                        this.AddSubAction(sub, listSubAction, listSubSpendItem, listPlanCost);
                    }
                }
                else
                {
                    planTargetAction.ListSubActionSpendItem = listSubSpendItem.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                    && x.SubActionPlanID == null && x.ParentID == null).ToList();
                    foreach (SubActionSpendItemModel item in planTargetAction.ListSubActionSpendItem)
                    {
                        this.AddSubCost(item, listSubSpendItem, listPlanCost);
                    }
                    bool isParent = planTargetAction.ListSubActionSpendItem.Any(x => x.ListChild != null && x.ListChild.Any());
                    foreach (SubActionSpendItemModel item in planTargetAction.ListSubActionSpendItem)
                    {
                        item.IsParent = isParent;
                    }
                }
                // Lay thong tin chi tiet chi phi theo tung hoat dong con
                planTargetAction.ListSubActionPlanCost = listPlanCost.Where(x => x.PlanTargetAcionID == planTargetAction.PlanTargetActionID
                && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            }
            foreach (PlanTargetModel pt in listPlanTarget)
            {
                pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).OrderBy(x => x.ActionOrder).ToList();
                pt.ListSubActionPlanCost = listPlanCost.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetAcionID == null
                && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            }
            return listPlanTarget;
        }

        public void AddSubAction(SubActionPlanModel subAction, List<SubActionPlanModel> listAllSubAction,
            List<SubActionSpendItemModel> listSubSpendItem,
            List<SubActionPlanCostModel> listCost)
        {
            // Lay thong tin chi phi
            subAction.ListSubActionPlanCost = listCost.Where(x => x.SubActionPlanID == subAction.SubActionPlanID && x.SubActionSpendItemID == null).ToList();
            List<SubActionPlanModel> listChild = listAllSubAction.Where(x => x.ParentID == subAction.SubActionPlanID).OrderBy(x => x.SubActionOrder).ToList();
            if (listAllSubAction != null && listChild.Any())
            {
                subAction.ListChildSubActionPlan = listChild;
                foreach (SubActionPlanModel child in listChild)
                {
                    this.AddSubAction(child, listAllSubAction, listSubSpendItem, listCost);
                }
            }
            else
            {
                // Lay thong tin chi phi
                subAction.ListSubActionSpendItem = listSubSpendItem.Where(x => x.SubActionPlanID == subAction.SubActionPlanID && x.ParentID == null).ToList();
                foreach (SubActionSpendItemModel item in subAction.ListSubActionSpendItem)
                {
                    this.AddSubCost(item, listSubSpendItem, listCost);
                }
                bool isParent = subAction.ListSubActionSpendItem.Any(x => x.ListChild != null && x.ListChild.Any());
                foreach (SubActionSpendItemModel item in subAction.ListSubActionSpendItem)
                {
                    item.IsParent = isParent;
                }
            }
        }

        public void AddSubCost(SubActionSpendItemModel cost, List<SubActionSpendItemModel> listSubSpendItem, List<SubActionPlanCostModel> listCost)
        {
            cost.ListSubActionPlanCost = listCost.Where(x => x.SubActionSpendItemID == cost.SubActionSpendItemID).ToList();
            List<SubActionSpendItemModel> listChild = listSubSpendItem.Where(x => x.ParentID == cost.SubActionSpendItemID).ToList();
            if (listChild != null && listChild.Any())
            {
                cost.ListChild = listChild;
                cost.IsParent = true;
                foreach (SubActionSpendItemModel item in listChild)
                {
                    this.AddSubCost(item, listSubSpendItem, listCost);
                }
                bool isParent = listChild.Any(x => x.ListChild != null && x.ListChild.Any());
                foreach (SubActionSpendItemModel item in listChild)
                {
                    item.IsParent = isParent;
                }
            }
        }

        public void GenSubItem(SubActionSpendItemModel cost, List<SubActionSpendItemModel> listSubSpendItem)
        {
            List<SubActionSpendItemModel> listChild = listSubSpendItem.Where(x => x.ParentID == cost.SubActionSpendItemID).ToList();
            if (listChild != null && listChild.Any())
            {
                cost.ListChild = listChild;
                foreach (SubActionSpendItemModel item in listChild)
                {
                    this.GenSubItem(item, listSubSpendItem);
                }
            }
        }

        public void DelSpendItem(SubActionSpendItem entity)
        {
            List<SubActionPlanCost> listCost = this.context.SubActionPlanCost.Where(x => x.SubActionSpendItemID == entity.SubActionSpendItemID).ToList();
            this.context.SubActionPlanCost.RemoveRange(listCost);
            List<SubActionSpendItem> listChild = this.context.SubActionSpendItem.Where(x => x.ParentID == entity.SubActionSpendItemID).ToList();
            if (listChild != null && listChild.Any())
            {
                foreach (SubActionSpendItem item in listChild)
                {
                    this.DelSpendItem(item);
                }
                this.context.SubActionSpendItem.Remove(entity);
            }
            else
            {
                this.context.SubActionSpendItem.Remove(entity);
            }
        }

        public void SumMoneyBySpendItem(SubActionSpendItem entity, List<SubActionPlanCost> listCost, double? totalPrice, int sign)
        {
            if (entity.TotalPrice > 0)
            {
                if (entity.ParentID != null)
                {
                    SubActionSpendItem parent = this.context.SubActionSpendItem.Find(entity.ParentID.Value);
                    parent.TotalPrice = parent.TotalPrice + (sign * totalPrice);
                    // Update danh sach chi tiet
                    List<SubActionPlanCost> listUpdateCost = this.context.SubActionPlanCost
                        .Where(x => x.SubActionSpendItemID == parent.SubActionSpendItemID).ToList();
                    this.UpdateSubActionPlanCost(listCost, listUpdateCost, sign);
                    this.SumMoneyBySpendItem(parent, listCost, totalPrice, sign);
                }
                else
                {
                    if (entity.SubActionPlanID != null)
                    {
                        this.UpdateSubActionCost(entity.SubActionPlanID, listCost, sign);
                    }
                    List<SubActionPlanCost> listTargetActionCost = this.context.SubActionPlanCost
                        .Where(x => x.PlanTargetActionID == entity.PlanTargetActionID && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
                    this.UpdateSubActionPlanCost(listCost, listTargetActionCost, sign);

                    PlanTargetAction pta = this.context.PlanTargetAction.Find(entity.PlanTargetActionID);
                    pta.TotalBudget = listTargetActionCost.Sum(x => x.TotalPrice);

                    List<SubActionPlanCost> listTargetCost = this.context.SubActionPlanCost
                        .Where(x => x.PlanTargetID.Value == pta.PlanTargetID && x.PlanTargetActionID == null && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
                    this.UpdateSubActionPlanCost(listCost, listTargetCost, sign);
                    PlanTarget pt = this.context.PlanTarget.Find(pta.PlanTargetID);
                    pt.TotalBuget = listTargetCost.Sum(x => x.TotalPrice);
                }
            }
        }

        private void UpdateSubActionCost(int? subActionID, List<SubActionPlanCost> listCostUpdate, int sign)
        {
            if (subActionID != null)
            {
                List<SubActionPlanCost> listCost = this.context.SubActionPlanCost.Where(x => x.SubActionPlanID == subActionID && x.SubActionSpendItemID == null).ToList();
                this.UpdateSubActionPlanCost(listCostUpdate, listCost, sign);
                SubActionPlan subAction = this.context.SubActionPlan.Find(subActionID);
                subAction.TotalBudget = listCost.Sum(x => x.TotalPrice);
                if (subAction.ParentID != null)
                {
                    this.UpdateSubActionCost(subAction.ParentID, listCostUpdate, sign);
                }
            }
        }

        private void UpdateSubActionPlanCost(List<SubActionPlanCost> listSrc, List<SubActionPlanCost> listDes, int sign)
        {
            foreach (SubActionPlanCost cost in listDes)
            {
                SubActionPlanCost costUpdate = listSrc.Where(x => x.PlanYear == cost.PlanYear).FirstOrDefault();
                if (costUpdate != null && costUpdate.TotalPrice != null)
                {
                    cost.TotalPrice = cost.TotalPrice.GetValueOrDefault() + (sign * costUpdate.TotalPrice);
                    if (cost.TotalPrice == 0)
                    {
                        cost.TotalPrice = null;
                    }
                }
            }
        }

        private void UpdateTotalCost(int planID)
        {
            List<SubActionPlanCost> listCost = this.context.SubActionPlanCost.Where(x => context.PlanTarget.Any(p => p.PlanID == planID && x.PlanTargetID == p.PlanTargetID)).ToList();
            // Chi phi theo hang muc con
            List<int> listCostSpendID = listCost.Where(x => x.SubActionSpendItemID != null).Select(x => x.SubActionSpendItemID.Value).Distinct().ToList();
            List<SubActionSpendItem> listSpend = this.context.SubActionSpendItem.Where(x => listCostSpendID.Contains(x.SubActionSpendItemID)).ToList();
            List<int> listRootSpendItemID = listSpend.Where(x => x.ParentID == null).Select(x => x.SubActionSpendItemID).ToList();
            List<SubActionPlanCost> listRootSpendCost = listCost.Where(x => x.SubActionSpendItemID != null && listRootSpendItemID.Contains(x.SubActionSpendItemID.Value)).ToList();
            List<int> listSubActionID = listCost.Where(x => x.SubActionPlanID != null).Select(x => x.SubActionPlanID.Value).Distinct().ToList();
            List<SubActionPlan> listSubAction = this.context.SubActionPlan.Where(x => listSubActionID.Contains(x.SubActionPlanID)).ToList();
            List<int> listPlanTargetActionID = listCost.Where(x => x.PlanTargetActionID != null).Select(x => x.PlanTargetActionID.Value).Distinct().ToList();
            List<PlanTargetAction> listPlanTargetAction = this.context.PlanTargetAction.Where(x => listPlanTargetActionID.Contains(x.PlanTargetActionID)).ToList();
            foreach (SubActionPlanCost item in listRootSpendCost)
            {
                this.UpdateSpendCost(item, listCost, listSpend, listPlanTargetAction, listSubAction);
            }
            List<SubActionPlanCost> listRootSubActionCost = listCost.Where(x => x.SubActionPlanID != null && x.SubActionSpendItemID == null
            && listSubAction.Any(s => s.ParentID == null && s.SubActionPlanID == x.SubActionPlanID)).ToList();
            Dictionary<int, List<SubActionPlanCost>> dicPlanTargetActionCost = new Dictionary<int, List<SubActionPlanCost>>();
            foreach (SubActionPlanCost item in listRootSubActionCost)
            {
                this.UpdateTreeSubCost(item, listCost, listSubAction);
                if (!dicPlanTargetActionCost.ContainsKey(item.PlanTargetActionID.GetValueOrDefault()))
                {
                    dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()] = new List<SubActionPlanCost>();
                }
                dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()].Add(item);
            }

            #region Cap nhat chi phi tong vao hoat dong
            // Update rieng phan muc tieu
            List<int> listPlanTargetID = listCost.Where(x => x.PlanTargetID != null).Select(x => x.PlanTargetID.Value).Distinct().ToList();
            List<SubActionPlanCost> listTargetCost = this.context.SubActionPlanCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID == null && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
            foreach (SubActionPlanCost targetCost in listTargetCost)
            {
                targetCost.Quantity = null;
                targetCost.Price = null;
                List<SubActionPlanCost> listPlanTargetActionCost = listCost.Where(x => x.PlanTargetID == targetCost.PlanTargetID && x.PlanTargetActionID != null
                && x.SubActionPlanID == null && x.SubActionSpendItemID == null && x.PlanYear == targetCost.PlanYear).ToList();
                if (listPlanTargetActionCost != null && listPlanTargetActionCost.Any())
                {
                    foreach (SubActionPlanCost item in listPlanTargetActionCost)
                    {
                        if (dicPlanTargetActionCost.ContainsKey(item.PlanTargetActionID.GetValueOrDefault()))
                        {
                            PlanTargetAction pta = listPlanTargetAction.Where(x => x.PlanTargetActionID == item.PlanTargetActionID.GetValueOrDefault()).FirstOrDefault();
                            List<SubActionPlanCost> listChildPlanTargetActionCost = dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()].Where(x => x.PlanYear == item.PlanYear).ToList();
                            bool isTotal;
                            if (pta.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_TOTAL)
                            {
                                isTotal = true;
                            }
                            else if (pta.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_PRICE)
                            {
                                isTotal = false;
                            }
                            else
                            {
                                isTotal = listChildPlanTargetActionCost.Count > 1;
                            }
                            if (isTotal)
                            {
                                item.Quantity = null;
                                item.Price = null;
                                item.TotalPrice = listChildPlanTargetActionCost.Select(x => x.TotalPrice.GetValueOrDefault()).Sum();
                            }
                            else
                            {
                                item.Price = listChildPlanTargetActionCost.Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                                if (item.Quantity != null)
                                {
                                    item.TotalPrice = item.Price * item.Quantity;
                                }
                                else
                                {
                                    item.TotalPrice = null;
                                }
                            }
                        }
                    }
                    targetCost.TotalPrice = listPlanTargetActionCost.Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                }
            }
            // Update tong chi phi             
            List<PlanTarget> listPlanTarget = this.context.PlanTarget.Where(x => listPlanTargetID.Contains(x.PlanTargetID)).ToList();
            foreach (SubActionPlan item in listSubAction)
            {
                item.TotalBudget = listCost.Where(x => x.SubActionPlanID == item.SubActionPlanID && x.SubActionSpendItemID == null).Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                if (item.TotalBudget == 0)
                {
                    item.TotalBudget = null;
                }
            }
            foreach (PlanTargetAction item in listPlanTargetAction)
            {
                item.TotalBudget = listCost.Where(x => x.PlanTargetActionID == item.PlanTargetActionID && x.PlanTargetID == item.PlanTargetID && x.SubActionPlanID == null && x.SubActionSpendItemID == null).Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                if (item.TotalBudget == 0)
                {
                    item.TotalBudget = null;
                }
            }
            foreach (PlanTarget item in listPlanTarget)
            {
                item.TotalBuget = listCost.Where(x => x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == null && x.SubActionSpendItemID == null).Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                if (item.TotalBuget == 0)
                {
                    item.TotalBuget = null;
                }
            }
            #endregion
        }

        public double UpdateTreeSubCost(SubActionPlanCost cost, List<SubActionPlanCost> listCost, List<SubActionPlan> listSubAction)
        {
            double totalMoney = 0;
            List<SubActionPlan> listChildAction = listSubAction.Where(x => x.ParentID == cost.SubActionPlanID).ToList();
            if (listChildAction != null && listChildAction.Any())
            {
                SubActionPlan subAction = listSubAction.Where(x => x.SubActionPlanID == cost.SubActionPlanID).FirstOrDefault();
                foreach (SubActionPlan child in listChildAction)
                {
                    SubActionPlanCost childCost = listCost.Where(x => x.SubActionPlanID == child.SubActionPlanID && x.PlanYear == cost.PlanYear).FirstOrDefault();
                    if (childCost != null)
                    {
                        totalMoney += this.UpdateTreeSubCost(childCost, listCost, listSubAction);
                    }
                }
                bool isTotal;
                if (subAction.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_TOTAL)
                {
                    isTotal = true;
                }
                else if (subAction.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_PRICE)
                {
                    isTotal = false;
                }
                else
                {
                    isTotal = listChildAction.Count > 1;
                }
                if (isTotal)
                {
                    cost.TotalPrice = totalMoney;
                    cost.Price = null;
                    cost.Quantity = null;
                }
                else
                {
                    cost.Price = totalMoney;
                    if (cost.Quantity != null)
                    {
                        cost.TotalPrice = totalMoney * cost.Quantity;
                    }
                    else
                    {
                        cost.TotalPrice = null;
                    }
                }
            }
            else
            {
                totalMoney = cost.TotalPrice.GetValueOrDefault();
            }
            return totalMoney;
        }

        public double UpdateSpendCost(SubActionPlanCost cost, List<SubActionPlanCost> listCost, List<SubActionSpendItem> listSpend,
            List<PlanTargetAction> listPta, List<SubActionPlan> listSubAction)
        {
            double totalMoney = 0;
            List<SubActionSpendItem> listSpendChild = listSpend.Where(x => x.ParentID == cost.SubActionSpendItemID).ToList();
            SubActionSpendItem spend = listSpend.Where(x => x.SubActionSpendItemID == cost.SubActionSpendItemID).FirstOrDefault();
            if (listSpendChild != null && listSpendChild.Any())
            {
                foreach (SubActionSpendItem child in listSpendChild)
                {
                    SubActionPlanCost childCost = listCost.Where(x => x.SubActionSpendItemID == child.SubActionSpendItemID && x.PlanYear == cost.PlanYear).FirstOrDefault();
                    if (childCost != null)
                    {
                        totalMoney += this.UpdateSpendCost(childCost, listCost, listSpend, listPta, listSubAction);
                    }
                }
                bool isTotal;
                if (spend.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_TOTAL)
                {
                    isTotal = true;
                }
                else if (spend.SumCostType.GetValueOrDefault() == Constants.SUM_COST_TYPE_PRICE)
                {
                    isTotal = false;
                }
                else
                {
                    isTotal = listSpendChild.Count > 1;
                }
                if (isTotal)
                {
                    cost.TotalPrice = totalMoney;
                    cost.Price = null;
                    cost.Quantity = null;
                }
                else
                {
                    cost.Price = totalMoney;
                    if (cost.Quantity != null)
                    {
                        cost.TotalPrice = totalMoney * cost.Quantity;
                        totalMoney = cost.TotalPrice.GetValueOrDefault();
                    }
                    else
                    {
                        cost.TotalPrice = null;
                        totalMoney = 0;
                    }
                }
            }
            else
            {
                totalMoney = cost.TotalPrice.GetValueOrDefault();
            }

            if (spend != null)
            {
                spend.TotalPrice = listCost.Where(x => x.SubActionSpendItemID == spend.SubActionSpendItemID).Select(x => x.TotalPrice.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
                if (spend.TotalPrice == 0)
                {
                    spend.TotalPrice = null;
                }
            }
            if (spend != null && spend.ParentID == null)
            {
                SubActionPlanCost parCost = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                && x.SubActionPlanID == cost.SubActionPlanID && x.PlanYear == cost.PlanYear && x.SubActionSpendItemID == null).FirstOrDefault();
                if (parCost != null)
                {
                    List<int> listRootSpend = listSpend.Where(x => x.PlanTargetActionID == parCost.PlanTargetActionID
                            && x.SubActionPlanID == parCost.SubActionPlanID && x.ParentID == null).Select(x => x.SubActionSpendItemID).ToList();
                    var listCostRoot = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                            && x.SubActionPlanID == cost.SubActionPlanID && x.PlanYear == cost.PlanYear && x.SubActionSpendItemID != null && listRootSpend.Contains(x.SubActionSpendItemID.Value)).Select(x => x.TotalPrice).ToList();

                    bool isTotal;
                    int? sumType = null;
                    if (parCost.SubActionPlanID != null)
                    {
                        SubActionPlan subAction = listSubAction.Where(x => x.SubActionPlanID == parCost.SubActionPlanID).FirstOrDefault();
                        sumType = subAction.SumCostType;
                    }
                    else
                    {
                        PlanTargetAction pta = listPta.Where(x => x.PlanTargetActionID == parCost.PlanTargetActionID).FirstOrDefault();
                        sumType = pta.SumCostType;
                    }
                    if (sumType.GetValueOrDefault() == Constants.SUM_COST_TYPE_TOTAL)
                    {
                        isTotal = true;
                    }
                    else if (sumType.GetValueOrDefault() == Constants.SUM_COST_TYPE_PRICE)
                    {
                        isTotal = false;
                    }
                    else
                    {
                        isTotal = listCostRoot.Count > 1;
                    }
                    if (isTotal)
                    {
                        parCost.TotalPrice = listCostRoot.Sum();
                        parCost.Quantity = null;
                        parCost.Price = null;
                    }
                    else
                    {
                        parCost.Price = listCostRoot.DefaultIfEmpty(0).Sum(); ;
                        if (parCost.Quantity != null)
                        {
                            parCost.TotalPrice = parCost.Price * parCost.Quantity;
                        }
                        else
                        {
                            parCost.TotalPrice = null;
                        }
                    }
                }
            }
            return totalMoney;
        }

        public void SavePlanCost(int PlanID, List<string> listUnit, List<string> listQuantity, List<string> listPrice, List<string> listTotalPrice, List<int> listSubActionPlanCostID, int planID)
        {
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            List<int> listPlanTargetID = this.context.PlanTarget.Where(x => x.PlanID == planID).Select(x => x.PlanTargetID).ToList();
            List<SubActionPlanCost> listCost = this.context.SubActionPlanCost.Where(x => x.PlanTargetID != null
            && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
            Plan plan = context.Plan.Find(planID);
            List<SpendItem> listSpendItem = context.SpendItem.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            List<SubActionSpendItem> listSubActionSpendItem = context.SubActionSpendItem.Where(x => context.PlanTargetAction.Any(t => t.PlanID == PlanID && t.PlanTargetActionID == x.PlanTargetActionID)).ToList();
            if (plan != null)
            {
                if (plan.Status == Constants.PLAN_APPROVED && plan.IsChange.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    ChangeRequest cr = context.ChangeRequest.Where(x => x.PlanID == plan.PlanID && x.Status == Constants.PLAN_APPROVED && x.IsFinish == null).FirstOrDefault();
                    if (cr != null)
                    {
                        // Lay thong tin hoat dong khoa
                        List<ChangeRequestAction> listChangeAction = context.ChangeRequestAction.Where(x => x.ChangeRequestID == cr.ChangeRequestID && x.ApproveStatus == Constants.IS_ACTIVE)
                            .ToList();
                        listCost.RemoveAll(x => !listChangeAction.Any(c => c.PlanTargetID == x.PlanTargetID)
                        || (x.PlanTargetActionID != null && !listChangeAction.Any(c => c.PlanTargetActionID == x.PlanTargetActionID)));
                    }
                }
                else if (plan.Status != Constants.PLAN_APPROVED)
                {
                    // Lay thong tin hoat dong khoa
                    List<ChangeRequestAction> listChangeAction = context.ChangeRequestAction.Where(x => x.PlanID == planID && x.ApproveStatus == Constants.IS_ACTIVE)
                        .ToList();
                    if (listChangeAction.Any())
                    {
                        listCost.RemoveAll(x => !listChangeAction.Any(c => c.PlanTargetID == x.PlanTargetID)
                        || (x.PlanTargetActionID != null && !listChangeAction.Any(c => c.PlanTargetActionID == x.PlanTargetActionID)));
                    }
                }
            }
            for (int i = 0; i < listSubActionPlanCostID.Count; i++)
            {
                string sTotalPrice = listTotalPrice[i];
                string sPrice = listPrice[i];
                string sQuantity = listQuantity[i];
                string unit = listUnit[i];
                SubActionPlanCost cost = listCost.Where(x => x.SubActionPlanCostID == listSubActionPlanCostID[i]).FirstOrDefault();
                if (cost != null)
                {
                    cost.Unit = unit;
                    cost.Quantity = null;
                    cost.Price = null;
                    cost.TotalPrice = null;
                    cost.ModifierID = userID;
                    cost.UpdateDate = now;
                    double totalPrice;
                    double price;
                    double quantity;
                    if (!string.IsNullOrWhiteSpace(sPrice))
                    {
                        sPrice = sPrice.Replace(",", "").Replace("'", "").Trim();
                        // Canh bao han muc doi voi nhung muc chi duoc khai bao san voi thong tin han muc
                        if (cost.SubActionSpendItemID != null)
                        {
                            SubActionSpendItem subSpendItem = listSubActionSpendItem.Where(x => x.SubActionSpendItemID == cost.SubActionSpendItemID).FirstOrDefault();
                            if (subSpendItem.SpendItemID != null)
                            {
                                SpendItem spendItem = listSpendItem.Where(x => x.SpendItemID == subSpendItem.SpendItemID).FirstOrDefault();
                                if (spendItem != null && spendItem.Price != null)
                                {
                                    if (double.Parse(sPrice) > spendItem.Price)
                                    {
                                        throw new BusinessException("Đơn giá chi phí 01:" + spendItem.SpenContent + " không được vượt quá hạn mức:" + String.Format("{0:n}", spendItem.Price));
                                    }
                                }
                            }
                        }
                        if (double.TryParse(sPrice, out price))
                        {
                            cost.Price = price;
                        }
                        //else
                        //{
                        //    throw new BusinessException("Giá phải nhập là kiểu số thập phân");
                        //}
                    }
                    if (!string.IsNullOrWhiteSpace(sQuantity))
                    {
                        sQuantity = sQuantity.Replace(",", "").Replace("'", "").Trim();
                        if (double.TryParse(sQuantity, out quantity))
                        {
                            cost.Quantity = quantity;
                        }
                    }
                    if (cost.Quantity != null && cost.Price != null)
                    {
                        cost.TotalPrice = cost.Quantity * cost.Price;
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(sTotalPrice))
                        {
                            if (double.TryParse(sTotalPrice, out totalPrice))
                            {
                                cost.TotalPrice = totalPrice;
                            }
                            //else
                            //{
                            //    throw new BusinessException("Giá phải nhập là kiểu số thập phân");
                            //}
                        }
                    }
                }
            }
            context.SaveChanges();
            // Cap nhat lai chi phi tong
            this.UpdateTotalCost(planID);
            context.SaveChanges();
            context.Configuration.AutoDetectChangesEnabled = true;
            context.Configuration.ValidateOnSaveEnabled = true;
        }

        public void InitPlanCost(int planID, int userID)
        {
            var listSubAction = (from s in this.context.SubActionPlan
                                 join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 join t in this.context.PlanTarget on p.PlanTargetID equals t.PlanTargetID
                                 where t.PlanID == planID
                                 select new
                                 {
                                     s.SubActionPlanID,
                                     p.PlanTargetActionID,
                                     t.PlanTargetID,
                                     SubAction = s
                                 }).ToList();
            var listPlanTargetAction = this.context.PlanTargetAction.Where(x => this.context.PlanTarget.Any(t => t.PlanTargetID == x.PlanTargetID
            && t.PlanID == planID)).ToList();
            var listSubCost = (from c in this.context.SubActionPlanCost
                               join t in this.context.PlanTarget on c.PlanTargetID equals t.PlanTargetID
                               where t.PlanID == planID && c.SubActionSpendItemID == null
                               select new
                               {
                                   c.SubActionPlanID,
                                   c.PlanTargetActionID,
                                   c.PlanTargetID,
                                   c.PlanYear
                               }).ToList();
            List<int> litsPlanTargetID = context.PlanTarget.Where(x => x.PlanID == planID).Select(x => x.PlanTargetID).Distinct().ToList();

            Plan plan = this.context.Plan.Find(planID);
            Period period = this.context.Period.Find(plan.PeriodID);
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            for (int year = fromYear; year <= toYear; year++)
            {
                foreach (int planTargetID in litsPlanTargetID)
                {
                    if (!listSubCost.Any(x => x.PlanTargetActionID == null && x.PlanTargetID == planTargetID && x.PlanYear == year))
                    {
                        SubActionPlanCost cost = new SubActionPlanCost();
                        cost.PlanTargetID = planTargetID;
                        cost.PlanYear = year;
                        cost.CreateDate = now;
                        cost.CreateUserID = userID;
                        cost.ModifierID = userID;
                        cost.UpdateDate = now;
                        this.context.SubActionPlanCost.Add(cost);
                    }
                }

                foreach (var planTargetAction in listPlanTargetAction)
                {
                    if (!listSubCost.Any(x => x.SubActionPlanID == null && x.PlanTargetActionID != null
                    && x.PlanTargetID == planTargetAction.PlanTargetID && x.PlanTargetActionID == planTargetAction.PlanTargetActionID && x.PlanYear == year))
                    {
                        SubActionPlanCost cost = new SubActionPlanCost();
                        cost.PlanTargetID = planTargetAction.PlanTargetID;
                        cost.PlanTargetActionID = planTargetAction.PlanTargetActionID;
                        cost.PlanYear = year;
                        cost.CreateDate = now;
                        cost.CreateUserID = userID;
                        cost.ModifierID = userID;
                        cost.UpdateDate = now;
                        this.context.SubActionPlanCost.Add(cost);
                    }
                    if (planTargetAction.SumCostType == null)
                    {
                        planTargetAction.SumCostType = Constants.SUM_COST_TYPE_TOTAL;
                    }
                }

                foreach (var subAction in listSubAction)
                {
                    if (!listSubCost.Any(x => x.SubActionPlanID != null && x.PlanTargetActionID != null
                    && x.PlanTargetID == subAction.PlanTargetID && x.PlanTargetActionID == subAction.PlanTargetActionID && x.SubActionPlanID == subAction.SubActionPlanID && x.PlanYear == year))
                    {
                        SubActionPlanCost cost = new SubActionPlanCost();
                        cost.PlanTargetID = subAction.PlanTargetID;
                        cost.PlanTargetActionID = subAction.PlanTargetActionID;
                        cost.SubActionPlanID = subAction.SubActionPlanID;
                        cost.PlanYear = year;
                        cost.CreateDate = now;
                        cost.CreateUserID = userID;
                        cost.ModifierID = userID;
                        cost.UpdateDate = now;
                        this.context.SubActionPlanCost.Add(cost);
                    }
                    if (subAction.SubAction != null && subAction.SubAction.SumCostType == null)
                    {
                        subAction.SubAction.SumCostType = Constants.SUM_COST_TYPE_TOTAL;
                    }
                }
            }
        }

        public void UpdateTrueCost(int planID)
        {
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => x.PlanID == planID).ToList();
            List<SubActionPlan> listSubActionPlan = context.SubActionPlan.Where(x => x.PlanID == planID).ToList();
            List<SubActionSpendItem> listSpendItem = context.SubActionSpendItem.Where(x => context.PlanTargetAction.Any(t => t.PlanID == planID && t.PlanTargetActionID == x.PlanTargetActionID)).ToList();
            List<SubActionPlanCost> listSubCost = context.SubActionPlanCost.Where(x => x.PlanTargetActionID != null && context.PlanTarget.Any(t => t.PlanID == planID && t.PlanTargetID == x.PlanTargetID)).ToList();

        }
    }
}