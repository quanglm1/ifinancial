﻿using IInterface.Common;
using IInterface.Common.CommonBO;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Business
{
    public class FlowProcessBusiness
    {
        private Entities context;
        public FlowProcessBusiness(Entities context)
        {
            this.context = context;
        }

        private bool HasRole(FlowProcess flow, int objectDeptID, User user, List<int> listRoleID, List<int> listDeptManID)
        {
            if (flow != null)
            {
                if (user.DepartmentID == objectDeptID)
                {
                    return true;
                }
                // Dau moi
                if (flow.IsRepresent.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    if (!listDeptManID.Contains(objectDeptID))
                    {
                        return false;
                    }
                }
                // Truong phong
                if (flow.IsHeadOffice.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    if (user.IsHeadOffice.GetValueOrDefault() != Constants.IS_ACTIVE)
                    {
                        // Quyen phu hop
                        return false;
                    }
                }
                // Khac
                if (flow.ProcessorType == Constants.FLOW_ROLE)
                {
                    if (listRoleID.Contains(flow.ProcessorID))
                    {
                        return true;
                    }
                }
                else
                {
                    if (user.DepartmentID == flow.ProcessorID)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public ObjectRoleBO GetRoleForPlanChange(int userID, int objectID)
        {
            List<int> listObjectID = new List<int> { objectID };
            Dictionary<int, ObjectRoleBO> dicRole = this.GetRoleForObject(userID, listObjectID, Constants.COMMENT_TYPE_PLAN_TARGET, true);
            return dicRole[objectID];
        }

        public ObjectRoleBO GetRoleForObject(int userID, int objectID, int objectType)
        {
            List<int> listObjectID = new List<int> { objectID };
            Dictionary<int, ObjectRoleBO> dicRole = this.GetRoleForObject(userID, listObjectID, objectType);
            return dicRole[objectID];
        }

        public Dictionary<int, ObjectRoleBO> GetRoleForDept(int userID, List<Department> listDept, int objectType)
        {
            User user = this.context.User.Find(userID);
            if (user == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<int> listRoleID = this.context.UserRole.Where(x => x.UserID == userID).Select(x => x.RoleID).ToList();
            List<int> listDeptManID = this.context.UserManageDepartment.Where(x => x.UserID == userID).Select(x => x.DepartmentID).ToList();
            List<FlowProcess> listFlow = this.context.FlowProcess.Where(x => x.ObjectType == objectType).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new Dictionary<int, ObjectRoleBO>();
            foreach (Department dept in listDept)
            {
                ObjectRoleBO roleBO = new ObjectRoleBO();
                roleBO.ObjectID = dept.DepartmentID;
                roleBO.Role = Constants.ROLE_NO_VIEW;
                if (dept.DepartmentID == user.DepartmentID)
                {
                    roleBO.Role = Constants.ROLE_EDIT;
                }
                else
                {
                    if (dept.DepartmentType == Constants.DEPT_TYPE_PBQUY)
                    {
                        // Ke hoach cua quy xu ly luong rieng
                        if (user.DepartmentID == Constants.HEAD_DEPT_ID)
                        {
                            if (user.IsHeadOffice == Constants.IS_ACTIVE)
                            {
                                roleBO.Role = Constants.ROLE_APPROVE;
                            }
                            else
                            {
                                roleBO.Role = Constants.ROLE_EDIT;
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < listFlow.Count; i++)
                        {
                            FlowProcess flow = listFlow[i];
                            if (this.HasRole(flow, dept.DepartmentID, user, listRoleID, listDeptManID))
                            {
                                roleBO.Role = Constants.ROLE_VIEW;
                            }
                        }
                    }
                }
                dicRole[dept.DepartmentID] = roleBO;
            }
            return dicRole;
        }

        public Dictionary<int, ObjectRoleBO> GetRoleForObject(int userID, List<int> listObjectID, int objectType, bool isPlanChange = false)
        {
            User user = this.context.User.Find(userID);
            if (user == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<int> listRoleID = this.context.UserRole.Where(x => x.UserID == userID).Select(x => x.RoleID).ToList();
            List<int> listDeptManID = this.context.UserManageDepartment.Where(x => x.UserID == userID).Select(x => x.DepartmentID).ToList();
            Dictionary<int, int?> dicStepNow = new Dictionary<int, int?>();
            Dictionary<int, ObjectRoleBO> dicRole = new Dictionary<int, ObjectRoleBO>();
            Dictionary<int, int> dicDept = new Dictionary<int, int>();
            Dictionary<int, int> dicStatus = new Dictionary<int, int>();
            Dictionary<int, int> dicPL = new Dictionary<int, int>();
            Dictionary<int, int> dicPlanType = new Dictionary<int, int>();
            int periodID = 0;
            switch (objectType)
            {
                case Constants.COMMENT_TYPE_PLAN_TARGET:
                    List<Plan> listPlan = this.context.Plan.Where(x => listObjectID.Contains(x.PlanID)).ToList();
                    listPlan.ForEach(x =>
                    {
                        dicStepNow[x.PlanID] = x.StepNow;
                        dicStatus[x.PlanID] = x.Status;
                        if (isPlanChange && x.Status == Constants.PLAN_APPROVED)
                        {
                            dicStepNow[x.PlanID] = x.ChangeStepNow;
                            dicStatus[x.PlanID] = x.ChangeStatus.GetValueOrDefault();
                        }
                        dicPlanType[x.PlanID] = x.Type.GetValueOrDefault();
                        dicDept[x.PlanID] = x.DepartmentID;
                        periodID = x.PeriodID;
                    });
                    break;
                case Constants.COMMENT_TYPE_SETTLE:
                    List<SettleSynthesis> listSettleReport = this.context.SettleSynthesis.Where(x => listObjectID.Contains(x.SettleSynthesisID)).ToList();
                    List<int> listPlanBySettleID = listSettleReport.Select(x => x.PlanID).Distinct().ToList();
                    List<Plan> listPlanBySettle = context.Plan.Where(x => listPlanBySettleID.Contains(x.PlanID)).ToList();
                    listSettleReport.ForEach(x =>
                    {
                        dicStepNow[x.SettleSynthesisID] = x.StepNow;
                        dicDept[x.SettleSynthesisID] = x.DepartmentID.GetValueOrDefault();
                        dicStatus[x.SettleSynthesisID] = x.Status;
                        Plan plan = listPlanBySettle.Where(p => p.PlanID == x.PlanID).FirstOrDefault();
                        dicPlanType[x.SettleSynthesisID] = plan.Type.GetValueOrDefault();
                        periodID = plan.PeriodID;
                    });
                    break;
                case Constants.COMMENT_TYPE_FINANCIAL:
                    List<FinancialReport> listFinancialReport = this.context.FinancialReport.Where(x => listObjectID.Contains(x.FinancialReportID)).ToList();
                    List<int> listPlanByFinancialID = listFinancialReport.Select(x => x.PlanID).Distinct().ToList();
                    List<Plan> listPlanByFinancial = context.Plan.Where(x => listPlanByFinancialID.Contains(x.PlanID)).ToList();
                    listFinancialReport.ForEach(x =>
                    {
                        dicStepNow[x.FinancialReportID] = x.StepNow;
                        dicDept[x.FinancialReportID] = x.DepartmentID.GetValueOrDefault();
                        dicStatus[x.FinancialReportID] = x.Status;
                        Plan plan = listPlanByFinancial.Where(p => p.PlanID == x.PlanID).FirstOrDefault();
                        dicPlanType[x.FinancialReportID] = plan.Type.GetValueOrDefault();
                        periodID = plan.PeriodID;
                    });
                    break;
                case Constants.COMMENT_TYPE_ACTION:
                    List<ActionReport> listActionReport = this.context.ActionReport.Where(x => listObjectID.Contains(x.ActionReportID)).ToList();
                    List<int> listPlanByActionID = listActionReport.Select(x => x.PlanID).Distinct().ToList();
                    List<Plan> listPlanByAction = context.Plan.Where(x => listPlanByActionID.Contains(x.PlanID)).ToList();
                    listActionReport.ForEach(x =>
                    {
                        dicStepNow[x.ActionReportID] = x.StepNow;
                        dicDept[x.ActionReportID] = x.DepartmentID.GetValueOrDefault();
                        dicStatus[x.ActionReportID] = x.Status;
                        Plan plan = listPlanByAction.Where(p => p.PlanID == x.PlanID).FirstOrDefault();
                        dicPlanType[x.ActionReportID] = plan.Type.GetValueOrDefault();
                        periodID = plan.PeriodID;
                    });
                    break;
                case Constants.COMMENT_TYPE_CHANGE_REQUEST:
                    List<ChangeRequest> listChangeRequest = this.context.ChangeRequest.Where(x => listObjectID.Contains(x.ChangeRequestID)).ToList();
                    List<int> listPlanByChangeRequestID = listChangeRequest.Select(x => x.PlanID).Distinct().ToList();
                    List<Plan> listPlanByChangeRequest = context.Plan.Where(x => listPlanByChangeRequestID.Contains(x.PlanID)).ToList();
                    listChangeRequest.ForEach(x =>
                    {
                        dicStepNow[x.ChangeRequestID] = x.StepNow;
                        dicDept[x.ChangeRequestID] = x.DepartmentID;
                        dicStatus[x.ChangeRequestID] = x.Status;
                        Plan plan = listPlanByChangeRequest.Where(p => p.PlanID == x.PlanID).FirstOrDefault();
                        dicPlanType[x.ChangeRequestID] = plan.Type.GetValueOrDefault();
                        periodID = plan.PeriodID;
                    });
                    break;
            }

            foreach (int objectID in listObjectID)
            {
                ObjectRoleBO roleBO = new ObjectRoleBO();
                roleBO.ObjectID = objectID;
                int? stepNow = dicStepNow[objectID];
                roleBO.Role = Constants.ROLE_NO_VIEW;
                if (stepNow == null && user.DepartmentID == dicDept[objectID])
                {
                    roleBO.Role = Constants.ROLE_EDIT;
                }
                else
                {
                    if (dicDept[objectID] == user.DepartmentID)
                    {
                        roleBO.Role = Constants.ROLE_VIEW;
                    }
                    else
                    {
                        if (dicPlanType[objectID] == Constants.MAIN_TYPE)
                        {
                            // Ke hoach cua quy xu ly luong rieng
                            if (user.DepartmentID == Constants.HEAD_DEPT_ID)
                            {
                                if (stepNow == null)
                                {
                                    roleBO.Role = Constants.ROLE_VIEW;
                                }
                                else
                                {
                                    if (user.IsHeadOffice == Constants.IS_ACTIVE)
                                    {
                                        if (dicStatus[objectID] == Constants.PLAN_APPROVED)
                                        {
                                            roleBO.Role = Constants.ROLE_AFTER_APPROVE;
                                        }
                                        else
                                        {
                                            roleBO.Role = Constants.ROLE_APPROVE;
                                        }
                                    }
                                    else
                                    {
                                        roleBO.Role = Constants.ROLE_EDIT;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Doi voi luong yeu cau thay doi thi lay theo luong cua ke hoach
                            List<FlowProcess> listFlow = this.context.FlowProcess.Where(x => x.ObjectType == objectType && x.PeriodID == periodID).ToList();
                            if (objectType == Constants.COMMENT_TYPE_CHANGE_REQUEST)
                            {
                                listFlow = this.context.FlowProcess.Where(x => x.ObjectType == Constants.COMMENT_TYPE_PLAN_TARGET && x.PeriodID == periodID).ToList();
                            }
                            for (int i = 0; i < listFlow.Count; i++)
                            {
                                FlowProcess flow = listFlow[i];
                                if (this.HasRole(flow, dicDept[objectID], user, listRoleID, listDeptManID))
                                {
                                    roleBO.Role = Constants.ROLE_VIEW;
                                    if (stepNow.GetValueOrDefault() > listFlow.Count)
                                    {
                                        if (i == listFlow.Count - 1)
                                        {
                                            roleBO.Role = Constants.ROLE_AFTER_APPROVE;
                                        }
                                    }
                                    else
                                    {
                                        if (stepNow.GetValueOrDefault() == flow.Step)
                                        {
                                            roleBO.Role = Constants.ROLE_EDIT + (flow.WorkType - 1);
                                            roleBO.PL = flow.PLNumber;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                dicRole[objectID] = roleBO;
            }
            return dicRole;
        }
    }
}