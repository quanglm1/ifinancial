﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using IInterface.Common;
using IInterface.Models;
using Microsoft.Extensions.Caching.Memory;

namespace IInterface.Business
{
    public class CaptchaBusiness
    {
        public static IMemoryCache MemoryCache = new MemoryCache(new MemoryCacheOptions());
        private const int CAPTCHA_TIME_CACHE = 5;
        public static FileDownloadForm GetCaptcha()
        {
            FileDownloadForm form = new FileDownloadForm();
            //Create Captcha string
            string CaptCha =Utils.RandomCaptcha(6);
            //Generate captcha image
            BitmapGenerator GenerateImageClass = new BitmapGenerator(CaptCha, 300, 75);
            //Create Stream to response
            using (MemoryStream memoryStream = new MemoryStream())
            {
                GenerateImageClass.Image.Save(memoryStream, ImageFormat.Jpeg);
                memoryStream.Seek(0, SeekOrigin.Begin);
                // save to file
                form.ContentType = "image/jpeg";
                form.FileName = Guid.NewGuid().ToString();
                while (MemoryCache.Get(form.FileName) != null)
                {
                    form.FileName = Guid.NewGuid().ToString();
                }
                form.EncodedData = Convert.ToBase64String(memoryStream.ToArray());
                MemoryCache.Set(form.FileName, CaptCha, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(CAPTCHA_TIME_CACHE)));
            }

            return form;
        }

        public static bool IsValidCaptcha(CaptchaForm captchaForm)
        {
            if (captchaForm == null || string.IsNullOrWhiteSpace(captchaForm.Key) || string.IsNullOrWhiteSpace(captchaForm.Captcha))
            {
                return false;
            }
            object cacheCaptcha = MemoryCache.Get(captchaForm.Key);
            if (cacheCaptcha == null || !cacheCaptcha.ToString().ToUpper().Equals(captchaForm.Captcha.ToUpper()))
            {
                return false;
            }
            MemoryCache.Remove(captchaForm.Key);
            return true;
        }
    }
}