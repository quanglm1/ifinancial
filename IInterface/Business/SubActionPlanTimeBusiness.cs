﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Business
{
    public class SubActionPlanTimeBusiness
    {
        private Entities context;
        public SubActionPlanTimeBusiness(Entities context)
        {
            this.context = context;
        }
        public void addSubActionPlanTime(int startYear, int endYear, int planTargetActionID)
        {

            if (endYear >= startYear)
            {
                for (int year = startYear; year <= endYear; year++)
                {
                    List<SubActionPlanTime> lstSubActionPlanTime = context.SubActionPlanTime
                                      .Where(x => x.PlanTargetActionID == planTargetActionID && (x.PlanYear == year))
                                      .ToList();
                    if (!lstSubActionPlanTime.Any())
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            SubActionPlanTime subActionPlanTime = new SubActionPlanTime();
                            subActionPlanTime.PlanTime = null;
                            subActionPlanTime.PlanTargetActionID = planTargetActionID;
                            subActionPlanTime.SubActionPlanID = null;
                            subActionPlanTime.PlanYear = year;
                            subActionPlanTime.CreateDate = DateTime.Now;
                            subActionPlanTime.CreateUserID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.Status = Constants.IS_ACTIVE;
                            subActionPlanTime.ModifierID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.UpdateDate = DateTime.Now;
                            context.SubActionPlanTime.Add(subActionPlanTime);
                            context.SaveChanges();
                        }
                    }
                    
                }
            }
        }

        public void addSubActionPlanTime(int startYear, int endYear, int planTargetActionID, int? subActionPlanId = null)
        {

            if (endYear >= startYear)
            {
                for (int year = startYear; year <= endYear; year++)
                {
                    List<SubActionPlanTime> lstSubActionPlanTime = context.SubActionPlanTime
                                      .Where(x => x.SubActionPlanID == subActionPlanId && (x.PlanYear == year))
                                      .ToList();
                    if (!lstSubActionPlanTime.Any())
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            SubActionPlanTime subActionPlanTime = new SubActionPlanTime();
                            subActionPlanTime.PlanTime = null;
                            subActionPlanTime.PlanTargetActionID = planTargetActionID;
                            subActionPlanTime.SubActionPlanID = subActionPlanId;
                            subActionPlanTime.PlanYear = year;
                            subActionPlanTime.CreateDate = DateTime.Now;
                            subActionPlanTime.CreateUserID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.Status = Constants.IS_ACTIVE;
                            subActionPlanTime.ModifierID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.UpdateDate = DateTime.Now;
                            context.SubActionPlanTime.Add(subActionPlanTime);
                            context.SaveChanges();
                        }
                    }

                }
            }
        }
    }
}