﻿using IInterface.Models;
using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IInterface.Common.CommonBO;

namespace IInterface.Business
{
    public class FinancialReportBusiness
    {

        public static int GET_TOTAL = 1;
        public static int GET_IN_QUARTER = 2;
        public static int GET_TO_QUARTER = 3;
        private Entities context;
        public FinancialReportBusiness(Entities context)
        {
            this.context = context;
        }
        public double GetTotalSettleMoney(FinancialReport report, int type)
        {
            if (report == null)
            {
                return 0;
            }
            int year = report.Year;
            int quarter = 4;
            if (report.Quarter.GetValueOrDefault() > 0)
            {
                quarter = report.Quarter.GetValueOrDefault();
            }
            if (type == GET_IN_QUARTER)
            {
                return context.SettleSynthesisCost.Where(x => context.SettleSynthesis.Any(s => s.PlanID == report.PlanID && s.SettleSynthesisID == x.SettleSynthesisID
                                && s.Year == year && s.Quarter == quarter) && x.PlanTargetActionID == null)
                                .Select(x => x.SetttleMoney)
                                .DefaultIfEmpty(0).Sum();
            }
            if (type == GET_TO_QUARTER)
            {
                return context.SettleSynthesisCost.Where(x => context.SettleSynthesis.Any(s => s.PlanID == report.PlanID && s.SettleSynthesisID == x.SettleSynthesisID
                                && (s.Year == year && s.Quarter < quarter)) && x.PlanTargetActionID == null)
                                .Select(x => x.SetttleMoney)
                                .DefaultIfEmpty(0).Sum();
            }
            return context.SettleSynthesisCost.Where(x => context.SettleSynthesis.Any(s => s.PlanID == report.PlanID && s.SettleSynthesisID == x.SettleSynthesisID
                                && s.Year == year && s.Quarter <= quarter) && x.PlanTargetActionID == null)
                                .Select(x => x.SetttleMoney)
                                .DefaultIfEmpty(0).Sum();
        }

        public List<FinancialReportDetailModel> GetListAction(int planTargetActionID, int planTargetID, FinancialReport entityFinancialReport)
        {
            PlanTarget entityPlanTarget = this.context.PlanTarget.Where(x => x.PlanTargetID == planTargetID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entityPlanTarget == null)
            {
                throw new BusinessException("Không tồn tại hoạt động, vui lòng kiểm tra lại");
            }
            List<FinancialReportDetailModel> listAction = (from pt in this.context.PlanTarget
                                                           join pta in this.context.PlanTargetAction.Where(x => planTargetActionID == 0 || x.PlanTargetActionID == planTargetActionID)
                                                           on pt.PlanTargetID equals pta.PlanTargetID into ptag
                                                           from ptaj in ptag.DefaultIfEmpty()
                                                           join s in this.context.SubActionPlan on ptaj.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                           from sj in sg.DefaultIfEmpty()
                                                           join a in this.context.MainAction on new { Status = Constants.IS_ACTIVE, SubActionID = sj.SubActionID.Value } equals new { Status = a.Status.Value, SubActionID = a.MainActionID } into ag
                                                           from aj in ag.DefaultIfEmpty()
                                                           where pt.PlanTargetID == entityPlanTarget.PlanTargetID && ptaj != null
                                                           group sj by new { pt.PlanTargetID, pt.TargetOrder, pt.PlanTargetTitle, pt.PlanTargetContent, ptaj.PlanTargetActionID, ptaj.ActionOrder, ptaj.ActionContent, ptaj.ActionCode, ptaj.TotalBudget } into g
                                                           orderby g.Key.TargetOrder, g.Key.ActionOrder
                                                           select new FinancialReportDetailModel
                                                           {
                                                               PlanTargetActionID = g.Key.PlanTargetActionID,
                                                               PlanTargetActionName = g.Key.ActionContent,
                                                               PlanTargetActionCode = g.Key.ActionCode,
                                                               PlanTargetID = g.Key.PlanTargetID,
                                                               TargetName = g.Key.PlanTargetContent,
                                                               TargetCode = g.Key.PlanTargetTitle,
                                                               PlanMoney = context.SubActionPlanCost.Where(sc => sc.PlanTargetID == planTargetID && sc.PlanTargetActionID == g.Key.PlanTargetActionID
                                                                   && sc.SubActionPlanID == null && sc.SubActionSpendItemID == null && sc.PlanYear == entityFinancialReport.Year).Select(sc => sc.TotalPrice).Sum(),
                                                               ListSubAction = g.Where(x => x != null && x.ParentID == null)
                                                               .OrderBy(x => x.SubActionOrder)
                                                               .Select(x => new SubActionPlanModel
                                                               {
                                                                   PlanTargetActionID = x.PlanTargetActionID,
                                                                   SubActionPlanID = x.SubActionPlanID,
                                                                   SubActionCode = x.SubActionCode,
                                                                   SubActionContent = x.SubActionContent,
                                                                   TotalBudget = context.SubActionPlanCost.Where(sc => sc.PlanTargetActionID == x.PlanTargetActionID
                                                                   && sc.SubActionPlanID == x.SubActionPlanID && sc.SubActionSpendItemID == null && sc.PlanYear == entityFinancialReport.Year).Select(sc => sc.TotalPrice).Sum()
                                                               }).ToList()
                                                           }).ToList();

            // Lay du lieu da nhap
            List<FinancialReportDetail> listDetail = (from a in this.context.FinancialReportDetail
                                                      join t in this.context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                                                      where a.FinancialReportID == entityFinancialReport.FinancialReportID && t.PlanTargetID == planTargetID
                                                      select a).ToList();

            // Tat ca SubAction
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlan.Where(x => this.context.PlanTargetAction.Any(p => p.PlanTargetID == planTargetActionID
            && p.PlanTargetActionID == x.PlanTargetActionID && (planTargetActionID == 0 || p.PlanTargetActionID == planTargetActionID)))
            .OrderBy(x => x.SubActionOrder)
            .Select(x => new SubActionPlanModel
            {
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                ParentID = x.ParentID,
                TotalBudget = context.SubActionPlanCost.Where(sc => sc.PlanTargetActionID == x.PlanTargetActionID
                                                                   && sc.SubActionPlanID == x.SubActionPlanID && sc.SubActionSpendItemID == null
                                                                   && sc.PlanYear == entityFinancialReport.Year).Select(sc => sc.TotalPrice).Sum()
            }).ToList();

            // Lay du lieu luy ke giai ngan toi dau quy - dau nam
            var quarter = entityFinancialReport.Quarter.GetValueOrDefault() > 0 ? entityFinancialReport.Quarter.GetValueOrDefault() : 4;
            List<SettleSynthesisCost> listDetailBefore = (from a in this.context.SettleSynthesisCost
                                                          join r in this.context.SettleSynthesis on a.SettleSynthesisID equals r.SettleSynthesisID
                                                          join t in this.context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                                                          where t.PlanID == entityFinancialReport.PlanID && t.PlanTargetID == planTargetID
                                                          && a.SubActionSpendItemID == null
                                                          && r.Year == entityFinancialReport.Year && r.Quarter <= quarter
                                                          select a).ToList();
            var nowSettle = context.SettleSynthesis.Where(x => x.PlanID == entityFinancialReport.PlanID && x.Year == entityFinancialReport.Year && x.Quarter == quarter).FirstOrDefault();
            if (nowSettle == null)
            {
                nowSettle = new SettleSynthesis
                {
                    SettleSynthesisID = -1
                };
            }
            // Lay thong tin ngan sach duoc duyet

            double sumPlanTargetMoney = 0;
            double sumDisbMoney = 0;
            bool isYearReport = entityFinancialReport.Quarter.GetValueOrDefault() <= 0;
            foreach (FinancialReportDetailModel action in listAction)
            {
                sumPlanTargetMoney += action.PlanMoney.GetValueOrDefault();
                FinancialReportDetail detail = listDetail.Where(x => x.PlanTargetActionID == action.PlanTargetActionID
                && x.SubPlanActionID == null).FirstOrDefault();

                var actionsDetail = listDetailBefore.Where(x => x.PlanTargetActionID == action.PlanTargetActionID && x.SubPlanActionID == null).ToList();
                var detailToQuarter = 0.0;
                if (isYearReport)
                {
                    detailToQuarter = actionsDetail.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                    action.DisbMoney = detailToQuarter;
                }
                else
                {
                    detailToQuarter = actionsDetail.Where(x => x.SettleSynthesisID != nowSettle.SettleSynthesisID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                    action.DisbMoney = actionsDetail.Where(x => x.SettleSynthesisID == nowSettle.SettleSynthesisID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                }
                action.DisbMoneyToQuarter = detailToQuarter;
                if (action.ListSubAction != null && action.ListSubAction.Any())
                {
                    foreach (SubActionPlanModel sub in action.ListSubAction)
                    {
                        detail = listDetail.Where(x => x.PlanTargetActionID == sub.PlanTargetActionID
                                                && x.SubPlanActionID == sub.SubActionPlanID).FirstOrDefault();
                        if (entityFinancialReport.Quarter == null && detail == null)
                        {
                            sumDisbMoney += sub.DisbMoney.GetValueOrDefault();
                        }
                        this.AddSubAction(sub, listSubAction, listDetailBefore, listDetail, nowSettle, isYearReport);
                    }
                }
            }
            FinancialReportDetail targetDetail = listDetail.Where(x => x.PlanTargetID == entityPlanTarget.PlanTargetID
                                               && x.PlanTargetActionID == null
                                               && x.SubPlanActionID == null).FirstOrDefault();

            var detailTargetToQuarter = 0.0;
            var detailTargetInQuarter = 0.0;
            if (isYearReport)
            {
                detailTargetToQuarter = listDetailBefore.Where(x => x.PlanTargetID == entityPlanTarget.PlanTargetID
                       && x.PlanTargetActionID == null && x.SubPlanActionID == null).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                detailTargetInQuarter = detailTargetToQuarter;
            }
            else
            {
                detailTargetToQuarter = listDetailBefore.Where(x => x.PlanTargetID == entityPlanTarget.PlanTargetID && x.SettleSynthesisID != nowSettle.SettleSynthesisID
                       && x.PlanTargetActionID == null && x.SubPlanActionID == null).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                detailTargetInQuarter = listDetailBefore.Where(x => x.PlanTargetID == entityPlanTarget.PlanTargetID && x.SettleSynthesisID == nowSettle.SettleSynthesisID
                        && x.PlanTargetActionID == null && x.SubPlanActionID == null).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
            }
            FinancialReportDetailModel target = new FinancialReportDetailModel
            {
                IsTarget = true,
                PlanTargetID = entityPlanTarget.PlanTargetID,
                TargetCode = entityPlanTarget.TargetCode,
                TargetName = entityPlanTarget.PlanTargetContent,
                DisbMoneyToQuarter = detailTargetToQuarter,
                DisbMoney = detailTargetInQuarter,
                PlanMoney = sumPlanTargetMoney
            };
            listAction.Insert(0, target);
            return listAction;
        }

        public FinancialReportInfoModel GetFinancialReportInfo(int id)
        {
            var entity = (from s in this.context.FinancialReport
                          join p in this.context.Plan on s.PlanID equals p.PlanID
                          join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                          join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                          where s.FinancialReportID == id && s.Status != Constants.IS_NOT_ACTIVE
                          select new
                          {
                              s.FinancialReportID,
                              s.Year,
                              s.Quarter,
                              s.PlanID,
                              s.DepartmentID,
                              p.PeriodID,
                              s.Status,
                              pr.PeriodName,
                              d.DepartmentName,
                              s.SendDate
                          }).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            FinancialReportInfoModel model = new FinancialReportInfoModel();
            model.FinancialReportID = entity.FinancialReportID;
            model.Year = entity.Year;
            model.Quarter = entity.Quarter;
            model.PlanID = entity.PlanID;
            model.PlanName = entity.PeriodName;
            model.Status = entity.Status;
            model.DeptName = entity.DepartmentName;
            var report = context.FinancialReport.Find(entity.FinancialReportID);
            model.MoneyInQuarter = GetTotalSettleMoney(report, GET_IN_QUARTER);
            model.MoneyToQuarter = GetTotalSettleMoney(report, GET_IN_QUARTER);
            model.SendDate = entity.SendDate;
            model.PeriodID = entity.PeriodID;
            model.DeptID = entity.DepartmentID.GetValueOrDefault();
            // Tinh tong tien theo ke hoach
            var planMoney = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.Status == 1)
                .Sum(x => x.TotalBuget == null ? 0 : x.TotalBuget);
            return model;
        }

        public Paginate<FinancialReportInfoModel> GetListFinancialReport(int userID, int periodID, int deptID, int status, int quarter, int year, int page)
        {
            List<int> listYear = new List<int>();
            List<FinancialReportInfoModel> listReport =
                (from s in this.context.FinancialReport
                 join pl in this.context.Plan on s.PlanID equals pl.PlanID
                 join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                 join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                 where (periodID == 0 || pr.PeriodID == periodID) && (deptID == 0 || d.DepartmentID == deptID) && d.Status != Constants.IS_NOT_ACTIVE
                 && pl.Status != Constants.IS_NOT_ACTIVE && pr.Status != Constants.IS_NOT_ACTIVE && s.Status != Constants.IS_NOT_ACTIVE
                 && (year == 0 || s.Year == year) && (quarter == 0 || s.Quarter == quarter)
                && (status == 0 || (status == Constants.REPORT_NEW)
                || (status == Constants.REPORT_SENT && s.Status >= Constants.PLAN_NEXT_LEVER))
                 select new FinancialReportInfoModel
                 {
                     CreateDate = s.CreateDate,
                     Creator = s.Creator,
                     PlanID = s.PlanID,
                     PeriodID = pl.PeriodID,
                     PlanName = pr.PeriodName,
                     Quarter = s.Quarter,
                     Year = s.Year,
                     FinancialReportID = s.FinancialReportID,
                     Type = s.Type,
                     Status = s.Status,
                     DeptID = d.DepartmentID,
                     DeptName = d.DepartmentName,
                     SendDate = s.SendDate,
                     IsHasReview = this.context.ReportReview.Any(x => x.ReportID == s.FinancialReportID && x.ReportType == Constants.REPORT_TYPE_FINANCIAL)
                 }).ToList();

            Period period = context.Period.Find(periodID);
            if (year == 0)
            {
                for (int i = period.FromDate.Year; i <= period.ToDate.Year; i++)
                {
                    listYear.Add(i);
                }
            }
            else
            {
                listYear.Add(year);
            }

            List<int> listQuarter = new List<int> { 0, 1, 2, 3, 4 };
            if (quarter > 0)
            {
                listQuarter.Clear();
                listQuarter.Add(quarter);
            }
            DateTime now = DateTime.Now;
            int quarterNow = (now.Month % 3 == 0) ? now.Month / 3 : (now.Month / 3) + 1;

            List<Department> listDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE && (deptID == 0 || x.DepartmentID == deptID)).ToList();
            Dictionary<int, ObjectRoleBO> dicRoleDept = new FlowProcessBusiness(this.context).GetRoleForDept(userID, listDept, Constants.COMMENT_TYPE_FINANCIAL);
            listDept.RemoveAll(x => !dicRoleDept.ContainsKey(x.DepartmentID) || dicRoleDept[x.DepartmentID].Role == Constants.ROLE_NO_VIEW);
            listReport = (from q in listQuarter
                          join y in listYear on 0 equals 0
                          join d in listDept on 0 equals 0
                          join l in listReport on new { DeptID = d.DepartmentID, Quarter = q, Year = y } equals new { DeptID = l.DeptID, Quarter = l.Quarter.GetValueOrDefault(), Year = l.Year } into lj
                          from s in lj.DefaultIfEmpty()
                          where (status == 0 || (status == Constants.REPORT_NEW && (s == null || s.Status == Constants.PLAN_NEW))
                || (status == Constants.REPORT_SENT && s != null && s.Status >= Constants.PLAN_NEXT_LEVER))
                && (y < now.Year || (y == now.Year && q <= quarterNow))
                          select new FinancialReportInfoModel
                          {
                              CreateDate = s == null ? null : s.CreateDate,
                              PeriodID = periodID,
                              PlanID = s == null ? 0 : s.PlanID,
                              PlanName = s == null ? period.PeriodName : s.PlanName,
                              Quarter = q == 0 ? null : (int?)q,
                              Year = y,
                              FinancialReportID = s == null ? 0 : s.FinancialReportID,
                              Type = s == null ? 0 : s.Type,
                              Status = s == null ? 0 : s.Status,
                              DeptID = s == null ? d.DepartmentID : s.DeptID,
                              DeptName = s == null ? d.DepartmentName : s.DeptName,
                              SendDate = s == null ? null : s.SendDate,
                              IsHasReview = s == null ? false : s.IsHasReview
                          }).ToList();
            // Lay quyen
            List<int> listReportID = listReport.Where(x => x.FinancialReportID > 0).Select(x => x.FinancialReportID).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.context).GetRoleForObject(userID, listReportID, Constants.COMMENT_TYPE_FINANCIAL);

            listReport.RemoveAll(x => x.FinancialReportID > 0 && dicRole[x.FinancialReportID].Role == Constants.ROLE_NO_VIEW);
            listReport.ForEach(x =>
            {
                if (x.FinancialReportID > 0)
                {
                    x.Role = dicRole[x.FinancialReportID].Role;
                }
                else
                {
                    x.Role = dicRoleDept[x.DeptID].Role;
                }
            });
            Paginate<FinancialReportInfoModel> paginate = new Paginate<FinancialReportInfoModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = listReport.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter == null || x.Quarter == 0 ? 100 : x.Quarter.Value)
                .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = listReport.Count;
            return paginate;
        }

        public void InitFinancialReportFromSettle(SettleSynthesis settleReport)
        {
            List<SettleSynthesisCost> listSettleCost = context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == settleReport.SettleSynthesisID
            && x.SubActionSpendItemID == null).ToList();
            List<FinancialReport> listFinancialReport = this.CreateFinancialReport(settleReport);
            var qReport = listFinancialReport[0];
            List<FinancialReportDetail> listFinancialReportCost = context.FinancialReportDetail.Where(x => x.FinancialReportID == qReport.FinancialReportID).ToList();
            List<FinancialReportDetail> listFinancialDetailBefore = (from a in this.context.FinancialReportDetail
                                                                     join r in this.context.FinancialReport on a.FinancialReportID equals r.FinancialReportID
                                                                     join t in this.context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                                                                     where t.PlanID == settleReport.PlanID
                                                                     && (r.Year < settleReport.Year || (r.Year == settleReport.Year && r.Quarter < settleReport.Quarter))
                                                                     select a).ToList();
            List<FinancialReportDetail> listFinancialQuarterCost = new List<FinancialReportDetail>();
            foreach (SettleSynthesisCost settleCost in listSettleCost)
            {
                FinancialReportDetail financialCost = listFinancialReportCost.Where(x => x.PlanTargetID == settleCost.PlanTargetID && x.PlanTargetActionID == settleCost.PlanTargetActionID && x.SubPlanActionID == settleCost.SubPlanActionID).FirstOrDefault();
                if (financialCost == null)
                {
                    financialCost = new FinancialReportDetail();
                    financialCost.FinancialReportID = qReport.FinancialReportID;
                    financialCost.PlanTargetID = settleCost.PlanTargetID.GetValueOrDefault();
                    financialCost.PlanTargetActionID = settleCost.PlanTargetActionID;
                    financialCost.SubPlanActionID = settleCost.SubPlanActionID;
                }
                if (financialCost.FinancialReportDetailID == 0)
                {
                    context.FinancialReportDetail.Add(financialCost);
                }
                listFinancialQuarterCost.Add(financialCost);
            }
            #region bao cao tai chinh nam
            if (listFinancialReport.Count > 1)
            {
                var yReport = listFinancialReport[1];
                listFinancialReportCost = context.FinancialReportDetail.Where(x => x.FinancialReportID == yReport.FinancialReportID).ToList();
                foreach (FinancialReportDetail quarterCost in listFinancialQuarterCost)
                {
                    FinancialReportDetail financialCost = listFinancialReportCost.Where(x => x.PlanTargetID == quarterCost.PlanTargetID && x.PlanTargetActionID == quarterCost.PlanTargetActionID && x.SubPlanActionID == quarterCost.SubPlanActionID).FirstOrDefault();
                    if (financialCost == null)
                    {
                        financialCost = new FinancialReportDetail();
                        financialCost.FinancialReportID = yReport.FinancialReportID;
                        financialCost.PlanTargetID = quarterCost.PlanTargetID;
                        financialCost.PlanTargetActionID = quarterCost.PlanTargetActionID;
                        financialCost.SubPlanActionID = quarterCost.SubPlanActionID;
                    }
                }
            }
            #endregion
        }

        private List<FinancialReport> CreateFinancialReport(SettleSynthesis settleReport)
        {
            List<FinancialReport> listFinancialReport = new List<FinancialReport>();
            FinancialReport financialReport = context.FinancialReport.Where(x => x.PlanID == settleReport.PlanID && x.Year == settleReport.Year && x.Quarter == settleReport.Quarter).FirstOrDefault();
            if (financialReport == null)
            {
                financialReport = new FinancialReport();
                financialReport.CreateDate = DateTime.Now;
                financialReport.Creator = settleReport.Creator;
                financialReport.DepartmentID = settleReport.DepartmentID;
                financialReport.PlanID = settleReport.PlanID;
                financialReport.Quarter = settleReport.Quarter;
                financialReport.Status = Constants.PLAN_NEW;
                financialReport.Type = settleReport.Type;
                financialReport.Year = settleReport.Year;
                context.FinancialReport.Add(financialReport);
                context.SaveChanges();
            }
            listFinancialReport.Add(financialReport);
            if (settleReport.Quarter.GetValueOrDefault() == 4)
            {
                FinancialReport financialYearReport = context.FinancialReport.Where(x => x.PlanID == settleReport.PlanID && x.Year == settleReport.Year && x.Quarter == null).FirstOrDefault();
                if (financialYearReport == null)
                {
                    financialYearReport = new FinancialReport();
                    financialYearReport.CreateDate = DateTime.Now;
                    financialYearReport.Creator = settleReport.Creator;
                    financialYearReport.DepartmentID = settleReport.DepartmentID;
                    financialYearReport.PlanID = settleReport.PlanID;
                    financialYearReport.Status = Constants.PLAN_NEW;
                    financialYearReport.Type = settleReport.Type;
                    financialYearReport.Year = settleReport.Year;
                    context.FinancialReport.Add(financialYearReport);
                    context.SaveChanges();
                }
                listFinancialReport.Add(financialYearReport);
            }
            return listFinancialReport;
        }


        private void AddSubAction(SubActionPlanModel subAction, List<SubActionPlanModel> listSubAction,
            List<SettleSynthesisCost> listDetailBefore, List<FinancialReportDetail> listDetail, SettleSynthesis nowSettle, bool isYearReport)
        {
            FinancialReportDetail detail = listDetail.Where(x => x.PlanTargetActionID == subAction.PlanTargetActionID
                                                && x.SubPlanActionID == subAction.SubActionPlanID).FirstOrDefault();
            var detailSubToQuarter = 0.0;
            var detailInQuarter = 0.0;
            if (isYearReport)
            {
                detailSubToQuarter = listDetailBefore.Where(x => x.PlanTargetActionID == subAction.PlanTargetActionID
                    && x.SubPlanActionID == subAction.SubActionPlanID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                detailInQuarter = detailSubToQuarter;
            }
            else
            {
                detailSubToQuarter = listDetailBefore.Where(x => x.PlanTargetActionID == subAction.PlanTargetActionID
                    && x.SubPlanActionID == subAction.SubActionPlanID && x.SettleSynthesisID != nowSettle.SettleSynthesisID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                detailInQuarter = listDetailBefore.Where(x => x.PlanTargetActionID == subAction.PlanTargetActionID
                && x.SubPlanActionID == subAction.SubActionPlanID && x.SettleSynthesisID == nowSettle.SettleSynthesisID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
            }
            subAction.DisbMoneyToQuarter = detailSubToQuarter;
            subAction.DisbMoney = detailInQuarter;
            List<SubActionPlanModel> listChild = listSubAction.Where(x => x.ParentID == subAction.SubActionPlanID).ToList();
            if (listChild != null && listChild.Any())
            {
                foreach (SubActionPlanModel child in listChild)
                {
                    this.AddSubAction(child, listSubAction, listDetailBefore, listDetail, nowSettle, isYearReport);
                }
                subAction.ListChildSubActionPlan = listChild;
            }
        }
    }
}