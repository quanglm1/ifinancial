﻿using ClosedXML.Excel;
using IInterface.Common;
using IInterface.Common.CommonExcel;
using IInterface.Models;
using IModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

namespace IInterface.Business
{
    public class CostEstimateBusiness
    {
        private Entities context;
        public CostEstimateBusiness(Entities context)
        {
            this.context = context;
        }

        public List<DeptCostInfo> GetListDeptCost(int periodID, int? deptType = null)
        {
            List<DeptCostInfo> listCost = new List<DeptCostInfo>();
            if (deptType == null)
            {
                listCost.AddRange(this.GetListRootDeptCost(periodID));
            }
            var query = from pt in this.context.PlanTarget
                        join su in this.context.SubActionPlanCost on pt.PlanTargetID equals su.PlanTargetID
                        join ta in this.context.Target on pt.TargetID equals ta.TargetID
                        join a in this.context.MainAction on ta.MainActionID equals a.MainActionID
                        join t in this.context.Target on a.TargetID equals t.TargetID
                        join pl in this.context.Plan on pt.PlanID equals pl.PlanID
                        join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                        join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                        where pl.PeriodID == periodID && (deptType == null || d.DepartmentType == deptType) && pr.Status == Constants.IS_ACTIVE
                        && pl.Status == Constants.PLAN_APPROVED && su.PlanTargetActionID != null && su.SubActionPlanID == null && su.SubActionSpendItemID == null
                        && su.PlanYear != null && a.Type == Constants.MAIN_TYPE && ta.Type == Constants.SUB_TYPE
                        && this.context.MappingActionDepartment.Any(x => x.MainActionID == a.MainActionID && (x.DepartmentType == d.DepartmentType))
                        group su by new
                        {
                            a.MainActionID,
                            a.ActionCode,
                            a.ActionContent,
                            t.TargetID,
                            t.TargetTitle,
                            t.TargetContent,
                            d.DepartmentID,
                            d.DepartmentName,
                            d.DepartmentType,
                            su.PlanYear
                        } into g
                        select new DeptCostInfo
                        {
                            ActionID = g.Key.MainActionID,
                            ActionName = g.Key.ActionContent,
                            ActionCode = g.Key.ActionCode,
                            Cost = g.Where(x => x.TotalPrice != null).Sum(x => x.TotalPrice.Value),
                            DeptID = g.Key.DepartmentID,
                            DeptName = g.Key.DepartmentName,
                            DeptType = g.Key.DepartmentType.Value,
                            TargetID = g.Key.TargetID,
                            TargetName = g.Key.TargetContent,
                            TargetTitle = g.Key.TargetTitle,
                            Year = g.Key.PlanYear.Value
                        };
            listCost.AddRange(query.ToList());
            return listCost;
        }

        public List<DeptCostInfo> GetListRootDeptCost(int periodID)
        {
            var query = from pta in this.context.PlanTargetAction
                        join su in this.context.SubActionPlanCost on pta.PlanTargetActionID equals su.PlanTargetActionID
                        join pt in this.context.PlanTarget on pta.PlanTargetID equals pt.PlanTargetID
                        join t in this.context.Target on pt.TargetID equals t.TargetID
                        join a in this.context.MainAction on pta.MainActionID equals a.MainActionID
                        join pl in this.context.Plan on pt.PlanID equals pl.PlanID
                        join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                        join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                        where pl.PeriodID == periodID && d.DepartmentType == Constants.DEPT_TYPE_PBQUY && pr.Status == Constants.IS_ACTIVE
                        && pl.Status == Constants.PLAN_APPROVED && su.SubActionPlanID == null && su.PlanYear != null
                        && a.Type == Constants.MAIN_TYPE && t.Type == Constants.MAIN_TYPE
                        group su by new
                        {
                            a.MainActionID,
                            a.ActionCode,
                            a.ActionContent,
                            t.TargetID,
                            t.TargetTitle,
                            t.TargetContent,
                            d.DepartmentID,
                            d.DepartmentName,
                            d.DepartmentType,
                            su.PlanYear
                        } into g
                        select new DeptCostInfo
                        {
                            ActionID = g.Key.MainActionID,
                            ActionName = g.Key.ActionContent,
                            ActionCode = g.Key.ActionCode,
                            Cost = g.Where(x => x.TotalPrice != null).Select(x => x.TotalPrice.Value).DefaultIfEmpty(0).Sum(),
                            DeptID = g.Key.DepartmentID,
                            DeptName = g.Key.DepartmentName,
                            DeptType = g.Key.DepartmentType.Value,
                            TargetID = g.Key.TargetID,
                            TargetName = g.Key.TargetContent,
                            TargetTitle = g.Key.TargetTitle,
                            Year = g.Key.PlanYear.Value
                        };
            return query.ToList();
        }

        public byte[] SetExportData(Period period, Plan plan, string templateFilePath)
        {
            List<int> listDeptType = new List<int>();
            listDeptType.Add(Constants.DEPT_TYPE_BO_NGANH);
            listDeptType.Add(Constants.DEPT_TYPE_TINH_TP);
            listDeptType.Add(Constants.DEPT_TYPE_TP_DU_LICH);
            listDeptType.Add(Constants.DEPT_TYPE_BV_CAI_NGHIEN);
            listDeptType.Add(Constants.DEPT_TYPE_SANG_KIEN);

            List<IXVTWorksheet> listSheet = new List<IXVTWorksheet>();

            using (MemoryStream ms = new MemoryStream())
            {
                using (FileStream file = new FileStream(templateFilePath, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);

                    IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                    IXVTWorksheet actionSheet = oBook.GetSheet(1);
                    this.SetDataToFirstSheet(actionSheet, plan);

                    IXVTWorksheet tempActionSheet = oBook.GetSheet(2);
                    IXVTWorksheet tempNoActionSheet = oBook.GetSheet(3);

                    IXVTWorksheet bonganhSheet = oBook.CopySheet(tempActionSheet, "Bo nganh");
                    listSheet.Add(bonganhSheet);

                    IXVTWorksheet tpSheet = oBook.CopySheet(tempActionSheet, "Cac tinh, TP");
                    listSheet.Add(tpSheet);

                    IXVTWorksheet dulichSheet = oBook.CopySheet(tempActionSheet, "TP du lich");
                    listSheet.Add(dulichSheet);

                    IXVTWorksheet caingienSheet = oBook.CopySheet(tempNoActionSheet, "Cai nghien");
                    listSheet.Add(caingienSheet);

                    IXVTWorksheet sangkienSheet = oBook.CopySheet(tempNoActionSheet, "Sang kien");
                    listSheet.Add(sangkienSheet);
                    tempActionSheet.Delete();
                    tempNoActionSheet.Delete();

                    for (int i = 0; i < listDeptType.Count; i++)
                    {
                        this.SetDataToSheet(listSheet[i], listDeptType[i], period);
                    }

                    Stream streamToWrite = oBook.ToStream();
                    byte[] bytesToWrite = new byte[streamToWrite.Length];
                    streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);
                    return bytesToWrite;
                }
            }
        }

        private void SetDataToFirstSheet(IXVTWorksheet sheet, Plan plan)
        {
            Period period = context.Period.Find(plan.PeriodID);
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            int rangeYear = toYear - fromYear;
            DateTime now = DateTime.Now;
            Department rootDept = this.context.Department.Where(x => x.ParentID == null).FirstOrDefault();
            string rootName = rootDept.DepartmentName.ToUpper().Trim();
            sheet.SheetName = "KH " + now.ToString("dd.MM.yyyy");
            sheet.SetCellValue(1, 1, rootName);
            sheet.SetCellValue(3, 1, " KẾ HOẠCH HOẠT ĐỘNG VÀ KINH PHÍ GIAI ĐOẠN " + fromYear + "-" + toYear);
            sheet.GetRange(3, 1, 3, 4 + rangeYear).Range.Merge();
            int lastCol = 5 + rangeYear;
            IXVTRange rangeHeader = sheet.GetRange(5, 3, 6, 3);
            IXVTRange rangeTotal = sheet.GetRange(5, 4, 6, 5);
            IXVTRange rangeYearData = sheet.GetRange(7, 3, 7, 3);
            IXVTRange rangeTotalData = sheet.GetRange(7, 4, 7, 5);
            rangeTotal.CopyTo(sheet.GetRange(5, lastCol - 1, 6, lastCol));
            rangeTotalData.CopyTo(sheet.GetRange(7, lastCol - 1, 7, lastCol));
            sheet.GetRange(5, lastCol - 1, 6, lastCol - 1).Range.Merge();
            sheet.GetRange(5, lastCol, 6, lastCol).Range.Merge();
            for (int i = 0; i <= rangeYear; i++)
            {
                rangeHeader.CopyTo(sheet.GetRange(5, 3 + i, 6, 3 + i));
                rangeYearData.CopyTo(sheet.GetRange(7, 3 + i, 7, 3 + i));
                int year = fromYear + i;
                sheet.SetCellValue(5, 3 + i, "Năm " + year);
                sheet.SetCellValue(6, 3 + i, "Số tiền");
            }
            sheet.SetCellValue(5, 4 + rangeYear, "Tổng ngân sách " + fromYear + "-" + toYear);
            sheet.SetCellValue(5, 5 + rangeYear, "Ghi chú");
            IXVTRange rangeData = sheet.GetRange(7, 1, 7, lastCol);

            List<PlanTargetModel> listData = this.GetListPlanTargetCost(plan.PlanID);
            List<CostEstimateSpendModel> listSpendRoot = this.GetListRootSpend(plan.PlanID);
            Dictionary<string, List<int>> dicPosSum = new Dictionary<string, List<int>>();
            Dictionary<string, int> dicPos = new Dictionary<string, int>();
            int startData = 7;
            List<int> listSumRow = new List<int>();
            for (int i = 0; i < listData.Count; i++)
            {
                listSumRow.Add(startData);
                PlanTargetModel target = listData[i];
                rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                sheet.SetCellValue(startData, 1, Utils.ToRoman(i + 1));
                sheet.SetCellValue(startData, 2, "Mục tiêu " + (i + 1) + ": " + target.PlanTargetContent);
                List<CostEstimateDetailModel> listTargetDetail = target.ListCostEstimateDetail;
                this.SetDetailCostData(sheet, listTargetDetail, period, startData);
                if (startData > 7)
                {
                    sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
                }
                List<PlanTargetActionModel> listTargetAction = target.ListPlanTargetActionModel;
                dicPos["target_" + target.PlanTargetID] = startData;
                dicPosSum["target_" + target.PlanTargetID] = new List<int>();
                startData++;
                if (listTargetAction != null && listTargetAction.Any())
                {
                    foreach (PlanTargetActionModel action in listTargetAction)
                    {
                        rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                        sheet.SetCellValue(startData, 1, action.ActionCode);
                        sheet.SetCellValue(startData, 2, action.ActionContent);
                        List<CostEstimateDetailModel> listActionDetail = action.ListCostEstimateDetail;
                        this.SetDetailCostData(sheet, listActionDetail, period, startData);
                        sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
                        dicPos["targetaction_" + action.PlanTargetActionID] = startData;
                        dicPosSum["target_" + target.PlanTargetID].Add(startData);
                        dicPosSum["targetaction_" + action.PlanTargetActionID] = new List<int>();
                        startData++;
                        List<SubActionPlanModel> listSub = action.ListSubActionPlan;
                        if (listSub != null && listSub.Any())
                        {
                            foreach (SubActionPlanModel sub in listSub)
                            {
                                rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                                sheet.SetCellValue(startData, 1, sub.SubActionCode);
                                sheet.SetCellValue(startData, 2, sub.SubActionContent);
                                List<CostEstimateDetailModel> listSubDetail = sub.ListCostEstimateDetail;
                                this.SetDetailCostData(sheet, listSubDetail, period, startData);
                                dicPos["subaction_" + sub.SubActionPlanID] = startData;
                                dicPosSum["targetaction_" + action.PlanTargetActionID].Add(startData);
                                dicPosSum["subaction_" + sub.SubActionPlanID] = new List<int>();
                                startData++;
                                if (sub.ListCostEstimateSpend != null && sub.ListCostEstimateSpend.Any())
                                {
                                    foreach (CostEstimateSpendModel spend in sub.ListCostEstimateSpend)
                                    {
                                        startData += this.SetSpendCostData(sheet, dicPos, dicPosSum, spend, period, startData, rangeData);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (action.ListCostEstimateSpend != null && action.ListCostEstimateSpend.Any())
                            {
                                foreach (CostEstimateSpendModel spend in action.ListCostEstimateSpend)
                                {
                                    startData += this.SetSpendCostData(sheet, dicPos, dicPosSum, spend, period, startData, rangeData);
                                }
                            }
                        }
                    }
                }
            }
            sheet.SetRangeFont(7, 1, 7, lastCol, "Times New Roman", 12, true, false);
            if (listSpendRoot != null && listSpendRoot.Any())
            {
                for (int i = 0; i < listSpendRoot.Count; i++)
                {
                    CostEstimateSpendModel spend = listSpendRoot[i];
                    dicPosSum["subcost_" + spend.CostEstimateSpendID] = new List<int>();
                    listSumRow.Add(startData);
                    startData += this.SetSpendCostData(sheet, dicPos, dicPosSum, spend, period, startData, rangeData, listData.Count + i + 1);
                }
            }
            rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
            sheet.SetCellValue(startData, 1, "");
            sheet.SetCellValue(startData, lastCol, "");
            sheet.SetCellValue(startData, 2, "Tổng cộng");

            // Thiet lap cong thuc
            List<string> listKey = dicPos.Keys.ToList();
            foreach (string key in listKey)
            {
                if (dicPosSum.ContainsKey(key))
                {
                    List<int> listPosSum = dicPosSum[key];
                    if (listPosSum != null && listPosSum.Any())
                    {
                        int posSum = dicPos[key];
                        for (int i = 0; i <= rangeYear + 1; i++)
                        {
                            string sumColName = Utils.GetExcelColumnName(3 + i);
                            string sum = string.Empty;
                            foreach (int pos in listPosSum)
                            {
                                sum += ", " + sumColName + pos;
                            }
                            sheet.SetCellFormula(posSum, 3 + i, "SUM(" + sum.Substring(2) + ")");
                        }
                    }
                }
            }

            for (int i = 0; i <= rangeYear + 1; i++)
            {
                string sum = "";
                string sumCol = Utils.GetExcelColumnName(3 + i);
                foreach (int row in listSumRow)
                {
                    sum += "," + sumCol + row;
                }
                sheet.SetCellFormula(startData, 3 + i, "SUM(" + sum.Substring(1) + ")");
            }
            sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
            startData++;
            rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
            string totalMoney = sheet.GetValueOfCell(startData - 1, rangeYear + 4).ToString();
            sheet.SetCellValue(startData, 1, Utils.ChuyenSo(totalMoney));
            sheet.SetCellValue(startData, lastCol, "");
            sheet.GetRange(startData, 1, startData, lastCol - 1).Range.Merge();
        }

        private void SetDetailCostData(IXVTWorksheet sheet, List<CostEstimateDetailModel> listCost, Period period, int row)
        {
            string des = string.Empty;
            if (listCost != null && listCost.Any())
            {
                int fromYear = period.FromDate.Year;
                int toYear = period.ToDate.Year;
                int rangeYear = toYear - fromYear;
                for (int i = 0; i <= rangeYear; i++)
                {
                    int year = fromYear + i;
                    CostEstimateDetailModel cost = listCost.Where(x => x.PlanYear == year).FirstOrDefault();
                    if (cost != null)
                    {
                        sheet.SetCellValue(row, 3 + i, cost.TotalMoney.ToString());
                        des = cost.Description;
                    }
                }
                sheet.SetCellFormula(row, 4 + rangeYear, "SUM(" + Utils.GetExcelColumnName(3) + row + ":" + Utils.GetExcelColumnName(3 + rangeYear) + row + ")");
                sheet.SetCellValue(row, 5 + rangeYear, des);
            }
        }

        private int SetSpendCostData(IXVTWorksheet sheet, Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum,
            CostEstimateSpendModel spend, Period period, int row, IXVTRange tempRange, int roman = 0)
        {
            tempRange.CopyTo(sheet.GetRange(row, 1, row, tempRange.ColumnsCount));
            int totalRow = 0;
            if (roman > 0)
            {
                sheet.SetCellValue(row, 1, Utils.ToRoman(roman));
            }
            else
            {
                sheet.SetCellValue(row, 1, spend.SpendItemTitle);
            }
            #region Thiet lap vi tri
            dicPos["subcost_" + spend.CostEstimateSpendID] = row;
            if (spend.PlanTargetID != null)
            {
                if (spend.ParentID != null)
                {
                    if (dicPosSum.ContainsKey("subcost_" + spend.ParentID.Value))
                    {
                        dicPosSum["subcost_" + spend.ParentID.Value].Add(row);
                    }
                    else
                    {
                        dicPosSum["subcost_" + spend.ParentID.Value] = new List<int> { row };
                    }
                }
                else if (spend.SubActionPlanID != null)
                {
                    dicPosSum["subaction_" + spend.SubActionPlanID.Value].Add(row);
                }
                else if (spend.PlanTargetActionID != null)
                {
                    dicPosSum["targetaction_" + spend.PlanTargetActionID.Value].Add(row);
                }
            }
            #endregion
            sheet.SetCellValue(row, 2, spend.SpendContent);
            List<CostEstimateDetailModel> listDetail = spend.ListDetail;
            this.SetDetailCostData(sheet, listDetail, period, row);
            if (spend.PlanTargetID == null && spend.ParentID == null)
            {
                sheet.SetRangeFont(row, 1, row, tempRange.ColumnsCount, "Times New Roman", 12, true, false);
            }
            row++;
            if (spend.ListChildSpend != null && spend.ListChildSpend.Any())
            {
                foreach (CostEstimateSpendModel child in spend.ListChildSpend)
                {
                    totalRow += this.SetSpendCostData(sheet, dicPos, dicPosSum, child, period, row++, tempRange);
                    row += totalRow;
                }
            }
            else
            {
                totalRow++;
            }
            return totalRow;
        }

        private void SetDataToSheet(IXVTWorksheet sheet, int deptType, Period period)
        {
            List<DeptCostInfo> listCost = this.GetListDeptCost(period.PeriodID, deptType);
            Department rootDept = this.context.Department.Where(x => x.ParentID == null).FirstOrDefault();
            string rootName = rootDept.DepartmentName.ToUpper().Trim();
            DeptTypeName deptTypeName = this.GetDeptName(deptType);
            sheet.SetCellValue(1, 1, rootName);

            //sheet.Copy
            if (deptType == Constants.DEPT_TYPE_BO_NGANH || deptType == Constants.DEPT_TYPE_TINH_TP || deptType == Constants.DEPT_TYPE_TP_DU_LICH)
            {
                this.SetDataWithAction(sheet, deptTypeName, period, listCost);
            }
            else
            {
                this.SetDataNoAction(sheet, deptTypeName, period, listCost);
            }
        }

        private void SetDataWithAction(IXVTWorksheet sheet, DeptTypeName name, Period period, List<DeptCostInfo> listCost)
        {
            sheet.SheetName = name.DeptNameNoSign;
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            int rangeYear = toYear - fromYear;
            List<ActionSynInfo> listTarget = this.GetListTarget(name.DeptType);
            var listAction = listTarget.Select(x => new { x.MainActionID, x.ActionCode, x.ActionContent }).Distinct().ToList();
            List<Department> listDept = this.context.Department.Where(x => x.Status == Constants.IS_ACTIVE && x.DepartmentType == name.DeptType)
                .OrderBy(x => x.DepartmentName).ToList();
            int countAction = listAction.Count;
            int lastCol = 2 + ((rangeYear + 1) * (countAction + 1));
            sheet.SetCellValue(2, lastCol - 1, "Phụ lục 0" + (name.IndexSheet + 1));
            sheet.SetRangeFont(2, 1, 2, lastCol, "Times New Roman", 12, false, false);
            sheet.SetCellValue(3, 1, "Kính phí các " + name.DeptNameSign + " đề xuất giai đoạn " + fromYear + "-" + toYear);
            sheet.SetCellValue(4, lastCol - 1, "Đơn vị: triệu đồng");
            sheet.SetRangeFont(4, 1, 4, lastCol, "Times New Roman", 12, false, true);
            sheet.GetRange(2, lastCol - 1, 2, lastCol).Range.Merge();
            sheet.GetRange(2, lastCol - 1, 2, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Right;
            sheet.GetRange(3, 1, 3, lastCol).Range.Merge();
            sheet.GetRange(4, lastCol - 1, 4, lastCol).Range.Merge();
            sheet.GetRange(4, lastCol - 1, 4, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Right;
            sheet.GetRange(3, 1, 3, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;
            IXVTRange rangeHeader = sheet.GetRange(5, 3, 6, 4);
            IXVTRange rangeData = sheet.GetRange(7, 3, 7, 4);
            IXVTRange rangeTotal = sheet.GetRange(8, 3, 8, 4);
            for (int i = 0; i <= rangeYear; i++)
            {
                int year = fromYear + i;
                rangeHeader.CopyTo(sheet.GetRange(5, 3 + (i * (countAction + 1)), 6, 2 + ((i + 1) * (countAction + 1))));
                rangeData.CopyTo(sheet.GetRange(7, 3 + (i * (countAction + 1)), 7, 2 + ((i + 1) * (countAction + 1))));
                rangeTotal.CopyTo(sheet.GetRange(8, 3 + (i * (countAction + 1)), 8, 2 + ((i + 1) * (countAction + 1))));
                sheet.SetCellValue(5, 3 + (i * (countAction + 1)), "Năm " + year);
                for (int j = 0; j < countAction; j++)
                {
                    var action = listAction[j];
                    int pos = 3 + (i * (countAction + 1)) + j;
                    sheet.SetCellValue(6, pos, action.ActionContent + " (" + action.ActionCode + ")");
                }
                sheet.SetCellValue(6, 2 + ((i + 1) * (countAction + 1)), "Tổng cộng");
            }
            sheet.GetRange(8, 1, 8, lastCol).CopyTo(sheet.GetRange(7 + listDept.Count, 1, 7 + listDept.Count, lastCol));
            rangeData = sheet.GetRange(7, 1, 7, lastCol);
            int startRow = 7;
            List<int> listSumCol = new List<int>();
            foreach (Department dept in listDept)
            {
                rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, lastCol));
                sheet.SetCellValue(startRow, 1, (startRow - 6).ToString());
                sheet.SetCellValue(startRow, 2, dept.DepartmentName);
                for (int i = 0; i <= rangeYear; i++)
                {
                    int year = fromYear + i;
                    for (int j = 0; j < countAction; j++)
                    {
                        var action = listAction[j];
                        int pos = j + j + 3;
                        double cost = listCost.Where(x => x.DeptID == dept.DepartmentID
                        && x.Year == year && x.ActionID == action.MainActionID).Sum(x => x.Cost);
                        sheet.SetCellValue(startRow, pos, cost.ToString());
                    }
                    string fromCol = Utils.GetExcelColumnName(3 + (i * (countAction + 1)));
                    string toCol = Utils.GetExcelColumnName(1 + ((i + 1) * (countAction + 1)));
                    string sum = "=SUM(" + fromCol + startRow + ":" + toCol + startRow + ")";
                    sheet.SetCellFormula(startRow, 2 + ((i + 1) * (countAction + 1)), sum);
                    listSumCol.Add(2 + ((i + 1) * (countAction + 1)));
                }
                startRow++;
            }
            foreach (int pos in listSumCol)
            {
                string sumCol = Utils.GetExcelColumnName(pos);
                string sum = "=SUM(" + sumCol + 7 + ":" + sumCol + (startRow - 1) + ")";
                sheet.SetCellFormula(startRow, pos, sum);
            }
        }

        private void SetDataNoAction(IXVTWorksheet sheet, DeptTypeName name, Period period, List<DeptCostInfo> listCost)
        {
            sheet.SheetName = name.DeptNameNoSign;
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            int rangeYear = toYear - fromYear;
            List<Department> listDept = this.context.Department.Where(x => x.Status == Constants.IS_ACTIVE && x.DepartmentType == name.DeptType)
                .OrderBy(x => x.DepartmentName).ToList();
            int lastCol = 3 + rangeYear;
            sheet.SetCellValue(2, lastCol, "Phụ lục 0" + (name.IndexSheet + 1));
            sheet.SetRangeFont(2, 1, 2, lastCol, "Times New Roman", 12, false, false);
            sheet.SetCellValue(3, 1, "Kính phí các " + name.DeptNameSign + " đề xuất giai đoạn " + fromYear + "-" + toYear);
            sheet.SetCellValue(5, lastCol - 1, "Đơn vị: triệu đồng");
            sheet.SetRangeFont(5, 1, 5, lastCol, "Times New Roman", 12, false, true);
            sheet.GetRange(3, 1, 3, lastCol).Range.Merge();
            sheet.GetRange(5, lastCol - 1, 5, lastCol).Range.Merge();
            sheet.GetRange(2, lastCol, 2, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Right;
            sheet.GetRange(5, lastCol - 1, 5, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Right;
            sheet.GetRange(3, 1, 3, lastCol).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;
            IXVTRange rangeHeader = sheet.GetRange(6, 3, 6, 3);
            IXVTRange rangeData = sheet.GetRange(7, 3, 7, 3);
            IXVTRange rangeTotal = sheet.GetRange(8, 3, 8, 3);
            for (int i = 0; i <= rangeYear; i++)
            {
                rangeHeader.CopyTo(sheet.GetRange(6, 3 + i, 6, 3 + i));
                rangeData.CopyTo(sheet.GetRange(7, 3 + i, 7, 3 + i));
                rangeTotal.CopyTo(sheet.GetRange(8, 3 + i, 8, 3 + i));
                int year = fromYear + i;
                sheet.SetCellValue(6, 3 + i, "Năm " + year);
            }
            rangeData = sheet.GetRange(7, 1, 7, lastCol);
            rangeTotal = sheet.GetRange(8, 1, 8, lastCol);
            rangeTotal.CopyTo(sheet.GetRange(7 + listDept.Count, 1, 7 + listDept.Count, lastCol));
            int startRow = 7;
            foreach (Department dept in listDept)
            {
                rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, lastCol));
                sheet.SetCellValue(startRow, 1, (startRow - 6).ToString());
                sheet.SetCellValue(startRow, 2, dept.DepartmentName);
                for (int i = 0; i <= rangeYear; i++)
                {
                    int year = fromYear + i;
                    double cost = listCost.Where(x => x.DeptID == dept.DepartmentID
                    && x.Year == year).Sum(x => x.Cost);
                    sheet.SetCellValue(startRow, 3 + i, cost.ToString());
                }
                startRow++;
            }
            for (int i = 0; i <= rangeYear; i++)
            {
                string sumCol = Utils.GetExcelColumnName(i + 3);
                string sum = "=SUM(" + sumCol + 7 + ":" + sumCol + (startRow - 1) + ")";
                sheet.SetCellFormula(startRow, i + 3, sum);
            }
        }

        public void Init(int planID)
        {
            Plan plan = context.Plan.Find(planID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch đã chọn");
            }
            Period period = context.Period.Find(plan.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Không tồn tại giai đoạn theo kế hoạch đã chọn");
            }
            List<CostEstimateDetail> listDetail = context.CostEstimateDetail.Where(x => x.PlanID == planID).ToList();
            // Insert vao ban cost truoc
            List<PlanTarget> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == planID).ToList();
            IQueryable<PlanTargetAction> iqPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(p => p.PlanID == planID && p.PlanTargetID == x.PlanTargetID));
            List<PlanTargetAction> listPlanTargetAction = iqPlanTargetAction.ToList();
            List<SubActionPlan> listSubActionPlan = context.SubActionPlan.Where(x => iqPlanTargetAction.Any(a => a.PlanTargetActionID == x.PlanTargetActionID)).ToList();
            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            // Luu san du lieu chi phi khac va chi phi hanh chinh
            List<CostEstimateSpend> listCostSpend = context.CostEstimateSpend.Where(x => x.PlanID == planID).ToList();
            double totalAllowance = context.Allowance.Where(x => x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
            double totalPaySheet = context.PaySheet.Where(x => x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
            List<CostEstimateSpend> listCostNew = new List<CostEstimateSpend>();
            CostEstimateSpend costOther = listCostSpend.FirstOrDefault(x => x.PlanTargetID == null && x.ParentID == null && x.SpendContent.Equals(Constants.QUY_SPEND_OTHER));
            if (costOther == null)
            {
                costOther = new CostEstimateSpend();
                costOther.PlanID = planID;
                costOther.SpendContent = Constants.QUY_SPEND_OTHER;
                costOther.OtherType = Constants.OTHER_TYPE_DU_PHONG;
                context.CostEstimateSpend.Add(costOther);
                listCostNew.Add(costOther);
            }

            #region Insert thong tin hanh chinh
            this.InitAd(planID);
            #endregion
            // Lay chi phi theo don vi dua tren tong hop tu duoi
            List<ActionCost> listActionCost = this.GetListActionCost(period);
            Dictionary<int, double> dicCostTargetAction = new Dictionary<int, double>();
            Dictionary<int, double> dicCostTarget = new Dictionary<int, double>();
            for (int year = startYear; year <= endYear; year++)
            {
                foreach (SubActionPlan sub in listSubActionPlan)
                {
                    CostEstimateDetail cd = new CostEstimateDetail();
                    cd.PlanID = planID;
                    PlanTargetAction pta = listPlanTargetAction.Where(x => x.PlanTargetActionID == sub.PlanTargetActionID).FirstOrDefault();
                    if (pta != null)
                    {
                        if (!listDetail.Any(x => x.PlanYear == year && x.SubActionPlanID == sub.SubActionPlanID && x.CostEstimateSpendID == null))
                        {
                            double totalMoney = 0;
                            List<ActionCost> listActionCostSub = listActionCost.Where(x => x.MainActionID == sub.SubActionID && x.PlanYear == year).ToList();
                            if (listActionCostSub != null && listActionCostSub.Any())
                            {
                                totalMoney = listActionCostSub.Sum(x => x.Cost);
                            }
                            cd.PlanTargetID = pta.PlanTargetID.Value;
                            cd.PlanTargetActionID = pta.PlanTargetActionID;
                            cd.SubActionPlanID = sub.SubActionPlanID;
                            cd.PlanYear = year;
                            cd.TotalMoney = totalMoney;
                            cd.MainActionID = sub.SubActionID.GetValueOrDefault();
                            context.CostEstimateDetail.Add(cd);
                            if (dicCostTargetAction.ContainsKey(pta.PlanTargetActionID))
                            {
                                dicCostTargetAction[pta.PlanTargetActionID] = dicCostTargetAction[pta.PlanTargetActionID] + totalMoney;
                            }
                            else
                            {
                                dicCostTargetAction[pta.PlanTargetActionID] = totalMoney;
                            }
                        }
                    }
                }
                foreach (PlanTargetAction pta in listPlanTargetAction)
                {
                    if (!listDetail.Any(x => x.PlanYear == year && x.PlanTargetActionID == pta.PlanTargetActionID && x.SubActionPlanID == null))
                    {
                        double totalMoney = 0;
                        List<ActionCost> listActionCostPlanTarget = listActionCost.Where(x => x.MainActionID == pta.MainActionID && x.PlanYear == year).ToList();
                        if (listActionCostPlanTarget != null && listActionCostPlanTarget.Any())
                        {
                            totalMoney = listActionCostPlanTarget.Sum(x => x.Cost);
                        }
                        if (totalMoney == 0 && dicCostTargetAction.ContainsKey(pta.PlanTargetActionID))
                        {
                            totalMoney = dicCostTargetAction[pta.PlanTargetActionID];
                        }
                        CostEstimateDetail cd = new CostEstimateDetail();
                        cd.PlanID = planID;
                        cd.PlanTargetID = pta.PlanTargetID.Value;
                        cd.PlanTargetActionID = pta.PlanTargetActionID;
                        cd.PlanYear = year;
                        cd.TotalMoney = totalMoney;
                        cd.MainActionID = pta.MainActionID.GetValueOrDefault();
                        context.CostEstimateDetail.Add(cd);
                        if (dicCostTarget.ContainsKey(pta.PlanTargetID.Value))
                        {
                            dicCostTarget[pta.PlanTargetID.Value] = dicCostTarget[pta.PlanTargetID.Value] + totalMoney;
                        }
                        else
                        {
                            dicCostTarget[pta.PlanTargetID.Value] = totalMoney;
                        }
                    }
                }
                foreach (PlanTarget pt in listPlanTarget)
                {
                    if (!listDetail.Any(x => x.PlanYear == year && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null))
                    {
                        double totalMoney = 0;
                        if (totalMoney == 0 && dicCostTarget.ContainsKey(pt.PlanTargetID))
                        {
                            totalMoney = dicCostTarget[pt.PlanTargetID];
                        }
                        CostEstimateDetail cd = new CostEstimateDetail();
                        cd.PlanID = planID;
                        cd.PlanTargetID = pt.PlanTargetID;
                        cd.PlanYear = year;
                        cd.TotalMoney = 0;
                        context.CostEstimateDetail.Add(cd);
                    }
                }
            }
        }

        public void InitAd(int planID)
        {
            Plan plan = context.Plan.Find(planID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch đã chọn");
            }
            Period period = context.Period.Find(plan.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Không tồn tại giai đoạn theo kế hoạch đã chọn");
            }
            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            // Luu san du lieu chi phi khac va chi phi hanh chinh
            List<CostEstimateSpend> listCostSpend = context.CostEstimateSpend.Where(x => x.PlanID == planID).ToList();
            List<Allowance> listAllowance = context.Allowance.Where(x => x.Year >= startYear && x.Year <= endYear && x.TotalPerYear != null).ToList();
            List<PaySheet> listPaySheet = context.PaySheet.Where(x => x.Year >= startYear && x.Year <= endYear && x.TotalPerYear != null).ToList();
            double totalAllowance = listAllowance.Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
            double totalPaySheet = listPaySheet.Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
            List<CostEstimateSpend> listCostNew = new List<CostEstimateSpend>();

            CostEstimateSpend costHC = listCostSpend.FirstOrDefault(x => x.PlanTargetID == null && x.ParentID == null && x.SpendContent.Equals(Constants.QUY_SPEND_HC));
            if (costHC == null)
            {
                costHC = new CostEstimateSpend();
                costHC.PlanID = planID;
                costHC.SpendContent = Constants.QUY_SPEND_HC;
                costHC.TotalPrice = totalAllowance + totalPaySheet;
                costHC.OtherType = Constants.OTHER_TYPE_HC;
                context.CostEstimateSpend.Add(costHC);
                listCostNew.Add(costHC);
            }
            if (listCostNew.Any())
            {
                context.SaveChanges();
            }
            #region Insert thong tin luong, phu cap
            CostEstimateSpend costHCLuong = listCostSpend.Where(x => x.ParentID == costHC.CostEstimateSpendID
            && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == null).FirstOrDefault();
            if (costHCLuong == null)
            {
                costHCLuong = new CostEstimateSpend();
                costHCLuong.PlanID = planID;
                costHCLuong.SpendContent = Constants.QUY_SPEND_HC_LUONG;
                costHCLuong.SpendItemTitle = "8.1";
                costHCLuong.ParentID = costHC.CostEstimateSpendID;
                costHCLuong.IsSalary = Constants.IS_ACTIVE;
                costHCLuong.TotalPrice = totalAllowance + totalPaySheet;
                context.CostEstimateSpend.Add(costHCLuong);
                context.SaveChanges();
                listCostNew.Add(costHCLuong);
            }
            // Luong, phu cap ban dieu hanh
            CostEstimateSpend costHCLuongBDH = listCostSpend.Where(x => x.ParentID == costHCLuong.CostEstimateSpendID
            && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_BDH) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == null).FirstOrDefault();
            if (costHCLuongBDH == null)
            {
                costHCLuongBDH = new CostEstimateSpend();
                costHCLuongBDH.PlanID = planID;
                costHCLuongBDH.SpendContent = Constants.QUY_SPEND_HC_LUONG_BDH;
                costHCLuongBDH.ParentID = costHCLuong.CostEstimateSpendID;
                costHCLuongBDH.IsSalary = Constants.IS_ACTIVE;
                costHCLuongBDH.GroupType = Constants.TYPE_BDH;
                costHCLuongBDH.TotalPrice = listAllowance.Where(x => x.Type == Constants.TYPE_BDH).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum()
                    + listPaySheet.Where(x => x.Type == Constants.TYPE_BDH).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                context.CostEstimateSpend.Add(costHCLuongBDH);
                context.SaveChanges();
                listCostNew.Add(costHCLuongBDH);
            }
            CostEstimateSpend costHCLuongChinhBDH = listCostSpend.Where(x => x.ParentID == costHCLuongBDH.CostEstimateSpendID
            && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_CHINH) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE).FirstOrDefault();
            if (costHCLuongChinhBDH == null)
            {
                costHCLuongChinhBDH = new CostEstimateSpend();
                costHCLuongChinhBDH.PlanID = planID;
                costHCLuongChinhBDH.SpendContent = Constants.QUY_SPEND_HC_LUONG_CHINH;
                costHCLuongChinhBDH.ParentID = costHCLuongBDH.CostEstimateSpendID;
                costHCLuongChinhBDH.IsSalary = Constants.IS_ACTIVE;
                costHCLuongChinhBDH.GroupType = Constants.TYPE_BDH;
                costHCLuongChinhBDH.SalaryType = Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE;
                costHCLuongChinhBDH.TotalPrice = listPaySheet.Where(x => x.Type == Constants.TYPE_BDH).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum(); ;
                context.CostEstimateSpend.Add(costHCLuongChinhBDH);
                listCostNew.Add(costHCLuongChinhBDH);
            }
            CostEstimateSpend costHCLuongPhuCapBDH = listCostSpend.Where(x => x.ParentID == costHCLuongBDH.CostEstimateSpendID
             && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_PHU_CAP) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE).FirstOrDefault();
            if (costHCLuongPhuCapBDH == null)
            {
                costHCLuongPhuCapBDH = new CostEstimateSpend();
                costHCLuongPhuCapBDH.PlanID = planID;
                costHCLuongPhuCapBDH.SpendContent = Constants.QUY_SPEND_HC_LUONG_PHU_CAP;
                costHCLuongPhuCapBDH.ParentID = costHCLuongBDH.CostEstimateSpendID;
                costHCLuongPhuCapBDH.IsSalary = Constants.IS_ACTIVE;
                costHCLuongPhuCapBDH.GroupType = Constants.TYPE_BDH;
                costHCLuongPhuCapBDH.SalaryType = Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE;
                costHCLuongPhuCapBDH.TotalPrice = listAllowance.Where(x => x.Type == Constants.TYPE_BDH).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                context.CostEstimateSpend.Add(costHCLuongPhuCapBDH);
                listCostNew.Add(costHCLuongPhuCapBDH);
            }
            // Luong, phu cap ban kiem soat
            CostEstimateSpend costHCLuongBKS = listCostSpend.Where(x => x.ParentID == costHCLuong.CostEstimateSpendID
            && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_BKS) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == null).FirstOrDefault();
            if (costHCLuongBKS == null)
            {
                costHCLuongBKS = new CostEstimateSpend();
                costHCLuongBKS.PlanID = planID;
                costHCLuongBKS.SpendContent = Constants.QUY_SPEND_HC_LUONG_BKS;
                costHCLuongBKS.ParentID = costHCLuong.CostEstimateSpendID;
                costHCLuongBKS.IsSalary = Constants.IS_ACTIVE;
                costHCLuongBKS.GroupType = Constants.TYPE_BKS;
                costHCLuongBKS.TotalPrice = listAllowance.Where(x => x.Type == Constants.TYPE_BKS).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum()
                    + listPaySheet.Where(x => x.Type == Constants.TYPE_BKS).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                context.CostEstimateSpend.Add(costHCLuongBKS);
                context.SaveChanges();
                listCostNew.Add(costHCLuongBKS);
            }
            CostEstimateSpend costHCLuongChinhBKS = listCostSpend.Where(x => x.ParentID == costHCLuongBKS.CostEstimateSpendID
            && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_CHINH) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE).FirstOrDefault();
            if (costHCLuongChinhBKS == null)
            {
                costHCLuongChinhBKS = new CostEstimateSpend();
                costHCLuongChinhBKS.PlanID = planID;
                costHCLuongChinhBKS.SpendContent = Constants.QUY_SPEND_HC_LUONG_CHINH;
                costHCLuongChinhBKS.ParentID = costHCLuongBKS.CostEstimateSpendID;
                costHCLuongChinhBKS.IsSalary = Constants.IS_ACTIVE;
                costHCLuongChinhBKS.SalaryType = Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE;
                costHCLuongChinhBKS.GroupType = Constants.TYPE_BKS;
                costHCLuongChinhBKS.TotalPrice = listPaySheet.Where(x => x.Type == Constants.TYPE_BKS).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum(); ;
                context.CostEstimateSpend.Add(costHCLuongChinhBKS);
                listCostNew.Add(costHCLuongChinhBKS);
            }
            CostEstimateSpend costHCLuongPhuCapBKS = listCostSpend.Where(x => x.ParentID == costHCLuongBKS.CostEstimateSpendID
             && x.SpendContent.Equals(Constants.QUY_SPEND_HC_LUONG_PHU_CAP) && x.IsSalary == Constants.IS_ACTIVE && x.SalaryType == Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE).FirstOrDefault();
            if (costHCLuongPhuCapBKS == null)
            {
                costHCLuongPhuCapBKS = new CostEstimateSpend();
                costHCLuongPhuCapBKS.PlanID = planID;
                costHCLuongPhuCapBKS.SpendContent = Constants.QUY_SPEND_HC_LUONG_PHU_CAP;
                costHCLuongPhuCapBKS.ParentID = costHCLuongBKS.CostEstimateSpendID;
                costHCLuongPhuCapBKS.GroupType = Constants.TYPE_BKS;
                costHCLuongPhuCapBKS.IsSalary = Constants.IS_ACTIVE;
                costHCLuongPhuCapBKS.SalaryType = Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE;
                costHCLuongPhuCapBKS.TotalPrice = listAllowance.Where(x => x.Type == Constants.TYPE_BKS).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                context.CostEstimateSpend.Add(costHCLuongPhuCapBKS);
                listCostNew.Add(costHCLuongPhuCapBKS);
            }
            context.SaveChanges();
            #endregion

            for (int year = startYear; year <= endYear; year++)
            {
                if (listCostNew.Any())
                {
                    foreach (CostEstimateSpend spend in listCostNew)
                    {
                        CostEstimateDetail cd = new CostEstimateDetail();
                        cd.PlanID = planID;
                        cd.PlanYear = year;
                        cd.TotalMoney = 0;
                        cd.CostEstimateSpendID = spend.CostEstimateSpendID;
                        // Tong hop luong
                        if (spend.IsSalary.GetValueOrDefault() == Constants.IS_ACTIVE)
                        {
                            int type = -1;
                            if (spend.GroupType.GetValueOrDefault() == Constants.TYPE_BDH)
                            {
                                type = Constants.TYPE_BDH;
                            }
                            else if (spend.GroupType.GetValueOrDefault() == Constants.TYPE_BKS)
                            {
                                type = Constants.TYPE_BKS;
                            }
                            if (type > 0)
                            {
                                if (spend.SalaryType == null)
                                {
                                    cd.TotalMoney = listAllowance.Where(x => x.Year == year && x.Type == type && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum()
                                        + listPaySheet.Where(x => x.Year == year && x.Type == type && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                                }
                                else if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE)
                                {
                                    cd.TotalMoney = listAllowance.Where(x => x.Year == year && x.Type == type && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                                }
                                else if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE)
                                {
                                    cd.TotalMoney = listPaySheet.Where(x => x.Year == year && x.Type == type && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                                }
                            }
                        }
                        context.CostEstimateDetail.Add(cd);
                    }
                }
            }
        }

        public void AddPlanCost(int startYear, int endYear, int planID, int planTargetID, int? mainActionID, int planTargetActionID, int? subActionPlanId = null)
        {
            if (endYear >= startYear)
            {
                for (int year = startYear; year <= endYear; year++)
                {
                    List<CostEstimateDetail> listCost = context.CostEstimateDetail
                    .Where(x => x.PlanID == planID && x.PlanTargetID == planTargetID && x.PlanTargetActionID == planTargetActionID
                    && x.SubActionPlanID == subActionPlanId && (x.PlanYear == year))
                    .ToList();
                    if (!listCost.Any())
                    {
                        CostEstimateDetail cd = new CostEstimateDetail();
                        cd.PlanID = planID;
                        cd.PlanTargetID = planTargetID;
                        cd.PlanTargetActionID = planTargetActionID;
                        cd.SubActionPlanID = subActionPlanId;
                        cd.PlanYear = year;
                        cd.MainActionID = mainActionID;
                        context.CostEstimateDetail.Add(cd);
                    }
                }
                context.SaveChanges();
            }

        }

        public List<PlanTargetModel> GetListPlanTargetCost(int planID, bool isSync = false)
        {
            Plan entity = this.context.Plan.Where(x => x.PlanID == planID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            Period period = this.context.Period.Find(entity.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetModel> listPlanTarget = this.context.PlanTarget.Where(x => x.PlanID == planID && x.Status == Constants.IS_ACTIVE)
                .Select(x => new PlanTargetModel
                {
                    PlanID = x.PlanID,
                    TargetID = x.TargetID,
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetCode = x.TargetCode,
                    TotalBudget = x.TotalBuget
                }).ToList();
            List<PlanTargetActionModel> listPlanTargetAction = (from pta in this.context.PlanTargetAction
                                                                join pt in this.context.PlanTarget
                                                                on pta.PlanTargetID equals pt.PlanTargetID
                                                                join s in this.context.SubActionPlan on pta.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                                from sj in sg.DefaultIfEmpty()
                                                                where pt.PlanID == planID
                                                                group sj by new { pt.PlanTargetID, pt.PlanTargetContent, pta.PlanTargetActionID, pta.TotalBudget, pta.ActionContent, pta.ActionCode, pta.ActionOrder } into g
                                                                orderby g.Key.PlanTargetID, g.Key.PlanTargetID
                                                                select new PlanTargetActionModel
                                                                {
                                                                    PlanTargetActionID = g.Key.PlanTargetActionID,
                                                                    ActionContent = g.Key.ActionContent,
                                                                    ActionCode = g.Key.ActionCode,
                                                                    PlanTargetID = g.Key.PlanTargetID,
                                                                    TotalBudget = g.Key.TotalBudget,
                                                                    ListSubActionPlan = g.Where(x => x != null && x.ParentID == null).Select(x => new SubActionPlanModel
                                                                    {
                                                                        SubActionPlanID = x.SubActionPlanID,
                                                                        SubActionCode = x.SubActionCode,
                                                                        SubActionContent = x.SubActionContent,
                                                                        PlanTargetActionID = x.PlanTargetActionID,
                                                                        TotalBudget = x.TotalBudget
                                                                    }).OrderBy(x => x.SubActionCode).ToList()
                                                                }).ToList();
            List<CostEstimateDetailModel> listCost = this.context.CostEstimateDetail
                .Where(x => x.PlanID == planID)
            .Select(x => new CostEstimateDetailModel
            {
                CostEstimateDetailID = x.CostEstimateDetailID,
                CostEstimateSpendID = x.CostEstimateSpendID,
                PlanTargetID = x.PlanTargetID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                PlanYear = x.PlanYear,
                TotalMoney = x.TotalMoney,
                Description = x.Description,
                MainActionID = x.MainActionID
            }).ToList();
            if (isSync)
            {
                List<ActionCost> listActionCost = this.GetListActionCost(period);
                foreach (CostEstimateDetailModel cost in listCost)
                {
                    List<ActionCost> listCostByAction = listActionCost.Where(x => x.MainActionID == cost.MainActionID && x.PlanYear == cost.PlanYear).ToList();
                    if (listCostByAction != null && listCostByAction.Any())
                    {
                        cost.TotalMoney = listCostByAction.Sum(x => x.Cost);
                    }
                }
                var listTargetCost = listCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID == null).ToList();
                foreach (CostEstimateDetailModel cost in listTargetCost)
                {
                    cost.TotalMoney = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanYear == cost.PlanYear
                        && x.PlanTargetActionID != null && x.SubActionPlanID == null).Sum(x => x.TotalMoney);
                }
            }

            // Lay subaction muc con
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlan.Where(x =>
            this.context.PlanTargetAction.Any(p => p.PlanID == planID
            && p.PlanTargetActionID == x.PlanTargetActionID))
            .Select(x => new SubActionPlanModel
            {
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                ParentID = x.ParentID,
                TotalBudget = x.TotalBudget
            }).OrderBy(x => x.SubActionCode).ToList();

            List<CostEstimateSpendModel> listSubSpendItem = this.context.CostEstimateSpend.Where(x => x.PlanID == planID)
            .Select(x => new CostEstimateSpendModel
            {
                CostEstimateSpendID = x.CostEstimateSpendID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                PlanTargetID = x.PlanTargetID,
                SpendContent = x.SpendContent,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice,
                MainActionID = x.MainActionID,
                IsSalary = x.IsSalary,
                SalaryType = x.SalaryType
            }).ToList();
            List<CostEstimateSpendModel> listSpendRoot = listSubSpendItem.Where(x => x.ParentID == null).ToList();
            foreach (CostEstimateSpendModel spend in listSpendRoot)
            {
                this.SetSpendCost(spend, listSubSpendItem, listCost);
            }
            foreach (SubActionPlanModel sub in listSubAction)
            {
                sub.ListCostEstimateDetail = listCost.Where(x => x.SubActionPlanID == sub.SubActionPlanID && x.CostEstimateSpendID == null).ToList();
                sub.ListCostEstimateSpend = listSubSpendItem.Where(x => x.SubActionPlanID == sub.SubActionPlanID).ToList();
            }
            foreach (PlanTargetActionModel planTargetAction in listPlanTargetAction)
            {
                // Lay thong tin chi tiet chi phi theo tung hoat dong con
                planTargetAction.ListCostEstimateDetail = listCost.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                && x.SubActionPlanID == null && x.CostEstimateSpendID == null).ToList();
                planTargetAction.ListCostEstimateSpend = listSubSpendItem.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                && x.SubActionPlanID == null).ToList();
                if (isSync)
                {
                    planTargetAction.TotalBudget = planTargetAction.ListCostEstimateDetail.Sum(x => x.TotalMoney);
                }

            }
            foreach (PlanTargetModel pt in listPlanTarget)
            {
                pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                pt.ListCostEstimateDetail = listCost.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null
                && x.SubActionPlanID == null && x.CostEstimateSpendID == null).ToList();
                pt.ListCostEstimateSpend = listSubSpendItem.Where(x => x.PlanTargetID == pt.PlanTargetID
                && x.PlanTargetActionID == null).ToList();
                if (isSync)
                {
                    pt.TotalBudget = pt.ListCostEstimateDetail.Sum(x => x.TotalMoney);
                }
            }
            return listPlanTarget;
        }

        public List<CostEstimateSpendModel> GetListRootSpend(int planID)
        {
            List<CostEstimateSpendModel> listSpend = this.context.CostEstimateSpend.Where(x => x.PlanID == planID && x.PlanTargetID == null)
            .Select(x => new CostEstimateSpendModel
            {
                CostEstimateSpendID = x.CostEstimateSpendID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                PlanTargetID = x.PlanTargetID,
                SpendContent = x.SpendContent,
                TotalPrice = x.TotalPrice,
                SpendItemTitle = x.SpendItemTitle,
                IsSalary = x.IsSalary,
                SalaryType = x.SalaryType,
                OtherType = x.OtherType
            }).ToList();
            List<CostEstimateDetailModel> listCost = this.context.CostEstimateDetail
                .Where(x => x.PlanID == planID)
            .Select(x => new CostEstimateDetailModel
            {
                CostEstimateDetailID = x.CostEstimateDetailID,
                CostEstimateSpendID = x.CostEstimateSpendID,
                PlanTargetID = x.PlanTargetID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                PlanYear = x.PlanYear,
                TotalMoney = x.TotalMoney,
                Description = x.Description,
                MainActionID = x.MainActionID
            }).ToList();
            List<CostEstimateSpendModel> listSpendRoot = listSpend.Where(x => x.ParentID == null).ToList();
            foreach (CostEstimateSpendModel spend in listSpendRoot)
            {
                this.SetSpendCost(spend, listSpend, listCost);
            }
            return listSpendRoot;
        }

        private void SetSpendCost(CostEstimateSpendModel spend, List<CostEstimateSpendModel> listSpend, List<CostEstimateDetailModel> listCost)
        {
            spend.ListDetail = listCost.Where(x => x.CostEstimateSpendID == spend.CostEstimateSpendID).ToList();
            spend.ListChildSpend = listSpend.Where(x => x.ParentID == spend.CostEstimateSpendID).ToList();
            if (spend.ListChildSpend != null && spend.ListChildSpend.Any())
            {
                foreach (CostEstimateSpendModel child in spend.ListChildSpend)
                {
                    this.SetSpendCost(child, listSpend, listCost);
                }
            }
        }

        private List<ActionSynInfo> GetListTarget(int deptType)
        {
            List<ActionSynInfo> listTarget = (from t in this.context.Target
                                              join a in this.context.MainAction on t.MainActionID equals a.MainActionID
                                              join m in this.context.MappingActionDepartment on a.MainActionID equals m.MainActionID
                                              where t.Status == Constants.IS_ACTIVE && a.Status == Constants.IS_ACTIVE
                                              && t.Type == Constants.SUB_TYPE && a.Type == Constants.MAIN_TYPE
                                              && m.DepartmentType == deptType
                                              select new ActionSynInfo
                                              {
                                                  TargetID = t.TargetID,
                                                  MainActionID = a.MainActionID,
                                                  DeptType = m.DepartmentType,
                                                  DeptID = m.DepartmentID,
                                                  ActionCode = a.ActionCode,
                                                  ActionContent = a.ActionContent
                                              }).Distinct().ToList();
            return listTarget;
        }

        public List<ActionCost> GetListActionCost(Period period)
        {
            List<DeptCostInfo> listDeptCost = this.GetListDeptCost(period.PeriodID);
            List<int> listActionID = listDeptCost.Select(x => x.ActionID).Distinct().ToList();
            var listAction = context.MappingActionDepartment.Where(x => listActionID.Contains(x.MainActionID) && x.DepartmentID != null).ToList();

            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            Dictionary<string, double> dicDeptType = new Dictionary<string, double>();
            for (int year = startYear; year <= endYear; year++)
            {
                foreach (var action in listAction)
                {
                    string key = action.DepartmentType + "_" + year;
                    if (dicDeptType.ContainsKey(key))
                    {
                        dicDeptType[key] = dicDeptType[key] + listDeptCost.Where(x => x.ActionID == action.MainActionID
                        && x.DeptID == action.DepartmentID && x.DeptType == action.DepartmentType && x.Year == year).Select(x => x.Cost).DefaultIfEmpty(0).Sum();
                    }
                    else
                    {
                        dicDeptType[key] = listDeptCost.Where(x => x.ActionID == action.MainActionID
                        && x.DeptID == action.DepartmentID && x.DeptType == action.DepartmentType && x.Year == year).Select(x => x.Cost).DefaultIfEmpty(0).Sum();
                    }
                }
            }
            List<MainAction> listMainAction = context.MainAction.Where(x => x.Status == Constants.IS_ACTIVE && listActionID.Contains(x.MainActionID)).ToList();
            List<ActionCost> listCost = new List<ActionCost>();
            for (int year = startYear; year <= endYear; year++)
            {
                foreach (var action in listMainAction)
                {
                    ActionCost cost = new ActionCost();
                    cost.MainActionID = action.MainActionID;
                    cost.ActionCode = action.ActionCode;
                    cost.ActionContent = action.ActionContent;
                    cost.PlanYear = year;
                    cost.Cost = 0;
                    var listMappingAction = listAction.Where(x => x.MainActionID == action.MainActionID).ToList();
                    if (listMappingAction != null && listMappingAction.Any())
                    {
                        foreach (var ma in listMappingAction)
                        {
                            string key = ma.DepartmentType + "_" + year;
                            if (ma.DepartmentID != null)
                            {
                                cost.Cost += listDeptCost.Where(x => x.ActionID == action.MainActionID
                                     && x.DeptID == ma.DepartmentID && x.DeptType == ma.DepartmentType && x.Year == year).Select(x => x.Cost).DefaultIfEmpty(0).Sum();
                            }
                            else
                            {
                                cost.Cost = listDeptCost.Where(x => x.ActionID == action.MainActionID
                                     && x.DeptType == ma.DepartmentType && x.Year == year).Select(x => x.Cost).DefaultIfEmpty(0).Sum();
                                if (dicDeptType.ContainsKey(key))
                                {
                                    cost.Cost += cost.Cost - dicDeptType[key];
                                }
                            }
                        }
                    }
                    else
                    {
                        cost.Cost = listDeptCost.Where(x => x.ActionID == action.MainActionID && x.Year == year).Select(x => x.Cost).DefaultIfEmpty(0).Sum();
                    }
                    listCost.Add(cost);
                }
            }
            return listCost;
        }

        public void SaveCostEstimate(List<string> listTotalPrice, List<long> listCostEstimateDetailID, List<string> listDes, int planID)
        {
            List<int> listPlanTargetID = this.context.PlanTarget.Where(x => x.PlanID == planID).Select(x => x.PlanTargetID).ToList();
            List<CostEstimateDetail> listCost = this.context.CostEstimateDetail.Where(x => x.PlanID == planID).ToList();
            int countYear = listCostEstimateDetailID.Count / listDes.Count;
            for (int i = 0; i < listCostEstimateDetailID.Count; i++)
            {
                string sTotalPrice = listTotalPrice[i];
                CostEstimateDetail cost = listCost.Where(x => x.CostEstimateDetailID == listCostEstimateDetailID[i]).FirstOrDefault();
                if (cost != null)
                {
                    double totalPrice;
                    if (!string.IsNullOrWhiteSpace(sTotalPrice))
                    {
                        if (double.TryParse(sTotalPrice, out totalPrice))
                        {
                            cost.TotalMoney = totalPrice;
                        }
                        else
                        {
                            throw new BusinessException("Giá phải nhập là kiểu số thập phân");
                        }
                    }
                    cost.Description = listDes[i / countYear];
                }
            }
            context.SaveChanges();
            // Cap nhat lai chi phi tong
            this.UpdateTotalCost(planID);
        }

        private void UpdateTotalCost(int planID)
        {
            List<CostEstimateDetail> listCost = this.context.CostEstimateDetail.Where(x => x.PlanID == planID).ToList();
            // Chi phi theo hang muc con
            List<long> listCostSpendID = listCost.Where(x => x.CostEstimateSpendID != null).Select(x => x.CostEstimateSpendID.Value).Distinct().ToList();
            List<CostEstimateSpend> listCostSpend = this.context.CostEstimateSpend.Where(x => listCostSpendID.Contains(x.CostEstimateSpendID)).ToList();
            foreach (CostEstimateSpend item in listCostSpend)
            {
                item.TotalPrice = listCost.Where(x => x.CostEstimateSpendID == item.CostEstimateSpendID).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
                if (item.TotalPrice == 0)
                {
                    item.TotalPrice = null;
                }
            }
            // Tinh lai chi phi tu lop thap nhat di len
            #region Cap nhat theo cac muc chi khac ngoai hoat dong
            List<CostEstimateDetail> listSpendCostDetail = listCost.Where(x => x.CostEstimateSpendID != null).ToList();
            foreach (CostEstimateDetail cost in listSpendCostDetail)
            {
                this.UpdateSpendCost(cost, listCost, listCostSpend);
            }
            #endregion

            #region Cap nhat du lieu theo hoat dong
            Dictionary<string, double> dicPlanTarget = new Dictionary<string, double>();
            Dictionary<string, double> dicPlanTargetAction = new Dictionary<string, double>();
            List<CostEstimateDetail> listSubCost = listCost.Where(x => x.SubActionPlanID != null && x.CostEstimateSpendID == null).ToList();
            foreach (CostEstimateDetail cost in listSubCost)
            {
                string key = cost.PlanTargetActionID.Value + "_" + cost.PlanYear;
                if (dicPlanTargetAction.ContainsKey(key))
                {
                    dicPlanTargetAction[key] = dicPlanTargetAction[key] + cost.TotalMoney;
                }
                else
                {
                    dicPlanTargetAction[key] = cost.TotalMoney;
                }
            }
            List<CostEstimateDetail> listPlanTargetActionCost = listCost.Where(x => x.PlanTargetActionID != null && x.SubActionPlanID == null && x.CostEstimateSpendID == null).ToList();
            foreach (CostEstimateDetail cost in listPlanTargetActionCost)
            {
                string key = cost.PlanTargetID + "_" + cost.PlanYear;
                string keyAction = cost.PlanTargetActionID.Value + "_" + cost.PlanYear;
                if (dicPlanTargetAction.ContainsKey(keyAction))
                {
                    cost.TotalMoney = dicPlanTargetAction[keyAction];
                }
                if (cost.PlanTargetActionID != null)
                {
                    if (dicPlanTarget.ContainsKey(key))
                    {
                        dicPlanTarget[key] = dicPlanTarget[key] + cost.TotalMoney;
                    }
                    else
                    {
                        dicPlanTarget[key] = cost.TotalMoney;
                    }
                }
            }
            List<CostEstimateDetail> listPlanTargetCost = listCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID == null && x.SubActionPlanID == null && x.CostEstimateSpendID == null).ToList();
            foreach (CostEstimateDetail cost in listPlanTargetCost)
            {
                string key = cost.PlanTargetID + "_" + cost.PlanYear;
                if (dicPlanTarget.ContainsKey(key))
                {
                    cost.TotalMoney = dicPlanTarget[key];
                }
            }
            #endregion            

            #region Cap nhat chi phi tong vao hoat dong
            // Update rieng phan muc tieu
            List<int> listPlanTargetID = listCost.Where(x => x.PlanTargetID != null).Select(x => x.PlanTargetID.Value).Distinct().ToList();
            List<CostEstimateDetail> listTargetCost = this.context.CostEstimateDetail.Where(x => x.PlanTargetID != null && x.PlanTargetActionID == null && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
            foreach (CostEstimateDetail targetCost in listTargetCost)
            {
                targetCost.TotalMoney = listCost.Where(x => x.PlanTargetID == targetCost.PlanTargetID && x.PlanTargetActionID != null
                && x.SubActionPlanID == null && x.CostEstimateSpendID == null && x.PlanYear == targetCost.PlanYear).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
            }
            // Update tong chi phi
            List<int> listSubActionID = listCost.Where(x => x.SubActionPlanID != null).Select(x => x.SubActionPlanID.Value).Distinct().ToList();
            List<int> listPlanTargetActionID = listCost.Where(x => x.PlanTargetActionID != null).Select(x => x.PlanTargetActionID.Value).Distinct().ToList();
            List<SubActionPlan> listSubAction = this.context.SubActionPlan.Where(x => listSubActionID.Contains(x.SubActionPlanID)).ToList();
            List<PlanTargetAction> listPlanTargetAction = this.context.PlanTargetAction.Where(x => listPlanTargetActionID.Contains(x.PlanTargetActionID)).ToList();
            List<PlanTarget> listPlanTarget = this.context.PlanTarget.Where(x => listPlanTargetID.Contains(x.PlanTargetID)).ToList();
            foreach (SubActionPlan item in listSubAction)
            {
                item.TotalBudget = listCost.Where(x => x.SubActionPlanID == item.SubActionPlanID && x.CostEstimateSpendID == null).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
                if (item.TotalBudget == 0)
                {
                    item.TotalBudget = null;
                }
            }
            foreach (PlanTargetAction item in listPlanTargetAction)
            {
                item.TotalBudget = listCost.Where(x => x.PlanTargetActionID == item.PlanTargetActionID && x.PlanTargetID == item.PlanTargetID && x.SubActionPlanID == null && x.CostEstimateSpendID == null).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
                if (item.TotalBudget == 0)
                {
                    item.TotalBudget = null;
                }
            }
            foreach (PlanTarget item in listPlanTarget)
            {
                item.TotalBuget = listCost.Where(x => x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == null && x.CostEstimateSpendID == null).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
                if (item.TotalBuget == 0)
                {
                    item.TotalBuget = null;
                }
            }
            #endregion
        }

        public void DelSpendCost(int spendID)
        {
            CostEstimateSpend spend = this.context.CostEstimateSpend.Find(spendID);
            if (spend == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int planID = spend.PlanID;
            List<CostEstimateDetail> listCostSpend = this.context.CostEstimateDetail.Where(x => x.CostEstimateSpendID == spendID).ToList();
            foreach (CostEstimateDetail detail in listCostSpend)
            {
                detail.TotalMoney = 0;
            }
            context.SaveChanges();
            this.UpdateTotalCost(planID);
            this.context.CostEstimateDetail.RemoveRange(listCostSpend);
            this.context.CostEstimateSpend.Remove(spend);
            context.SaveChanges();
        }

        public double UpdateSpendCost(CostEstimateDetail cost, List<CostEstimateDetail> listCost, List<CostEstimateSpend> listSpend)
        {
            double totalMoney = 0;
            List<CostEstimateSpend> listSpendChild = listSpend.Where(x => x.ParentID == cost.CostEstimateSpendID).ToList();
            if (listSpendChild != null && listSpendChild.Any())
            {
                foreach (CostEstimateSpend child in listSpendChild)
                {
                    CostEstimateDetail childCost = listCost.Where(x => x.CostEstimateSpendID == child.CostEstimateSpendID && x.PlanYear == cost.PlanYear).FirstOrDefault();
                    if (childCost != null)
                    {
                        totalMoney += this.UpdateSpendCost(childCost, listCost, listSpend);
                    }
                }
                cost.TotalMoney = totalMoney;
            }
            else
            {
                totalMoney = cost.TotalMoney;
            }
            CostEstimateSpend spend = listSpend.Where(x => x.CostEstimateSpendID == cost.CostEstimateSpendID).FirstOrDefault();
            if (spend != null && spend.ParentID == null)
            {
                CostEstimateDetail parCost = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                && x.SubActionPlanID == cost.SubActionPlanID && x.PlanYear == cost.PlanYear && x.CostEstimateSpendID == null).FirstOrDefault();
                if (parCost != null)
                {
                    parCost.TotalMoney = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                            && x.SubActionPlanID == cost.SubActionPlanID && x.PlanYear == cost.PlanYear && x.CostEstimateSpendID != null).Select(x => x.TotalMoney).DefaultIfEmpty(0).Sum();
                }
            }
            return totalMoney;
        }

        private DeptTypeName GetDeptName(int deptType)
        {
            DeptTypeName name = new DeptTypeName();
            string deptNameNoSign = string.Empty;
            string deptNameSign = string.Empty;
            int index = 0;
            switch (deptType)
            {
                case Constants.DEPT_TYPE_BO_NGANH:
                    deptNameNoSign = "Bo nganh";
                    deptNameSign = "Bộ, ngành";
                    index = 2;
                    break;
                case Constants.DEPT_TYPE_TINH_TP:
                    deptNameNoSign = "Cac tinh, TP";
                    deptNameSign = "tỉnh, thành phố";
                    index = 3;
                    break;
                case Constants.DEPT_TYPE_TP_DU_LICH:
                    deptNameNoSign = "TP du lich";
                    deptNameSign = "thành phố du lịch";
                    index = 3;
                    break;
                case Constants.DEPT_TYPE_BV_CAI_NGHIEN:
                    deptNameNoSign = "Bẹnh vien cai nghien";
                    deptNameSign = "Bệnh viện thực hiện công tác tư vấn cai nghiện thuốc lá";
                    index = 4;
                    break;
                case Constants.DEPT_TYPE_SANG_KIEN:
                    deptNameNoSign = "Sang kien";
                    deptNameSign = "Sáng kiến không về PCTH thuốc lá";
                    index = 5;
                    break;
            }
            name.DeptNameNoSign = deptNameNoSign;
            name.DeptNameSign = deptNameSign;
            name.IndexSheet = index;
            name.DeptType = deptType;
            return name;
        }
    }


    public class DeptTypeName
    {
        public string DeptNameNoSign { get; set; }
        public string DeptNameSign { get; set; }
        public int DeptType { get; set; }
        public int IndexSheet { get; set; }
    }
}