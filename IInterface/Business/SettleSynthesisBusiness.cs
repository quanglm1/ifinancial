﻿using ClosedXML.Excel;
using IInterface.Common;
using IInterface.Common.CommonExcel;
using IInterface.Models;
using IModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using IInterface.Common.CommonBO;
using System.Globalization;

namespace IInterface.Business
{
    public class SettleSynthesisBusiness
    {
        private Entities context;
        public SettleSynthesisBusiness(Entities context)
        {
            this.context = context;
        }

        public PlanTargetModel GetListCost(int planTargetActionID, int planTargetID, int synID, bool isReview = false)
        {
            SettleSynthesis entitySettle = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entitySettle == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            PlanTargetModel entityPlanTarget = this.context.PlanTarget.Where(x => x.PlanTargetID == planTargetID && x.Status == Constants.IS_ACTIVE)
                .Select(x => new PlanTargetModel
                {
                    PlanID = x.PlanID,
                    TargetID = x.TargetID,
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetCode = x.TargetCode,
                    TotalBudget = context.SubActionPlanCost.Where(sc => sc.PlanTargetID == planTargetID && sc.PlanTargetActionID == null
                                                                   && sc.SubActionPlanID == null && sc.SubActionSpendItemID == null && sc.PlanYear == entitySettle.Year).Select(sc => sc.TotalPrice).Sum()
                }).FirstOrDefault();
            if (entityPlanTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listPlanTargetAction = (from pt in this.context.PlanTarget
                                                                join pta in this.context.PlanTargetAction.Where(x => planTargetActionID == 0 || x.PlanTargetActionID == planTargetActionID)
                                                                on pt.PlanTargetID equals pta.PlanTargetID
                                                                where pt.PlanTargetID == entityPlanTarget.PlanTargetID
                                                                orderby pt.TargetOrder, pta.ActionOrder
                                                                select new PlanTargetActionModel
                                                                {
                                                                    PlanTargetActionID = pta.PlanTargetActionID,
                                                                    ActionContent = pta.ActionContent,
                                                                    ActionCode = pta.ActionCode,
                                                                    PlanTargetID = pta.PlanTargetID,
                                                                    TotalBudget = context.SubActionPlanCost.Where(sc => sc.PlanTargetActionID == pta.PlanTargetActionID
                                                                   && sc.SubActionPlanID == null && sc.SubActionSpendItemID == null && sc.PlanYear == entitySettle.Year).Select(sc => sc.TotalPrice).Sum()
                                                                }).ToList();


            // Lay subaction muc con
            List<SubActionPlanModel> listSubAction = this.context.SubActionPlan.Where(x => this.context.PlanTargetAction.Any(p => p.PlanTargetID == planTargetID
            && p.PlanTargetActionID == x.PlanTargetActionID && (planTargetActionID == 0 || p.PlanTargetActionID == planTargetActionID)))
            .Select(x => new SubActionPlanModel
            {
                PlanTargetID = planTargetID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                SubActionCode = x.SubActionCode,
                SubActionContent = x.SubActionContent,
                ParentID = x.ParentID,
                SubActionOrder = x.SubActionOrder,
                TotalBudget = context.SubActionPlanCost.Where(sc => sc.PlanTargetActionID == x.PlanTargetActionID
                                                                   && sc.SubActionPlanID == x.SubActionPlanID && sc.SubActionSpendItemID == null
                                                                   && sc.PlanYear == entitySettle.Year).Select(sc => sc.TotalPrice).Sum()
            }).OrderBy(x => x.SubActionOrder).ToList();

            // Lay du lieu da nhap
            List<SettleSynCostModel> listSettle = (from s in this.context.SettleSynthesisCost
                                                   join t in this.context.PlanTarget on s.PlanTargetID equals t.PlanTargetID
                                                   where s.SettleSynthesisID == synID && t.PlanTargetID == planTargetID
                                                   && (planTargetActionID == 0 || s.PlanTargetActionID == planTargetActionID)
                                                   select new SettleSynCostModel
                                                   {
                                                       SettleSynthesisCostID = s.SettleSynthesisCostID,
                                                       SettleSynthesisID = s.SettleSynthesisID,
                                                       SubActionSpendItemID = s.SubActionSpendItemID,
                                                       ActionCode = s.ActionCode,
                                                       ActionTitle = s.ActionTitle,
                                                       PlanSetttleMoney = s.PlanSetttleMoney == null ? 0 : s.PlanSetttleMoney.Value,
                                                       SetttleMoney = s.SetttleMoney,
                                                       SetttleMoneyChange = s.SetttleMoneyChange,
                                                       Description = s.Description,
                                                       DescriptionChange = s.DescriptionChange,
                                                       ParentID = s.ParentID,
                                                       OrderNumber = s.OrderNumber,
                                                       PlanTargetActionID = s.PlanTargetActionID,
                                                       SubPlanActionID = s.SubPlanActionID,
                                                       PlanTargetID = s.PlanTargetID,
                                                       ExpenseName = s.ActionTitle,
                                                       CostUpdateUserID = s.UpdateUserID,
                                                       CostUpdateType = s.UpdateType
                                                   }).ToList();
            // Thiet lap lai danh sach cha con
            List<SettleSynCostModel> listRootSettle = listSettle.Where(x => x.ParentID == null).ToList();
            foreach (SettleSynCostModel item in listRootSettle)
            {
                this.AddSettleCost(item, listSettle);
            }

            // Gan chi phi
            foreach (SubActionPlanModel sub in listSubAction)
            {
                SettleSynCostModel settleCost = listSettle.Where(x => x.PlanTargetActionID == sub.PlanTargetActionID
                                        && x.SubPlanActionID == sub.SubActionPlanID
                                        && x.SubActionSpendItemID == null && x.PlanTargetActionSpendItemID == null
                                        && x.ParentID == null).FirstOrDefault();
                if (settleCost != null)
                {
                    sub.SettleSynthesisCostID = settleCost.SettleSynthesisCostID;
                    sub.TotalSettleMoney = settleCost.SetttleMoney;
                    sub.SettleDescription = settleCost.Description;
                    sub.TotalSettleMoneyChange = settleCost.SetttleMoneyChange;
                    sub.SettleDescriptionChange = settleCost.DescriptionChange;
                    sub.CostUpdateUserID = settleCost.CostUpdateUserID;
                    sub.CostUpdateType = settleCost.CostUpdateType;
                    sub.ListSettleCost = listSettle.Where(x => x.PlanTargetActionID == sub.PlanTargetActionID
                                        && x.SubPlanActionID == sub.SubActionPlanID
                                        && x.SubActionSpendItemID == null
                                        && x.ParentID == settleCost.SettleSynthesisCostID).ToList();
                }
            }

            List<SubActionSpendItemModel> listSubSpendItem = this.context.SubActionSpendItem.Where(x => this.context.PlanTargetAction
            .Any(p => p.PlanTargetActionID == x.PlanTargetActionID && p.PlanTargetID == planTargetID
            && (planTargetActionID == 0 || p.PlanTargetActionID == planTargetActionID)))
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = context.SubActionPlanCost.Where(sc => sc.SubActionSpendItemID == x.SubActionSpendItemID
                                                                   && sc.PlanYear == entitySettle.Year).Select(sc => sc.TotalPrice).Sum()
            }).ToList();
            // Lay thong tin quyet toan neu co
            foreach (SubActionSpendItemModel item in listSubSpendItem)
            {
                SettleSynCostModel settleCost = listSettle.Where(x => x.PlanTargetActionID == item.PlanTargetActionID
                                        && x.SubPlanActionID == item.SubActionPlanID
                                        && x.SubActionSpendItemID == item.SubActionSpendItemID
                                        && x.ParentID == null).FirstOrDefault();
                if (settleCost != null)
                {
                    item.SettleSynthesisCostID = settleCost.SettleSynthesisCostID;
                    item.TotalSettleMoney = settleCost.SetttleMoney;
                    item.SettleDescription = settleCost.Description;
                    item.ListSettleCost = listSettle.Where(x => x.PlanTargetActionID == item.PlanTargetActionID
                                        && x.SubPlanActionID == item.SubActionPlanID
                                        && x.SubActionSpendItemID == item.SubActionSpendItemID
                                        && x.ParentID == settleCost.SettleSynthesisCostID).ToList();
                }
            }

            foreach (PlanTargetActionModel planTargetAction in listPlanTargetAction)
            {
                SettleSynCostModel settleCost = listSettle.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                && x.SubPlanActionID == null).FirstOrDefault();
                if (settleCost != null)
                {
                    planTargetAction.SettleSynthesisCostID = settleCost.SettleSynthesisCostID;
                    planTargetAction.TotalSettleMoney = settleCost.SetttleMoney;
                    planTargetAction.SettleDescription = settleCost.Description;
                    planTargetAction.TotalSettleMoneyChange = settleCost.SetttleMoneyChange;
                    planTargetAction.SettleDescriptionChange = settleCost.DescriptionChange;
                    planTargetAction.CostUpdateUserID = settleCost.CostUpdateUserID;
                    planTargetAction.CostUpdateType = settleCost.CostUpdateType;
                    planTargetAction.ListSettleCost = listSettle.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID && x.SubPlanActionID == null
                    && x.SubActionSpendItemID == null && x.ParentID == settleCost.SettleSynthesisCostID).ToList();
                }
                planTargetAction.ListSubActionPlan = listSubAction.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                && x.ParentID == null).OrderBy(x => x.SubActionOrder).ToList();
                if (planTargetAction.ListSubActionPlan != null && planTargetAction.ListSubActionPlan.Any())
                {
                    foreach (SubActionPlanModel sub in planTargetAction.ListSubActionPlan)
                    {
                        this.AddSubAction(sub, listSubAction, listSubSpendItem, listSettle);
                    }
                }
                else
                {
                    planTargetAction.ListSubActionSpendItem = listSubSpendItem.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID
                    && x.SubActionPlanID == null && x.ParentID == null && x.PlanTargetSpendItemID == null).ToList();
                    foreach (SubActionSpendItemModel item in planTargetAction.ListSubActionSpendItem)
                    {
                        this.AddSubCost(item, listSubSpendItem, listSettle);
                    }
                }
            }

            var targetCost = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID
                                               && x.PlanTargetID == entityPlanTarget.PlanTargetID
                                               && x.PlanTargetActionID == null
                                               && x.SubPlanActionID == null
                                               && x.ParentID == null).FirstOrDefault();
            if (targetCost != null)
            {
                entityPlanTarget.SettleSynthesisCostID = targetCost.SettleSynthesisCostID;
                entityPlanTarget.TotalSettleMoney = targetCost.SetttleMoney;
                entityPlanTarget.SettleDescription = targetCost.Description;
                entityPlanTarget.TotalSettleMoneyChange = targetCost.SetttleMoneyChange;
                entityPlanTarget.SettleDescriptionChange = targetCost.DescriptionChange;
                entityPlanTarget.CostUpdateUserID = targetCost.UpdateUserID;
                entityPlanTarget.CostUpdateType = targetCost.UpdateType;
            }
            entityPlanTarget.ListPlanTargetActionModel = listPlanTargetAction;
            return entityPlanTarget;
        }

        public void AddSubAction(SubActionPlanModel subAction, List<SubActionPlanModel> listAllSubAction,
            List<SubActionSpendItemModel> listSubSpendItem,
            List<SettleSynCostModel> listCost)
        {
            List<SubActionPlanModel> listChild = listAllSubAction.Where(x => x.ParentID == subAction.SubActionPlanID).OrderBy(x => x.SubActionOrder).ToList();
            if (listAllSubAction != null && listChild.Any())
            {
                subAction.ListChildSubActionPlan = listChild;
                foreach (SubActionPlanModel child in listChild)
                {
                    this.AddSubAction(child, listAllSubAction, listSubSpendItem, listCost);
                }
            }
            else
            {
                // Lay thong tin chi phi
                subAction.ListSubActionSpendItem = listSubSpendItem.Where(x => x.SubActionPlanID == subAction.SubActionPlanID && x.ParentID == null).ToList();
                foreach (SubActionSpendItemModel item in subAction.ListSubActionSpendItem)
                {
                    this.AddSubCost(item, listSubSpendItem, listCost);
                }
            }
        }

        public void SetSubActionData(IXVTWorksheet sheet, SubActionPlanModel subActionPlan,
            ref int startRow, Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum)
        {
            if (subActionPlan != null)
            {
                dicPos["subaction_" + subActionPlan.SubActionPlanID] = startRow;
                if (subActionPlan.ParentID == null)
                {
                    dicPosSum["targetaction_" + subActionPlan.PlanTargetActionID].Add(startRow);
                }
                else
                {
                    dicPosSum["subaction_" + subActionPlan.ParentID.Value].Add(startRow);
                }
                if (!dicPosSum.ContainsKey("subaction_" + subActionPlan.SubActionPlanID))
                {
                    dicPosSum["subaction_" + subActionPlan.SubActionPlanID] = new List<int>();
                }
                sheet.SetCellValue(startRow, 1, "'" + subActionPlan.SubActionCode);
                sheet.SetCellValue(startRow, 2, "'" + subActionPlan.SubActionContent);
                sheet.SetCellValue(startRow, 3, subActionPlan.TotalBudget.GetValueOrDefault().ToString());
                sheet.SetCellValue(startRow, 4, subActionPlan.TotalSettleMoney.GetValueOrDefault().ToString());
                sheet.SetCellValue(startRow, 5, "'" + subActionPlan.SettleDescription);
                sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                sheet.SetCellFormat(startRow, 4, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                if (subActionPlan.ParentID == null)
                {
                    sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 12, false, false);
                }
                else
                {
                    sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 12, false, true);
                }
                startRow++;
                if (subActionPlan.ListChildSubActionPlan != null && subActionPlan.ListChildSubActionPlan.Any())
                {
                    foreach (SubActionPlanModel item in subActionPlan.ListChildSubActionPlan)
                    {
                        this.SetSubActionData(sheet, item, ref startRow, dicPos, dicPosSum);
                    }
                }
                else
                {
                    if (subActionPlan.ListSubActionSpendItem != null && subActionPlan.ListSubActionSpendItem.Any())
                    {
                        foreach (SubActionSpendItemModel item in subActionPlan.ListSubActionSpendItem)
                        {
                            this.SetSubCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                        }
                    }
                    if (subActionPlan.ListSettleCost != null && subActionPlan.ListSettleCost.Any())
                    {
                        foreach (SettleSynCostModel item in subActionPlan.ListSettleCost)
                        {
                            this.SetSettleCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                        }
                    }
                }
            }
        }

        public void AddSettleCost(SettleSynCostModel cost, List<SettleSynCostModel> listSettle)
        {
            List<SettleSynCostModel> listChild = listSettle.Where(x => x.ParentID == cost.SettleSynthesisCostID).ToList();
            if (listChild != null && listChild.Any())
            {
                cost.ListChildCost = listChild;
                foreach (SettleSynCostModel item in listChild)
                {
                    this.AddSettleCost(item, listSettle);
                }
            }
        }

        public void SetSettleCostData(IXVTWorksheet sheet, SettleSynCostModel settle,
            ref int startRow, Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum)
        {
            if (settle != null)
            {
                dicPos["settlecost_" + settle.SettleSynthesisCostID] = startRow;
                if (settle.ParentID != null && dicPosSum.ContainsKey("settlecost_" + settle.ParentID.GetValueOrDefault()))
                {
                    dicPosSum["settlecost_" + settle.ParentID.Value].Add(startRow);
                }
                else if (settle.SubActionSpendItemID != null)
                {
                    dicPosSum["subcost_" + settle.SubActionSpendItemID.Value].Add(startRow);
                }
                else if (settle.SubPlanActionID != null)
                {
                    dicPosSum["subaction_" + settle.SubPlanActionID.Value].Add(startRow);
                }
                else
                {
                    dicPosSum["targetaction_" + settle.PlanTargetActionID.Value].Add(startRow);
                }
                if (!dicPosSum.ContainsKey("settlecost_" + settle.SettleSynthesisCostID))
                {
                    dicPosSum["settlecost_" + settle.SettleSynthesisCostID] = new List<int>();
                }
                sheet.SetCellValue(startRow, 1, "'" + settle.ActionCode);
                sheet.SetCellValue(startRow, 2, "'" + settle.ActionTitle);
                sheet.SetCellValue(startRow, 3, settle.PlanSetttleMoney.ToString());
                sheet.SetCellValue(startRow, 4, settle.SetttleMoney.ToString());
                sheet.SetCellValue(startRow, 5, "'" + settle.Description);
                sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                sheet.SetCellFormat(startRow, 4, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                bool bold = settle.ListChildCost != null && settle.ListChildCost.Any();
                bool italic = settle.ParentID != null && bold;
                sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 12,
                                                    bold, italic);
                startRow++;
                if (settle.ListChildCost != null && settle.ListChildCost.Any())
                {
                    foreach (SettleSynCostModel item in settle.ListChildCost)
                    {
                        this.SetSettleCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                    }
                }
            }
        }

        public void AddSubCost(SubActionSpendItemModel cost,
            List<SubActionSpendItemModel> listSubSpendItem, List<SettleSynCostModel> listCost)
        {
            List<SubActionSpendItemModel> listChild = listSubSpendItem.Where(x => x.ParentID == cost.SubActionSpendItemID).ToList();
            if (listChild != null && listChild.Any())
            {
                cost.ListChild = listChild;
                foreach (SubActionSpendItemModel item in listChild)
                {
                    this.AddSubCost(item, listSubSpendItem, listCost);
                }
            }
        }

        public void SetSubCostData(IXVTWorksheet sheet, SubActionSpendItemModel subSpendItem,
            ref int startRow, Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum)
        {
            if (subSpendItem != null)
            {
                dicPos["subcost_" + subSpendItem.SubActionSpendItemID] = startRow;
                if (subSpendItem.ParentID == null)
                {
                    if (subSpendItem.SubActionPlanID != null)
                    {
                        dicPosSum["subaction_" + subSpendItem.SubActionPlanID.Value].Add(startRow);
                    }
                    else
                    {
                        dicPosSum["targetaction_" + subSpendItem.PlanTargetActionID.Value].Add(startRow);
                    }
                }
                else
                {
                    dicPosSum["subcost_" + subSpendItem.ParentID.Value].Add(startRow);
                }
                if (!dicPosSum.ContainsKey("subcost_" + subSpendItem.SubActionSpendItemID))
                {
                    dicPosSum["subcost_" + subSpendItem.SubActionSpendItemID] = new List<int>();
                }
                sheet.SetCellValue(startRow, 1, "'" + subSpendItem.SpendItemTitle);
                sheet.SetCellValue(startRow, 2, "'" + subSpendItem.SpendItemName);
                sheet.SetCellValue(startRow, 3, subSpendItem.TotalPrice.GetValueOrDefault().ToString());
                sheet.SetCellValue(startRow, 4, subSpendItem.TotalSettleMoney.GetValueOrDefault().ToString());
                sheet.SetCellValue(startRow, 5, "'" + subSpendItem.SettleDescription);
                sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                sheet.SetCellFormat(startRow, 4, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                bool bold = (subSpendItem.ListChild != null && subSpendItem.ListChild.Any())
                    || ((subSpendItem.ListSettleCost != null && subSpendItem.ListSettleCost.Any()));
                bool italic = subSpendItem.ParentID != null && bold;
                sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 12,
                                                    bold, italic);
                startRow++;
                if (subSpendItem.ListChild != null && subSpendItem.ListChild.Any())
                {
                    foreach (SubActionSpendItemModel item in subSpendItem.ListChild)
                    {
                        this.SetSubCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                    }
                }
                if (subSpendItem.ListSettleCost != null && subSpendItem.ListSettleCost.Any())
                {
                    foreach (SettleSynCostModel item in subSpendItem.ListSettleCost)
                    {
                        this.SetSettleCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                    }
                }
            }
        }

        public void UpdateSubActionCost(int subActionID, double cost, List<SettleSynthesisCost> listCost)
        {
            SubActionPlan subAction = this.context.SubActionPlan.Find(subActionID);
            if (subAction != null)
            {
                SettleSynthesisCost subCost = listCost.Where(x => x.SubPlanActionID == subActionID && x.SubActionSpendItemID == null).FirstOrDefault();
                if (subCost != null)
                {
                    subCost.SetttleMoney = subCost.SetttleMoney - cost;
                }
                if (subAction.ParentID != null)
                {
                    this.UpdateSubActionCost(subAction.ParentID.Value, cost, listCost);
                }
            }
        }

        public void UpdateSubSpend(int subSpendID, double cost, List<SettleSynthesisCost> listCost)
        {
            SubActionSpendItem subSpend = this.context.SubActionSpendItem.Find(subSpendID);
            if (subSpend != null)
            {
                SettleSynthesisCost subCost = listCost.Where(x => x.SubActionSpendItemID == subSpendID).FirstOrDefault();
                if (subCost != null)
                {
                    subCost.SetttleMoney = subCost.SetttleMoney - cost;
                }
                if (subCost.ParentID != null)
                {
                    this.UpdateSubSpend(subSpend.ParentID.Value, cost, listCost);
                }
            }
        }

        public void UpdateSettleCost(long synCostID, double cost, List<SettleSynthesisCost> listCost)
        {
            SettleSynthesisCost subCost = listCost.Where(x => x.SettleSynthesisCostID == synCostID).FirstOrDefault();
            if (subCost != null)
            {
                subCost.SetttleMoney = subCost.SetttleMoney - cost;
            }
            if (subCost.ParentID != null)
            {
                this.UpdateSettleCost(subCost.ParentID.Value, cost, listCost);
            }
        }

        public void RemoveSettleCost(SettleSynthesisCost entity)
        {
            List<SettleSynthesisCost> listCost = this.context.SettleSynthesisCost.Where(x => x.ParentID == entity.SettleSynthesisCostID).ToList();
            if (listCost != null && listCost.Any())
            {
                foreach (SettleSynthesisCost cost in listCost)
                {
                    this.RemoveSettleCost(cost);
                }
            }
            this.context.SettleSynthesisCost.Remove(entity);
        }

        public void DelSettleCost(long synCostID)
        {
            SettleSynthesisCost cost = this.context.SettleSynthesisCost.Find(synCostID);
            if (cost == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            // Update cac du lieu lien quan
            List<SettleSynthesisCost> listCost = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == cost.SettleSynthesisID).ToList();
            if (cost.SubPlanActionID != null)
            {
                this.UpdateSubActionCost(cost.SubPlanActionID.Value, cost.SetttleMoney, listCost);
            }
            if (cost.SubActionSpendItemID != null)
            {
                this.UpdateSubSpend(cost.SubActionSpendItemID.Value, cost.SetttleMoney, listCost);
            }
            if (cost.ParentID != null)
            {
                this.UpdateSettleCost(cost.ParentID.Value, cost.SetttleMoney, listCost);
            }
            this.RemoveSettleCost(cost);
            this.context.SaveChanges();
        }

        private SettleSynthesisCost InitCost(SettleSynthesisCost cost, SettleSynthesis entity, PlanTarget pt, List<SubActionPlanCost> listPlanCost)
        {
            if (cost == null)
            {
                cost = new SettleSynthesisCost();
                cost.SettleSynthesisID = entity.SettleSynthesisID;
                cost.PlanTargetID = pt.PlanTargetID;
                cost.ActionCode = pt.TargetCode;
                cost.ActionTitle = pt.PlanTargetContent;
                cost.PlanSetttleMoney = listPlanCost.Where(x => x.PlanTargetID == pt.PlanTargetID
                && x.PlanTargetActionID == null && x.SubActionSpendItemID == null
                && x.PlanYear == entity.Year).Select(x => x.TotalPrice).FirstOrDefault();
            }
            return cost;
        }

        public void Save(SettleCostData data, int planTargetID)
        {
            SettleSynthesis entity = this.context.SettleSynthesis.Find(data.SettleSynthesisID);
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            PlanTarget pt = this.context.PlanTarget.Find(planTargetID);
            if (pt == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            UserInfo user = SessionManager.GetUserInfo();
            bool isReview = user.DepartmentID != entity.DepartmentID;
            List<SettleSynthesisCost> listSynCost = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == data.SettleSynthesisID
            && x.PlanTargetID == planTargetID).ToList();
            List<SubActionPlanCost> listPlanCost = this.context.SubActionPlanCost.Where(x => x.PlanTargetID == planTargetID).ToList();
            if (entity.Status == Constants.PLAN_NEXT_LEVER)
            {
                // Luu comment neu co
                if (!string.IsNullOrWhiteSpace(data.Comment))
                {
                    Comment comment = new Comment();
                    comment.CommentDate = DateTime.Now;
                    comment.CommentUserID = SessionManager.GetUserInfo().UserID;
                    comment.Content = data.Comment.Trim();
                    comment.ObjectID = planTargetID;
                    comment.Type = Constants.COMMENT_TYPE_SETTLE;
                    this.context.Comment.Add(comment);
                }
            }
            List<PlanTargetAction> listTargetAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID).ToList();
            var listSubAction = (from s in this.context.SubActionPlan
                                 join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 where p.PlanTargetID == planTargetID
                                 select new
                                 {
                                     PlanTargetID = p.PlanTargetID.Value,
                                     p.PlanTargetActionID,
                                     s.SubActionPlanID,
                                     s.TotalBudget,
                                     s.SubActionCode,
                                     s.SubActionContent
                                 }).ToList();
            var listSubActionSpend = (from s in this.context.SubActionSpendItem
                                      join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                      where p.PlanTargetID == planTargetID
                                      select new
                                      {
                                          PlanTargetID = p.PlanTargetID.Value,
                                          p.PlanTargetActionID,
                                          s.SubActionPlanID,
                                          s.SubActionSpendItemID,
                                          s.TotalPrice,
                                          s.SpendItemTitle,
                                          s.SpendItemName
                                      }).ToList();
            for (int i = 0; i < data.IDs.Length; i++)
            {
                long? synCostID = data.SyncCostIDs[i];
                int id = data.IDs[i];
                string type = data.Types[i];
                string value = data.ArrValue[i];
                string des = data.ArrDesc[i];
                if (synCostID != null && synCostID.Value > 0)
                {
                    SettleSynthesisCost cost = listSynCost.Where(x => x.SettleSynthesisCostID == synCostID).FirstOrDefault();
                    if (cost == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    if (entity.Status == Constants.PLAN_NEXT_LEVER)
                    {
                        if (isReview)
                        {
                            if (cost.UpdateType == null || cost.UpdateType == Constants.UPDATE_TYPE_TAO)
                            {
                                cost.DescriptionChange = cost.Description;
                                cost.SetttleMoneyChange = cost.SetttleMoney;
                                cost.UpdateType = Constants.UPDATE_TYPE_REVIEW;
                            }
                        }
                        else if (cost.UpdateType.GetValueOrDefault() == Constants.UPDATE_TYPE_REVIEW)
                        {
                            cost.DescriptionChange = cost.Description;
                            cost.SetttleMoneyChange = cost.SetttleMoney;
                            cost.UpdateType = Constants.UPDATE_TYPE_TAO;
                        }
                    }
                    cost.Description = (string.IsNullOrWhiteSpace(des) ? null : des.Trim());
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        double d;
                        if (double.TryParse(value.Trim(), out d))
                        {
                            cost.SetttleMoney = d;
                        }
                        else
                        {
                            throw new BusinessException("Số tiền quyết toán không phải là kiểu số");
                        }
                    }
                    else
                    {
                        cost.SetttleMoney = 0;
                    }
                }
                else
                {
                    SettleSynthesisCost cost = null;
                    if ("target".Equals(type))
                    {
                        cost = listSynCost.Where(x => x.PlanTargetID == id && x.PlanTargetActionID == null && x.SubPlanActionID == null && x.SubActionSpendItemID == null).FirstOrDefault();
                        cost = InitCost(cost, entity, pt, listPlanCost);
                        cost.PlanTargetID = id;
                        cost.PlanSetttleMoney = listPlanCost.Where(x => x.PlanTargetID == id && x.PlanTargetActionID == null
                        && x.SubActionPlanID == null && x.SubActionSpendItemID == null && x.PlanYear == entity.Year).Select(x => x.TotalPrice).FirstOrDefault();
                        cost.ActionCode = pt.TargetCode;
                        cost.ActionTitle = pt.PlanTargetTitle;
                    }
                    else if ("targetaction".Equals(type))
                    {
                        PlanTargetAction pta = listTargetAction.Where(x => x.PlanTargetActionID == id).FirstOrDefault();
                        if (pta == null)
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                        cost = listSynCost.Where(x => x.PlanTargetActionID == id && x.SubPlanActionID == null && x.SubActionSpendItemID == null).FirstOrDefault();
                        cost = InitCost(cost, entity, pt, listPlanCost);
                        cost.PlanTargetActionID = pta.PlanTargetActionID;
                        cost.PlanSetttleMoney = listPlanCost.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID
                        && x.SubActionPlanID == null && x.SubActionSpendItemID == null && x.PlanYear == entity.Year).Select(x => x.TotalPrice).FirstOrDefault();
                        cost.ActionCode = pta.ActionCode;
                        cost.ActionTitle = pta.ActionContent;
                    }
                    else if ("subaction".Equals(type))
                    {
                        var subAction = listSubAction.Where(x => x.SubActionPlanID == id).FirstOrDefault();
                        if (subAction == null)
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                        cost = listSynCost.Where(x => x.SubPlanActionID == id && x.SubActionSpendItemID == null).FirstOrDefault();
                        cost = InitCost(cost, entity, pt, listPlanCost);
                        cost.PlanTargetActionID = subAction.PlanTargetActionID;
                        cost.SubPlanActionID = subAction.SubActionPlanID;
                        cost.PlanSetttleMoney = listPlanCost.Where(x => x.SubActionPlanID == subAction.SubActionPlanID
                        && x.SubActionSpendItemID == null && x.PlanYear == entity.Year).Select(x => x.TotalPrice).FirstOrDefault();
                        cost.ActionCode = subAction.SubActionCode;
                        cost.ActionTitle = subAction.SubActionContent;
                    }
                    else if ("subcost".Equals(type))
                    {
                        var subCost = listSubActionSpend.Where(x => x.SubActionSpendItemID == id).FirstOrDefault();
                        if (subCost == null)
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                        cost = listSynCost.Where(x => x.SubPlanActionID == null && x.SubActionSpendItemID == id).FirstOrDefault();
                        cost = InitCost(cost, entity, pt, listPlanCost);
                        cost.PlanTargetActionID = subCost.PlanTargetActionID;
                        cost.SubPlanActionID = subCost.SubActionPlanID;
                        cost.SubActionSpendItemID = subCost.SubActionSpendItemID;
                        cost.PlanSetttleMoney = listPlanCost.Where(x => x.SubActionSpendItemID == subCost.SubActionSpendItemID && x.PlanYear == entity.Year).Select(x => x.TotalPrice).FirstOrDefault();
                        cost.ActionCode = subCost.SpendItemTitle;
                        cost.ActionTitle = subCost.SpendItemName;
                    }
                    cost.Description = (string.IsNullOrWhiteSpace(des) ? null : des.Trim());
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        double d;
                        if (double.TryParse(value.Trim(), out d))
                        {
                            cost.SetttleMoney = d;
                        }
                        else
                        {
                            throw new BusinessException("Số tiền quyết toán không phải là kiểu số");
                        }
                    }
                    else
                    {
                        cost.SetttleMoney = 0;
                    }
                    if (cost.SettleSynthesisCostID == 0)
                    {
                        this.context.SettleSynthesisCost.Add(cost);
                    }
                }
            }
            this.context.SaveChanges();
            this.UpdateTotalCost(entity.SettleSynthesisID);
            this.context.SaveChanges();
            FinancialReportBusiness financialReportBus = new FinancialReportBusiness(context);
            financialReportBus.InitFinancialReportFromSettle(entity);
            // Xoa du lieu thua
            this.context.Database.ExecuteSqlCommand("delete from SettleSynthesisCost where SettleSynthesisID = " + entity.SettleSynthesisID
                + " and SetttleMoney = 0");
            this.context.SaveChanges();
        }

        public Paginate<SettleInfoModel> GetListSettleReport(int userID, int periodID, int deptID, int status, int quarter, int year, int page)
        {
            List<int> listYear = new List<int>();
            List<SettleInfoModel> listReport =
                (from s in this.context.SettleSynthesis
                 join pl in this.context.Plan on s.PlanID equals pl.PlanID
                 join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                 join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                 where (deptID == 0 || pl.DepartmentID == deptID) && pl.Status == Constants.PLAN_APPROVED && d.Status != Constants.IS_NOT_ACTIVE
                 && pl.Status != Constants.IS_NOT_ACTIVE && pr.Status != Constants.IS_NOT_ACTIVE && s.Status != Constants.IS_NOT_ACTIVE
                 && s.Status != Constants.IS_NOT_ACTIVE && (periodID == 0 || pr.PeriodID == periodID)
                 && (year == 0 || s.Year == year) && (quarter == 0 || s.Quarter == quarter)
                && (status == 0 || (status == Constants.REPORT_NEW)
                || (status == Constants.REPORT_SENT && s.Status >= Constants.PLAN_NEXT_LEVER))
                 select new SettleInfoModel
                 {
                     CreateDate = s.CreateDate,
                     Creator = s.Creator,
                     PeriodID = s.PlanID,
                     PlanName = pr.PeriodName,
                     Quarter = s.Quarter,
                     Year = s.Year,
                     SettleSynthesisID = s.SettleSynthesisID,
                     TotalPlanMoney = s.TotalPlanMoney,
                     TotalReceivedMoney = s.TotalReceivedMoney,
                     TotalSettleMoney = s.TotalSettleMoney,
                     TottalDisbMoney = s.TottalDisbMoney,
                     Type = s.Type,
                     Status = s.Status,
                     DeptID = s.DepartmentID.Value,
                     DeptName = d.DepartmentName,
                     SendDate = s.SendDate,
                     IsHasReview = this.context.ReportReview.Any(x => x.ReportID == s.SettleSynthesisID && x.ReportType == Constants.REPORT_TYPE_SETTLE)
                 }).ToList();

            Period period = context.Period.Find(periodID);
            if (year == 0)
            {
                for (int i = period.FromDate.Year; i <= period.ToDate.Year; i++)
                {
                    listYear.Add(i);
                }
            }
            else
            {
                listYear.Add(year);
            }

            List<int> listQuarter = new List<int> { 1, 2, 3, 4 };
            if (quarter > 0)
            {
                listQuarter.Clear();
                listQuarter.Add(quarter);
            }
            DateTime now = DateTime.Now;
            int quarterNow = (now.Month % 3 == 0) ? now.Month / 3 : (now.Month / 3) + 1;

            // Lay quyen
            List<Department> listDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE && (deptID == 0 || x.DepartmentID == deptID)).ToList();
            Dictionary<int, ObjectRoleBO> dicRoleDept = new FlowProcessBusiness(this.context).GetRoleForDept(userID, listDept, Constants.COMMENT_TYPE_SETTLE);
            listDept.RemoveAll(x => !dicRoleDept.ContainsKey(x.DepartmentID) || dicRoleDept[x.DepartmentID].Role == Constants.ROLE_NO_VIEW);
            listReport = (from q in listQuarter
                          join y in listYear on 0 equals 0
                          join d in listDept on 0 equals 0
                          join l in listReport on new { DeptID = d.DepartmentID, Quarter = q, Year = y } equals new { DeptID = l.DeptID, Quarter = l.Quarter.GetValueOrDefault(), Year = l.Year } into lj
                          from s in lj.DefaultIfEmpty()
                          where (status == 0 || (status == Constants.REPORT_NEW && (s == null || s.Status == Constants.PLAN_NEW))
                || (status == Constants.REPORT_SENT && s != null && s.Status >= Constants.PLAN_NEXT_LEVER))
                && (y < now.Year || (y == now.Year && q <= quarterNow))
                          select new SettleInfoModel
                          {
                              CreateDate = s == null ? null : s.CreateDate,
                              PeriodID = periodID,
                              PlanID = s == null ? 0 : s.PlanID,
                              PlanName = s == null ? period.PeriodName : s.PlanName,
                              Quarter = q == 0 ? null : (int?)q,
                              Year = y,
                              SettleSynthesisID = s == null ? 0 : s.SettleSynthesisID,
                              Type = s == null ? 0 : s.Type,
                              Status = s == null ? 0 : s.Status,
                              DeptID = s == null ? d.DepartmentID : s.DeptID,
                              DeptName = s == null ? d.DepartmentName : s.DeptName,
                              SendDate = s == null ? null : s.SendDate,
                              IsHasReview = s == null ? false : s.IsHasReview
                          }).ToList();

            List<int> listReportID = listReport.Where(x => x.SettleSynthesisID > 0).Select(x => x.SettleSynthesisID).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.context).GetRoleForObject(userID, listReportID, Constants.COMMENT_TYPE_SETTLE);
            listReport.RemoveAll(x => x.SettleSynthesisID > 0 && dicRole[x.SettleSynthesisID].Role == Constants.ROLE_NO_VIEW);
            listReport.ForEach(x =>
            {
                if (x.SettleSynthesisID > 0)
                {
                    x.Role = dicRole[x.SettleSynthesisID].Role;
                }
                else
                {
                    x.Role = dicRoleDept[x.DeptID].Role;
                }
            });
            Paginate<SettleInfoModel> paginate = new Paginate<SettleInfoModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = listReport.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter)
                .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = listReport.Count;
            return paginate;
        }

        public byte[] Export(int settleSynthesisId, int year, string tempFilePath)
        {
            var entity = (from s in context.SettleSynthesis
                          join p in context.Plan on s.PlanID equals p.PlanID
                          join d in context.Department on p.DepartmentID equals d.DepartmentID
                          join pr in context.Period on p.PeriodID equals pr.PeriodID
                          where s.SettleSynthesisID == settleSynthesisId && s.Status != Constants.PLAN_DEACTIVED
                          select new
                          {
                              pr.PeriodID,
                              s.SettleSynthesisID,
                              s.Year,
                              s.Quarter,
                              s.PlanID,
                              s.TotalSettleMoney,
                              s.TotalPlanMoney,
                              s.Status,
                              pr.PeriodName,
                              d.DepartmentName,
                              s.SendDate
                          }).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            var plan = context.Plan.Find(entity.PlanID);

            using (MemoryStream ms = new MemoryStream())
            {
                using (FileStream file = new FileStream(tempFilePath, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);

                    IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                    IXVTWorksheet sheet = oBook.GetSheet(1);
                    sheet.SheetName = "Phu luc 06B - Bang THQT Quy " + Utils.ToRoman(entity.Quarter.Value);

                    string reportName = "QUÝ " + Utils.ToQuarter(entity.Quarter) + "/" +
                                        entity.Year;

                    sheet.SetCellValue(1, 1, "Tên đơn vị: " + entity.DepartmentName);
                    sheet.SetCellValue(2, 1, "BẢNG TỔNG HỢP QUYẾT TOÁN");
                    sheet.SetCellValue(3, 1, reportName);
                    sheet.SetCellValue(4, 1, "Quyết toán hợp đồng số .............. ngày ../../.... ");
                    sheet.SetCellValue(5, 1, "Nguồn kinh phí: Quỹ Phòng, chống tác hại của thuốc lá - Bộ Y tế");

                    int startRow = 10;

                    List<PlanTargetModel> lstPlanTarget = (from pt in context.PlanTarget
                                                           where pt.PlanID == plan.PlanID
                                                           orderby pt.TargetOrder
                                                           select new PlanTargetModel
                                                           {
                                                               PlanTargetID = pt.PlanTargetID,
                                                               PlanTargetTitle = pt.PlanTargetTitle,
                                                               TargetCode = pt.TargetCode,
                                                               PlanTargetContent = pt.PlanTargetContent,
                                                               TotalBudget = pt.TotalBuget
                                                           }).ToList();

                    var info = CultureInfo.GetCultureInfo("vi-VN");
                    double totalCost = (from sc in this.context.SubActionPlanCost
                                        join pt in this.context.PlanTarget on sc.PlanTargetID equals pt.PlanTargetID
                                        where pt.PlanID == plan.PlanID && sc.PlanYear == year && sc.PlanTargetActionID == null
                                        select sc.TotalPrice == null ? 0 : sc.TotalPrice.Value
                                  ).DefaultIfEmpty(0).Sum();
                    sheet.SetCellValue(6, 1, "Tổng ngân sách được duyệt: " + String.Format(info, "{0:c}", totalCost));
                    sheet.SetCellValue(7, 1, "Kinh phí đã nhận : ");
                    Dictionary<string, List<int>> dicPosSum = new Dictionary<string, List<int>>();
                    Dictionary<string, int> dicPos = new Dictionary<string, int>();
                    if (lstPlanTarget.Count > 0)
                    {
                        foreach (var pt in lstPlanTarget)
                        {
                            if (startRow > 10)
                            {
                                sheet.CopyRow(10, startRow);
                            }
                            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(context);
                            PlanTargetModel planTarget = bus.GetListCost(0, pt.PlanTargetID, settleSynthesisId);
                            sheet.SetCellValue(startRow, 1, "'" + planTarget.TargetCode);
                            sheet.SetCellValue(startRow, 2, "'" + planTarget.PlanTargetContent);
                            sheet.SetCellValue(startRow, 3, planTarget.TotalBudget.ToString());
                            sheet.SetCellValue(startRow, 4, planTarget.TotalSettleMoney.ToString());
                            sheet.SetCellValue(startRow, 5, "'" + planTarget.SettleDescription);
                            sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            sheet.SetCellFormat(startRow, 4, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 12,
                                                           true, false);
                            dicPos["target_" + pt.PlanTargetID] = startRow;
                            dicPosSum["target_" + pt.PlanTargetID] = new List<int>();
                            startRow++;

                            if (planTarget.ListPlanTargetActionModel.Count > 0)
                            {
                                foreach (var action in planTarget.ListPlanTargetActionModel)
                                {
                                    if (startRow > 11)
                                    {
                                        sheet.CopyRow(11, startRow);
                                    }
                                    dicPos["targetaction_" + action.PlanTargetActionID] = startRow;
                                    dicPosSum["target_" + pt.PlanTargetID].Add(startRow);
                                    dicPosSum["targetaction_" + action.PlanTargetActionID] = new List<int>();
                                    sheet.SetCellValue(startRow, 1, "'" + action.ActionCode);
                                    sheet.SetCellValue(startRow, 2, "'" + action.ActionContent);
                                    sheet.SetCellValue(startRow, 3, action.TotalBudget.ToString());
                                    sheet.SetCellValue(startRow, 4, action.TotalSettleMoney.ToString());
                                    sheet.SetCellValue(startRow, 5, "'" + action.SettleDescription);
                                    startRow++;

                                    if (action.ListSubActionPlan != null && action.ListSubActionPlan.Any())
                                    {
                                        foreach (SubActionPlanModel item in action.ListSubActionPlan)
                                        {
                                            this.SetSubActionData(sheet, item, ref startRow, dicPos, dicPosSum);
                                        }
                                    }
                                    else
                                    {
                                        if (action.ListSubActionSpendItem != null && action.ListSubActionSpendItem.Any())
                                        {
                                            foreach (SubActionSpendItemModel item in action.ListSubActionSpendItem)
                                            {
                                                this.SetSubCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                                            }
                                        }
                                        if (action.ListSettleCost != null && action.ListSettleCost.Any())
                                        {
                                            foreach (SettleSynCostModel item in action.ListSettleCost)
                                            {
                                                this.SetSettleCostData(sheet, item, ref startRow, dicPos, dicPosSum);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Thiet lap cong thuc tinh tong
                        List<string> listKey = dicPos.Keys.ToList();
                        string totalSum = string.Empty;
                        string totalSumPlan = string.Empty;
                        foreach (string key in listKey)
                        {
                            int posSum = dicPos[key];
                            if (key.StartsWith("target_"))
                            {
                                totalSum += ", " + Utils.GetExcelColumnName(4) + posSum;
                                totalSumPlan += ", " + Utils.GetExcelColumnName(3) + posSum;
                            }
                            if (dicPosSum.ContainsKey(key))
                            {
                                List<int> listPosSum = dicPosSum[key];
                                if (listPosSum != null && listPosSum.Any())
                                {
                                    string sum = string.Empty;
                                    foreach (int pos in listPosSum)
                                    {
                                        string colName = Utils.GetExcelColumnName(4);
                                        sum += ", " + colName + pos;
                                    }
                                    sheet.SetCellFormula(posSum, 4, "SUM(" + sum.Substring(2) + ")");
                                }
                            }
                        }
                        sheet.SetCellValue(startRow, 2, "Tổng cộng");
                        sheet.SetCellFormula(startRow, 3, "SUM(" + totalSumPlan.Substring(2) + ")");
                        sheet.SetCellFormula(startRow, 4, "SUM(" + totalSum.Substring(2) + ")");

                        int rowtong = startRow;

                        sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        sheet.SetCellFormat(startRow, 4, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 11, true, false);
                        string val = sheet.GetValueOfCell(startRow, 4).ToString();
                        string bangchu = "";
                        if (!string.IsNullOrEmpty(val))
                        {
                            bangchu = Utils.ChuyenSo(val);
                            bangchu = "Bằng chữ: " + bangchu;
                            startRow++;
                            sheet.GetRange(startRow, 1, startRow, 5).Range.Merge();
                            sheet.SetCellValue(startRow, 1, bangchu);
                        }
                        sheet.SetRangeFont(startRow, 1, startRow, 5, "Times New Roman", 11, true, false);
                        sheet.SetRangeBorderThin(7, 1, startRow, 5);
                        startRow++;
                        startRow++;
                        int origin = startRow;

                        // Lay thong tin giai ngan
                        List<Disbursement> listDisb = context.Disbursement.Where(x => x.PlanID == plan.PlanID).OrderBy(x => x.Number).ToList();
                        double totalDisb = 0;
                        string sumDisb = "C" + startRow;
                        foreach (Disbursement disb in listDisb)
                        {
                            totalDisb += disb.Money.GetValueOrDefault();
                            sheet.SetCellValue(startRow, 2, "Tạm ứng lần " + disb.Number + ":");
                            sheet.SetCellValue(startRow, 3, disb.Money.GetValueOrDefault().ToString());
                            sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            startRow++;
                        }
                        sheet.SetCellValue(startRow, 2, "Tổng số tạm ứng:");
                        if (listDisb.Any())
                        {
                            sheet.SetCellFormula(startRow, 3, "SUM(" + sumDisb + ":C" + (startRow - 1) + ")");
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, 3, "0");
                        }
                        sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        startRow++;
                        sheet.SetCellValue(startRow, 2, "Tổng số giải ngân: ");
                        sheet.SetCellFormula(startRow, 3, "D" + (startRow - 4 - listDisb.Count));
                        sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        startRow++;
                        sheet.SetCellValue(startRow, 2, "Chênh lệch tạm ứng và giải ngân: ");
                        sheet.SetCellFormula(startRow, 3, "C" + (startRow - 2) + "-C" + (startRow - 1));
                        sheet.SetCellFormat(startRow, 3, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");

                        sheet.SetRangeFont(origin, 1, startRow, 6, "Times New Roman", 12, false, false);
                        sheet.SetRangeBorderThin(origin, 2, startRow, 4);

                        startRow++;
                        startRow++;

                        sheet.GetRange(startRow, 4, startRow, 5).Range.Merge();
                        sheet.SetCellValue(startRow, 4, "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                        sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11, false, true);

                        sheet.GetRange(startRow, 4, startRow, 5).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;
                        startRow++;

                        sheet.GetRange(startRow, 1, startRow, 2).Range.Merge();
                        sheet.GetRange(startRow, 3, startRow, 4).Range.Merge();

                        sheet.SetCellValue(startRow, 1, "Người lập báo cáo");
                        sheet.SetCellValue(startRow, 3, "Kế toán trưởng");
                        sheet.SetCellValue(startRow, 5, "Thủ trưởng đơn vị");
                        sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11, true, false);
                        startRow++;
                        sheet.SetCellValue(startRow, 5, "(Ký tên, đóng dấu)");
                        sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11, false, true);
                    }

                    sheet.SetSheetWrapText();
                    Stream streamToWrite = oBook.ToStream();
                    byte[] bytesToWrite = new byte[streamToWrite.Length];
                    streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                    string newFile = "Bao_cao_quyet_toan_" + Utils.ToQuarter(entity.Quarter) + "/" +
                                     entity.Year + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    return bytesToWrite.ToArray();
                }
            }
        }

        private void SetPlanTargetActionCostToDict(Dictionary<int, List<SettleSynthesisCost>> dicPlanTargetActionCost, SettleSynthesisCost item)
        {
            if (!dicPlanTargetActionCost.ContainsKey(item.PlanTargetActionID.GetValueOrDefault()))
            {
                dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()] = new List<SettleSynthesisCost>();
            }
            dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()].Add(item);
        }


        private void UpdateTotalCost(int settleReportID)
        {
            List<SettleSynthesisCost> listCost = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == settleReportID).ToList();
            // Chi phi theo hang muc con
            List<int> listCostSpendID = listCost.Where(x => x.SubActionSpendItemID != null).Select(x => x.SubActionSpendItemID.Value).Distinct().ToList();
            List<SubActionSpendItem> listSpend = this.context.SubActionSpendItem.Where(x => listCostSpendID.Contains(x.SubActionSpendItemID)).ToList();
            List<int> listRootSpendItemID = listSpend.Where(x => x.ParentID == null).Select(x => x.SubActionSpendItemID).ToList();
            List<SettleSynthesisCost> listRootSpendCost = listCost.Where(x => x.SubActionSpendItemID != null && listRootSpendItemID.Contains(x.SubActionSpendItemID.Value)).ToList();
            List<int> listSubActionID = listCost.Where(x => x.SubPlanActionID != null).Select(x => x.SubPlanActionID.Value).Distinct().ToList();
            List<SubActionPlan> listSubAction = this.context.SubActionPlan.Where(x => listSubActionID.Contains(x.SubActionPlanID)).ToList();
            Dictionary<int, List<SettleSynthesisCost>> dicPlanTargetActionCost = new Dictionary<int, List<SettleSynthesisCost>>();
            foreach (SettleSynthesisCost item in listRootSpendCost)
            {
                this.UpdateSpendCost(item, listCost, listSpend, listSubAction);
                if (item.SubPlanActionID == null && item.PlanTargetActionID != null)
                {
                    this.SetPlanTargetActionCostToDict(dicPlanTargetActionCost, item);
                }
            }
            List<SettleSynthesisCost> listRootSubActionCost = listCost.Where(x => x.SubPlanActionID != null && x.SubActionSpendItemID == null
            && listSubAction.Any(s => s.ParentID == null && s.SubActionPlanID == x.SubPlanActionID)).ToList();
            foreach (SettleSynthesisCost item in listRootSubActionCost)
            {
                this.UpdateTreeSubCost(item, listCost, listSubAction);
                this.SetPlanTargetActionCostToDict(dicPlanTargetActionCost, item);
            }

            #region Cap nhat chi phi tong vao hoat dong
            // Update rieng phan muc tieu
            List<int> listPlanTargetID = listCost.Where(x => x.PlanTargetID != null).Select(x => x.PlanTargetID.Value).Distinct().ToList();
            List<SettleSynthesisCost> listTargetCost = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == settleReportID
            && x.PlanTargetID != null && x.PlanTargetActionID == null && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
            foreach (SettleSynthesisCost targetCost in listTargetCost)
            {
                targetCost.SetttleMoney = 0;
                List<SettleSynthesisCost> listPlanTargetActionCost = listCost.Where(x => x.PlanTargetID == targetCost.PlanTargetID && x.PlanTargetActionID != null
                && x.SubPlanActionID == null && x.SubActionSpendItemID == null).ToList();
                if (listPlanTargetActionCost != null && listPlanTargetActionCost.Any())
                {
                    foreach (SettleSynthesisCost item in listPlanTargetActionCost)
                    {
                        if (dicPlanTargetActionCost.ContainsKey(item.PlanTargetActionID.GetValueOrDefault()))
                        {
                            List<SettleSynthesisCost> listChildPlanTargetActionCost = dicPlanTargetActionCost[item.PlanTargetActionID.GetValueOrDefault()].ToList();
                            item.SetttleMoney = listChildPlanTargetActionCost.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                        }
                    }
                    targetCost.SetttleMoney = listPlanTargetActionCost.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                }
            }
            #endregion
        }

        public double UpdateSpendCost(SettleSynthesisCost cost, List<SettleSynthesisCost> listCost, List<SubActionSpendItem> listSpend,
            List<SubActionPlan> listSubAction)
        {
            double totalMoney = 0;
            List<SubActionSpendItem> listSpendChild = listSpend.Where(x => x.ParentID == cost.SubActionSpendItemID).ToList();
            SubActionSpendItem spend = listSpend.Where(x => x.SubActionSpendItemID == cost.SubActionSpendItemID).FirstOrDefault();
            if (listSpendChild != null && listSpendChild.Any())
            {
                foreach (SubActionSpendItem child in listSpendChild)
                {
                    SettleSynthesisCost childCost = listCost.Where(x => x.SubActionSpendItemID == child.SubActionSpendItemID).FirstOrDefault();
                    if (childCost != null)
                    {
                        totalMoney += this.UpdateSpendCost(childCost, listCost, listSpend, listSubAction);
                    }
                }
                cost.SetttleMoney = totalMoney;
            }
            else
            {
                totalMoney = cost.SetttleMoney;
            }
            if (spend != null && spend.ParentID == null)
            {
                SettleSynthesisCost parCost = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                && x.SubPlanActionID == cost.SubPlanActionID && x.SubActionSpendItemID == null).FirstOrDefault();
                if (parCost != null)
                {
                    List<int> listRootSpend = listSpend.Where(x => x.PlanTargetActionID == parCost.PlanTargetActionID
                            && x.SubActionPlanID == parCost.SubPlanActionID && x.ParentID == null).Select(x => x.SubActionSpendItemID).ToList();
                    var listCostRoot = listCost.Where(x => x.PlanTargetID == cost.PlanTargetID && x.PlanTargetActionID == cost.PlanTargetActionID
                            && x.SubPlanActionID == cost.SubPlanActionID && x.SubActionSpendItemID != null && listRootSpend.Contains(x.SubActionSpendItemID.Value)).Select(x => x.SetttleMoney).ToList();
                    parCost.SetttleMoney = listCostRoot.Sum();
                }
            }
            return totalMoney;
        }

        public double UpdateTreeSubCost(SettleSynthesisCost cost, List<SettleSynthesisCost> listCost, List<SubActionPlan> listSubAction)
        {
            double totalMoney = 0;
            List<SubActionPlan> listChildAction = listSubAction.Where(x => x.ParentID == cost.SubPlanActionID).ToList();
            if (listChildAction != null && listChildAction.Any())
            {
                SubActionPlan subAction = listSubAction.Where(x => x.SubActionPlanID == cost.SubPlanActionID).FirstOrDefault();
                foreach (SubActionPlan child in listChildAction)
                {
                    SettleSynthesisCost childCost = listCost.Where(x => x.SubPlanActionID == child.SubActionPlanID).FirstOrDefault();
                    if (childCost != null)
                    {
                        totalMoney += this.UpdateTreeSubCost(childCost, listCost, listSubAction);
                    }
                }
                cost.SetttleMoney = totalMoney;
            }
            else
            {
                totalMoney = cost.SetttleMoney;
            }
            return totalMoney;
        }

    }
}