﻿using IInterface.Common;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IInterface.Business
{
    public class SubActionPlanCostBusiness
    {
        private Entities context;
        int userID = SessionManager.GetUserInfo().UserID;
        DateTime now = DateTime.Now;
        public SubActionPlanCostBusiness(Entities context)
        {
            this.context = context;
        }
        public void addSubActionPlanCost(int startYear, int endYear, int planTargetActionID, int planTargetID)
        {
            if (endYear >= startYear)
            {
                for (int i = startYear; i <= endYear; i++)
                {
                    List<SubActionPlanCost> lstSubActionPlanCost = context.SubActionPlanCost
                            .Where(x => x.PlanTargetActionID == planTargetActionID && (x.PlanYear == i))
                            .ToList();
                    if (!lstSubActionPlanCost.Any())
                    {
                        SubActionPlanCost subActionPlanCost = new SubActionPlanCost();
                        //   subActionPlanTime.PlanTime = starRow;
                        subActionPlanCost.PlanTargetActionID = planTargetActionID;
                        subActionPlanCost.Unit = null;
                        subActionPlanCost.Price = null;
                        subActionPlanCost.TotalPrice = null;
                        subActionPlanCost.Quantity = null;
                        subActionPlanCost.SumTotalBudget = null;
                        subActionPlanCost.PlanYear = i;
                        subActionPlanCost.CreateDate = now;
                        subActionPlanCost.CreateUserID = userID;
                        subActionPlanCost.UpdateDate = now;
                        subActionPlanCost.ModifierID = userID;
                        subActionPlanCost.Status = Constants.IS_ACTIVE;
                        subActionPlanCost.PlanTargetID = planTargetID;
                        context.SubActionPlanCost.Add(subActionPlanCost);
                    }
                  

                }
                context.SaveChanges();
            }

        }
        public void addSubActionPlanCost(int startYear, int endYear, int planTargetActionID,int subActionPlanId, int planTargetID)
        {
            if (endYear >= startYear)
            {
                for (int i = startYear; i <= endYear; i++)
                {
                    List<SubActionPlanCost> lstSubActionPlanCost = context.SubActionPlanCost
                    .Where(x => x.SubActionPlanID == subActionPlanId && (x.PlanYear == i))
                    .ToList();
                    if (!lstSubActionPlanCost.Any())
                    {
                        SubActionPlanCost subActionPlanCost = new SubActionPlanCost();
                        //   subActionPlanTime.PlanTime = starRow;
                        subActionPlanCost.PlanTargetActionID = planTargetActionID;
                        subActionPlanCost.SubActionPlanID = subActionPlanId;
                        subActionPlanCost.Unit = null;
                        subActionPlanCost.Price = null;
                        subActionPlanCost.TotalPrice = null;
                        subActionPlanCost.Quantity = null;
                        subActionPlanCost.SumTotalBudget = null;
                        subActionPlanCost.PlanYear = i;
                        subActionPlanCost.CreateDate = now;
                        subActionPlanCost.CreateUserID = userID;
                        subActionPlanCost.UpdateDate = now;
                        subActionPlanCost.ModifierID = userID;
                        subActionPlanCost.Status = Constants.IS_ACTIVE;
                        subActionPlanCost.PlanTargetID = planTargetID;
                        context.SubActionPlanCost.Add(subActionPlanCost);
                    }
                }
                context.SaveChanges();
            }

        }
    }
}