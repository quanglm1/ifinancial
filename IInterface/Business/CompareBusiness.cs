﻿using System.Collections.Generic;
using System.Linq;
using IInterface.Common;
using IInterface.Models;
using IModel;

namespace IInterface.Business
{
    public class CompareBusiness
    {
        private Entities context;
        public CompareBusiness(Entities context)
        {
            this.context = context;
        }

        private PlanReviewHistory CommonCheck(int planID, int userID, int type)
        {

            Plan plan = context.Plan.Find(planID);
            if (plan == null || plan.Status == Constants.PLAN_NEW || plan.Status == Constants.PLAN_PLANNED)
            { 
                return null;
            }
            if (type == 0 && plan.Status == Constants.PLAN_APPROVED)
            {
                return null;
            }
            if (type == 1 && plan.ChangeStatus == Constants.PLAN_APPROVED)
            {
                return null;
            }
            int? step = type == 0 ? plan.StepNow : plan.ChangeStepNow;
            var reviewHis = context.PlanReviewHistory.Where(x => x.PlanID == planID && (x.Step == null || x.Step <= step))
                .OrderByDescending(x => x.PlanReviewHistoryID).FirstOrDefault();
            return reviewHis;
        }

        public void UpdatePlanTargetChange(int planID, int userID, int planTargetID, int type, List<TargetActionModel> listTargetAction)
        {
            var reviewHis = CommonCheck(planID, userID, type);
            if (reviewHis == null)
            {
                return;
            }
            FlowProcessBusiness fb = new FlowProcessBusiness(context);
            var role = fb.GetRoleForObject(userID, planID, Constants.COMMENT_TYPE_PLAN_TARGET);
            if (role == null || role.Role == 0 || (role.PL != null && role.PL.Value != 1))
            {
                return;
            }

            List<PlanTargetActionHis> listTargetActionHis = context.PlanTargetActionHis
                .Where(x => x.PlanID == planID && x.PlanHisID == reviewHis.PlanHisID && x.PlanTargetID == planTargetID).ToList();
            foreach (var action in listTargetAction)
            {
                var hisAction = listTargetActionHis.Where(x => x.PlanTargetActionID == action.PlanTargetActionID).FirstOrDefault();
                if (hisAction == null)
                {
                    action.IsActionContentChange = true;
                    action.IsActionMethodChange = true;
                    action.IsContentPropagandaChange = true;
                    action.IsObjectScopeChange = true;
                }
                else
                {
                    action.IsActionContentChange = !EqualString(action.ActionContent, hisAction.ActionContent);
                    action.IsActionMethodChange = !EqualString(action.ActionMethod, hisAction.ActionMethod);
                    action.IsContentPropagandaChange = !EqualString(action.ContentPropaganda, hisAction.ContentPropaganda);
                    action.IsObjectScopeChange = !EqualString(action.ObjectScope, hisAction.ObjectScope);
                }
            }
        }

        public void UpdateSubActionSpendItem(int planID, int userID, int type, int? parentID, List<SubActionSpendItemModel> listSubActionSpendItem)
        {
            var reviewHis = CommonCheck(planID, userID, type);
            if (reviewHis == null)
            {
                return;
            }
            List<SubActionSpendItemHis> listSubSpendHis = context.SubActionSpendItemHis
                .Where(x => x.PlanHisID == reviewHis.PlanHisID).ToList();
            CompareSubItem(listSubSpendHis, listSubActionSpendItem);
        }

        private void CompareSubItem(List<SubActionSpendItemHis> listSubSpendHis, List<SubActionSpendItemModel> listSubSpendItem)
        {
            foreach (var item in listSubSpendItem)
            {
                var hisItem = listSubSpendHis.Where(x => x.SubActionSpendItemID == item.SubActionSpendItemID).FirstOrDefault();
                if (hisItem == null)
                {
                    item.IsChangeName = true;
                }
                else
                {
                    item.IsChangeName = !EqualString(item.SpendItemName, hisItem.SpendItemName);
                }
                if (item.ListChild != null && item.ListChild.Any())
                {
                    CompareSubItem(listSubSpendHis, item.ListChild);
                }
            }
        }

        public void UpdateSubActionCost(int planID, int userID, int type, List<SubActionPlanCostModel> listSubActionPlanCost)
        {
            var reviewHis = CommonCheck(planID, userID, type);
            if (reviewHis == null)
            {
                return;
            }
            List<SubActionPlanCostHis> listSubActionPlanCostHis = context.SubActionPlanCostHis
                .Where(x => x.PlanHisID == reviewHis.PlanHisID).ToList();
            foreach (var item in listSubActionPlanCost)
            {
                var hisItem = listSubActionPlanCostHis.Where(x => x.SubActionPlanCostID == item.SubActionPlanCostID).FirstOrDefault();
                if (hisItem == null)
                {
                    item.IsQuantityChange = true;
                    item.IsPriceChange = true;
                    item.IsTotalPriceChange = true;
                }
                else
                {
                    item.IsQuantityChange = !EqualDouble(item.Quantity, hisItem.Quantity);
                    item.IsPriceChange = !EqualDouble(item.Price, hisItem.Price);
                    item.IsTotalPriceChange = !EqualDouble(item.TotalPrice, hisItem.TotalPrice);
                }
            }
        }

        private bool EqualString(string oldString, string newString)
        {
            if (string.IsNullOrWhiteSpace(oldString) && string.IsNullOrWhiteSpace(newString))
            {
                return true;
            }
            if (string.IsNullOrWhiteSpace(oldString) || string.IsNullOrWhiteSpace(newString))
            {
                return false;
            }
            return oldString.Trim().Equals(newString.Trim());
        }

        private bool EqualInt(int? oldInt, int? newInt)
        {
            return oldInt.GetValueOrDefault().Equals(newInt.GetValueOrDefault());
        }

        private bool EqualDouble(double? oldFloat, double? newFloat)
        {
            return oldFloat.GetValueOrDefault().Equals(newFloat.GetValueOrDefault());
        }
    }
}