﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace IInterface.Business
{
    public class PlanHistoryBusiness
    {
        private Entities context;
        public PlanHistoryBusiness(Entities context)
        {
            this.context = context;
        }
        public void InsertPlanItemHistory(long planHisID, long planID)
        {
            using (Entities taskContext = new Entities())
            {
                // Thong tin muc tieu
                List<PlanTarget> listPlanTarget = taskContext.PlanTarget.Where(x => x.PlanID == planID).ToList();
                foreach (PlanTarget planTarget in listPlanTarget)
                {
                    PlanTargetHis planTargetHis = new PlanTargetHis();
                    Utils.Copy(planTarget, planTargetHis);
                    planTargetHis.PlanHisID = planHisID;
                    taskContext.PlanTargetHis.Add(planTargetHis);
                }
                // Chi so muc tieu
                List<PlanTargetIndex> listPlanTargetIndex = taskContext.PlanTargetIndex.Where(x => x.PlanID == planID).ToList();
                foreach (PlanTargetIndex planTargetIndex in listPlanTargetIndex)
                {
                    PlanTargetIndexHis planTargetIndexHis = new PlanTargetIndexHis();
                    Utils.Copy(planTargetIndex, planTargetIndexHis);
                    planTargetIndexHis.PlanHisID = planHisID;
                    taskContext.PlanTargetIndexHis.Add(planTargetIndexHis);
                }
                // Hoat dong con
                List<int> listPlanTargetID = listPlanTarget.Select(x => x.PlanTargetID).ToList();
                List<PlanTargetAction> listPlanTargetAction = taskContext.PlanTargetAction.Where(x => x.PlanTargetID != null && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
                foreach (PlanTargetAction planTargetAction in listPlanTargetAction)
                {
                    PlanTargetActionHis planTargetActionHis = new PlanTargetActionHis();
                    Utils.Copy(planTargetAction, planTargetActionHis);
                    planTargetActionHis.PlanHisID = planHisID;
                    taskContext.PlanTargetActionHis.Add(planTargetActionHis);
                }
                // Hoat dong chau
                List<int> listPlanTargetActionID = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();
                List<SubActionPlan> listSubAction = taskContext.SubActionPlan.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
                foreach (SubActionPlan subAction in listSubAction)
                {
                    SubActionPlanHis subActionHis = new SubActionPlanHis();
                    Utils.Copy(subAction, subActionHis);
                    subActionHis.PlanHisID = planHisID;
                    taskContext.SubActionPlanHis.Add(subActionHis);
                }
                // Chi so hoat dong
                List<PlanActionIndex> listPlanActionIndex = taskContext.PlanActionIndex.Where(x => listPlanTargetID.Contains(x.PlanTargetID)).ToList();
                foreach (PlanActionIndex planActionIndex in listPlanActionIndex)
                {
                    PlanActionIndexHis planActionIndexHis = new PlanActionIndexHis();
                    Utils.Copy(planActionIndex, planActionIndexHis);
                    planActionIndexHis.PlanHisID = planHisID;
                    taskContext.PlanActionIndexHis.Add(planActionIndexHis);
                }
                // Thoi gian
                List<SubActionPlanTime> listTime = taskContext.SubActionPlanTime.Where(x => x.PlanTargetActionID != null
                && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
                foreach (SubActionPlanTime time in listTime)
                {
                    SubActionPlanTimeHis timeHis = new SubActionPlanTimeHis();
                    Utils.Copy(time, timeHis);
                    timeHis.PlanHisID = planHisID;
                    taskContext.SubActionPlanTimeHis.Add(timeHis);
                }
                // Kinh phi
                List<SubActionSpendItem> listSpend = taskContext.SubActionSpendItem.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
                foreach (SubActionSpendItem spend in listSpend)
                {
                    SubActionSpendItemHis spendHis = new SubActionSpendItemHis();
                    Utils.Copy(spend, spendHis);
                    spendHis.PlanHisID = planHisID;
                    taskContext.SubActionSpendItemHis.Add(spendHis);
                }
                List<SubActionPlanCost> listCost = taskContext.SubActionPlanCost.Where(x => x.PlanTargetID != null
                && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
                foreach (SubActionPlanCost cost in listCost)
                {
                    SubActionPlanCostHis costHist = new SubActionPlanCostHis();
                    Utils.Copy(cost, costHist);
                    costHist.PlanHisID = planHisID;
                    taskContext.SubActionPlanCostHis.Add(costHist);
                }
                taskContext.SaveChanges();
            }
        }
        public long InsertPlanHistory(int planID, int planStatus)
        {
            Plan plan = context.Plan.Find(planID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            PlanHis planHis = new PlanHis();
            // Thong tin chung
            Utils.Copy(plan, planHis);
            planHis.Status = planStatus;
            context.PlanHis.Add(planHis);
            context.SaveChanges();
            Task.Factory.StartNew(() => InsertPlanItemHistory(planHis.ID, plan.PlanID));
            return planHis.ID;
        }

        public PlanModel GetPlan(long planHisID)
        {
            PlanHis planHis = context.PlanHis.Find(planHisID);
            if (planHis == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Plan objPlan = context.Plan.Find(planHis.PlanID);
            if (objPlan == null)
            {
                return null;
            }
            Period objPeriod = context.Period.Find(objPlan.PeriodID);
            Department objDepartment = context.Department.Find(objPlan.DepartmentID);
            PlanModel model = new PlanModel();
            model.PeriodID = objPlan.PeriodID;
            model.Type = objDepartment.DepartmentType;
            //Department
            model.DepartmentID = objPlan.DepartmentID;
            model.DepartmentName = objDepartment.DepartmentName;
            model.Address = objDepartment.Address;
            model.PhoneNumber = objDepartment.PhoneNumber;
            model.Fax = objDepartment.Fax;
            model.AccountNumber = objDepartment.AccountNumber;
            model.BankName = objDepartment.BankName;
            model.FromDate = objPeriod.FromDate;
            model.ToDate = objPeriod.ToDate;
            model.PlanID = objPlan.PlanID;
            model.PlanContent = objPlan.PlanContent;
            model.PlanTarget = objPlan.PlanTarget;
            model.RepresentPersonName = objPlan.RepresentPersonName;
            model.RepresentPosition = objPlan.RepresentPosition;
            model.RepresentAddress = objPlan.RepresentAddress;
            model.RepresentPhone = objPlan.RepresentPhone;
            model.RepresentEmail = objPlan.RepresentEmail;
            model.ContactPersonName = objPlan.ContactPersonName;
            model.ContactPosition = objPlan.ContactPosition;
            model.ContactAddress = objPlan.ContactAddress;
            model.ContactPhone = objPlan.ContactPhone;
            model.ContactEmail = objPlan.ContactEmail;
            model.Status = objPlan.Status;
            return model;
        }

        public List<TargetIndexModel> GetListTargetIndex(int? PlanTargetID, long planHisID)
        {
            PlanHis planHis = context.PlanHis.Find(planHisID);
            if (planHis == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            PlanTargetHis objPlanTarget = context.PlanTargetHis.Where(a => a.PlanHisID == planHisID && a.PlanID == planHis.PlanID && a.PlanTargetID == PlanTargetID).FirstOrDefault();
            List<TargetIndexModel> listTargetIndex = (from x in context.PlanTargetIndexHis
                                                      join y in context.MainIndex on x.MainIndexID equals y.MainIndexID into g1
                                                      from y in g1.DefaultIfEmpty()
                                                      where x.PlanTargetID == objPlanTarget.PlanTargetID && x.PlanID == planHis.PlanID && x.PlanHisID == planHis.ID
                                                      select new TargetIndexModel
                                                      {
                                                          MainIndexID = x.MainIndexID,
                                                          ContentIndex = x.ContentIndex,
                                                          TypeIndex = x.TypeIndex,
                                                          ValueIndex = x.ValueIndex,
                                                          ValueTotal = x.ValueTotal,
                                                          ValueIndexChange = x.ValueIndexChange,
                                                          ValueTotalChange = x.ValueTotalChange,
                                                          UpdateUserID = x.UpdateUser
                                                      }).ToList();
            return listTargetIndex;
        }

        public List<TargetActionModel> GetListActionOfTarget(int PlanTargetID, long planHisID)
        {
            PlanHis planHis = context.PlanHis.Find(planHisID);
            if (planHis == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<TargetActionModel> TargetActions = new List<TargetActionModel>();
            //Danh sach hoat dong
            TargetActions = (from x in context.PlanTargetActionHis
                             where x.PlanTargetID == PlanTargetID && x.PlanID == planHis.PlanID && x.PlanHisID == planHis.ID
                             select new TargetActionModel
                             {
                                 ActionCode = x.ActionCode,
                                 ActionContent = x.ActionContent,
                                 MainActionID = x.MainActionID,
                                 ContentPropaganda = x.ContentPropaganda,
                                 ObjectScope = x.ObjectScope,
                                 ActionMethod = x.ActionMethod,
                                 PlanTargetActionID = x.PlanTargetActionID,
                                 UpdateUserID = x.UpdateUserID
                             }).OrderBy(x => x.PlanTargetActionID)
                            .ToList();
            List<int> listPlanTargetActionID = TargetActions.Select(x => x.PlanTargetActionID).ToList();
            Dictionary<int, string> dicTime = this.ActionTimesHis(listPlanTargetActionID, planHisID);
            // Danh sach thoi gian da khai bao
            TargetActions = TargetActions
                .Select(x => new TargetActionModel
                {
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    MainActionID = x.MainActionID,
                    ContentPropaganda = x.ContentPropaganda,
                    ObjectScope = x.ObjectScope,
                    ActionMethod = x.ActionMethod,
                    PlanTargetActionID = x.PlanTargetActionID,
                    UpdateUserID = x.UpdateUserID,
                    ActionTimes = dicTime[x.PlanTargetActionID]
                }).ToList();
            return TargetActions;
        }

        public List<SubActionPlanModel> GetSubAction(long planHisID, int PlanTargetActionID = 0)
        {
            var IQSubAction = (from x in context.SubActionPlanHis
                               where x.PlanHisID == planHisID
                               select new SubActionPlanModel
                               {
                                   SubActionContent = x.SubActionContent,
                                   SubActionPlanID = x.SubActionPlanID,
                                   PlanID = x.PlanID,
                                   SubActionOrder = x.SubActionOrder,
                                   PlanTargetActionID = x.PlanTargetActionID
                               });
            if (PlanTargetActionID != 0)
            {
                IQSubAction = IQSubAction.Where(a => a.PlanTargetActionID == PlanTargetActionID);
            }
            return IQSubAction.OrderBy(x => x.SubActionOrder).ToList();
        }

        public List<PlanActionIndexModel> GetSubIndex(long planHisID, int PlanTargetActionID = 0)
        {
            var IQSubAction = (from x in context.PlanActionIndexHis
                               join y in context.ActionIndex on x.ActionIndexID equals y.ActionIndexID
                               where x.PlanHisID == planHisID
                               select new PlanActionIndexModel
                               {
                                   PlanActionIndexID = x.PlanActionIndexID,
                                   ActionIndexID = x.ActionIndexID,
                                   PlanTargetActionID = x.PlanTargetActionID,
                                   IndexValue = x.IndexValue,
                                   IndexName = y.IndexName,
                                   IndexUnitName = y.UnitName,
                                   PlanTargetID = x.PlanTargetID
                               });
            if (PlanTargetActionID != 0)
            {
                IQSubAction = IQSubAction.Where(a => a.PlanTargetActionID == PlanTargetActionID);
            }
            return IQSubAction.OrderByDescending(x => x.PlanActionIndexID).ToList();
        }

        public Dictionary<int, string> ActionTimesHis(List<int> listPlanTargetActionID, long planHisID, bool isQuy = false)
        {
            PlanHis planHis = context.PlanHis.Find(planHisID);
            if (planHis == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var period = (from pr in context.Period
                          where pr.PeriodID == planHis.PeriodID
                          select new
                          {
                              pr.PeriodID,
                              pr.FromDate,
                              pr.ToDate
                          }).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<SubActionPlanTimeModel> listTime = context.SubActionPlanTimeHis.Where(x => x.PlanHisID == planHisID && x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetActionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
            Dictionary<int, string> dicTime = new Dictionary<int, string>();
            foreach (int planTargetActionID in listPlanTargetActionID)
            {
                List<SubActionPlanTimeModel> listPlanTargetActionTime = listTime.Where(x => x.PlanTargetAcionID == planTargetActionID && x.SubActionPlanID == null).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetAcionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
                if (!listPlanTargetActionTime.Any(x => !string.IsNullOrWhiteSpace(x.PlanTime)))
                {
                    List<SubActionPlanTimeModel> listSubActionTime = listTime.Where(x => x.SubActionPlanID != null).ToList();
                    List<int> listSubActionID = listSubActionTime.Select(x => x.SubActionPlanID.Value).Distinct().ToList();
                    for (int year = fromYear; year <= toYear; year++)
                    {
                        foreach (int subActionID in listSubActionID)
                        {
                            List<SubActionPlanTimeModel> listTimeYear = listSubActionTime.Where(x => x.PlanYear == year && x.SubActionPlanID == subActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                            for (int i = 0; i < listTimeYear.Count; i++)
                            {
                                SubActionPlanTimeModel subTime = listTimeYear[i];
                                List<SubActionPlanTimeModel> planTargetActionTimes = listPlanTargetActionTime.Where(x => x.PlanYear == year).ToList();
                                if (planTargetActionTimes.Count > i)
                                {
                                    SubActionPlanTimeModel planTargetActionTime = planTargetActionTimes[i];
                                    if (!string.IsNullOrWhiteSpace(subTime.PlanTime))
                                    {
                                        planTargetActionTime.PlanTime = "X";
                                    }
                                }
                            }
                        }
                    }
                }

                string time = this.GetTime(fromYear, toYear, listPlanTargetActionTime, isQuy);
                dicTime[planTargetActionID] = string.IsNullOrWhiteSpace(time) ? "Chưa có thông tin thời gian, vui lòng vào chức năng để cập nhật" : time;

            }
            return dicTime;
        }

        public Dictionary<int, string> SubActionTimesHis(int planTargetID, int planHisID, bool isQuy = false)
        {
            PlanHis planHis = context.PlanHis.Find(planHisID);
            if (planHis == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            var period = (from pr in context.Period
                          where pr.PeriodID == planHis.PeriodID
                          select new
                          {
                              pr.PeriodID,
                              pr.FromDate,
                              pr.ToDate
                          }).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<SubActionPlanTimeModel> listTime = context.SubActionPlanTimeHis.Where(x => x.PlanHisID == planHisID && x.SubActionPlanID != null
            && context.PlanTargetActionHis.Any(t => t.PlanHisID == planHisID && t.PlanTargetID == planTargetID && t.PlanTargetActionID == x.PlanTargetActionID)).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetActionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
            List<SubActionPlanHis> listSubAction = context.SubActionPlanHis.Where(x => x.PlanHisID == planHisID
            && context.PlanTargetActionHis.Any(t => t.PlanHisID == planHisID && t.PlanTargetID == planTargetID && t.PlanTargetActionID == x.PlanTargetActionID)).ToList();
            Dictionary<int, string> dicTime = new Dictionary<int, string>();
            foreach (SubActionPlanHis sub in listSubAction)
            {
                List<SubActionPlanTimeModel> listSubActionTime = listTime.Where(x => x.PlanTargetAcionID == sub.PlanTargetActionID && x.SubActionPlanID == sub.SubActionPlanID).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetAcionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
                if (!listSubActionTime.Any(x => !string.IsNullOrWhiteSpace(x.PlanTime)))
                {
                    List<SubActionPlanHis> listChildAction = listSubAction.Where(x => x.ParentID == sub.SubActionPlanID).ToList();
                    if (listChildAction.Any())
                    {
                        List<SubActionPlanTimeModel> listChildActionTime = listTime.Where(x => x.SubActionPlanID != null).ToList();
                        List<int> listSubActionID = listChildActionTime.Select(x => x.SubActionPlanID.Value).Distinct().ToList();
                        for (int year = fromYear; year <= toYear; year++)
                        {
                            foreach (int subActionID in listSubActionID)
                            {
                                List<SubActionPlanTimeModel> listTimeYear = listChildActionTime.Where(x => x.PlanYear == year && x.SubActionPlanID == subActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                                for (int i = 0; i < listTimeYear.Count; i++)
                                {
                                    SubActionPlanTimeModel subTime = listTimeYear[i];
                                    List<SubActionPlanTimeModel> subActionTimes = listSubActionTime.Where(x => x.PlanYear == year).ToList();
                                    if (subActionTimes.Count > i)
                                    {
                                        SubActionPlanTimeModel subActionTime = subActionTimes[i];
                                        if (!string.IsNullOrWhiteSpace(subTime.PlanTime))
                                        {
                                            subActionTime.PlanTime = "X";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                string time = this.GetTime(fromYear, toYear, listSubActionTime, isQuy);
                dicTime[sub.SubActionPlanID] = time;
            }
            return dicTime;
        }

        public Dictionary<int, string> SubActionTimes(int planTargetID, int planID, bool isQuy = false)
        {
            var period = (from pl in context.Plan
                          join pr in context.Period on pl.PeriodID equals pr.PeriodID
                          where pl.PlanID == planID
                          select new
                          {
                              pr.PeriodID,
                              pr.FromDate,
                              pr.ToDate
                          }).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<SubActionPlanTimeModel> listTime = context.SubActionPlanTime.Where(x => x.SubActionPlanID != null
            && context.PlanTargetAction.Any(t => t.PlanTargetID == planTargetID && t.PlanTargetActionID == x.PlanTargetActionID)).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetActionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
            List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => context.PlanTargetAction.Any(t => t.PlanTargetID == planTargetID && t.PlanTargetActionID == x.PlanTargetActionID)).ToList();
            Dictionary<int, string> dicTime = new Dictionary<int, string>();
            foreach (SubActionPlan sub in listSubAction)
            {
                List<SubActionPlanTimeModel> listSubActionTime = listTime.Where(x => x.PlanTargetAcionID == sub.PlanTargetActionID && x.SubActionPlanID == sub.SubActionPlanID).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetAcionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
                if (!listSubActionTime.Any(x => !string.IsNullOrWhiteSpace(x.PlanTime)))
                {
                    List<SubActionPlan> listChildAction = listSubAction.Where(x => x.ParentID == sub.SubActionPlanID).ToList();
                    if (listChildAction.Any())
                    {
                        List<SubActionPlanTimeModel> listChildActionTime = listTime.Where(x => x.SubActionPlanID != null).ToList();
                        List<int> listSubActionID = listChildActionTime.Select(x => x.SubActionPlanID.Value).Distinct().ToList();
                        for (int year = fromYear; year <= toYear; year++)
                        {
                            foreach (int subActionID in listSubActionID)
                            {
                                List<SubActionPlanTimeModel> listTimeYear = listChildActionTime.Where(x => x.PlanYear == year && x.SubActionPlanID == subActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                                for (int i = 0; i < listTimeYear.Count; i++)
                                {
                                    SubActionPlanTimeModel subTime = listTimeYear[i];
                                    List<SubActionPlanTimeModel> subActionTimes = listSubActionTime.Where(x => x.PlanYear == year).ToList();
                                    if (subActionTimes.Count > i)
                                    {
                                        SubActionPlanTimeModel subActionTime = subActionTimes[i];
                                        if (!string.IsNullOrWhiteSpace(subTime.PlanTime))
                                        {
                                            subActionTime.PlanTime = "X";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                string time = this.GetTime(fromYear, toYear, listSubActionTime, isQuy);
                dicTime[sub.SubActionPlanID] = time;
            }
            return dicTime;
        }

        public Dictionary<int, string> ActionTimes(List<int> listPlanTargetActionID, int planID, bool isQuy = false)
        {
            var period = (from pl in context.Plan
                          join pr in context.Period on pl.PeriodID equals pr.PeriodID
                          where pl.PlanID == planID
                          select new
                          {
                              pr.PeriodID,
                              pr.FromDate,
                              pr.ToDate
                          }).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<SubActionPlanTimeModel> listTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetActionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
            Dictionary<int, string> dicTime = new Dictionary<int, string>();
            foreach (int planTargetActionID in listPlanTargetActionID)
            {
                List<SubActionPlanTimeModel> listPlanTargetActionTime = listTime.Where(x => x.PlanTargetAcionID == planTargetActionID && x.SubActionPlanID == null).OrderBy(x => x.PlanYear)
                .ThenBy(x => x.SubActionPlanTimeID).Select(x => new SubActionPlanTimeModel
                {
                    PlanYear = x.PlanYear,
                    SubActionPlanTimeID = x.SubActionPlanTimeID,
                    PlanTime = x.PlanTime,
                    PlanTargetAcionID = x.PlanTargetAcionID,
                    SubActionPlanID = x.SubActionPlanID
                }).ToList();
                if (!listPlanTargetActionTime.Any(x => !string.IsNullOrWhiteSpace(x.PlanTime)))
                {
                    List<SubActionPlanTimeModel> listSubActionTime = listTime.Where(x => x.SubActionPlanID != null).ToList();
                    List<int> listSubActionID = listSubActionTime.Select(x => x.SubActionPlanID.Value).Distinct().ToList();
                    for (int year = fromYear; year <= toYear; year++)
                    {
                        foreach (int subActionID in listSubActionID)
                        {
                            List<SubActionPlanTimeModel> listTimeYear = listSubActionTime.Where(x => x.PlanYear == year && x.SubActionPlanID == subActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                            for (int i = 0; i < listTimeYear.Count; i++)
                            {
                                SubActionPlanTimeModel subTime = listTimeYear[i];
                                List<SubActionPlanTimeModel> planTargetActionTimes = listPlanTargetActionTime.Where(x => x.PlanYear == year).ToList();
                                if (planTargetActionTimes.Count > i)
                                {
                                    SubActionPlanTimeModel planTargetActionTime = planTargetActionTimes[i];
                                    if (!string.IsNullOrWhiteSpace(subTime.PlanTime))
                                    {
                                        planTargetActionTime.PlanTime = "X";
                                    }
                                }
                            }
                        }
                    }
                }

                string time = this.GetTime(fromYear, toYear, listPlanTargetActionTime, isQuy);
                dicTime[planTargetActionID] = string.IsNullOrWhiteSpace(time) ? "Chưa có thông tin thời gian, vui lòng vào chức năng để cập nhật" : time;
            }

            return dicTime;
        }

        private string GetTime(int fromYear, int toYear, List<SubActionPlanTimeModel> listTime, bool isQuy = false)
        {
            if (listTime == null || !listTime.Any())
            {
                return string.Empty;
            }
            Dictionary<int, List<int>> dicYearTime = new Dictionary<int, List<int>>();
            for (int year = fromYear; year <= toYear; year++)
            {
                dicYearTime[year] = new List<int>();
                List<SubActionPlanTimeModel> listTimeYear = listTime.Where(x => x.PlanYear == year).OrderBy(x => x.SubActionPlanTimeID).ToList();
                for (int i = 0; i < listTimeYear.Count; i++)
                {
                    SubActionPlanTimeModel subTime = listTimeYear[i];
                    if (!string.IsNullOrWhiteSpace(subTime.PlanTime))
                    {
                        int nowQuarter = i + 1;
                        dicYearTime[year].Add(nowQuarter);
                    }
                }
            }
            string time = string.Empty;
            if (dicYearTime.Keys.Any())
            {
                #region Xu ly thoi gian rieng doi voi truong hop hoat hoat cap quy
                if (isQuy)
                {
                    List<int> listYear = new List<int>();
                    foreach (int year in dicYearTime.Keys)
                    {
                        if (dicYearTime[year] != null && dicYearTime[year].Count == 4)
                        {
                            listYear.Add(year);
                        }
                    }
                    listYear = listYear.OrderBy(x => x).ToList();
                    bool isFull = true;
                    if (listYear.Any())
                    {
                        int startYear = listYear[0];
                        for (int i = 0; i < listYear.Count; i++)
                        {
                            if (startYear + i != listYear[i])
                            {
                                isFull = false;
                                break;
                            }
                        }
                    }
                    if (isFull)
                    {
                        time = "Năm " + listYear[0];
                        if (listYear.Count > 1)
                        {
                            time += "-" + listYear[listYear.Count - 1];
                        }
                    }
                }
                #endregion
                if (string.IsNullOrWhiteSpace(time))
                {
                    foreach (int year in dicYearTime.Keys)
                    {
                        if (dicYearTime[year] != null && dicYearTime[year].Any())
                        {
                            time += "; Năm " + year + ": ";
                            foreach (int quarter in dicYearTime[year])
                            {
                                time += "quý " + quarter + ", ";
                            }
                            time = time.Remove(time.Length - 2);
                        }
                    }
                }
            }
            return time.StartsWith(";") ? time.Substring(2) : time;
        }

    }
}