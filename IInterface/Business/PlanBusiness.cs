﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Business
{
    public class PlanBusiness
    {
        private Entities context;
        public PlanBusiness(Entities context)
        {
            this.context = context;
        }
        public void DelPlanTarget(List<PlanTarget> lstPlanDel, int planID = 0)
        {
            List<int> listPlanTargetIDDel = lstPlanDel.Select(x => x.PlanTargetID).ToList();
            List<long> listPlanTargetIDDelLong = listPlanTargetIDDel.Select(x => (long)x).ToList();
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
            List<int> listPlanTargetActionIDDel = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();

            // Comment
            List<Comment> listComment = context.Comment.Where(x => x.Type == Constants.COMMENT_TYPE_INDEX && listPlanTargetIDDelLong.Contains(x.ObjectID)).ToList();
            context.Comment.RemoveRange(listComment);
            // Chi phi
            List<SubActionPlanCost> listSubCost = context.SubActionPlanCost.Where(x => x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
            context.SubActionPlanCost.RemoveRange(listSubCost);
            // Chi phi theo muc chi
            List<SubActionSpendItem> listSubItemCost = context.SubActionSpendItem.Where(x => x.PlanTargetActionID != null && listPlanTargetActionIDDel.Contains(x.PlanTargetActionID.Value)).ToList();
            context.SubActionSpendItem.RemoveRange(listSubItemCost);
            var plan = context.Plan.Find(planID);
            if (plan.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                var listCostEstDetail = context.CostEstimateDetail.Where(x => x.PlanID == planID && x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
                context.CostEstimateDetail.RemoveRange(listCostEstDetail);
                var listCostEstSpend = context.CostEstimateSpend.Where(x => x.PlanID == planID && x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
                context.CostEstimateSpend.RemoveRange(listCostEstSpend);
            }
            // Thoi gian
            List<SubActionPlanTime> listTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID != null && listPlanTargetActionIDDel.Contains(x.PlanTargetActionID.Value)).ToList();
            context.SubActionPlanTime.RemoveRange(listTime);
            List<CostEstimateSpend> listCostSpend = context.CostEstimateSpend.Where(x => x.PlanID == planID && x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
            context.CostEstimateSpend.RemoveRange(listCostSpend);
            List<CostEstimateDetail> listCostDetail = context.CostEstimateDetail.Where(x => x.PlanID == planID && x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
            context.CostEstimateDetail.RemoveRange(listCostDetail);
            // Chi tieu hoat dong
            List<PlanActionIndex> listActionIndex = context.PlanActionIndex.Where(x => listPlanTargetIDDel.Contains(x.PlanTargetID)).ToList();
            context.PlanActionIndex.RemoveRange(listActionIndex);
            // Chi tieu muc tieu
            List<PlanTargetIndex> listTargetIndex = context.PlanTargetIndex.Where(x => x.PlanTargetID != null && listPlanTargetIDDel.Contains(x.PlanTargetID.Value)).ToList();
            context.PlanTargetIndex.RemoveRange(listTargetIndex);
            // Hoat dong chau
            List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanTargetActionID != null && listPlanTargetActionIDDel.Contains(x.PlanTargetActionID.Value)).ToList();
            context.SubActionPlan.RemoveRange(listSubAction);
            // Hoat dong con
            context.PlanTargetAction.RemoveRange(listPlanTargetAction);
            // Muc tieu
            context.PlanTarget.RemoveRange(lstPlanDel);
        }

        public void SynTarget(int planID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            List<Target> listTarget = context.Target.Where(x => x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE).OrderBy(x => x.TargetOrder).ToList();
            for (int i = 0; i < listTarget.Count; i++)
            {
                int order = i + 1;
                Target t = listTarget[i];
                PlanTarget pt = new PlanTarget();
                pt.CreateDate = DateTime.Now;
                pt.CreateUserID = user.UserID;
                pt.DepartmentID = user.DepartmentID;
                pt.PlanID = planID;
                pt.PlanTargetContent = t.TargetContent;
                pt.PlanTargetTitle = "Mục tiêu " + order;
                pt.Status = Constants.IS_ACTIVE;
                pt.TargetCode = "2." + order;
                pt.TargetID = t.TargetID;
                context.PlanTarget.Add(pt);
            }
            context.SaveChanges();
        }

        public void SynIndex(int planTargetID)
        {
            var planTarget = (from t in context.PlanTarget
                              join pl in context.Plan on t.PlanID equals pl.PlanID
                              where t.PlanTargetID == planTargetID
                              select new
                              {
                                  t.PlanTargetID,
                                  t.TargetID,
                                  t.PlanID,
                                  pl.PeriodID
                              }).FirstOrDefault();
            if (planTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (planTarget.TargetID == null)
            {
                throw new BusinessException("Mục tiêu này không gắn với mục tiêu được khai báo trong danh mục của quỹ nên không tổng hợp được");
            }

            List<TargetIndexModel> listMainIndex = this.GetIndexPlaned(planTarget.PeriodID, planTarget.TargetID.Value);

            List<PlanTargetIndex> listPlanTargetIndex = context.PlanTargetIndex.Where(x => x.PlanTargetID == planTargetID).ToList();
            foreach (PlanTargetIndex index in listPlanTargetIndex)
            {
                TargetIndexModel mainIndex = listMainIndex.Where(x => x.MainIndexID == index.MainIndexID).FirstOrDefault();
                if (mainIndex != null)
                {
                    index.ValueIndex = mainIndex.ValueIndex;
                }
            }
            listMainIndex.RemoveAll(x => listPlanTargetIndex.Any(i => i.MainIndexID == x.MainIndexID));
            UserInfo user = SessionManager.GetUserInfo();
            foreach (TargetIndexModel index in listMainIndex)
            {
                PlanTargetIndex pti = new PlanTargetIndex();
                pti.MainIndexID = index.MainIndexID;
                pti.PlanID = planTarget.PlanID;
                pti.PlanTargetID = planTarget.PlanTargetID;
                pti.TargetID = index.TargetID;
                pti.TypeIndex = index.TypeIndex;
                pti.ContentIndex = index.ContentIndex;
                pti.ValueIndex = index.ValueIndex;
                pti.UpdateUser = user.UserID;
                context.PlanTargetIndex.Add(pti);
            }
        }

        public void SynActionAndIndex(Plan plan)
        {
            UserInfo user = SessionManager.GetUserInfo();

            List<PlanTarget> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == plan.PlanID).ToList();
            List<MainAction> listAction = context.MainAction.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            List<MainAction> listRootAction = listAction.Where(x => x.ParentID == null).ToList();
            List<PlanTargetAction> listPlanTargetAction = (from ta in context.PlanTargetAction
                                                           join tg in context.PlanTarget on ta.PlanTargetID equals tg.PlanTargetID
                                                           join pl in context.Plan on tg.PlanID equals pl.PlanID
                                                           join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                                           where pl.PeriodID == plan.PeriodID && pl.Status == Constants.PLAN_APPROVED
                                                           && d.Status == Constants.IS_ACTIVE && d.DepartmentType == Constants.DEPT_TYPE_PBQUY
                                                           && d.DepartmentID != Constants.HEAD_DEPT_ID && ta.MainActionID != null
                                                           select ta).ToList();
            List<SubActionPlan> listSubActionPlan = (from s in context.SubActionPlan
                                                     join ta in context.PlanTargetAction on s.PlanTargetActionID equals ta.PlanTargetActionID
                                                     join tg in context.PlanTarget on ta.PlanTargetID equals tg.PlanTargetID
                                                     join pl in context.Plan on tg.PlanID equals pl.PlanID
                                                     join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                                     where pl.PeriodID == plan.PeriodID && pl.Status == Constants.PLAN_APPROVED
                                                     && d.Status == Constants.IS_ACTIVE && d.DepartmentType == Constants.DEPT_TYPE_PBQUY
                                                     && d.DepartmentID != Constants.HEAD_DEPT_ID && ta.MainActionID != null && s.SubActionID != null
                                                     select s).ToList();
            Dictionary<int, PlanTargetAction> dicAction = new Dictionary<int, PlanTargetAction>();
            foreach (PlanTarget pt in listPlanTarget)
            {
                this.SynIndex(pt.PlanTargetID);
                List<MainAction> listActionByTarget = listRootAction.Where(x => x.TargetID == pt.TargetID).OrderBy(x => x.MainActionID).ToList();
                for (int i = 0; i < listActionByTarget.Count; i++)
                {
                    MainAction action = listActionByTarget[i];
                    PlanTargetAction deptAction = listPlanTargetAction.Where(x => x.MainActionID == action.MainActionID).FirstOrDefault();
                    PlanTargetAction pta = new PlanTargetAction();
                    pta.ActionOrder = i + 1;
                    pta.ActionCode = pt.TargetCode + "." + pta.ActionOrder;
                    pta.ActionContent = action.ActionContent;
                    if (deptAction != null)
                    {
                        pta.ContentPropaganda = deptAction.ContentPropaganda;
                        pta.ActionMethod = deptAction.ActionMethod;
                        pta.ObjectScope = deptAction.ObjectScope;
                        pta.Result = deptAction.Result;
                    }
                    pta.CreateUserID = user.UserID;
                    pta.MainActionID = action.MainActionID;
                    pta.PlanID = plan.PlanID;
                    pta.PlanTargetID = pt.PlanTargetID;
                    context.PlanTargetAction.Add(pta);
                    dicAction[action.MainActionID] = pta;
                }
            }
            context.SaveChanges();
            foreach (MainAction action in listRootAction)
            {
                PlanTargetAction pta = dicAction.ContainsKey(action.MainActionID) ? dicAction[action.MainActionID] : null;
                if (pta == null)
                {
                    continue;
                }
                List<MainAction> listChildAction = listAction.Where(x => x.ParentID == action.MainActionID).ToList();
                if (listChildAction != null && listChildAction.Any())
                {
                    for (int i = 0; i < listChildAction.Count; i++)
                    {
                        MainAction subAction = listChildAction[i];
                        SubActionPlan deptAction = listSubActionPlan.Where(x => x.SubActionID == subAction.MainActionID).FirstOrDefault();
                        SubActionPlan subActionPlan = new SubActionPlan();
                        subActionPlan.PlanTargetActionID = pta.PlanTargetActionID;
                        subActionPlan.SubActionOrder = i + 1;
                        subActionPlan.SubActionCode = pta.ActionCode + "." + subActionPlan.SubActionOrder;
                        subActionPlan.SubActionContent = action.ActionContent;
                        if (deptAction != null)
                        {
                            subActionPlan.ContentPropaganda = deptAction.ContentPropaganda;
                            subActionPlan.SubActionMethod = deptAction.SubActionMethod;
                            subActionPlan.SubActionObject = deptAction.SubActionObject;
                            subActionPlan.Result = deptAction.Result;
                        }
                        subActionPlan.CreateUserID = user.UserID;
                        subActionPlan.SubActionID = action.MainActionID;
                        subActionPlan.PlanID = plan.PlanID;
                        context.SubActionPlan.Add(subActionPlan);
                    }
                }
            }
            context.SaveChanges();
        }

        public List<TargetIndexModel> GetIndexPlaned(int PeriodID, int TargetID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            List<TargetIndexModel> listSynTargetIndex = (from mi in context.MainIndex
                                                         join pi in context.PlanTargetIndex on mi.MainIndexID equals pi.MainIndexID
                                                         join pl in context.Plan on pi.PlanID equals pl.PlanID
                                                         join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                                         where d.Status == Constants.IS_ACTIVE && mi.Status == Constants.IS_ACTIVE
                                                         && d.DepartmentType == Constants.DEPT_TYPE_PBQUY && d.DepartmentID != user.DepartmentID && pl.Status == Constants.PLAN_APPROVED
                                                         && pi.TargetID != null && pi.TargetID == TargetID && mi.Type == Constants.MAIN_TYPE
                                                         && (mi.TypeIndex == Constants.TYPE_INDEX_INPUT || mi.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE)
                                                         group pi by new
                                                         {
                                                             mi.MainIndexID,
                                                             mi.ContentIndex,
                                                             mi.TypeIndex,
                                                             TargetID = pi.TargetID.Value
                                                         } into g
                                                         select new TargetIndexModel
                                                         {
                                                             MainIndexID = g.Key.MainIndexID,
                                                             ContentIndex = g.Key.ContentIndex,
                                                             TypeIndex = g.Key.TypeIndex,
                                                             TargetID = g.Key.TargetID,
                                                             ValueIndex = g.Key.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE ? g.Average(x => x.ValueIndex) : g.Sum(x => x.ValueIndex)
                                                         }).ToList();

            List<TargetIndexModel> listTargetIndex = (from mi in context.MainIndex
                                                      join mmi in context.MainIndex on mi.MappingIndexID equals mmi.MainIndexID
                                                      join pi in context.PlanTargetIndex on mi.MainIndexID equals pi.MainIndexID
                                                      join pl in context.Plan on pi.PlanID equals pl.PlanID
                                                      join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                                      where d.Status == Constants.IS_ACTIVE && mi.Status == Constants.IS_ACTIVE
                                                      && d.DepartmentType != Constants.DEPT_TYPE_PBQUY && pl.Status == Constants.PLAN_APPROVED
                                                      && pi.TargetID != null && mi.Type == Constants.SUB_TYPE
                                                      && (mi.TypeIndex == Constants.TYPE_INDEX_INPUT || mi.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE)
                                                      group pi by new
                                                      {
                                                          mmi.MainIndexID,
                                                          mmi.ContentIndex,
                                                          mmi.TypeIndex,
                                                          TargetID = mmi.TargetID.Value
                                                      } into g
                                                      select new TargetIndexModel
                                                      {
                                                          MainIndexID = g.Key.MainIndexID,
                                                          ContentIndex = g.Key.ContentIndex,
                                                          TypeIndex = g.Key.TypeIndex,
                                                          TargetID = g.Key.TargetID,
                                                          ValueIndex = g.Key.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE ? g.Average(x => x.ValueIndex) : g.Sum(x => x.ValueIndex)
                                                      }).ToList();
            listSynTargetIndex.AddRange(listTargetIndex);
            return listSynTargetIndex;
        }

        //Lay len danh sach hoat dong duoc khai bao
        public List<TargetActionModel> GetActionPlaned(int PeriodID, int TargetID = 0)
        {
            UserInfo user = SessionManager.GetUserInfo();
            var ActionPlaneds = from ta in context.PlanTargetAction
                                join ma in context.MainAction on ta.MainActionID equals ma.MainActionID
                                join tg in context.PlanTarget on ta.PlanTargetID equals tg.PlanTargetID
                                join pl in context.Plan on tg.PlanID equals pl.PlanID
                                join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                where pl.PeriodID == PeriodID && pl.Status == Constants.PLAN_APPROVED && pl.Type == Constants.MAIN_TYPE
                                && d.Status == Constants.IS_ACTIVE && d.DepartmentType == Constants.DEPT_TYPE_PBQUY
                                && d.DepartmentID != user.DepartmentID && ta.MainActionID != null && (TargetID == 0 || tg.TargetID == TargetID)
                                orderby ma.MainActionID
                                select new TargetActionModel
                                {
                                    PlanID = pl.PlanID,
                                    PlanTargetActionID = ta.PlanTargetActionID,
                                    ActionCode = ta.ActionCode,
                                    ActionContent = ta.ActionContent,
                                    ActionMethod = ta.ActionMethod,
                                    ContentPropaganda = ta.ContentPropaganda,
                                    Result = ta.Result,
                                    PlanTargetID = ta.PlanTargetID,
                                    TargetID = tg.TargetID,
                                    MainActionID = ma.MainActionID
                                };
            List<TargetActionModel> lstActions = ActionPlaneds.ToList();
            return lstActions;
        }

        public List<SubActionPlanModel> GetSubActionPlaned(int PeriodID, int TargetID = 0)
        {
            UserInfo user = SessionManager.GetUserInfo();
            var IQSubAction = (from s in context.SubActionPlan
                               join ma in context.MainAction on s.SubActionID equals ma.MainActionID
                               join ta in context.PlanTargetAction on s.PlanTargetActionID equals ta.PlanTargetActionID
                               join tg in context.PlanTarget on ta.PlanTargetID equals tg.PlanTargetID
                               join pl in context.Plan on tg.PlanID equals pl.PlanID
                               join d in context.Department on pl.DepartmentID equals d.DepartmentID
                               where pl.PeriodID == PeriodID && pl.Status == Constants.PLAN_APPROVED
                               && d.Status == Constants.IS_ACTIVE && d.DepartmentType == Constants.DEPT_TYPE_PBQUY
                               && d.DepartmentID != user.DepartmentID && ta.MainActionID != null && s.SubActionID != null
                               orderby ma.MainActionID
                               select new SubActionPlanModel
                               {
                                   SubActionContent = s.SubActionContent,
                                   ContentPropaganda = s.ContentPropaganda,
                                   SubActionObject = s.SubActionObject,
                                   SubActionMethod = s.SubActionMethod,
                                   Result = s.Result,
                                   SubActionPlanID = s.SubActionPlanID,
                                   PlanID = pl.PlanID,
                                   SubActionOrder = s.SubActionOrder,
                                   PlanTargetActionID = ta.PlanTargetActionID,
                                   SubTargetID = s.SubActionID
                               });
            return IQSubAction.ToList();
        }

        public Dictionary<int, string> GetResultByActionIndex(List<int> listPlanTargetActionID, int planTargetID)
        {
            List<PlanActionIndexModel> listActionIndex = (from x in context.PlanActionIndex
                                                          join y in context.ActionIndex on x.ActionIndexID equals y.ActionIndexID
                                                          where x.PlanTargetID == planTargetID && listPlanTargetActionID.Contains(x.PlanTargetActionID) && x.SubActionPlanID == null
                                                          select new PlanActionIndexModel
                                                          {
                                                              PlanActionIndexID = x.PlanActionIndexID,
                                                              SubActionPlanID = x.SubActionPlanID,
                                                              ActionIndexID = x.ActionIndexID,
                                                              PlanTargetActionID = x.PlanTargetActionID,
                                                              IndexValue = x.IndexValue,
                                                              IndexName = y.IndexName,
                                                              IndexUnitName = y.UnitName,
                                                              PlanTargetID = x.PlanTargetID
                                                          }).ToList();
            Dictionary<int, string> dicResult = new Dictionary<int, string>();
            foreach (int planTargetActionID in listPlanTargetActionID)
            {
                string result = string.Empty;
                List<PlanActionIndexModel> planActionIndexs = listActionIndex.Where(x => x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID == null).ToList();
                if (planActionIndexs != null && planActionIndexs.Any())
                {
                    foreach (PlanActionIndexModel planActionIndex in planActionIndexs)
                    {
                        result += "; " + planActionIndex.GetFullInfo();
                    }
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Substring(2);
                }
                dicResult[planTargetActionID] = result;
            }
            return dicResult;
        }

        public Dictionary<int, string> GetResultBySubActionIndex(List<int> listSubActionID, int planTargetID)
        {
            List<PlanActionIndexModel> listActionIndex = (from x in context.PlanActionIndex
                                                          join y in context.ActionIndex on x.ActionIndexID equals y.ActionIndexID
                                                          where x.PlanTargetID == planTargetID && x.SubActionPlanID != null && listSubActionID.Contains(x.SubActionPlanID.Value)
                                                          select new PlanActionIndexModel
                                                          {
                                                              PlanActionIndexID = x.PlanActionIndexID,
                                                              SubActionPlanID = x.SubActionPlanID,
                                                              ActionIndexID = x.ActionIndexID,
                                                              PlanTargetActionID = x.PlanTargetActionID,
                                                              IndexValue = x.IndexValue,
                                                              IndexName = y.IndexName,
                                                              IndexUnitName = y.UnitName,
                                                              PlanTargetID = x.PlanTargetID
                                                          }).ToList();
            Dictionary<int, string> dicResult = new Dictionary<int, string>();
            foreach (int subActionID in listSubActionID)
            {
                string result = string.Empty;
                List<PlanActionIndexModel> planActionIndexs = listActionIndex.Where(x => x.PlanTargetActionID == subActionID && x.SubActionPlanID == null).ToList();
                if (planActionIndexs != null && planActionIndexs.Any())
                {
                    foreach (PlanActionIndexModel planActionIndex in planActionIndexs)
                    {
                        result += "; " + planActionIndex.GetFullInfo();
                    }
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Substring(2);
                }
                dicResult[subActionID] = result;
            }
            return dicResult;
        }

        public Dictionary<int, double> GetPlanCostByYear(int PlanID)
        {
            Plan entity = context.Plan.Find(PlanID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            Period period = context.Period.Find(entity.PeriodID);
            Dictionary<int, double> listCost = new Dictionary<int, double>();
            if (entity.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                List<long> listRootSpendID = context.CostEstimateSpend.Where(x => x.PlanID == PlanID && x.PlanTargetID == null && x.ParentID == null)
                    .Select(x => x.CostEstimateSpendID).ToList();
                var listCostEstimate = context.CostEstimateDetail.Where(x => x.PlanID == PlanID)
                    .Where(x => (x.PlanTargetID != null && x.PlanTargetActionID == null) || (x.CostEstimateSpendID != null && x.PlanTargetID == null && listRootSpendID.Contains(x.CostEstimateSpendID.Value)))
                    .GroupBy(x => x.PlanYear).Select(x => new
                    {
                        Year = x.Key,
                        TotalMoney = x.Sum(t => t.TotalMoney)
                    })
                    .ToList();
                int startYear = period.FromDate.Year;
                int endYear = period.ToDate.Year;
                for (int year = startYear; year <= endYear; year++)
                {
                    var cost = listCostEstimate.Where(x => x.Year == year).FirstOrDefault();
                    if (cost != null)
                    {
                        listCost[year] = cost.TotalMoney;
                    }
                    else
                    {
                        listCost[year] = 0;
                    }
                }
            }
            else
            {
                var listCostEstimate = context.SubActionPlanCost.Where(x => context.PlanTarget.Any(t => t.PlanTargetID == x.PlanTargetID && t.PlanID == PlanID))
                    .Where(x => x.PlanTargetID != null && x.PlanTargetActionID == null && x.SubActionSpendItemID == null)
                    .GroupBy(x => x.PlanYear).Select(x => new
                    {
                        Year = x.Key,
                        TotalMoney = x.Sum(t => t.TotalPrice == null ? 0 : t.TotalPrice.Value)
                    })
                    .ToList();
                int startYear = period.FromDate.Year;
                int endYear = period.ToDate.Year;
                for (int year = startYear; year <= endYear; year++)
                {
                    var cost = listCostEstimate.Where(x => x.Year == year).FirstOrDefault();
                    if (cost != null)
                    {
                        listCost[year] = cost.TotalMoney;
                    }
                    else
                    {
                        listCost[year] = 0;
                    }
                }
            }
            return listCost;
        }

        public void InitTime(int planID)
        {
            Plan plan = context.Plan.Find(planID);
            Period period = context.Period.Find(plan.PeriodID);
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanTargetID == x.PlanTargetID && t.PlanID == planID)).ToList();
            List<SubActionPlan> listSubAtion = context.SubActionPlan.Where(x => x.PlanID == planID).ToList();
            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            if (endYear >= startYear)
            {
                for (int year = startYear; year <= endYear; year++)
                {
                    foreach (PlanTargetAction pta in listPlanTargetAction)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            SubActionPlanTime subActionPlanTime = new SubActionPlanTime();
                            subActionPlanTime.PlanTime = null;
                            subActionPlanTime.PlanTargetActionID = pta.PlanTargetActionID;
                            subActionPlanTime.SubActionPlanID = null;
                            subActionPlanTime.PlanYear = year;
                            subActionPlanTime.CreateDate = DateTime.Now;
                            subActionPlanTime.CreateUserID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.Status = Constants.IS_ACTIVE;
                            subActionPlanTime.ModifierID = SessionManager.GetUserInfo().UserID;
                            subActionPlanTime.UpdateDate = DateTime.Now;
                            context.SubActionPlanTime.Add(subActionPlanTime);
                            context.SaveChanges();
                        }
                        foreach (SubActionPlan sub in listSubAtion)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                SubActionPlanTime subActionPlanTime = new SubActionPlanTime();
                                subActionPlanTime.PlanTime = null;
                                subActionPlanTime.PlanTargetActionID = sub.PlanTargetActionID;
                                subActionPlanTime.SubActionPlanID = sub.SubActionPlanID;
                                subActionPlanTime.PlanYear = year;
                                subActionPlanTime.CreateDate = DateTime.Now;
                                subActionPlanTime.CreateUserID = SessionManager.GetUserInfo().UserID;
                                subActionPlanTime.Status = Constants.IS_ACTIVE;
                                subActionPlanTime.ModifierID = SessionManager.GetUserInfo().UserID;
                                subActionPlanTime.UpdateDate = DateTime.Now;
                                context.SubActionPlanTime.Add(subActionPlanTime);
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
        }

        public List<SelectListItem> GetlistDepartment(int userID)
        {
            // Lay danh sach don vi phan cong
            List<SelectListItem> listDept = (from m in this.context.UserManageDepartment
                                             join d in this.context.Department on m.DepartmentID equals d.DepartmentID
                                             where m.UserID == userID && d.Status == Constants.IS_ACTIVE
                                             orderby d.DepartmentName
                                             select new SelectListItem
                                             {
                                                 Value = d.DepartmentID + "",
                                                 Text = d.DepartmentName
                                             }).ToList();
            if (!listDept.Any())
            {
                listDept = (from d in this.context.Department
                            where d.Status == Constants.IS_ACTIVE
                            orderby d.DepartmentName
                            select new SelectListItem
                            {
                                Value = d.DepartmentID + "",
                                Text = d.DepartmentName
                            }).ToList();
            }
            listDept.Insert(0, new SelectListItem { Value = "0", Text = "---Tất cả---" });

            return listDept;
        }

        public List<ChangeRequestActionModel> GetRequestAction(int planID)
        {
            Plan plan = context.Plan.Find(planID);
            List<ChangeRequestActionModel> listChangeAction = null;
            if (plan != null && plan.IsChange.GetValueOrDefault() == Constants.IS_ACTIVE)
            {
                ChangeRequest cr = context.ChangeRequest.Where(x => x.PlanID == plan.PlanID && x.Status == Constants.PLAN_APPROVED && x.IsFinish == null).FirstOrDefault();
                if (cr != null)
                {
                    // Lay thong tin hoat dong khoa
                    listChangeAction = context.ChangeRequestAction.Where(x => x.ChangeRequestID == cr.ChangeRequestID && x.ApproveStatus == Constants.IS_ACTIVE)
                        .Select(x => new ChangeRequestActionModel
                        {
                            ApproveStatus = x.ApproveStatus,
                            ChangeRequestActionID = x.ChangeRequestActionID,
                            ChangeRequestID = x.ChangeRequestID,
                            PlanTargetActionID = x.PlanTargetActionID,
                            PlanTargetID = x.PlanTargetID
                        }).ToList();
                }
            }
            return listChangeAction;
        }

        public List<ChangeRequestActionModel> GetPlanUnlockAction(int planID)
        {
            // Lay thong tin hoat dong khoa
            List<ChangeRequestActionModel> listChangeAction = context.ChangeRequestAction.Where(x => x.PlanID == planID && x.ApproveStatus == Constants.IS_ACTIVE)
                .Select(x => new ChangeRequestActionModel
                {
                    ApproveStatus = x.ApproveStatus,
                    PlanID = x.PlanID,
                    ChangeRequestID = x.ChangeRequestID,
                    PlanTargetActionID = x.PlanTargetActionID,
                    PlanTargetID = x.PlanTargetID
                }).ToList();
            if (!listChangeAction.Any())
            {
                return null;
            }
            return listChangeAction;
        }

        public List<SubActionPlanBO> GetListActionChange(int planID)
        {
            List<SubActionPlanBO> listSubActionPlan = new List<SubActionPlanBO>();
            // Lay ke hoach duoc phe duyet gan nhat de so sanh
            PlanHis planHis = context.PlanHis.Where(x => x.PlanID == planID && x.Status == Constants.PLAN_APPROVED).OrderByDescending(x => x.ID).FirstOrDefault();
            if (planHis != null)
            {
                long planHisID = planHis.ID;
                List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => x.PlanID == planID).ToList();
                List<PlanTargetActionHis> listPlanTargetActionHis = context.PlanTargetActionHis.Where(x => x.PlanHisID == planHisID && x.PlanID == planID).ToList();
                List<SubActionPlanTime> listSubTime = context.SubActionPlanTime.Where(x => context.PlanTargetAction.Any(t => t.PlanTargetActionID == x.PlanTargetActionID && t.PlanID == planID)).ToList();
                List<SubActionPlanTimeHis> listSubTimeHis = context.SubActionPlanTimeHis.Where(x => x.PlanHisID == planHisID && context.PlanTargetActionHis.Any(t => t.PlanTargetActionID == x.PlanTargetActionID && t.PlanID == planID && t.PlanHisID == planHisID)).ToList();
                List<SubActionPlanCost> listSubCost = context.SubActionPlanCost.Where(x => context.PlanTargetAction.Any(t => t.PlanTargetActionID == x.PlanTargetActionID && t.PlanID == planID)).ToList();
                List<SubActionPlanCostHis> listSubCostHis = context.SubActionPlanCostHis.Where(x => x.PlanHisID == planHisID && context.PlanTargetActionHis.Any(t => t.PlanTargetActionID == x.PlanTargetActionID && t.PlanID == planID && t.PlanHisID == planHisID)).ToList();
                List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanID == planID).ToList();
                List<SubActionPlanHis> listSubActionHis = context.SubActionPlanHis.Where(x => x.PlanHisID == planHisID && x.PlanID == planID).ToList();
                foreach (PlanTargetAction pta in listPlanTargetAction)
                {
                    PlanTargetActionHis ptaHis = listPlanTargetActionHis.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).FirstOrDefault();
                    if (ptaHis != null)
                    {
                        bool isChange = false;
                        string changeContent = string.Empty;
                        if (!Utils.StringEqual(pta.MainActionID.GetValueOrDefault().ToString(), ptaHis.MainActionID.GetValueOrDefault().ToString()))
                        {
                            isChange = true;
                            changeContent = "Thay đổi hoạt động gợi ý \r\n";
                        }
                        // So sanh noi dung
                        if (!Utils.StringEqual(pta.ActionContent, ptaHis.ActionContent))
                        {
                            isChange = true;
                            changeContent = "Thay đổi tên hoạt động \r\n";
                        }
                        if (!Utils.StringEqual(pta.ContentPropaganda, ptaHis.ContentPropaganda))
                        {
                            isChange = true;
                            changeContent = "Thay đổi nội dung hoạt động \r\n";
                        }
                        if (!Utils.StringEqual(pta.ObjectScope, ptaHis.ObjectScope))
                        {
                            isChange = true;
                            changeContent = "Thay đổi đối tượng, phạm vi của hoạt động \r\n";
                        }
                        if (!Utils.StringEqual(pta.ActionMethod, ptaHis.ActionMethod))
                        {
                            isChange = true;
                            changeContent = "Thay đổi phương thức hoạt động \r\n";
                        }
                        if (!Utils.StringEqual(pta.Result, ptaHis.Result))
                        {
                            isChange = true;
                            changeContent = "Thay đổi kết quả dự kiến hoạt động \r\n";
                        }
                        // Chi phi, ke hoach thoi gian
                        if (!Utils.StringEqual(pta.TotalBudget.GetValueOrDefault().ToString(), ptaHis.TotalBudget.GetValueOrDefault().ToString()))
                        {
                            isChange = true;
                            changeContent = "Thay đổi tổng chi phí của hoạt động gợi ý \r\n";
                        }
                        if (!isChange)
                        {
                            List<SubActionPlanTime> listActionTime = listSubTime.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                            List<SubActionPlanTimeHis> listActionTimeHis = listSubTimeHis.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                            for (int i = 0; i < listActionTime.Count; i++)
                            {
                                SubActionPlanTime subTime = listActionTime[i];
                                if (listActionTimeHis.Count >= i + 1)
                                {
                                    SubActionPlanTimeHis subTimeHis = listActionTimeHis[i];
                                    if (!Utils.StringEqual(subTime.PlanTime, subTimeHis.PlanTime))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi thời gian của hoạt động gợi ý \r\n";
                                    }
                                }
                            }

                        }
                        if (isChange)
                        {
                            SubActionPlanBO cAction = new SubActionPlanBO
                            {
                                PlanTargetID = pta.PlanTargetID.GetValueOrDefault(),
                                PlanTargetActionID = pta.PlanTargetActionID,
                            };
                            listSubActionPlan.Add(cAction);
                        }
                        else
                        {
                            #region Hoat dong con
                            // Check toi hoat dong con
                            List<SubActionPlan> listSubActionByPta = listSubAction.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).ToList();
                            List<SubActionPlanHis> listSubActionHisByPta = listSubActionHis.Where(x => x.PlanTargetActionID == ptaHis.PlanTargetActionID).ToList();
                            foreach (SubActionPlan sa in listSubActionByPta)
                            {
                                SubActionPlanHis saHis = listSubActionHisByPta.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID && x.SubActionPlanID == sa.SubActionPlanID).FirstOrDefault();
                                if (saHis != null)
                                {
                                    isChange = false;
                                    if (!Utils.StringEqual(sa.SubActionID.GetValueOrDefault().ToString(), saHis.SubActionID.GetValueOrDefault().ToString()))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi hoạt động gợi ý của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    // So sanh noi dung
                                    if (!Utils.StringEqual(sa.SubActionContent, saHis.SubActionContent))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi tên của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    if (!Utils.StringEqual(sa.ContentPropaganda, saHis.ContentPropaganda))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi nội dung của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    if (!Utils.StringEqual(sa.SubActionObject, saHis.SubActionObject))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi đối tượng, phạm vi của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    if (!Utils.StringEqual(sa.SubActionMethod, saHis.SubActionMethod))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi phương thức của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    if (!Utils.StringEqual(sa.Result, saHis.Result))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi kết quả dự kiến của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    // Chi phi, ke hoach thoi gian
                                    if (!Utils.StringEqual(sa.TotalBudget.GetValueOrDefault().ToString(), saHis.TotalBudget.GetValueOrDefault().ToString()))
                                    {
                                        isChange = true;
                                        changeContent = "Thay đổi chi phí của hoạt động con:" + sa.SubActionContent + " \r\n";
                                    }
                                    if (!isChange)
                                    {
                                        List<SubActionPlanTime> listActionTime = listSubTime.Where(x => x.SubActionPlanID == sa.SubActionPlanID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                                        List<SubActionPlanTimeHis> listActionTimeHis = listSubTimeHis.Where(x => x.SubActionPlanID == saHis.SubActionPlanID).OrderBy(x => x.SubActionPlanTimeID).ToList();
                                        for (int i = 0; i < listActionTime.Count; i++)
                                        {
                                            SubActionPlanTime subTime = listActionTime[i];
                                            if (listActionTimeHis.Count >= i + 1)
                                            {
                                                SubActionPlanTimeHis subTimeHis = listActionTimeHis[i];
                                                if (!Utils.StringEqual(subTime.PlanTime, subTimeHis.PlanTime))
                                                {
                                                    isChange = true;
                                                    changeContent = "Thay đổi thời gian của hoạt động con:" + sa.SubActionContent + " \r\n";
                                                }
                                            }
                                        }

                                    }
                                    if (isChange)
                                    {
                                        SubActionPlanBO cAction = new SubActionPlanBO
                                        {
                                            PlanTargetID = pta.PlanTargetID.GetValueOrDefault(),
                                            PlanTargetActionID = pta.PlanTargetActionID,
                                            SubActionPlanID = sa.SubActionPlanID,
                                            ChangeContent = changeContent
                                        };
                                        listSubActionPlan.Add(cAction);
                                    }
                                }
                                else
                                {
                                    SubActionPlanBO cAction = new SubActionPlanBO
                                    {
                                        PlanTargetID = pta.PlanTargetID.GetValueOrDefault(),
                                        PlanTargetActionID = pta.PlanTargetActionID,
                                        SubActionPlanID = sa.SubActionPlanID,
                                        ChangeContent = "Thêm mới hoạt động con: " + sa.SubActionContent
                                    };
                                    listSubActionPlan.Add(cAction);
                                }
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        SubActionPlanBO cAction = new SubActionPlanBO
                        {
                            PlanTargetID = pta.PlanTargetID.GetValueOrDefault(),
                            PlanTargetActionID = pta.PlanTargetActionID,
                            ChangeContent = "Thêm mới hoạt động"
                        };
                        listSubActionPlan.Add(cAction);
                    }
                }
            }
            return listSubActionPlan;
        }

        public void RestoreApprovePlan(int planID)
        {
            // Lay ke hoach duoc phe duyet gan nhat de so sanh
            PlanHis planHis = context.PlanHis.Where(x => x.PlanID == planID && x.Status == Constants.PLAN_APPROVED).OrderByDescending(x => x.ID).FirstOrDefault();
            if (planHis == null)
            {
                return;
            }
            List<int> listPlanTargetID = context.PlanTarget.Where(x => x.PlanID == planID).Select(x => x.PlanTargetID).ToList();
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => x.PlanTargetID != null && x.PlanID == planID).ToList();
            List<int> listPlanTargetActionID = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();
            // Chi so hoat dong
            List<PlanActionIndex> listPlanActionIndex = context.PlanActionIndex.Where(x => listPlanTargetID.Contains(x.PlanTargetID)).ToList();
            List<PlanActionIndexHis> listPlanActionIndexHis = context.PlanActionIndexHis.Where(x => x.PlanHisID == planHis.ID).ToList();
            foreach (PlanActionIndex planActionIndex in listPlanActionIndex)
            {
                PlanActionIndexHis planActionIndexHis = listPlanActionIndexHis.Where(x => x.PlanActionIndexID == planActionIndex.PlanActionIndexID).FirstOrDefault();
                if (planActionIndexHis != null)
                {
                    Utils.Copy(planActionIndexHis, planActionIndex);
                }
                else
                {
                    context.PlanActionIndex.Remove(planActionIndex);
                }
            }
            // Thoi gian
            List<SubActionPlanTime> listTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID != null
            && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
            List<SubActionPlanTimeHis> listTimeHis = context.SubActionPlanTimeHis.Where(x => x.PlanTargetActionID != null
            && x.PlanHisID == planHis.ID).ToList();
            foreach (SubActionPlanTime time in listTime)
            {
                SubActionPlanTimeHis timeHis = listTimeHis.Where(x => x.SubActionPlanTimeID == time.SubActionPlanTimeID).FirstOrDefault();
                if (timeHis != null)
                {
                    Utils.Copy(timeHis, time);
                }
                else
                {
                    context.SubActionPlanTime.Remove(time);
                }
            }
            // Kinh phi
            List<SubActionSpendItem> listSpend = context.SubActionSpendItem.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
            List<SubActionSpendItemHis> listSpendHis = context.SubActionSpendItemHis.Where(x => x.PlanTargetActionID != null && x.PlanHisID == planHis.ID).ToList();
            foreach (SubActionSpendItem spend in listSpend)
            {
                SubActionSpendItemHis spendHis = listSpendHis.Where(x => x.SubActionSpendItemID == spend.SubActionSpendItemID).FirstOrDefault();
                if (spendHis != null)
                {
                    Utils.Copy(spendHis, spend);
                }
                else
                {
                    context.SubActionSpendItem.Remove(spend);
                }
            }
            List<SubActionPlanCost> listCost = context.SubActionPlanCost.Where(x => x.PlanTargetID != null
            && listPlanTargetID.Contains(x.PlanTargetID.Value)).ToList();
            List<SubActionPlanCostHis> listCostHis = context.SubActionPlanCostHis.Where(x => x.PlanTargetID != null
            && x.PlanHisID == planHis.ID).ToList();
            foreach (SubActionPlanCost cost in listCost)
            {
                SubActionPlanCostHis costHist = listCostHis.Where(x => x.SubActionPlanCostID == cost.SubActionPlanCostID).FirstOrDefault();
                if (costHist != null)
                {
                    Utils.Copy(costHist, cost);
                }
                else
                {
                    context.SubActionPlanCost.Add(cost);
                }
            }
            // Hoat dong chau
            List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value) && x.PlanID == planID).ToList();
            List<SubActionPlanHis> listSubActionHis = context.SubActionPlanHis.Where(x => x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value) && x.PlanID == planID && x.PlanHisID == planHis.ID).ToList();
            foreach (SubActionPlan subAction in listSubAction)
            {
                SubActionPlanHis subActionHis = listSubActionHis.Where(x => x.SubActionPlanID == subAction.SubActionPlanID).FirstOrDefault();
                if (subActionHis != null)
                {
                    Utils.Copy(subActionHis, subAction);
                }
                else
                {
                    context.SubActionPlan.Remove(subAction);
                }
            }
            // Hoat dong cha

            List<PlanTargetActionHis> listPlanTargetActionHis = context.PlanTargetActionHis.Where(x => x.PlanTargetID != null && x.PlanID == planID && x.PlanHisID == planHis.ID).ToList();
            foreach (PlanTargetAction planTargetAction in listPlanTargetAction)
            {
                PlanTargetActionHis planTargetActionHis = listPlanTargetActionHis.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID).FirstOrDefault();
                if (planTargetActionHis != null)
                {
                    Utils.Copy(planTargetActionHis, planTargetAction);
                }
                else
                {
                    context.PlanTargetAction.Remove(planTargetAction);
                }
            }
            context.SaveChanges();
        }
    }
}