﻿using System;
using System.Web.Mvc;
using IInterface.Business;
using IInterface.Models;

namespace IInterface.Controllers
{
    [AllowAnonymous]
    public class CaptchaController : Controller
    {
        // GET: Captcha
        [HttpPost]
        public string GetCurrentYear()
        {
            return DateTime.Now.Year.ToString();
        }

        [HttpPost]
        public JsonResult GetCaptcha()
        {
            var res = CaptchaBusiness.GetCaptcha();

            return Json(new { Type = "CAPTCHADATA", Data = res });
        }

        [HttpPost]
        public bool IsValidCaptcha(CaptchaForm captchaForm)
        {
            return CaptchaBusiness.IsValidCaptcha(captchaForm);
        }
    }
}