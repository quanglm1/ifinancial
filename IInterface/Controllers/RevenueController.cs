﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class RevenueController : Controller
    {
        // GET: Revenue
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Quản lý thu", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            List<SelectListItem> listPeriod =
                (from x in this._context.Period
                 where x.Status != Constants.PERIOD_DEACTIVED
                 orderby x.CreateDate descending
                 select new SelectListItem()
                 {
                     Value = "" + x.PeriodID,
                     Text = x.PeriodName
                 }).ToList();
            listPeriod.Insert(0, new SelectListItem { Value = "", Text = "--Tất cả--" });
            ViewData["listPeriod"] = listPeriod;
            return View();
        }

        public PartialViewResult Search(RevenueSearchModel model, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            IQueryable<RevenueModel> iqSearch =
                from x in this._context.Revenue
                select new RevenueModel
                {
                    Description = x.Description,
                    RevenueID = x.RevenueID,
                    PeriodID = x.PeriodID,
                    PeriodName = _context.Period.Where(p => p.PeriodID == x.PeriodID).Select(p => p.PeriodName).FirstOrDefault(),
                    Source = x.Source,
                    CreateDate = x.CreateDate,
                    Money = x.Money != null ? "" + x.Money : "",
                    RevenueDate = x.RevenueDate
                };
            if (model.PeriodID != null)
            {
                iqSearch = iqSearch.Where(x => x.PeriodID == model.PeriodID);
            }
            if (!string.IsNullOrWhiteSpace(model.Source))
            {
                string s = model.Source.Trim().ToLower();
                iqSearch = iqSearch.Where(x => x.Source.ToLower().Contains(s));
            }
            if (model.FromRevenueDate != null)
            {
                iqSearch = iqSearch.Where(x => x.RevenueDate >= model.FromRevenueDate);
            }
            if (model.ToRevenueDate != null)
            {
                iqSearch = iqSearch.Where(x => x.RevenueDate <= model.ToRevenueDate);
            }
            Paginate<RevenueModel> paginate = new Paginate<RevenueModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = iqSearch.OrderBy(x => x.RevenueDate).ThenBy(x => x.Source).Skip((page - 1) * Constants.PAGE_SIZE)
                    .Take(Constants.PAGE_SIZE).ToList(),
                Count = iqSearch.Count()
            };
            ViewData["listRevenue"] = paginate;
            return PartialView("_ListResult");
        }

        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            var model = new RevenueModel();
            if (id != null && id > 0)
            {
                model = this._context.Revenue
                    .Where(x => x.RevenueID == id)
                    .Select(x => new RevenueModel
                    {
                        Description = x.Description,
                        RevenueID = x.RevenueID,
                        PeriodID = x.PeriodID,
                        Source = x.Source,
                        CreateDate = x.CreateDate,
                        Money = x.Money != null ? "" + x.Money : "",
                        RevenueDate = x.RevenueDate
                    }).FirstOrDefault();

                if (model == null)
                {
                    throw new BusinessException("Không tồn tại khoản thu đã chọn");
                }
            }
            List<SelectListItem> listPeriod =
                (from x in this._context.Period
                 where x.Status != Constants.PERIOD_DEACTIVED
                 orderby x.CreateDate descending
                 select new SelectListItem()
                 {
                     Value = "" + x.PeriodID,
                     Text = x.PeriodName
                 }).ToList();
            ViewData["listPeriod"] = listPeriod;
            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(RevenueModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            if (ModelState.IsValid)
            {
                string msg;
                if (model.RevenueID > 0)
                {
                    Revenue entity = this._context.Revenue.FirstOrDefault(x => x.RevenueID == model.RevenueID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại khoản thu đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật khoản thu thành công";
                }
                else
                {
                    Revenue entity = model.ToEntity();
                    this._context.Revenue.Add(entity);
                    this._context.SaveChanges();
                    msg = "Thêm mới khoản thu thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            Revenue obj = _context.Revenue.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại khoản thu đã chọn");
            }

            _context.Revenue.Remove(obj);

            this._context.SaveChanges();
            return Json("Xóa khoản thu thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}