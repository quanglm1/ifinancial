﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class RoleController : Controller
    {
        private readonly Entities _context = new Entities();

        // GET: Roles
        [BreadCrumb(ControllerName = "Nhóm người dùng", ActionName = "Nhóm người dùng", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult OpenAddOrEdit(int? id)
        {
            RoleModel model = null;
            if (id.HasValue && id.Value > 0)
            {
                model = this._context.Role.Where(x => x.RoleID == id && x.Status == Constants.IS_ACTIVE).Select(x => new RoleModel
                {
                    RoleID = x.RoleID,
                    RoleName = x.RoleName,
                    Description = x.Description,
                    Status = x.Status
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại nhóm người dùng đã chọn");
                }
            }
            return PartialView("_AddOrEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(RoleModel Role)
        {
            string msg;
            if (ModelState.IsValid)
            {
                if (Role.RoleID > 0)
                {
                    Role entity = this._context.Role.Where(x => x.RoleID == Role.RoleID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại nhóm người dùng đã chọn");
                    }
                    entity.RoleName = Role.RoleName.Trim();
                    entity.Description = string.IsNullOrWhiteSpace(Role.Description) ? null : Role.Description.Trim();
                    msg = "Cập nhật nhóm người dùng thành công";
                }
                else
                {
                    Role entity = new Role();
                    entity.RoleName = Role.RoleName.Trim();
                    entity.Description = string.IsNullOrWhiteSpace(Role.Description) ? null : Role.Description.Trim();
                    entity.CreateDate = DateTime.Now;
                    entity.CreateByID = SessionManager.GetUserInfo().UserID;
                    entity.Status = Constants.IS_ACTIVE;
                    this._context.Role.Add(entity);
                    msg = "Thêm mới nhóm người dùng thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public JsonResult Delete(int id)
        {
            Role Role = _context.Role.Where(x => x.RoleID == id && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (Role == null)
            {
                throw new BusinessException("Không tồn tại nhóm người dùng đã chọn");
            }
            int countUserRole = (from u in this._context.User
                                 join r in this._context.UserRole on u.UserID equals r.UserID
                                 where u.Status != Constants.IS_DELETED_USER && r.RoleID == id
                                 select u.UserID).Count();
            if(countUserRole > 0)
            {
                throw new BusinessException("Nhóm người dùng đã có tài khoản không thể xóa dữ liệu");
            }
            Role.Status = Constants.IS_NOT_ACTIVE;
            this._context.SaveChanges();
            return Json("Xóa nhóm người dùng thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ViewListMenuForAdd(int id)
        {
            Role entity = this._context.Role.Where(x => x.RoleID == id && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại nhóm người dùng đã chọn");
            }
            List<int> listMenuRole = this._context.MenuRole.Where(x => x.RoleID == id).Select(x => x.MenuID).ToList();
            List<MenuModel> listMenu = this._context.Menu.Where(x => x.Status == Constants.IS_ACTIVE && x.ParentID == null)
                .Select(x => new MenuModel
                {
                    MenuID = x.MenuID,
                    MenuName = x.MenuName,
                    IsSelected = listMenuRole.Contains(x.MenuID)
                }).ToList();
            // Lay danh sach menu con
            List<int> listMenuId = listMenu.Select(x => x.MenuID).ToList();
            List<Menu> listChildMenu = this._context.Menu.Where(x => x.Status == Constants.IS_ACTIVE && listMenuId.Contains(x.ParentID.Value)).ToList();
            listMenu.ForEach(x =>
            {
                x.ListChildMenu = listChildMenu.Where(c => c.ParentID.Value == x.MenuID)
                .Select(c => new MenuModel
                {
                    MenuID = c.MenuID,
                    MenuName = c.MenuName,
                    IsSelected = listMenuRole.Contains(c.MenuID)
                }).ToList();
            });
            ViewData["listMenu"] = listMenu;
            ViewData["roleId"] = id;
            return PartialView("_AddMenu");
        }

        public JsonResult AddMenus(int roleId, int[] menuIds)
        {
            Role entity = _context.Role.Where(x => x.RoleID == roleId && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại nhóm người dùng đã chọn");
            }
            List<MenuRole> listMenuRoleExist = this._context.MenuRole.Where(x => x.RoleID == roleId).ToList();
            List<MenuRole> listAddMenuRole = new List<MenuRole>();
            // add them ca menu cha
            List<int> listParentId = this._context.Menu.Where(x => x.Status == Constants.IS_ACTIVE && menuIds.Contains(x.MenuID) && x.ParentID != null)
                .Select(x => x.ParentID.Value).ToList();
            List<int> listMenuID = menuIds.ToList();
            listMenuID.AddRange(listParentId);
            listMenuID = listMenuID.Distinct().ToList();
            foreach (int MenuId in listMenuID)
            {
                if (!listMenuRoleExist.Any(x => x.MenuID == MenuId))
                {
                    MenuRole ur = new MenuRole();
                    ur.MenuID = MenuId;
                    ur.RoleID = roleId;
                    listAddMenuRole.Add(ur);
                }
            }
            List<MenuRole> listMenuRoleDel = listMenuRoleExist.Where(x => !listMenuID.Contains(x.MenuID)).ToList();
            if (listAddMenuRole.Count > 0)
            {
                _context.MenuRole.AddRange(listAddMenuRole);
            }
            if (listMenuRoleDel.Count > 0)
            {
                _context.MenuRole.RemoveRange(listMenuRoleDel);
            }
            _context.SaveChanges();
            ViewData["RoleId"] = roleId;
            return Json("Cập nhật danh sách chức năng cho nhóm thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchRole(string RoleName = null, int page = 1)
        {
            if (string.IsNullOrWhiteSpace(RoleName))
            {
                RoleName = null;
            }
            else
            {
                RoleName = RoleName.Trim().ToLower();
            }
            IQueryable<RoleModel> iqRole = this._context.Role.Where(x => x.Status == Constants.IS_ACTIVE && (RoleName == null || x.RoleName.ToLower().Contains(RoleName))).Select(x => new RoleModel()
            {
                RoleID = x.RoleID,
                Description = x.Description,
                RoleName = x.RoleName,
                Status = x.Status,
                CreateDate = x.CreateDate
            });
            Paginate<RoleModel> paginate = new Paginate<RoleModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqRole.OrderBy(x => x.RoleName).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = iqRole.Count();
            ViewData["listRole"] = paginate;
            return PartialView("_ListRole");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}