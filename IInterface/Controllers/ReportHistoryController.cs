﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReportHistoryController : Controller
    {
        private Entities Context = new Entities();
        public PartialViewResult LoadReportHis(int reportID, int reportType)
        {
            List<ReportReviewModel> listReview = new ReportReviewBusiness(this.Context).GetListReportReview(reportID, reportType);
            ViewData["reportType"] = reportType;
            ViewData["listReview"] = listReview;
            return PartialView("_ReviewHistory");
        }

        public JsonResult Delete(int id)
        {
            ReportReview obj = Context.ReportReview.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn lịch sử đã chọn");
            }
            Context.ReportReview.Remove(obj);
            this.Context.SaveChanges();
            return Json("Xóa lịch sử thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}