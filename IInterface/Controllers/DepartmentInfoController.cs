﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;

namespace IInterface.Controllers
{
    public class DepartmentInfoController : Controller
    {
        private readonly Entities _context = new Entities();
        // GET: DepartmentInfo
        [BreadCrumb(ControllerName = "Thông tin đơn vị", ActionName = "Thông tin đơn vị", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            var user = SessionManager.GetUserInfo();
            int? id = null;
            if (user != null)
            {
                id = user.DepartmentID;
            }
            DepartmentModel model = null;
            if (id.HasValue && id.Value > 0)
            {
                Department entity = this._context.Department.FirstOrDefault(x => x.DepartmentID == id && x.Status == Constants.IS_ACTIVE);
                if (entity == null)
                {
                    throw new BusinessException("Không tồn tại đơn vị đã chọn");
                }
                model = new DepartmentModel();
                model.EntityTo(entity);
                ViewData["listMainAction"] = GetLstMainAction(model.TargetID);
            }
            // Lay don vi cha
            List<SelectListItem> listParent = this.GetListParent(id);
            listParent.Insert(0, new SelectListItem { Value = "0", Text = "---Chọn đơn vị cha---", Selected = (id.HasValue && model.ParentID == null) });
            ViewData["listParent"] = listParent;
            ViewData["listTarget"] = GetLstTarget();
            ViewData["listDeptType"] = Utils.GetDepartmentType();
            //return View("AddOrEdit", model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(DepartmentModel model)
        {
            var user = SessionManager.GetUserInfo();
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(model.DepartmentCode))
                {
                    string deptCode = model.DepartmentCode.Trim().ToLower();
                    if (this._context.Department.Any(x => x.Status == Constants.IS_ACTIVE
                                                          && (model.DepartmentID == 0 || x.DepartmentID != model.DepartmentID) && x.DepartmentCode.ToLower().Equals(deptCode)))
                    {
                        throw new BusinessException("Đã tồn tại đơn vị có mã là " + deptCode);
                    }
                }
                string msg;
                if (model.DepartmentID > 0)
                {
                    if (model.DepartmentID != user.DepartmentID)
                    {
                        throw new BusinessException("Đơn vị không hợp lệ");
                    }

                    Department entity = this._context.Department.FirstOrDefault(x => x.DepartmentID == model.DepartmentID && x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị đã chọn");
                    }
                    int? deptType = entity.DepartmentType;
                    int? parID = entity.ParentID;
                    model.UpdateEntity(entity);
                    entity.ParentID = parID;
                    entity.DepartmentType = deptType;
                    msg = "Cập nhật thông tin thành công";
                }
                else
                {
                    model.Creator = SessionManager.GetUserInfo().UserName;
                    Department entity = model.ToEntity();
                    this._context.Department.Add(entity);
                    msg = "Thêm mới đơn vị thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }


        private List<SelectListItem> GetListParent(int? id = null)
        {
            List<Department> listDepartment = this._context.Department.Where(x => x.Status == Constants.IS_ACTIVE
                                                                                  && (id == null || x.DepartmentID != id)).OrderBy(x => x.ParentID).ThenBy(x => x.DepartmentName).ToList();
            List<Department> listRoot = listDepartment.Where(x => x.ParentID == null).ToList();
            Dictionary<int, List<Department>> dicRoot1 = new Dictionary<int, List<Department>>();
            listRoot.ForEach(x =>
            {
                dicRoot1[x.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x.DepartmentID).ToList();
            });

            Dictionary<int, List<Department>> dicRoot2 = new Dictionary<int, List<Department>>();
            dicRoot1.Values.ToList().ForEach(x =>
            {
                x.ForEach(x1 =>
                {
                    dicRoot2[x1.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x1.DepartmentID).ToList();
                });
            });
            List<SelectListItem> listParent = new List<SelectListItem>();
            bool hasSelected = false;
            listRoot.ForEach(x =>
            {
                SelectListItem item = new SelectListItem
                {
                    Value = x.DepartmentID.ToString(),
                    Text = x.DepartmentName,
                    Selected = (!id.HasValue && !hasSelected && (x.ParentID == null))
                };
                if (x.ParentID == null)
                {
                    hasSelected = true;
                }
                listParent.Add(item);
                // cap 2
                if (dicRoot1.ContainsKey(x.DepartmentID))
                {
                    dicRoot1[x.DepartmentID].ForEach(x1 =>
                    {
                        SelectListItem item1 = new SelectListItem
                        {
                            Value = x1.DepartmentID.ToString(),
                            Text = "... " + x1.DepartmentName
                        };
                        listParent.Add(item1);
                        // cap 3
                        if (dicRoot2.ContainsKey(x1.DepartmentID))
                        {
                            dicRoot2[x1.DepartmentID].ForEach(x2 =>
                            {
                                SelectListItem item2 = new SelectListItem
                                {
                                    Value = x2.DepartmentID.ToString(),
                                    Text = "...... " + x2.DepartmentName
                                };
                                listParent.Add(item2);
                            });
                        }
                    });
                }
            });
            return listParent;
        }

        private List<SelectListItem> GetLstTarget()
        {
            var res = new List<SelectListItem>();
            res.Add(new SelectListItem
            {
                Text = "-- Chọn --",
                Value = ""
            });
            var lst = _context.Target.Where(o => o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.TargetTitle,
                Value = o.TargetID.ToString()
            }).ToList();

            if (lst.Count > 0)
            {
                res.AddRange(lst);
            }

            return res;
        }

        private List<SelectListItem> GetLstMainAction(int? targetId)
        {
            var res = new List<SelectListItem>();

            res = _context.MainAction.Where(o => o.TargetID == targetId && o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.ActionCode + "-" + o.ActionContent,
                Value = o.TargetID.ToString()
            }).ToList();

            return res;
        }
    }
}