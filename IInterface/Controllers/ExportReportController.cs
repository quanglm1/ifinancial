﻿using System;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using IInterface.Business;
using IInterface.Common.CommonExcel;

namespace IInterface.Controllers
{
    public class ExportReportController : Controller
    {
        // GET: ExportReport
        private readonly Entities _context = new Entities();

        #region Xuất báo cáo tài chính
        /// <summary>
        /// Báo cáo tài chính
        /// </summary>
        /// <param name="financialId"></param>
        /// <returns></returns>
        public FileResult ExportFinancialReport(int? financialId)
        {
            FinancialReportBusiness bus = new FinancialReportBusiness(_context);
            var model = bus.GetFinancialReportInfo(financialId.Value);
            if (model.Quarter != null)
            {
                return ExportFinancialReportQuarter(financialId);
            }
            else
            {
                return ExportFinancialReportYear(financialId);
            }
        }
        public FileResult ExportFinancialReportQuarter(int? financialId)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (financialId != null)
            {
                //lấy thông tin kỳ báo cáo tài chính
                FinancialReportBusiness bus = new FinancialReportBusiness(_context);

                var model = bus.GetFinancialReportInfo(financialId.Value);
                var plan = _context.Plan.Find(model.PlanID);


                // lấy danh sách mục tiêu
                List<PlanTargetModel> listTarget = (from pt in _context.PlanTarget
                                                    where pt.PlanID == plan.PlanID
                                                    orderby pt.TargetOrder
                                                    select new PlanTargetModel
                                                    {
                                                        PlanTargetID = pt.PlanTargetID,
                                                        PlanTargetTitle = pt.PlanTargetTitle,
                                                        TargetCode = pt.TargetCode,
                                                        PlanTargetContent = pt.PlanTargetContent
                                                    }).ToList();

                if (listTarget.Count > 0)
                {
                    string fileName = "Bao_Cao_Tai_Chinh_Quy.xlsx";
                    string filePath = Server.MapPath("~/Template/") + fileName;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                        {
                            byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            ms.Write(bytes, 0, (int)file.Length);

                            IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                            IXVTWorksheet sheet = oBook.GetSheet(1);
                            sheet.SheetName = "Phu luc 06A";
                            IXVTRange rangeData = sheet.GetRange(7, 1, 7, 8);
                            IXVTRange rangeDisb = sheet.GetRange(7, 2, 7, 3);

                            string donVi = userInfo.DepartmentName;
                            string reportName = "BÁO CÁO TÀI CHÍNH QUÝ " + Utils.ToQuarter(model.Quarter) + "/" +
                                                model.Year;

                            sheet.SetCellValue(1, 1, "Tên đơn vị: " + donVi);
                            sheet.SetCellValue(2, 1, reportName);

                            int startRow = 7;
                            string sumc = "";
                            string sumd = "";
                            string sume = "";
                            string sumf = "";
                            string sumg = "";
                            int countTarget = 1;
                            double totalDisb = 0;
                            Dictionary<string, List<int>> dicPosSum = new Dictionary<string, List<int>>();
                            Dictionary<string, int> dicPos = new Dictionary<string, int>();
                            foreach (var target in listTarget)
                            {
                                FinancialReport entity = _context.FinancialReport.FirstOrDefault(x => x.FinancialReportID == financialId
                                                                                                      && x.Status != Constants.IS_NOT_ACTIVE);
                                if (entity == null)
                                {
                                    throw new BusinessException("Không tồn tại báo cáo hoạt động hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
                                }
                                PlanTarget pt = _context.PlanTarget.FirstOrDefault(x => x.PlanID == entity.PlanID && x.PlanTargetID == target.PlanTargetID);
                                if (pt == null)
                                {
                                    throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
                                }
                                dicPos["target_" + pt.PlanTargetID] = startRow;
                                dicPosSum["target_" + pt.PlanTargetID] = new List<int>();
                                List<FinancialReportDetailModel> listDetail = bus.GetListAction(0, target.PlanTargetID, entity);
                                if (entity.Quarter != null)
                                {
                                    foreach (var detail in listDetail)
                                    {
                                        string actionCode = "";
                                        string actionName = "";
                                        if (detail.IsTarget)
                                        {
                                            actionCode = detail.TargetCode != null ? detail.TargetCode.Trim() : string.Empty;
                                            actionName = "Mục tiêu " + (countTarget++) + ": " + detail.TargetName;
                                        }
                                        else
                                        {
                                            actionCode = detail.PlanTargetActionCode != null ? detail.PlanTargetActionCode.Trim() : string.Empty;
                                            actionName = detail.PlanTargetActionName.Trim();
                                            dicPos["targetaction_" + detail.PlanTargetActionID] = startRow;
                                            dicPosSum["target_" + pt.PlanTargetID].Add(startRow);
                                            dicPosSum["targetaction_" + detail.PlanTargetActionID] = new List<int>();
                                        }

                                        double totalInQuarter = detail.DisbMoneyToQuarter.GetValueOrDefault() + detail.DisbMoney.GetValueOrDefault();
                                        double remainMoney = detail.PlanMoney.GetValueOrDefault() - totalInQuarter;

                                        rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 8));
                                        sheet.SetCellValue(startRow, 1, "'" + actionCode);
                                        sheet.SetCellValue(startRow, 2, actionName);
                                        if (detail.PlanMoney.GetValueOrDefault() != 0)
                                        {
                                            sheet.SetCellValue(startRow, 3, detail.PlanMoney.ToString());
                                            sheet.SetCellValue(startRow, 4, detail.DisbMoneyToQuarter.ToString());
                                            sheet.SetCellValue(startRow, 5, detail.DisbMoney.ToString());
                                            sheet.SetCellFormula(startRow, 6, "D" + startRow + "+" + "E" + startRow);
                                            sheet.SetCellFormula(startRow, 7, "C" + startRow + "-" + "F" + startRow);
                                            sheet.SetCellFormula(startRow, 8, "F" + startRow + "/" + "C" + startRow);
                                        }
                                        else
                                        {
                                            sheet.SetCellValue(startRow, 3, string.Empty);
                                            if (detail.DisbMoneyToQuarter.GetValueOrDefault() != 0
                                                || detail.DisbMoney.GetValueOrDefault() != 0)
                                            {
                                                sheet.SetCellValue(startRow, 4, detail.DisbMoneyToQuarter.ToString());
                                                sheet.SetCellValue(startRow, 5, detail.DisbMoney.ToString());
                                                sheet.SetCellFormula(startRow, 6, "D" + startRow + "+" + "E" + startRow);
                                                sheet.SetCellFormula(startRow, 7, "C" + startRow + "-" + "F" + startRow);
                                                sheet.SetCellValue(startRow, 8, string.Empty);
                                            }
                                            else
                                            {
                                                sheet.SetCellValue(startRow, 4, string.Empty);
                                                sheet.SetCellValue(startRow, 5, string.Empty);
                                                sheet.SetCellValue(startRow, 6, string.Empty);
                                                sheet.SetCellValue(startRow, 7, string.Empty);
                                                sheet.SetCellValue(startRow, 8, string.Empty);
                                            }
                                        }

                                        if (detail.IsTarget)
                                        {
                                            sumc += "C" + startRow + ",";
                                            sumd += "D" + startRow + ",";
                                            sume += "E" + startRow + ",";
                                            sumf += "F" + startRow + ",";
                                            sumg += "G" + startRow + ",";
                                            totalDisb += detail.DisbMoney.GetValueOrDefault();

                                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, true, false);
                                        }
                                        else
                                        {
                                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, false);
                                        }

                                        startRow++;

                                        if (detail.ListSubAction != null && detail.ListSubAction.Any())
                                        {
                                            foreach (var subDetail in detail.ListSubAction)
                                            {
                                                dicPos["subaction_" + subDetail.SubActionPlanID] = startRow;
                                                dicPosSum["targetaction_" + detail.PlanTargetActionID].Add(startRow);
                                                double subtotalInQuarter = subDetail.DisbMoneyToQuarter.GetValueOrDefault() + subDetail.DisbMoney.GetValueOrDefault();
                                                double subremainMoney = subDetail.TotalBudget.GetValueOrDefault() - totalInQuarter;
                                                rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 8));
                                                sheet.SetCellValue(startRow, 1, "'" + subDetail.SubActionCode.Trim());
                                                sheet.SetCellValue(startRow, 2, subDetail.SubActionContent.Trim());
                                                if (subDetail.TotalBudget.GetValueOrDefault() != 0)
                                                {
                                                    sheet.SetCellValue(startRow, 3, subDetail.TotalBudget.ToString());
                                                    sheet.SetCellValue(startRow, 4, subDetail.DisbMoneyToQuarter.ToString());
                                                    sheet.SetCellValue(startRow, 5, subDetail.DisbMoney.ToString());
                                                    sheet.SetCellFormula(startRow, 6, "D" + startRow + "+" + "E" + startRow);
                                                    sheet.SetCellFormula(startRow, 7, "C" + startRow + "-" + "F" + startRow);
                                                    sheet.SetCellFormula(startRow, 8, "F" + startRow + "/" + "C" + startRow);
                                                }
                                                else
                                                {
                                                    sheet.SetCellValue(startRow, 3, string.Empty);
                                                    if (subDetail.DisbMoneyToQuarter.GetValueOrDefault() != 0
                                                        || subDetail.DisbMoney.GetValueOrDefault() != 0)
                                                    {
                                                        sheet.SetCellValue(startRow, 4, subDetail.DisbMoneyToQuarter.ToString());
                                                        sheet.SetCellValue(startRow, 5, subDetail.DisbMoney.ToString());
                                                        sheet.SetCellFormula(startRow, 6, "D" + startRow + "+" + "E" + startRow);
                                                        sheet.SetCellFormula(startRow, 7, "C" + startRow + "-" + "F" + startRow);
                                                        sheet.SetCellValue(startRow, 8, string.Empty);
                                                    }
                                                    else
                                                    {
                                                        sheet.SetCellValue(startRow, 4, string.Empty);
                                                        sheet.SetCellValue(startRow, 5, string.Empty);
                                                        sheet.SetCellValue(startRow, 6, string.Empty);
                                                        sheet.SetCellValue(startRow, 7, string.Empty);
                                                        sheet.SetCellValue(startRow, 8, string.Empty);
                                                    }
                                                }

                                                if (detail.IsTarget)
                                                {
                                                    sumc += "C" + startRow + ",";
                                                    sumd += "D" + startRow + ",";
                                                    sume += "E" + startRow + ",";
                                                    sumf += "F" + startRow + ",";
                                                    sumg += "G" + startRow + ",";

                                                    sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, true, false);
                                                }
                                                else
                                                {
                                                    sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, false);
                                                }

                                                startRow++;
                                            }
                                        }
                                    }
                                }
                            }
                            // Thiet lap cong thuc tinh tong
                            List<string> listKey = dicPos.Keys.ToList();
                            foreach (string key in listKey)
                            {
                                int posSum = dicPos[key];
                                if (dicPosSum.ContainsKey(key))
                                {
                                    List<int> listPosSum = dicPosSum[key];
                                    if (listPosSum != null && listPosSum.Any())
                                    {
                                        string sum3 = string.Empty;
                                        string sum4 = string.Empty;
                                        string sum5 = string.Empty;
                                        foreach (int pos in listPosSum)
                                        {
                                            string colName = Utils.GetExcelColumnName(3);
                                            sum3 += ", " + colName + pos;
                                            colName = Utils.GetExcelColumnName(4);
                                            sum4 += ", " + colName + pos;
                                            colName = Utils.GetExcelColumnName(5);
                                            sum5 += ", " + colName + pos;
                                        }
                                        sheet.SetCellFormula(posSum, 3, "SUM(" + sum3.Substring(2) + ")");
                                        sheet.SetCellFormula(posSum, 4, "SUM(" + sum4.Substring(2) + ")");
                                        sheet.SetCellFormula(posSum, 5, "SUM(" + sum5.Substring(2) + ")");
                                    }
                                }
                            }

                            rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 8));
                            sheet.SetCellValue(startRow, 1, "");
                            sheet.SetCellValue(startRow, 2, "Tổng kinh phí");

                            sumc = sumc.TrimEnd(new char[] { ',' });
                            sumd = sumd.TrimEnd(new char[] { ',' });
                            sume = sume.TrimEnd(new char[] { ',' });
                            sumf = sumf.TrimEnd(new char[] { ',' });
                            sumg = sumg.TrimEnd(new char[] { ',' });
                            sumc = "=SUM(" + sumc + ")";
                            sumd = "=SUM(" + sumd + ")";
                            sume = "=SUM(" + sume + ")";
                            sumf = "=SUM(" + sumf + ")";
                            sumg = "=SUM(" + sumg + ")";

                            sheet.SetCellFormula(startRow, 3, sumc);
                            sheet.SetCellFormula(startRow, 4, sumd);
                            sheet.SetCellFormula(startRow, 5, sume);
                            sheet.SetCellFormula(startRow, 6, sumf);
                            sheet.SetCellFormula(startRow, 7, sumg);
                            string percent = "=F" + startRow + "/C" + startRow;
                            sheet.SetCellFormula(startRow, 8, percent);
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, true, false);
                            startRow++;
                            sheet.GetRange(startRow, 2, startRow, 8).Range.Merge();
                            sheet.SetCellValue(startRow, 2, "(Số tiền Quyết toán trong Quý bằng chữ: " + Utils.ChuyenSo(totalDisb.ToString()) + ")");
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, false);
                            sheet.SetRangeBorderThin(7, 1, startRow, 8);
                            startRow++;
                            startRow++;
                            int origin = startRow;
                            // Lay thong tin giai ngan
                            List<Disbursement> listDisb = _context.Disbursement.Where(x => x.PlanID == model.PlanID).OrderBy(x => x.Number).ToList();
                            totalDisb = 0;
                            string sumDisb = "C" + startRow;
                            foreach (Disbursement disb in listDisb)
                            {
                                rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                                totalDisb += disb.Money.GetValueOrDefault();
                                sheet.SetCellValue(startRow, 2, "Tạm ứng lần " + disb.Number + ":");
                                sheet.SetCellValue(startRow, 3, disb.Money.GetValueOrDefault().ToString());
                                startRow++;
                            }
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Tổng số tạm ứng:");
                            if (listDisb.Any())
                            {
                                sheet.SetCellFormula(startRow, 3, "SUM(" + sumDisb + ":C" + (startRow - 1) + ")");
                            }
                            else
                            {
                                sheet.SetCellValue(startRow, 3, "0");
                            }
                            startRow++;
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Tổng số giải ngân: ");
                            sheet.SetCellFormula(startRow, 3, "F" + (startRow - 4 - listDisb.Count));
                            startRow++;
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Chênh lệch tạm ứng và giải ngân: ");
                            sheet.SetCellFormula(startRow, 3, "C" + (startRow - 2) + "-C" + (startRow - 1));

                            sheet.SetRangeFont(origin, 1, startRow, 8, "Times New Roman", 12, false, false);
                            sheet.SetRangeBorderThin(origin, 2, startRow, 4);

                            startRow = startRow + 2;
                            int origin2 = startRow;
                            string today = ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " +
                                           DateTime.Now.Year;
                            sheet.GetRange(startRow, 6, startRow, 8).Range.Merge();
                            sheet.SetCellValue(startRow, 6, today);
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, true);

                            startRow++;
                            sheet.SetCellValue(startRow, 2, "Người lập báo cáo");
                            sheet.GetRange(startRow, 3, startRow, 5).Range.Merge();
                            sheet.SetCellValue(startRow, 3, "Kế toán trưởng");
                            sheet.GetRange(startRow, 6, startRow, 8).Range.Merge();
                            sheet.SetCellValue(startRow, 6, "Thủ trưởng đơn vị");
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, true, false);

                            startRow++;
                            sheet.GetRange(startRow, 6, startRow, 8).Range.Merge();
                            sheet.SetCellValue(startRow, 6, "(Ký tên, đóng dấu)");
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, true);


                            sheet.GetRange(origin2, 1, startRow, 8).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;

                            sheet.SetSheetWrapText();

                            Stream streamToWrite = oBook.ToStream();
                            byte[] bytesToWrite = new byte[streamToWrite.Length];
                            streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                            string newFile = "Bao_Cao_Tai_Chinh_Quy " + model.Quarter + "_Nam " + model.Year + "_" + userInfo.UserName + ".xlsx";
                            return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                        }
                    }
                }
                throw new BusinessException("Không có dữ liệu");
            }
            throw new BusinessException("Dữ liệu không hợp lệ");
        }

        public FileResult ExportFinancialReportYear(int? financialId)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (financialId != null)
            {
                //lấy thông tin kỳ báo cáo tài chính
                FinancialReportBusiness bus = new FinancialReportBusiness(_context);

                var model = bus.GetFinancialReportInfo(financialId.Value);
                var plan = _context.Plan.Find(model.PlanID);


                // lấy danh sách mục tiêu
                List<PlanTargetModel> listTarget = (from pt in _context.PlanTarget
                                                    where pt.PlanID == plan.PlanID
                                                    orderby pt.TargetOrder
                                                    select new PlanTargetModel
                                                    {
                                                        PlanTargetID = pt.PlanTargetID,
                                                        PlanTargetTitle = pt.PlanTargetTitle,
                                                        TargetCode = pt.TargetCode,
                                                        PlanTargetContent = pt.PlanTargetContent
                                                    }).ToList();

                if (listTarget.Count > 0)
                {
                    string fileName = "Bao_Cao_Tai_Chinh_Nam.xlsx";
                    string filePath = Server.MapPath("~/Template/") + fileName;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                        {
                            byte[] bytes = new byte[file.Length];
                            file.Read(bytes, 0, (int)file.Length);
                            ms.Write(bytes, 0, (int)file.Length);

                            IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                            IXVTWorksheet sheet = oBook.GetSheet(1);
                            sheet.SheetName = "Phu luc 07";
                            IXVTRange rangeData = sheet.GetRange(7, 1, 7, 6);
                            IXVTRange rangeDisb = sheet.GetRange(7, 2, 7, 3);
                            string donVi = userInfo.DepartmentName;
                            string reportName = "BÁO CÁO TÀI CHÍNH NĂM " + model.Year;

                            sheet.SetCellValue(1, 1, "Tên đơn vị: " + donVi);
                            sheet.SetCellValue(2, 1, reportName);

                            int startRow = 7;
                            string sumc = "";
                            string sumd = "";
                            string sume = "";
                            double totalDis = 0;
                            int countTarget = 1;
                            Dictionary<string, List<int>> dicPosSum = new Dictionary<string, List<int>>();
                            Dictionary<string, int> dicPos = new Dictionary<string, int>();
                            foreach (var target in listTarget)
                            {
                                FinancialReport entity = _context.FinancialReport.FirstOrDefault(x => x.FinancialReportID == financialId
                                                                                                      && x.Status != Constants.IS_NOT_ACTIVE);
                                if (entity == null)
                                {
                                    throw new BusinessException("Không tồn tại báo cáo hoạt động hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
                                }
                                PlanTarget pt = _context.PlanTarget.FirstOrDefault(x => x.PlanID == entity.PlanID && x.PlanTargetID == target.PlanTargetID);
                                if (pt == null)
                                {
                                    throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
                                }
                                dicPos["target_" + pt.PlanTargetID] = startRow;
                                dicPosSum["target_" + pt.PlanTargetID] = new List<int>();
                                List<FinancialReportDetailModel> listDetail = bus.GetListAction(0, target.PlanTargetID, entity);
                                if (entity.Quarter == null)
                                {
                                    foreach (var detail in listDetail)
                                    {
                                        string actionCode = "";
                                        string actionName = "";
                                        if (detail.IsTarget)
                                        {
                                            actionCode = detail.TargetCode != null ? detail.TargetCode.Trim() : string.Empty;
                                            actionName = "Mục tiêu " + (countTarget++) + ": " + detail.TargetName;
                                        }
                                        else
                                        {
                                            actionCode = detail.PlanTargetActionCode != null ? detail.PlanTargetActionCode.Trim() : string.Empty;
                                            actionName = detail.PlanTargetActionName.Trim();
                                            dicPos["targetaction_" + detail.PlanTargetActionID] = startRow;
                                            dicPosSum["target_" + pt.PlanTargetID].Add(startRow);
                                            dicPosSum["targetaction_" + detail.PlanTargetActionID] = new List<int>();
                                        }

                                        double remainMoney = detail.PlanMoney.GetValueOrDefault() - detail.DisbMoney.GetValueOrDefault();

                                        rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 6));
                                        sheet.SetCellValue(startRow, 1, "'" + actionCode);
                                        sheet.SetCellValue(startRow, 2, actionName);
                                        if (detail.PlanMoney.GetValueOrDefault() != 0)
                                        {
                                            sheet.SetCellValue(startRow, 3, detail.PlanMoney.ToString());
                                            sheet.SetCellValue(startRow, 4, detail.DisbMoney.ToString());
                                            sheet.SetCellFormula(startRow, 5, "C" + startRow + "-" + "D" + startRow);
                                            sheet.SetCellFormula(startRow, 6, "D" + startRow + "/" + "C" + startRow);
                                        }
                                        else
                                        {
                                            sheet.SetCellValue(startRow, 3, string.Empty);
                                            if (detail.DisbMoney.GetValueOrDefault() != 0)
                                            {
                                                sheet.SetCellValue(startRow, 4, detail.DisbMoney.ToString());
                                                sheet.SetCellFormula(startRow, 5, "C" + startRow + "-" + "D" + startRow);
                                                sheet.SetCellValue(startRow, 6, string.Empty);
                                            }
                                            else
                                            {
                                                sheet.SetCellValue(startRow, 4, string.Empty);
                                                sheet.SetCellValue(startRow, 5, string.Empty);
                                                sheet.SetCellValue(startRow, 6, string.Empty);
                                            }
                                        }

                                        if (detail.IsTarget)
                                        {
                                            sumc += "C" + startRow + ",";
                                            sumd += "D" + startRow + ",";
                                            sume += "E" + startRow + ",";
                                            totalDis += detail.DisbMoney.GetValueOrDefault();

                                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, true, false);
                                        }
                                        else
                                        {
                                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, false, false);
                                        }

                                        startRow++;

                                        if (detail.ListSubAction != null && detail.ListSubAction.Any())
                                        {
                                            foreach (var subDetail in detail.ListSubAction)
                                            {
                                                double subremainMoney = subDetail.TotalBudget.GetValueOrDefault() - subDetail.DisbMoney.GetValueOrDefault();

                                                rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 6));
                                                sheet.SetCellValue(startRow, 1, "'" + subDetail.SubActionCode.Trim());
                                                sheet.SetCellValue(startRow, 2, subDetail.SubActionContent.Trim());
                                                if (subDetail.TotalBudget.GetValueOrDefault() != 0)
                                                {
                                                    sheet.SetCellValue(startRow, 3, subDetail.TotalBudget.ToString());
                                                    sheet.SetCellValue(startRow, 4, subDetail.DisbMoney.ToString());
                                                    sheet.SetCellFormula(startRow, 5, "C" + startRow + "-" + "D" + startRow);
                                                    sheet.SetCellFormula(startRow, 6, "D" + startRow + "/" + "C" + startRow);
                                                }
                                                else
                                                {
                                                    sheet.SetCellValue(startRow, 3, string.Empty);
                                                    if (subDetail.DisbMoney.GetValueOrDefault() != 0)
                                                    {
                                                        sheet.SetCellValue(startRow, 4, subDetail.DisbMoney.ToString());
                                                        sheet.SetCellFormula(startRow, 5, "C" + startRow + "-" + "D" + startRow);
                                                        sheet.SetCellValue(startRow, 6, string.Empty);
                                                    }
                                                    else
                                                    {
                                                        sheet.SetCellValue(startRow, 4, string.Empty);
                                                        sheet.SetCellValue(startRow, 5, string.Empty);
                                                        sheet.SetCellValue(startRow, 6, string.Empty);
                                                    }
                                                }

                                                if (detail.IsTarget)
                                                {
                                                    sumc += "C" + startRow + ",";
                                                    sumd += "D" + startRow + ",";
                                                    sume += "E" + startRow + ",";

                                                    sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, true, false);
                                                }
                                                else
                                                {
                                                    sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, false, false);
                                                }

                                                startRow++;
                                            }
                                        }
                                    }
                                }
                            }
                            // Thiet lap cong thuc tinh tong
                            List<string> listKey = dicPos.Keys.ToList();
                            foreach (string key in listKey)
                            {
                                int posSum = dicPos[key];
                                if (dicPosSum.ContainsKey(key))
                                {
                                    List<int> listPosSum = dicPosSum[key];
                                    if (listPosSum != null && listPosSum.Any())
                                    {
                                        string sum3 = string.Empty;
                                        string sum4 = string.Empty;
                                        foreach (int pos in listPosSum)
                                        {
                                            string colName = Utils.GetExcelColumnName(3);
                                            sum3 += ", " + colName + pos;
                                            colName = Utils.GetExcelColumnName(4);
                                            sum4 += ", " + colName + pos;
                                        }
                                        sheet.SetCellFormula(posSum, 3, "SUM(" + sum3.Substring(2) + ")");
                                        sheet.SetCellFormula(posSum, 4, "SUM(" + sum4.Substring(2) + ")");
                                    }
                                }
                            }

                            rangeData.CopyTo(sheet.GetRange(startRow, 1, startRow, 6));
                            sheet.SetCellValue(startRow, 2, "Tổng kinh phí");

                            sumc = sumc.TrimEnd(new char[] { ',' });
                            sumd = sumd.TrimEnd(new char[] { ',' });
                            sume = sume.TrimEnd(new char[] { ',' });
                            sumc = "=SUM(" + sumc + ")";
                            sumd = "=SUM(" + sumd + ")";
                            sume = "=SUM(" + sume + ")";

                            sheet.SetCellFormula(startRow, 3, sumc);
                            sheet.SetCellFormula(startRow, 4, sumd);
                            sheet.SetCellFormula(startRow, 5, sume);
                            string percent = "=D" + startRow + "/C" + startRow;
                            sheet.SetCellFormula(startRow, 6, percent);

                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, true, false);
                            startRow++;
                            sheet.GetRange(startRow, 2, startRow, 6).Range.Merge();
                            sheet.SetCellValue(startRow, 2, "(Số tiền Quyết toán trong Năm bằng chữ: " + Utils.ChuyenSo(totalDis.ToString()) + ")");
                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, false, false);
                            sheet.SetRangeBorderThin(7, 1, startRow, 6);
                            startRow++;
                            startRow++;
                            int origin = startRow;
                            // Lay thong tin giai ngan
                            List<Disbursement> listDisb = _context.Disbursement.Where(x => x.PlanID == model.PlanID).OrderBy(x => x.Number).ToList();
                            double totalDisb = 0;
                            string sumDisb = "C" + startRow;
                            foreach (Disbursement disb in listDisb)
                            {
                                rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                                totalDisb += disb.Money.GetValueOrDefault();
                                sheet.SetCellValue(startRow, 2, "Tạm ứng lần " + disb.Number + ":");
                                sheet.SetCellValue(startRow, 3, disb.Money.GetValueOrDefault().ToString());
                                startRow++;
                            }
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Tổng số tạm ứng:");
                            if (listDisb.Any())
                            {
                                sheet.SetCellFormula(startRow, 3, "SUM(" + sumDisb + ":C" + (startRow - 1) + ")");
                            }
                            else
                            {
                                sheet.SetCellValue(startRow, 3, "0");
                            }
                            startRow++;
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Tổng số giải ngân: ");
                            sheet.SetCellFormula(startRow, 3, "D" + (startRow - 4 - listDisb.Count));
                            startRow++;
                            rangeDisb.CopyTo(sheet.GetRange(startRow, 2, startRow, 3));
                            sheet.SetCellValue(startRow, 2, "Chênh lệch tạm ứng và giải ngân: ");
                            sheet.SetCellFormula(startRow, 3, "C" + (startRow - 2) + "-C" + (startRow - 1));

                            sheet.SetRangeFont(origin, 1, startRow, 6, "Times New Roman", 12, false, false);
                            sheet.SetRangeBorderThin(origin, 2, startRow, 4);

                            startRow = startRow + 2;
                            int origin2 = startRow;
                            string today = ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " +
                                           DateTime.Now.Year;
                            sheet.GetRange(startRow, 4, startRow, 6).Range.Merge();
                            sheet.SetCellValue(startRow, 4, today);
                            sheet.SetRangeFont(startRow, 1, startRow, 8, "Times New Roman", 12, false, true);

                            startRow++;
                            sheet.SetCellValue(startRow, 2, "Người lập báo cáo");
                            sheet.SetCellValue(startRow, 3, "Kế toán trưởng");
                            sheet.GetRange(startRow, 4, startRow, 6).Range.Merge();
                            sheet.SetCellValue(startRow, 4, "Thủ trưởng đơn vị");
                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, true, false);

                            startRow++;
                            sheet.GetRange(startRow, 4, startRow, 6).Range.Merge();
                            sheet.SetCellValue(startRow, 4, "(Ký tên, đóng dấu)");
                            sheet.SetRangeFont(startRow, 1, startRow, 6, "Times New Roman", 12, false, true);


                            sheet.GetRange(origin2, 1, startRow, 6).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;

                            sheet.SetSheetWrapText();

                            Stream streamToWrite = oBook.ToStream();
                            byte[] bytesToWrite = new byte[streamToWrite.Length];
                            streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                            string newFile = "Bao_Cao_Tai_Chinh_Nam " + model.Year + "_" + userInfo.UserName + ".xlsx";
                            return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                        }
                    }
                }
                throw new BusinessException("Không có dữ liệu");
            }
            throw new BusinessException("Dữ liệu không hợp lệ");
        }
        #endregion

        #region Xuất báo cáo quyết toán phụ lục 06B
        /// <summary>
        /// báo cáo tổng hợp quyết toán
        /// </summary>
        /// <param name="settleSynthesisId"></param>
        /// <returns></returns>
        //public FileResult ExportStatementReport(int? planId)
        public FileResult ExportStatementReport(int? settleSynthesisId)
        {
            if (settleSynthesisId != null)
            {
                var entity = (from s in _context.SettleSynthesis
                              join p in _context.Plan on s.PlanID equals p.PlanID
                              join d in _context.Department on p.DepartmentID equals d.DepartmentID
                              join pr in _context.Period on p.PeriodID equals pr.PeriodID
                              where s.SettleSynthesisID == settleSynthesisId && s.Status != Constants.PLAN_DEACTIVED
                              select new
                              {
                                  pr.PeriodID,
                                  s.SettleSynthesisID,
                                  s.Year,
                                  s.Quarter,
                                  s.PlanID,
                                  s.TotalSettleMoney,
                                  s.TotalPlanMoney,
                                  s.Status,
                                  pr.PeriodName,
                                  d.DepartmentName,
                                  s.SendDate
                              }).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }

                var plan = _context.Plan.Find(entity.PlanID);

                string fileName = "Bao_cao_quyet_toan.xlsx";
                string filePath = Server.MapPath("~/Template/") + fileName;
                byte[] bytesToWrite = new SettleSynthesisBusiness(_context).Export(settleSynthesisId.Value, entity.Year, filePath);
                string newFile = "Bao_cao_quyet_toan_" + Utils.ToQuarter(entity.Quarter) + "/" +
                                    entity.Year + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);

            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }
        #endregion

        #region Xuất báo cáo Kế hoạch hoạt động
        /// <summary>
        /// Báo cáo Kế hoạch hoạt động            1.2 KH - PHU LUC 02 - DON VI.xlsx
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public FileResult ExportPlanActionReport(int? planId)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (planId != null)
            {
                var plan = _context.Plan.Find(planId);
                var periode = _context.Period.Find(plan.PeriodID);

                string fileName = "Ke_Hoach_Hoat_Dong.xlsx";
                string filePath = Server.MapPath("~/Template/") + fileName;
                using (MemoryStream ms = new MemoryStream())
                {
                    using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        byte[] bytes = new byte[file.Length];
                        file.Read(bytes, 0, (int)file.Length);
                        ms.Write(bytes, 0, (int)file.Length);

                        IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                        IXVTWorksheet sheet = oBook.GetSheet(1);

                        int startYear = periode.FromDate.Year;
                        int endYear = periode.ToDate.Year;

                        string startTime = periode.FromDate.ToString("MM/yyyy");
                        string endTime = periode.ToDate.ToString("MM/yyyy");

                        var unit = _context.Department.Find(plan.DepartmentID);
                        string donVi = unit.DepartmentName;
                        string reportName = "KẾ HOẠCH HOẠT ĐỘNG GIAI ĐOẠN " + startYear + "-" + endYear;
                        string kemtheo = "(Kèm theo đề xuất hoạt động ngày     tháng      năm " + startYear + " của " +
                                         donVi + ")";
                        sheet.SetCellValue(1, 1, "Tên đơn vị: " + donVi);
                        sheet.SetCellValue(2, 1, "Thời gian thực hiện dự án: Tháng " + startTime + " -" + endTime);
                        sheet.SetCellValue(4, 1, reportName);
                        sheet.SetCellValue(5, 1, kemtheo);
                        sheet.SetCellValue(8, 3, "Năm " + startYear);
                        sheet.SetCellValue(8, 7, "Năm " + endYear);

                        int startRow = 10;

                        List<PlanTargetModel> lstPlanTarget = GetPlanTargetByPlan(planId.Value);
                        foreach (PlanTargetModel pt in lstPlanTarget)
                        {
                            if (startRow > 10)
                            {
                                sheet.CopyRow(10, startRow);
                            }
                            sheet.SetCellValue(startRow, 1, pt.TargetCode);
                            sheet.SetCellValue(startRow, 2, pt.PlanTargetContent);
                            sheet.SetAutoFitColumn(startRow, 2);
                            startRow++;

                            List<PlanTargetActionModel> lstPlanTargetAtion = pt.ListPlanTargetActionModel;
                            if (lstPlanTargetAtion.Count > 0)
                            {
                                foreach (PlanTargetActionModel planTargetAction in lstPlanTargetAtion)
                                {
                                    sheet.SetCellValue(startRow, 1, "'" + planTargetAction.ActionCode);
                                    sheet.SetCellValue(startRow, 2, planTargetAction.ActionContent);

                                    var lstPlanTime = planTargetAction.ListSubActionPlanTime;
                                    if (lstPlanTime.Count > 0)
                                    {
                                        int colIndex = 3;
                                        foreach (var objPlanTime in lstPlanTime)
                                        {
                                            sheet.SetCellValue(startRow, colIndex, objPlanTime.PlanTime);
                                            colIndex++;
                                        }
                                    }

                                    if (planTargetAction.ListSubActionPlan.Count == 0)
                                    {
                                        sheet.SetCellValue(startRow, 11, "'" + planTargetAction.Exucuter);
                                        sheet.SetCellValue(startRow, 12, planTargetAction.Supporter);
                                        sheet.SetCellValue(startRow, 13, planTargetAction.Note);
                                    }

                                    sheet.SetRangeFont(startRow, 1, startRow, 13, "Times New Roman", 12, false, false);
                                    startRow++;

                                    var lstSubActionPlan = planTargetAction.ListSubActionPlan;
                                    if (lstSubActionPlan.Count > 0)
                                    {
                                        foreach (var subActionPlan in lstSubActionPlan)
                                        {
                                            sheet.SetCellValue(startRow, 1, "'" + subActionPlan.SubActionCode);
                                            sheet.SetCellValue(startRow, 2, subActionPlan.SubActionContent);

                                            var lstSubPlanTime = subActionPlan.ListSubActionPlanTime;
                                            if (lstSubPlanTime.Count > 0)
                                            {
                                                int colIndex = 3;
                                                foreach (var objPlanTime in lstSubPlanTime)
                                                {
                                                    sheet.SetCellValue(startRow, colIndex, objPlanTime.PlanTime);
                                                    colIndex++;
                                                }
                                            }

                                            if (subActionPlan.ListChildSubActionPlan.Count == 0)
                                            {
                                                sheet.SetCellValue(startRow, 11, "'" + subActionPlan.Exucuter);
                                                sheet.SetCellValue(startRow, 12, subActionPlan.Supporter);
                                                sheet.SetCellValue(startRow, 13, subActionPlan.Note);
                                            }
                                            sheet.SetRangeFont(startRow, 1, startRow, 13, "Times New Roman", 12, false, false);
                                            startRow++;
                                        }
                                    }
                                }
                            }
                        }

                        sheet.SetRangeBorderThin(10, 1, startRow - 1, 13);
                        sheet.GetRange(10, 3, startRow + 2, 13).Range.Style.Alignment.Horizontal =
                            XLAlignmentHorizontalValues.Center;

                        startRow++;

                        sheet.GetRange(startRow + 1, 1, startRow + 1, 4).Range.Merge();
                        sheet.GetRange(startRow + 1, 6, startRow + 1, 13).Range.Merge();
                        sheet.GetRange(startRow + 2, 6, startRow + 2, 13).Range.Merge();
                        sheet.SetCellValue(startRow + 1, 1, "Người lập");
                        sheet.SetCellValue(startRow + 1, 6, "Thủ trưởng đơn vị");
                        sheet.SetCellValue(startRow + 2, 6, "(Ký tên, đóng dấu)");

                        sheet.SetRangeFont(startRow + 1, 1, startRow + 1, 13, "Times New Roman", 13, true, false);
                        sheet.SetRangeFont(startRow + 2, 1, startRow + 2, 13, "Times New Roman", 12, false, true);
                        sheet.GetRange(startRow + 1, 1, startRow + 2, 11).Range.Style.Alignment.Horizontal =
                            XLAlignmentHorizontalValues.Center;
                        sheet.SetSheetWrapText();

                        Stream streamToWrite = oBook.ToStream();
                        byte[] bytesToWrite = new byte[streamToWrite.Length];
                        streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                        string newFile = "Ke_Hoach_Hoat_Dong_" + startYear + endYear + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                        return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                    }
                }
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        public List<PlanTargetModel> GetPlanTargetByPlan(int planId)
        {
            List<PlanTargetModel> lstPlanTarget = _context.PlanTarget.Where(x => x.PlanID == planId)
                .OrderBy(x => x.TargetOrder)
                .ThenBy(x => x.PlanTargetTitle)
                .Select(x => new PlanTargetModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetID = x.TargetID,
                    PlanID = x.PlanID,
                    DepartmentID = x.DepartmentID,
                    TargetCode = x.TargetCode
                })
                .ToList();
            foreach (PlanTargetModel pt in lstPlanTarget)
            {
                List<PlanTargetActionModel> lst = GetPlanTargetAction(pt.PlanID, pt.PlanTargetID);


                foreach (PlanTargetActionModel pta in lst)
                {
                    List<SubActionPlanModel> lstSubAction = GetSubActionPlanByPlanTargetId(pta.PlanTargetActionID);
                    pta.ListSubActionPlan = lstSubAction;
                }
                pt.ListPlanTargetActionModel = lst;
            }

            return lstPlanTarget;
        }

        public List<PlanTargetActionModel> GetPlanTargetAction(int planId, int? targetId)
        {
            List<PlanTargetActionModel> lstPlanTargetAction =
                _context.PlanTargetAction.Where(x => x.PlanID == planId && x.PlanTargetID == targetId)
                    .OrderBy(x => x.ActionOrder)
                    .Select(x => new PlanTargetActionModel
                    {
                        PlanTargetActionID = x.PlanTargetActionID,
                        ActionCode = x.ActionCode,
                        ActionContent = x.ActionContent,
                        Exucuter = x.Exucuter,
                        Supporter = x.Supporter,
                        Note = x.Note

                    }).ToList();

            foreach (PlanTargetActionModel pta in lstPlanTargetAction)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = _context.SubActionPlanTime
                    .Where(x => x.PlanTargetActionID == pta.PlanTargetActionID && x.SubActionPlanID == null)
                    .Select(x => new SubActionPlanTimeModel
                    {
                        SubActionPlanTimeID = x.SubActionPlanTimeID,
                        PlanTargetAcionID = x.PlanTargetActionID,
                        PlanTime = x.PlanTime,
                        PlanYear = x.PlanYear,
                        Executer = x.Executer,
                        Supporter = x.Supporter,
                        Note = x.Note,
                        CreateUserID = x.CreateUserID,
                        ModifierID = x.ModifierID

                    })
                    .ToList();
                pta.ListSubActionPlanTime = lstSubActionPlanTime;

            }

            return lstPlanTargetAction;
        }
        // Lay thong tin SubAction
        public List<SubActionPlanModel> GetSubActionPlanByPlanTargetId(int PlanTargetActionID)
        {
            List<SubActionPlanModel> lstSubActionPlan = _context.SubActionPlan.Where(x => x.PlanTargetActionID == PlanTargetActionID && x.ParentID == null)
                .OrderBy(x => x.SubActionOrder)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = _context.SubActionPlanTime
                    .Where(x => x.SubActionPlanID == sal.SubActionPlanID)
                    .Select(x => new SubActionPlanTimeModel
                    {
                        SubActionPlanTimeID = x.SubActionPlanTimeID,
                        SubActionPlanID = x.SubActionPlanID,
                        PlanTime = x.PlanTime,
                        CreateUserID = x.CreateUserID,
                        ModifierID = x.ModifierID

                    })
                    .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
                sal.ListChildSubActionPlan = GetListChildSubActionPlanBySubActionId(sal.SubActionPlanID.Value);
            }

            return lstSubActionPlan;
        }

        public List<SubActionPlanModel> GetListChildSubActionPlanBySubActionId(int subActionPlanId)
        {
            List<SubActionPlanModel> lstSubActionPlan = _context.SubActionPlan.Where(x => x.ParentID == subActionPlanId)
                .OrderBy(x => x.SubActionOrder)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = _context.SubActionPlanTime
                    .Where(x => x.SubActionPlanID == sal.SubActionPlanID)
                    .Select(x => new SubActionPlanTimeModel
                    {
                        SubActionPlanTimeID = x.SubActionPlanTimeID,
                        SubActionPlanID = x.SubActionPlanID,
                        PlanTime = x.PlanTime,
                        CreateUserID = x.CreateUserID,
                        ModifierID = x.ModifierID,
                        //   Supporter = x.Supporter,
                        //  Note = x.Note
                    })
                    .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
            }
            return lstSubActionPlan;
        }
        #endregion

        #region Xuất báo cáo dự toán kinh phí
        /// <summary>
        /// Báo cáo dự toán kinh phí              1.3 DU TOAN - PHU LUC 03 - DON VI.xlsx
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public FileResult ExportPlanCostReport(int? planId)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (planId != null)
            {
                var plan = _context.Plan.Find(planId);
                var period = _context.Period.Find(plan.PeriodID);

                string fileName = "Du_Toan_Kinh_Phi.xlsx";
                string filePath = Server.MapPath("~/Template/") + fileName;
                using (MemoryStream ms = new MemoryStream())
                {
                    using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        byte[] bytes = new byte[file.Length];
                        file.Read(bytes, 0, (int)file.Length);
                        ms.Write(bytes, 0, (int)file.Length);

                        IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                        IXVTWorksheet sheet = oBook.GetSheet(1);

                        int startYear = period.FromDate.Year;
                        int endYear = period.ToDate.Year;

                        string startTime = period.FromDate.ToString("MM/yyyy");
                        string endTime = period.ToDate.ToString("MM/yyyy");

                        var unit = _context.Department.Find(plan.DepartmentID);
                        string donVi = unit.DepartmentName;
                        string reportName = "DỰ TOÁN KINH PHÍ CHI TIẾT GIAI ĐOẠN " + startYear + "-" + endYear;

                        sheet.SetCellValue(1, 1, "Tên đơn vị: " + donVi);
                        sheet.SetCellValue(2, 1, "Thời gian thực hiện dự án: Tháng " + startTime + " -" + endTime);
                        sheet.SetCellValue(3, 1, reportName);

                        sheet.SetCellValue(5, 3, "Năm " + startYear);
                        sheet.SetCellValue(5, 7, "Năm " + endYear);
                        sheet.SetCellValue(5, 11, "Tổng NS năm " + startYear + "+" + endYear);
                        Dictionary<string, List<int>> dicPosSum = new Dictionary<string, List<int>>();
                        Dictionary<string, int> dicPos = new Dictionary<string, int>();
                        Dictionary<string, int> dicCostType = new Dictionary<string, int>();

                        int startRow = 7;

                        PlanCostBusiness bus = new PlanCostBusiness(_context);
                        List<PlanTargetModel> lstPlanTarget = bus.GetListCost(plan.PlanID);

                        if (lstPlanTarget.Count > 0)
                        {
                            foreach (PlanTargetModel pt in lstPlanTarget)
                            {
                                if (startRow > 7)
                                {
                                    sheet.CopyRow(7, startRow);
                                }
                                dicPos["target_" + pt.PlanTargetID] = startRow;
                                dicPosSum["target_" + pt.PlanTargetID] = new List<int>();
                                dicCostType["target_" + pt.PlanTargetID] = Constants.SUM_COST_TYPE_TOTAL;

                                sheet.SetCellValue(startRow, 1, pt.TargetCode);
                                sheet.SetCellValue(startRow, 2, pt.PlanTargetContent);
                                List<SubActionPlanCostModel> lstSubActionPlanCost = pt.ListSubActionPlanCost;

                                this.SetDetailCostData(ref sheet, lstSubActionPlanCost, period, startRow);

                                startRow++;
                                List<PlanTargetActionModel> lstPlanTargetAtion = pt.ListPlanTargetActionModel;
                                if (lstPlanTargetAtion.Count > 0)
                                {
                                    foreach (PlanTargetActionModel ptam in lstPlanTargetAtion)
                                    {
                                        sheet.SetCellValue(startRow, 1, "'" + ptam.ActionCode);
                                        sheet.SetCellValue(startRow, 2, ptam.ActionContent);
                                        sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11,
                                            true, false);
                                        dicPos["targetaction_" + ptam.PlanTargetActionID] = startRow;
                                        dicPosSum["target_" + pt.PlanTargetID].Add(startRow);
                                        dicPosSum["targetaction_" + ptam.PlanTargetActionID] = new List<int>();
                                        dicCostType["targetaction_" + ptam.PlanTargetActionID] = ptam.SumCostType.HasValue ? ptam.SumCostType.Value : Constants.SUM_COST_TYPE_TOTAL;

                                        List<SubActionPlanCostModel> lstSubActionPlanCostC1 = ptam.ListSubActionPlanCost;
                                        this.SetDetailCostData(ref sheet, lstSubActionPlanCostC1, period, startRow);
                                        startRow++;

                                        if (ptam.ListSubActionPlan != null && ptam.ListSubActionPlan.Any())
                                        {
                                            foreach (SubActionPlanModel item in ptam.ListSubActionPlan)
                                            {
                                                SetListSubActionPlan(ref sheet, item, ref startRow, period, dicPos, dicPosSum, dicCostType);
                                            }
                                        }
                                        else if (ptam.ListSubActionSpendItem != null && ptam.ListSubActionSpendItem.Any())
                                        {
                                            foreach (SubActionSpendItemModel item in ptam.ListSubActionSpendItem)
                                            {
                                                SetListSubActionSpendItem(ref sheet, item, ref startRow, period, dicPos, dicPosSum, dicCostType);
                                            }
                                        }
                                    }
                                }
                            }

                            // Thiet lap cong thuc tinh tong
                            List<string> listKey = dicPos.Keys.ToList();
                            int rangeYear = endYear - startYear;
                            foreach (string key in listKey)
                            {
                                if (dicPosSum.ContainsKey(key))
                                {
                                    List<int> listPosSum = dicPosSum[key];
                                    if (listPosSum != null && listPosSum.Any())
                                    {
                                        int posSum = dicPos[key];
                                        for (int i = 0; i <= rangeYear; i++)
                                        {
                                            int colSum;
                                            bool isSumPrice = false;
                                            int costType = dicCostType[key];
                                            if (key.StartsWith("subcost"))
                                            {
                                                if (costType < 0)
                                                {
                                                    isSumPrice = (listPosSum.Count == 1);
                                                }
                                                else
                                                {
                                                    isSumPrice = (costType == Constants.SUM_COST_TYPE_PRICE);
                                                }
                                            }
                                            else
                                            {
                                                isSumPrice = (costType == Constants.SUM_COST_TYPE_PRICE);
                                            }
                                            if (isSumPrice)
                                            {
                                                colSum = 5 + (i * 4);
                                                if (listPosSum.Count == 1)
                                                {
                                                    sheet.SetCellFormula(posSum, colSum, Utils.GetExcelColumnName(6 + (i * 4)) + listPosSum[0]);
                                                }
                                                else
                                                {
                                                    string sum = string.Empty;
                                                    foreach (int pos in listPosSum)
                                                    {
                                                        sum += ", " + Utils.GetExcelColumnName(6 + (i * 4)) + pos;
                                                    }
                                                    sheet.SetCellFormula(posSum, 5 + (i * 4), "SUM(" + sum.Substring(2) + ")");
                                                }
                                            }
                                            else
                                            {
                                                string sum = string.Empty;
                                                colSum = 6 + (i * 4);
                                                foreach (int pos in listPosSum)
                                                {
                                                    sum += ", " + Utils.GetExcelColumnName(colSum) + pos;
                                                }
                                                sheet.SetCellFormula(posSum, colSum, "SUM(" + sum.Substring(2) + ")");
                                            }

                                            sheet.SetCellFormat(posSum, colSum, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                                        }
                                    }
                                }
                            }
                            List<string> listPosTarget = listKey.Where(x => x.StartsWith("target_")).ToList();
                            List<string> listSumCol = new List<string>();
                            for (int i = 0; i <= rangeYear; i++)
                            {
                                int colSum = 6 + (i * 4);
                                string sum = string.Empty;
                                string colSumName = Utils.GetExcelColumnName(colSum);
                                foreach (string targetPos in listPosTarget)
                                {
                                    sum += ", " + colSumName + dicPos[targetPos];
                                }
                                listSumCol.Add(colSumName + startRow);
                                sheet.SetCellFormula(startRow, colSum, "SUM(" + sum.Substring(2) + ")");
                                sheet.SetCellFormat(startRow, colSum, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            }
                            int lastCol = 3 + (rangeYear + 1) * 4;
                            sheet.SetCellFormula(startRow, lastCol, "SUM(" + string.Join(",", listSumCol) + ")");
                            string val = sheet.GetValueOfCell(startRow, lastCol).ToString();
                            string bangchu = "";
                            sheet.SetCellValue(startRow, 2, "Cộng");
                            sheet.SetCellFormat(startRow, 6, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            sheet.SetCellFormat(startRow, 10, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                            sheet.SetCellFormat(startRow, 11, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");

                            sheet.GetRange(startRow, 1, startRow, 2).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;

                            if (!string.IsNullOrEmpty(val))
                            {
                                bangchu = Utils.ChuyenSo(val);
                                bangchu = "Bằng chữ: " + bangchu;
                            }

                            sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11, true, false);
                            startRow++;

                            sheet.GetRange(startRow, 1, startRow, 10).Range.Merge();
                            sheet.SetCellValue(startRow, 1, bangchu);
                            sheet.SetRangeFont(startRow, 1, startRow, 11, "Times New Roman", 11, true, false);

                            sheet.SetRangeBorderThin(7, 1, startRow, 11);

                            sheet.GetRange(startRow + 2, 2, startRow + 2, 4).Range.Merge();
                            sheet.GetRange(startRow + 2, 6, startRow + 2, 10).Range.Merge();
                            sheet.GetRange(startRow + 3, 6, startRow + 3, 10).Range.Merge();
                            sheet.SetCellValue(startRow + 2, 2, "Người lập");
                            sheet.SetCellValue(startRow + 2, 6, "Thủ trưởng đơn vị");
                            sheet.SetCellValue(startRow + 3, 6, "(Ký tên, đóng dấu)");

                            sheet.SetRangeFont(startRow + 2, 1, startRow + 2, 11, "Times New Roman", 11, true, false);
                            sheet.SetRangeFont(startRow + 3, 1, startRow + 3, 11, "Times New Roman", 11, false, true);
                            sheet.GetRange(startRow + 2, 1, startRow + 3, 11).Range.Style.Alignment.Horizontal =
                                XLAlignmentHorizontalValues.Center;
                        }
                        sheet.SetSheetWrapText();

                        Stream streamToWrite = oBook.ToStream();
                        byte[] bytesToWrite = new byte[streamToWrite.Length];
                        streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                        string newFile = "Du_Toan_Kinh_Phi_" + startYear + endYear + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                        return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                    }
                }
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        private void SetDetailCostData(ref IXVTWorksheet sheet, List<SubActionPlanCostModel> listCost, Period period, int startRow)
        {
            if (listCost != null && listCost.Any())
            {
                int fromYear = period.FromDate.Year;
                int toYear = period.ToDate.Year;
                int rangeYear = toYear - fromYear;
                int lastCol = 3 + (rangeYear + 1) * 4;
                List<string> listSumCol = new List<string>();
                for (int i = 0; i <= rangeYear; i++)
                {
                    int year = fromYear + i;
                    int startCol = 3 + (i * 4);
                    SubActionPlanCostModel cost = listCost.Where(x => x.PlanYear == year).FirstOrDefault();
                    if (cost != null)
                    {
                        sheet.SetCellValue(startRow, startCol++, cost.Price == null || cost.Quantity == null ? string.Empty : cost.Unit);
                        sheet.SetCellValue(startRow, startCol++, cost.Price == null || cost.Quantity == null ? string.Empty : cost.Quantity.ToString());
                        sheet.SetCellValue(startRow, startCol++, cost.Price == null || cost.Quantity == null ? string.Empty : cost.Price.ToString());
                        sheet.SetCellFormula(startRow, startCol, Utils.GetExcelColumnName(startCol - 1) + startRow + "*" + Utils.GetExcelColumnName(startCol - 2) + startRow);

                        sheet.SetCellFormat(startRow, startCol, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        sheet.SetCellFormat(startRow, startCol - 1, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
                        sheet.SetRangeFont(startRow, 1, startRow, lastCol, "Times New Roman", 11,
                            true, false);
                        listSumCol.Add(Utils.GetExcelColumnName(6 + (i * 4)) + startRow);
                    }
                }
                sheet.SetCellFormula(startRow, lastCol, "SUM(" + string.Join(",", listSumCol) + ")");
                sheet.SetCellFormat(startRow, lastCol, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
            }
        }

        public void SetListSubActionPlan(ref IXVTWorksheet sheet, SubActionPlanModel subActionPlan, ref int startRow, Period period,
            Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum, Dictionary<string, int> dicCostType)
        {
            dicPos["subaction_" + subActionPlan.SubActionPlanID] = startRow;
            dicCostType["subaction_" + subActionPlan.SubActionPlanID] = subActionPlan.SumCostType != null ? subActionPlan.SumCostType.Value : Constants.SUM_COST_TYPE_TOTAL;
            if (subActionPlan.ParentID == null)
            {
                dicPosSum["targetaction_" + subActionPlan.PlanTargetActionID].Add(startRow);
            }
            else
            {
                dicPosSum["subaction_" + subActionPlan.ParentID.Value].Add(startRow);
            }
            if (!dicPosSum.ContainsKey("subaction_" + subActionPlan.SubActionPlanID))
            {
                dicPosSum["subaction_" + subActionPlan.SubActionPlanID] = new List<int>();
            }
            sheet.SetCellValue(startRow, 1, "'" + subActionPlan.SubActionCode);
            sheet.SetCellValue(startRow, 2, subActionPlan.SubActionContent);
            string sumactionk = "=SUM(F" + startRow + ",J" + startRow + ")";
            sheet.SetCellFormula(startRow, 11, sumactionk);
            sheet.SetCellFormat(startRow, 11, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
            List<SubActionPlanCostModel> lstSubActionPlanCost = subActionPlan.ListSubActionPlanCost;
            this.SetDetailCostData(ref sheet, lstSubActionPlanCost, period, startRow);
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            int rangeYear = toYear - fromYear;
            int lastCol = 3 + (rangeYear + 1) * 4;
            if (subActionPlan.ParentID == null)
            {
                sheet.SetRangeFont(startRow, 1, startRow, lastCol, "Times New Roman", 11,
                                                false, false);
            }
            else
            {
                sheet.SetRangeFont(startRow, 1, startRow, lastCol, "Times New Roman", 11,
                                                                false, true);
            }
            startRow++;

            if (subActionPlan.ListChildSubActionPlan != null)
            {
                foreach (SubActionPlanModel subPlan in subActionPlan.ListChildSubActionPlan)
                {
                    SetListSubActionPlan(ref sheet, subPlan, ref startRow, period, dicPos, dicPosSum, dicCostType);
                }
            }
            if (subActionPlan.ListSubActionSpendItem != null)
            {
                foreach (SubActionSpendItemModel subPlan in subActionPlan.ListSubActionSpendItem)
                {
                    SetListSubActionSpendItem(ref sheet, subPlan, ref startRow, period, dicPos, dicPosSum, dicCostType);
                }
            }
        }

        public void SetListSubActionSpendItem(ref IXVTWorksheet sheet, SubActionSpendItemModel subActionPlan, ref int startRow, Period period,
             Dictionary<string, int> dicPos, Dictionary<string, List<int>> dicPosSum, Dictionary<string, int> dicCostType)
        {
            // Thiet lap vi tri
            dicPos["subcost_" + subActionPlan.SubActionSpendItemID] = startRow;
            dicCostType["subcost_" + subActionPlan.SubActionSpendItemID] = subActionPlan.SumCostType != null ? subActionPlan.SumCostType.Value : -1;
            if (subActionPlan.ParentID != null)
            {
                if (dicPosSum.ContainsKey("subcost_" + subActionPlan.ParentID.Value))
                {
                    dicPosSum["subcost_" + subActionPlan.ParentID.Value].Add(startRow);
                }
                else
                {
                    dicPosSum["subcost_" + subActionPlan.ParentID.Value] = new List<int> { startRow };
                }
            }
            else
            {
                if (subActionPlan.SubActionPlanID != null)
                {
                    dicPosSum["subaction_" + subActionPlan.SubActionPlanID.Value].Add(startRow);
                }
                else
                {
                    dicPosSum["targetaction_" + subActionPlan.PlanTargetActionID.Value].Add(startRow);
                }
            }
            sheet.SetCellValue(startRow, 1, "'" + subActionPlan.SpendItemTitle);
            sheet.SetCellValue(startRow, 2, subActionPlan.SpendItemName);
            string sumactionk = "=SUM(F" + startRow + ",J" + startRow + ")";
            sheet.SetCellFormula(startRow, 11, sumactionk);
            sheet.SetCellFormat(startRow, 11, "_(* #,##0_);_(* (#,##0);_(* \"-\"??_);_(@_)");
            List<SubActionPlanCostModel> lstSubActionPlanCost = subActionPlan.ListSubActionPlanCost;
            this.SetDetailCostData(ref sheet, lstSubActionPlanCost, period, startRow);
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            int rangeYear = toYear - fromYear;
            int lastCol = 3 + (rangeYear + 1) * 4;
            bool bold = subActionPlan.IsParent;
            bool italic = subActionPlan.ParentID != null && bold;
            sheet.SetRangeFont(startRow, 1, startRow, lastCol, "Times New Roman", 11,
                                                bold, italic);
            startRow++;

            if (subActionPlan.ListChild != null)
            {
                foreach (SubActionSpendItemModel subPlan in subActionPlan.ListChild)
                {
                    SetListSubActionSpendItem(ref sheet, subPlan, ref startRow, period, dicPos, dicPosSum, dicCostType);
                }
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}