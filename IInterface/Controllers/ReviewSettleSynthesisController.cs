﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReviewSettleSynthesisController : Controller
    {
        // GET: Action
        private Entities context = new Entities();
        public ActionResult Index()
        {
            var listPlan = (from pr in this.context.Period
                            where pr.Status == Constants.IS_ACTIVE
                            orderby pr.FromDate descending
                            select new
                            {
                                pr.PeriodID,
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listPlanItem = new List<SelectListItem>();
            listPlanItem.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            listPlan.ForEach(x =>
            {
                listPlanItem.Add(new SelectListItem { Value = x.PeriodID.ToString(), Text = x.PeriodName });
            });
            ViewData["listPlan"] = listPlanItem;
            List<SelectListItem> listYear = new List<SelectListItem>();
            listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i < maxYear; i++)
                {
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                }
            }
            ViewData["listYear"] = listYear;
            // Lay danh sach don vi quan ly
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<SelectListItem> listDept = (from m in this.context.UserManageDepartment
                                             join d in this.context.Department on m.DepartmentID equals d.DepartmentID
                                             where m.UserID == userID && d.Status == Constants.IS_ACTIVE
                                             orderby d.DepartmentName
                                             select new SelectListItem
                                             {
                                                 Value = d.DepartmentID + "",
                                                 Text = d.DepartmentName
                                             }).ToList();
            listDept.Insert(0, new SelectListItem { Value = "0", Text = "---Tất cả---" });
            ViewData["listDept"] = listDept;
            return View();
        }

        public ActionResult AddOrEdit(long? id)
        {
            SettleInfoModel model = null;
            if (id != null)
            {
                var entity = (from s in this.context.SettleSynthesis
                              join p in this.context.Plan on s.PlanID equals p.PlanID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              where s.SettleSynthesisID == id && s.Status == Constants.PLAN_NEXT_LEVER
                              select new
                              {
                                  s.SettleSynthesisID,
                                  s.Year,
                                  s.Quarter,
                                  s.PlanID,
                                  s.TotalSettleMoney,
                                  s.TotalPlanMoney,
                                  s.Status,
                                  pr.PeriodName,
                                  d.DepartmentName,
                                  s.SendDate
                              }).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                model = new SettleInfoModel();
                model.SettleSynthesisID = entity.SettleSynthesisID;
                model.Year = entity.Year;
                model.Quarter = entity.Quarter;
                model.PeriodID = entity.PlanID;
                model.PlanName = entity.PeriodName;
                model.TotalSettleMoney = entity.TotalSettleMoney;
                model.TotalPlanMoney = entity.TotalPlanMoney;
                model.Status = entity.Status;
                model.DeptName = entity.DepartmentName;
                model.SendDate = entity.SendDate;
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            UserInfo userInfo = SessionManager.GetUserInfo();

            if (id != null)
            {
                List<TargetModel> listTarget = (from pt in this.context.PlanTarget
                                                join t in this.context.Target on pt.TargetID equals t.TargetID
                                                where pt.PlanID == model.PeriodID && t.Status == Constants.IS_ACTIVE && pt.Status == Constants.IS_ACTIVE
                                                orderby t.TargetOrder
                                                select new TargetModel
                                                {
                                                    TargetID = t.TargetID,
                                                    TargetTitle = t.TargetTitle,
                                                    TargetContent = t.TargetContent
                                                }).ToList();
                ViewData["listTarget"] = listTarget;
            }
            return View("AddOrEdit", model);
        }

        public PartialViewResult LoadExpenseInfo(int synID, int targetID, int isView = 0)
        {
            SettleSynthesis entity = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo quyết toán, vui lòng kiểm tra lại");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.TargetID == targetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            PlanTargetModel planTarget = bus.GetListCost(0, targetID, synID);
            ViewData["planTarget"] = planTarget;
            SettleSynthesisCost detail = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID
                    && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.SettleSynthesisCostID && c.Type == Constants.COMMENT_TYPE_SETTLE
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.SettleSynthesisCostID;
            }
            if (isView == 0)
            {
                return PartialView("_ExpenseInfo");
            }
            else
            {
                return PartialView("_ExpenseInfoView");
            }
        }

        public PartialViewResult Search(int PlanID, int Year, int DeptID, int page = 1)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<int> listDeptID = this.context.UserManageDepartment.Where(x => x.UserID == userID)
                .Select(x => x.DepartmentID).ToList();
            IQueryable<SettleInfoModel> IQSearch =
                from s in this.context.SettleSynthesis
                join pl in this.context.Plan on s.PlanID equals pl.PlanID
                join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                where (userInfo.IsHeadOffice == Constants.IS_ACTIVE || listDeptID.Contains(pl.DepartmentID)) && pl.Status == Constants.PLAN_APPROVED
                && s.Status >= Constants.PLAN_NEXT_LEVER
                select new SettleInfoModel
                {
                    CreateDate = s.CreateDate,
                    Creator = s.Creator,
                    PeriodID = s.PlanID,
                    PlanName = pr.PeriodName,
                    DeptName = d.DepartmentName,
                    DeptID = d.DepartmentID,
                    Quarter = s.Quarter,
                    Year = s.Year,
                    SettleSynthesisID = s.SettleSynthesisID,
                    TotalPlanMoney = s.TotalPlanMoney,
                    TotalReceivedMoney = s.TotalReceivedMoney,
                    TotalSettleMoney = s.TotalSettleMoney,
                    TottalDisbMoney = s.TottalDisbMoney,
                    Type = s.Type,
                    Status = s.Status,
                    SendDate = s.SendDate
                };
            if (PlanID > 0)
            {
                IQSearch = IQSearch.Where(x => x.PeriodID == PlanID);
            }
            if (Year > 0)
            {
                IQSearch = IQSearch.Where(x => x.Year == Year);
            }
            if (DeptID > 0)
            {
                IQSearch = IQSearch.Where(x => x.DeptID == DeptID);
            }

            Paginate<SettleInfoModel> paginate = new Paginate<SettleInfoModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = IQSearch.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter)
                .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = IQSearch.Count();
            ViewData["listResult"] = paginate;
            return PartialView("_ListResult");
        }

        public PartialViewResult LoadCostByAction(int planTargetActionID, int targetID, int synID, int isView = 0)
        {
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            PlanTargetModel planTarget = bus.GetListCost(planTargetActionID, targetID, synID);
            ViewData["planTarget"] = planTarget;
            if (isView == 0)
            {
                return PartialView("_ListExpense");
            }
            else
            {
                return PartialView("_ListExpenseView");
            }
        }

        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }

        public JsonResult LoadYear(int planID)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            var listPlan = (from pl in this.context.Plan
                            join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                            where pl.DepartmentID == deptID && pl.Status == Constants.PLAN_APPROVED && (planID == 0 || pl.PlanID == planID)
                            orderby pr.FromDate descending
                            select new
                            {
                                pl.PlanID,
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listYear = new List<SelectListItem>();
            listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i < maxYear; i++)
                {
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                }
            }
            ViewData["listYear"] = listYear;
            return Json(listYear);
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, long synID)
        {
            SettleSynthesis entitySettle = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entitySettle == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanID == entitySettle.PlanID && x.PlanTargetID == planTargetID
            && x.MainActionID != null && x.ActionContent != null && x.ActionContent.Trim() != "")
                .Select(x => new PlanTargetActionModel
                {
                    PlanTargetID = x.PlanTargetActionID,
                    PlanTargetActionID = x.PlanTargetActionID,
                    PlanID = x.PlanID.Value,
                    MainActionID = x.MainActionID.Value,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }


        public JsonResult Approve(int synID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(synID, Constants.REPORT_TYPE_SETTLE, Constants.IS_ACTIVE);
            return Json("Phê duyệt báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Agree(int synID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(synID, Constants.REPORT_TYPE_SETTLE, Constants.IS_ACTIVE);
            return Json("Gửi báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int synID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(synID, Constants.REPORT_TYPE_SETTLE, Constants.IS_NOT_ACTIVE);
            return Json("Mở khóa báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnApprove(int synID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(synID, Constants.REPORT_TYPE_SETTLE, Constants.IS_NOT_ACTIVE);
            return Json("Hủy phê duyệt báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(SettleCostData data, int PlanTargetID)
        {
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            bus.Save(data, PlanTargetID);
            return Json("Cập nhật thông tin quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}