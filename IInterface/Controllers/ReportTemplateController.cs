﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IInterface.Filter;
using Spire.Doc;
using Spire.Xls;

namespace IInterface.Controllers
{
    public class ReportTemplateController : Controller
    {
        private readonly Entities _context = new Entities();
        // GET: ReportTemplate
        [BreadCrumb(ControllerName = "Quản lý mẫu báo cáo", ActionName = "Danh sách báo cáo", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AddOrEditReportTemplate(ReportTemplateModel model)
        {
            var upload = model.ReportTemplateAvatar;
            string msg = string.Empty;
            if (upload != null && upload.ContentLength > 0)
            {
                string fileName = model.FileName;
                string fileExt = Path.GetExtension(fileName);
                string fileType = "doc,docx,xls,xlsx,";
                if (fileExt != null && fileType.Contains(fileExt + ","))
                {
                    throw new BusinessException("Hệ thống chỉ cho phép mẫu báo cáo file word hoặc file excel");
                }
                if (fileExt.Contains("doc"))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        Document document = new Document(Request.Files[0].InputStream);
                        document.SaveToStream(m, Spire.Doc.FileFormat.PDF);
                        byte[] bytes = m.ToArray();
                        model.Template = bytes;
                    }
                }
                else
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        Workbook workbook = new Workbook();
                        workbook.LoadFromStream(Request.Files[0].InputStream);
                        workbook.SaveToStream(m, Spire.Xls.FileFormat.PDF);
                        byte[] bytes = m.ToArray();
                        model.Template = bytes;
                    }
                }

            }
            ReportTemplate entity = null;
            if (model.ReportTemplateID > 0)
            {
                entity = _context.ReportTemplate.FirstOrDefault(x => x.Status != Constants.IS_NOT_ACTIVE && x.ReportTemplateID == model.ReportTemplateID);
                if (entity == null)
                {
                    throw new BusinessException("Mẫu báo cáo không tồn tại trong hệ thống");
                }
                model.UpdateEntity(entity);
                msg = "Cập nhật mẫu báo cáo thành công";
            }
            else
            {
                entity = model.ToEntity();
                _context.ReportTemplate.Add(entity);
                msg = "Thêm mới mẫu báo cáo thành công";
            }
            _context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult OpenAddOrEditReportTemplate(int? ReportTemplateId)
        {
            ReportTemplateModel model = null;
            if (ReportTemplateId.HasValue && ReportTemplateId.Value > 0)
            {
                ReportTemplate entity = _context.ReportTemplate.FirstOrDefault(x => x.ReportTemplateID == ReportTemplateId && x.Status != Constants.IS_NOT_ACTIVE);
                if (entity == null)
                {
                    throw new BusinessException("mẫu báo cáo không tồn tại trong hệ thống");
                }
                model = new ReportTemplateModel();
                model.ReportTemplateID = entity.ReportTemplateID;
                model.ReportName = entity.ReportName;
                model.Description = entity.Description;
            }
            return PartialView("_AddOrEdit", model);
        }
        public JsonResult DeleteReportTemplate(int id)
        {
            ReportTemplate entity = _context.ReportTemplate.Where(x => x.ReportTemplateID == id).FirstOrDefault();
            if (entity != null)
            {
                entity.Status = Constants.IS_NOT_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Xóa mẫu báo cáo thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchReportTemplate(string ReportName = null, int page = 1)
        {
            if (string.IsNullOrWhiteSpace(ReportName))
            {
                ReportName = null;
            }
            else
            {
                ReportName = ReportName.ToLower();
            }
            IQueryable<ReportTemplateModel> iQReportTemplate = from x in _context.ReportTemplate
                                                               where x.Status != Constants.IS_NOT_ACTIVE
                                                               && (ReportName == null || x.ReportName.ToLower().Contains(ReportName))
                                                               orderby x.ReportName
                                                               select new ReportTemplateModel
                                                               {
                                                                   ReportTemplateID = x.ReportTemplateID,
                                                                   ReportName = x.ReportName,
                                                                   CreateDate = x.CreateDate,
                                                                   Status = x.Status,
                                                                   Description = x.Description,
                                                                   FileName = x.FileName
                                                               };
            Paginate<ReportTemplateModel> paginate = new Paginate<ReportTemplateModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.Count = iQReportTemplate.Count();
            List<ReportTemplateModel> listReportTemplate = iQReportTemplate.Skip((page - 1) * paginate.PageSize).Take(paginate.PageSize).ToList();
            paginate.List = listReportTemplate;
            ViewData["listReportTemplate"] = paginate;
            return PartialView("_Lst");
        }

        public FileResult DownloadTemplate(int id)
        {
            ReportTemplate att = this._context.ReportTemplate.Find(id);
            if (att == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            return File(att.Template, System.Net.Mime.MediaTypeNames.Application.Pdf);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}