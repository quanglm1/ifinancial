﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class SubActionPlanTimeController : Controller
    {
        private Entities Context = new Entities();
        private ObjectRoleBO GetRole(Plan plan)
        {
            var user = SessionManager.GetUserInfo();
            ObjectRoleBO role = null;
            ChangeRequest cr = plan.Status == Constants.PLAN_APPROVED ?
                Context.ChangeRequest.Where(x => x.PlanID == plan.PlanID && x.IsFinish == null).OrderByDescending(x => x.ChangeRequestID).FirstOrDefault() : null;
            if (cr != null)
            {
                role = new FlowProcessBusiness(Context).GetRoleForPlanChange(user.UserID, plan.PlanID);
            }
            else
            {
                role = new FlowProcessBusiness(Context).GetRoleForObject(user.UserID, plan.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            }
            return role;
        }
        // GET: SubPlanActionTime
        int userID = SessionManager.GetUserInfo().UserID;
        DateTime now = DateTime.Now;
        [BreadCrumb(ControllerName = "Thời gian thực hiện", ActionName = "Thời gian thực hiện", AreaName = "Kế hoạch")]
        public ActionResult Index(int planID)
        {
            // Plan plan = Context.Plan.Find(PlanID);
            Plan plan = Context.Plan.Where(x => x.PlanID == planID).FirstOrDefault();
            if (plan != null)
            {
                Period period = Context.Period.Find(plan.PeriodID);
                int startYear = period.FromDate.Year;
                int finishYear = period.ToDate.Year;
                int rangeYear = finishYear - startYear;
                ViewData["Dept"] = Context.Department.Find(plan.DepartmentID);
                ViewData["Plan"] = plan;
                ViewData["Period"] = period;
                //     ViewData["StartYear"] = startYear;
                ViewData["RangeYear"] = rangeYear;

                List<PlanTargetModel> lstPlanTarget = GetPlanTargetByPlan(plan.PlanID);
                ViewData["PlanTarget"] = lstPlanTarget;
                List<ChangeRequestActionModel> listChangeAction;
                if (plan.IsChange.GetValueOrDefault() == 0)
                {
                    listChangeAction = new PlanBusiness(Context).GetPlanUnlockAction(plan.PlanID);
                }
                else
                {
                    listChangeAction = new PlanBusiness(Context).GetRequestAction(plan.PlanID);
                }
                if (listChangeAction != null)
                {
                    ViewData["listChangeAction"] = listChangeAction;
                }
                // Role
                ObjectRoleBO role = GetRole(plan);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home");
                }
                ViewData["role"] = role;

            }
            else
            {
                ViewBag.ErrorContent = "Kế hoạch chưa được lập, vui lòng tạo kế hoạch với thông tin chung trước";
                return RedirectToAction("RoleErrorBusiness", "Home");
            }


            return View();
        }

        public List<PlanTargetModel> GetPlanTargetByPlan(int planId)
        {
            List<PlanTargetModel> lstPlanTarget = Context.PlanTarget.Where(x => x.PlanID == planId)
                .OrderBy(x => x.TargetOrder)
                .ThenBy(x => x.PlanTargetTitle)
                .Select(x => new PlanTargetModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetID = x.TargetID,
                    PlanID = x.PlanID,
                    TargetCode = x.TargetCode,
                    DepartmentID = x.DepartmentID
                })
                .ToList();
            foreach (PlanTargetModel pt in lstPlanTarget)
            {
                List<PlanTargetActionModel> lst = GetPlanTargetAction(pt.PlanID, pt.PlanTargetID);


                foreach (PlanTargetActionModel pta in lst)
                {
                    List<SubActionPlanModel> lstSubAction = GetSubActionPlanByPlanTargetId(pta.PlanTargetActionID);
                    pta.ListSubActionPlan = lstSubAction;
                }
                pt.ListPlanTargetActionModel = lst;
            }

            return lstPlanTarget;
        }

        public List<PlanTargetActionModel> GetPlanTargetAction(int planId, int? targetId)
        {
            List<PlanTargetActionModel> lstPlanTargetAction =
                Context.PlanTargetAction.Where(x => x.PlanID == planId && x.PlanTargetID == targetId)
                .OrderBy(x => x.ActionOrder)
                .Select(x => new PlanTargetActionModel
                {
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note

                }).ToList();

            foreach (PlanTargetActionModel pta in lstPlanTargetAction)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = Context.SubActionPlanTime
                           .Where(x => x.PlanTargetActionID == pta.PlanTargetActionID && x.SubActionPlanID == null)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               PlanTargetAcionID = x.PlanTargetActionID,
                               PlanTime = x.PlanTime,
                               PlanYear = x.PlanYear,
                               Executer = x.Executer,
                               Supporter = x.Supporter,
                               Note = x.Note,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID

                           }).ToList();
                pta.ListSubActionPlanTime = lstSubActionPlanTime;

            }

            return lstPlanTargetAction;
        }
        // Lay thong tin SubAction
        public List<SubActionPlanModel> GetSubActionPlanByPlanTargetId(int PlanTargetActionID)
        {
            List<SubActionPlanModel> lstSubActionPlan = Context.SubActionPlan.Where(x => x.PlanTargetActionID == PlanTargetActionID && x.ParentID == null)
                .OrderBy(x => x.SubActionOrder)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = Context.SubActionPlanTime
                           .Where(x => x.SubActionPlanID == sal.SubActionPlanID)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               SubActionPlanID = x.SubActionPlanID,
                               PlanTime = x.PlanTime,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID

                           })
                           .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
                sal.ListChildSubActionPlan = GetListChildSubActionPlanBySubActionId(sal.SubActionPlanID.Value);
            }

            return lstSubActionPlan;
        }

        public List<SubActionPlanModel> GetListChildSubActionPlanBySubActionId(int subActionPlanId)
        {
            List<SubActionPlanModel> lstSubActionPlan = Context.SubActionPlan.Where(x => x.ParentID == subActionPlanId)
                .OrderBy(x => x.SubActionOrder)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = Context.SubActionPlanTime
                           .Where(x => x.SubActionPlanID == sal.SubActionPlanID)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               SubActionPlanID = x.SubActionPlanID,
                               PlanTime = x.PlanTime,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID
                           })
                           .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
            }
            return lstSubActionPlan;
        }

        private List<SubActionPlanTime> GetListSubActionPlanTime(int? planTargetActionId, int? subActionPlanId, int startYear, int finishYear)
        {

            List<SubActionPlanTime> lstSubActionPlanTime = Context.SubActionPlanTime
                             .Where(x => ((x.PlanTargetActionID == planTargetActionId) || (x.SubActionPlanID == subActionPlanId)) && (x.PlanYear == startYear || x.PlanYear == finishYear))
                             .ToList();

            return lstSubActionPlanTime;
        }

        public JsonResult EditSubActionPlanTime(List<string> actionPlanTime, int planID)
        {
            // Xoa het thong tin cua viec lap ke hoach cu
            Plan plan = Context.Plan.Find(planID);
            if (plan != null)
            {
                // Role
                ObjectRoleBO role = GetRole(plan);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    throw new BusinessException("Bạn không có quyền thao tác trên kế hoạch này");
                }
                if (role.Role <= Constants.ROLE_VIEW)
                {
                    throw new BusinessException("Bạn chỉ có quyền xem kế hoạch này");
                }
                List<PlanTargetModel> lstPlanTarget = GetPlanTargetByPlan(plan.PlanID);
                List<ChangeRequestAction> listChangeAction = null;
                if (plan != null && plan.IsChange.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    ChangeRequest cr = Context.ChangeRequest.Where(x => x.PlanID == plan.PlanID && x.Status == Constants.PLAN_APPROVED && x.IsFinish == null).FirstOrDefault();
                    if (cr != null)
                    {
                        // Lay thong tin hoat dong khoa
                        listChangeAction = Context.ChangeRequestAction.Where(x => x.ChangeRequestID == cr.ChangeRequestID && x.ApproveStatus == Constants.IS_ACTIVE)
                            .ToList();
                    }
                }
                foreach (PlanTargetModel ptm in lstPlanTarget)
                {
                    List<PlanTargetActionModel> lstPlanTargetAction = ptm.ListPlanTargetActionModel;
                    foreach (PlanTargetActionModel ptam in lstPlanTargetAction)
                    {
                        PlanTargetAction pta = Context.PlanTargetAction.Where(p => p.PlanTargetActionID == ptam.PlanTargetActionID).FirstOrDefault();
                        if (pta != null)
                        {
                            pta.Exucuter = null;
                            pta.Supporter = null;
                            pta.Note = null;
                            pta.UpdateUserID = SessionManager.GetUserInfo().UserID;
                        }
                        if (listChangeAction != null && !listChangeAction.Any(x => x.PlanTargetActionID == pta.PlanTargetActionID))
                        {
                            continue;
                        }
                        List<SubActionPlanTime> lstSubActionPlanTime = Context.SubActionPlanTime
                       .Where(x => x.PlanTargetActionID == pta.PlanTargetActionID)
                       .ToList();

                        foreach (SubActionPlanTime sapt in lstSubActionPlanTime)
                        {
                            sapt.PlanTime = null;
                            sapt.UpdateDate = DateTime.Now;
                            sapt.UpdateUserID = SessionManager.GetUserInfo().UserID;
                        }
                        Context.SaveChanges();
                        // Xoa ke hoach cua cac Subaction Plan con cua ActionPlanTargeAction
                        List<SubActionPlanModel> lstSubActionPlan = GetSubActionPlanByPlanTargetId(pta.PlanTargetActionID);
                        foreach (SubActionPlanModel subActionPlan in lstSubActionPlan)
                        {
                            SubActionPlan sap = Context.SubActionPlan.Where(s => s.SubActionPlanID == subActionPlan.SubActionPlanID).FirstOrDefault();
                            if (sap != null)
                            {
                                List<SubActionPlanTime> lstSapt = Context.SubActionPlanTime
                                .Where(x => x.SubActionPlanID == subActionPlan.SubActionPlanID)
                                .ToList();

                                foreach (SubActionPlanTime sapt in lstSapt)
                                {
                                    sapt.PlanTime = null;
                                    sapt.UpdateUserID = SessionManager.GetUserInfo().UserID;
                                    sapt.UpdateDate = DateTime.Now;
                                }
                                Context.SaveChanges();
                            }

                        }
                    }
                }
            }

            // Day cac thong tin ke hoach moi vao trong bang
            //==========================================================================


            foreach (var item in actionPlanTime)
            {
                var itemArr = item.Split('$');
                var timeArr = itemArr[0];
                var otherArr = itemArr[1];
                var planTime = timeArr.Split('_');
                var otherVal = otherArr.Split(new string[] { "^&*" }, StringSplitOptions.None);

                int subActionPlanTimeId = int.Parse(planTime[0]);

                SubActionPlanTime subActionPlanTime = Context.SubActionPlanTime.Where(x => x.SubActionPlanTimeID == subActionPlanTimeId).FirstOrDefault();
                if (subActionPlanTime != null)
                {
                    subActionPlanTime.PlanTime = "X";
                    subActionPlanTime.ModifierID = userID;
                    subActionPlanTime.UpdateDate = now;
                }

                if (planTime[1].StartsWith("pta"))
                {
                    var planTargetActionID = int.Parse(planTime[1].Split('#')[1]);
                    PlanTargetAction planTargetAction = Context.PlanTargetAction.Where(x => x.PlanTargetActionID == planTargetActionID
                        && Context.PlanTarget.Any(t => t.PlanID == planID && x.PlanTargetID == t.PlanTargetID)).FirstOrDefault();
                    if (planTargetAction != null)
                    {
                        planTargetAction.Exucuter = otherVal[0];
                        planTargetAction.Supporter = otherVal[1];
                        planTargetAction.Note = otherVal[2];
                    }
                }
                else if (planTime[1].StartsWith("sub"))
                {
                    var subActionID = int.Parse(planTime[1].Split('#')[1]);
                    SubActionPlan subActionPlan = Context.SubActionPlan.Where(x => x.SubActionPlanID == subActionID
                    && Context.PlanTargetAction.Any(a => Context.PlanTarget.Any(t => t.PlanTargetID == a.PlanTargetID
                    && t.PlanID == planID) && a.PlanTargetActionID == x.PlanTargetActionID)).FirstOrDefault();
                    if (subActionPlan != null)
                    {
                        subActionPlan.Exucuter = otherVal[0];
                        subActionPlan.Supporter = otherVal[1];
                        subActionPlan.Note = otherVal[2];
                    }
                }
            }
            Context.SaveChanges();

            return Json(new { Type = "SUCCESS", Message = "Cập nhật thông tin kế hoạch thành công." });
        }

        public PartialViewResult PreparAddSubActionPlan(int? PlanTargetAcionID, int? SubActionPlanID, int? PlanID, int? PlanTargetID)
        {

            PlanTargetAction pta = Context.PlanTargetAction.Where(x => x.PlanTargetActionID == PlanTargetAcionID).FirstOrDefault();
            if (pta == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            SubActionPlan sap = null;

            if (SubActionPlanID != null && SubActionPlanID > 0)
            {
                sap = Context.SubActionPlan.Where(x => x.SubActionPlanID == SubActionPlanID).FirstOrDefault();
            }
            Plan plan = Context.Plan.Find(PlanID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            SubActionPlanModel entity = new SubActionPlanModel();

            entity.ParentPlanTagetActionCode = sap != null ? sap.SubActionCode : pta.ActionCode;
            entity.ParentPlanTagetActionContent = sap != null ? sap.SubActionContent : pta.ActionContent;
            entity.PlanTargetActionID = pta.PlanTargetActionID;
            entity.PeriodID = plan.PeriodID;
            entity.PlanID = plan.PlanID;
            if (sap != null)
            {
                entity.SubActionPlanID = sap.SubActionPlanID;
            }
            entity.PlanTargetID = PlanTargetID;

            return PartialView("AddSubActionPlan", entity);
        }

        public JsonResult AddSubActionPlan(SubActionPlanModel obj, int PlanID)
        {
            PlanTargetAction pta = Context.PlanTargetAction.Find(obj.PlanTargetActionID);
            if (pta == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            string parentActionCode = pta.ActionCode != null ? pta.ActionCode : "";
            int? maxSubOrder = Context.SubActionPlan.Where(x => x.PlanTargetActionID == obj.PlanTargetActionID).Max(y => y.SubActionOrder);
            string subActionCode = maxSubOrder != null ? parentActionCode + "." + (maxSubOrder + 1).ToString() : parentActionCode + "." + "1";

            if (obj.SubActionPlanID != null && obj.SubActionPlanID > 0)
            {
                SubActionPlan subActionPlan = Context.SubActionPlan.Where(x => x.SubActionPlanID == obj.SubActionPlanID).FirstOrDefault();

                maxSubOrder = Context.SubActionPlan.Where(x => x.ParentID == obj.SubActionPlanID).Max(y => y.SubActionOrder);
                subActionCode = maxSubOrder != null ? subActionPlan.SubActionCode + "." + (maxSubOrder + 1).ToString() : subActionPlan.SubActionCode + "." + "1";

            }
            int? subActionOrder = maxSubOrder != null ? maxSubOrder + 1 : 1;
            //  SubActionPlan subActionPlan = Context.SubActionPlan.Where(x => x.SubActionPlanID == obj.SubActionPlanID).FirstOrDefault();

            Plan plan = Context.Plan.Find(PlanID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Period period = Context.Period.Find(plan.PeriodID);

            SubActionPlan entity = new SubActionPlan();
            entity.PlanTargetActionID = obj.PlanTargetActionID;
            entity.ParentID = obj.SubActionPlanID != null ? obj.SubActionPlanID : null;
            entity.SubActionCode = subActionCode;
            entity.SubActionContent = obj.SubActionContent;
            entity.SumCostType = obj.SumCostType;
            entity.SubActionOrder = subActionOrder;
            Context.SubActionPlan.Add(entity);
            Context.SaveChanges();
            if (period != null)
            {
                int startYear = period.FromDate.Year;
                int endYear = period.ToDate.Year;
                {
                    if (endYear >= startYear)
                    {
                        for (int year = startYear; year <= endYear; year++)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                SubActionPlanTime subActionPlanTime = new SubActionPlanTime();
                                subActionPlanTime.PlanTime = null;
                                subActionPlanTime.PlanTargetActionID = obj.PlanTargetActionID;
                                subActionPlanTime.SubActionPlanID = entity.SubActionPlanID;
                                subActionPlanTime.PlanYear = year;
                                subActionPlanTime.CreateDate = now;
                                subActionPlanTime.CreateUserID = userID;
                                subActionPlanTime.Status = Constants.IS_ACTIVE;
                                subActionPlanTime.ModifierID = userID;
                                subActionPlanTime.UpdateDate = now;
                                Context.SubActionPlanTime.Add(subActionPlanTime);


                            }
                            Context.SaveChanges();
                        }

                    }
                    SubActionPlanCostBusiness subBusiness = new SubActionPlanCostBusiness(Context);
                    subBusiness.addSubActionPlanCost(startYear, endYear, obj.PlanTargetActionID.Value, entity.SubActionPlanID, obj.PlanTargetID.Value);
                }
                Context.SaveChanges();
            }
            return Json(new { Type = "SUCCESS", Message = "Thêm hoạt động thành công!" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelSubActionPlan(int subActionPlanID)
        {
            SubActionPlan subAction = Context.SubActionPlan.Find(subActionPlanID);
            if (subAction == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<SubActionSpendItem> listSpendItem = Context.SubActionSpendItem.Where(x => x.SubActionPlanID == subActionPlanID).ToList();
            if (listSpendItem.Any())
            {
                Context.SubActionSpendItem.RemoveRange(listSpendItem);
            }
            List<SubActionPlanCost> listCost = Context.SubActionPlanCost.Where(x => x.SubActionPlanID == subActionPlanID && x.SubActionSpendItemID == null).ToList();
            if (listCost.Any())
            {
                Context.SubActionPlanCost.RemoveRange(listCost);
            }
            List<SubActionPlanTime> listTime = Context.SubActionPlanTime.Where(x => x.SubActionPlanID == subActionPlanID).ToList();
            if (listTime.Any())
            {
                Context.SubActionPlanTime.RemoveRange(listTime);
            }
            Context.SubActionPlan.Remove(subAction);
            Context.SaveChanges();

            return Json(new { Type = "SUCCESS", Message = "Xóa hoạt động thành công!" }, JsonRequestBehavior.AllowGet);
        }
    }

}