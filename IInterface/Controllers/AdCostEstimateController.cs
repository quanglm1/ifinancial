﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class AdCostEstimateController : Controller
    {
        // GET: Action
        private Entities context = new Entities();

        [BreadCrumb(ControllerName = "Chi phí hành chính", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult SearchPeriodDept(string PeriodName = null, int page = 1)
        {
            IQueryable<Plan> lstPlan = (from a in context.Plan
                                        join b in context.Department on a.DepartmentID equals b.DepartmentID
                                        where a.DepartmentID == Constants.HEAD_DEPT_ID
                                        select a);
            IQueryable<PeriodDeptModel> iqPeriod =
                (from x in context.Period
                 join y in lstPlan on x.PeriodID equals y.PeriodID into g1
                 from y in g1.DefaultIfEmpty()
                 where x.Status != Constants.PERIOD_DEACTIVED
                 select new PeriodDeptModel
                 {
                     PeriodID = x.PeriodID,
                     PlanID = y.PlanID,
                     IsChange = y.IsChange,
                     DepartmentID = Constants.HEAD_DEPT_ID,
                     Description = x.Description,
                     PeriodName = x.PeriodName,
                     FromDate = x.FromDate,
                     ToDate = x.ToDate,
                     HasAdCost = y != null && context.CostEstimateSpend.Any(x => x.PlanID == y.PlanID && x.OtherType == Constants.OTHER_TYPE_HC && x.ParentID == null),
                     Status = y == null ? 0 : y.Status
                 }).Distinct();
            if (!string.IsNullOrWhiteSpace(PeriodName))
            {
                iqPeriod.Where(x => x.PeriodName.ToLower().Contains(PeriodName));
            }
            Paginate<PeriodDeptModel> paginate = new Paginate<PeriodDeptModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqPeriod.OrderByDescending(x => x.ToDate).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = iqPeriod.Count();
            ViewData["listPeriodDept"] = paginate;
            return PartialView("_ListPeriodDept");
        }

        [BreadCrumb(ControllerName = "Chi tiết", ActionName = "Chi phí hành chính", AreaName = "Kế hoạch")]
        public ActionResult AdCost(int? periodID, int? planID, int isView = 0)
        {
            Plan plan = null;
            if (planID != null && planID >= 0)
            {
                plan = context.Plan.Where(x => x.PlanID == planID).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
                periodID = plan.PeriodID;
            }
            Period period = context.Period.Where(x => x.PeriodID == periodID).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
            }
            if (period != null)
            {
                ViewData["Period"] = period;

                if (plan == null)
                {
                    plan = context.Plan.Where(x => x.PeriodID == periodID && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
                    if (plan == null)
                    {
                        // Tao ke hoach
                        Department objDepartment = (from a in context.Department
                                                    where a.DepartmentID == Constants.HEAD_DEPT_ID
                                                    select a).FirstOrDefault();
                        plan = new Plan();
                        plan.PeriodID = periodID.Value;
                        plan.DepartmentID = objDepartment.DepartmentID;
                        plan.Status = Constants.PLAN_APPROVED;
                        plan.Type = Constants.MAIN_TYPE;
                        plan.RepresentPersonName = objDepartment.RepresentPersonName;
                        plan.RepresentPosition = objDepartment.RepresentPosition;
                        plan.RepresentAddress = objDepartment.RepresentAddress;
                        plan.RepresentEmail = objDepartment.RepresentEmail;
                        plan.RepresentPhone = objDepartment.RepresentPhone;
                        plan.ContactPersonName = objDepartment.ContactPersonName;
                        plan.ContactPosition = objDepartment.ContactPosition;
                        plan.ContactAddress = objDepartment.ContactAddress;
                        plan.ContactEmail = objDepartment.ContactEmail;
                        plan.ContactPhone = objDepartment.ContactPhone;
                        plan.FromDate = period.FromDate;
                        plan.ToDate = period.ToDate;
                        context.Plan.Add(plan);
                        context.SaveChanges();
                    }
                }
                ViewData["Plan"] = plan;
                if (plan != null)
                {
                    CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
                    if (isView == 0)
                    {
                        bus.InitAd(plan.PlanID);
                        context.SaveChanges();
                    }
                    List<CostEstimateSpendModel> listCostSpend = bus.GetListRootSpend(plan.PlanID).Where(x => x.ParentID == null && x.OtherType != null && x.OtherType == Constants.OTHER_TYPE_HC).ToList();
                    ViewData["listSpend "] = listCostSpend;
                }
                else
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
            }
            return View();
        }

        public JsonResult DelSpendItem(int id)
        {
            CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
            bus.DelSpendCost(id);
            return Json(new { Type = "SUCCESS", Message = "Xóa hạng mục chi thành công" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCostEstimate(List<string> TotalPrice, List<long> SubCostID, List<string> ArrDesc, int periodId)
        {
            Plan plan = this.context.Plan.Where(x => x.PeriodID == periodId && x.Status != Constants.IS_NOT_ACTIVE && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng kiểm tra lại");
            }
            CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
            bus.SaveCostEstimate(TotalPrice, SubCostID, ArrDesc, plan.PlanID);
            context.SaveChanges();
            return Json(new { Type = "SUCCESS", Message = "Cập nhật chi phí hành chính thành công" });

        }

        public PartialViewResult AddSpendItem(int planID, int? id, string type, string[] spendCodes, string[] spendContents)
        {
            Plan plan = context.Plan.Find(planID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch đã chọn");
            }
            Period period = context.Period.Find(plan.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int userID = SessionManager.GetUserInfo().UserID;
            int? planTargetID = null;
            int? planTargetActionID = null;
            int? subActionPlanID = null;
            int? parentID = null;
            int? MainActionID = null;
            if ("targetaction".Equals(type))
            {
                PlanTargetAction pta = this.context.PlanTargetAction.Find(id);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                planTargetID = pta.PlanTargetID.Value;
                MainActionID = pta.MainActionID.Value;
            }
            else if ("subaction".Equals(type))
            {
                var subAction = (from s in this.context.SubActionPlan
                                 join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 select new
                                 {
                                     p.PlanTargetID,
                                     p.PlanTargetActionID,
                                     s.SubActionPlanID,
                                     s.SubActionID
                                 }).FirstOrDefault();
                if (subAction == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = subAction.PlanTargetActionID;
                subActionPlanID = subAction.SubActionPlanID;
                planTargetID = subAction.PlanTargetID.Value;
                MainActionID = subAction.SubActionID.Value;
            }
            else if ("subcost".Equals(type))
            {
                CostEstimateSpend subItem = this.context.CostEstimateSpend.Find(id);
                if (subItem == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                if (subItem.PlanTargetActionID != null)
                {
                    PlanTargetAction pta = this.context.PlanTargetAction.Find(subItem.PlanTargetActionID);
                    if (pta == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                    planTargetActionID = pta.PlanTargetActionID;
                    subActionPlanID = subItem.SubActionPlanID;
                    planTargetID = pta.PlanTargetID.Value;
                    MainActionID = subItem.MainActionID;
                }
                parentID = id;
            }
            // Luu muc chi cao nhat lam cha
            int startIdx = 0;
            List<CostEstimateSpend> listSubSpendItem = new List<CostEstimateSpend>();
            List<CostEstimateSpendModel> listModel = new List<CostEstimateSpendModel>();
            for (int i = startIdx; i < spendContents.Length; i++)
            {
                CostEstimateSpend spendItem = new CostEstimateSpend();
                spendItem.PlanTargetID = planTargetID;
                spendItem.ParentID = parentID;
                spendItem.PlanTargetActionID = planTargetActionID;
                spendItem.SpendContent = spendContents[i];
                spendItem.SpendItemTitle = spendCodes[i];
                spendItem.SubActionPlanID = subActionPlanID;
                spendItem.MainActionID = MainActionID;
                spendItem.PlanID = planID;
                this.context.CostEstimateSpend.Add(spendItem);
                listSubSpendItem.Add(spendItem);
            }
            context.SaveChanges();
            // Insert phan cost
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<CostEstimateDetail> listCost = new List<CostEstimateDetail>();
            for (int year = fromYear; year <= toYear; year++)
            {
                foreach (CostEstimateSpend item in listSubSpendItem)
                {
                    CostEstimateDetail cost = new CostEstimateDetail();
                    cost.PlanTargetActionID = planTargetActionID;
                    cost.PlanTargetID = planTargetID;
                    cost.PlanYear = year;
                    cost.SubActionPlanID = subActionPlanID;
                    cost.MainActionID = item.MainActionID;
                    cost.CostEstimateSpendID = item.CostEstimateSpendID;
                    cost.PlanID = planID;
                    this.context.CostEstimateDetail.Add(cost);
                    listCost.Add(cost);
                }
            }
            context.SaveChanges();
            for (int i = 0; i < listSubSpendItem.Count; i++)
            {
                CostEstimateSpendModel model = new CostEstimateSpendModel();
                model.CostEstimateSpendID = listSubSpendItem[i].CostEstimateSpendID;
                model.SpendContent = listSubSpendItem[i].SpendContent;
                model.SpendItemTitle = listSubSpendItem[i].SpendItemTitle;
                model.SubActionPlanID = subActionPlanID;
                model.PlanTargetActionID = planTargetActionID;
                model.PlanTargetID = planTargetID;
                model.MainActionID = listSubSpendItem[i].MainActionID;
                model.ListDetail = listCost.Where(x => x.CostEstimateSpendID == model.CostEstimateSpendID)
                    .Select(x => new CostEstimateDetailModel
                    {
                        CostEstimateDetailID = x.CostEstimateDetailID,
                        CostEstimateSpendID = x.CostEstimateSpendID,
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetActionID = x.PlanTargetActionID,
                        SubActionPlanID = x.SubActionPlanID,
                        PlanYear = x.PlanYear,
                        TotalMoney = x.TotalMoney,
                        Description = x.Description,
                        MainActionID = x.MainActionID
                    }).ToList();
                listModel.Add(model);
            }
            ViewData["Period"] = period;
            ViewData["RangeYear"] = toYear - fromYear;
            ViewData["listSubSpendItem"] = listModel;
            return PartialView("_ListSubCostInsert");
        }

        public JsonResult SyncSalary(int id)
        {
            CostEstimateSpend spend = this.context.CostEstimateSpend.Find(id);
            if (spend == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Plan plan = context.Plan.Find(spend.PlanID);
            Period period = context.Period.Find(plan.PeriodID);
            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            List<double> listCost = new List<double>();
            for (int year = startYear; year <= endYear; year++)
            {
                double cost = 0;
                if (spend.IsSalary.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE)
                    {
                        cost = context.PaySheet.Where(x => x.Year == year && x.Type == spend.GroupType && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                    }
                    else if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE)
                    {
                        cost = context.Allowance.Where(x => x.Year == year && x.Type == spend.GroupType && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                    }
                }
                listCost.Add(cost);
            }
            return Json(listCost, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}