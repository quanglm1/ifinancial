﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Common;
using IInterface.Models;
using IModel;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class StatisticController : Controller
    {
        private readonly Entities _context = new Entities();

        // GET: Statistic
        [BreadCrumb(ControllerName = "Thống kê", ActionName = "Thống kê", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            StatisticModel res = new StatisticModel();

            List<int> lstPlan = _context.Plan.Where(o => o.Status != Constants.IS_NOT_ACTIVE).Select(o => o.PlanID).ToList();

            res.TargetNum = _context.PlanTarget.Count(o => lstPlan.Contains(o.PlanID));
            res.ActionNum = _context.PlanTargetAction.Count(o => lstPlan.Contains(o.PlanID.Value));
            res.IndexNum = _context.PlanTargetIndex.Count(o => lstPlan.Contains(o.PlanID.Value));

            ViewData["StatisticResult"] = res;
            return View();
        }

        public PartialViewResult GetTargetDetail()
        {
            List<int> lstPlan = _context.Plan.Where(o => o.Status != Constants.IS_NOT_ACTIVE).Select(o => o.PlanID).ToList();


            var lstTarget = (from pt in _context.PlanTarget
                             where lstPlan.Contains(pt.PlanID)
                             select new PlanTargetModel()
                             {
                                 TargetID = pt.TargetID,
                                 PlanTargetContent = pt.PlanTargetContent,
                                 PlanTargetTitle = pt.PlanTargetTitle,
                                 PlanTargetID = pt.PlanTargetID,
                             }).OrderBy(o => o.PlanTargetID).ToList();


            ViewData["ListTarget"] = lstTarget;

            return PartialView("_DetailTarget");
        }

        public PartialViewResult GetActionDetail()
        {
            List<int> lstPlan = _context.Plan.Where(o => o.Status != Constants.IS_NOT_ACTIVE).Select(o => o.PlanID).ToList();

            var lstAction = (from pa in _context.PlanTargetAction
                             where lstPlan.Contains(pa.PlanID.Value)
                             select new PlanTargetActionModel()
                             {
                                 PlanTargetActionID = pa.PlanTargetActionID,
                                 ActionContent = pa.ActionContent,
                                 PlanTargetID = pa.PlanTargetID,
                                 ActionOrder = pa.ActionOrder,
                             }).OrderBy(o => o.PlanTargetID).ThenBy(o => o.ActionOrder).ToList();


            ViewData["ListAction"] = lstAction;

            return PartialView("_DetailAction");
        }

        public PartialViewResult GetIndexDetail()
        {
            List<int> lstPlan = _context.Plan.Where(o => o.Status != Constants.IS_NOT_ACTIVE).Select(o => o.PlanID).ToList();

            var lstIndex = (from pi in _context.PlanTargetIndex
                            where lstPlan.Contains(pi.PlanID.Value)
                            select pi).OrderBy(o => o.PlanID).ThenBy(o => o.PlanTargetID).ToList();

            ViewData["ListIndex"] = lstIndex;

            return PartialView("_DetailIndex");
        }
    }
}