﻿using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;
using IInterface.Business;

namespace IInterface.Controllers
{
    public class ChangeRequestController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [BreadCrumb(ActionName = "Danh sách yêu cầu", ControllerName = "Yêu cầu thay đổi", AreaName = "Kế hoạch")]
        public ActionResult IndexDept()
        {
            ViewData["listPeriod"] = this.GetPeriod(true);
            return View();
        }

        [BreadCrumb(ActionName = "Danh sách yêu cầu", ControllerName = "Xem xét yêu cầu thay đổi", AreaName = "Kế hoạch")]
        public ActionResult IndexReview()
        {
            var listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE)
                .OrderBy(x => x.FromDate).Select(x => new
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID + "",
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Selected = false
                }).ToList();
            int yearNow = DateTime.Now.Year;
            List<SelectListItem> listSelect = listPeriod.Select(x => new SelectListItem
            {
                Text = x.Text,
                Value = x.Value
            }).ToList();
            foreach (var period in listPeriod)
            {
                if (period.FromDate.Year <= yearNow && period.ToDate.Year >= yearNow)
                {
                    SelectListItem item = listSelect.Where(x => x.Value.Equals(period.Value)).FirstOrDefault();
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
            }

            ViewData["listPeriod"] = listSelect;
            UserInfo user = SessionManager.GetUserInfo();
            ViewData["listDepartment"] = new PlanBusiness(context).GetlistDepartment(user.UserID);
            return View();
        }

        public PartialViewResult SearchDept(int PeriodID, int page = 0)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            var listResult = GetChangeRequest(PeriodID, objUser.DepartmentID, page);
            ViewData["listResult"] = listResult;
            return PartialView("_LstResult");
        }

        public PartialViewResult SearchReview(int PeriodID, int? DeptID, int page = 0)
        {
            var listResult = GetChangeRequest(PeriodID, DeptID, page);
            ViewData["listResult"] = listResult;
            return PartialView("_LstResultReview");
        }

        public Paginate<ChangeRequestModel> GetChangeRequest(int PeriodID, int? DeptID, int page)
        {
            UserInfo user = SessionManager.GetUserInfo();
            List<ChangeRequestModel> listChangeReq = (from a in context.ChangeRequest
                                                      join b in context.Plan on a.PlanID equals b.PlanID
                                                      join c in context.Period on b.PeriodID equals c.PeriodID
                                                      join d in context.Department on b.DepartmentID equals d.DepartmentID
                                                      where c.PeriodID == PeriodID && (DeptID == null || DeptID == 0 || b.DepartmentID == DeptID)
                                                      select new ChangeRequestModel
                                                      {
                                                          ChangeRequestID = a.ChangeRequestID,
                                                          PlanID = a.PlanID,
                                                          PeriodID = c.PeriodID,
                                                          PeriodName = c.PeriodName,
                                                          DepartmentID = a.DepartmentID,
                                                          DepartmentName = d.DepartmentName,
                                                          CreateDate = a.CreateDate,
                                                          Description = a.Description,
                                                          StepNow = a.StepNow,
                                                          Status = a.Status
                                                      }).ToList();
            List<int> listRequestID = listChangeReq.Select(x => x.ChangeRequestID).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, listRequestID, Constants.COMMENT_TYPE_CHANGE_REQUEST);
            Paginate<ChangeRequestModel> paginate = new Paginate<ChangeRequestModel>();
            listChangeReq.RemoveAll(x => !dicRole.ContainsKey(x.ChangeRequestID) || dicRole[x.ChangeRequestID].Role == Constants.ROLE_NO_VIEW);
            foreach (ChangeRequestModel changeReq in listChangeReq)
            {
                changeReq.Role = dicRole[changeReq.ChangeRequestID].Role;
            }
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = listChangeReq.OrderBy(x => x.CreateDate).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = listChangeReq.Count();
            return paginate;
        }

        [BreadCrumb(ActionName = "Cập nhật yêu cầu", ControllerName = "Yêu cầu thay đổi", AreaName = "Kế hoạch")]
        public ActionResult AddOrEdit(int? ChangeRequestID, int? PlanID = null)
        {
            UserInfo user = SessionManager.GetUserInfo();
            var listPeriod = this.GetPeriod();
            if (listPeriod == null)
            {
                return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Chưa được khai báo giai đoạn lập kế hoạch" });
            }
            PlanModel plan = null;
            if (PlanID != null && PlanID != 0 && (ChangeRequestID == null || ChangeRequestID.Value <= 0))
            {
                ViewData["IsFix"] = Constants.IS_ACTIVE;
                ChangeRequestID = (from cr in context.ChangeRequest
                                   join pl in context.Plan on cr.PlanID equals pl.PlanID
                                   where pl.PlanID == PlanID && pl.Status == Constants.PLAN_APPROVED && cr.IsFinish == null
                                   select cr.ChangeRequestID).FirstOrDefault();
                plan = (from pl in context.Plan
                        join pr in context.Period on pl.PeriodID equals pr.PeriodID
                        where pl.PlanID == PlanID && pl.DepartmentID == user.DepartmentID
                        && pl.Status == Constants.PLAN_APPROVED
                        select new PlanModel
                        {
                            PeriodID = pr.PeriodID,
                            PlanID = pl.PlanID,
                            DepartmentID = pl.DepartmentID,
                            PeriodName = pr.PeriodName
                        }).FirstOrDefault();
                if (plan == null)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại kế hoạch đã được phê duyệt" });
                }
            }
            if (ChangeRequestID == null || ChangeRequestID.Value <= 0)
            {
                if (plan == null)
                {
                    int periodID = listPeriod.Where(x => x.Selected).Select(x => int.Parse(x.Value)).FirstOrDefault();
                    plan = (from pl in context.Plan
                            join pr in context.Period on pl.PeriodID equals pr.PeriodID
                            where pr.PeriodID == periodID && pl.DepartmentID == user.DepartmentID
                            && pl.Status == Constants.PLAN_APPROVED
                            select new PlanModel
                            {
                                PeriodID = pr.PeriodID,
                                PlanID = pl.PlanID,
                                DepartmentID = pl.DepartmentID,
                                PeriodName = pr.PeriodName
                            }).FirstOrDefault();
                    if (plan == null)
                    {
                        return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại kế hoạch đã được phê duyệt" });
                    }
                    ViewData["plan"] = plan;
                }
            }
            else
            {
                ChangeRequest cr = context.ChangeRequest.Find(ChangeRequestID);
                if (cr == null)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại yêu cầu thay đổi" });
                }
                plan = (from pl in context.Plan
                        join pr in context.Period on pl.PeriodID equals pr.PeriodID
                        where pl.PlanID == cr.PlanID && pl.Status == Constants.PLAN_APPROVED
                        select new PlanModel
                        {
                            PeriodID = pr.PeriodID,
                            PlanID = pl.PlanID,
                            DepartmentID = pl.DepartmentID,
                            PeriodName = pr.PeriodName
                        }).FirstOrDefault();
                if (plan == null)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại kế hoạch đã được phê duyệt" });
                }
                ViewData["changeRequest"] = cr;
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, ChangeRequestID.Value, Constants.COMMENT_TYPE_CHANGE_REQUEST);
                if (role == null || role.Role < Constants.ROLE_VIEW)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Bạn không có quyền cập nhật yêu cầu này" });
                }
                if (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE)
                {
                    return RedirectToAction("View", new { ChangeRequestID = ChangeRequestID, PlanID = PlanID });
                }
                plan.Role = role.Role;
            }
            ViewData["plan"] = plan;
            foreach (SelectListItem item in listPeriod)
            {
                item.Selected = false;
                if (item.Value.Equals(plan.PeriodID.ToString()))
                {
                    item.Selected = true;
                }
            }
            ViewData["listPeriod"] = listPeriod;
            return View();
        }

        [BreadCrumb(ActionName = "Chi tiết thay đổi", ControllerName = "Yêu cầu thay đổi", AreaName = "Kế hoạch")]
        public ActionResult View(int planID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            ChangeRequest changeRequest = (from cr in context.ChangeRequest
                                           join pl in context.Plan on cr.PlanID equals pl.PlanID
                                           where pl.PlanID == planID && pl.Status == Constants.PLAN_APPROVED && pl.IsChange == Constants.IS_ACTIVE
                                           select cr).FirstOrDefault();
            if (changeRequest == null)
            {
                return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại yêu cầu thay đổi" });
            }
            PlanModel plan = (from pl in context.Plan
                              join pr in context.Period on pl.PeriodID equals pr.PeriodID
                              where pl.PlanID == changeRequest.PlanID && pl.Status == Constants.PLAN_APPROVED
                              select new PlanModel
                              {
                                  PeriodID = pr.PeriodID,
                                  PlanID = pl.PlanID,
                                  PeriodName = pr.PeriodName,
                                  IsChange = pl.IsChange
                              }).FirstOrDefault();
            if (plan == null)
            {
                return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Không tồn tại kế hoạch đã được phê duyệt" });
            }
            ViewData["changeRequest"] = changeRequest;
            ViewData["plan"] = plan;
            List<PlanTargetModel> listPlanTarget = GetListAction(changeRequest.PlanID, changeRequest.ChangeRequestID);
            ViewData["listPlanTarget"] = listPlanTarget;
            return View();
        }

        public PartialViewResult LoadAction(int PeriodID, int ChangeRequestID = 0)
        {
            int planID = 0;
            if (ChangeRequestID > 0)
            {
                ChangeRequest cr = context.ChangeRequest.Find(ChangeRequestID);
                if (cr == null)
                {
                    throw new BusinessException("Không tồn tại yêu cầu");
                }
                planID = cr.PlanID;
            }
            else
            {
                UserInfo user = SessionManager.GetUserInfo();
                Period period = context.Period.Find(PeriodID);
                if (period == null)
                {
                    throw new BusinessException("Không tồn tại giai đoạn kế hoạch");
                }
                Plan plan = context.Plan.Where(x => x.PeriodID == PeriodID && x.DepartmentID == user.DepartmentID && x.Status == Constants.PLAN_APPROVED).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch được phê duyệt giai đoạn " + period.FromDate.Year + " - " + period.ToDate.Year);
                }
                planID = plan.PlanID;
            }
            List<PlanTargetModel> listPlanTarget = GetListAction(planID, ChangeRequestID);
            ViewData["listPlanTarget"] = listPlanTarget;
            return PartialView("_ListAction");
        }

        public JsonResult SaveRequest(ChangeRequestModel model)
        {
            string msg;
            UserInfo user = SessionManager.GetUserInfo();
            ChangeRequest cr = null;
            List<ChangeRequestAction> listAction = null;
            if (model.PlanTargetActionIDs == null || !model.PlanTargetActionIDs.Any())
            {
                throw new BusinessException("Bạn chưa chọn hoạt động cần điều chỉnh");
            }
            if (model.ChangeRequestID > 0)
            {
                cr = context.ChangeRequest.Find(model.ChangeRequestID);
                if (cr == null)
                {
                    throw new BusinessException("Không tồn tại yêu cầu thay đổi");
                }
                if (cr.DepartmentID == user.DepartmentID)
                {
                    PlanModel plan = this.GetPlan(model.PeriodID);
                    if (plan == null)
                    {
                        throw new BusinessException("Không tồn tại kế hoạch được phê duyệt giai đoạn " + plan.FromDate.Year + " - " + plan.ToDate.Year);
                    }
                    if (context.ChangeRequest.Any(x => x.PlanID == plan.PlanID && x.ChangeRequestID != cr.ChangeRequestID
                     && x.Status != Constants.IS_NOT_ACTIVE && x.Status != Constants.PLAN_APPROVED))
                    {
                        throw new BusinessException("Kế hoạch giai đoạn " + plan.FromDate.Year + " - " + plan.ToDate.Year + " đang được yêu cầu điều chỉnh, vui lòng kiểm tra lại");
                    }
                    cr.PlanID = plan.PlanID.Value;
                }
                cr.LastUpdateDate = DateTime.Now;
                cr.LastUpdateUserID = user.UserID;
                cr.Description = model.Description;
                listAction = context.ChangeRequestAction.Where(x => x.ChangeRequestID == model.ChangeRequestID).ToList();
                msg = "Cập nhật yêu cầu thay đổi thành công";
            }
            else
            {
                PlanModel plan = this.GetPlan(model.PeriodID);
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch được phê duyệt giai đoạn " + plan.FromDate.Year + " - " + plan.ToDate.Year);
                }
                if (context.ChangeRequest.Any(x => x.PlanID == plan.PlanID
                     && x.Status != Constants.IS_NOT_ACTIVE && x.Status != Constants.PLAN_APPROVED))
                {
                    throw new BusinessException("Kế hoạch giai đoạn " + plan.FromDate.Year + " - " + plan.ToDate.Year + " đang được yêu cầu điều chỉnh, vui lòng kiểm tra lại");
                }
                cr = new ChangeRequest();
                cr.CreateDate = DateTime.Now;
                cr.CreateUserID = user.UserID;
                cr.DepartmentID = user.DepartmentID.Value;
                cr.PlanID = plan.PlanID.Value;
                cr.Status = Constants.PLAN_NEW;
                cr.Description = model.Description;
                context.ChangeRequest.Add(cr);
                listAction = new List<ChangeRequestAction>();
                msg = "Tạo yêu cầu thay đổi thành công";
            }
            context.SaveChanges();
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanTargetID == x.PlanTargetID && t.PlanID == cr.PlanID)).ToList();
            foreach (int planTargetActionID in model.PlanTargetActionIDs)
            {
                ChangeRequestAction crAction = listAction.Where(x => x.PlanTargetActionID == planTargetActionID).FirstOrDefault();
                if (crAction != null)
                {
                    if (cr.DepartmentID != user.DepartmentID)
                    {
                        crAction.ApproveStatus = Constants.IS_ACTIVE;
                    }
                    listAction.RemoveAll(x => x.PlanTargetActionID == planTargetActionID);
                }
                else
                {
                    PlanTargetAction pta = listPlanTargetAction.Where(x => x.PlanTargetActionID == planTargetActionID).FirstOrDefault();
                    if (pta == null)
                    {
                        throw new BusinessException("Hoạt động không thuộc kế hoạch");
                    }
                    crAction = new ChangeRequestAction();
                    crAction.ChangeRequestID = cr.ChangeRequestID;
                    crAction.PlanTargetActionID = planTargetActionID;
                    crAction.PlanTargetID = pta.PlanTargetID.Value;
                    crAction.PlanID = cr.PlanID;
                    context.ChangeRequestAction.Add(crAction);
                }
            }
            if (cr.DepartmentID == user.DepartmentID)
            {
                context.ChangeRequestAction.RemoveRange(listAction);
            }
            else
            {
                // Cap nhat trang thai ko duoc phe duyet
                foreach (ChangeRequestAction action in listAction)
                {
                    action.ApproveStatus = Constants.IS_NOT_ACTIVE;
                }
            }
            context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        private PlanModel GetPlan(int periodID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            PlanModel plan = (from pl in context.Plan
                              join pr in context.Period on pl.PeriodID equals pr.PeriodID
                              where pl.PeriodID == periodID && pl.Status == Constants.PLAN_APPROVED && pl.DepartmentID == user.DepartmentID
                              select new PlanModel
                              {
                                  PeriodID = pr.PeriodID,
                                  PlanID = pl.PlanID,
                                  PeriodName = pr.PeriodName,
                                  FromDate = pr.FromDate,
                                  ToDate = pr.ToDate
                              }).FirstOrDefault();
            return plan;
        }

        public JsonResult Delete(int ChangeRequestID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            ChangeRequest entity = context.ChangeRequest.Find(ChangeRequestID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại yêu cầu");
            }
            if (entity.DepartmentID != user.DepartmentID)
            {
                throw new BusinessException("Bạn không có quyền xóa yêu cầu này");
            }
            entity.Status = Constants.IS_NOT_ACTIVE;
            context.SaveChanges();
            return Json("Xóa đề xuất thay đổi kế hoạch thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RequestReview(int ChangeRequestID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            ChangeRequest entity = context.ChangeRequest.Find(ChangeRequestID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại yêu cầu");
            }
            if (entity.DepartmentID != user.DepartmentID)
            {
                throw new BusinessException("Bạn không có quyền gửi yêu cầu đối với kế hoạch này");
            }
            // Check role
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, ChangeRequestID, Constants.COMMENT_TYPE_CHANGE_REQUEST);
            if (role == null || (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE))
            {
                throw new BusinessException("Yêu cầu đã được gửi trước đó");
            }
            entity.Status = Constants.PLAN_NEXT_LEVER;
            entity.StepNow = entity.StepNow.GetValueOrDefault() + 1;
            ReportReview entityHis = new ReportReview();
            entityHis.UserID = user.UserID;
            entityHis.ReportID = ChangeRequestID;
            entityHis.ProcessDate = DateTime.Now;
            entityHis.ReportType = Constants.REPORT_TYPE_CHANGE_REQUEST;
            entityHis.Status = Constants.IS_ACTIVE;
            entityHis.ActionType = Constants.REPORT_ACTION_TYPE_SEND;
            context.ReportReview.Add(entityHis);
            context.SaveChanges();
            return Json("Gửi đề xuất thay đổi kế hoạch thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Agree(int ChangeRequestID)
        {
            if (ChangeRequestID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                ChangeRequest entity = context.ChangeRequest.Find(ChangeRequestID);
                if (entity.DepartmentID == user.DepartmentID)
                {
                    throw new BusinessException("Bạn không có quyền xem xét yêu cầu của chính đơn vị");
                }
                if (entity.Status < Constants.PLAN_NEXT_LEVER)
                {
                    throw new BusinessException("Yêu cầu chưa được gửi lên quỹ");
                }
                // Check role
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, ChangeRequestID, Constants.COMMENT_TYPE_CHANGE_REQUEST);
                if (role == null || (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE))
                {
                    throw new BusinessException("Bạn không có quyền xem xét yêu cầu này");
                }
                entity.Status = Constants.PLAN_NEXT_LEVER;
                entity.StepNow = entity.StepNow.GetValueOrDefault() + 1;
                ReportReview entityHis = new ReportReview();
                entityHis.UserID = user.UserID;
                entityHis.ReportID = ChangeRequestID;
                entityHis.ProcessDate = DateTime.Now;
                entityHis.ReportType = Constants.REPORT_TYPE_CHANGE_REQUEST;
                entityHis.Status = Constants.IS_ACTIVE;
                entityHis.ActionType = Constants.REPORT_ACTION_TYPE_AGREE;
                context.ReportReview.Add(entityHis);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Không tồn tại yêu cầu");
            }
            return Json("Xem xét và gửi yêu cầu lên cấp trên thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Unlock(int ChangeRequestID)
        {
            if (ChangeRequestID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                ChangeRequest entity = context.ChangeRequest.Find(ChangeRequestID);
                if (entity.DepartmentID == user.DepartmentID)
                {
                    throw new BusinessException("Bạn không có quyền xem xét yêu cầu của chính đơn vị");
                }
                if (entity.Status < Constants.PLAN_NEXT_LEVER)
                {
                    throw new BusinessException("Yêu cầu chưa được gửi lên quỹ");
                }
                // Check role
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, ChangeRequestID, Constants.COMMENT_TYPE_CHANGE_REQUEST);
                if (role == null || (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE))
                {
                    throw new BusinessException("Bạn không có quyền mở khóa yêu cầu này");
                }
                entity.Status = Constants.PLAN_NEXT_LEVER;
                entity.StepNow = entity.StepNow.GetValueOrDefault() - 1;
                if (entity.StepNow.GetValueOrDefault() <= 0)
                {
                    entity.StepNow = null;
                }
                ReportReview entityHis = new ReportReview();
                entityHis.UserID = user.UserID;
                entityHis.ReportID = ChangeRequestID;
                entityHis.ProcessDate = DateTime.Now;
                entityHis.ReportType = Constants.REPORT_TYPE_CHANGE_REQUEST;
                entityHis.Status = Constants.IS_ACTIVE;
                entityHis.ActionType = Constants.REPORT_ACTION_TYPE_UNLOCK;
                context.ReportReview.Add(entityHis);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Không tồn tại yêu cầu");
            }
            return Json("Mở khóa yêu cầu thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApproveOrReject(int ChangeRequestID, int Status)
        {
            string msg;
            if (ChangeRequestID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                ChangeRequest entity = context.ChangeRequest.Find(ChangeRequestID);
                if (entity.DepartmentID == user.DepartmentID)
                {
                    throw new BusinessException("Bạn không có quyền xem xét yêu cầu của chính đơn vị");
                }
                if (entity.Status < Constants.PLAN_NEXT_LEVER)
                {
                    throw new BusinessException("Yêu cầu chưa được gửi lên quỹ");
                }
                // Check role
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, ChangeRequestID, Constants.COMMENT_TYPE_CHANGE_REQUEST);
                if (role == null || role.Role < Constants.ROLE_APPROVE)
                {
                    throw new BusinessException("Bạn không có quyền thao tác yêu cầu này");
                }

                int reviewType;
                if (Status == Constants.IS_ACTIVE)
                {
                    reviewType = Constants.REPORT_ACTION_TYPE_APPROVE;
                    entity.Status = Constants.PLAN_APPROVED;
                    entity.StepNow = entity.StepNow.GetValueOrDefault() + 1;
                    // cap nhat lai trang thai cua ke hoach goc cho phep bat dau chinh sua
                    Plan plan = context.Plan.Find(entity.PlanID);
                    plan.ChangeStatus = Constants.PLAN_NEW;
                    plan.IsChange = Constants.IS_ACTIVE;
                    plan.ChangeStepNow = null;
                    msg = "Phê duyệt yêu cầu thành công";
                }
                else
                {
                    reviewType = Constants.REPORT_ACTION_TYPE_UNAPPROVE;
                    entity.Status = Constants.PLAN_NEXT_LEVER;
                    entity.StepNow = entity.StepNow.GetValueOrDefault() - 1;
                    if (entity.StepNow.GetValueOrDefault() <= 0)
                    {
                        entity.StepNow = null;
                    }
                    // cap nhat lai trang thai cua ke hoach goc quay lai phe duyet
                    Plan plan = context.Plan.Find(entity.PlanID);
                    plan.ChangeStatus = null;
                    plan.IsChange = null;
                    plan.ChangeStepNow = null;
                    msg = "Hủy phê duyệt yêu cầu thành công";
                }
                ReportReview entityHis = new ReportReview();
                entityHis.UserID = user.UserID;
                entityHis.ReportID = ChangeRequestID;
                entityHis.ProcessDate = DateTime.Now;
                entityHis.ReportType = Constants.REPORT_TYPE_CHANGE_REQUEST;
                entityHis.Status = Constants.IS_ACTIVE;
                entityHis.ActionType = reviewType;
                context.ReportReview.Add(entityHis);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Không tồn tại yêu cầu");
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public List<PlanTargetModel> GetListAction(int PlanID, int? ChangeRequestID)
        {
            List<ChangeRequestAction> listRequestAction = null;
            if (ChangeRequestID != null && ChangeRequestID.Value > 0)
            {
                listRequestAction = context.ChangeRequestAction.Where(x => x.ChangeRequestID == ChangeRequestID
                && context.ChangeRequest.Any(c => c.ChangeRequestID == x.ChangeRequestID && c.Status != Constants.IS_NOT_ACTIVE)).ToList();
            }
            else
            {
                listRequestAction = new List<ChangeRequestAction>();
            }
            List<PlanTargetModel> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == PlanID).OrderBy(x => x.PlanTargetID)
                    .Select(x => new PlanTargetModel
                    {
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetTitle = x.PlanTargetTitle,
                        TargetCode = x.TargetCode,
                        PlanTargetContent = x.PlanTargetContent,
                        TotalBudget = x.TotalBuget
                    }).ToList();

            List<PlanTargetActionModel> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanID == PlanID && t.PlanTargetID == x.PlanTargetID))
                .OrderBy(x => x.PlanTargetActionID)
                .Select(x => new PlanTargetActionModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    TotalBudget = x.TotalBudget
                }).ToList();
            foreach (PlanTargetActionModel pta in listPlanTargetAction)
            {
                ChangeRequestAction crActiond = listRequestAction.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).FirstOrDefault();
                if (crActiond != null)
                {
                    pta.IsChosen = true;
                    pta.ApproveStatus = crActiond.ApproveStatus;
                    pta.SettleDescription = crActiond.ChangeContent;
                }
            }
            foreach (PlanTargetModel pt in listPlanTarget)
            {
                pt.IsChosen = true;
                pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                if (pt.ListPlanTargetActionModel != null && pt.ListPlanTargetActionModel.Any())
                {
                    if (!pt.ListPlanTargetActionModel.Any(x => x.IsChosen))
                    {
                        pt.IsChosen = false;
                    }
                    if (pt.ListPlanTargetActionModel.Any(x => x.ApproveStatus != null))
                    {
                        pt.ApproveStatus = Constants.IS_ACTIVE;
                    }
                }
                else
                {
                    pt.IsChosen = false;
                }
            }
            return listPlanTarget;
        }

        private List<SelectListItem> GetPeriod(bool isAll = false)
        {
            var listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE)
                .OrderBy(x => x.FromDate).Select(x => new
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID + "",
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Selected = false
                }).ToList();
            int yearNow = DateTime.Now.Year;
            List<SelectListItem> listSelect = listPeriod.Select(x => new SelectListItem
            {
                Text = x.Text,
                Value = x.Value
            }).ToList();
            bool hasSected = false;
            foreach (var period in listPeriod)
            {
                if (period.FromDate.Year <= yearNow && period.ToDate.Year >= yearNow)
                {
                    SelectListItem item = listSelect.Where(x => x.Value.Equals(period.Value)).FirstOrDefault();
                    if (item != null)
                    {
                        item.Selected = true;
                        hasSected = true;
                    }
                }
            }
            if (!hasSected && listSelect.Any())
            {
                listSelect[0].Selected = true;
            }
            if (isAll)
            {
                listSelect.Insert(0, new SelectListItem
                {
                    Text = "-- Tất cả --",
                    Value = ""
                });
            }
            return listSelect;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}