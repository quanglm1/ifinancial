﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class SettleSynthesisController : ReportController
    {
        //private void RemoveGarbage()
        //{
        //    var lstTarget = this.context.SettleSynthesisCost.Where(x => x.PlanTargetActionID == null).GroupBy(x => new { x.SettleSynthesisID, x.PlanTargetID })
        //        .Where(grp => grp.Count() > 1).Select(x => new { maxID = x.Max(g => g.SettleSynthesisCostID), x.Key.SettleSynthesisID, x.Key.PlanTargetID }).ToList();
        //    foreach (var item in lstTarget)
        //    {
        //        var lstRemove = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == item.SettleSynthesisID
        //        && x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == null && x.SettleSynthesisCostID != item.maxID).ToList();
        //        this.context.SettleSynthesisCost.RemoveRange(lstRemove);
        //    }
        //    var lstTargetAction = this.context.SettleSynthesisCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID != null && x.SubPlanActionID == null && x.SubActionSpendItemID == null)
        //        .GroupBy(x => new { x.SettleSynthesisID, x.PlanTargetID, x.PlanTargetActionID })
        //        .Where(grp => grp.Count() > 1).Select(x => new { maxID = x.Max(g => g.SettleSynthesisCostID), x.Key.SettleSynthesisID, x.Key.PlanTargetID, x.Key.PlanTargetActionID }).ToList();
        //    foreach (var item in lstTargetAction)
        //    {
        //        var lstRemove = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == item.SettleSynthesisID
        //        && x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == item.PlanTargetActionID
        //        && x.SubActionSpendItemID == null && x.SubPlanActionID == null
        //        && x.SettleSynthesisCostID != item.maxID).ToList();
        //        this.context.SettleSynthesisCost.RemoveRange(lstRemove);
        //    }
        //    var lstSubAction = this.context.SettleSynthesisCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID != null
        //        && x.SubPlanActionID != null && x.SubActionSpendItemID == null)
        //        .GroupBy(x => new { x.SettleSynthesisID, x.PlanTargetID, x.PlanTargetActionID, x.SubPlanActionID })
        //        .Where(grp => grp.Count() > 1).Select(x => new { maxID = x.Max(g => g.SettleSynthesisCostID), x.Key.SettleSynthesisID, x.Key.PlanTargetID,
        //            x.Key.PlanTargetActionID, x.Key.SubPlanActionID }).ToList();
        //    foreach (var item in lstSubAction)
        //    {
        //        var lstRemove = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == item.SettleSynthesisID
        //        && x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == item.PlanTargetActionID
        //        && x.SubActionSpendItemID == null && x.SubPlanActionID == item.SubPlanActionID
        //        && x.SettleSynthesisCostID != item.maxID).ToList();
        //        this.context.SettleSynthesisCost.RemoveRange(lstRemove);
        //    }
        //    var lstSpend = this.context.SettleSynthesisCost.Where(x => x.PlanTargetID != null && x.PlanTargetActionID != null
        //        && x.SubPlanActionID == null && x.SubActionSpendItemID != null)
        //        .GroupBy(x => new { x.SettleSynthesisID, x.PlanTargetID, x.PlanTargetActionID, x.SubActionSpendItemID })
        //        .Where(grp => grp.Count() > 1).Select(x => new {
        //            maxID = x.Max(g => g.SettleSynthesisCostID),
        //            x.Key.SettleSynthesisID,
        //            x.Key.PlanTargetID,
        //            x.Key.PlanTargetActionID,
        //            x.Key.SubActionSpendItemID
        //        }).ToList();
        //    foreach (var item in lstSpend)
        //    {
        //        var lstRemove = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == item.SettleSynthesisID
        //        && x.PlanTargetID == item.PlanTargetID && x.PlanTargetActionID == item.PlanTargetActionID
        //        && x.SubActionSpendItemID == item.SubActionSpendItemID && x.SubPlanActionID == null
        //        && x.SettleSynthesisCostID != item.maxID).ToList();
        //        this.context.SettleSynthesisCost.RemoveRange(lstRemove);
        //    }
        //}

        [BreadCrumb(ControllerName = "Báo cáo quyết toán", ActionName = "Quyết toán quý", AreaName = "Báo cáo")]
        public ActionResult IndexDept()
        {
            return BaseIndexDept();
        }

        [BreadCrumb(ControllerName = "Quyết toán quý", ActionName = "Quyết toán quý", AreaName = "Xem xét báo cáo")]
        public ActionResult IndexReview()
        {
            return BaseIndexReview();
        }
        [BreadCrumb(ControllerName = "Quyết toán quý", ActionName = "Quyết toán quý", AreaName = "Báo cáo")]
        public ActionResult AddOrEdit(int? id, int periodID = 0, int year = 0, int? quarter = 0, int isReview = 0)
        {
            SettleInfoModel model = null;
            Plan plan = null;
            if (id == null || id == 0)
            {
                if (periodID <= 0 || year <= 0)
                {
                    RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Dữ liệu không hợp lệ" });
                }
                if (isReview == 0)
                {
                    if (quarter == 0)
                    {
                        quarter = null;
                    }
                    UserInfo user = SessionManager.GetUserInfo();
                    plan = context.Plan.Where(x => x.DepartmentID == user.DepartmentID && x.PeriodID == periodID).FirstOrDefault();
                    if (plan == null)
                    {
                        RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Kế hoạch chưa được phê duyệt" });
                    }
                    SettleSynthesis report = context.SettleSynthesis.Where(x => x.DepartmentID == user.DepartmentID && x.Year == year && x.Quarter == quarter
                    && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                    if (report == null)
                    {
                        report = new SettleSynthesis();
                        report.CreateDate = DateTime.Now;
                        report.Creator = user.UserName;
                        report.DepartmentID = user.DepartmentID;
                        report.PlanID = plan.PlanID;
                        report.Quarter = quarter;
                        report.Status = Constants.PLAN_NEW;
                        report.Type = user.DeptType;
                        report.Year = year;
                        context.SettleSynthesis.Add(report);
                        context.SaveChanges();
                    }
                    id = report.SettleSynthesisID;
                }
            }
            if (id != null)
            {
                var entity = (from s in this.context.SettleSynthesis
                              join p in this.context.Plan on s.PlanID equals p.PlanID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              where s.SettleSynthesisID == id && s.Status != Constants.PLAN_DEACTIVED
                              select new
                              {
                                  pr.PeriodID,
                                  s.SettleSynthesisID,
                                  s.Year,
                                  s.Quarter,
                                  s.PlanID,
                                  s.DepartmentID,
                                  s.TotalSettleMoney,
                                  s.TotalPlanMoney,
                                  s.Status,
                                  pr.PeriodName,
                                  d.DepartmentName,
                                  s.SendDate
                              }).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                model = new SettleInfoModel();
                model.SettleSynthesisID = entity.SettleSynthesisID;
                model.Year = entity.Year;
                model.Quarter = entity.Quarter;
                model.PeriodID = entity.PeriodID;
                model.PlanID = entity.PlanID;
                model.PlanName = entity.PeriodName;
                model.TotalSettleMoney = entity.TotalSettleMoney;
                model.TotalPlanMoney = entity.TotalPlanMoney;
                model.Status = entity.Status;
                model.DeptName = entity.DepartmentName;
                model.SendDate = entity.SendDate;
                model.DeptID = entity.DepartmentID.GetValueOrDefault();
                plan = this.context.Plan.Find(entity.PlanID);
                year = entity.Year;
            }
            var totalPlanMoney = SetAddOrEditData(id, plan, year, isReview, false);
            if (model != null)
            {
                model.TotalPlanMoney = totalPlanMoney;
                model.TotalSettleMoney = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == id)
                    .Where(x => x.PlanTargetActionID == null).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
            }
            return View("AddOrEdit", model);
        }

        [BreadCrumb(ControllerName = "Quyết toán quý", ActionName = "Chi tiết", AreaName = "Báo cáo")]
        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }

        public JsonResult SaveGeneralInfo(SettleCostData data)
        {
            SettleSynthesis settle = null;
            if (data.SettleSynthesisID > 0)
            {
                settle = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == data.SettleSynthesisID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                if (settle == null)
                {
                    throw new BusinessException("Không tồn tại báo cáo quyết toán, vui lòng kiểm tra lại");
                }
                int deptID = SessionManager.GetUserInfo().DepartmentID.Value;
                Plan plan = this.context.Plan.Where(x => x.PeriodID == data.PeriodID && x.DepartmentID == deptID).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
                }
                settle.PlanID = plan.PlanID;
                settle.Year = data.Year;
                settle.Quarter = data.Quarter;
                settle.DepartmentID = SessionManager.GetUserInfo().DepartmentID;
                this.context.SaveChanges();
            }
            else
            {
                // Check xem bao cao da tao chua
                settle = this.context.SettleSynthesis.Where(x => x.PlanID == data.PeriodID && x.Year == data.Year && x.Quarter == data.Quarter).FirstOrDefault();
                if (settle != null)
                {
                    throw new BusinessException("Đã tồn tại báo cáo quyết toán cho thời gian và kế hoạch vừa chọn, vui lòng kiểm tra lại");
                }
                settle = new SettleSynthesis();
                settle.PlanID = data.PeriodID;
                settle.Year = data.Year;
                settle.Quarter = data.Quarter;
                settle.CreateDate = DateTime.Now;
                settle.Status = Constants.PERIOD_NEW;
                settle.Creator = SessionManager.GetUserInfo().UserID.ToString();
                settle.CreateUserID = SessionManager.GetUserInfo().UserID;
                settle.DepartmentID = SessionManager.GetUserInfo().DepartmentID;
                this.context.SettleSynthesis.Add(settle);
                this.context.SaveChanges();
            }
            return Json(settle.SettleSynthesisID, JsonRequestBehavior.AllowGet);
        }

        private void ValidateEditRole(int id)
        {
            var user = SessionManager.GetUserInfo();
            var busRole = new FlowProcessBusiness(this.context);
            var role = busRole.GetRoleForObject(user.UserID, id, Constants.COMMENT_TYPE_SETTLE);
            if (role == null || role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
        }

        public JsonResult DeleteReport(int id)
        {
            SettleSynthesis entity = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == id && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo quyết toán");
            }
            var user = SessionManager.GetUserInfo();
            Plan plan = this.context.Plan.Where(x => x.PlanID == entity.PlanID && x.DepartmentID == user.DepartmentID).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            ValidateEditRole(id);
            var finalReport = context.FinancialReport.Where(x => x.PlanID == plan.PlanID && x.Year == entity.Year && x.Quarter == entity.Quarter).FirstOrDefault();
            if (finalReport != null)
            {
                this.context.Database.ExecuteSqlCommand("delete from FinancialReportDetail where FinancialReportID = " + finalReport.FinancialReportID);
                this.context.Database.ExecuteSqlCommand("delete from ReportReview where ReportID = " + finalReport.FinancialReportID + " and ReportType = " + Constants.REPORT_TYPE_FINANCIAL);
                this.context.FinancialReport.Remove(finalReport);
            }
            this.context.Database.ExecuteSqlCommand("delete from SettleSynthesisCost where SettleSynthesisID = " + entity.SettleSynthesisID);
            this.context.Database.ExecuteSqlCommand("delete from ReportReview where ReportID = " + entity.SettleSynthesisID + " and ReportType = " + Constants.REPORT_TYPE_SETTLE);
            this.context.SettleSynthesis.Remove(entity);
            this.context.SaveChanges();
            return Json("Xóa báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadExpenseInfo(int synID, int planTargetID, int isView = 0)
        {
            SettleSynthesis entity = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo quyết toán, vui lòng kiểm tra lại");
            }
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            PlanTargetModel planTarget = bus.GetListCost(0, planTargetID, synID);
            ViewData["planTarget"] = planTarget;
            SettleSynthesisCost detail = this.context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID
                    && x.PlanTargetID == planTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.SettleSynthesisCostID && c.Type == Constants.COMMENT_TYPE_SETTLE
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.SettleSynthesisCostID;
            }
            ViewData["settleReport"] = entity;
            ViewData["isView"] = isView;
            if (isView == 0)
            {
                return PartialView("_ExpenseInfo");
            }
            else
            {
                return PartialView("_ExpenseInfoView");
            }
        }

        public PartialViewResult Search(int PeriodID, int Year, int DeptID = 0, int Status = 0, int Quarter = 0, int IsReview = 0, int page = 1)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (IsReview == Constants.IS_ACTIVE)
            {
                ViewData["listResult"] = new SettleSynthesisBusiness(this.context).GetListSettleReport(userInfo.UserID, PeriodID, DeptID, Status, Quarter, Year, page); ;
            }
            else
            {
                ViewData["listResult"] = new SettleSynthesisBusiness(this.context).GetListSettleReport(userInfo.UserID, PeriodID, userInfo.DepartmentID.Value, Status, Quarter, Year, page); ;
            }
            ViewData["isReview"] = IsReview;
            return PartialView("_ListResult");
        }

        public PartialViewResult LoadCostByAction(int planTargetActionID, int planTargetID, int synID)
        {
            SettleSynthesis entity = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo quyết toán, vui lòng kiểm tra lại");
            }
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            PlanTargetModel planTarget = bus.GetListCost(planTargetActionID, planTargetID, synID);
            ViewData["settleReport"] = entity;
            return PartialView("_ListExpense", planTarget);
        }

        public JsonResult DelExpenseByAction(long synCostID)
        {
            SettleSynthesisCost cost = this.context.SettleSynthesisCost.Find(synCostID);
            if (cost == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            ValidateEditRole(cost.SettleSynthesisID);
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            bus.DelSettleCost(synCostID);
            return Json("Xóa dữ liệu thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, int synID)
        {
            SettleSynthesis entitySettle = this.context.SettleSynthesis.Where(x => x.SettleSynthesisID == synID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entitySettle == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID
            && x.ActionContent != null && x.ActionContent.Trim() != "").OrderBy(x => x.ActionOrder)
                .Select(x => new PlanTargetActionModel
                {
                    PlanID = x.PlanID.Value,
                    PlanTargetID = x.PlanTargetID.Value,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }

        public JsonResult SendReport(int synID)
        {
            var user = SessionManager.GetUserInfo();
            var bus = new FlowProcessBusiness(this.context);
            var role = bus.GetRoleForObject(user.UserID, synID, Constants.COMMENT_TYPE_SETTLE);
            if (role == null || role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            new ReportReviewBusiness(this.context).SendReport(synID, Constants.REPORT_TYPE_SETTLE);
            return Json("Gửi báo cáo quyết toán thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AddSpendItem(int synID, long id, string type, string[] spendCodes, string[] spendContents)
        {
            ValidateEditRole(synID);
            int userID = SessionManager.GetUserInfo().UserID;
            int planTargetID;
            int planTargetActionID;
            int? subActionPlanID = null;
            long? parentID = null;
            int? subSpendItemID = null;
            List<SettleSynthesisCost> listSettleCost = new List<SettleSynthesisCost>();
            if ("targetaction".Equals(type))
            {
                PlanTargetAction pta = this.context.PlanTargetAction.Find(id);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                planTargetID = pta.PlanTargetID.Value;
                parentID = context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID && x.PlanTargetActionID == pta.PlanTargetActionID
                && x.SubActionSpendItemID == null && x.SubPlanActionID == null).Select(x => x.SettleSynthesisCostID).FirstOrDefault();
            }
            else if ("subaction".Equals(type))
            {
                var subAction = (from s in this.context.SubActionPlan
                                 join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 select new
                                 {
                                     p.PlanTargetID,
                                     p.PlanTargetActionID,
                                     s.SubActionPlanID
                                 }).FirstOrDefault();
                if (subAction == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = subAction.PlanTargetActionID;
                subActionPlanID = subAction.SubActionPlanID;
                planTargetID = subAction.PlanTargetID.Value;
                parentID = context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID && x.PlanTargetActionID == subAction.PlanTargetActionID
                && x.SubActionSpendItemID == null && x.SubPlanActionID == subAction.SubActionPlanID).Select(x => x.SettleSynthesisCostID).FirstOrDefault();
            }
            else if ("subcost".Equals(type))
            {
                SubActionSpendItem subItem = this.context.SubActionSpendItem.Find(id);
                if (subItem == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                PlanTargetAction pta = this.context.PlanTargetAction.Find(subItem.PlanTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                subActionPlanID = subItem.SubActionPlanID;
                planTargetID = pta.PlanTargetID.Value;
                subSpendItemID = subItem.SubActionSpendItemID;
                parentID = context.SettleSynthesisCost.Where(x => x.SettleSynthesisID == synID && x.PlanTargetActionID == pta.PlanTargetActionID
                && x.SubActionSpendItemID == subItem.SubActionSpendItemID && x.SubPlanActionID == subItem.SubActionPlanID).Select(x => x.SettleSynthesisCostID).FirstOrDefault();
            }
            else
            {
                SettleSynthesisCost settleCost = this.context.SettleSynthesisCost.Find(id);
                if (settleCost == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetID = settleCost.PlanTargetID.Value;
                planTargetActionID = settleCost.PlanTargetActionID.Value;
                subActionPlanID = settleCost.SubPlanActionID;
                subSpendItemID = settleCost.SubActionSpendItemID;
                parentID = settleCost.SettleSynthesisCostID;
            }
            for (int i = 0; i < spendContents.Length; i++)
            {
                SettleSynthesisCost nowCost = new SettleSynthesisCost();
                nowCost.SettleSynthesisID = synID;
                nowCost.PlanTargetID = planTargetID;
                nowCost.PlanTargetActionID = planTargetActionID;
                nowCost.SubPlanActionID = subActionPlanID;
                nowCost.SubActionSpendItemID = subSpendItemID;
                nowCost.ParentID = parentID;
                nowCost.ActionCode = spendCodes[i];
                nowCost.ActionTitle = spendContents[i];
                this.context.SettleSynthesisCost.Add(nowCost);
                listSettleCost.Add(nowCost);
            }
            this.context.SaveChanges();
            ViewData["listSettleCost"] = listSettleCost.Select(x => new SettleSynCostModel
            {
                SettleSynthesisCostID = x.SettleSynthesisCostID,
                SettleSynthesisID = x.SettleSynthesisID,
                ActionCode = x.ActionCode,
                ActionTitle = x.ActionTitle,
                CostUpdateType = x.UpdateType,
                CostUpdateUserID = x.UpdateUserID,
                PlanTargetID = x.PlanTargetID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubPlanActionID = x.SubPlanActionID,
                SubActionSpendItemID = x.SubActionSpendItemID
            }).ToList();
            return PartialView("_ListSubCostInsert");
        }

        public JsonResult DelSpendItem(long id)
        {
            SettleSynthesisCost entity = this.context.SettleSynthesisCost.Find(id);
            if (entity == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            ValidateEditRole(entity.SettleSynthesisID);
            this.context.SettleSynthesisCost.Remove(entity);
            this.context.SaveChanges();
            return Json(new { Type = "SUCCESS", Message = "Xóa hạng mục chi thành công" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(SettleCostData data, int PlanTargetID)
        {
            ValidateEditRole(data.SettleSynthesisID);
            SettleSynthesisBusiness bus = new SettleSynthesisBusiness(this.context);
            bus.Save(data, PlanTargetID);
            return Json("Cập nhật thông tin quyết toán thành công", JsonRequestBehavior.AllowGet);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}