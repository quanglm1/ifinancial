﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReviewActionReportController : Controller
    {
        // GET: Action
        private Entities context = new Entities();
        public ActionResult Index()
        {
            var listPlan = (from pr in this.context.Period
                            where pr.Status == Constants.IS_ACTIVE
                            orderby pr.FromDate descending
                            select new
                            {
                                pr.PeriodID,
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listPlanItem = new List<SelectListItem>();
            listPlanItem.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            listPlan.ForEach(x =>
            {
                listPlanItem.Add(new SelectListItem { Value = x.PeriodID.ToString(), Text = x.PeriodName });
            });
            ViewData["listPlan"] = listPlanItem;
            List<SelectListItem> listYear = new List<SelectListItem>();
            listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i < maxYear; i++)
                {
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                }
            }
            ViewData["listYear"] = listYear;
            // Lay danh sach don vi quan ly
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<SelectListItem> listDept = (from m in this.context.UserManageDepartment
                                             join d in this.context.Department on m.DepartmentID equals d.DepartmentID
                                             where m.UserID == userID && d.Status == Constants.IS_ACTIVE
                                             orderby d.DepartmentName
                                             select new SelectListItem
                                             {
                                                 Value = d.DepartmentID + "",
                                                 Text = d.DepartmentName
                                             }).ToList();
            listDept.Insert(0, new SelectListItem { Value = "0", Text = "---Tất cả---" });
            ViewData["listDept"] = listDept;
            return View();
        }

        public ActionResult AddOrEdit(long? id)
        {
            ActionReportInfoModel model = null;
            if (id != null)
            {
                var entity = (from s in this.context.ActionReport
                              join p in this.context.Plan on s.PlanID equals p.PlanID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              where s.ActionReportID == id && s.Status >= Constants.PLAN_NEXT_LEVER
                              select new
                              {
                                  s.ActionReportID,
                                  s.Year,
                                  s.Quarter,
                                  s.PlanID,
                                  s.Status,
                                  pr.PeriodName,
                                  d.DepartmentName,
                                  s.Advantage,
                                  s.Difficulty,
                                  s.Propose,
                                  s.SendDate
                              }).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                model = new ActionReportInfoModel();
                model.ActionReportID = entity.ActionReportID;
                model.Year = entity.Year;
                model.Quarter = entity.Quarter;
                model.PlanID = entity.PlanID;
                model.PlanName = entity.PeriodName;
                model.Status = entity.Status;
                model.DeptName = entity.DepartmentName;
                model.Advantage = entity.Advantage;
                model.Difficulty = entity.Difficulty;
                model.Propose = entity.Propose;
                model.SendDate = entity.SendDate;
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            UserInfo userInfo = SessionManager.GetUserInfo();

            if (id != null)
            {
                List<TargetModel> listTarget = (from pt in this.context.PlanTarget
                                                join t in this.context.Target on pt.TargetID equals t.TargetID
                                                where pt.PlanID == model.PlanID && t.Status == Constants.IS_ACTIVE && pt.Status == Constants.IS_ACTIVE
                                                orderby t.TargetOrder
                                                select new TargetModel
                                                {
                                                    TargetID = t.TargetID,
                                                    TargetTitle = t.TargetTitle,
                                                    TargetContent = t.TargetContent
                                                }).ToList();
                ViewData["listTarget"] = listTarget;
                // File dinh kem
                List<ActionReportFileModel> listActionReportFile = this.context.ActionReportFile.Where(x => x.ActionReportID == id.Value)
                .Select(x => new ActionReportFileModel
                {
                    ActionReportFileID = x.ActionReportFileID,
                    ActionReportID = x.ActionReportID,
                    DocName = x.DocName,
                    FileName = x.FileName,
                    FileSize = x.FileSize
                }).ToList();
                ViewData["listActionReportFile"] = listActionReportFile;
            }
            return View("AddOrEdit", model);
        }
        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }

        public PartialViewResult LoadActionInfo(int actionReportID, int targetID, int isView = 0)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.TargetID == targetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            List<ActionReportDetailModel> listDetail = bus.GetListAction(0, targetID, actionReportID);
            ViewData["listDetail"] = listDetail;
            ActionReportDetail detail = this.context.ActionReportDetail.Where(x => x.ActionReportID == actionReportID
                    && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            // Neu la bao cao nam lay them thong tin chi so
            if (entity.Quarter == null)
            {
                List<TargetIndexModel> lstTargetIndex = bus.GetActionReportTargetIndex(actionReportID, targetID)
                    .Select(x => new TargetIndexModel
                    {
                        TypeIndex = x.TypeIndex,
                        ValueIndex = x.ValueIndex,
                        ContentIndex = (x.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_CHOICE ?
                        x.ContentIndex : x.PlanValueIndex + " " + x.ContentIndex)
                    }).ToList();
                ViewData["listTargetIndexChoosen"] = lstTargetIndex;
                if (detail != null)
                {
                    ViewData["mainResult"] = detail.TargetResult;
                }

            }
            ViewData["actionReport"] = entity;
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.ActionReportDetailID && c.Type == Constants.COMMENT_TYPE_ACTION
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.ActionReportDetailID;
            }
            if (isView == 0)
            {
                return PartialView("_ActionInfo");
            }
            else
            {
                return PartialView("_ActionInfoView");
            }
        }

        public PartialViewResult OpenIndexPopup(int actionReportID, int targetID, int page = 1)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<ActionReportIndexModel> listActionReportIndex = bus.GetActionReportTargetIndex(actionReportID, targetID);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.TargetID == targetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            Paginate<ActionReportIndexModel> paginate = bus.GetReportIndex(actionReportID, targetID, page);
            ViewData["listTargetIndex"] = paginate;
            ViewData["targetName"] = pt.PlanTargetContent;
            return PartialView("_PopupTargetIndex");
        }

        public PartialViewResult LoadReportIndex(int actionReportID, int targetID)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<TargetIndexModel> lstTargetIndex = bus.GetActionReportTargetIndex(actionReportID, targetID)
                    .Select(x => new TargetIndexModel
                    {
                        TypeIndex = x.TypeIndex,
                        ValueIndex = x.ValueIndex,
                        ContentIndex = (x.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_CHOICE ?
                        x.ContentIndex : x.PlanValueIndex + " " + x.ContentIndex)
                    }).ToList();
            ViewData["listTargetIndexChoosen"] = lstTargetIndex;
            return PartialView("_TargetIndexView");
        }

        public JsonResult ChoiceIndex(List<IndexMappingBO> TargetIndexIDs, int TargetID, int ActionReportID, int Page=1)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.SaveReportTargetIndex(TargetIndexIDs, TargetID, ActionReportID, Page);
            return Json("Cập nhật chỉ số mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult Approve(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(reportID, Constants.REPORT_TYPE_ACTION, Constants.IS_ACTIVE);
            return Json("Phê duyệt báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Unlock(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(reportID, Constants.REPORT_TYPE_ACTION, Constants.IS_NOT_ACTIVE);
            return Json("Mở khóa báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnApprove(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(reportID, Constants.REPORT_TYPE_ACTION, Constants.IS_NOT_ACTIVE);
            return Json("Hủy phê duyệt báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Agree(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(reportID, Constants.REPORT_TYPE_ACTION, Constants.IS_ACTIVE);
            return Json("Gửi báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult Search(int PeriodID, int Year, int DeptID, int Status, int Quarter, int page = 1)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            ViewData["listResult"] = new ActionReportBusiness(this.context).GetListActionReport(userID, PeriodID, DeptID, Status, Quarter, Year, page);
            return PartialView("_ListResult");
        }

        public PartialViewResult LoadReportDetail(int planTargetActionID, int targetID, long actionReportID, int isView = 0)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<ActionReportDetailModel> listDetail = bus.GetListAction(planTargetActionID, targetID, actionReportID);
            ViewData["listDetail"] = listDetail;
            if (isView == 0)
            {
                return PartialView("_ListDetail");
            }
            else
            {
                return PartialView("_ListDetailView");
            }
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, long actionReportID)
        {
            ActionReport entityactionReport = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entityactionReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID
            && x.ActionContent != null && x.ActionContent.Trim() != "")
                .Select(x => new PlanTargetActionModel
                {
                    PlanID = x.PlanID.Value,
                    PlanTargetID = x.PlanTargetID.Value,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ActionReportData data)
        {
            ActionReportBusiness bus = new Business.ActionReportBusiness(this.context);
            bus.Save(data);
            return Json("Cập nhật báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadActionReportFile(string actionReportFileID)
        {
            ActionReportFile att = this.context.ActionReportFile.Find(actionReportFileID);
            if (att == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            return File(att.FileContent, System.Net.Mime.MediaTypeNames.Application.Octet, att.FileName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}