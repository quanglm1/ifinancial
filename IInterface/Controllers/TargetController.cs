﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class TargetController : Controller
    {
        private readonly Entities _context = new Entities();
        // GET: Target
        [BreadCrumb(ControllerName = "Mục tiêu", AreaName = "Danh mục")]
        public ActionResult Index(int? type)
        {
            ViewData["typeDept"] = type;
            return View();
        }
        public PartialViewResult Search(string TargetTitleSearch = null, string TargetContentSearch = null, int? typeDept = 0, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IQueryable<TargetModel> IQSearch =
                from x in this._context.Target
                where x.Status != Constants.IS_NOT_ACTIVE
                && x.Type == typeDept
                select new TargetModel
                {
                    TargetID = x.TargetID,
                    Type = x.Type,
                    TargetTitle = x.TargetTitle,
                    TargetOrder = x.TargetOrder,
                    TargetContent = x.TargetContent,
                    ParentID = x.ParentID,
                    Status = x.Status
                };
            if (!string.IsNullOrWhiteSpace(TargetTitleSearch))
            {
                IQSearch = IQSearch.Where(x => x.TargetTitle.ToLower().Contains(TargetTitleSearch.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(TargetContentSearch))
            {
                IQSearch = IQSearch.Where(x => x.TargetContent.ToLower().Contains(TargetContentSearch.Trim().ToLower()));
            }


            Paginate<TargetModel> paginate = new Paginate<TargetModel> { PageSize = Constants.PAGE_SIZE };

            List<TargetModel> lst = null;
            if (IQSearch.Any())
            {
                lst = IQSearch.OrderBy(x => x.TargetOrder).ThenBy(o => o.TargetTitle)
                   .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
                foreach (var item in lst)
                {
                    if (item.ParentID > 0)
                    {
                        var target = _context.Target.Find(item.ParentID);
                        if (target != null)
                            item.ParentName = target.TargetTitle;
                    }
                }
            }
            paginate.List = lst;
            paginate.Count = IQSearch.Count();


            ViewData["listResult"] = paginate;
            ViewData["typeDeptView"] = typeDept;
            return PartialView("_ListResult");
        }
        public PartialViewResult PrepareAddOrEdit(int? id, int? typeDept)
        {
            TargetModel model = new TargetModel();

            if (id.HasValue && id.Value > 0)
            {
                model = this._context.Target
                    .Where(x => x.TargetID == id && x.Status != Constants.IS_NOT_ACTIVE)
                    .Select(x => new TargetModel
                    {
                        TargetID = x.TargetID,
                        Type = x.Type,
                        TargetTitle = x.TargetTitle,
                        TargetOrder = x.TargetOrder,
                        TargetContent = x.TargetContent,
                        ParentID = x.ParentID,
                        Status = x.Status,
                        DepartmentType = x.DepartmentType
                    })
                    .FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại mục tiêu đã chọn");
                }
            }
            else
            {
                model.Type = typeDept;
            }
            List<Target> listTarget = _context.Target.Where(o => o.Status == Constants.IS_ACTIVE && o.Type == Constants.MAIN_TYPE).ToList();
            List<SelectListItem> listParent = listTarget.Select(
                    o => new SelectListItem
                    {
                        Value = o.TargetID.ToString(),
                        Text = o.TargetTitle + "-" + o.TargetContent
                    }).ToList();

            ViewData["listParent"] = listParent;
            if (listParent.Any())
            {
                ViewData["listMainAction"] = GetLstMainAction(listTarget.First().TargetID);
            }
            List<SelectListItem> listDepartmentType = Utils.GetDepartmentType().ToList();
            if (typeDept.GetValueOrDefault() == Constants.SUB_TYPE)
            {
                listDepartmentType.RemoveAll(x => x.Value.Equals(Constants.DEPT_TYPE_PBQUY.ToString()));
            }
            else
            {
                listDepartmentType.RemoveAll(x => !x.Value.Equals(Constants.DEPT_TYPE_PBQUY.ToString()));
            }
            ViewData["listDeptType"] = listDepartmentType;
            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(TargetModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                if (model.TargetID > 0)
                {
                    Target entity = this._context.Target.FirstOrDefault(x => x.TargetID == model.TargetID && x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại mục tiêu đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật thành công";
                }
                else
                {
                    Target entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    this._context.Target.Add(entity);
                    msg = "Thêm mới thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public JsonResult SaveAction(int hidAddTargetID, int ActionId)
        {
            var target = _context.Target.Find(hidAddTargetID);
            if (target == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            if (target.Type != Constants.SUB_TYPE)
            {
                throw new BusinessException("Chỉ ánh xạ mục tiêu đơn vị");
            }
            var action = _context.MainAction.Find(ActionId);
            if (action == null)
            {
                throw new BusinessException("Không tồn tại hoạt động đã chọn");
            }
            if (action.Type != Constants.MAIN_TYPE)
            {
                throw new BusinessException("Chỉ ánh xạ với hoạt động quỹ");
            }
            target.MainActionID = ActionId;
            this._context.SaveChanges();
            return Json("Cập nhật hoạt động cho mục tiêu đơn vị thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveAction(int targetID)
        {
            var obj = _context.Target.Find(targetID);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            if (obj.Type != Constants.SUB_TYPE)
            {
                throw new BusinessException("Chỉ ánh xạ mục tiêu đơn vị");
            }
            obj.MainActionID = null;
            this._context.SaveChanges();

            return Json("Xóa cấu hình thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var obj = _context.Target.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            try
            {
                obj.Status = Constants.IS_NOT_ACTIVE;
                this._context.SaveChanges();
            }
            catch
            {
                throw new BusinessException("Không được xóa mục tiêu đã chọn");
            }

            return Json("Xóa mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetLstMainAction(int? targetId)
        {
            var res = new List<SelectListItem>();

            res = _context.MainAction.Where(o => o.TargetID == targetId && o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.ActionCode + "-" + o.ActionContent,
                Value = o.TargetID.ToString()
            }).ToList();

            return res;
        }

        public PartialViewResult LoadTargetAction(int id)
        {
            var obj = _context.Target.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            ViewData["targetName"] = obj.TargetContent;
            List<TargetModel> listTarget = this._context.Target.Where(x => x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE)
                .Select(x => new TargetModel
                {
                    TargetTitle = x.TargetTitle,
                    TargetContent = x.TargetContent,
                    TargetID = x.TargetID,
                    TargetOrder = x.TargetOrder,
                }).ToList();
            ViewData["listTarget"] = listTarget;
            int firstTartgetID = (listTarget != null && listTarget.Any()) ? listTarget.First().TargetID : 0;
            int hasMainTarget = Constants.IS_NOT_ACTIVE;
            if (obj.MainActionID != null)
            {
                var mappingAction = _context.MainAction.Find(obj.MainActionID.Value);
                firstTartgetID = mappingAction.TargetID.Value;
                hasMainTarget = Constants.IS_ACTIVE;
            }
            ViewData["mainTargetID"] = firstTartgetID;
            ViewData["hasMainTarget"] = hasMainTarget;
            if (firstTartgetID > 0)
            {
                List<MainActionModel> lstAction = this.GetListMainActionForAddAction(firstTartgetID, id);
                ViewData["lstAction"] = lstAction;
            }
            ViewData["subTargetID"] = id;
            return PartialView("_ListTargetAction");
        }

        public PartialViewResult ViewListActionByTarget(int targetId, int id)
        {
            Target entity = _context.Target.FirstOrDefault(x => x.TargetID == targetId
            && x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }

            List<MainActionModel> lstAction = this.GetListMainActionForAddAction(targetId, id);
            ViewData["lstAction"] = lstAction;
            return PartialView("_ListAction");
        }

        private List<MainActionModel> GetListMainActionForAddAction(int targetId, int id)
        {
            var obj = _context.Target.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            var selectedMainActionID = obj.MainActionID.GetValueOrDefault();
            List<MainActionModel> lstAction = _context.MainAction.Where(x => x.TargetID == targetId
                 && x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE)
                .Select(x => new MainActionModel
                {
                    TargetID = x.TargetID,
                    MainActionID = x.MainActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    IsSelected = (x.MainActionID == selectedMainActionID)
                }).ToList();
            return lstAction;
        }


        public JsonResult GetLstTargetByDept(int? deptType)
        {
            var res = _context.Target.Where(o => o.DepartmentType == deptType && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.TargetTitle + "-" + o.TargetContent,
                Value = o.TargetID.ToString()
            }).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}