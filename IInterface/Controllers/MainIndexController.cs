﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IModel;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;

namespace IInterface.Controllers
{
    public class MainIndexController : Controller
    {
        private readonly Entities _context = new Entities();
        // GET: MainIndex
        [BreadCrumb(ActionName = "Chỉ số", ControllerName = "Chỉ số", AreaName = "Danh mục")]
        public ActionResult Index()
        {
            var listDeptId = GetlistDeptType();
            listDeptId.Insert(0, new SelectListItem
            {
                Text = "-- Tất cả --",
                Value = "",
            });
            var lstTarget = new List<SelectListItem>();
            lstTarget.Add(new SelectListItem
            {
                Text = "-- Tất cả --",
                Value = "",
            });

            ViewBag.TargetID = lstTarget;
            ViewBag.DeptID = listDeptId;
            return View();
        }
        public PartialViewResult Search(int? DeptID = null, int? TargetID = 0, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IEnumerable<MainIndexModel> iqSearch = (from x in this._context.MainIndex
                                                    join y in _context.Target on x.TargetID equals y.TargetID
                                                    where x.Status != Constants.IS_NOT_ACTIVE
                                                    select new MainIndexModel
                                                    {
                                                        ContentIndex = x.ContentIndex,
                                                        MainIndexID = x.MainIndexID,
                                                        TargetName = y.TargetTitle,
                                                        TypeIndex = x.TypeIndex,
                                                        Type = y.Type,
                                                        CreateTime = x.CreateTime,
                                                        TargetID = x.TargetID,
                                                        OrderNumber = x.OrderNumber,
                                                    }).AsEnumerable();

            if (DeptID != null)
            {
                iqSearch = iqSearch.Where(x => x.Type == DeptID);
            }

            if (TargetID != 0)
            {
                iqSearch = iqSearch.Where(x => x.TargetID == TargetID);
            }

            Paginate<MainIndexModel> paginate = new Paginate<MainIndexModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = iqSearch.OrderBy(x => x.TargetID).ThenBy(o => o.OrderNumber).Skip((page - 1) * Constants.PAGE_SIZE)
                    .Take(Constants.PAGE_SIZE).ToList(),
                Count = iqSearch.Count()
            };
            ViewData["listResult"] = paginate;
            return PartialView("_LstResult");
        }
        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            MainIndexModel model = new MainIndexModel();
            UserInfo objUser = SessionManager.GetUserInfo();
            var lstDeptType = GetlistDeptType();
            ViewData["ListType"] = GetListType();
            ViewData["ListDeptType"] = lstDeptType;
            var mainIndexes = _context.MainIndex.Where(x => x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE).ToArray();
            List<SelectListItem> listIndex = new List<SelectListItem>();
            listIndex.Add(new SelectListItem
            {
                Text = "-- Chọn --",
                Value = "",
            });
            foreach (var i in mainIndexes)
            {
                listIndex.Add(new SelectListItem
                {
                    Value = i.MainIndexID.ToString(),
                    Text = i.ContentIndex
                });
            }
            ViewData["ListIndex"] = listIndex;


            if (id.HasValue && id.Value > 0)
            {
                model = this._context.MainIndex
                    .Where(x => x.MainIndexID == id && x.Status != Constants.IS_NOT_ACTIVE)
                    .Select(x => new MainIndexModel
                    {
                        Type = x.Type,
                        MainIndexID = x.MainIndexID,
                        Status = x.Status,
                        TargetID = x.TargetID,
                        Creator = objUser.UserName,
                        ContentIndex = x.ContentIndex,
                        TypeIndex = x.TypeIndex,
                        OrderNumber = x.OrderNumber,
                        MappingIndexID = x.MappingIndexID,
                        IndexValue = x.IndexValue
                    })
                    .FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại chỉ số đã chọn");
                }

                ViewData["ListTarget"] = GetLstTarget(model.Type);
            }
            else
            {
                if (lstDeptType.Count > 0)
                {
                    ViewData["ListTarget"] = GetLstTarget(Convert.ToInt32(lstDeptType[0].Value));
                }
            }

            return PartialView("_AddOrEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(MainIndexModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            string msg;
            if (ModelState.IsValid)
            {
                if (model.MainIndexID > 0)
                {
                    MainIndex entity = this._context.MainIndex.FirstOrDefault(x => x.MainIndexID == model.MainIndexID && x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại chỉ số đã chọn");
                    }
                    model.Creator = entity.Creator;
                    model.UpdateEntity(entity);
                    msg = "Cập nhật thành công";
                }
                else
                {
                    MainIndex entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    entity.CreateTime = DateTime.Now;
                    entity.Status = Constants.IS_ACTIVE;
                    entity.Creator = user != null ? user.UserName : "";
                    this._context.MainIndex.Add(entity);
                    msg = "Thêm mới thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            MainIndex obj = _context.MainIndex.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại chỉ số đã chọn");
            }

            if (_context.PlanTargetIndex.Any(o => o.MainIndexID == id))
            {
                throw new BusinessException("Không thể xóa chỉ số đã chọn");
            }
            if (_context.PlanTargetIndex.Any(x => x.MainIndexID == id))
            {
                throw new BusinessException("Chỉ số đã được sử dụng trong kế hoạch, không thể xóa");
            }
            _context.MainIndex.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa chỉ số thành công", JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetListType()
        {
            List<SelectListItem> listType = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_CHOICE,
                    Value = Constants.TYPE_INDEX_CHOICE.ToString()
                },
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_INPUT,
                    Value = Constants.TYPE_INDEX_INPUT.ToString()
                },
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_AVERAGE,
                    Value = Constants.TYPE_INDEX_INPUT_AVERAGE.ToString()
                }
            };
            return listType;
        }

        private List<SelectListItem> GetlistDeptType()
        {
            List<SelectListItem> listType = new List<SelectListItem>();
            listType.Add(new SelectListItem { Text = Constants.DEPT_MAIN_TYPE_NAME, Value = Constants.MAIN_TYPE.ToString() });
            listType.Add(new SelectListItem { Text = Constants.DEPT_SUB_TYPE_NAME, Value = Constants.SUB_TYPE.ToString() });
            return listType;
        }

        [HttpPost]
        public JsonResult LoadListTarget(int? type)
        {
            var res = GetLstTarget(type);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetLstTarget(int? type)
        {
            var res = _context.Target.Where(o => o.Type == type && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.TargetTitle + "-" + o.TargetContent,
                Value = o.TargetID.ToString()
            }).ToList();

            return res;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
