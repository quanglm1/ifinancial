﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class PlanHistoryController : Controller
    {
        private Entities context = new Entities();

        [BreadCrumb(ControllerName = "Thông tin lịch sử", ActionName = "Chi tiết kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult Step(long PlanHisID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            PlanHis objPlan = context.PlanHis.Find(PlanHisID);
            Department objDepartment = context.Department.Find(objPlan.DepartmentID);
            Period objPeriod = (from a in context.Period
                                where a.PeriodID == objPlan.PeriodID
                                select a).FirstOrDefault();

            PlanModel model = new PlanModel();
            model.PlanHisID = objPlan.ID;
            model.PeriodID = objPlan.PeriodID;
            model.Type = objUser.DeptType;
            //Department
            model.DepartmentID = objDepartment.DepartmentID;
            model.DepartmentName = objDepartment.DepartmentName;
            model.Address = objDepartment.Address;
            model.PhoneNumber = objDepartment.PhoneNumber;
            model.Fax = objDepartment.Fax;
            model.AccountNumber = objDepartment.AccountNumber;
            model.BankName = objDepartment.BankName;
            model.RepresentPersonName = objDepartment.RepresentPersonName;
            model.RepresentPosition = objDepartment.RepresentPosition;
            model.RepresentAddress = objDepartment.RepresentAddress;
            model.RepresentPhone = objDepartment.RepresentPhone;
            model.RepresentEmail = objDepartment.RepresentEmail;
            model.ContactPersonName = objDepartment.ContactPersonName;
            model.ContactPosition = objDepartment.ContactPosition;
            model.ContactAddress = objDepartment.ContactAddress;
            model.ContactPhone = objDepartment.ContactPhone;
            model.ContactEmail = objDepartment.ContactEmail;
            model.FromDate = objPeriod.FromDate;
            model.ToDate = objPeriod.ToDate;
            model.Status = Constants.PLAN_NEW;

            if (objPlan != null)
            {
                model.PlanID = (int)objPlan.PlanID;
                model.PlanContent = objPlan.PlanContent;
                model.PlanTarget = objPlan.PlanTarget;
                model.RepresentPersonName = objPlan.RepresentPersonName;
                model.RepresentPosition = objPlan.RepresentPosition;
                model.RepresentAddress = objPlan.RepresentAddress;
                model.RepresentPhone = objPlan.RepresentPhone;
                model.RepresentEmail = objPlan.RepresentEmail;
                model.ContactPersonName = objPlan.ContactPersonName;
                model.ContactPosition = objPlan.ContactPosition;
                model.ContactAddress = objPlan.ContactAddress;
                model.ContactPhone = objPlan.ContactPhone;
                model.ContactEmail = objPlan.ContactEmail;
                model.FromDate = objPlan.FromDate;
                model.ToDate = objPlan.ToDate;
                model.Status = objPlan.Status;
            }
            else
            {
                model.Role = Constants.ROLE_EDIT;
                model.PL = null;
            }
            PlanTargetHis pt = context.PlanTargetHis
               .Where(a => a.PlanID == objPlan.PlanID && a.PlanHisID == PlanHisID)
               .OrderBy(x => x.PlanTargetID).FirstOrDefault();
            if (pt != null)
            {
                ViewData["FirstPlanTargetID"] = pt.PlanTargetID;
            }
            ViewData["PlanHisID"] = model.PlanHisID;
            return View(model);
        }

        public PartialViewResult Step1Target(long PlanHisID, int PlanTargetID = 0)
        {
            List<PlanTargetModel> PlanTargetModels = context.PlanTargetHis
               .Where(a => a.PlanHisID == PlanHisID)
               .Select(a => new PlanTargetModel
               {
                   PlanTargetID = a.PlanTargetID,
                   PlanTargetTitle = a.PlanTargetTitle,
                   PlanTargetContent = a.PlanTargetContent
               }).ToList();
            ViewData["ListTarget"] = PlanTargetModels;
            string targetTitle = string.Empty;
            if(PlanTargetID > 0)
            {
                PlanTargetHis th = context.PlanTargetHis.Where(x => x.PlanHisID == PlanHisID && x.PlanTargetID == PlanTargetID).FirstOrDefault();
                if(th != null)
                {
                    targetTitle = th.PlanTargetTitle;
                }
            }
            if (PlanTargetID == 0 && PlanTargetModels.Any())
            {
                PlanTargetID = PlanTargetModels.FirstOrDefault().PlanTargetID;
                targetTitle = PlanTargetModels.FirstOrDefault().PlanTargetTitle;
            }
            if (PlanTargetID > 0)
            {
                PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                ViewData["ListIndex"] = bus.GetListTargetIndex(PlanTargetID, PlanHisID);
            }
            ViewData["PlanTargetID"] = PlanTargetID;
            ViewData["targetTitle"] = targetTitle;
            return PartialView("_Step2Target");
        }
        public PartialViewResult Step1Indexs(int PlanHisID, int PlanTargetID)
        {
            PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
            ViewData["ListIndex"] = bus.GetListTargetIndex(PlanTargetID, PlanHisID);
            return PartialView("_Step2Indexs");
        }
        public PartialViewResult Step2Action(long PlanHisID, int PlanTargetID)
        {
            if (PlanTargetID == 0)
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu");
            }
            PlanTargetHis objPlanTarget = context.PlanTargetHis.Where(a => a.PlanHisID == PlanHisID && a.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (objPlanTarget == null)
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu");
            }
            PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
            ViewData["ListActions"] = bus.GetListActionOfTarget(PlanTargetID, PlanHisID); ;
            ViewData["AllSubActions"] = bus.GetSubAction(PlanHisID);
            ViewData["AllSubIndexs"] = bus.GetSubIndex(PlanHisID);
            return PartialView("_Step3Action");
        }

        public PartialViewResult ViewCost(long PlanHisID)
        {
            PlanHis plan = context.PlanHis.Where(x => x.ID == PlanHisID).FirstOrDefault();
            if (plan != null)
            {
                Period period = context.Period.Find(plan.PeriodID);
                int startYear = period.FromDate.Year;
                int finishYear = period.ToDate.Year;
                int rangeYear = finishYear - startYear;

                ViewData["Period"] = period;
                ViewData["RangeYear"] = rangeYear;
                PlanCostBusiness bus = new PlanCostBusiness(context);
                List<PlanTargetModel> lstPlanTarget = bus.GetListCostHis(PlanHisID);
                ViewData["PlanTarget"] = lstPlanTarget;
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng tạo kế hoạch với thông tin chung trước");
            }
            return PartialView("_ListSubActionPlanCost");
        }

        public PartialViewResult ViewTime(long PlanHisID)
        {
            PlanHis plan = context.PlanHis.Where(x => x.ID == PlanHisID).FirstOrDefault();
            if (plan != null)
            {
                Period period = context.Period.Find(plan.PeriodID);
                int startYear = period.FromDate.Year;
                int finishYear = period.ToDate.Year;
                int rangeYear = finishYear - startYear;

                ViewData["Period"] = period;
                ViewData["RangeYear"] = rangeYear;

                List<PlanTargetModel> lstPlanTarget = GetPlanTargetByPlan(plan.PlanID);
                ViewData["PlanTarget"] = lstPlanTarget;
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng tạo kế hoạch với thông tin chung trước");
            }
            return PartialView("_ListSubActionPlanTime");
        }

        public PartialViewResult PrepareImplementation(long PlanHisID)
        {
            PlanModel model = new PlanModel();
            PlanHis entity = context.PlanHis.Find(PlanHisID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            model.PlanHisID = entity.ID;
            model.Implementation = entity.Implementation;
            return PartialView("_Implementation", model);
        }

        #region Lay thong tin thoi gian
        public List<PlanTargetModel> GetPlanTargetByPlan(long planHisID)
        {
            List<PlanTargetModel> lstPlanTarget = context.PlanTargetHis.Where(x => x.PlanHisID == planHisID)
                .Select(x => new PlanTargetModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    TargetID = x.TargetID,
                    PlanID = x.PlanID,
                    DepartmentID = x.DepartmentID
                })
                .ToList();
            foreach (PlanTargetModel pt in lstPlanTarget)
            {
                List<PlanTargetActionModel> lst = GetPlanTargetAction(pt.PlanID, pt.PlanTargetID);


                foreach (PlanTargetActionModel pta in lst)
                {
                    List<SubActionPlanModel> lstSubAction = GetSubActionPlanByPlanTargetId(planHisID, pta.PlanTargetActionID);
                    pta.ListSubActionPlan = lstSubAction;
                }
                pt.ListPlanTargetActionModel = lst;
            }

            return lstPlanTarget;
        }

        public List<PlanTargetActionModel> GetPlanTargetAction(long planHisID, int? planTargetID)
        {
            List<PlanTargetActionModel> lstPlanTargetAction =
                context.PlanTargetActionHis.Where(x => x.PlanHisID == planHisID && x.PlanTargetID == planTargetID)
                .Select(x => new PlanTargetActionModel
                {
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note

                }).ToList();

            foreach (PlanTargetActionModel pta in lstPlanTargetAction)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = context.SubActionPlanTimeHis
                           .Where(x => x.PlanHisID == planHisID && x.PlanTargetActionID == pta.PlanTargetActionID && x.SubActionPlanID == null)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               PlanTargetAcionID = x.PlanTargetActionID,
                               PlanTime = x.PlanTime,
                               PlanYear = x.PlanYear,
                               Executer = x.Executer,
                               Supporter = x.Supporter,
                               Note = x.Note,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID

                           })
                           .ToList();
                pta.ListSubActionPlanTime = lstSubActionPlanTime;

            }

            return lstPlanTargetAction;
        }
        // Lay thong tin SubAction
        public List<SubActionPlanModel> GetSubActionPlanByPlanTargetId(long planHisID, int PlanTargetActionID)
        {
            List<SubActionPlanModel> lstSubActionPlan = context.SubActionPlanHis.Where(x => x.PlanHisID == planHisID && x.PlanTargetActionID == PlanTargetActionID && x.ParentID == null)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = context.SubActionPlanTimeHis
                           .Where(x => x.PlanHisID == planHisID && x.SubActionPlanID == sal.SubActionPlanID)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               SubActionPlanID = x.SubActionPlanID,
                               PlanTime = x.PlanTime,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID

                           })
                           .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
                sal.ListChildSubActionPlan = GetListChildSubActionPlanBySubActionId(planHisID, sal.SubActionPlanID.Value);
            }

            return lstSubActionPlan;
        }

        public List<SubActionPlanModel> GetListChildSubActionPlanBySubActionId(long planHisID, int subActionPlanId)
        {
            List<SubActionPlanModel> lstSubActionPlan = context.SubActionPlanHis.Where(x => x.PlanHisID == planHisID && x.ParentID == subActionPlanId)
                .Select(x => new SubActionPlanModel
                {
                    SubActionPlanID = x.SubActionPlanID,
                    SubActionContent = x.SubActionContent,
                    PlanTargetActionID = x.PlanTargetActionID,
                    SubActionCode = x.SubActionCode,
                    Exucuter = x.Exucuter,
                    Supporter = x.Supporter,
                    Note = x.Note,
                    ParentID = x.ParentID

                }).ToList();
            foreach (SubActionPlanModel sal in lstSubActionPlan)
            {
                List<SubActionPlanTimeModel> lstSubActionPlanTime = context.SubActionPlanTimeHis
                           .Where(x => x.PlanHisID == planHisID && x.SubActionPlanID == sal.SubActionPlanID)
                           .Select(x => new SubActionPlanTimeModel
                           {
                               SubActionPlanTimeID = x.SubActionPlanTimeID,
                               SubActionPlanID = x.SubActionPlanID,
                               PlanTime = x.PlanTime,
                               CreateUserID = x.CreateUserID,
                               ModifierID = x.ModifierID,
                               //   Supporter = x.Supporter,
                               //  Note = x.Note
                           })
                           .ToList();
                sal.ListSubActionPlanTime = lstSubActionPlanTime;
            }
            return lstSubActionPlan;
        }

        #endregion
    }
}