﻿using IInterface.Business;
using DocumentFormat.OpenXml.Wordprocessing;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System.Linq;
using System.Collections.Generic;
using IInterface.Common.OpenXML;
using System.Web.Mvc;
using System;
using System.IO;
using DocumentFormat.OpenXml.Packaging;

namespace IInterface.Controllers
{
    public partial class ActionReportController : ReportController
    {
        public FileResult ExportActionReport(int actionReportID)
        {
            ActionReportInfoModel entity = (from a in this.context.ActionReport
                                            join pl in this.context.Plan on a.PlanID equals pl.PlanID
                                            join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                                            where a.ActionReportID == actionReportID && a.Status != Constants.PLAN_DEACTIVED
                                            select new ActionReportInfoModel
                                            {
                                                ActionReportID = a.ActionReportID,
                                                PlanID = a.PlanID,
                                                DeptName = d.DepartmentName,
                                                Year = a.Year,
                                                Quarter = a.Quarter,
                                                Advantage = a.Advantage,
                                                Difficulty = a.Difficulty,
                                                Propose = a.Propose
                                            }).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo được chọn");
            }
            PlanModel plan = (from pl in this.context.Plan
                              join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                              join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                              where pl.PlanID == entity.PlanID && pl.Status != Constants.PLAN_DEACTIVED
                              select new PlanModel
                              {
                                  PeriodID = pl.PeriodID,
                                  PlanID = pl.PlanID,
                                  DepartmentID = pl.DepartmentID,
                                  DepartmentName = d.DepartmentName,
                                  Address = d.Address,
                                  PhoneNumber = d.PhoneNumber,
                                  Fax = d.Fax,
                                  AccountNumber = d.AccountNumber,
                                  BankName = d.BankName,
                                  RepresentPersonName = pl.RepresentPersonName,
                                  RepresentPosition = pl.RepresentPosition,
                                  RepresentAddress = pl.RepresentAddress,
                                  RepresentPhone = pl.RepresentPhone,
                                  RepresentEmail = pl.RepresentEmail,
                                  ContactPersonName = pl.ContactPersonName,
                                  ContactPosition = pl.ContactPosition,
                                  ContactAddress = pl.ContactAddress,
                                  ContactPhone = pl.ContactPhone,
                                  ContactEmail = pl.ContactEmail,
                                  PlanContent = pl.PlanContent,
                                  FromDate = pr.FromDate,
                                  ToDate = pr.ToDate,
                                  PlanTarget = pl.PlanTarget
                              }).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch được chọn");
            }
            // Lay thong tin kinh phi
            var totalPlanMoney = (from sc in this.context.SubActionPlanCost
                                  join pt in this.context.PlanTarget on sc.PlanTargetID equals pt.PlanTargetID
                                  where pt.PlanID == plan.PlanID && sc.PlanYear == entity.Year && sc.PlanTargetActionID == null
                                  select sc.TotalPrice == null ? 0 : sc.TotalPrice.Value
                                  ).DefaultIfEmpty(0).Sum();
            plan.TotalPlanMoney = totalPlanMoney;
            List<DocData> listData = new List<DocData>();
            listData.Add(new DocData { Table = this.GenHeader(entity) });
            Paragraph p = new Paragraph();
            p.AddCenter();
            p.AddNewLine(2);
            if (entity.Quarter == null)
            {
                p.AddText(text: "BÁO CÁO HOẠT ĐỘNG NĂM " + entity.Year, fontSize: "28", isBold: true);
            }
            else
            {
                p.AddText(text: "BÁO CÁO HOẠT ĐỘNG QUÝ " + Utils.ToRoman(entity.Quarter.Value) + "/NĂM " + entity.Year, fontSize: "28", isBold: true);
            }
            p.AddNewLine();
            listData.Add(new DocData { Paragraph = p });
            listData.AddRange(this.GenActionContent(entity, plan));
            listData.AddRange(this.GenMoreInfo(entity, totalPlanMoney));
            listData.Add(new DocData { Table = this.GenSignArea(plan) });
            byte[] data = this.GenDocPL04(listData);
            return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, "PL04_Bao cao hoat dong_Donvi.docx");
        }

        private byte[] GenDocPL04(List<DocData> listData)
        {
            // Tao file moi tu file cu
            string newFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".docx";
            string newFilePath = Path.Combine(Server.MapPath("~/TempFile"), newFile);
            string templateFilePath = Path.Combine(Server.MapPath("~/Template"), "PL04_Bao cao quy.docx");
            System.IO.File.Copy(templateFilePath, newFilePath);
            using (WordprocessingDocument doc = WordprocessingDocument.Open(newFilePath, true))
            {
                MainDocumentPart mainPart = doc.MainDocumentPart;
                Paragraph firstP = mainPart.Document.Body.GetFirstChild<Paragraph>();
                if (firstP != null)
                {
                    firstP.Remove();
                }
                PageSize pageSize = new PageSize() { Width = (int)16838U, Height = (int)11906U, Orient = PageOrientationValues.Landscape, Code = (int)9U };
                PageMargin pageMargin = new PageMargin() { Top = 907, Right = (int)1134U, Bottom = 851, Left = (int)567U, Header = (int)709U, Footer = (int)709U, Gutter = (int)0U };
                mainPart.Document.Body.AppendChild(pageSize);
                mainPart.Document.Body.AppendChild(pageMargin);
                foreach (DocData d in listData)
                {
                    if (d.Paragraph != null)
                    {
                        mainPart.Document.Body.AppendChild(d.Paragraph);
                    }
                    if (d.Table != null)
                    {
                        mainPart.Document.Body.AppendChild(d.Table);
                    }
                }
                Paragraph pNewLine = new Paragraph();
                mainPart.Document.Body.AppendChild(pNewLine);

                SectionProperties sectionProperties = new SectionProperties() { RsidRPr = "006647C9", RsidR = "003E08D4", RsidSect = "00965A25" };
                FooterReference footerReference = new FooterReference() { Type = HeaderFooterValues.Default, Id = "rId8" };

                Columns columns = new Columns() { Space = "720" };
                DocGrid docGrid = new DocGrid() { LinePitch = 360 };

                sectionProperties.Append(footerReference);
                sectionProperties.Append(columns);
                sectionProperties.Append(docGrid);
                mainPart.Document.Body.AppendChild(sectionProperties);
            }
            return System.IO.File.ReadAllBytes(newFilePath);
        }

        private List<DocData> GenMoreInfo(ActionReportInfoModel actionReport, double totalPlanMoney)
        {
            List<DocData> listData = new List<DocData>();
            Paragraph p = new Paragraph();
            p.AddBoth();
            p.AddNewLine();
            p.AddText(text: "Phần 3. NHỮNG THUẬN LỢI, KHÓ KHĂN TRONG QUÁ TRÌNH THỰC HIỆN", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "3.1. Thuận lợi: ", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            if (!string.IsNullOrWhiteSpace(actionReport.Advantage))
            {
                if (actionReport.Advantage.Trim().StartsWith("-")
                || actionReport.Advantage.Trim().StartsWith("+"))
                {
                    List<Paragraph> listParagraph = this.GenListParagraphFromContent(actionReport.Advantage.Trim(), false);
                    listParagraph.ForEach(x =>
                    {
                        listData.Add(new DocData { Paragraph = x });
                    }); ;
                }
                else
                {
                    p.AddText(text: string.IsNullOrWhiteSpace(actionReport.Advantage) ? string.Empty : actionReport.Advantage.Trim());
                }
            }
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "3.2. Khó khăn: ", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            if (!string.IsNullOrWhiteSpace(actionReport.Difficulty))
            {
                if (actionReport.Difficulty.Trim().StartsWith("-")
                || actionReport.Difficulty.Trim().StartsWith("+"))
                {
                    List<Paragraph> listParagraph = this.GenListParagraphFromContent(actionReport.Difficulty.Trim(), false);
                    listParagraph.ForEach(x =>
                    {
                        listData.Add(new DocData { Paragraph = x });
                    }); ;
                }
                else
                {
                    p.AddText(text: string.IsNullOrWhiteSpace(actionReport.Difficulty) ? string.Empty : actionReport.Difficulty.Trim());
                }
            }
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "3.3. Đề xuất: ", isBold: true);
            if (!string.IsNullOrWhiteSpace(actionReport.Propose))
            {
                if (actionReport.Propose.Trim().StartsWith("-")
                || actionReport.Propose.Trim().StartsWith("+"))
                {
                    List<Paragraph> listParagraph = this.GenListParagraphFromContent(actionReport.Propose.Trim(), false);
                    listParagraph.ForEach(x =>
                    {
                        listData.Add(new DocData { Paragraph = x });
                    }); ;
                }
                else
                {
                    p.AddText(text: string.IsNullOrWhiteSpace(actionReport.Propose) ? string.Empty : actionReport.Propose.Trim());
                }
            }
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "Phần 4. TÌNH HÌNH GIẢI NGÂN", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "-	Tổng kinh phí theo kế hoạch năm được duyệt: " + Utils.FormatMoney(totalPlanMoney));
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            FinancialReport financialReport = this.context.FinancialReport.Where(x => x.PlanID == actionReport.PlanID
             && x.Year == actionReport.Year && x.Quarter == actionReport.Quarter).FirstOrDefault();
            double totalMoneyInReport = financialReport == null ? 0 : new FinancialReportBusiness(context).GetTotalSettleMoney(financialReport, FinancialReportBusiness.GET_TOTAL);
            p.AddText(text: "-	Tổng kinh phí giải ngân (trong kỳ báo cáo): " + Utils.FormatMoney(totalMoneyInReport));
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            double percent = totalPlanMoney == 0 ? 0 : Math.Round(totalMoneyInReport * 100 / totalPlanMoney, 2);
            p.AddText(text: "-	Tỉ lệ giải ngân: " + percent.ToString() + "%");
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "Phần 5. SẢN PHẨM HOẠT ĐỘNG KÈM THEO BÁO CÁO", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "Liệt kê tên các sản phẩm/ tài liệu (nếu có)");
            listData.Add(new DocData { Paragraph = p });
            var listAtt = this.context.ActionReportFile.Where(x => x.ActionReportID == actionReport.ActionReportID)
                .GroupBy(x => x.DocName).Select(x => new
                {
                    DocName = x.Key,
                    CountDoc = x.Count()
                }).ToList();
            foreach (var d in listAtt)
            {
                p = new Paragraph();
                p.AddBoth();
                p.AddText(text: "-	" + d.DocName + " (" + d.CountDoc + " tài liệu)");
                listData.Add(new DocData { Paragraph = p });
            }
            return listData;
        }

        private Table GenSignArea(PlanModel plan)
        {
            Table table = new Table();
            TableProperties tableProperties = new TableProperties();
            TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };
            TableLook tableLook = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };

            tableProperties.Append(tableWidth);
            tableProperties.Append(tableLook);
            table.Append(tableProperties);

            TableRow tr = new TableRow();
            TableCell c1 = new TableCell();
            c1.AddColWidth(50);
            Paragraph p = new Paragraph();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddCenter();
            p.AddText(text: "CÁN BỘ PHỤ TRÁCH", isBold: true);
            p.AddNewLine(5);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.ContactPersonName) ? string.Empty : plan.ContactPersonName.Trim()), isBold: true);
            c1.Append(p);
            tr.Append(c1);
            TableCell c2 = new TableCell();
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "LÃNH ĐẠO PHÊ DUYỆT", isBold: true);
            p.AddNewLine(5);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.RepresentPersonName) ? string.Empty : plan.RepresentPersonName.Trim()), isBold: true);
            c2.Append(p);
            tr.Append(c2);
            table.Append(tr);
            return table;
        }

        private List<DocData> GenActionContent(ActionReportInfoModel actionReport, PlanModel plan)
        {
            List<DocData> listData = new List<DocData>();
            // Thong tin chung
            var listDetail = (from a in this.context.ActionReportDetail
                              join t in this.context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                              join ta in this.context.PlanTargetAction on a.PlanTargetActionID equals ta.PlanTargetActionID into tag
                              from taj in tag.DefaultIfEmpty()
                              join s in this.context.SubActionPlan on a.SubPlanActionID equals s.SubActionPlanID into sg
                              from sj in sg.DefaultIfEmpty()
                              where a.ActionReportID == actionReport.ActionReportID && (a.PlanTargetActionID == null || (a.Result != null && a.Result != ""))
                              orderby t.TargetOrder, taj.ActionOrder, sj.SubActionOrder
                              select new
                              {
                                  PlanTargetActionID = a.PlanTargetActionID,
                                  PlanTargetActionName = taj.ActionContent,
                                  PlanTargetActionCode = taj.ActionCode,
                                  PlanTargetID = t.PlanTargetID,
                                  PlanTargetContent = t.PlanTargetContent,
                                  SubPlanActionID = a.SubPlanActionID,
                                  SubCode = sj.SubActionCode,
                                  SubContent = sj.SubActionContent,
                                  PlanContent = a.PlanContent,
                                  Result = a.Result,
                                  Description = a.Description,
                                  TargetResult = a.TargetResult,
                                  TargetOrder = t.TargetOrder,
                                  ActionOrder = taj.ActionOrder,
                                  SubActionOrder = sj.SubActionOrder
                              }).ToList();
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<ActionReportIndexModel> listReportIndex = bus.GetActionReportTargetIndex(actionReport.ActionReportID);
            var listPlanTarget = listDetail.Where(x => x.PlanTargetActionID == null)
                .OrderBy(x => x.TargetOrder)
                .Select(x => new
                {
                    PlanTargetID = x.PlanTargetID,
                    ActionName = x.PlanTargetContent,
                    Description = x.Description,
                    TargetResult = x.TargetResult,
                    PlanContent = x.PlanContent
                }).OrderBy(x => x.PlanTargetID).ToList();
            #region Thong tin chung
            List<PlanTargetModel> listPlanTargetModel = context.PlanTarget.OrderBy(x => x.TargetOrder).Where(x => x.PlanID == plan.PlanID).Select(x => new PlanTargetModel
            {
                PlanTargetContent = x.PlanTargetContent
            }).ToList();
            listData.AddRange(this.GenGeneralInfo(plan, listPlanTargetModel));
            #endregion
            // Title
            Paragraph p = new Paragraph();
            p.AddBoth();
            p.AddNewLine();
            listData.Add(new DocData { Paragraph = p });
            if (actionReport.Quarter != null)
            {
                p.AddText(text: "Phần 2. BÁO CÁO TIẾN ĐỘ", isBold: true);
                p = new Paragraph();
                p.AddCenter();
                p.AddText(text: "BẢNG CHỈ SỐ HOẠT ĐỘNG", isBold: true, fontSize: "28");
                p.AddNewLine();
                p.AddText(text: "(Lũy kế theo quý)", isBold: true, fontSize: "28");
                listData.Add(new DocData { Paragraph = p });
                listData.AddRange(this.GenActionIndex(actionReport.ActionReportID, plan.PlanID.Value));
            }
            else
            {
                p.AddText(text: "Phần 2. NHỮNG KẾT QUẢ CHÍNH", isBold: true);
                // Add them thong tin
                for (int i = 1; i <= listPlanTarget.Count; i++)
                {
                    var pt = listPlanTarget[i - 1];
                    p = new Paragraph();
                    p.AddBoth();
                    p.AddSpacing(line: "276", before: "60");
                    p.AddIndentation();
                    p.AddText("Mục tiêu " + i + ". " + pt.ActionName, isBold: true, isItalic: true);
                    listData.Add(new DocData { Paragraph = p });
                    // Ket qua chinh
                    if (actionReport.Quarter == null && !string.IsNullOrWhiteSpace(pt.TargetResult))
                    {
                        List<Paragraph> listParagraph = this.GenListParagraphFromContent(pt.TargetResult);
                        listParagraph.ForEach(x =>
                        {
                            listData.Add(new DocData { Paragraph = x });
                        });
                    }
                }
                p = new Paragraph();
                p.AddCenter();
                p.AddText("BẢNG CHỈ SỐ THEO MỤC TIÊU", isBold: true);
                listData.Add(new DocData { Paragraph = p });
                Table tableIndex = this.GenTargetIndex(actionReport.ActionReportID, plan.PlanID.Value);
                listData.Add(new DocData { Table = tableIndex });

                p = new Paragraph();
                p.AddCenter();
                p.AddNewLine();
                p.AddText("BẢNG CHỈ SỐ HOẠT ĐỘNG", isBold: true);
                p.AddNewLine();
                p.AddText("(Tổng kết cả năm)", isBold: true);
                listData.Add(new DocData { Paragraph = p });
                listData.AddRange(this.GenActionIndex(actionReport.ActionReportID, plan.PlanID.Value));
            }
            p = new Paragraph();
            p.AddBoth();
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            p.AddSpacing(line: "276", before: "60");
            p.AddText(text: "BẢNG TỔNG HỢP KẾT QUẢ HOẠT ĐỘNG", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            p = new Paragraph();
            p.AddBoth();
            listData.Add(new DocData { Paragraph = p });

            Table table = new Table();
            table.AddBasicProperty(true);
            TableRow trHeader = new TableRow();
            TableCell c = new TableCell();
            c.AddColWidth(22);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Hoạt động", isBold: true);
            c.Append(p);
            trHeader.Append(c);
            c = new TableCell();
            c.AddColWidth(26);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Kế hoạch", isBold: true);
            c.Append(p);
            trHeader.Append(c);
            c = new TableCell();
            c.AddColWidth(26);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Kết quả đạt được", isBold: true);
            c.Append(p);
            trHeader.Append(c);
            c = new TableCell();
            c.AddColWidth(26);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Ghi chú", isBold: true);
            c.Append(p);
            trHeader.Append(c);
            table.Append(trHeader);

            #region Tien do
            for (int i = 1; i <= listPlanTarget.Count; i++)
            {
                var pt = listPlanTarget[i - 1];
                TableRow tr = new TableRow();
                c = new TableCell();
                p = new Paragraph();
                p.AddSpacing(line: "276", before: "120", after: "120");
                p.AddText("Mục tiêu " + i + ". " + pt.ActionName, fontSize: "24", isBold: true);
                c.Append(p);
                tr.Append(c);
                c = new TableCell();
                List<Paragraph> listPPlanContent = this.GenListParagraphFromContent(pt.PlanContent, false, "24");
                if (listPPlanContent.Count > 0)
                {
                    foreach (var pItem in listPPlanContent)
                    {
                        c.Append(pItem);
                    }
                }
                else
                {
                    p = new Paragraph();
                    p.AddSpacing(line: "276", before: "120", after: "120");
                    c.Append(p);
                }
                tr.Append(c);
                c = new TableCell();
                List<Paragraph> listPTargetResult = this.GenListParagraphFromContent(pt.TargetResult, false, "24");
                if (listPTargetResult.Count > 0)
                {
                    foreach (var pItem in listPTargetResult)
                    {
                        c.Append(pItem);
                    }
                }
                else
                {
                    p = new Paragraph();
                    p.AddSpacing(line: "276", before: "120", after: "120");
                    c.Append(p);
                }
                tr.Append(c);
                c = new TableCell();
                List<Paragraph> listPDes = this.GenListParagraphFromContent(pt.Description, false, "24");
                if (listPTargetResult.Count > 0)
                {
                    foreach (var pItem in listPDes)
                    {
                        c.Append(pItem);
                    }
                }
                else
                {
                    p = new Paragraph();
                    p.AddSpacing(line: "276", before: "120", after: "120");
                    c.Append(p);
                }
                tr.Append(c);
                table.Append(tr);
                var listPlanTargetAction = listDetail.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID != null
                && x.SubPlanActionID == null).OrderBy(x => x.ActionOrder).ToList();
                foreach (var pta in listPlanTargetAction)
                {
                    if (pta == null)
                    {
                        continue;
                    }
                    tr = new TableRow();
                    c = new TableCell();
                    p = new Paragraph();
                    p.AddSpacing(line: "276", before: "120", after: "120");
                    string title = "Hoạt động";
                    if (!string.IsNullOrWhiteSpace(pta.PlanTargetActionCode))
                    {
                        title += " " + pta.PlanTargetActionCode.Trim() + ": ";
                    }
                    p.AddText(title + pta.PlanTargetActionName, fontSize: "24");
                    c.Append(p);
                    tr.Append(c);
                    c = new TableCell();
                    List<Paragraph> listPTPlanContent = this.GenListParagraphFromContent(pta.PlanContent, false, "24");
                    if (listPTPlanContent.Count > 0)
                    {
                        foreach (var pItem in listPTPlanContent)
                        {
                            c.Append(pItem);
                        }
                    }
                    else
                    {
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        c.Append(p);
                    }
                    tr.Append(c);
                    c = new TableCell();
                    List<Paragraph> listPTResult = this.GenListParagraphFromContent(pta.Result, false, "24");
                    if (listPTResult.Count > 0)
                    {
                        foreach (var pItem in listPTResult)
                        {
                            c.Append(pItem);
                        }
                    }
                    else
                    {
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        c.Append(p);
                    }
                    tr.Append(c);
                    c = new TableCell();
                    List<Paragraph> listPTDes = this.GenListParagraphFromContent(pta.Description, false, "24");
                    if (listPTDes.Count > 0)
                    {
                        foreach (var pItem in listPTDes)
                        {
                            c.Append(pItem);
                        }
                    }
                    else
                    {
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        c.Append(p);
                    }
                    tr.Append(c);
                    table.Append(tr);
                    // Sub
                    var listSubAction = listDetail.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID && x.SubPlanActionID != null).OrderBy(x => x.SubActionOrder).ToList();
                    foreach (var s in listSubAction)
                    {
                        if (s == null)
                        {
                            continue;
                        }
                        tr = new TableRow();
                        c = new TableCell();
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        title = "Hoạt động";
                        if (!string.IsNullOrWhiteSpace(s.SubCode))
                        {
                            title += " " + s.SubCode.Trim() + ": ";
                        }
                        p.AddText(title + s.SubContent, fontSize: "24");
                        c.Append(p);
                        tr.Append(c);
                        c = new TableCell();
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        p.AddText((string.IsNullOrWhiteSpace(s.PlanContent) ? string.Empty : s.PlanContent.Trim()), fontSize: "24");
                        c.Append(p);
                        tr.Append(c);
                        c = new TableCell();
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        p.AddText((string.IsNullOrWhiteSpace(s.Result) ? string.Empty : s.Result.Trim()), fontSize: "24");
                        c.Append(p);
                        tr.Append(c);
                        c = new TableCell();
                        p = new Paragraph();
                        p.AddSpacing(line: "276", before: "120", after: "120");
                        p.AddText((string.IsNullOrWhiteSpace(s.Description) ? string.Empty : s.Description.Trim()), fontSize: "24");
                        c.Append(p);
                        tr.Append(c);
                        table.Append(tr);
                    }
                }
            }
            #endregion
            listData.Add(new DocData { Table = table });
            return listData;
        }

        private List<DocData> GenActionIndex(int reportID, int planID)
        {
            List<PlanTarget> listPlantarget = context.PlanTarget.Where(x => x.PlanID == planID).OrderBy(x => x.TargetOrder).ToList();
            ActionReportBusiness bus = new ActionReportBusiness(context);
            List<ActionReportActionIndexModel> listActionIndex = bus.GetActionReportActionIndex(reportID);
            List<DocData> listData = new List<DocData>();
            int i = 1;
            foreach (PlanTarget pt in listPlantarget)
            {
                Paragraph p = new Paragraph();
                p.AddBoth();
                p.AddText(text: "Mục tiêu " + i + ": " + pt.PlanTargetContent, fontSize: "24", isBold: true);
                listData.Add(new DocData { Paragraph = p });
                i++;
                List<ActionReportActionIndexModel> listTargetActionIndex = listActionIndex.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                foreach (ActionReportActionIndexModel actionIndex in listTargetActionIndex)
                {
                    p = new Paragraph();
                    p.AddBoth();
                    p.AddSpacing("276", "120", "120");
                    p.AddIndentation("150");
                    ParagraphProperties pProperty = p.ParagraphProperties;
                    NumberingProperties numberingProperties = new NumberingProperties();
                    NumberingLevelReference numberingLevelReference = new NumberingLevelReference() { Val = 0 };
                    NumberingId numberingId = new NumberingId() { Val = 3 };

                    numberingProperties.Append(numberingLevelReference);
                    numberingProperties.Append(numberingId);
                    pProperty.Append(numberingProperties);
                    string indexName = !string.IsNullOrWhiteSpace(actionIndex.PrefixName) ? actionIndex.PrefixName + " " : "";
                    p.AddText(text: indexName + actionIndex.IndexValue + "/" + actionIndex.FullIndexName, fontSize: "24");
                    listData.Add(new DocData { Paragraph = p });
                }
            }
            return listData;
        }

        private Table GenTargetIndex(int reportID, int planID)
        {
            List<PlanTarget> listPlantarget = context.PlanTarget.Where(x => x.PlanID == planID).OrderBy(x => x.TargetOrder).ToList();
            ActionReportBusiness bus = new ActionReportBusiness(context);
            List<ActionReportIndexModel> listTargetIndex = bus.GetActionReportTargetIndex(reportID);
            Table table = new Table();
            table.AddBasicProperty(true);
            TableRow tr = new TableRow();
            TableCell tc = new TableCell();
            #region Header
            tc.AddColWidth(16);
            Paragraph p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "Mục tiêu", fontSize: "24", isBold: true);
            tc.Append(p);
            tr.Append(tc);

            tc = new TableCell();
            tc.AddColWidth(34);
            p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "Chỉ số theo kế hoạch", fontSize: "24", isBold: true);
            tc.Append(p);
            tr.Append(tc);

            tc = new TableCell();
            tc.AddColWidth(34);
            p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "Chỉ số thực tế đạt được", fontSize: "24", isBold: true);
            tc.Append(p);
            tr.Append(tc);

            tc = new TableCell();
            tc.AddColWidth(16);
            p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "Ghi chú", fontSize: "24", isBold: true);
            tc.Append(p);
            tr.Append(tc);
            table.Append(tr);
            #endregion

            int i = 1;
            foreach (PlanTarget pt in listPlantarget)
            {
                List<ActionReportIndexModel> listTargetIndexByTarget = listTargetIndex.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                int j = 1;
                foreach (ActionReportIndexModel index in listTargetIndexByTarget)
                {
                    tr = new TableRow();
                    tc = new TableCell();
                    p = new Paragraph();
                    p.AddCenter();
                    p.AddText(text: "Mục tiêu " + i, fontSize: "24", isBold: true);
                    tc.Append(p);
                    if (listTargetIndexByTarget.Count > 1)
                    {
                        tc.AddVerticalMerge(j == 1);
                    }
                    tr.Append(tc);

                    string middle = string.Empty;
                    if (index.TypeIndex.GetValueOrDefault() != Constants.TYPE_INDEX_CHOICE)
                    {
                        middle = " ";
                    }
                    string ContentActualIndex;
                    string ContentPlanIndex;
                    if (index.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_CHOICE)
                    {
                        ContentActualIndex = index.ContentIndex;
                        ContentPlanIndex = index.ContentIndex;
                    }
                    else
                    {
                        if (index.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_INPUT_AVERAGE)
                        {
                            middle = "% ";
                            if (index.ContentIndex != null && index.ContentIndex.StartsWith("%"))
                            {
                                index.ContentIndex = index.ContentIndex.Substring(1).Trim();
                            }
                        }
                        ContentActualIndex = index.ValueIndex.GetValueOrDefault() + middle + index.ContentIndex;
                        ContentPlanIndex = index.PlanValueIndex.GetValueOrDefault() + middle + index.ContentIndex;
                    }

                    tc = new TableCell();
                    tc.AddColWidth(34);
                    p = new Paragraph();
                    p.AddBoth();
                    p.AddText(text: ContentPlanIndex, fontSize: "24");
                    tc.Append(p);
                    tr.Append(tc);

                    tc = new TableCell();
                    tc.AddColWidth(34);
                    p = new Paragraph();
                    p.AddBoth();
                    p.AddText(text: ContentActualIndex, fontSize: "24");
                    tc.Append(p);
                    tr.Append(tc);

                    tc = new TableCell();
                    p = new Paragraph();
                    p.AddBoth();
                    p.AddText(text: "", fontSize: "24");
                    tc.Append(p);
                    if (listTargetIndexByTarget.Count > 1)
                    {
                        tc.AddVerticalMerge(j == 1);
                    }
                    tr.Append(tc);
                    table.Append(tr);
                    j++;
                }
                i++;
            }
            return table;
        }

        private List<DocData> GenGeneralInfo(PlanModel plan, List<PlanTargetModel> listPlanTarget)
        {
            List<DocData> listData = new List<DocData>();
            // Title
            Paragraph p = new Paragraph();
            p.AddBoth();
            p.AddText(text: "Phần 1. THÔNG TIN CHUNG", isBold: true);
            listData.Add(new DocData { Paragraph = p });
            // Muc tieu
            Table table = new Table();
            table.AddBasicProperty(true);
            TableRow tr = new TableRow();
            TableCell c = new TableCell();
            c.AddColWidth(20);
            p = new Paragraph();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Mục tiêu chung", isBold: true);
            c.Append(p);
            tr.Append(c);
            c = new TableCell();
            c.AddColWidth(80);
            List<Paragraph> listParagraph = this.GenListParagraphFromContent(plan.PlanTarget);
            if (listParagraph != null && listParagraph.Any())
            {
                foreach (Paragraph pC in listParagraph)
                {
                    c.Append(pC);
                }
            }
            else
            {
                c.Append(new Paragraph());
            }
            tr.Append(c);
            table.Append(tr);
            // Muc tieu cu the
            tr = new TableRow();
            c = new TableCell();
            p = new Paragraph();
            p.AddSpacing(line: "276", before: "120", after: "120");
            p.AddText(text: "Mục tiêu cụ thể", isBold: true);
            c.Append(p);
            tr.Append(c);
            c = new TableCell();
            if (listPlanTarget.Any())
            {
                for (int i = 1; i <= listPlanTarget.Count; i++)
                {
                    PlanTargetModel pt = listPlanTarget[i - 1];
                    p = new Paragraph();
                    p.AddSpacing(line: "276", before: "120", after: "120");
                    p.AddIndentation("360");
                    p.AddText(text: "Mục tiêu " + i + ". ", isBold: true);
                    p.AddText(pt.PlanTargetContent);
                    c.Append(p);
                }
            }
            else
            {
                c.Append(new Paragraph());
            }
            tr.Append(c);
            table.Append(tr);
            // Kinh phi ho tro
            tr = new TableRow();
            c = new TableCell();
            p = new Paragraph();
            p.AddBoth();
            p.AddSpacing(line: "276", before: "60");
            p.AddText(text: "Tổng kinh phí hỗ trợ", isBold: true);
            c.Append(p);
            tr.Append(c);
            c = new TableCell();
            p = new Paragraph();
            p.AddBoth();
            p.AddSpacing(line: "276", before: "60");
            p.AddText(text: string.Format("{0:C}", plan.TotalPlanMoney).Replace("$", "") + " đ ");
            p.AddText(text: "(" + Utils.ChuyenSo(plan.TotalPlanMoney.ToString()) + ")");
            c.Append(p);
            tr.Append(c);
            table.Append(tr);
            listData.Add(new DocData { Table = table });
            return listData;
        }

        private Table GenHeader(ActionReportInfoModel actionReport)
        {
            Table table = new Table();
            TableProperties tableProperties = new TableProperties();
            TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };
            TableLook tableLook = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };

            tableProperties.Append(tableWidth);
            tableProperties.Append(tableLook);

            TableGrid tableGrid = new TableGrid();
            GridColumn gridColumn1 = new GridColumn() { Width = "4635" };
            GridColumn gridColumn2 = new GridColumn() { Width = "4635" };
            tableGrid.Append(gridColumn1);
            tableGrid.Append(gridColumn2);
            table.Append(tableProperties);
            table.Append(tableGrid);
            TableRow tr = new TableRow();
            TableCell c1 = new TableCell();
            c1.AddColWidth(50);
            Paragraph p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "60");
            p.AddText(text: actionReport.DeptName.Trim().ToUpper(), isBold: true);
            c1.Append(p);
            tr.Append(c1);
            TableCell c2 = new TableCell();
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276", before: "60");
            p.AddText(text: "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM", isBold: true, isNewLine: true);
            p.AddText(text: "ĐỘC LẬP – TỰ DO – HẠNH PHÚC", isBold: true);
            c2.Append(p);
            tr.Append(c2);
            table.Append(tr);
            return table;
        }

        private Paragraph CreateBasicParagraph(string text, bool isBold = false, bool isItalic = false, string left = null)
        {
            Paragraph p = new Paragraph();
            p.AddBasicSpacing(left);
            p.AddText(text: text, isBold: isBold, isItalic: isItalic);
            return p;
        }

        /// <summary>
        /// Truong hop nguoi dung nhap noi dung co dau xuong dong thi tach thanh cac Paragraph
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private List<Paragraph> GenListParagraphFromContent(string content, bool isIntable = true, string fontSize = "26")
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            if (!string.IsNullOrWhiteSpace(content))
            {
                string[] arr = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                foreach (string s in arr)
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        Paragraph p = new Paragraph();
                        if (isIntable)
                        {
                            p.AddSpacing(line: "276", before: "120", after: "120");
                        }
                        else
                        {
                            p.AddSpacing(line: "276", before: "60");
                        }
                        p.AddText(text: s.Trim(), fontSize: fontSize);
                        listParagraph.Add(p);
                    }
                }
            }
            return listParagraph;
        }

    }

    class DocData
    {
        public Paragraph Paragraph { get; set; }
        public Table Table { get; set; }
    }
}