﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class CommentController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();
        public JsonResult DelComment(long commentID)
        {
            Comment comment = this.context.Comment.Find(commentID);
            if (comment == null || comment.CommentUserID != SessionManager.GetUserInfo().UserID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            this.context.Comment.Remove(comment);
            this.context.SaveChanges();
            return Json("Xóa comment thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveComment(CommentModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Content))
            {
                throw new BusinessException("Bạn chưa nhập nội dung comment");
            }
            Comment comment = new Comment();
            comment.CommentDate = DateTime.Now;
            comment.CommentUserID = SessionManager.GetUserInfo().UserID;
            comment.Content = model.Content.Trim();
            comment.ObjectID = model.ObjectID;
            comment.Type = model.Type;
            this.context.Comment.Add(comment);
            this.context.SaveChanges();
            return Json("Nhập comment thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateComment(CommentModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Content))
            {
                throw new BusinessException("Bạn chưa nhập nội dung comment");
            }
            Comment comment = this.context.Comment.Find(model.CommentID);
            if (comment == null || comment.CommentUserID != SessionManager.GetUserInfo().UserID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            comment.UpdateDate = DateTime.Now;
            comment.Content = model.Content.Trim();
            this.context.SaveChanges();
            return Json("Cập nhật comment thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RefreshComment(long ObjectID, int Type)
        {
            List<CommentModel> listComment = (from c in this.context.Comment
                                              join u in this.context.User on c.CommentUserID equals u.UserID
                                              where c.ObjectID == ObjectID && c.Type == Type
                                              select new CommentModel
                                              {
                                                  CommentDate = c.CommentDate,
                                                  CommentFullName = u.FullName,
                                                  CommentID = c.CommentID,
                                                  CommentUserID = u.UserID,
                                                  CommentUserName = u.UserName,
                                                  Content = c.Content,
                                                  ObjectID = c.ObjectID,
                                                  Type = c.Type
                                              }).ToList();
            ViewData["listComment"] = listComment;
            ViewData["objectID"] = ObjectID;
            ViewData["objectType"] = Type;
            return PartialView("_ListComment");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}