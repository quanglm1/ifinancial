﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class UserController : Controller
    {
        private readonly Entities _context = new Entities();
        // GET: User
        [BreadCrumb(ControllerName = "Người dùng", ActionName = "Người dùng", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AddOrEditUser(UserModel user)
        {
            // Kiem tra trung tai khoan            
            if (_context.User.Any(x => x.Status != Constants.IS_DELETED_USER && (user.UserID == 0 || x.UserID != user.UserID) && x.UserName.ToLower().Equals(user.UserName.Trim().ToLower())))
            {
                throw new BusinessException("Tài khoản đã tồn tại trong hệ thống");
            }
            else
            {
                var upload = user.UserAvatar;
                string msg = string.Empty;
                if (upload != null && upload.ContentLength > 0)
                {
                    int scale = 100;
                    if (upload.ContentLength > 1024 * 256)
                    {
                        scale = 50;
                    }
                    Image img = Image.FromStream(upload.InputStream);
                    using (MemoryStream s = Utils.GetJpeg(img, scale))
                    {
                        user.Avatar = s.ToArray();
                    }
                }
                Department dept = _context.Department.Find(user.DepartmentID);
                if (dept == null)
                {
                    throw new BusinessException("Đơn vị không tồn tại trong hệ thống");
                }
                UserInfo userInfo = SessionManager.GetUserInfo();
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != dept.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền tạo tài khoản cho đơn vị của mình");
                }
                if (dept.DepartmentType != Constants.DEPT_TYPE_PBQUY)
                {
                    user.listManageDeptID = null;
                    user.ManageDeptIDs = null;
                }
                User entity = null;
                List<UserRole> listUserRole = new List<UserRole>();
                List<UserManageDepartment> listUserManageDept = new List<UserManageDepartment>();
                if (user.UserID > 0)
                {
                    entity = _context.User.FirstOrDefault(x => x.Status != Constants.IS_DELETED_USER && x.UserID == user.UserID);
                    if (entity == null)
                    {
                        throw new BusinessException("Tài khoản không tồn tại trong hệ thống");
                    }
                    user.UpdateEntity(entity);
                    // Phan quyen
                    listUserRole.AddRange(_context.UserRole.Where(x => x.UserID == user.UserID).ToList());
                    // Quan ly don vi
                    listUserManageDept.AddRange(_context.UserManageDepartment.Where(x => x.UserID == user.UserID).ToList());
                    msg = "Cập nhật tài khoản thành công";
                }
                else
                {
                    user.CreateDate = DateTime.Now;
                    user.Status = Constants.IS_ACTIVE;
                    user.Creator = SessionManager.GetUserInfo().UserName;
                    entity = user.ToEntity();
                    _context.User.Add(entity);
                    msg = "Thêm mới tài khoản thành công";
                }
                _context.SaveChanges();
                // Phan quyen
                List<int> listRoleID = new List<int>();
                if (!string.IsNullOrWhiteSpace(user.RoleIDs))
                {
                    listRoleID.AddRange(user.RoleIDs.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).Select(v => int.Parse(v)).ToList());
                }
                List<int> listRoleInsert = listRoleID.Where(x => listUserRole.All(r => r.RoleID != x)).ToList();
                List<UserRole> listUserRoleDel = listUserRole.Where(x => listRoleID.All(v => x.RoleID != v)).ToList();
                if (listRoleInsert.Count > 0)
                {
                    listRoleInsert.ForEach(x =>
                    {
                        UserRole ur = new UserRole();
                        ur.RoleID = x;
                        ur.UserID = entity.UserID;
                        _context.UserRole.Add(ur);
                    });
                }
                if (listUserRoleDel.Count > 0)
                {
                    _context.UserRole.RemoveRange(listUserRoleDel);
                }
                // quan ly don vi
                List<int> listDeptId = new List<int>();
                if (!string.IsNullOrWhiteSpace(user.ManageDeptIDs))
                {
                    listDeptId.AddRange(user.ManageDeptIDs.Split(',').Where(v => !string.IsNullOrWhiteSpace(v)).Select(v => int.Parse(v)).ToList());
                }
                List<int> listDeptInsert = listDeptId.Where(x => listUserManageDept.All(r => r.DepartmentID != x)).ToList();
                List<UserManageDepartment> listUserManageDeptDel = listUserManageDept.Where(x => listDeptId.All(v => x.DepartmentID != v)).ToList();
                if (listDeptInsert.Count > 0)
                {
                    listDeptInsert.ForEach(x =>
                    {
                        UserManageDepartment ud = new UserManageDepartment();
                        ud.DepartmentID = x;
                        ud.UserID = entity.UserID;
                        _context.UserManageDepartment.Add(ud);
                    });
                }
                if (listUserManageDeptDel.Count > 0)
                {
                    _context.UserManageDepartment.RemoveRange(listUserManageDeptDel);
                }
                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }
        public PartialViewResult OpenAddOrEditUser(int? userId)
        {
            UserModel model = null;
            if (userId.HasValue && userId.Value > 0)
            {
                User entity = _context.User.FirstOrDefault(x => x.UserID == userId && x.Status != Constants.IS_DELETED_USER);
                if (entity == null)
                {
                    throw new BusinessException("Tài khoản không tồn tại trong hệ thống");
                }
                model = new UserModel();
                model.EntityTo(entity);
                // Lay them thong tin phan quyen va quan ly don vi
                List<int> listRoleId = (from ur in _context.UserRole
                                        where ur.UserID == userId && _context.Role.Any(r => r.RoleID == ur.RoleID && r.Status == Constants.IS_ACTIVE)
                                        select ur.RoleID).ToList();
                List<int> listDeptId = (from ud in _context.UserManageDepartment
                                        where ud.UserID == userId && _context.Department.Any(d => d.DepartmentID == ud.DepartmentID && d.Status == Constants.IS_ACTIVE)
                                        select ud.DepartmentID).ToList();
                if (listRoleId.Count > 0)
                {
                    model.RoleIDs = string.Join(",", listRoleId);
                    model.listRoleID = listRoleId.ToArray();
                }
                if (listDeptId.Count > 0)
                {
                    model.ManageDeptIDs = string.Join(",", listDeptId);
                    model.listManageDeptID = listDeptId.ToArray();
                }
            }
            // Lay danh sach don vi va danh sach vai tro
            ViewData["listDepartment"] = GetListDepartment();
            ViewData["listDepartmentManage"] = GetListDepartment(true);
            List<SelectListItem> listRole = _context.Role.Where(x => x.Status == Constants.IS_ACTIVE)
                .Select(x => new SelectListItem
                {
                    Text = x.RoleName,
                    Value = x.RoleID.ToString()
                }).ToList();
            ViewData["listRole"] = listRole;
            return PartialView("_AddOrEdit", model);
        }
        public JsonResult DeleteUser(int[] userIds)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID)).ToList();
            listUser.ForEach(x =>
            {
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != x.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền xóa tài khoản cho đơn vị của mình");
                }
                x.Status = Constants.IS_DELETED_USER;
            });
            _context.SaveChanges();
            return Json("Xóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActiveUser(int[] userIds)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID) && x.Status == Constants.IS_NOT_ACTIVE).ToList();
            listUser.ForEach(x =>
            {
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != x.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền kích hoạt tài khoản cho đơn vị của mình");
                }
                x.Status = Constants.IS_ACTIVE;
            });
            _context.SaveChanges();
            return Json("Kích hoạt tài khoản thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockUser(int[] userIds)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            List<User> listUser = _context.User.Where(x => userIds.Contains(x.UserID)).ToList();
            listUser.ForEach(x =>
            {
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != x.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền khóa tài khoản cho đơn vị của mình");
                }
                x.Status = Constants.IS_NOT_ACTIVE;
            });
            _context.SaveChanges();
            return Json("Khóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActiveUserSingle(int id)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID) && x.Status != Constants.IS_DELETED_USER);
            if (user == null)
            {
                throw new BusinessException("Tài khoản không tồn tại trong hệ thống");
            }
            else
            {
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != user.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền kích hoạt tài khoản cho đơn vị của mình");
                }
                user.Status = Constants.IS_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Kích hoạt tài khoản thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockUserSingle(int id)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID));
            if (user == null)
            {
                throw new BusinessException("Tài khoản không tồn tại trong hệ thống");
            }
            else
            {
                if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != user.DepartmentID)
                {
                    throw new BusinessException("Bạn chỉ có quyền khóa tài khoản cho đơn vị của mình");
                }
                user.Status = Constants.IS_NOT_ACTIVE;
            }
            _context.SaveChanges();
            return Json("Khóa tài khoản thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResetPassword(int id)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            string passReset = Common.StringCipher.GetRandomString();
            User user = _context.User.FirstOrDefault(x => id.Equals(x.UserID));
            if (user == null)
            {
                throw new BusinessException("Tài khoản không tồn tại trong hệ thống");
            }
            if (userInfo.DepartmentID != Constants.HEAD_DEPT_ID && userInfo.DepartmentID != user.DepartmentID)
            {
                throw new BusinessException("Bạn chỉ có quyền thay đổi mật khẩu tài khoản cho đơn vị của mình");
            }
            if (user.LastLoginDate != null)
            {
                // Neu dang nhap roi thi cap nhat lai de hien thi mat khau
                user.LastLoginDate = null;
            }
            else
            {
                // Neu khong thi set lai that
                user.Password = Common.StringCipher.Encrypt(passReset);

            }
            _context.SaveChanges();
            return Json("Cập nhật mật khẩu thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeptType(int deptID)
        {
            Department dept = _context.Department.Find(deptID);
            int deptType = 0;
            if (dept != null)
            {
                deptType = dept.DepartmentType.GetValueOrDefault();
            }
            return Json(deptType, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchUser(string Keyword = null, int page = 1)
        {
            if (string.IsNullOrWhiteSpace(Keyword))
            {
                Keyword = null;
            }
            else
            {
                Keyword = Keyword.Trim().ToLower();
            }
            UserInfo userInfo = SessionManager.GetUserInfo();
            IQueryable<UserModel> iQUser = from x in _context.User
                                           join d in _context.Department on x.DepartmentID equals d.DepartmentID
                                           where x.Status != Constants.IS_DELETED_USER
                                           && (userInfo.DepartmentID == Constants.HEAD_DEPT_ID || d.DepartmentID == userInfo.DepartmentID)
                                           && (Keyword == null || x.UserName.ToLower().Contains(Keyword)
                                           || x.FullName.ToLower().Contains(Keyword)
                                           || x.PhoneNumber.ToLower().Contains(Keyword)
                                           || x.Email.ToLower().Contains(Keyword))
                                           orderby x.UserName, x.LastName, x.FullName
                                           select new UserModel
                                           {
                                               UserID = x.UserID,
                                               UserName = x.UserName,
                                               CreateDate = x.CreateDate,
                                               LastLoginDate = x.LastLoginDate,
                                               Status = x.Status,
                                               Password = x.Password,
                                               FullName = x.FullName,
                                               PhoneNumber = x.PhoneNumber,
                                               Email = x.Email,
                                               Position = x.Position,
                                               Avatar = x.Avatar,
                                               DepartmentID = x.DepartmentID,
                                               DepartmentName = d.DepartmentName
                                           };
            Paginate<UserModel> paginate = new Paginate<UserModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.Count = iQUser.Count();
            List<UserModel> listUser = iQUser.Skip((page - 1) * paginate.PageSize).Take(paginate.PageSize).ToList();
            // Thiet lap lai mat khau
            listUser.ForEach(x =>
            {
                if (x.LastLoginDate.HasValue)
                {
                    x.Password = "********";
                }
                else
                {
                    x.Password = StringCipher.Decrypt(x.Password);
                }
            });
            paginate.List = listUser;
            ViewData["listUser"] = paginate;
            return PartialView("_LstUser");
        }

        private List<SelectListItem> GetListDepartment(bool isNotPB = false)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            List<Department> listDepartment = _context.Department.Where(x => (userInfo.DepartmentID == Constants.HEAD_DEPT_ID || x.DepartmentID == userInfo.DepartmentID)
            && x.Status == Constants.IS_ACTIVE && (!isNotPB || (x.DepartmentType != Constants.DEPT_TYPE_PBQUY && x.ParentID != null))).OrderBy(x => x.ParentID).ThenBy(x => x.DepartmentName).ToList();
            List<Department> listRoot = listDepartment.Where(x => (userInfo.DepartmentID == Constants.HEAD_DEPT_ID || x.DepartmentID == userInfo.DepartmentID) && x.ParentID == null).ToList();
            if (!listRoot.Any())
            {
                return listDepartment.Select(x => new SelectListItem
                {
                    Value = x.DepartmentID.ToString(),
                    Text = x.DepartmentName
                }).ToList();
            }
            Dictionary<int, List<Department>> dicRoot1 = new Dictionary<int, List<Department>>();
            listRoot.ForEach(x =>
            {
                dicRoot1[x.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x.DepartmentID).ToList();
            });

            Dictionary<int, List<Department>> dicRoot2 = new Dictionary<int, List<Department>>();
            dicRoot1.Values.ToList().ForEach(x =>
            {
                x.ForEach(x1 =>
                {
                    dicRoot2[x1.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x1.DepartmentID).ToList();
                });
            });
            List<SelectListItem> listSelectDepartment = new List<SelectListItem>();
            listRoot.ForEach(x =>
            {
                SelectListItem item = new SelectListItem
                {
                    Value = x.DepartmentID.ToString(),
                    Text = x.DepartmentName
                };
                listSelectDepartment.Add(item);
                // cap 2
                if (dicRoot1.ContainsKey(x.DepartmentID))
                {
                    dicRoot1[x.DepartmentID].ForEach(x1 =>
                    {
                        SelectListItem item1 = new SelectListItem
                        {
                            Value = x1.DepartmentID.ToString(),
                            Text = "... " + x1.DepartmentName
                        };
                        listSelectDepartment.Add(item1);
                        // cap 3
                        if (dicRoot2.ContainsKey(x1.DepartmentID))
                        {
                            dicRoot2[x1.DepartmentID].ForEach(x2 =>
                            {
                                SelectListItem item2 = new SelectListItem
                                {
                                    Value = x2.DepartmentID.ToString(),
                                    Text = "...... " + x2.DepartmentName
                                };
                                listSelectDepartment.Add(item2);
                            });
                        }
                    });
                }
            });
            return listSelectDepartment;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}