﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public partial class PeriodDepartmentController : Controller
    {
        private Entities context = new Entities();
        //Main
        [BreadCrumb(ControllerName = "Danh sách kế hoạch", ActionName = "Danh sách kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            if (objUser.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                return RedirectToAction("Index", "PlanSummary");
            }
            return View();
        }
        public PartialViewResult SearchPeriodDept(string PeriodName = null, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IQueryable<Plan> lstPlan = (from a in context.Plan
                                        join b in context.Department on a.DepartmentID equals b.DepartmentID
                                        where a.DepartmentID == objUser.DepartmentID
                                        select a);
            IQueryable<PeriodDeptModel> iqPeriod =
                (from x in context.Period
                 join y in lstPlan on x.PeriodID equals y.PeriodID into g1
                 from y in g1.DefaultIfEmpty()
                 where x.Status != Constants.PERIOD_DEACTIVED
                 select new PeriodDeptModel()
                 {
                     PeriodID = x.PeriodID,
                     PlanID = y.PlanID,
                     IsChange = y.IsChange,
                     DepartmentID = objUser.DepartmentID,
                     DepartmentName = objUser.DepartmentName,
                     Description = x.Description,
                     PeriodName = x.PeriodName,
                     FromDate = x.FromDate,
                     ToDate = x.ToDate,
                     Status = y == null ? 0 : y.Status
                 }).Distinct();
            if (!string.IsNullOrWhiteSpace(PeriodName))
            {
                PeriodName = PeriodName.Trim().ToLower();
                iqPeriod = iqPeriod.Where(x => x.PeriodName.ToLower().Contains(PeriodName));
            }
            Paginate<PeriodDeptModel> paginate = new Paginate<PeriodDeptModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqPeriod.OrderByDescending(x => x.ToDate).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            // Quyen
            List<int> listPlanID = paginate.List.Where(x => x.PlanID != null).Select(x => x.PlanID.Value).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.context).GetRoleForObject(objUser.UserID, listPlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            paginate.List.ForEach(x =>
            {
                if (x.PlanID == null)
                {
                    x.Role = Constants.ROLE_EDIT;
                }
                else
                {
                    x.Role = dicRole[x.PlanID.Value].Role;
                    x.PL = dicRole[x.PlanID.Value].PL;
                }
            });
            paginate.Count = iqPeriod.Count();
            ViewData["listPeriodDept"] = paginate;
            return PartialView("_ListPeriodDept");
        }
        public ActionResult PlanningTime()
        {
            return View();
        }
        public ActionResult EstimateCost()
        {
            return View();
        }
        //Step 1
        [BreadCrumb(ControllerName = "Lập kế hoạch", ActionName = "Chi tiết kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult PlanningInfo(int PeriodID, int PlanID = 0)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            Plan objPlan = new Plan();
            Department objDepartment = new Department();
            if (PlanID == 0)
            {
                objDepartment = (from a in context.Department
                                 where a.DepartmentID == objUser.DepartmentID.Value
                                 select a).FirstOrDefault();
                objPlan = context.Plan.Where(a => a.PeriodID == PeriodID
                   && a.DepartmentID == objDepartment.DepartmentID
                   && a.ParentPlanID == null).FirstOrDefault();
            }
            else
            {
                objPlan = context.Plan.Find(PlanID);
                objDepartment = context.Department.Find(objPlan.DepartmentID);
            }
            Period objPeriod = (from a in context.Period
                                where a.PeriodID == PeriodID
                                select a).FirstOrDefault();

            PlanModel model = new PlanModel();
            model.PeriodID = PeriodID;
            model.PeriodName = objPeriod.PeriodName;
            model.Type = objUser.DeptType;
            //Department
            model.DepartmentID = objDepartment.DepartmentID;
            model.DepartmentName = objDepartment.DepartmentName;
            model.Address = objDepartment.Address;
            model.PhoneNumber = objDepartment.PhoneNumber;
            model.Fax = objDepartment.Fax;
            model.AccountNumber = objDepartment.AccountNumber;
            model.BankName = objDepartment.BankName;
            model.RepresentPersonName = objDepartment.RepresentPersonName;
            model.RepresentPosition = objDepartment.RepresentPosition;
            model.RepresentAddress = objDepartment.RepresentAddress;
            model.RepresentPhone = objDepartment.RepresentPhone;
            model.RepresentEmail = objDepartment.RepresentEmail;
            model.ContactPersonName = objDepartment.ContactPersonName;
            model.ContactPosition = objDepartment.ContactPosition;
            model.ContactAddress = objDepartment.ContactAddress;
            model.ContactPhone = objDepartment.ContactPhone;
            model.ContactEmail = objDepartment.ContactEmail;
            model.FromDate = objPeriod.FromDate;
            model.ToDate = objPeriod.ToDate;
            model.Status = Constants.PLAN_NEW;

            if (objPlan != null)
            {
                model.PlanID = (int)objPlan.PlanID;
                model.PlanContent = objPlan.PlanContent;
                model.PlanTarget = objPlan.PlanTarget;
                model.RepresentPersonName = objPlan.RepresentPersonName;
                model.RepresentPosition = objPlan.RepresentPosition;
                model.RepresentAddress = objPlan.RepresentAddress;
                model.RepresentPhone = objPlan.RepresentPhone;
                model.RepresentEmail = objPlan.RepresentEmail;
                model.ContactPersonName = objPlan.ContactPersonName;
                model.ContactPosition = objPlan.ContactPosition;
                model.ContactAddress = objPlan.ContactAddress;
                model.ContactPhone = objPlan.ContactPhone;
                model.ContactEmail = objPlan.ContactEmail;
                model.FromDate = objPlan.FromDate;
                model.ToDate = objPlan.ToDate;
                model.Status = objPlan.Status;
                // Set phan quyen
                ObjectRoleBO role = new FlowProcessBusiness(this.context).GetRoleForObject(objUser.UserID, objPlan.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home");
                }
                model.Role = role.Role;
                model.PL = role.PL;
                // Lay muc tieu
                PlanTarget pt = context.PlanTarget
               .Where(a => a.PlanID == objPlan.PlanID)
               .OrderBy(x => x.PlanTargetID).FirstOrDefault();
                if (pt != null)
                {
                    ViewData["FirstPlanTargetID"] = pt.PlanTargetID;
                }
            }
            else
            {
                model.Role = Constants.ROLE_EDIT;
                model.PL = null;
            }
            ViewData["PlanID"] = model.PlanID == null ? 0 : model.PlanID.Value;
            ViewData["PeriodID"] = model.PeriodID;
            ViewData["PlanStatus"] = model.Status;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PlanInforSave(PlanModel model)
        {
            string msg;
            var objUser = SessionManager.GetUserInfo();
            if (ModelState.IsValid)
            {
                try
                {
                    Plan entity = null;
                    if (model.PlanID.HasValue)
                    {
                        entity = context.Plan.Where(a => a.PlanID == model.PlanID).FirstOrDefault();
                    }
                    else
                    {
                        context.Plan.Where(a => a.PeriodID == model.PeriodID
                            && a.DepartmentID == model.DepartmentID).FirstOrDefault();
                    }
                    if (entity != null)
                    {
                        // Validate permission
                        List<int> listPlanID = new List<int> { entity.PlanID };
                        Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.context).GetRoleForObject(objUser.UserID, listPlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
                        listPlanID.ForEach(x =>
                        {
                            if (dicRole[x].Role <= Constants.ROLE_VIEW || (dicRole[x].PL.GetValueOrDefault() > 0 && dicRole[x].PL.GetValueOrDefault() != 1))
                            {
                                throw new BusinessException("Bạn không có quyền cập nhật kế hoạch");
                            }
                        });
                        model.ToUpdateEntity(entity);
                        msg = "Lưu thông tin kế hoạch tổng thể thành công";
                    }
                    else
                    {
                        entity = model.ToEntity();
                        entity.Status = Constants.PLAN_NEW;
                        //Neu la phong ban truc thuoc, luu loai ke hoach la cua quy
                        if (objUser.DeptType == Constants.MAIN_TYPE)
                        {
                            entity.Type = Constants.MAIN_TYPE;
                        }
                        else
                        {
                            entity.Type = Constants.SUB_TYPE;
                        }
                        this.context.Plan.Add(entity);
                        msg = "Lưu thông tin kế hoạch tổng thể thành công";
                    }
                    this.context.SaveChanges();
                    return Json(new
                    {
                        entity.PlanID,
                        msg
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    throw new BusinessException("Lỗi trong quá trình lưu thông tin");
                }

            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public JsonResult SaveTargetResult(int planTargetID, string targetResult)
        {
            PlanTarget pt = context.PlanTarget.Find(planTargetID);
            if (pt == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu");
            }
            pt.Description = targetResult;
            context.SaveChanges();
            return Json("Cập nhật nội dung của mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DelTargetResult(int planTargetID)
        {
            PlanTarget pt = context.PlanTarget.Find(planTargetID);
            if (pt == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu");
            }
            pt.Description = null;
            context.SaveChanges();
            return Json("Cập nhật nội dung của mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }
        public JsonResult RequestReviewPlan(int PlanID, int? IsPassValidate = null)
        {
            PlanErrorBO resBO = new PlanErrorBO();
            resBO.Type = 0;
            string comment = string.Empty;
            int reviewType = 0;
            if (PlanID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                Plan entity = context.Plan.Find(PlanID);
                if (entity == null)
                {
                    throw new BusinessException("Kế hoạch chưa được lập");
                }
                if (entity.DepartmentID == user.DepartmentID
                    && entity.StepNow.GetValueOrDefault() != 0)
                {
                    throw new BusinessException("Kế hoạch đã được gửi trước đó");
                }
                if (entity.DepartmentID == user.DepartmentID)
                {
                    comment = Constants.PLAN_SENT_COMMENT;
                    reviewType = Constants.PLAN_TYPE_SEND;
                }
                else
                {
                    comment = Constants.PLAN_AGREE_SENT_COMMENT;
                    reviewType = Constants.PLAN_TYPE_AGREE;
                }
                // Kiem tra truoc khi gui yeu cau len quy                    
                string name = string.Empty;
                // Kiem tra da co muc tieu chua
                if (!context.PlanTarget.Any(x => x.PlanID == PlanID))
                {
                    throw new BusinessException("Kế hoạch chưa có mục tiêu, vui lòng chọn mục tiêu và hoạt động");
                }

                // Chua co hoat dong
                //if (!this.CheckHasAction(PlanID))
                //{
                //    resBO.Name = name;
                //    resBO.Type = 2;
                //    return Json(resBO, JsonRequestBehavior.AllowGet);
                //}
                //// Thoi gian
                //if (!this.CheckActionTime(PlanID, out name))
                //{
                //    resBO.Name = name;
                //    resBO.Type = 1;
                //    return Json(resBO, JsonRequestBehavior.AllowGet);
                //}
                //// Chua co chi so muc tieu
                //if (!this.CheckPlanTargetIndex(PlanID))
                //{
                //    resBO.Name = name;
                //    resBO.Type = 3;
                //    return Json(resBO, JsonRequestBehavior.AllowGet);
                //}
                //// Chua co chi so hoat dong
                //if (!this.CheckPlanActionIndex(PlanID))
                //{
                //    resBO.Name = name;
                //    resBO.Type = 4;
                //    return Json(resBO, JsonRequestBehavior.AllowGet);
                //}
                //// Chua co chi so hoat dong
                //if (!this.CheckActionContent(PlanID, out name))
                //{
                //    resBO.Name = name;
                //    resBO.Type = 6;
                //    return Json(resBO, JsonRequestBehavior.AllowGet);
                //}
                //// Chua co chi phi
                //if (IsPassValidate.GetValueOrDefault() == 0)
                //{
                //    if (!this.CheckPlanCost(PlanID, out name))
                //    {
                //        resBO.Name = name;
                //        resBO.Type = 5;
                //        return Json(resBO, JsonRequestBehavior.AllowGet);
                //    }
                //}
                long planHisID = 0;
                try
                {
                    PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                    planHisID = bus.InsertPlanHistory(PlanID, Constants.PLAN_NEXT_LEVER);
                }
                catch (Exception)
                {
                    // ignored
                }
                entity.Status = Constants.PLAN_NEXT_LEVER;
                entity.StepNow = entity.StepNow.GetValueOrDefault() + 1;
                UserInfo objUser = SessionManager.GetUserInfo();
                PlanReviewHistory entityHis = new PlanReviewHistory();
                entityHis.UserID = objUser.UserID;
                entityHis.PlanID = PlanID;
                entityHis.CreateDate = DateTime.Now;
                entityHis.Comment = comment;
                entityHis.ReviewType = reviewType;
                entityHis.PlanHisID = planHisID;
                entityHis.Step = entity.StepNow;
                context.PlanReviewHistory.Add(entityHis);
                // Xoa thong tin khoa
                List<ChangeRequestAction> listLockAction = context.ChangeRequestAction.Where(x => x.PlanID == PlanID).ToList();
                context.ChangeRequestAction.RemoveRange(listLockAction);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lưu, bạn chưa khai báo mục tiêu và hoạt động");
            }
            return Json(resBO, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnlockPlan(int PlanID, int IsSefl = 0)
        {
            if (PlanID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                Plan entity = context.Plan.Find(PlanID);
                if (entity.DepartmentID == user.DepartmentID)
                {
                    throw new BusinessException("Bạn không có quyền xem xét kế hoạch của chính đơn vị");
                }
                if (entity.Status < Constants.PLAN_NEXT_LEVER)
                {
                    throw new BusinessException("Kế hoạch chưa được gửi lên quỹ");
                }
                // Check role
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
                if (role == null || (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE))
                {
                    throw new BusinessException("Bạn không có quyền mở khóa kế hoạch này");
                }

                long planHisID = 0;
                try
                {
                    PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                    planHisID = bus.InsertPlanHistory(PlanID, Constants.PLAN_NEXT_LEVER);
                }
                catch (Exception)
                {
                    // ignored
                }
                entity.Status = Constants.PLAN_NEXT_LEVER;
                entity.StepNow = entity.StepNow.GetValueOrDefault() - 1;
                if (entity.StepNow.GetValueOrDefault() <= 0)
                {
                    entity.StepNow = null;
                }
                UserInfo objUser = SessionManager.GetUserInfo();
                PlanReviewHistory entityHis = new PlanReviewHistory();
                entityHis.UserID = objUser.UserID;
                entityHis.PlanID = PlanID;
                entityHis.CreateDate = DateTime.Now;
                if (IsSefl == 0)
                {
                    entityHis.Comment = Constants.PLAN_UNLOCK_COMMENT;
                }
                else
                {
                    entityHis.Comment = "Mở khóa cập nhật thông tin";
                }
                entityHis.ReviewType = Constants.PLAN_TYPE_UNLOCK;
                entityHis.PlanHisID = planHisID;
                entityHis.Step = entity.StepNow;
                context.PlanReviewHistory.Add(entityHis);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lưu, bạn chưa khai báo mục tiêu và hoạt động");
            }
            return Json("Mở khóa kế hoạch thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApproveOrRejectPlan(int PlanID, int Status)
        {
            string msg;
            if (PlanID != 0)
            {
                UserInfo user = SessionManager.GetUserInfo();
                Plan entity = context.Plan.Find(PlanID);
                if (entity.DepartmentID == user.DepartmentID)
                {
                    throw new BusinessException("Bạn không có quyền phê duyệt hoặc từ chối kế hoạch của chính đơn vị");
                }
                if (entity.Status < Constants.PLAN_NEXT_LEVER)
                {
                    throw new BusinessException("Kế hoạch chưa được gửi lên quỹ");
                }
                // Check role
                ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
                if (role == null || role.Role < Constants.ROLE_APPROVE)
                {
                    throw new BusinessException("Bạn không có quyền thao tác kế hoạch này");
                }

                long planHisID = 0;
                try
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                        if (Status == Constants.IS_ACTIVE)
                        {
                            planHisID = bus.InsertPlanHistory(PlanID, Constants.PLAN_APPROVED);
                        }
                        else
                        {
                            planHisID = bus.InsertPlanHistory(PlanID, Constants.PLAN_NEXT_LEVER);
                        }
                        dbContextTransaction.Commit();
                    }
                }
                catch (Exception)
                {
                    // ignored
                    throw new BusinessException("Thao tác bị lỗi, vui lòng thử lại sau");
                }
                int reviewType;
                string comment;
                if (Status == Constants.IS_ACTIVE)
                {
                    reviewType = Constants.PLAN_TYPE_APPROVE;
                    comment = Constants.PLAN_APPROVE_COMMENT;
                    entity.Status = Constants.PLAN_APPROVED;
                    entity.StepNow = entity.StepNow.GetValueOrDefault() + 1;
                    // Cap nhat lai trang thai yeu cau neu co
                    ChangeRequest cr = context.ChangeRequest.Where(x => x.PlanID == entity.PlanID && x.Status == Constants.PLAN_APPROVED && x.IsFinish == null).FirstOrDefault();
                    if (cr != null)
                    {
                        cr.IsFinish = Constants.IS_ACTIVE;
                    }
                    // Neu chua co ke hoach chot thi luu
                    if (!context.PlanHis.Any(x => x.PlanID == entity.PlanID && x.FirstApprove == Constants.IS_ACTIVE))
                    {
                        PlanHis planHis = context.PlanHis.Find(planHisID);
                        if (planHis != null)
                        {
                            planHis.FirstApprove = Constants.IS_ACTIVE;
                        }
                    }
                    // Xoa thong tin khoa
                    List<ChangeRequestAction> listLockAction = context.ChangeRequestAction.Where(x => x.PlanID == PlanID).ToList();
                    context.ChangeRequestAction.RemoveRange(listLockAction);
                    msg = "Phê duyệt kế hoạch thành công";
                }
                else
                {
                    reviewType = Constants.PLAN_TYPE_UNAPPROVE;
                    comment = Constants.PLAN_UNAPPROVE_COMMENT;
                    entity.Status = Constants.PLAN_NEXT_LEVER;
                    // Neu tu choi ke hoach tim neu co dau moi thi tra ve dau moi con khong thi lui 1 buoc
                    FlowProcess flowProcess = context.FlowProcess.Where(x => x.ObjectType == Constants.COMMENT_TYPE_PLAN_TARGET && x.IsRepresent == Constants.IS_ACTIVE)
                        .OrderBy(x => x.Step).FirstOrDefault();
                    if (entity.Type == Constants.SUB_TYPE && flowProcess != null)
                    {
                        entity.StepNow = flowProcess.Step;
                    }
                    else
                    {
                        entity.StepNow = entity.StepNow.GetValueOrDefault() - 1;
                        if (entity.StepNow.GetValueOrDefault() <= 0)
                        {
                            entity.StepNow = null;
                        }
                    }
                    // Cap nhat lai trang thai yeu cau neu co
                    ChangeRequest cr = context.ChangeRequest.Where(x => x.PlanID == entity.PlanID && x.Status == Constants.PLAN_APPROVED && x.IsFinish == null).FirstOrDefault();
                    if (cr != null)
                    {
                        cr.IsFinish = null;
                    }
                    else
                    {
                        // Neu co ke hoach chot thi bo luu ban chot
                        PlanHis planHis = context.PlanHis.Where(x => x.PlanID == entity.PlanID && x.FirstApprove == Constants.IS_ACTIVE).FirstOrDefault();
                        if (planHis != null)
                        {
                            planHis.FirstApprove = null;
                        }
                    }
                    msg = "Từ chối kế hoạch thành công";
                }
                UserInfo objUser = SessionManager.GetUserInfo();
                PlanReviewHistory entityHis = new PlanReviewHistory();
                entityHis.UserID = objUser.UserID;
                entityHis.PlanID = PlanID;
                entityHis.CreateDate = DateTime.Now;
                entityHis.Comment = comment;
                entityHis.ReviewType = reviewType;
                entityHis.PlanHisID = planHisID;
                entityHis.Step = entity.StepNow;
                context.PlanReviewHistory.Add(entityHis);
                context.SaveChanges();
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lưu, bạn chưa khai báo mục tiêu và hoạt động");
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        //Step 2
        //Load du lieu day len popup chon muc tieu
        public PartialViewResult Step1(int PeriodID, int PlanID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            Plan objPlan;
            if (PlanID == 0)
            {
                objPlan = context.Plan.Where(a => a.PeriodID == PeriodID && a.DepartmentID == objUser.DepartmentID).FirstOrDefault();
            }
            else
            {
                objPlan = context.Plan.Find(PlanID);
            }
            //if (objPlan == null)
            //{
            //    throw new BusinessException("Thông tin chung kế hoạch chưa được lưu thông tin");
            //}
            Department objDepartment = context.Department.Find(objPlan.DepartmentID);
            Period objPeriod = (from a in context.Period
                                where a.PeriodID == PeriodID
                                select a).FirstOrDefault();

            PlanModel model = new PlanModel();
            model.PeriodID = PeriodID;
            model.Type = objUser.DeptType;
            //Department
            model.DepartmentID = objDepartment.DepartmentID;
            model.DepartmentName = objDepartment.DepartmentName;
            model.Address = objDepartment.Address;
            model.PhoneNumber = objDepartment.PhoneNumber;
            model.Fax = objDepartment.Fax;
            model.AccountNumber = objDepartment.AccountNumber;
            model.BankName = objDepartment.BankName;
            model.FromDate = objPeriod.FromDate;
            model.ToDate = objPeriod.ToDate;
            model.Status = Constants.PLAN_NEW;
            model.PlanID = (int)objPlan.PlanID;
            model.PlanContent = objPlan.PlanContent;
            model.PlanTarget = objPlan.PlanTarget;
            model.RepresentPersonName = objPlan.RepresentPersonName;
            model.RepresentPosition = objPlan.RepresentPosition;
            model.RepresentAddress = objPlan.RepresentAddress;
            model.RepresentPhone = objPlan.RepresentPhone;
            model.RepresentEmail = objPlan.RepresentEmail;
            model.ContactPersonName = objPlan.ContactPersonName;
            model.ContactPosition = objPlan.ContactPosition;
            model.ContactAddress = objPlan.ContactAddress;
            model.ContactPhone = objPlan.ContactPhone;
            model.ContactEmail = objPlan.ContactEmail;
            model.Status = objPlan.Status;
            UserInfo user = SessionManager.GetUserInfo();
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, objPlan.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            if (role != null && role.Role >= Constants.ROLE_EDIT && (role.PL == null || role.PL == 1))
            {
                return PartialView("_Step1", model);
            }
            else
            {
                return PartialView("_Step1View", model);
            }
        }
        public PartialViewResult Step2(int PlanID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            // Kiem tra viec khoa
            List<ChangeRequestActionModel> listChangeAction = new PlanBusiness(context).GetPlanUnlockAction(PlanID);
            if (listChangeAction != null && listChangeAction.Any())
            {
                ViewData["NoChangeTarget"] = Constants.IS_ACTIVE;
            }
            return PartialView("_Step2");
        }
        public PartialViewResult Step3(int PlanTargetID)
        {
            PlanTargetModel model = new PlanTargetModel();
            PlanTarget objPlantarget = context.PlanTarget.Find(PlanTargetID);
            if (objPlantarget == null)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để khai báo hoạt động");
            }
            model.PlanTargetID = objPlantarget.PlanTargetID;
            model.PlanID = objPlantarget.PlanID;
            model.PlanTargetTitle = objPlantarget.PlanTargetTitle;
            return PartialView("_Step3", model);
        }
        [HttpPost]
        public PartialViewResult PrepareTarget(int PlanID = 0, int page = 1)
        {
            List<TargetModel> ListTargets = new List<TargetModel>();
            UserInfo objUser = SessionManager.GetUserInfo();
            Department dept = context.Department.Find(objUser.DepartmentID);
            if (PlanID != 0)
            {
                var objPlan = context.Plan.Find(PlanID);
                if (objPlan == null || objPlan.Status == Constants.IS_NOT_ACTIVE)
                {
                    throw new BusinessException("Thông tin chung kế hoạch chưa được lưu thông tin hoặc kế hoạch đã bị xóa");
                }
                dept = context.Department.Find(objPlan.DepartmentID);
                //Neu la don vi 
                var ExceptMainListID = context.PlanTarget.Where(x => x.PlanID == PlanID).Select(x => x.TargetID).ToList();

                //Get template Target
                ListTargets = context.Target
                    .Where(x => x.Status != Constants.IS_NOT_ACTIVE && x.Type == objPlan.Type
                    && (x.DepartmentType == null || x.DepartmentType == 0 || x.DepartmentType == dept.DepartmentType))
                    .Select(x => new TargetModel
                    {
                        TargetContent = x.TargetContent,
                        TargetID = x.TargetID,
                        TargetTitle = x.TargetTitle,
                        Status = x.Status,
                        Choosen = ExceptMainListID.Any(y => y == x.TargetID) ? 1 : 0
                    })
                    .Distinct()
                    .ToList();
                //Kiem tra neu la phong ban thuoc quy
                if (objPlan.Type == Constants.MAIN_TYPE)
                {
                    //Lay len danh sach target duoc phan cong nhiem vu lap ke hoach
                    ListTargets = (from a in ListTargets
                                   join b in context.DepartmentAction on a.TargetID equals b.TargetID
                                   where b.DepartmentID == objUser.DepartmentID
                                   select a)
                                   .Distinct()
                                   .ToList();

                }

                SessionManager.SetValue(Constants.PLAN_ID, PlanID);
            }
            else
            {
                throw new BusinessException("Bạn phải lưu thông tin chung trước");
            }
            Paginate<TargetModel> paginate = new Paginate<TargetModel>();
            paginate.PageSize = Constants.PAGE_SIZE_SUPER_MAX;
            paginate.List = ListTargets.OrderBy(x => x.TargetOrder).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            for (int i = 1; i <= paginate.List.Count; i++)
            {
                var target = paginate.List[i - 1];
                target.TargetTitle = "Mục tiêu " + i;
            }
            paginate.Count = ListTargets.Count();
            ViewData["listTarget"] = paginate;
            return PartialView("_Step2Target");
        }
        //Thuc hien lua chọn cac muc tieu
        public PartialViewResult ChoiceTarget(string[] TargetIDs, int PlanID)
        {
            Plan plan = context.Plan.Find(PlanID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch đang chọn");
            }
            var objUser = SessionManager.GetUserInfo();
            List<PlanTargetModel> PlanTargetModels = new List<PlanTargetModel>();
            if (!TargetIDs.Any())
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu để thêm");
            }
            List<PlanTarget> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == PlanID && x.TargetID != null).ToList();
            int CodeNumber = 1;
            //get main list
            List<int> lstTargetID = TargetIDs.Select(a => int.Parse(a)).ToList();
            List<int> lstTargetIDDel = listPlanTarget.Where(x => !lstTargetID.Contains(x.TargetID.Value)).Select(x => x.TargetID.Value).ToList();
            lstTargetID.RemoveAll(x => listPlanTarget.Any(p => p.TargetID.Value == x));
            if (lstTargetID != null && lstTargetID.Any())
            {
                //for save
                List<PlanTarget> lstPlan = context.Target
                    .Where(x => lstTargetID.Contains(x.TargetID))
                    .OrderBy(x => x.TargetOrder)
                    .ToList()
                    .Select(x => new PlanTarget
                    {
                        PlanID = PlanID,
                        TargetID = x.TargetID,
                        CreateDate = DateTime.Now,
                        PlanTargetContent = x.TargetContent,
                        PlanTargetTitle = x.TargetTitle,
                        TargetOrder = x.TargetOrder,
                        Status = Constants.IS_ACTIVE,
                        DepartmentID = objUser.DepartmentID.Value,
                        TargetCode = x.TargetOrder == null ? (CodeNumber++).ToString() : x.TargetOrder.ToString()
                    }).ToList();
                if (lstPlan.Any())
                {
                    context.PlanTarget.AddRange(lstPlan);
                }
            }
            // delete before save
            if (lstTargetIDDel != null && lstTargetIDDel.Any())
            {
                PlanBusiness bus = new PlanBusiness(context);
                List<PlanTarget> lstPlanDel = context.PlanTarget.Where(x => x.PlanID == PlanID && x.TargetID != null && lstTargetIDDel.Contains(x.TargetID.Value)).ToList();
                bus.DelPlanTarget(lstPlanDel, PlanID);
            }
            if ((lstTargetID != null && lstTargetID.Any())
                || (lstTargetIDDel != null && lstTargetIDDel.Any()))
            {
                context.SaveChanges();
                this.UpdatePlanOrder(plan);
                context.SaveChanges();
            }
            PlanTargetModels = context.PlanTarget
                .Where(a => a.PlanID == PlanID)
                .OrderBy(x => x.TargetOrder)
                .ThenBy(x => x.PlanTargetTitle)
                .Select(a => new PlanTargetModel
                {
                    PlanTargetID = a.PlanTargetID,
                    PlanTargetTitle = a.PlanTargetTitle,
                    PlanTargetContent = a.PlanTargetContent,
                    TargetCode = a.TargetCode
                }).ToList();
            ViewData["PlanTargetModels"] = PlanTargetModels;
            return PartialView("_Step2TargetView");
        }
        public PartialViewResult LoadComments(int PlanID, int PlanTargetID)
        {
            ViewData["ListComments"] = GetIndexComment(PlanID, PlanTargetID);
            return PartialView("_Step2Reviews");
        }
        public PartialViewResult SaveComments(string CommentContent, int PlanID, int PlanTargetID)
        {
            PlanTarget objPlanTarget = context.PlanTarget.Where(a => a.PlanTargetID == PlanTargetID && a.PlanID == PlanID).FirstOrDefault();
            UserInfo objUser = SessionManager.GetUserInfo();
            Comment entity = new Comment();
            entity.CommentUserID = objUser.UserID;
            entity.Content = CommentContent;
            entity.ObjectID = objPlanTarget.PlanTargetID;
            entity.Type = Constants.COMMENT_TYPE_INDEX;
            entity.CommentDate = DateTime.Now;
            context.Comment.Add(entity);
            context.SaveChanges();
            ViewData["ListComments"] = GetIndexComment(PlanID, PlanTargetID);
            return PartialView("_Step2Reviews");
        }
        public PartialViewResult FirtLoadTarget(int? PlanID, int? PlanTargetID = null)
        {
            List<PlanTargetModel> PlanTargetModels = new List<PlanTargetModel>();
            if (PlanID != null)
            {
                PlanTargetModels = context.PlanTarget
               .Where(a => a.PlanID == PlanID)
               .OrderBy(x => x.TargetOrder)
               .ThenBy(x => x.PlanTargetTitle)
               .Select(a => new PlanTargetModel
               {
                   PlanTargetID = a.PlanTargetID,
                   PlanTargetTitle = a.PlanTargetTitle,
                   PlanTargetContent = a.PlanTargetContent,
                   TargetCode = a.TargetCode
               }).ToList();
            }
            ViewData["PlanTargetModels"] = PlanTargetModels;
            ViewData["PlanTargetID"] = PlanTargetID;
            return PartialView("_Step2TargetView");
        }
        /// <summary>
        /// Mo popup them moi chi so
        /// </summary>
        /// <param name="PlanTargetID"></param>
        /// <param name="PlanID"></param>
        /// <returns></returns>
        public PartialViewResult PrepareAddNewIndex(int? PlanTargetID, int? PlanID = 0, int planTargetIndexID = 0)
        {
            PlanIndexModel model = new PlanIndexModel();
            model.PlanID = PlanID;
            model.PlanTargetID = PlanTargetID;
            if (planTargetIndexID > 0)
            {
                model = context.PlanTargetIndex.Where(x => x.PlanTargetIndexID == planTargetIndexID)
                    .Select(x => new PlanIndexModel
                    {
                        ContentIndex = x.ContentIndex,
                        CreateUserID = x.CreateUserID,
                        MainIndexID = x.MainIndexID,
                        PlanID = x.PlanID,
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetIndexID = x.PlanTargetIndexID,
                        TypeIndex = x.TypeIndex,
                        UpdateUser = x.UpdateUser,
                        ValueIndex = x.ValueIndex,
                        ValueTotal = x.ValueTotal
                    }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại chỉ số");
                }
            }
            ViewData["ListType"] = GetListType();
            return PartialView("_Step2AddNewIndex", model);
        }
        /// <summary>
        /// Mo popup them moi muc tieu
        /// </summary>
        /// <param name="PlanID"></param>
        /// <returns></returns>
        public PartialViewResult PrepareAddNewTarget(int PlanID, int PlanTargetID = 0)
        {
            PlanTargetModel model = new PlanTargetModel();
            model.PlanID = PlanID;
            if (PlanTargetID > 0)
            {
                PlanTarget pt = context.PlanTarget.Find(PlanTargetID);
                if (pt != null)
                {
                    model.PlanTargetID = PlanTargetID;
                    model.PlanTargetTitle = pt.PlanTargetTitle;
                    model.PlanTargetContent = pt.PlanTargetContent;
                }
            }
            return PartialView("_Step2AddNewTarget", model);
        }

        public JsonResult RemovePlanTarget(int PlanTargetID)
        {
            PlanTarget planTarget = context.PlanTarget.Find(PlanTargetID);
            if (planTarget == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đang chọn");
            }
            Plan plan = context.Plan.Find(planTarget.PlanID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            this.DelPlanTarget(PlanTargetID);
            context.SaveChanges();
            this.UpdatePlanOrder(plan);
            context.SaveChanges();
            return Json("Xóa mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemovePlanTargetIndex(int PlanTargetIndexID)
        {
            PlanTargetIndex index = context.PlanTargetIndex.Find(PlanTargetIndexID);
            if (index == null)
            {
                throw new BusinessException("Không tồn tại chỉ số đang chọn");
            }
            context.PlanTargetIndex.Remove(index);
            context.SaveChanges();
            return Json("Xóa chỉ số thành công", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// thuc hien them moi chi so tu popup them moi chi so
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult AddIndex(PlanIndexModel model)
        {
            if (ModelState.IsValid)
            {
                // Kiem tra du lieu dau vao
                if (model.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE || model.TypeIndex == Constants.TYPE_INDEX_INPUT)
                {
                    if (model.ValueIndex == null)
                    {
                        throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tuyệt đối hoặc tỷ lệ");
                    }
                    if (model.ValueIndex.Value < 0)
                    {
                        throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tuyệt đối hoặc tỷ lệ là số dương");
                    }
                    if (model.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE)
                    {
                        if (model.ValueIndex.Value < 0 || model.ValueIndex.Value > 100)
                        {
                            throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tỷ lệ là giá trị từ 0 đến 100");
                        }
                        model.ValueTotal = null;
                    }
                }
                else
                {
                    model.ValueIndex = null;
                    model.ValueTotal = null;
                }
                PlanTarget plantarget = context.PlanTarget.Find(model.PlanTargetID);
                PlanTargetIndex entity = null;
                if (model.PlanTargetIndexID <= 0)
                {
                    entity = new PlanTargetIndex();
                }
                else
                {
                    entity = context.PlanTargetIndex.Find(model.PlanTargetIndexID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại chỉ số");
                    }
                }
                entity.ContentIndex = model.ContentIndex;
                entity.PlanID = model.PlanID;
                entity.PlanTargetID = model.PlanTargetID;
                entity.ValueIndex = model.ValueIndex;
                entity.ValueTotal = model.ValueTotal;
                entity.ValueIndexChange = model.ValueIndex;
                entity.ValueTotalChange = model.ValueTotal;
                entity.TypeIndex = model.TypeIndex;
                entity.TargetID = plantarget.TargetID;
                entity.CreateUserID = SessionManager.GetUserInfo().UserID;
                if (model.PlanTargetIndexID <= 0)
                {
                    context.PlanTargetIndex.Add(entity);
                }
                context.SaveChanges();
                string msg = "Thêm mới tiêu chí thành công";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }
        /// <summary>
        /// thuc hien them moi vao db muc tieu tu popup them moi chi tieu
        /// </summary>
        /// <param name="PlanTargetContent"></param>
        /// <param name="PlanTargetTitle"></param>
        /// <param name="PlanID"></param>
        /// <returns></returns>
        public JsonResult AddTarget(PlanTargetModel model)
        {
            if (ModelState.IsValid)
            {
                Plan plan = context.Plan.Find(model.PlanID);
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch được chọn");
                }
                string msg;
                PlanTarget entity = null;
                if (model.PlanTargetID > 0)
                {
                    entity = context.PlanTarget.Find(model.PlanTargetID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại mục tiêu được chọn");
                    }
                    entity.PlanTargetTitle = model.PlanTargetTitle;
                    entity.PlanTargetContent = model.PlanTargetContent;
                    context.SaveChanges();
                    msg = "Cập nhật mục tiêu thành công";
                }
                else
                {
                    entity = new PlanTarget();
                    entity.PlanID = model.PlanID;
                    entity.PlanTargetTitle = model.PlanTargetTitle;
                    entity.PlanTargetContent = model.PlanTargetContent;
                    entity.Status = Constants.IS_ACTIVE;
                    context.PlanTarget.Add(entity);
                    context.SaveChanges();
                    List<PlanTarget> listPlanTarget = context.PlanTarget.Where(a => a.PlanID == model.PlanID).OrderBy(x => x.PlanTargetID).ToList();
                    string prefixCode = plan.Type.GetValueOrDefault() == Constants.MAIN_TYPE ? "2." : "5.";
                    int count = 1;
                    foreach (PlanTarget planTarget in listPlanTarget)
                    {
                        string targetCode = prefixCode + count;
                        count++;
                        if (string.IsNullOrWhiteSpace(planTarget.TargetCode) || !targetCode.Equals(planTarget.TargetCode))
                        {
                            planTarget.TargetCode = targetCode;
                        }
                    }
                    context.SaveChanges();
                    msg = "Thêm mới mục tiêu thành công";
                }

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }
        [HttpPost]
        public PartialViewResult TargetSelected(int? PlanTargetID, int? PlanID)
        {
            Plan plan = context.Plan.Find(PlanID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch được chọn");
            }
            List<TargetIndexModel> ChoosingTargetIndex = new List<TargetIndexModel>();
            int CountComment = 0;
            if (PlanTargetID == null || PlanTargetID == 0)
            {
                throw new BusinessException("Bạn chưa chọn tiêu chí để thêm");
            }
            PlanTarget objPlanTarget = context.PlanTarget.Where(a => a.PlanID == PlanID && a.PlanTargetID == PlanTargetID).FirstOrDefault();
            ChoosingTargetIndex = (from x in context.PlanTargetIndex
                                   join y in context.MainIndex on x.MainIndexID equals y.MainIndexID into g1
                                   from y in g1.DefaultIfEmpty()
                                   where x.PlanTargetID == objPlanTarget.PlanTargetID && x.PlanID == PlanID
                                   orderby y.OrderNumber
                                   select new TargetIndexModel
                                   {
                                       PlanTargetIndexID = x.PlanTargetIndexID,
                                       MainIndexID = x.MainIndexID,
                                       ContentIndex = x.ContentIndex,
                                       TypeIndex = x.TypeIndex,
                                       ValueIndex = x.ValueIndex,
                                       ValueTotal = x.ValueTotal,
                                       ValueIndexChange = x.ValueIndexChange,
                                       ValueTotalChange = x.ValueTotalChange,
                                       UpdateUserID = x.UpdateUser
                                   }).ToList();
            CountComment = (from x in context.Comment
                            join y in context.PlanTarget on x.ObjectID equals y.PlanTargetID
                            where y.PlanID == PlanID && y.Status != Constants.IS_NOT_ACTIVE
                            select x.CommentID).Count();
            ViewData["CountComment"] = CountComment;
            ViewData["listTargetIndexChoosen"] = ChoosingTargetIndex;
            ViewData["ListType"] = GetListType();
            ViewData["plan"] = plan;
            ViewData["planTarget"] = objPlanTarget;
            UserInfo user = SessionManager.GetUserInfo();
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID.Value, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            // Kiem tra viec khoa
            List<ChangeRequestActionModel> listChangeAction = new PlanBusiness(context).GetPlanUnlockAction(plan.PlanID);
            if (listChangeAction != null && listChangeAction.Any())
            {
                ViewData["NoChangeTarget"] = Constants.IS_ACTIVE;
            }
            return PartialView("_Step2TargetIndexView");
        }
        public PartialViewResult PrepareIndex(int? PlanTargetID, int? PlanID = 0, int? planTargetIndexID = 0, int page = 1)
        {
            UserInfo objUserInfo = SessionManager.GetUserInfo();
            if (PlanTargetID == null || PlanTargetID == 0 || PlanID == null || PlanID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm tiêu chí");
            }
            IEnumerable<PlanTargetIndex> PlanTargetIndexs = context.PlanTargetIndex
                .Where(x => x.PlanID == PlanID && x.PlanTargetID == PlanTargetID)
                .AsEnumerable();
            PlanTarget objPlanTarget = context.PlanTarget.Find(PlanTargetID);
            if (objPlanTarget == null)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm tiêu chí");
            }
            Plan plan = context.Plan.Find(objPlanTarget.PlanID);
            List<TargetIndexModel> TargetIndexs = (from a in context.MainIndex
                                                   join b in PlanTargetIndexs on a.MainIndexID equals b.MainIndexID into g1
                                                   from b in g1.DefaultIfEmpty()
                                                   where a.Type == plan.Type && a.Status == Constants.IS_ACTIVE
                                                   && a.TargetID == objPlanTarget.TargetID && (planTargetIndexID == 0 || b.PlanTargetIndexID == planTargetIndexID)
                                                   select new TargetIndexModel
                                                   {
                                                       MainIndexID = a.MainIndexID,
                                                       ContentIndex = a.ContentIndex,
                                                       TypeIndex = a.TypeIndex,
                                                       Choosen = b == null ? 0 : 1,
                                                       ValueIndex = b == null ? a.IndexValue : b.ValueIndex,
                                                       ValueTotal = b == null ? null : b.ValueTotal
                                                   }).ToList();

            Paginate<TargetIndexModel> paginate = new Paginate<TargetIndexModel>();
            paginate.PageSize = Constants.PAGE_SIZE_SUPER_MAX;
            paginate.List = TargetIndexs.OrderBy(x => x.OrderNumber).ThenBy(x => x.MainIndexID).Skip((page - 1) * Constants.PAGE_SIZE_SUPER_MAX).Take(Constants.PAGE_SIZE_SUPER_MAX).ToList();
            paginate.Count = TargetIndexs.Count();
            ViewData["listTargetIndex"] = paginate;
            ViewData["planTargetIndexID"] = planTargetIndexID;
            return PartialView("_Step2TargetIndex");
        }
        public PartialViewResult ChoiceIndex(List<IndexMappingBO> TargetIndexIDs, int? PlanTargetID, int? PlanID = 0)
        {
            List<TargetIndexModel> ChoosingTargetIndex = new List<TargetIndexModel>();
            UserInfo objUser = SessionManager.GetUserInfo();
            if (!TargetIndexIDs.Any())
            {
                throw new BusinessException("Bạn chưa chọn tiêu chí để thêm");
            }
            if (PlanTargetID == null || PlanTargetID == 0 || PlanID == null || PlanID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm tiêu chí");
            }
            //Get targetID
            var PlanTarget = context.PlanTarget.Find(PlanTargetID);
            if (PlanTarget.TargetID == null)
            {
                throw new BusinessException("Mục tiêu của kế hoạch phải được gắn với mục tiêu được Quỹ gợi ý");
            }
            ViewData["planTarget"] = PlanTarget;
            Plan plan = context.Plan.Find(PlanID);
            ViewData["plan"] = plan;
            //get main list
            if (TargetIndexIDs != null && TargetIndexIDs.Any())
            {
                // get all old Index
                var lstTargetIndexDel = context.PlanTargetIndex
                    .Where(a => a.PlanID == PlanID && a.PlanTargetID == PlanTargetID && a.MainIndexID != null)
                    .ToList();
                //for save
                List<PlanTargetIndex> lstTargetIndex = (from a in TargetIndexIDs
                                                        join b in context.MainIndex.Where(x => x.TargetID == PlanTarget.TargetID) on a.MainIndexID equals b.MainIndexID
                                                        select new PlanTargetIndex
                                                        {
                                                            PlanID = PlanID.Value,
                                                            MainIndexID = a.MainIndexID,
                                                            ContentIndex = b.ContentIndex,
                                                            TargetID = PlanTarget.TargetID,
                                                            ValueIndex = a.ValueIndex,
                                                            ValueTotal = a.ValueTotal,
                                                            PlanTargetID = PlanTargetID,
                                                            TypeIndex = b.TypeIndex
                                                        }).ToList();

                //for add new
                List<PlanTargetIndex> lstTargetIndexAddNew = lstTargetIndex
                    .Where(a => !lstTargetIndexDel.Any(x => x.MainIndexID == a.MainIndexID))
                    .Select(a => new PlanTargetIndex
                    {
                        PlanID = PlanID.Value,
                        MainIndexID = a.MainIndexID,
                        ContentIndex = a.ContentIndex,
                        TargetID = PlanTarget.TargetID,
                        PlanTargetID = PlanTargetID,
                        ValueIndex = a.ValueIndex,
                        ValueTotal = a.ValueTotal,
                        TypeIndex = a.TypeIndex,
                        ValueIndexChange = a.ValueIndex,
                        ValueTotalChange = a.ValueTotal,
                        CreateUserID = objUser.UserID,
                        UpdateUser = objUser.UserID
                    }).ToList();

                // Kiem tra du lieu dau vao
                foreach (PlanTargetIndex indexMap in lstTargetIndexAddNew)
                {
                    if (indexMap.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE || indexMap.TypeIndex == Constants.TYPE_INDEX_INPUT)
                    {
                        if (indexMap.ValueIndex == null)
                        {
                            throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tuyệt đối hoặc tỷ lệ");
                        }
                        if (indexMap.ValueIndex.Value < 0)
                        {
                            throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tuyệt đối hoặc tỷ lệ là số dương");
                        }
                        if (indexMap.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE)
                        {
                            if (indexMap.ValueIndex.Value < 0 || indexMap.ValueIndex.Value > 100)
                            {
                                throw new BusinessException("Bạn phải nhập giá trị cho chỉ số loại tỷ lệ là giá trị từ 0 đến 100");
                            }
                            indexMap.ValueTotal = null;
                        }
                    }
                    else
                    {
                        indexMap.ValueIndex = null;
                        indexMap.ValueTotal = null;
                    }
                }

                context.PlanTargetIndex.AddRange(lstTargetIndexAddNew);
                //for edit

                var lstTargetIndexEdit = (from a in lstTargetIndex
                                          join b in lstTargetIndexDel on a.MainIndexID equals b.MainIndexID
                                          select new
                                          {
                                              PlanTargetIndex = b,
                                              ValueIndex = a.ValueIndex,
                                              ValueTotal = a.ValueTotal,
                                              ValueIndexChange = b.ValueIndex,
                                              ValueTotalChange = b.ValueTotal,
                                              TypeIndex = a.TypeIndex
                                          }).ToList();

                foreach (var index in lstTargetIndexEdit)
                {
                    PlanTargetIndex planTargetIndex = index.PlanTargetIndex;
                    planTargetIndex.UpdateUser = objUser.UserID;
                    planTargetIndex.TypeIndex = index.TypeIndex;
                    planTargetIndex.ValueIndex = index.ValueIndex;
                    planTargetIndex.ValueTotal = index.ValueTotal;
                    planTargetIndex.ValueIndexChange = index.ValueIndex;
                    planTargetIndex.ValueTotalChange = index.ValueTotal;
                }
                lstTargetIndexDel.RemoveAll(x => lstTargetIndexEdit.Any(i => i.PlanTargetIndex.PlanTargetIndexID == x.PlanTargetIndexID));
                if (lstTargetIndexDel.Any())
                {
                    context.PlanTargetIndex.RemoveRange(lstTargetIndexDel);
                }
                context.SaveChanges();
            }
            ChoosingTargetIndex = (from x in context.PlanTargetIndex
                                   join yj in context.MainIndex on x.MainIndexID equals yj.MainIndexID into yg
                                   from y in yg.DefaultIfEmpty()
                                   where x.PlanTargetID == PlanTargetID && x.PlanID == PlanID
                                   orderby y.OrderNumber
                                   select new TargetIndexModel
                                   {
                                       PlanTargetIndexID = x.PlanTargetIndexID,
                                       MainIndexID = x.MainIndexID.Value,
                                       ContentIndex = x.ContentIndex,
                                       TypeIndex = x.TypeIndex,
                                       ValueIndex = x.ValueIndex,
                                       ValueTotal = x.ValueTotal,
                                       PlanTargetID = PlanTargetID.Value,
                                       ValueIndexChange = x.ValueIndexChange,
                                       ValueTotalChange = x.ValueTotalChange,
                                       UpdateUserID = x.UpdateUser.Value,
                                   }).ToList();
            ViewData["listTargetIndexChoosen"] = ChoosingTargetIndex;
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(objUser.UserID, PlanID.Value, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            return PartialView("_Step2TargetIndexView");
        }

        public PartialViewResult UpdateIndex(List<IndexMappingBO> TargetIndexIDs, int? PlanTargetID, int planTargetIndexID, int? PlanID = 0)
        {
            List<TargetIndexModel> ChoosingTargetIndex = new List<TargetIndexModel>();
            UserInfo objUser = SessionManager.GetUserInfo();
            if (!TargetIndexIDs.Any())
            {
                throw new BusinessException("Bạn chưa chọn tiêu chí để thêm");
            }
            if (PlanTargetID == null || PlanTargetID == 0 || PlanID == null || PlanID == 0)
            {
                throw new BusinessException("Bạn chưa lựa chọn mục tiêu để thêm tiêu chí");
            }
            //Get targetID
            var PlanTarget = context.PlanTarget.Find(PlanTargetID);
            if (PlanTarget.TargetID == null)
            {
                throw new BusinessException("Mục tiêu của kế hoạch phải được gắn với mục tiêu được Quỹ gợi ý");
            }
            Plan plan = context.Plan.Find(PlanID);
            ViewData["plan"] = plan;
            ViewData["planTarget"] = PlanTarget;
            //get main list
            if (TargetIndexIDs != null && TargetIndexIDs.Any())
            {
                PlanTargetIndex entity = context.PlanTargetIndex.Find(planTargetIndexID);
                if (entity == null || entity.PlanTargetID != PlanTargetID)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                IndexMappingBO indexBO = TargetIndexIDs.First();
                entity.UpdateUser = objUser.UserID;
                entity.ValueTotal = indexBO.ValueTotal;
                entity.ValueIndex = indexBO.ValueIndex;
                context.SaveChanges();
            }
            ChoosingTargetIndex = (from x in context.PlanTargetIndex
                                   join y in context.MainIndex on x.MainIndexID equals y.MainIndexID
                                   where x.PlanTargetID == PlanTargetID && x.PlanID == PlanID
                                   select new TargetIndexModel
                                   {
                                       PlanTargetIndexID = x.PlanTargetIndexID,
                                       MainIndexID = x.MainIndexID.Value,
                                       ContentIndex = x.ContentIndex,
                                       TypeIndex = y.TypeIndex,
                                       ValueIndex = x.ValueIndex,
                                       ValueTotal = x.ValueTotal,
                                       PlanTargetID = PlanTargetID.Value,
                                       ValueIndexChange = x.ValueIndexChange,
                                       ValueTotalChange = x.ValueTotalChange,
                                       UpdateUserID = x.UpdateUser.Value,
                                   }).ToList();
            ViewData["listTargetIndexChoosen"] = ChoosingTargetIndex;
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(objUser.UserID, PlanID.Value, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            return PartialView("_Step2TargetIndexView");
        }

        //Step 3
        public PartialViewResult LoadActionOfTarget(int? PlanTargetID, int? PlanID)
        {
            if (PlanTargetID == null || PlanTargetID == 0)
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu");
            }
            PlanTarget objPlanTarget = context.PlanTarget.Where(a => a.PlanID == PlanID && a.PlanTargetID == PlanTargetID).FirstOrDefault();
            if (objPlanTarget == null)
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu");
            }
            Plan plan = context.Plan.Find(objPlanTarget.PlanID);
            //Danh sach hoat dong
            List<TargetActionModel> TargetActions = (from x in context.PlanTargetAction
                                                     where x.PlanTargetID == objPlanTarget.PlanTargetID && x.PlanID == PlanID
                                                     orderby x.ActionOrder
                                                     select new TargetActionModel
                                                     {
                                                         ActionCode = x.ActionCode,
                                                         ActionContent = x.ActionContent,
                                                         MainActionID = x.MainActionID,
                                                         ContentPropaganda = x.ContentPropaganda,
                                                         ObjectScope = x.ObjectScope,
                                                         ActionMethod = x.ActionMethod,
                                                         Result = x.Result,
                                                         ActionOrder = x.ActionOrder,
                                                         PlanTargetActionID = x.PlanTargetActionID,
                                                         UpdateUserID = x.UpdateUserID
                                                     })
                            .ToList();
            List<int> listPlanTargetActionID = TargetActions.Select(x => x.PlanTargetActionID).ToList();
            Dictionary<int, string> dicTime = new PlanHistoryBusiness(context).ActionTimes(listPlanTargetActionID, PlanID.Value);
            // Danh sach thoi gian da khai bao
            TargetActions = TargetActions
                .Select(x => new TargetActionModel
                {
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    MainActionID = x.MainActionID,
                    ContentPropaganda = x.ContentPropaganda,
                    ObjectScope = x.ObjectScope,
                    ActionMethod = x.ActionMethod,
                    PlanTargetActionID = x.PlanTargetActionID,
                    UpdateUserID = x.UpdateUserID,
                    Result = x.Result,
                    ActionTimes = dicTime[x.PlanTargetActionID]
                }).ToList();
            UserInfo user = SessionManager.GetUserInfo();
            CompareBusiness cb = new CompareBusiness(context);
            cb.UpdatePlanTargetChange(plan.PlanID, user.UserID, PlanTargetID.Value, 0, TargetActions);
            ViewData["listMainAction"] = MainActionOfTargets(plan, objPlanTarget.TargetID);
            ViewData["AllSubActions"] = GetSubAction(objPlanTarget.PlanTargetID);
            ViewData["AllSubIndexs"] = GetSubIndex(PlanTargetID.Value);
            ViewData["listTargetAction"] = TargetActions;
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID.Value, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            List<ChangeRequestActionModel> listChangeAction = new PlanBusiness(context).GetPlanUnlockAction(objPlanTarget.PlanID);
            if (listChangeAction != null)
            {
                ViewData["listChangeAction"] = listChangeAction;
            }
            return PartialView("_Step3ActionView");
        }
        public PartialViewResult LoadSubAction(int? PlanTargetActionID)
        {
            if (PlanTargetActionID == null || PlanTargetActionID == 0)
            {
                throw new BusinessException("Hoạt động cha đã bị xóa hoặc hết hiệu lực");
            }
            PlanTargetAction pta = context.PlanTargetAction.Find(PlanTargetActionID);
            if (pta == null)
            {
                throw new BusinessException("Hoạt động cha đã bị xóa hoặc hết hiệu lực");
            }
            PlanTarget objPlanTarget = context.PlanTarget.Where(a => a.PlanTargetID == pta.PlanTargetID).FirstOrDefault();
            if (objPlanTarget == null)
            {
                throw new BusinessException("Bạn chưa chọn mục tiêu");
            }
            UserInfo user = SessionManager.GetUserInfo();
            var listSubAction = GetSubAction(objPlanTarget.PlanTargetID, PlanTargetActionID.Value);
            ViewData["AllSubIndexs"] = GetSubIndex(objPlanTarget.PlanTargetID, PlanTargetActionID.Value);
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, objPlanTarget.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            List<ChangeRequestActionModel> listChangeAction = new PlanBusiness(context).GetPlanUnlockAction(objPlanTarget.PlanID);
            if (listChangeAction != null)
            {
                ViewData["listChangeAction"] = listChangeAction;
            }
            return PartialView("_Step3SubActionView", listSubAction);
        }
        public PartialViewResult LoadSubIndex(int? PlanTargetActionID)
        {
            if (PlanTargetActionID == null || PlanTargetActionID == 0)
            {
                throw new BusinessException("Hoạt động cha đã bị xóa hoặc hết hiệu lực");
            }
            PlanTargetAction pta = context.PlanTargetAction.Find(PlanTargetActionID);
            if (pta == null)
            {
                throw new BusinessException("Hoạt động cha đã bị xóa hoặc hết hiệu lực");
            }
            ViewData["SubIndexs"] = GetSubIndex(pta.PlanTargetID.Value, PlanTargetActionID.Value);
            return PartialView("_Step3SubIndexView");
        }
        [HttpPost]
        public PartialViewResult PrepareAddActionOfTarget(int PlanTargetID, int? PlanID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            Plan plan = context.Plan.Find(PlanID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            Period period = context.Period.Find(plan.PeriodID);
            ViewData["period"] = period;
            //Dung tim templateAction theo TargetID
            PlanTarget objPlanTarget = context.PlanTarget.Find(PlanTargetID);
            if (objPlanTarget == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu");
            }
            ViewData["listAction"] = MainActionOfTargets(plan, objPlanTarget.TargetID);
            TargetActionModel model = new TargetActionModel();
            model.PlanID = PlanID;
            model.PlanTargetID = PlanTargetID;
            int? ActionOrder = context.PlanTargetAction
                            .Where(a => a.PlanTargetID == objPlanTarget.PlanTargetID && a.PlanID == PlanID)
                            .Max(a => a.ActionOrder);
            model.ActionOrder = ActionOrder.GetValueOrDefault() + 1;
            ViewData["TargetName"] = objPlanTarget.PlanTargetContent;
            return PartialView("_Step3ActionAdd", model);
        }
        public PartialViewResult PrepareAddSubAction(int PlanTargetActionID)
        {
            var TargetAction = (from x in context.PlanTargetAction
                                join t in context.PlanTarget on x.PlanTargetID equals t.PlanTargetID
                                join p in context.Plan on t.PlanID equals p.PlanID
                                join r in context.Period on p.PeriodID equals r.PeriodID
                                where x.PlanTargetActionID == PlanTargetActionID
                                select new
                                {
                                    Plan = p,
                                    Period = r,
                                    TargetAction = x
                                }).FirstOrDefault();
            if (TargetAction == null)
            {
                throw new BusinessException("Không tồn tại hoạt động");
            }
            UserInfo objUser = SessionManager.GetUserInfo();
            if (TargetAction.Plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            Period period = TargetAction.Period;
            ViewData["period"] = period;
            //Dung tim templateAction theo TargetID
            PlanTargetAction objAction = context.PlanTargetAction.Find(PlanTargetActionID);
            ViewData["listAction"] = MainActionOfTargets(TargetAction.Plan, objAction.PlanTarget.TargetID, objAction.MainActionID, true);
            SubActionModel model = new SubActionModel();
            model.PlanActionID = PlanTargetActionID;
            model.PlanActionContent = objAction.ActionContent;
            model.PlanTargetTitle = objAction.PlanTarget.PlanTargetTitle;
            int? ActionOrder = context.SubActionPlan
                            .Where(a => a.PlanTargetActionID == PlanTargetActionID)
                            .Max(a => a.SubActionOrder);
            model.SubActionOrder = ActionOrder.GetValueOrDefault() + 1;
            return PartialView("_Step3SubActionAdd", model);
        }
        public PartialViewResult PrepareAddSubIndex(int PlanTargetActionID, int? SubActionPlanID = null)
        {
            //Dung tim templateIndex theo TargetID

            PlanTargetAction objAction = context.PlanTargetAction.Find(PlanTargetActionID);
            // Kiem tra neu co con thi ko co add
            if (SubActionPlanID == null && context.SubActionPlan.Where(x => x.PlanTargetActionID == PlanTargetActionID).Any())
            {
                throw new BusinessException("Bạn chỉ được thêm chỉ số hoạt động đối với các hoạt động con");
            }
            int MainActionID = objAction.MainActionID == null ? 0 : objAction.MainActionID.Value;
            if (SubActionPlanID != null && SubActionPlanID != 0)
            {
                var objSubAction = context.SubActionPlan.Find(SubActionPlanID);
                MainActionID = objSubAction.SubActionID == null ? 0 : objSubAction.SubActionID.Value;
            }
            PlanTargetActionModel model = new PlanTargetActionModel();
            model.PlanTargetActionID = PlanTargetActionID;
            model.SubActionPlanID = SubActionPlanID;
            var listActionIndex = (from a in context.ActionIndex
                                   where a.ActionID == MainActionID
                                   || (context.MainAction.Any(x => x.ParentID == MainActionID && a.ActionID == x.MainActionID && x.Status == Constants.IS_ACTIVE))
                                   orderby a.ActionIndexID
                                   select new PlanActionIndexModel
                                   {
                                       IndexUnitName = a.UnitName,
                                       IndexName = a.IndexName,
                                       ActionIndexID = a.ActionIndexID
                                   }).ToList();
            var listPlanActionindex = context.PlanActionIndex.Where(x => x.PlanTargetActionID == PlanTargetActionID && x.SubActionPlanID == SubActionPlanID).OrderBy(x => x.PlanActionIndexID).ToList();
            foreach (var index in listActionIndex)
            {
                if (listPlanActionindex.Any(x => x.ActionIndexID == index.ActionIndexID))
                {
                    index.Choosen = Constants.IS_ACTIVE;
                }
            }
            foreach (var index in listPlanActionindex)
            {
                if (index.ActionIndexID == null)
                {
                    listActionIndex.Add(new PlanActionIndexModel
                    {
                        IndexUnitName = index.UnitName,
                        IndexName = index.IndexName,
                        PlanActionIndexID = index.PlanActionIndexID,
                        IndexValue = index.IndexValue,
                        Choosen = Constants.IS_ACTIVE,
                        ActionIndexID = index.ActionIndexID
                    });
                }
                else
                {
                    var actionIndex = listActionIndex.Where(x => x.ActionIndexID == index.ActionIndexID).FirstOrDefault();
                    if (actionIndex != null)
                    {
                        actionIndex.PlanActionIndexID = index.PlanActionIndexID;
                        actionIndex.IndexValue = index.IndexValue;
                        actionIndex.ActionIndexID = index.ActionIndexID;
                    }
                }
            }
            ViewData["listIndex"] = listActionIndex;
            return PartialView("_Step3SubIndexAdd", model);
        }
        public PartialViewResult PrepareEditActionOfTarget(int PlanTargetActionID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            var TargetAction = (from x in context.PlanTargetAction
                                where x.PlanTargetActionID == PlanTargetActionID
                                select new TargetActionModel
                                {
                                    ActionContent = x.ActionContent,
                                    MainActionID = x.IsOtherAction == Constants.IS_ACTIVE ? 0 : x.MainActionID,
                                    ContentPropaganda = x.ContentPropaganda,
                                    ObjectScope = x.ObjectScope,
                                    ActionMethod = x.ActionMethod,
                                    PlanTargetActionID = x.PlanTargetActionID,
                                    ActionCode = x.ActionCode,
                                    ActionOrder = x.ActionOrder,
                                    PlanTargetID = x.PlanTargetID,
                                    PlanID = x.PlanID,
                                    Result = x.Result
                                }).FirstOrDefault();
            if (TargetAction == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            PlanTarget PlanTarget = context.PlanTarget.Where(a => a.PlanTargetID == TargetAction.PlanTargetID).FirstOrDefault();
            if (PlanTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Plan plan = context.Plan.Find(PlanTarget.PlanID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<SelectListItem> listAction = MainActionOfTargets(plan, PlanTarget.TargetID);
            ViewData["TargetName"] = PlanTarget.PlanTargetContent;
            ViewData["listAction"] = listAction;
            return PartialView("_Step3ActionAdd", TargetAction);
        }

        public PartialViewResult PrepareEditSubAction(int SubActionID)
        {
            var SubAction = (from s in context.SubActionPlan
                             join x in context.PlanTargetAction on s.PlanTargetActionID equals x.PlanTargetActionID
                             join t in context.PlanTarget on x.PlanTargetID equals t.PlanTargetID
                             join p in context.Plan on t.PlanID equals p.PlanID
                             join r in context.Period on p.PeriodID equals r.PeriodID
                             where s.SubActionPlanID == SubActionID
                             select new
                             {
                                 Plan = p,
                                 Period = r,
                                 PlanTargetAction = x,
                                 SubActionPlan = s,
                                 PlanTarget = t
                             }).FirstOrDefault();
            if (SubAction == null)
            {
                throw new BusinessException("Không tồn tại hoạt động");
            }
            UserInfo objUser = SessionManager.GetUserInfo();
            if (SubAction.Plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            Period period = SubAction.Period;
            ViewData["period"] = period;
            //Dung tim templateAction theo TargetID
            ViewData["listAction"] = MainActionOfTargets(SubAction.Plan, SubAction.PlanTarget.TargetID, SubAction.PlanTargetAction.MainActionID, true);
            SubActionModel model = new SubActionModel();
            model.PlanActionID = SubAction.PlanTargetAction.PlanTargetActionID;
            SubActionPlan subActionPlan = SubAction.SubActionPlan;
            model.PlanActionContent = SubAction.PlanTargetAction.ActionContent;
            model.SubActionPlanID = subActionPlan.SubActionPlanID;
            model.MainActionID = subActionPlan.IsOtherAction.GetValueOrDefault() == Constants.IS_ACTIVE ? 0 : subActionPlan.SubActionID;
            model.SubActionID = subActionPlan.SubActionID;
            model.SubActionContent = subActionPlan.SubActionContent;
            model.ContentPropaganda = subActionPlan.ContentPropaganda;
            model.SubActionObject = subActionPlan.SubActionObject;
            model.SubActionMethod = subActionPlan.SubActionMethod;
            model.Result = subActionPlan.Result;
            model.SubActionOrder = subActionPlan.SubActionOrder;

            return PartialView("_Step3SubActionAdd", model);
        }

        public PartialViewResult PrepareAddOrEditOwnActionIndex(int planActionIndexID, int planTargetActionID, int? subActionPlanID = null)
        {
            PlanActionIndexModel model = new PlanActionIndexModel();
            model.PlanTargetActionID = planTargetActionID;
            model.SubActionPlanID = subActionPlanID;
            if (planActionIndexID > 0)
            {
                PlanActionIndex entity = context.PlanActionIndex.Find(planActionIndexID);
                if (entity == null)
                {
                    throw new BusinessException("Không tồn tại chỉ số hoạt động");
                }
                model.PlanActionIndexID = entity.PlanActionIndexID;
                model.PlanTargetID = entity.PlanTargetID;
                model.PlanTargetActionID = entity.PlanTargetActionID;
                model.SubActionPlanID = entity.SubActionPlanID;
                model.IndexName = entity.IndexName;
                model.IndexValue = entity.IndexValue;
                model.IndexUnitName = entity.UnitName;
            }
            return PartialView("_AddActionIndex", model);
        }
        [ValidateInput(false)]
        public JsonResult SaveActionOfTarget(TargetActionModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                try
                {
                    PlanTargetAction entity = null;
                    UserInfo objUser = SessionManager.GetUserInfo();
                    PlanTarget objPlanTarget = context.PlanTarget.Where(a => a.PlanID == model.PlanID && a.PlanTargetID == model.PlanTargetID).FirstOrDefault();
                    if (objPlanTarget == null)
                    {
                        throw new BusinessException("Không tồn tại mục tiêu");
                    }
                    //lay len vi tri tiep theo
                    int? ActionOrder = context.PlanTargetAction
                        .Where(a => a.PlanTargetID == objPlanTarget.PlanTargetID && a.PlanID == objPlanTarget.PlanID)
                        .Max(a => a.ActionOrder);
                    if (model.PlanTargetActionID != 0)
                    {
                        entity = context.PlanTargetAction.Find(model.PlanTargetActionID);
                        var oldPos = entity.ActionOrder.GetValueOrDefault();
                        model.UpdateEntity(entity);
                        if (entity.ActionOrder.GetValueOrDefault() != 0
                            && entity.ActionOrder.GetValueOrDefault() != oldPos)
                        {
                            var oldEntity = context.PlanTargetAction.Where(x => x.PlanTargetID == objPlanTarget.PlanTargetID
                            && x.ActionOrder == entity.ActionOrder).FirstOrDefault();
                            if (oldEntity != null)
                            {
                                oldEntity.ActionOrder = oldPos;
                            }
                        }
                        if (entity.ActionOrder == null)
                        {
                            entity.ActionOrder = ActionOrder == null ? 1 : (ActionOrder + 1);
                        }
                        entity.ActionCode = objPlanTarget.TargetCode + "." + entity.ActionOrder;
                        entity.UpdateUserID = objUser.UserID;
                        entity.PlanTargetID = objPlanTarget.PlanTargetID;
                        entity.UpdatedDate = DateTime.Now;
                    }
                    else
                    {
                        entity = model.ToEntity();
                        entity.CreatedDate = DateTime.Now;
                        entity.CreateUserID = objUser.UserID;

                        if (entity.MainActionID == 0)
                        {
                            entity.MainActionID = null;
                        }
                        if (entity.ActionOrder == null)
                        {
                            entity.ActionOrder = ActionOrder == null ? 1 : (ActionOrder + 1);
                        }
                        entity.ActionCode = objPlanTarget.TargetCode + "." + entity.ActionOrder;
                        entity.PlanTargetID = objPlanTarget.PlanTargetID;
                        entity.SumCostType = Constants.SUM_COST_TYPE_TOTAL;
                        this.context.PlanTargetAction.Add(entity);
                    }
                    msg = "Lưu thông tin hoạt động thành công";
                    this.context.SaveChanges();
                    // Order
                    this.UpdatePlanTargetOrder(objPlanTarget);
                    // Xu ly time va cost
                    Plan plan = context.Plan.Find(model.PlanID);
                    Period period = context.Period.Where(x => x.PeriodID == plan.PeriodID).FirstOrDefault();
                    if (objUser.DeptType == Constants.MAIN_TYPE && objUser.DepartmentID == Constants.HEAD_DEPT_ID)
                    {
                        CostEstimateBusiness bus = new CostEstimateBusiness(context);
                        bus.AddPlanCost(period.FromDate.Year, period.ToDate.Year, plan.PlanID, entity.PlanTargetID.Value, entity.MainActionID, entity.PlanTargetActionID);
                    }
                    else
                    {
                        SubActionPlanCostBusiness subBusiness = new SubActionPlanCostBusiness(context);
                        subBusiness.addSubActionPlanCost(period.FromDate.Year, period.ToDate.Year, entity.PlanTargetActionID, entity.PlanTargetID.Value);
                    }

                    SubActionPlanTimeBusiness planTimeBusiness = new SubActionPlanTimeBusiness(context);
                    planTimeBusiness.addSubActionPlanTime(period.FromDate.Year, period.ToDate.Year, entity.PlanTargetActionID);
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    throw new BusinessException("Lỗi trong quá trình lưu thông tin: " + e.Message);
                }

            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }
        public JsonResult SaveSubActionPlan(SubActionModel model)
        {
            PlanTargetAction objPlanAction = context.PlanTargetAction.Find(model.PlanActionID);
            if (objPlanAction == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            PlanTarget planTarget = context.PlanTarget.Find(objPlanAction.PlanTargetID);
            if (planTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            UserInfo user = SessionManager.GetUserInfo();
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, planTarget.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            if (role == null || role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác");
            }
            string parentActionCode = objPlanAction.ActionCode != null ? objPlanAction.ActionCode : "";
            int? maxSubOrder = context.SubActionPlan.Where(x => x.PlanTargetActionID == objPlanAction.PlanTargetActionID).Max(y => y.SubActionOrder);
            int? subActionOrder = maxSubOrder != null ? maxSubOrder + 1 : 1;
            SubActionPlan entity;
            if (model.SubActionPlanID == 0)
            {
                entity = new SubActionPlan();
                entity.CreateDate = DateTime.Now;
            }
            else
            {
                entity = context.SubActionPlan.Find(model.SubActionPlanID);
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                entity.UpdateDate = DateTime.Now;
                var oldPos = entity.SubActionOrder.GetValueOrDefault();
                if (model.SubActionOrder.GetValueOrDefault() != 0
                            && model.SubActionOrder.GetValueOrDefault() != oldPos)
                {
                    var oldEntity = context.SubActionPlan.Where(x => x.PlanTargetActionID == entity.PlanTargetActionID
                    && x.SubActionOrder == entity.SubActionOrder).FirstOrDefault();
                    if (oldEntity != null)
                    {
                        oldEntity.SubActionOrder = oldPos;
                    }
                }
            }
            entity.PlanTargetActionID = objPlanAction.PlanTargetActionID;
            if (model.MainActionID != null && model.MainActionID > 0)
            {
                entity.SubActionID = model.MainActionID;
                entity.IsOtherAction = null;
            }
            else if (model.MainActionID != null && model.MainActionID == 0)
            {
                entity.SubActionID = null;
                entity.IsOtherAction = Constants.IS_ACTIVE;
            }
            else
            {
                entity.SubActionID = null;
                entity.IsOtherAction = null;
            }
            entity.PlanID = planTarget.PlanID;
            entity.SubActionContent = model.SubActionContent;
            entity.SubActionOrder = model.SubActionOrder;
            if (entity.SubActionOrder == null)
            {
                entity.SubActionOrder = subActionOrder;
            }
            entity.SubActionCode = parentActionCode + "." + entity.SubActionOrder;
            entity.SubActionObject = model.SubActionObject;
            entity.SubActionMethod = model.SubActionMethod;
            entity.ContentPropaganda = model.ContentPropaganda;
            entity.SubActionPlanType = Constants.IS_ACTIVE;
            entity.SumCostType = Constants.SUM_COST_TYPE_TOTAL;
            if (model.SubActionPlanID == 0)
            {
                context.SubActionPlan.Add(entity);
            }
            context.SaveChanges();
            Period period = context.Period.Where(x => x.PeriodID == objPlanAction.Plan.PeriodID).FirstOrDefault();
            // Order
            this.UpdatePlanTargetActionOrder(objPlanAction);
            //Them thoi gian va chi phi
            if (user.DeptType == Constants.MAIN_TYPE && user.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                CostEstimateBusiness bus = new CostEstimateBusiness(context);
                bus.AddPlanCost(period.FromDate.Year, period.ToDate.Year, planTarget.PlanID, objPlanAction.PlanTargetID.Value, entity.SubActionID, entity.PlanTargetActionID.Value, entity.SubActionPlanID);
            }
            else
            {
                SubActionPlanCostBusiness subBusiness = new SubActionPlanCostBusiness(context);
                subBusiness.addSubActionPlanCost(period.FromDate.Year, period.ToDate.Year, entity.PlanTargetActionID.Value, entity.SubActionPlanID, objPlanAction.PlanTargetID.Value);
            }

            SubActionPlanTimeBusiness planTimeBusiness = new SubActionPlanTimeBusiness(context);

            planTimeBusiness.addSubActionPlanTime(period.FromDate.Year, period.ToDate.Year, objPlanAction.PlanTargetActionID, entity.SubActionPlanID);

            return Json(new { Type = "SUCCESS", Message = "Thêm hoạt động thành công!" });
        }
        public JsonResult SaveSubIndex(List<PlanActionIndexModel> ActionIndexs, int PlanTargetActionID, int? SubActionPlanID = null)
        {
            if (SubActionPlanID == 0)
            {
                SubActionPlanID = null;
            }
            var objPlanAction = context.PlanTargetAction.Find(PlanTargetActionID);
            List<int> listPlanActionIndexID = ActionIndexs == null ? new List<int>() : ActionIndexs.Where(x => x.PlanActionIndexID > 0).Select(x => x.PlanActionIndexID).ToList();
            var lstPlanActionIndex = context.PlanActionIndex.Where(a => a.PlanTargetActionID == objPlanAction.PlanTargetActionID && a.SubActionPlanID == SubActionPlanID).ToList();
            var lstPlanActionIndexDel = lstPlanActionIndex.Where(x => !listPlanActionIndexID.Contains(x.PlanActionIndexID)).ToList();
            if (ActionIndexs != null && ActionIndexs.Any())
            {
                var lstPlanActionIndexUpdate = lstPlanActionIndex.Where(x => listPlanActionIndexID.Contains(x.PlanActionIndexID)).ToList();
                foreach (var actionIndex in lstPlanActionIndexUpdate)
                {
                    var indexPost = ActionIndexs.Where(x => x.PlanActionIndexID == actionIndex.PlanActionIndexID).FirstOrDefault();
                    if (indexPost != null)
                    {
                        if (indexPost.IndexValue.GetValueOrDefault() == 0)
                        {
                            throw new BusinessException("Bạn chưa nhập giá trị cho chỉ số: " + actionIndex.IndexName);
                        }
                        actionIndex.IndexValue = indexPost.IndexValue.GetValueOrDefault();
                        if (actionIndex.ActionIndexID.GetValueOrDefault() > 0)
                        {
                            var dmActionIndex = context.ActionIndex.Find(actionIndex.ActionIndexID);
                            if (dmActionIndex != null)
                            {
                                actionIndex.IndexName = dmActionIndex.IndexName;
                                actionIndex.UnitName = dmActionIndex.UnitName;
                            }
                        }
                    }
                }
            }
            context.PlanActionIndex.RemoveRange(lstPlanActionIndexDel);
            //khai bao de luu vao csdl
            if (ActionIndexs != null && ActionIndexs.Any())
            {
                ActionIndexs.RemoveAll(x => x.PlanActionIndexID > 0);
                List<int> listActionIndexID = ActionIndexs.Where(x => x.ActionIndexID.GetValueOrDefault() > 0).Select(x => x.ActionIndexID.Value).ToList();
                List<ActionIndex> listActionIndex = context.ActionIndex.Where(x => listActionIndexID.Contains(x.ActionIndexID)).ToList();
                List<PlanActionIndex> lstActionIndexs = (from a in ActionIndexs.Where(x => x.ActionIndexID.GetValueOrDefault() > 0)
                                                         join b in listActionIndex on a.ActionIndexID equals b.ActionIndexID
                                                         select new PlanActionIndex
                                                         {
                                                             IndexValue = a.IndexValue.GetValueOrDefault(),
                                                             ActionIndexID = a.ActionIndexID,
                                                             PlanTargetActionID = objPlanAction.PlanTargetActionID,
                                                             PlanTargetID = objPlanAction.PlanTargetID.Value,
                                                             SubActionPlanID = SubActionPlanID,
                                                             IndexName = b.IndexName,
                                                             UnitName = b.UnitName
                                                         }).ToList();
                if (lstActionIndexs.Any())
                {
                    foreach (var item in lstActionIndexs)
                    {
                        if (item.IndexValue == 0)
                        {
                            throw new BusinessException("Bạn chưa nhập giá trị cho chỉ số: " + item.IndexName);
                        }
                    }
                    context.PlanActionIndex.AddRange(lstActionIndexs);
                }
            }
            context.SaveChanges();
            if (SubActionPlanID != null && SubActionPlanID > 0)
            {
                this.UpdateActionResult(objPlanAction.PlanTargetID.Value, objPlanAction.PlanTargetActionID, SubActionPlanID);
            }
            else
            {
                this.UpdateActionResult(objPlanAction.PlanTargetID.Value, objPlanAction.PlanTargetActionID);
            }
            return Json(new { Type = "SUCCESS", Message = "Thêm chỉ số cho hoạt động thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveOwnActionIndex(PlanActionIndexModel model)
        {
            PlanActionIndex actionIndex = null;
            PlanTargetAction pta = context.PlanTargetAction.Find(model.PlanTargetActionID);
            if (pta == null)
            {
                throw new BusinessException("Không tồn tại hoạt động");
            }
            if (model.PlanActionIndexID > 0)
            {
                actionIndex = context.PlanActionIndex.Find(model.PlanActionIndexID);
                if (actionIndex == null)
                {
                    throw new BusinessException("Không tồn tại chỉ số hoạt động");
                }
                actionIndex.IndexName = model.IndexName;
                actionIndex.UnitName = model.IndexUnitName;
                actionIndex.IndexValue = model.IndexValue.GetValueOrDefault();
            }
            else
            {
                actionIndex = new PlanActionIndex();
                actionIndex.PlanTargetActionID = model.PlanTargetActionID;
                actionIndex.SubActionPlanID = model.SubActionPlanID;
                actionIndex.IndexName = model.IndexName;
                actionIndex.UnitName = model.IndexUnitName;
                actionIndex.IndexValue = model.IndexValue.GetValueOrDefault();
                actionIndex.PlanTargetID = pta.PlanTargetID.Value;
                context.PlanActionIndex.Add(actionIndex);
            }
            context.SaveChanges();
            if (model.SubActionPlanID != null && model.SubActionPlanID > 0)
            {
                this.UpdateActionResult(pta.PlanTargetID.Value, pta.PlanTargetActionID, model.SubActionPlanID);
            }
            this.UpdateActionResult(pta.PlanTargetID.Value, pta.PlanTargetActionID);
            return Json(new { Type = "SUCCESS", Message = "Thêm chỉ số cho hoạt động thành công!" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeletePlanTargetAction(int SubTargetActionID)
        {
            PlanTargetAction planTargetAction = context.PlanTargetAction.Find(SubTargetActionID);
            if (planTargetAction == null)
            {
                throw new BusinessException("Không tồn tại hoạt động đang chọn");
            }
            PlanTarget planTarget = context.PlanTarget.Find(planTargetAction.PlanTargetID);
            if (planTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            this.DelPlanTargetAction(SubTargetActionID);
            string msg = "Xóa hoạt động thành công";
            this.context.SaveChanges();
            this.UpdatePlanTargetOrder(planTarget);
            this.context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteSubAction(int SubTargetActionID)
        {
            SubActionPlan subAction = context.SubActionPlan.Find(SubTargetActionID);
            if (subAction == null)
            {
                throw new BusinessException("Không tồn tại hoạt động đang chọn");
            }
            PlanTargetAction planTargetAction = context.PlanTargetAction.Find(subAction.PlanTargetActionID);
            if (planTargetAction == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            this.DelSubAction(SubTargetActionID);
            string msg = "Xóa hoạt động thành công";
            this.context.SaveChanges();
            this.UpdatePlanTargetActionOrder(planTargetAction);
            this.context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteSubIndex(int PlanActionIndexID)
        {
            string msg = "";
            UserInfo objUser = SessionManager.GetUserInfo();
            PlanActionIndex objSubActionIndex = context.PlanActionIndex.Find(PlanActionIndexID);
            context.PlanActionIndex.Remove(objSubActionIndex);
            msg = "Xóa chỉ số hoạt động thành công";
            this.context.SaveChanges();
            this.UpdateActionResult(objSubActionIndex.PlanTargetID, objSubActionIndex.PlanTargetActionID, objSubActionIndex.SubActionPlanID);
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        private List<PlanCommentModel> GetIndexComment(int PlanID, int PlanTargetID)
        {
            List<PlanCommentModel> lstComments = new List<PlanCommentModel>();
            if (PlanID != 0)
            {
                lstComments = (from x in context.Comment
                               join y in context.PlanTarget on x.ObjectID equals y.PlanTargetID
                               join z in context.User on x.CommentUserID equals z.UserID
                               where y.PlanID == PlanID && y.Status != Constants.IS_NOT_ACTIVE
                               && x.Type == Constants.COMMENT_TYPE_INDEX
                               && y.PlanTargetID == PlanTargetID
                               select new PlanCommentModel
                               {
                                   CommentID = x.CommentID,
                                   Content = x.Content,
                                   UserName = z.UserName,
                                   PlanTargetID = y.PlanTargetID,
                                   CommentUserID = x.CommentUserID,
                                   CommentDate = x.CommentDate,
                                   Type = Constants.COMMENT_TYPE_INDEX
                               }).ToList();
            }
            return lstComments;
        }
        private List<SelectListItem> MainActionOfTargets(Plan plan, int? TargetID, int? MainActionID = null, bool mustByParent = false)
        {
            List<SelectListItem> listAction = new List<SelectListItem>();
            var objUser = SessionManager.GetUserInfo();
            //Kiem tra neu la phong ban thuoc quy            
            if (objUser.DeptType == Constants.MAIN_TYPE && plan.DepartmentID != Constants.HEAD_DEPT_ID && plan.Type == Constants.MAIN_TYPE)
            {
                var IQTemPlateAction = from a in context.MainAction
                                       join b in context.DepartmentAction on a.MainActionID equals b.ActionID
                                       where a.TargetID == TargetID && b.DepartmentID == objUser.DepartmentID && a.Status == Constants.IS_ACTIVE
                                       select a;
                //neu 
                if (MainActionID != null && MainActionID != 0)
                {
                    IQTemPlateAction = IQTemPlateAction.Where(a => a.ParentID == MainActionID);
                    var Actions = from a in IQTemPlateAction
                                  select new SelectListItem
                                  {
                                      Text = a.ActionContent,
                                      Value = a.MainActionID.ToString()
                                  };
                    listAction.AddRange(Actions.ToList());
                }
                else if (!mustByParent)
                {
                    var Actions = from a in IQTemPlateAction
                                  where a.ParentID == null
                                  select new SelectListItem
                                  {
                                      Text = a.ActionContent,
                                      Value = a.MainActionID.ToString()
                                  };
                    listAction.AddRange(Actions.ToList());
                }
            }
            else
            {
                var IQTemPlateAction = context.MainAction
                                        .Where(a => a.TargetID == TargetID && a.Status == Constants.IS_ACTIVE
                                        && (a.IsOther == null || a.IsOther == 0));
                //neu 
                if (MainActionID != null && MainActionID != 0)
                {
                    IQTemPlateAction = IQTemPlateAction.Where(a => a.ParentID == MainActionID);
                    var Actions = from a in IQTemPlateAction
                                  select new SelectListItem
                                  {
                                      Text = a.ActionContent,
                                      Value = a.MainActionID.ToString()
                                  };
                    listAction.AddRange(Actions.ToList());
                }
                else if (!mustByParent)
                {
                    var Actions = from a in IQTemPlateAction
                                  where a.ParentID == null
                                  select new SelectListItem
                                  {
                                      Text = a.ActionContent,
                                      Value = a.MainActionID.ToString()
                                  };
                    listAction.AddRange(Actions.ToList());
                }
            }
            if (listAction.Any())
            {
                listAction.Insert(0, new SelectListItem
                {
                    Text = Constants.OTHER_CHOICE_ACTION,
                    Value = ""
                });
            }
            var otherAction = context.MainAction
                            .Where(a => a.TargetID == TargetID && a.Status == Constants.IS_ACTIVE
                            && a.IsOther == Constants.IS_ACTIVE)
                            .Select(a => new SelectListItem
                            {
                                Text = a.ActionContent,
                                Value = a.MainActionID.ToString()
                            }).FirstOrDefault();
            if (otherAction != null)
            {
                listAction.Add(otherAction);
            }
            return listAction;
        }
        private List<SubActionPlanModel> GetSubAction(int PlanTargetID, int PlanTargetActionID = 0)
        {
            PlanTarget planTarget = context.PlanTarget.Find(PlanTargetID);
            if (planTarget == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            var IQSubAction = (from x in context.SubActionPlan
                               join y in context.PlanTargetAction on x.PlanTargetActionID equals y.PlanTargetActionID
                               where y.PlanTargetID == PlanTargetID && (PlanTargetActionID == 0 || y.PlanTargetActionID == PlanTargetActionID)
                               select new SubActionPlanModel
                               {
                                   SubActionCode = x.SubActionCode,
                                   SubActionContent = x.SubActionContent,
                                   SubActionPlanID = x.SubActionPlanID,
                                   PlanID = x.PlanID,
                                   SubActionOrder = x.SubActionOrder,
                                   PlanTargetActionID = x.PlanTargetActionID,
                                   ContentPropaganda = x.ContentPropaganda,
                                   SubActionObject = x.SubActionObject,
                                   SubActionMethod = x.SubActionMethod,
                                   Result = x.Result
                               });
            List<SubActionPlanModel> listSubAction = IQSubAction.OrderBy(x => x.SubActionOrder).ToList();
            Dictionary<int, string> dicTime = new PlanHistoryBusiness(context).SubActionTimes(planTarget.PlanTargetID, planTarget.PlanID);
            foreach (SubActionPlanModel subAction in listSubAction)
            {
                if (dicTime.ContainsKey(subAction.SubActionPlanID.Value))
                {
                    subAction.ActionTime = dicTime[subAction.SubActionPlanID.Value];
                }
            }
            return listSubAction;
        }
        private List<PlanActionIndexModel> GetSubIndex(int PlanTargetID, int PlanTargetActionID = 0)
        {
            var IQSubAction = (from x in context.PlanActionIndex
                               join t in context.PlanTargetAction on x.PlanTargetActionID equals t.PlanTargetActionID
                               where t.PlanTargetID == PlanTargetID && (PlanTargetActionID == 0 || x.PlanTargetActionID == PlanTargetActionID)
                               orderby x.ActionIndexID
                               select new PlanActionIndexModel
                               {
                                   PlanActionIndexID = x.PlanActionIndexID,
                                   SubActionPlanID = x.SubActionPlanID,
                                   ActionIndexID = x.ActionIndexID,
                                   PlanTargetActionID = x.PlanTargetActionID,
                                   IndexValue = x.IndexValue,
                                   IndexName = x.IndexName,
                                   IndexUnitName = x.UnitName,
                                   PlanTargetID = x.PlanTargetID
                               });
            return IQSubAction.OrderByDescending(x => x.PlanActionIndexID).ToList();
        }
        private List<SelectListItem> GetListType()
        {
            List<SelectListItem> listType = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_CHOICE,
                    Value = Constants.TYPE_INDEX_CHOICE.ToString()
                },
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_INPUT,
                    Value = Constants.TYPE_INDEX_INPUT.ToString()
                },
                new SelectListItem
                {
                    Text = Constants.TYPE_NAME_INDEX_AVERAGE,
                    Value = Constants.TYPE_INDEX_INPUT_AVERAGE.ToString()
                }
            };
            return listType;
        }

        private void UpdateActionResult(int planTargetID, int planTargetActionID, int? subActionPlanID = null)
        {
            List<PlanActionIndexModel> listActionIndex = this.GetSubIndex(planTargetID, planTargetActionID);
            if (subActionPlanID != null && subActionPlanID > 0)
            {
                listActionIndex = listActionIndex.Where(x => x.SubActionPlanID == subActionPlanID).ToList();
            }
            string result = string.Empty;
            for (int i = 0; i < listActionIndex.Count; i++)
            {
                PlanActionIndexModel actionIndex = listActionIndex[i];
                if (i == listActionIndex.Count - 1)
                {
                    result += actionIndex.GetFullInfo();
                }
                else
                {
                    result += actionIndex.GetFullInfo() + "\r\n";
                }
            }
            if (subActionPlanID != null && subActionPlanID > 0)
            {
                SubActionPlan subAction = context.SubActionPlan.Find(subActionPlanID);
                if (subAction == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                subAction.Result = result;
                PlanTargetAction pta = context.PlanTargetAction.Find(subAction.PlanTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                pta.Result = null;
            }
            else
            {
                PlanTargetAction pta = context.PlanTargetAction.Find(planTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                pta.Result = result;
            }
            this.context.SaveChanges();
        }

        private bool CheckActionTime(int planID, out string actionName)
        {
            bool res = true;
            actionName = string.Empty;
            List<int> listPlanTargetActionID = context.PlanTargetAction.Where(x => context.PlanTarget.Any(p => p.PlanTargetID == x.PlanTargetID && p.PlanID == planID))
                .Select(x => x.PlanTargetActionID).ToList();
            List<SubActionPlanTime> listSubTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID != null
            && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
            foreach (int planTargetActionID in listPlanTargetActionID)
            {
                List<SubActionPlanTime> listPlanTargetTime = listSubTime.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
                if (!listPlanTargetTime.Any(x => !string.IsNullOrWhiteSpace(x.PlanTime)))
                {
                    res = false;
                    actionName = context.PlanTargetAction.Find(planTargetActionID).ActionContent;
                    break;
                }
            }
            return res;
        }

        private bool CheckHasAction(int planID)
        {
            var list = context.PlanTargetAction.Where(x => context.PlanTarget.Any(p => p.PlanTargetID == x.PlanTargetID && p.PlanID == planID))
                .GroupBy(x => x.PlanTargetID).Select(x => new { PlanTargetID = x.Key.Value, Count = x.Count() }).ToList();
            foreach (var item in list)
            {
                if (item.Count == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CheckPlanCost(int planID, out string name)
        {
            bool res = true;
            name = string.Empty;
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(p => p.PlanTargetID == x.PlanTargetID && p.PlanID == planID))
                .ToList();
            foreach (PlanTargetAction targetAction in listPlanTargetAction)
            {
                if (targetAction.TotalBudget == null || targetAction.TotalBudget.Value <= 0)
                {
                    res = false;
                    name = targetAction.ActionContent;
                    break;
                }
            }

            return res;
        }

        private bool CheckPlanTargetIndex(int planID)
        {
            return !context.PlanTargetIndex.Where(x => x.PlanID == planID)
                .Any(x => (x.TypeIndex == Constants.TYPE_INDEX_INPUT || x.TypeIndex == Constants.TYPE_INDEX_INPUT_AVERAGE)
                && (x.ValueIndex == null || x.ValueIndex == 0));
        }

        private bool CheckPlanActionIndex(int planID)
        {
            return !context.PlanActionIndex.Where(x => context.PlanTarget.Any(p => p.PlanTargetID == x.PlanTargetID && p.PlanID == planID))
                .Any(x => x.IndexValue == 0);
        }

        private bool CheckActionContent(int planID, out string name)
        {
            name = string.Empty;
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(p => p.PlanTargetID == x.PlanTargetID && p.PlanID == planID)
            && (x.ContentPropaganda == null || x.ObjectScope == null || x.ContentPropaganda.Trim().Equals("") || x.ObjectScope.Trim().Equals("")
            || x.ActionMethod == null || x.Result == null || x.ActionMethod.Trim().Equals("") || x.Result.Trim().Equals(""))).ToList();
            if (listPlanTargetAction.Any())
            {
                List<int> listPlanTargetActionID = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();
                List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanID == planID && x.PlanTargetActionID != null && listPlanTargetActionID.Contains(x.PlanTargetActionID.Value)).ToList();
                foreach (PlanTargetAction pta in listPlanTargetAction)
                {
                    if (!listSubAction.Any(x => x.PlanTargetActionID == pta.PlanTargetActionID))
                    {
                        name = pta.ActionContent;
                        return false;
                    }
                }
                listSubAction = listSubAction.Where(x => (x.ContentPropaganda == null || x.SubActionObject == null || x.ContentPropaganda.Trim().Equals("") || x.SubActionObject.Trim().Equals("")
                    || x.SubActionMethod == null || x.Result == null || x.SubActionMethod.Trim().Equals("") || x.Result.Trim().Equals(""))).ToList();
                if (listSubAction.Any())
                {
                    name = listSubAction[0].SubActionContent;
                }
            }
            return true;
        }

        private void DelPlanTarget(int planTargetID)
        {
            PlanTarget pt = context.PlanTarget.Find(planTargetID);
            if (pt == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<PlanTarget> listPlanTarget = new List<PlanTarget> { pt };
            PlanBusiness bus = new Business.PlanBusiness(context);
            bus.DelPlanTarget(listPlanTarget, pt.PlanID);
        }

        private void DelSubAction(int subActionID)
        {
            SubActionPlan sub = context.SubActionPlan.Find(subActionID);
            if (sub == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int planTargetActionID = sub.PlanTargetActionID.Value;
            PlanTargetAction pt = context.PlanTargetAction.Find(planTargetActionID);
            if (pt == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<SubActionPlan> listAllSubAction = context.SubActionPlan.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            List<SubActionPlan> listSubActionDel = this.GetTreeSubAction(sub, listAllSubAction);
            List<int> listSubActionID = listSubActionDel.Select(x => x.SubActionPlanID).ToList();
            // Chi phi
            List<SubActionPlanCost> listSubCost = context.SubActionPlanCost.Where(x => x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID != null && listSubActionID.Contains(x.SubActionPlanID.Value)).ToList();
            // Chi phi hoat dong cha
            List<SubActionPlanCost> listSubCostRoot = listSubCost.Where(x => x.SubActionPlanID == subActionID && x.SubActionSpendItemID == null).ToList();
            List<SubActionPlanCost> listTargetCost = context.SubActionPlanCost.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            foreach (SubActionPlanCost cost in listTargetCost)
            {
                SubActionPlanCost costUpdate = listSubCostRoot.Where(x => x.PlanYear == cost.PlanYear).FirstOrDefault();
                if (costUpdate != null)
                {
                    cost.TotalPrice = cost.TotalPrice.GetValueOrDefault() - costUpdate.TotalPrice.GetValueOrDefault();
                }
            }
            // Chi phi muc tieu
            context.SubActionPlanCost.RemoveRange(listSubCost);
            // Chi phi theo muc chi
            List<SubActionSpendItem> listSubItemCost = context.SubActionSpendItem.Where(x => x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID != null && listSubActionID.Contains(x.SubActionPlanID.Value)).ToList();
            context.SubActionSpendItem.RemoveRange(listSubItemCost);
            // Thoi gian
            List<SubActionPlanTime> listTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID != null && listSubActionID.Contains(x.SubActionPlanID.Value)).ToList();
            context.SubActionPlanTime.RemoveRange(listTime);
            // Chi tieu hoat dong
            List<PlanActionIndex> listActionIndex = context.PlanActionIndex.Where(x => x.PlanTargetActionID == planTargetActionID && x.SubActionPlanID != null && listSubActionID.Contains(x.SubActionPlanID.Value)).ToList();
            context.PlanActionIndex.RemoveRange(listActionIndex);
            // Hoat dong chau
            context.SubActionPlan.RemoveRange(listSubActionDel);
        }

        private List<SubActionPlan> GetTreeSubAction(SubActionPlan subAction, List<SubActionPlan> listSubAction)
        {
            List<SubActionPlan> listSubRemove = new List<SubActionPlan>();
            List<SubActionPlan> listChild = listSubAction.Where(x => x.ParentID == subAction.SubActionPlanID).ToList();
            if (listChild != null && listChild.Any())
            {
                foreach (SubActionPlan child in listChild)
                {
                    listSubRemove.AddRange(this.GetTreeSubAction(child, listSubAction));
                }
            }
            else
            {
                listSubRemove.Add(subAction);
            }
            return listSubRemove;
        }

        private void DelPlanTargetAction(int planTargetActionID)
        {
            PlanTargetAction pt = context.PlanTargetAction.Find(planTargetActionID);
            if (pt == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            // Chi phi
            List<SubActionPlanCost> listSubCost = context.SubActionPlanCost.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            // Cap nhat chi phi cho muc tieu
            List<SubActionPlanCost> listSubCostRoot = listSubCost.Where(x => x.SubActionPlanID == null && x.SubActionSpendItemID == null).ToList();
            List<SubActionPlanCost> listPlanTargetCost = context.SubActionPlanCost.Where(x => x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null && x.SubActionSpendItemID == null).ToList();
            foreach (SubActionPlanCost targetCost in listPlanTargetCost)
            {
                SubActionPlanCost rootTargetActionCost = listSubCostRoot.Where(x => x.PlanYear == targetCost.PlanYear).FirstOrDefault();
                if (rootTargetActionCost != null)
                {
                    targetCost.TotalPrice = targetCost.TotalPrice.GetValueOrDefault() - rootTargetActionCost.TotalPrice.GetValueOrDefault();
                }
            }
            context.SubActionPlanCost.RemoveRange(listSubCost);
            // Chi phi theo muc chi
            List<SubActionSpendItem> listSubItemCost = context.SubActionSpendItem.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            context.SubActionSpendItem.RemoveRange(listSubItemCost);
            // Thoi gian
            List<SubActionPlanTime> listTime = context.SubActionPlanTime.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            context.SubActionPlanTime.RemoveRange(listTime);
            // Chi tieu hoat dong
            List<PlanActionIndex> listActionIndex = context.PlanActionIndex.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            context.PlanActionIndex.RemoveRange(listActionIndex);
            // Hoat dong chau
            List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanTargetActionID == planTargetActionID).ToList();
            context.SubActionPlan.RemoveRange(listSubAction);
            // Hoat dong con
            context.PlanTargetAction.Remove(pt);
        }

        private void UpdatePlanOrder(Plan plan)
        {
            List<PlanTarget> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == plan.PlanID).OrderBy(o => o.Target.TargetOrder).ThenBy(x => x.PlanTargetID).ToList();
            for (int i = 0; i < listPlanTarget.Count; i++)
            {
                int order = i + 1;
                PlanTarget planTarget = listPlanTarget[i];
                planTarget.TargetOrder = order;
                planTarget.TargetCode = (plan.Type == Constants.MAIN_TYPE) ? "2." + order : "5." + order;
                planTarget.PlanTargetTitle = "Mục tiêu " + order;
                this.UpdatePlanTargetOrder(planTarget);
            }
        }

        private void UpdatePlanTargetOrder(PlanTarget planTarget)
        {
            int? max = int.MaxValue;
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => x.PlanTargetID == planTarget.PlanTargetID).OrderBy(x => x.ActionOrder == null ? max : x.ActionOrder)
                .ThenByDescending(x => x.UpdatedDate == null ? x.CreatedDate : x.UpdatedDate).ToList();
            List<SubActionPlan> listSubActionTarget = context.SubActionPlan.Where(x => context.PlanTargetAction.Any(p => p.PlanTargetActionID == x.PlanTargetActionID && p.PlanTargetID == planTarget.PlanTargetID)).ToList();
            for (int i = 0; i < listPlanTargetAction.Count; i++)
            {
                PlanTargetAction planTargetAction = listPlanTargetAction[i];
                planTargetAction.ActionOrder = i + 1;
                planTargetAction.ActionCode = planTarget.TargetCode + "." + planTargetAction.ActionOrder;
                List<SubActionPlan> listSubAction = listSubActionTarget.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID)
                    .OrderBy(x => x.SubActionOrder == null ? max : x.SubActionOrder)
                    .ThenByDescending(x => x.UpdateDate == null ? x.CreateDate : x.UpdateDate).ToList();
                for (int j = 0; j < listSubAction.Count; j++)
                {
                    SubActionPlan subAction = listSubAction[j];
                    subAction.SubActionOrder = j + 1;
                    subAction.SubActionCode = planTargetAction.ActionCode + "." + subAction.SubActionOrder;
                    this.UpdateSubActionOrder(subAction, listSubAction);
                }
            }
        }

        private void UpdatePlanTargetActionOrder(PlanTargetAction planTargetAction)
        {
            int? max = int.MaxValue;
            List<SubActionPlan> listSubAction = context.SubActionPlan.Where(x => x.PlanTargetActionID == planTargetAction.PlanTargetActionID)
                .OrderBy(x => x.SubActionOrder == null ? max : x.SubActionOrder)
                .ThenByDescending(x => x.UpdateDate == null ? x.CreateDate : x.UpdateDate).ToList();
            for (int i = 0; i < listSubAction.Count; i++)
            {
                SubActionPlan subAction = listSubAction[i];
                subAction.SubActionOrder = i + 1;
                subAction.SubActionCode = planTargetAction.ActionCode + "." + subAction.SubActionOrder;
                this.UpdateSubActionOrder(subAction, listSubAction);
            }
        }
        private void UpdateSubActionOrder(SubActionPlan subAction, List<SubActionPlan> listSubAction)
        {
            int? max = int.MaxValue;
            List<SubActionPlan> listChild = listSubAction.Where(x => x.ParentID == subAction.SubActionPlanID)
                .OrderBy(x => x.SubActionOrder == null ? max : x.SubActionOrder)
                .ThenByDescending(x => x.UpdateDate == null ? x.CreateDate : x.UpdateDate).ToList();
            if (listChild != null && listChild.Any())
            {
                for (int i = 0; i < listChild.Count; i++)
                {
                    SubActionPlan child = listChild[i];
                    child.SubActionOrder = i + 1;
                    child.SubActionCode = subAction.SubActionCode + "." + child.SubActionOrder;
                    this.UpdateSubActionOrder(child, listSubAction);
                }
            }
        }

        [BreadCrumb(ControllerName = "Chi tiết kế hoạch", ActionName = "Mở khóa kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult PrepareUnlockAction(int planID)
        {
            PlanModel plan = (from pl in context.Plan
                              join pr in context.Period on pl.PeriodID equals pr.PeriodID
                              where pl.PlanID == planID
                              && pl.Status >= Constants.PLAN_NEXT_LEVER
                              select new PlanModel
                              {
                                  PeriodID = pr.PeriodID,
                                  PlanID = pl.PlanID,
                                  PeriodName = pr.PeriodName
                              }).FirstOrDefault();
            ViewData["plan"] = plan;
            List<PlanTargetModel> listPlanTarget = GetListAction(planID);
            ViewData["listPlanTarget"] = listPlanTarget;
            return View("_ListAction");
        }

        public List<PlanTargetModel> GetListAction(int PlanID)
        {
            List<ChangeRequestAction> listRequestAction = context.ChangeRequestAction.Where(x => x.PlanID == PlanID).ToList();
            List<PlanTargetModel> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == PlanID).OrderBy(x => x.PlanTargetID)
                    .Select(x => new PlanTargetModel
                    {
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetTitle = x.PlanTargetTitle,
                        TargetCode = x.TargetCode,
                        PlanTargetContent = x.PlanTargetContent,
                        TotalBudget = x.TotalBuget
                    }).ToList();

            List<PlanTargetActionModel> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanID == PlanID && t.PlanTargetID == x.PlanTargetID))
                .OrderBy(x => x.PlanTargetActionID)
                .Select(x => new PlanTargetActionModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent,
                    TotalBudget = x.TotalBudget
                }).ToList();
            foreach (PlanTargetActionModel pta in listPlanTargetAction)
            {
                ChangeRequestAction crActiond = listRequestAction.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).FirstOrDefault();
                if (crActiond != null)
                {
                    pta.IsChosen = true;
                    pta.ApproveStatus = crActiond.ApproveStatus;
                }
            }
            foreach (PlanTargetModel pt in listPlanTarget)
            {
                pt.IsChosen = true;
                pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                if (pt.ListPlanTargetActionModel != null && pt.ListPlanTargetActionModel.Any())
                {
                    if (!pt.ListPlanTargetActionModel.Any(x => x.IsChosen))
                    {
                        pt.IsChosen = false;
                    }
                    if (pt.ListPlanTargetActionModel.Any(x => x.ApproveStatus != null))
                    {
                        pt.ApproveStatus = Constants.IS_ACTIVE;
                    }
                }
            }
            return listPlanTarget;
        }

        public JsonResult SaveUnlockAction(ChangeRequestModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            Plan plan = context.Plan.Find(model.PlanID);
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, plan.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            if (role == null || (role.Role < Constants.ROLE_EDIT && role.Role != Constants.ROLE_AFTER_APPROVE))
            {
                throw new BusinessException("Bạn không có quyền mở khóa kế hoạch này");
            }
            // Xoa du lieu
            List<ChangeRequestAction> listAction = context.ChangeRequestAction.Where(x => x.PlanID == plan.PlanID).ToList();
            if (listAction.Any())
            {
                context.ChangeRequestAction.RemoveRange(listAction);
            }
            List<PlanTargetAction> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanTargetID == x.PlanTargetID && t.PlanID == plan.PlanID)).ToList();
            foreach (int planTargetActionID in model.PlanTargetActionIDs)
            {
                PlanTargetAction pta = listPlanTargetAction.Where(x => x.PlanTargetActionID == planTargetActionID).FirstOrDefault();
                if (pta == null)
                {
                    throw new BusinessException("Hoạt động không thuộc kế hoạch");
                }
                ChangeRequestAction crAction = new ChangeRequestAction();
                crAction.PlanID = plan.PlanID;
                crAction.ApproveStatus = Constants.IS_ACTIVE;
                crAction.PlanTargetActionID = planTargetActionID;
                crAction.PlanTargetID = pta.PlanTargetID.Value;
                context.ChangeRequestAction.Add(crAction);
            }
            context.SaveChanges();
            if (role.Role == Constants.ROLE_EDIT && plan.DepartmentID != Constants.HEAD_DEPT_ID)
            {
                return UnlockPlan(plan.PlanID);
            }
            else if (role.Role == Constants.ROLE_APPROVE && plan.DepartmentID != Constants.HEAD_DEPT_ID)
            {
                return ApproveOrRejectPlan(plan.PlanID, Constants.IS_NOT_ACTIVE);
            }
            return UnlockPlan(plan.PlanID);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}