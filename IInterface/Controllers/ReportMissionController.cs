﻿using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReportMissionController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Báo cáo chỉ số hoạt động", ActionName = "Báo cáo chỉ số hoạt động", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            List<Period> listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            ViewData["listPeriod"] = listPeriod;
            List<SelectListItem> listSpendItem = (List<SelectListItem>)GetSpendItemSelectList();
            ViewData["ListSpendItem"] = listSpendItem;
            return View();
        }
        public List<SelectListItem> GetSpendItemSelectList(int? spendID = null)
        {
            var spentItem = context.SpendItem.ToList();
            // Initialise list and add first "All" item
            List<SelectListItem> options = new List<SelectListItem>();
            // Get the top level parents

            var parents = spentItem.Where(x => (spendID != null && x.ParentID == spendID) || (spendID == null && x.ParentID == null));
            foreach (var parent in parents)
            {
                // Add SelectListItem for the parent
                options.Add(new SelectListItem()
                {
                    Value = parent.SpendItemID.ToString(),
                    Text = parent.SpenContent
                });
                // Get the child items associated with the parent
                var children = spentItem.Where(x => x.ParentID == parent.SpendItemID);
                // Add SelectListItem for each child
                foreach (var child in children)
                {
                    options.Add(new SelectListItem()
                    {
                        Value = child.SpendItemID.ToString(),
                        Text = string.Format("--- {0}", child.SpenContent)
                    });
                }
            }
            return options;
        }


        public PartialViewResult GetListAction(int periodID)
        {
            // Lay danh sach nhiem vu
            List<MissionModel> listMission = context.Mission.Where(x => x.Status == Constants.IS_ACTIVE)
                .Select(x => new MissionModel
                {
                    MissionID = x.MissionID,
                    MissionCode = x.MissionCode,
                    MissionContent = x.MissionContent,
                    MissionName = x.MissionName
                }).ToList();

            List<MainActionModel> listAction = context.MainAction.Where(x => x.Status == Constants.IS_ACTIVE && x.Type == Constants.SUB_TYPE
            && context.Mission.Any(m => m.MissionID == x.MissionID))
            .Select(x => new MainActionModel
            {
                MainActionID = x.MainActionID,
                MissionID = x.MissionID,
                ActionCode = x.ActionCode,
                ActionContent = x.ActionContent
            }).ToList();
            // Tong hop so lieu theo chi so
            List<ActionIndexModel> listActionIndex
                = (from ai in context.ActionIndex
                   join a in context.MainAction on ai.ActionID equals a.MainActionID
                   join pa in context.PlanActionIndex on ai.ActionIndexID equals pa.ActionIndexID
                   join pt in context.PlanTarget on pa.PlanTargetID equals pt.PlanTargetID
                   join pl in context.Plan on pt.PlanID equals pl.PlanID
                   where pl.Type == Constants.SUB_TYPE && pl.PeriodID == periodID && pl.Status == Constants.PLAN_APPROVED
                   group pa by new { ai.ActionIndexID, ai.ActionID, a.ActionCode, a.ActionContent, ai.IndexName, ai.UnitName } into g
                   orderby g.Key.ActionID, g.Key.ActionIndexID
                   select new ActionIndexModel
                   {
                       ActionID = g.Key.ActionID,
                       ActionIndexID = g.Key.ActionIndexID,
                       UnitName = g.Key.UnitName,
                       IndexValue = g.Sum(x => x.IndexValue)
                   }).ToList();

            // Tong theo muc chi
            List<ReportMissionModel> listSpend = (from rm in context.ReportMission
                                                  join s in context.SpendItem on rm.SpendItemID equals s.SpendItemID
                                                  where rm.PeriodID == periodID && s.Status == Constants.IS_ACTIVE
                                                  select new ReportMissionModel
                                                  {
                                                      ReportMissionID = rm.ReportMissionID,
                                                      SpendItemID = rm.SpendItemID,
                                                      SpendItemName = s.SpenContent,
                                                      ActionID = rm.ActionID
                                                  }).ToList();
            List<int> listSpendID = listSpend.Where(x => x.SpendItemID != null).Select(x => x.SpendItemID.Value).ToList();
            var listSpendCost = (from p in context.SubActionSpendItem
                                 join c in context.SubActionPlanCost on p.SubActionSpendItemID equals c.SubActionSpendItemID
                                 join pt in context.PlanTarget on c.PlanTargetID equals pt.PlanTargetID
                                 join pl in context.Plan on pt.PlanID equals pl.PlanID
                                 where pl.Status == Constants.PLAN_APPROVED && pl.PeriodID == periodID && p.SpendItemID != null
                       && listSpendID.Contains(p.SpendItemID.Value)
                                 group c by new { p.SpendItemID } into g
                                 orderby g.Key.SpendItemID
                                 select new
                                 {
                                     g.Key.SpendItemID,
                                     Total = g.Sum(x => x.Quantity == null ? 0 : x.Quantity.Value)
                                 }).ToList();
            foreach (ReportMissionModel spend in listSpend)
            {
                var objCount = listSpendCost.FirstOrDefault(x => x.SpendItemID == spend.SpendItemID);
                if (objCount != null)
                {
                    spend.ReportCount = objCount.Total;
                }
            }
            // Tong hop theo hoat dong
            List<MainActionModel> listActionCount = (from a in context.MainAction
                                                     join pa in context.PlanTargetAction on a.MainActionID equals pa.MainActionID
                                                     join pt in context.PlanTarget on pa.PlanTargetID equals pt.PlanTargetID
                                                     join pl in context.Plan on pt.PlanID equals pl.PlanID
                                                     join c in context.SubActionPlanCost on pa.PlanTargetActionID equals c.PlanTargetActionID
                                                     where pl.Type == Constants.SUB_TYPE && pl.PeriodID == periodID
                                                     && a.Status == Constants.IS_ACTIVE && pl.Status == Constants.PLAN_APPROVED && c.SubActionPlanID == null && c.SubActionSpendItemID == null
                                                     group c by new { a.MainActionID, a.ActionCode, a.ActionContent } into g
                                                     orderby g.Key.MainActionID
                                                     select new MainActionModel
                                                     {
                                                         MainActionID = g.Key.MainActionID,
                                                         ActionCode = g.Key.ActionCode,
                                                         ActionContent = g.Key.ActionContent,
                                                         ReportCount = g.Sum(x => x.Quantity)
                                                     }).ToList();
            listActionCount.AddRange((from a in context.MainAction
                                      join su in context.SubActionPlan on a.MainActionID equals su.SubActionID
                                      join pa in context.PlanTargetAction on su.PlanTargetActionID equals pa.PlanTargetActionID
                                      join pt in context.PlanTarget on pa.PlanTargetID equals pt.PlanTargetID
                                      join pl in context.Plan on pt.PlanID equals pl.PlanID
                                      join c in context.SubActionPlanCost on su.PlanTargetActionID equals c.PlanTargetActionID
                                      where pl.Type == Constants.SUB_TYPE && pl.PeriodID == periodID
                                      && a.Status == Constants.IS_ACTIVE && pl.Status == Constants.PLAN_APPROVED && c.SubActionSpendItemID == null
                                      group c by new { a.MainActionID, a.ActionCode, a.ActionContent } into g
                                      orderby g.Key.MainActionID
                                      select new MainActionModel
                                      {
                                          MainActionID = g.Key.MainActionID,
                                          ActionCode = g.Key.ActionCode,
                                          ActionContent = g.Key.ActionContent,
                                          ReportCount = g.Sum(x => x.Quantity)
                                      }).ToList());
            // Gan lai thong tin so luong
            foreach (MainActionModel action in listAction)
            {
                action.ListActionIndex = listActionIndex.Where(x => x.ActionID == action.MainActionID).ToList();
                action.ListSpendItem = listSpend.Where(x => x.ActionID == action.MainActionID).ToList();
                action.ReportCount = listActionCount.Where(x => x.MainActionID == action.MainActionID).Select(x => x.ReportCount).FirstOrDefault();
            }
            foreach (MissionModel mission in listMission)
            {
                mission.ListAction = listAction.Where(x => x.MissionID == mission.MissionID && x.ParentID == null).ToList();
                if (mission.ListAction != null && mission.ListAction.Any())
                {
                    foreach (MainActionModel action in mission.ListAction)
                    {
                        this.AddSubAction(action, listAction);
                    }
                }
            }
            ViewData["listMission"] = listMission;
            return PartialView("_LstResult");
        }

        public JsonResult AddSpendItem(int hPeriodID, int actionID, List<int> spendItemIDs)
        {
            MainAction action = context.MainAction.Where(x => x.MainActionID == actionID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (action == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (action.MissionID == null)
            {
                throw new BusinessException("Hoạt động chưa được gắn với nhiệm vụ");
            }
            List<ReportMission> listReportMission = context.ReportMission.Where(x => x.PeriodID == hPeriodID
            && x.ActionID == actionID).ToList();
            spendItemIDs.RemoveAll(x => listReportMission.Any(r => r.SpendItemID == x));
            spendItemIDs = spendItemIDs.Distinct().ToList();
            foreach (int spendItemID in spendItemIDs)
            {
                ReportMission entity = new ReportMission();
                entity.PeriodID = hPeriodID;
                entity.ActionID = actionID;
                entity.MissionID = action.MissionID.Value;
                entity.SpendItemID = spendItemID;
                context.ReportMission.Add(entity);
            }
            context.SaveChanges();
            return Json("Thêm mục chi vào báo cáo thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelSpendItem(int reportMissionID)
        {
            ReportMission entity = context.ReportMission.Find(reportMissionID);
            context.ReportMission.Remove(entity);
            context.SaveChanges();
            return Json("Loại bỏ mục chi khỏi báo cáo thành công", JsonRequestBehavior.AllowGet);
        }

        private void AddSubAction(MainActionModel action, List<MainActionModel> listAction)
        {
            action.ListChildAction = listAction.Where(x => x.ParentID == action.MainActionID).ToList();
            if (action.ListChildAction != null && action.ListChildAction.Any())
            {
                foreach (MainActionModel child in action.ListChildAction)
                {
                    this.AddSubAction(child, listAction);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}