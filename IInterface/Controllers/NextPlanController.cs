﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class NextPlanController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Hoạt động kéo dài", ActionName = "Hoạt động kéo dài", AreaName = "Kế hoạch")]
        public ActionResult Index(int? type)
        {
            List<SelectListItem> listPeriod =
                (from x in this.context.Period
                 where x.Status != Constants.PERIOD_DEACTIVED
                 orderby x.CreateDate descending
                 select new SelectListItem()
                 {
                     Value = "" + x.PeriodID,
                     Text = x.PeriodName
                 }).ToList();
            ViewData["listPeriod"] = listPeriod;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PeriodID"></param>
        /// <param name="IsAll"></param>
        /// <returns></returns>
        public PartialViewResult LoadAction(int PeriodID, int? IsAll)
        {
            Period period = context.Period.Where(x => x.Status != Constants.IS_NOT_ACTIVE && x.PeriodID == PeriodID).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Không tồn tại giai đoạn");
            }
            int reportYear = period.ToDate.Year;
            ViewData["reportYear"] = reportYear;
            List<PlanTargetActionModel> listPlanTarget = (from a in context.PlanTargetAction
                                                          join t in context.PlanTarget on a.PlanTargetID equals t.PlanTargetID
                                                          join p in context.Plan on t.PlanID equals p.PlanID
                                                          join d in context.Department on p.DepartmentID equals d.DepartmentID
                                                          where p.PeriodID == PeriodID
                                                          && d.Status == Constants.IS_ACTIVE && d.DepartmentType == Constants.DEPT_TYPE_PBQUY
                                                          select new PlanTargetActionModel
                                                          {
                                                              PlanTargetActionID = a.PlanTargetActionID,
                                                              ActionCode = a.ActionCode,
                                                              ActionContent = a.ActionContent,
                                                              DeptName = d.DepartmentName,
                                                              TotalBudget = a.TotalBudget,
                                                              RemainMoney = a.MoneyNextPlan,
                                                              IsNextPlan = a.IsNextPlan
                                                          }).ToList();
            List<PlanTargetActionModel> listFinReportDetail = (from a in context.SettleSynthesis
                                                               join b in context.SettleSynthesisCost on a.SettleSynthesisID equals b.SettleSynthesisID
                                                               join p in context.Plan on a.PlanID equals p.PlanID
                                                               where p.PeriodID == PeriodID && a.Status != Constants.IS_NOT_ACTIVE
                                                               && b.SubPlanActionID == null && b.PlanTargetActionID != null && b.ParentID == null && b.SubActionSpendItemID == null
                                                               group b by new { b.PlanTargetActionID } into g
                                                               select new PlanTargetActionModel
                                                               {
                                                                   PlanTargetActionID = g.Key.PlanTargetActionID.Value,
                                                                   TotalSettleMoney = g.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum()
                                                               }).ToList();
            if (IsAll.GetValueOrDefault() == Constants.IS_NOT_ACTIVE)
            {
                listPlanTarget.RemoveAll(x => x.TotalBudget == x.TotalSettleMoney);
            }
            foreach (PlanTargetActionModel pta in listPlanTarget)
            {
                PlanTargetActionModel ptaReport = listFinReportDetail.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).FirstOrDefault();
                if (ptaReport != null)
                {
                    pta.TotalSettleMoney = ptaReport.TotalSettleMoney;
                    if (pta.RemainMoney == null)
                    {
                        pta.RemainMoney = pta.TotalBudget - pta.TotalSettleMoney;
                    }
                }
            }
            ViewData["listResult"] = listPlanTarget;
            return PartialView("_LstResult");
        }

        public JsonResult SaveNextPlan(List<string> ArrMoney, List<string> ArrPlanTargetID, List<string> ArrPlanTargetSelectedID)
        {
            Dictionary<int, double> dicActionMoney = new Dictionary<int, double>();
            for (int i = 0; i < ArrPlanTargetID.Count; i++)
            {
                if (ArrPlanTargetSelectedID.Contains(ArrPlanTargetID[i]))
                {
                    int ptaID;
                    if (int.TryParse(ArrPlanTargetID[i], out ptaID))
                    {
                        if (!string.IsNullOrWhiteSpace(ArrMoney[i]))
                        {
                            double money;
                            if (double.TryParse(ArrMoney[i], out money))
                            {
                                dicActionMoney[ptaID] = money;
                            }
                            else
                            {
                                throw new BusinessException("Số tiền phải là dữ liệu kiểu số");
                            }
                        }
                    }
                    else
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }
                }
            }
            if (!dicActionMoney.Any())
            {
                throw new BusinessException("Bạn chưa chọn hoạt động nào");
            }
            List<int> listPlanTargetActionID = dicActionMoney.Keys.ToList();
            var listPlanTargetAction = (from pr in context.Period
                                        join pl in context.Plan on pr.PeriodID equals pl.PeriodID
                                        join pt in context.PlanTarget on pl.PlanID equals pt.PlanID
                                        join pa in context.PlanTargetAction on pt.PlanTargetID equals pa.PlanTargetID
                                        where listPlanTargetActionID.Contains(pa.PlanTargetActionID)
                                        && pr.Status != Constants.IS_NOT_ACTIVE && pl.Status != Constants.IS_NOT_ACTIVE
                                        select new { PlanTargetAction = pa, pr.PeriodID }).ToList();
            if (!listPlanTargetAction.Any())
            {
                throw new BusinessException("Bạn chưa chọn hoạt động nào");
            }
            int periodID = listPlanTargetAction.First().PeriodID;
            foreach (var pta in listPlanTargetAction)
            {
                if (periodID != pta.PeriodID)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                pta.PlanTargetAction.IsNextPlan = Constants.IS_ACTIVE;
                pta.PlanTargetAction.MoneyNextPlan = dicActionMoney[pta.PlanTargetAction.PlanTargetActionID];
            }
            context.SaveChanges();
            return Json("Cập nhật hoạt động chuyển sang giai đoạn tiếp theo thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}