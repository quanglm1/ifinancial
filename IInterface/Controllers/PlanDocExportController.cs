﻿using DocumentFormat.OpenXml.Wordprocessing;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System.Linq;
using System.Collections.Generic;
using IInterface.Common.OpenXML;
using System.Web.Mvc;
using System;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using IInterface.Business;

namespace IInterface.Controllers
{
    public class PlanDocExportController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        public FileResult ExportPLanPL01(int planID)
        {
            PlanModel plan = (from p in this.context.Plan
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              where p.PlanID == planID && p.Status != Constants.PLAN_DEACTIVED
                              select new PlanModel
                              {
                                  PeriodID = p.PeriodID,
                                  PlanID = p.PlanID,
                                  DepartmentID = p.DepartmentID,
                                  DepartmentName = d.DepartmentName,
                                  Address = d.Address,
                                  PhoneNumber = d.PhoneNumber,
                                  Fax = d.Fax,
                                  AccountNumber = d.AccountNumber,
                                  BankName = d.BankName,
                                  RepresentPersonName = p.RepresentPersonName,
                                  RepresentPosition = p.RepresentPosition,
                                  RepresentAddress = p.RepresentAddress,
                                  RepresentPhone = p.RepresentPhone,
                                  RepresentEmail = p.RepresentEmail,
                                  ContactPersonName = p.ContactPersonName,
                                  ContactPosition = p.ContactPosition,
                                  ContactAddress = p.ContactAddress,
                                  ContactPhone = p.ContactPhone,
                                  ContactEmail = p.ContactEmail,
                                  PlanContent = p.PlanContent,
                                  FromDate = pr.FromDate,
                                  ToDate = pr.ToDate,
                                  PlanTarget = p.PlanTarget,
                                  Implementation = p.Implementation,
                                  Monitor = p.Monitor,
                                  CostManagement = p.CostManagement
                              }).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch được chọn");
            }
            List<PlanTargetModel> listPlanTarget = this.GetListAction(planID);
            List<Paragraph> listParagraph = new List<Paragraph>();
            listParagraph.AddRange(this.GenGeneralInfo(plan));
            listParagraph.AddRange(this.GenActionContent(plan, listPlanTarget));
            byte[] data = this.GenDocPL01(listParagraph, this.GenSignArea(plan));
            return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, "PL01_KH_Donvi_" + plan.DepartmentName + ".docx");
        }

        private byte[] GenDocPL01(List<Paragraph> listParagraph, Table tableSign)
        {
            // Tao file moi tu file cu
            string newFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".docx";
            string newFilePath = Path.Combine(Server.MapPath("~/TempFile"), newFile);
            string templateFilePath = Path.Combine(Server.MapPath("~/Template"), "PL01_DonVi.docx");
            System.IO.File.Copy(templateFilePath, newFilePath);
            using (WordprocessingDocument doc = WordprocessingDocument.Open(newFilePath, true))
            {
                MainDocumentPart mainPart = doc.MainDocumentPart;
                Paragraph firstP = mainPart.Document.Body.GetFirstChild<Paragraph>();
                if (firstP != null)
                {
                    firstP.Remove();
                }
                foreach (Paragraph p in listParagraph)
                {
                    mainPart.Document.Body.AppendChild(p);
                }
                Paragraph pNewLine = new Paragraph();
                mainPart.Document.Body.AppendChild(pNewLine);
                mainPart.Document.Body.AppendChild(tableSign);

                SectionProperties sectionProperties = new SectionProperties() { RsidRPr = "006647C9", RsidR = "003E08D4", RsidSect = "00965A25" };
                FooterReference footerReference = new FooterReference() { Type = HeaderFooterValues.Default, Id = "rId8" };
                PageSize pageSize = new PageSize() { Width = (int)11907U, Height = (int)16840U, Code = (int)9U };
                PageMargin pageMargin = new PageMargin() { Top = 864, Right = (int)994U, Bottom = 432, Left = (int)1642U, Header = (int)0U, Footer = (int)346U, Gutter = (int)0U };
                Columns columns = new Columns() { Space = "720" };
                DocGrid docGrid = new DocGrid() { LinePitch = 360 };

                sectionProperties.Append(footerReference);
                sectionProperties.Append(pageSize);
                sectionProperties.Append(pageMargin);
                sectionProperties.Append(columns);
                sectionProperties.Append(docGrid);
                mainPart.Document.Body.AppendChild(sectionProperties);
            }
            return System.IO.File.ReadAllBytes(newFilePath);
        }

        private Table GenSignArea(PlanModel plan)
        {
            Table table = new Table();
            TableProperties tableProperties = new TableProperties();
            TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };
            TableLook tableLook = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };

            tableProperties.Append(tableWidth);
            tableProperties.Append(tableLook);

            TableGrid tableGrid = new TableGrid();
            GridColumn gridColumn1 = new GridColumn() { Width = "3714" };
            GridColumn gridColumn2 = new GridColumn() { Width = "5557" };

            tableGrid.Append(gridColumn1);
            tableGrid.Append(gridColumn2);
            table.Append(tableProperties);
            table.Append(tableGrid);

            TableRow tr = new TableRow();
            TableCell c1 = new TableCell();
            c1.AddColWidth(33);
            Paragraph p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "Nơi nhận:", isBold: true, isItalic: true);
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Quỹ PCTH thuốc lá");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Lưu");
            c1.Append(p);
            tr.Append(c1);
            TableCell c2 = new TableCell();
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276");
            p.AddText(text: plan.DepartmentName.Trim().ToUpper(), isBold: true, isNewLine: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.RepresentPosition) ? string.Empty : plan.RepresentPosition.Trim()), isBold: true);
            p.AddNewLine(5);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.RepresentPersonName) ? string.Empty : plan.RepresentPersonName.Trim()), isBold: true);
            c2.Append(p);
            tr.Append(c2);
            table.Append(tr);
            return table;
        }

        private List<Paragraph> GenActionContent(PlanModel plan, List<PlanTargetModel> listPlanTarget)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            listParagraph.Add(this.CreateBasicParagraph(text: "II. NỘI DUNG HOẠT ĐỘNG", isBold: true));
            listParagraph.Add(this.CreateBasicParagraph(text: "1. Cơ sở đề xuất hoạt động:", isBold: true));
            listParagraph.AddRange(this.GenListParagraphFromContent(plan.PlanContent));
            Paragraph p = new Paragraph();
            p.AddBasicSpacing();
            p.AddText(text: "2. Thời gian thực hiện: ", isBold: true);
            p.AddText(text: "Từ tháng " + plan.FromDate.ToString("MM/yyyy") + " – " + plan.ToDate.ToString("MM/yyyy"));
            listParagraph.Add(p);
            listParagraph.Add(this.CreateBasicParagraph(text: "3. Mục tiêu chung:", isBold: true));
            listParagraph.AddRange(this.GenListParagraphFromContent(plan.PlanTarget));
            listParagraph.Add(this.CreateBasicParagraph(text: "4. Mục tiêu cụ thể:", isBold: true));
            for (int i = 1; i <= listPlanTarget.Count; i++)
            {
                PlanTargetModel target = listPlanTarget[i - 1];
                string title = "Mục tiêu " + i + ". ";
                p = new Paragraph();
                p.AddBasicSpacing("360");
                p.AddText(text: title, isBold: true, isItalic: true);
                p.AddText(text: target.PlanTargetContent);
                listParagraph.Add(p);
            }
            listParagraph.Add(this.CreateBasicParagraph(text: "5. Các chỉ số và hoạt động:", isBold: true));
            for (int i = 1; i <= listPlanTarget.Count; i++)
            {
                PlanTargetModel target = listPlanTarget[i - 1];
                string title = "5." + i + ". Mục tiêu " + i + ". ";
                p = new Paragraph();
                p.AddBasicSpacing("360");
                p.AddText(text: title, isBold: true, isItalic: true);
                p.AddText(text: target.PlanTargetContent);
                listParagraph.Add(p);
                listParagraph.Add(this.CreateBasicParagraph(text: "Chỉ số:", isBold: true, left: "360"));
                List<TargetIndexModel> listIndex = target.ListPlanTargetIndex;
                if (listIndex != null)
                {
                    for (int j = 1; j <= listIndex.Count; j++)
                    {
                        TargetIndexModel index = listIndex[j - 1];
                        p = new Paragraph();
                        p.AddBasicSpacing("360");
                        double? value = index.ValueIndex;
                        string contentIndex = index.ContentIndex.Trim();
                        if (contentIndex.StartsWith("-"))
                        {
                            contentIndex = contentIndex.Substring(1).Trim();
                        }
                        if (value != null)
                        {
                            if (index.TypeIndex.GetValueOrDefault() != Constants.TYPE_INDEX_CHOICE)
                            {
                                if (index.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_INPUT_AVERAGE)
                                {
                                    if (contentIndex.StartsWith("%"))
                                    {
                                        contentIndex = contentIndex.Substring(1).Trim();
                                    }
                                    contentIndex = value.Value.ToString() + "% " + contentIndex;
                                }
                                else
                                {
                                    string firtChar = contentIndex.First().ToString().ToLower();
                                    contentIndex = value.Value.ToString() + " " + firtChar + contentIndex.Substring(1);
                                }
                            }
                        }
                        p.AddText(text: "- " + contentIndex);
                        listParagraph.Add(p);
                    }
                }
                List<PlanTargetActionModel> listPlanTargetAction = target.ListPlanTargetActionModel;
                List<int> listPlanTargetActionID = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();
                PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                Dictionary<int, string> dicTime = bus.ActionTimes(listPlanTargetActionID, plan.PlanID.Value);
                if (listPlanTargetAction != null)
                {
                    listParagraph.Add(this.CreateBasicParagraph(text: "Hoạt động:", isBold: true, left: "360"));
                    for (int j = 1; j <= listPlanTargetAction.Count; j++)
                    {
                        title = "5." + i + "." + j;
                        PlanTargetActionModel targetAction = listPlanTargetAction[j - 1];
                        targetAction.ActionCode = title;
                        targetAction.ActionTime = dicTime[targetAction.PlanTargetActionID];
                        listParagraph.AddRange(this.GenOnePlanTargetAction(targetAction));
                    }
                }
            }
            // Lay doan cuoi khong co noi dung
            p = new Paragraph();
            p.AddBasicSpacing();
            p.AddText(text: "6. Kế hoạch triển khai dự án: ", isBold: true);
            p.AddText(text: "Phụ lục 02");
            listParagraph.Add(p);
            Dictionary<int, double> dicCost = new PlanBusiness(context).GetPlanCostByYear(plan.PlanID.Value);
            long totalMoney = dicCost.Values.Select(x => Convert.ToInt64(x)).Sum();
            p = new Paragraph();
            p.AddBasicSpacing();
            p.AddLeft();
            p.AddText(text: "7. Ngân sách của dự án: " + Utils.FormatMoney(totalMoney, true) + "đ", isBold: true);
            p.AddNewLine();
            p.AddTab();
            p.AddText(text: "(" + Utils.ChuyenSo(totalMoney.ToString()) + ")", isBold: true, isItalic: true);
            p.AddNewLine();
            p.AddText(text: "Kinh phí chi tiết của dự án tại Phụ lục 03");
            listParagraph.Add(p);
            if (plan.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                listParagraph.AddRange(GenListParagraphFromContent(plan.Implementation));
            }
            else
            {
                p = new Paragraph();
                p.AddBasicSpacing();
                p.AddLeft();
                p.AddText(text: "8. Tổ chức quản lý và theo dõi thực hiện hoạt động", isBold: true);
                listParagraph.Add(p);
                listParagraph.AddRange(GenListParagraphFromContent(plan.Monitor));
                p = new Paragraph();
                p.AddBasicSpacing();
                p.AddLeft();
                p.AddText(text: "9. Tổ chức quản lý tài chính", isBold: true);
                listParagraph.Add(p);
                listParagraph.AddRange(GenListParagraphFromContent(plan.CostManagement));
            }
            return listParagraph;
        }

        private List<Paragraph> GenGeneralInfo(PlanModel plan)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            Paragraph p = new Paragraph();
            p.AddRight();
            p.AddText("Phụ lục 01", isBold: true);
            p.AddNewLine();
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "240", after: "100");
            p.AddText(text: "KẾ HOẠCH HOẠT ĐỘNG PHÒNG CHỐNG TÁC HẠI CỦA THUỐC LÁ " + plan.DepartmentName.Trim().ToUpper() + " GIAI ĐOẠN " + plan.FromDate.Year + " – " + plan.ToDate.Year, isBold: true, fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "240", after: "100");
            p.AddText("(Kèm theo Hợp đồng số ………………………… ngày   /   /" + plan.FromDate.Year + ")", isItalic: true);
            listParagraph.Add(p);
            listParagraph.Add(this.CreateBasicParagraph(text: "I. THÔNG TIN VỀ CƠ QUAN, ĐƠN VỊ ĐỀ NGHỊ HỖ TRỢ", isBold: true));
            listParagraph.Add(this.CreateBasicParagraph(text: "1. Tên tổ chức: " + plan.DepartmentName.Trim().ToUpper(), isBold: true));
            listParagraph.Add(this.CreateBasicParagraph(text: "2. Địa chỉ: " + (string.IsNullOrWhiteSpace(plan.Address) ? string.Empty : plan.Address.Trim()), isBold: true));
            p = new Paragraph();
            p.AddBasicSpacing();
            p.AddTab();
            p.AddText(text: "Điện thoại: ", isBold: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.PhoneNumber) ? string.Empty : plan.PhoneNumber.Trim()));
            p.AddText(text: " – ");
            p.AddText(text: "Fax: ", isBold: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.Fax) ? string.Empty : plan.Fax.Trim()));
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddSpacing(line: "240", after: "80");
            p.AddText(text: "3. Tài khoản: ", isBold: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.AccountNumber) ? string.Empty : plan.AccountNumber.Trim()));
            p.AddText(text: " – ");
            p.AddText(text: (string.IsNullOrWhiteSpace(plan.BankName) ? string.Empty : plan.BankName.Trim()));
            listParagraph.Add(p);
            listParagraph.Add(this.CreateBasicParagraph(text: "4. Người chịu trách nhiệm chính: ", isBold: true));
            listParagraph.Add(this.AddBasicBullet("Họ và tên: ", plan.RepresentPersonName));
            listParagraph.Add(this.AddBasicBullet("Chức vụ: ", plan.RepresentPosition));
            string contactRes = (string.IsNullOrWhiteSpace(plan.RepresentAddress) ? string.Empty : plan.RepresentAddress.Trim());
            if (!string.IsNullOrWhiteSpace(contactRes))
            {
                contactRes += "; ";
            }
            contactRes += (string.IsNullOrWhiteSpace(plan.RepresentPhone) ? string.Empty : plan.RepresentPhone.Trim());
            listParagraph.Add(this.AddBasicBullet("Địa chỉ liên lạc và điện thoại: ", contactRes));
            // Dau moi
            p = new Paragraph();
            p.AddSpacing(line: "240", after: "80");
            p.AddText(text: "5. Cán bộ đầu mối: ", isBold: true);
            listParagraph.Add(p);
            listParagraph.Add(this.AddBasicBullet("Họ và tên: ", plan.ContactPersonName));
            listParagraph.Add(this.AddBasicBullet("Chức vụ: ", plan.ContactPosition));
            contactRes = (string.IsNullOrWhiteSpace(plan.ContactAddress) ? string.Empty : plan.ContactAddress.Trim());
            if (!string.IsNullOrWhiteSpace(contactRes))
            {
                contactRes += "; ";
            }
            contactRes += (string.IsNullOrWhiteSpace(plan.ContactPhone) ? string.Empty : plan.ContactPhone.Trim());
            listParagraph.Add(this.AddBasicBullet("Địa chỉ liên lạc và điện thoại: ", contactRes));
            return listParagraph;
        }

        private Paragraph CreateBasicParagraph(string text, bool isBold = false, bool isItalic = false, string left = null)
        {
            Paragraph p = new Paragraph();
            p.AddBasicSpacing(left);
            p.AddText(text: text, isBold: isBold, isItalic: isItalic);
            return p;
        }

        private Paragraph AddBasicBullet(string title, string text)
        {
            Paragraph p = new Paragraph();
            p.AddBasicBullet(val: 3);
            p.AddBasicSpacing();
            p.AddText(text: title, isItalic: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(text) ? string.Empty : text.Trim()));
            return p;
        }

        private List<Paragraph> GenOnePlanTargetAction(PlanTargetActionModel targetAction)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            if (!string.IsNullOrWhiteSpace(targetAction.ActionContent))
            {
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_TITLE));
            }
            if (targetAction.ListSubActionPlan != null)
            {
                List<int> listSubActionID = targetAction.ListSubActionPlan.Select(x => x.SubActionPlanID.GetValueOrDefault()).ToList();
                Dictionary<int, string> dicSubActionTime = new PlanHistoryBusiness(context).SubActionTimes(targetAction.PlanTargetID.Value, targetAction.PlanID.Value);
                for (int i = 1; i <= targetAction.ListSubActionPlan.Count; i++)
                {
                    SubActionPlanModel sub = targetAction.ListSubActionPlan[i - 1];
                    string title = targetAction.ActionCode + "." + i;
                    PlanTargetActionModel pta = new PlanTargetActionModel();
                    pta.ActionCode = title;
                    pta.ActionContent = sub.SubActionContent;
                    pta.ContentPropaganda = sub.ContentPropaganda;
                    pta.ObjectScope = sub.SubActionObject;
                    pta.ActionMethod = sub.SubActionMethod;
                    pta.Result = sub.Result;
                    if (dicSubActionTime.ContainsKey(sub.SubActionPlanID.Value))
                    {
                        pta.ActionTime = dicSubActionTime[sub.SubActionPlanID.Value];
                    }
                    listParagraph.AddRange(this.GenOnePlanTargetAction(pta));
                }
            }
            if (!string.IsNullOrWhiteSpace(targetAction.ActionContent) && (targetAction.ListSubActionPlan == null || targetAction.ListSubActionPlan.Count == 0))
            {
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_CONTENT));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_OBJECT));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_METHOD));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_RESULT));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_TIME));
            }
            return listParagraph;
        }

        private List<Paragraph> GenOnePlanTargetActionContent(PlanTargetActionModel targetAction, int type)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            Paragraph p = new Paragraph();
            p.AddBasicSpacing();
            p.AddIndentation("360");
            bool isBold = (type == Constants.PARAGRAPH_TARGET_ACTION_TITLE);
            string content = string.Empty;
            string title = string.Empty;
            switch (type)
            {
                case Constants.PARAGRAPH_TARGET_ACTION_TITLE:
                    content = ((string.IsNullOrWhiteSpace(targetAction.ActionCode) || targetAction.ActionContent.Trim().StartsWith(targetAction.ActionCode)) ? targetAction.ActionContent.Trim() : targetAction.ActionCode.Trim()) + ". " + targetAction.ActionContent.Trim();
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_CONTENT:
                    title = "Nội dung: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ContentPropaganda) ? string.Empty : targetAction.ContentPropaganda.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_OBJECT:
                    title = "Đối tượng, phạm vi: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ObjectScope) ? string.Empty : targetAction.ObjectScope.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_METHOD:
                    title = "Phương thức thực hiện: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ActionMethod) ? string.Empty : targetAction.ActionMethod.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_RESULT:
                    title = "Kết quả dự kiến: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.Result) ? string.Empty : targetAction.Result.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_TIME:
                    title = "Thời gian: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ActionTime) ? string.Empty : targetAction.ActionTime.Trim());
                    break;
            }
            if (!string.IsNullOrWhiteSpace(content) && !content.EndsWith("."))
            {
                content += ".";
            }
            if (!string.IsNullOrWhiteSpace(title))
            {
                if (!isBold && (content.Contains("\r\n") || content.Contains("\n")))
                {
                    p.AddText(text: title.Trim(), isUnderline: true);
                }
                else
                {
                    p.AddText(text: title, isUnderline: true);
                }
                listParagraph.Add(p);
            }

            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!isBold && (content.Contains("\r\n") || content.Contains("\n")))
                {
                    if (type == Constants.PARAGRAPH_TARGET_ACTION_RESULT)
                    {
                        listParagraph.AddRange(this.GenListParagraphFromContent(content, true));
                    }
                    else
                    {
                        listParagraph.AddRange(this.GenListParagraphFromContent(content));
                    }
                }
                else
                {
                    if (type == Constants.PARAGRAPH_TARGET_ACTION_RESULT)
                    {
                        content = FormatResult(content);
                    }
                    else
                    {
                        content = FormatHead(content);
                    }
                    p.AddText(text: content, isBold: isBold);
                    if (string.IsNullOrWhiteSpace(title))
                    {
                        listParagraph.Add(p);
                    }
                }
            }
            return listParagraph;
        }

        private string FormatHead(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return string.Empty;
            }
            if (s.Length == 1)
            {
                return s.ToUpper();
            }
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        private string FormatResult(string s)
        {
            s = FormatHead(s);
            var arr = s.Split(' ');
            double test = -1;
            if (double.TryParse(arr[0], out test))
            {
                s = arr[0];
                arr[0] = string.Empty;
                var content = string.Join(" ", arr).Trim();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    if (content.Length == 1)
                    {
                        return s + " " + content.ToLower();
                    }
                    return s + " " + char.ToLower(content[0]) + content.Substring(1);
                }
            }
            else
            {
                var nums = "0123456789";
                var fNum = string.Empty;
                int idx = 0;
                foreach (var c in arr[0])
                {
                    if (nums.Contains(c.ToString()))
                    {
                        fNum += c.ToString();
                    }
                    else
                    {
                        break;
                    }
                    idx++;
                }
                s = (fNum + " " + s.Substring(idx)).Trim();
                return FormatResult(s);
            }
            return s;
        }

        /// <summary>
        /// Truong hop nguoi dung nhap noi dung co dau xuong dong thi tach thanh cac Paragraph
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private List<Paragraph> GenListParagraphFromContent(string content, bool isAddHead = false)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            if (!string.IsNullOrWhiteSpace(content))
            {
                string[] arr = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                foreach (string s in arr)
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        Paragraph p = new Paragraph();
                        p.AddBasicSpacing();
                        p.AddIndentation("360");
                        if (isAddHead)
                        {
                            p.AddText(text: "- " + FormatResult(s.Trim().Replace("  ", " ")));
                        }
                        else
                        {
                            p.AddText(text: FormatHead(s.Trim().Replace("  ", " ")));
                        }
                        listParagraph.Add(p);
                    }
                }
            }
            return listParagraph;
        }

        private List<PlanTargetModel> GetListAction(int planID)
        {
            List<PlanTargetModel> listPlanTarget = this.context.PlanTarget.Where(x => x.PlanID == planID)
                .OrderBy(x => x.TargetOrder)
                .ThenBy(x => x.PlanTargetTitle)
                  .Select(x => new PlanTargetModel
                  {
                      PlanTargetID = x.PlanTargetID,
                      PlanTargetContent = x.PlanTargetContent
                  }).ToList();
            foreach (PlanTargetModel target in listPlanTarget)
            {
                List<PlanTargetActionModel> listAction = (from pt in this.context.PlanTargetAction
                                                          join p in this.context.PlanTarget on pt.PlanTargetID equals p.PlanTargetID
                                                          join s in this.context.SubActionPlan on pt.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                          from sj in sg.DefaultIfEmpty()
                                                          where pt.PlanID == planID && pt.PlanTargetID == target.PlanTargetID
                                                          orderby pt.ActionOrder
                                                          group new { pt, sj } by new { p.PlanID, p.PlanTargetID, pt.PlanTargetActionID, pt.ActionContent, pt.ActionCode, pt.ActionOrder, pt.ContentPropaganda, pt.ObjectScope, pt.ActionMethod, pt.Result } into g
                                                          orderby g.Key.ActionOrder, g.Key.PlanTargetActionID
                                                          select new PlanTargetActionModel
                                                          {
                                                              PlanID = g.Key.PlanID,
                                                              PlanTargetID = g.Key.PlanTargetID,
                                                              PlanTargetActionID = g.Key.PlanTargetActionID,
                                                              ActionContent = g.Key.ActionContent,
                                                              ActionCode = g.Key.ActionCode,
                                                              ContentPropaganda = g.Key.ContentPropaganda,
                                                              ObjectScope = g.Key.ObjectScope,
                                                              ActionMethod = g.Key.ActionMethod,
                                                              Result = g.Key.Result,
                                                              ListSubActionPlan = g.Where(x => x.sj != null).OrderBy(x => x.sj.SubActionOrder).Select(x =>
                                                              new SubActionPlanModel
                                                              {
                                                                  SubActionPlanID = x.sj.SubActionPlanID,
                                                                  SubActionCode = x.sj.SubActionCode,
                                                                  SubActionContent = x.sj.SubActionContent,
                                                                  ContentPropaganda = x.sj.ContentPropaganda,
                                                                  SubActionObject = x.sj.SubActionObject,
                                                                  SubActionMethod = x.sj.SubActionMethod,
                                                                  Result = x.sj.Result
                                                              }).ToList()
                                                          }).ToList();
                target.ListPlanTargetActionModel = listAction;
                // Index
                List<TargetIndexModel> listPlanTargetIndex = (from pi in this.context.PlanTargetIndex
                                                              join y in context.MainIndex on pi.MainIndexID equals y.MainIndexID into g1
                                                              from y in g1.DefaultIfEmpty()
                                                              where pi.PlanID == planID && pi.PlanTargetID == target.PlanTargetID
                                                              orderby y.OrderNumber
                                                              select new TargetIndexModel
                                                              {
                                                                  TypeIndex = pi.TypeIndex,
                                                                  ValueIndex = pi.ValueIndex,
                                                                  ContentIndex = pi.ContentIndex
                                                              }).ToList();
                target.ListPlanTargetIndex = listPlanTargetIndex;
            }
            return listPlanTarget;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}