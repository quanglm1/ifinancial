﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class PlanCostController : Controller
    {
        private Entities Context = new Entities();

        private ObjectRoleBO GetRole(Plan plan)
        {
            var user = SessionManager.GetUserInfo();
            ObjectRoleBO role = null;
            ChangeRequest cr = plan.Status == Constants.PLAN_APPROVED ?
                Context.ChangeRequest.Where(x => x.PlanID == plan.PlanID && x.IsFinish == null).OrderByDescending(x => x.ChangeRequestID).FirstOrDefault() : null;
            if (cr != null)
            {
                role = new FlowProcessBusiness(Context).GetRoleForPlanChange(user.UserID, plan.PlanID);
            }
            else
            {
                role = new FlowProcessBusiness(Context).GetRoleForObject(user.UserID, plan.PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            }
            return role;
        }

        // GET: SubActionPlanCost
        [BreadCrumb(ControllerName = "Dự toán kinh phí", ActionName = "Dự toán kinh phí", AreaName = "Kế hoạch")]
        public ActionResult Index(int planID)
        {
            //   Plan plan = Context.Plan.Find(PlanID);
            Plan plan = Context.Plan.Where(x => x.PlanID == planID).FirstOrDefault();
            if (plan != null)
            {
                Period period = Context.Period.Find(plan.PeriodID);
                UserInfo user = SessionManager.GetUserInfo();
                if (plan.DepartmentID == Constants.HEAD_DEPT_ID)
                {
                    if (user.DepartmentID != Constants.HEAD_DEPT_ID)
                    {
                        return RedirectToAction("Index", "Home", new { periodId = period.PeriodID });
                    }
                    else
                    {
                        return RedirectToAction("Index", "CostEstimate", new { periodId = period.PeriodID });
                    }
                }
                // Role
                ObjectRoleBO role = GetRole(plan);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    return RedirectToAction("RoleErrorBusiness", "Home");
                }
                SetData(plan, role);
            }
            else
            {
                return RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Kế hoạch chưa được lập, vui lòng tạo kế hoạch với thông tin chung trước" });
            }
            return View();
        }

        public PartialViewResult ReloadCost(int planID)
        {
            Plan plan = Context.Plan.Where(x => x.PlanID == planID).FirstOrDefault();
            if (plan != null)
            {
                // Role
                ObjectRoleBO role = GetRole(plan);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    throw new BusinessException("Bạn không có quyền thực hiện chức năng này");
                }
                SetData(plan, role);
            }
            else
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng tạo kế hoạch với thông tin chung trước");
            }
            return PartialView("_ListSubActionPlanCost");
        }

        private void SetData(Plan plan, ObjectRoleBO role)
        {
            if (plan != null)
            {
                Period period = Context.Period.Find(plan.PeriodID);
                UserInfo user = SessionManager.GetUserInfo();
                int startYear = period.FromDate.Year;
                int finishYear = period.ToDate.Year;
                int rangeYear = finishYear - startYear;

                ViewData["Period"] = period;
                ViewData["Plan"] = plan;
                ViewData["Dept"] = Context.Department.Find(plan.DepartmentID);
                ViewData["RangeYear"] = rangeYear;
                List<SelectListItem> listSpendItem = GetSpendItemSelectList();
                ViewData["ListSpendItem"] = listSpendItem;
                ViewData["role"] = role;
                if (role.Role >= Constants.ROLE_EDIT)
                {
                    PlanCostBusiness bus = new PlanCostBusiness(this.Context);
                    bus.InitPlanCost(plan.PlanID, SessionManager.GetUserInfo().UserID);
                    Context.SaveChanges();
                }
                List<PlanTargetModel> lstPlanTarget = GetPlanTargetByPlan(plan.PlanID);
                ViewData["PlanTarget"] = lstPlanTarget;
                List<ChangeRequestActionModel> listChangeAction;
                if (plan.IsChange.GetValueOrDefault() == 0)
                {
                    listChangeAction = new PlanBusiness(Context).GetPlanUnlockAction(plan.PlanID);
                }
                else
                {
                    listChangeAction = new PlanBusiness(Context).GetRequestAction(plan.PlanID);
                }
                if (listChangeAction != null)
                {
                    ViewData["listChangeAction"] = listChangeAction;
                }
            }
        }

        public PartialViewResult LoadSpendItem(int spendID)
        {
            SubActionSpendItemModel model = this.Context.SubActionSpendItem.Where(x => x.SubActionSpendItemID == spendID && x.Status == Constants.IS_ACTIVE)
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemID = x.SpendItemID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice,
                SumCostType = x.SumCostType,
                Description = x.Description
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại mục chi vừa chọn");
            }
            return PartialView("_EditSpendItem", model);
        }

        public PartialViewResult OpenSpendItemPopup(int id, string type)
        {
            this.Session["SessionSpendItem"] = null;
            List<SelectListItem> listSpendItem;
            if ("spendItem".Equals(type))
            {
                listSpendItem = GetSpendItemSelectList(id);
            }
            else
            {
                listSpendItem = GetSpendItemSelectList();
            }
            ViewData["ListSpendItem"] = listSpendItem;
            return PartialView("_AddSpendItem");
        }

        public PartialViewResult OpenSpendItem(int id, string type)
        {
            this.Session["SessionSpendItem"] = null;
            List<SelectListItem> listSpendItem;
            if ("spendItem".Equals(type))
            {
                listSpendItem = GetSpendItemSelectList(id);
            }
            else
            {
                listSpendItem = GetSpendItemSelectList();
            }
            ViewData["ListSpendItem"] = listSpendItem;
            return PartialView("_AddSpendItem");
        }

        public JsonResult DelSpendItem(int id)
        {
            SubActionSpendItem entity = this.Context.SubActionSpendItem.Find(id);
            if (entity != null)
            {
                int? planID = null;
                if (entity.PlanTargetActionID != null)
                {
                    PlanTargetAction pta = this.Context.PlanTargetAction.Find(entity.PlanTargetActionID);
                    planID = pta.PlanID;
                }
                else if (entity.SubActionPlanID != null)
                {
                    var subAction = (from s in this.Context.SubActionPlan
                                     join p in this.Context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                     where s.SubActionPlanID == entity.SubActionPlanID
                                     select new
                                     {
                                         p.PlanTargetID,
                                         p.PlanTargetActionID,
                                         s.SubActionPlanID
                                     }).FirstOrDefault();

                    // Get Plan
                    PlanTargetAction pta = this.Context.PlanTargetAction.Find(subAction.PlanTargetActionID);
                    planID = pta.PlanID;
                }
                var plan = Context.Plan.Find(planID);
                ObjectRoleBO role = GetRole(plan);
                if (role == null || role.Role == Constants.ROLE_NO_VIEW)
                {
                    throw new BusinessException("Bạn không có quyền thao tác trên kế hoạch này");
                }
                if (role.Role <= Constants.ROLE_VIEW)
                {
                    throw new BusinessException("Bạn chỉ có quyền xem kế hoạch này");
                }
                PlanCostBusiness bus = new PlanCostBusiness(this.Context);
                List<SubActionPlanCost> listCost = this.Context.SubActionPlanCost.Where(x => x.SubActionSpendItemID == id).ToList();
                bus.SumMoneyBySpendItem(entity, listCost, entity.TotalPrice, -1);
                bus.DelSpendItem(entity);
                this.Context.SaveChanges();
            }
            return Json(new { Type = "SUCCESS", Message = "Xóa hạng mục chi thành công" }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AddSpendItem(int id, string type, string[] spendCodes, int[] spendIDs, string[] spendContents)
        {
            int userID = SessionManager.GetUserInfo().UserID;
            int planTargetID;
            int planTargetActionID;
            int? subActionPlanID = null;
            int? parentID = null;
            int? planID = null;
            if ("targetaction".Equals(type))
            {
                PlanTargetAction pta = this.Context.PlanTargetAction.Find(id);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                planTargetID = pta.PlanTargetID.Value;
                planID = pta.PlanID;
            }
            else if ("subaction".Equals(type))
            {
                var subAction = (from s in this.Context.SubActionPlan
                                 join p in this.Context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 where s.SubActionPlanID == id
                                 select new
                                 {
                                     p.PlanTargetID,
                                     p.PlanTargetActionID,
                                     s.SubActionPlanID
                                 }).FirstOrDefault();
                if (subAction == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = subAction.PlanTargetActionID;
                subActionPlanID = subAction.SubActionPlanID;
                planTargetID = subAction.PlanTargetID.Value;
                // Get Plan
                PlanTargetAction pta = this.Context.PlanTargetAction.Find(planTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planID = pta.PlanID;
            }
            else
            {
                SubActionSpendItem subItem = this.Context.SubActionSpendItem.Find(id);
                if (subItem == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                PlanTargetAction pta = this.Context.PlanTargetAction.Find(subItem.PlanTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planID = pta.PlanID;
                planTargetActionID = pta.PlanTargetActionID;
                subActionPlanID = subItem.SubActionPlanID;
                planTargetID = pta.PlanTargetID.Value;
                parentID = id;
            }
            var plan = Context.Plan.Find(planID);
            ObjectRoleBO role = GetRole(plan);
            if (role == null || role.Role == Constants.ROLE_NO_VIEW)
            {
                throw new BusinessException("Bạn không có quyền thao tác trên kế hoạch này");
            }
            if (role.Role <= Constants.ROLE_VIEW)
            {
                throw new BusinessException("Bạn chỉ có quyền xem kế hoạch này");
            }
            // Luu muc chi cao nhat lam cha
            int startIdx = 0;
            List<SubActionSpendItem> listSubSpendItem = new List<SubActionSpendItem>();
            List<SubActionSpendItemModel> listModel = new List<SubActionSpendItemModel>();
            if (spendIDs[0] > 0)
            {
                startIdx = 1;
                SubActionSpendItem spendItem = new SubActionSpendItem();
                spendItem.CreateDate = DateTime.Now;
                spendItem.CreateUserID = userID;
                spendItem.ParentID = parentID;
                spendItem.PlanTargetActionID = planTargetActionID;
                spendItem.SpendItemID = spendIDs[0];
                spendItem.SpendItemName = spendContents[0];
                spendItem.SpendItemTitle = spendCodes[0];
                spendItem.Status = Constants.IS_ACTIVE;
                spendItem.SubActionPlanID = subActionPlanID;
                this.Context.SubActionSpendItem.Add(spendItem);
                this.Context.SaveChanges();
                parentID = spendItem.SubActionSpendItemID;
                listSubSpendItem.Add(spendItem);
                SubActionSpendItemModel model = new SubActionSpendItemModel();
                model.SubActionSpendItemID = spendItem.SubActionSpendItemID;
                model.SpendItemName = spendItem.SpendItemName;
                model.SpendItemTitle = spendItem.SpendItemTitle;
                model.SubActionPlanID = subActionPlanID;
                model.PlanTargetActionID = planTargetActionID;
                model.SpendItemID = spendItem.SpendItemID;
                model.ParentID = spendItem.ParentID;
                listModel.Add(model);
            }
            for (int i = startIdx; i < spendIDs.Length; i++)
            {
                SubActionSpendItem spendItem = new SubActionSpendItem();
                spendItem.CreateDate = DateTime.Now;
                spendItem.CreateUserID = userID;
                spendItem.ParentID = parentID;
                spendItem.PlanTargetActionID = planTargetActionID;
                if (spendIDs[i] > 0)
                {
                    spendItem.SpendItemID = spendIDs[i];
                }
                spendItem.SpendItemName = spendContents[i];
                spendItem.SpendItemTitle = spendCodes[i];
                spendItem.Status = Constants.IS_ACTIVE;
                spendItem.SubActionPlanID = subActionPlanID;
                this.Context.SubActionSpendItem.Add(spendItem);
                listSubSpendItem.Add(spendItem);
            }
            Context.SaveChanges();
            // Insert phan cost
            Period period = (from pt in this.Context.PlanTarget
                             join pl in this.Context.Plan on pt.PlanID equals pl.PlanID
                             join pr in this.Context.Period on pl.PeriodID equals pr.PeriodID
                             where pt.PlanTargetID == planTargetID
                             select pr).FirstOrDefault();
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<SubActionPlanCost> listCost = new List<SubActionPlanCost>();
            for (int year = fromYear; year <= toYear; year++)
            {
                foreach (SubActionSpendItem item in listSubSpendItem)
                {
                    SubActionPlanCost cost = new SubActionPlanCost();
                    cost.CreateDate = DateTime.Now;
                    cost.CreateUserID = userID;
                    cost.UpdateDate = DateTime.Now;
                    cost.ModifierID = userID;
                    cost.PlanTargetActionID = planTargetActionID;
                    cost.PlanTargetID = planTargetID;
                    cost.PlanYear = year;
                    cost.Status = Constants.IS_ACTIVE;
                    cost.SubActionPlanID = subActionPlanID;
                    cost.SubActionSpendItemID = item.SubActionSpendItemID;
                    this.Context.SubActionPlanCost.Add(cost);
                    listCost.Add(cost);
                }
            }
            Context.SaveChanges();

            int level = 0;
            if (parentID != null)
            {
                level = this.GetSubItemLevel(parentID);
            }

            if (listModel.Any())
            {
                SubActionSpendItemModel model = listModel[0];
                model.Level = level + 1;
                model.ListChild = new List<SubActionSpendItemModel>();
                model.ListSubActionPlanCost = listCost.Where(x => x.SubActionSpendItemID == model.SubActionSpendItemID)
                        .Select(x => new SubActionPlanCostModel
                        {
                            SubActionPlanCostID = x.SubActionPlanCostID,
                            SubActionSpendItemID = x.SubActionSpendItemID,
                            PlanYear = x.PlanYear,
                            Unit = x.Unit,
                            Price = x.Price,
                            Quantity = x.Quantity,
                            TotalPrice = x.TotalPrice,
                            CreateUserID = x.CreateUserID,
                            ModifierID = x.ModifierID
                        }).ToList();
                for (int i = 1; i < listSubSpendItem.Count; i++)
                {
                    SubActionSpendItemModel child = new SubActionSpendItemModel();
                    child.Level = model.Level + 1;
                    child.ParentID = model.SubActionSpendItemID;
                    child.SubActionSpendItemID = listSubSpendItem[i].SubActionSpendItemID;
                    child.SpendItemName = listSubSpendItem[i].SpendItemName;
                    child.SpendItemTitle = listSubSpendItem[i].SpendItemTitle;
                    child.SubActionPlanID = subActionPlanID;
                    child.PlanTargetActionID = planTargetActionID;
                    child.SpendItemID = listSubSpendItem[i].SpendItemID;
                    child.ListSubActionPlanCost = listCost.Where(x => x.SubActionSpendItemID == listSubSpendItem[i].SubActionSpendItemID)
                        .Select(x => new SubActionPlanCostModel
                        {
                            SubActionPlanCostID = x.SubActionPlanCostID,
                            SubActionSpendItemID = x.SubActionSpendItemID,
                            PlanYear = x.PlanYear,
                            Unit = x.Unit,
                            Price = x.Price,
                            Quantity = x.Quantity,
                            TotalPrice = x.TotalPrice,
                            CreateUserID = x.CreateUserID,
                            ModifierID = x.ModifierID
                        }).ToList();
                    model.ListChild.Add(child);
                }
            }
            else
            {
                for (int i = 0; i < listSubSpendItem.Count; i++)
                {
                    SubActionSpendItemModel model = new SubActionSpendItemModel();
                    model.ParentID = listSubSpendItem[i].ParentID;
                    model.Level = level + 1;
                    model.SubActionSpendItemID = listSubSpendItem[i].SubActionSpendItemID;
                    model.SpendItemName = listSubSpendItem[i].SpendItemName;
                    model.SpendItemTitle = listSubSpendItem[i].SpendItemTitle;
                    model.SubActionPlanID = subActionPlanID;
                    model.PlanTargetActionID = planTargetActionID;
                    model.SpendItemID = listSubSpendItem[i].SpendItemID;
                    model.ListSubActionPlanCost = listCost.Where(x => x.SubActionSpendItemID == listSubSpendItem[i].SubActionSpendItemID)
                        .Select(x => new SubActionPlanCostModel
                        {
                            SubActionPlanCostID = x.SubActionPlanCostID,
                            SubActionSpendItemID = x.SubActionSpendItemID,
                            PlanYear = x.PlanYear,
                            Unit = x.Unit,
                            Price = x.Price,
                            Quantity = x.Quantity,
                            TotalPrice = x.TotalPrice,
                            CreateUserID = x.CreateUserID,
                            ModifierID = x.ModifierID
                        }).ToList();
                    listModel.Add(model);
                }
            }
            ViewData["Period"] = period;
            ViewData["RangeYear"] = toYear - fromYear;
            ViewData["listSubSpendItem"] = listModel;
            PlanTarget planTarget = Context.PlanTarget.Find(planTargetID);
            List<ChangeRequestActionModel> listChangeAction = new PlanBusiness(Context).GetRequestAction(planTarget.PlanID);
            if (listChangeAction != null)
            {
                ViewData["listChangeAction"] = listChangeAction;
            }
            ViewData["role"] = role;
            return PartialView("_ListSubCostInsert");
        }

        public PartialViewResult ReloadTargetAction(int? targetActionID, int? subActionID)
        {
            if (subActionID.GetValueOrDefault() > 0)
            {
                SubActionPlan sa = Context.SubActionPlan.Find(subActionID);
                if (sa == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động");
                }
            }
            return PartialView("_ListPlanTargetActionOne");
        }

        private int GetSubItemLevel(int? subItemID)
        {
            int level;
            SubActionSpendItem item = Context.SubActionSpendItem.Find(subItemID);
            if (item == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (item.ParentID == null)
            {
                level = 1;
            }
            else
            {
                level = 1 + this.GetSubItemLevel(item.ParentID);
            }
            return level;
        }

        public PartialViewResult GetForEditSpendItem(int subItemID)
        {
            SubActionSpendItemModel model = Context.SubActionSpendItem.Where(x => x.SubActionSpendItemID == subItemID)
            .Select(x => new SubActionSpendItemModel
            {
                SubActionSpendItemID = x.SubActionSpendItemID,
                PlanTargetSpendItemID = x.PlanTargetSpendItemID,
                PlanTargetActionID = x.PlanTargetActionID,
                SubActionPlanID = x.SubActionPlanID,
                ParentID = x.ParentID,
                SpendItemID = x.SpendItemID,
                SpendItemName = x.SpendItemName,
                SpendItemTitle = x.SpendItemTitle,
                TotalPrice = x.TotalPrice,
                SumCostType = x.SumCostType
            }).FirstOrDefault();
            if (model == null)
            {
                throw new BusinessException("Không tồn tại mục chi vừa chọn");
            }
            List<SubActionSpendItemModel> listRoot = new PlanCostBusiness(Context).GetListSubItem(subItemID);
            List<SelectListItem> listParent = new List<SelectListItem>();
            listParent.Add(new SelectListItem
            {
                Text = "-- Mục chi cha --",
                Value = ""
            });
            foreach (var root in listRoot)
            {
                AddSpendTree(listParent, root);
            }
            ViewData["listParent"] = listParent;
            return PartialView("_ListSpendItemPar", model);
        }

        private void AddSpendTree(List<SelectListItem> listParent, SubActionSpendItemModel item)
        {
            string spendItemName = Utils.GenSpace(item.Level) + item.SpendItemName;
            listParent.Add(new SelectListItem
            {
                Text = spendItemName,
                Value = item.SubActionSpendItemID.ToString()
            });
            if (item.ListChild != null && item.ListChild.Any())
            {
                foreach (var child in item.ListChild)
                {
                    child.Level = item.Level + 1;
                    AddSpendTree(listParent, child);
                }
            }
        }

        public JsonResult SavePlanCost(List<string> Unit, List<string> Quantity, List<string> Price, List<string> TotalPrice, List<int> SubCostID, int PlanID)
        {
            int deptID = SessionManager.GetUserInfo().DepartmentID.Value;
            Plan plan = this.Context.Plan.Where(x => x.PlanID == PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng kiểm tra lại");
            }
            // Role
            ObjectRoleBO role = GetRole(plan);
            if (role == null || role.Role == Constants.ROLE_NO_VIEW)
            {
                throw new BusinessException("Bạn không có quyền thao tác trên kế hoạch này");
            }
            if (role.Role <= Constants.ROLE_VIEW)
            {
                throw new BusinessException("Bạn chỉ có quyền xem kế hoạch này");
            }
            PlanCostBusiness bus = new PlanCostBusiness(this.Context);
            bus.SavePlanCost(PlanID, Unit, Quantity, Price, TotalPrice, SubCostID, plan.PlanID);
            Context.SaveChanges();
            return Json(new { Type = "SUCCESS", Message = "Cập nhật thông tin dự toán thành công" });

        }

        public List<PlanTargetModel> GetPlanTargetByPlan(int planId)
        {
            PlanCostBusiness bus = new PlanCostBusiness(this.Context);
            List<PlanTargetModel> lstPlanTarget = bus.GetListCost(planId);
            return lstPlanTarget;
        }

        public JsonResult UpdateSpendItem(SubActionSpendItemModel model)
        {
            SubActionSpendItem entity = Context.SubActionSpendItem.Where(x => x.SubActionSpendItemID == model.SubActionSpendItemID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại mục chi");
            }
            PlanTargetAction pta = Context.PlanTargetAction.Find(entity.PlanTargetActionID);
            if (pta == null)
            {
                throw new BusinessException("Không tồn tại hoạt động");
            }
            Plan plan = Context.Plan.Where(x => x.Status != Constants.IS_NOT_ACTIVE && Context.PlanTarget.Any(t => t.PlanID == x.PlanID && t.PlanTargetID == pta.PlanTargetID)).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            ObjectRoleBO role = GetRole(plan);
            if (role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền tác động kế hoạch này");
            }
            entity.SpendItemTitle = model.SpendItemTitle;
            entity.SpendItemName = model.SpendItemName;
            entity.SumCostType = model.SumCostType;
            entity.ParentID = model.ParentID;
            entity.Description = model.Description;
            Context.SaveChanges();
            return Json("Cập nhật thông tin mục chi thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSumCostType(int actionID, string actionType, int? SumCostType)
        {
            if (string.IsNullOrWhiteSpace(actionType) || actionID == 0)
            {
                throw new BusinessException("Không tồn tại hoạt động");
            }
            if (actionType.Equals("targetaction"))
            {
                PlanTargetAction pta = Context.PlanTargetAction.Find(actionID);
                if (pta == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động");
                }
                Plan plan = Context.Plan.Where(x => x.Status != Constants.IS_NOT_ACTIVE && Context.PlanTarget.Any(t => t.PlanID == x.PlanID && t.PlanTargetID == pta.PlanTargetID)).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch");
                }
                ObjectRoleBO role = GetRole(plan);
                if (role.Role < Constants.ROLE_EDIT)
                {
                    throw new BusinessException("Bạn không có quyền tác động kế hoạch này");
                }
                pta.SumCostType = SumCostType;
            }
            else
            {
                SubActionPlan subAction = Context.SubActionPlan.Find(actionID);
                if (subAction == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động");
                }
                PlanTargetAction pta = Context.PlanTargetAction.Find(subAction.PlanTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động");
                }
                Plan plan = Context.Plan.Where(x => x.Status != Constants.IS_NOT_ACTIVE && Context.PlanTarget.Any(t => t.PlanID == x.PlanID && t.PlanTargetID == pta.PlanTargetID)).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch");
                }
                ObjectRoleBO role = GetRole(plan);
                if (role.Role < Constants.ROLE_EDIT)
                {
                    throw new BusinessException("Bạn không có quyền tác động kế hoạch này");
                }
                subAction.SumCostType = SumCostType;
            }
            Context.SaveChanges();
            return Json("Cập nhật cách tính chi phí thành công", JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetSpendItemSelectList(int? spendID = null)
        {
            var spentItem = Context.SpendItem.ToList();
            // Initialise list and add first "All" item
            List<SelectListItem> options = new List<SelectListItem>
            {
                new SelectListItem(){ Value = "0", Text = "---Tất cả ---" }
            };
            // Get the top level parents

            var parents = spentItem.Where(x => (spendID != null && x.ParentID == spendID) || (spendID == null && x.ParentID == null));
            foreach (var parent in parents)
            {
                // Add SelectListItem for the parent
                options.Add(new SelectListItem()
                {
                    Value = parent.SpendItemID.ToString(),
                    Text = parent.Price != null ? parent.SpenContent + "_" + parent.Price : parent.SpenContent
                });
                // Get the child items associated with the parent
                var children = spentItem.Where(x => x.ParentID == parent.SpendItemID);
                // Add SelectListItem for each child
                foreach (var child in children)
                {
                    options.Add(new SelectListItem()
                    {
                        Value = child.SpendItemID.ToString(),
                        Text = string.Format("--- {0}", child.Price != null ? child.SpenContent + "_" + child.Price : child.SpenContent)
                    });
                }
            }
            return options;
        }

    }
}
