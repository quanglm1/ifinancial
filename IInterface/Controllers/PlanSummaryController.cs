﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class PlanSummaryController : Controller
    {
        private Entities context = new Entities();
        // GET: PlanSummary
        [BreadCrumb(ControllerName = "Tổng hợp kế hoạch", ActionName = "Tổng hợp kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult Search(string PeriodName = null, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IQueryable<Plan> lstPlan = (from a in context.Plan
                                        join b in context.Department on a.DepartmentID equals b.DepartmentID
                                        where a.DepartmentID == objUser.DepartmentID
                                        select a);
            IQueryable<PeriodDeptModel> iqPeriod = (from x in context.Period
                                                    join y in lstPlan on x.PeriodID equals y.PeriodID into g1
                                                    from y in g1.DefaultIfEmpty()
                                                    where x.Status != Constants.PERIOD_DEACTIVED
                                                    select new PeriodDeptModel()
                                                    {
                                                        PeriodID = x.PeriodID,
                                                        PlanID = y.PlanID,
                                                        DepartmentID = objUser.DepartmentID,
                                                        DepartmentName = objUser.DepartmentName,
                                                        Description = x.Description,
                                                        PeriodName = x.PeriodName,
                                                        FromDate = x.FromDate,
                                                        ToDate = x.ToDate,
                                                        Status = y == null ? 0 : y.Status
                                                    }).Distinct();
            if (!string.IsNullOrWhiteSpace(PeriodName))
            {
                iqPeriod.Where(x => x.PeriodName.ToLower().Contains(PeriodName));
            }
            Paginate<PeriodDeptModel> paginate = new Paginate<PeriodDeptModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqPeriod.OrderByDescending(x => x.ToDate).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = iqPeriod.Count();
            ViewData["listSummaryPlan"] = paginate;
            return PartialView("_ListResult");
        }
        [BreadCrumb(ControllerName = "Tổng hợp kế hoạch", ActionName = "Chi tiết kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult Step(int PeriodID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            Period period = context.Period.Where(x => x.PeriodID == PeriodID).FirstOrDefault();
            if (period == null)
            {
                throw new BusinessException("Không tồn tại giai đoạn thực hiện");
            }
            var TargetPlaned = GetTargetPlaned(PeriodID);
            var model = context.Plan
                .Where(a => a.PeriodID == PeriodID && a.DepartmentID == objUser.DepartmentID)
                .Select(a => new PlanModel
                {
                    PeriodID = a.PeriodID,
                    DepartmentID = a.DepartmentID,
                    ParentPlanID = a.ParentPlanID,
                    PlanContent = a.PlanContent,
                    PlanTarget = a.PlanTarget,
                    Status = a.Status,
                    Type = Constants.MAIN_TYPE,
                    FromDate = period.FromDate,
                    ToDate = period.ToDate
                })
                .FirstOrDefault();


            if (model == null)
            {
                model = new PlanModel();
                model.PlanID = 0;
                model.PeriodID = PeriodID;
            }
            //Lay len thong tin cua quy
            var objDepartment = (from a in context.Department
                                 where a.DepartmentID == objUser.DepartmentID.Value
                                 select a).FirstOrDefault();
            model.Type = Constants.MAIN_TYPE;
            model.FromDate = period.FromDate;
            model.ToDate = period.ToDate;
            model.PlanContent = "";
            //Department
            model.DepartmentID = objDepartment.DepartmentID;
            model.DepartmentName = objDepartment.DepartmentName;
            model.Address = objDepartment.Address;
            model.PhoneNumber = objDepartment.PhoneNumber;
            model.Fax = objDepartment.Fax;
            model.AccountNumber = objDepartment.AccountNumber;
            model.BankName = objDepartment.BankName;
            model.RepresentPersonName = objDepartment.RepresentPersonName;
            model.RepresentPosition = objDepartment.RepresentPosition;
            model.RepresentAddress = objDepartment.RepresentAddress;
            model.RepresentPhone = objDepartment.RepresentPhone;
            model.RepresentEmail = objDepartment.RepresentEmail;
            model.ContactPersonName = objDepartment.ContactPersonName;
            model.ContactPosition = objDepartment.ContactPosition;
            model.ContactAddress = objDepartment.ContactAddress;
            model.ContactPhone = objDepartment.ContactPhone;
            model.ContactEmail = objDepartment.ContactEmail;
            ViewData["firstTargetID"] = TargetPlaned.First().TargetID;
            return View(model);
        }
        //Luu thong tin ke hoach de dieu chinh
        public JsonResult PlanInforSave(string Target, int PeriodID, int PlanID = 0)
        {
            SaveSummaryPlan(Target, PeriodID, PlanID);
            return Json("Tổng hợp kế hoạch thành công", JsonRequestBehavior.AllowGet);

        }
        public PartialViewResult Step1Target(int PeriodID, int TargetID = 0)
        {
            var TargetPlaned = GetTargetPlaned(PeriodID);
            ViewData["ListTarget"] = TargetPlaned;
            if (TargetID == 0 && TargetPlaned.Any())
            {
                TargetID = TargetPlaned.FirstOrDefault().TargetID.Value;
            }
            if (TargetID > 0)
            {
                PlanBusiness bus = new PlanBusiness(context);
                ViewData["ListIndex"] = bus.GetIndexPlaned(PeriodID, TargetID);
            }
            ViewData["TargetID"] = TargetID;
            return PartialView("_Step2Target");
        }
        public PartialViewResult Step1Indexs(int PeriodID, int TargetID)
        {
            PlanBusiness bus = new PlanBusiness(context);
            ViewData["ListIndex"] = bus.GetIndexPlaned(PeriodID, TargetID);
            return PartialView("_Step2Indexs");
        }
        public PartialViewResult Step2Action(int PeriodID, int TargetID)
        {
            PlanBusiness bus = new PlanBusiness(context);
            ViewData["ListActions"] = bus.GetActionPlaned(PeriodID, TargetID);
            ViewData["AllSubActions"] = bus.GetSubActionPlaned(PeriodID, TargetID);
            ViewData["AllSubIndexs"] = GetSubIndexPlaned(PeriodID, TargetID);
            return PartialView("_Step3Action");
        }
        //Lay len danh sach cac muc tieu da duoc su dung o don vi
        private List<PlanTargetModel> GetTargetPlaned(int PeriodID)
        {
            List<PlanTargetModel> lstTargetPlaned = (from a in context.Target
                                                     where a.Status == Constants.IS_ACTIVE && a.Type == Constants.MAIN_TYPE
                                                     select new PlanTargetModel
                                                     {
                                                         PlanTargetContent = a.TargetContent,
                                                         TargetID = a.TargetID
                                                     }).ToList();
            for (int i = 0; i < lstTargetPlaned.Count; i++)
            {
                lstTargetPlaned[i].TargetCode = "2." + (i + 1);
                lstTargetPlaned[i].PlanTargetTitle = "Mục tiêu " + (i + 1);
            }
            return lstTargetPlaned;
        }

        //Lay len danh sach hoat dong con

        private List<PlanActionIndexModel> GetSubIndexPlaned(int PeriodID, int TargetID = 0)
        {
            var IQSubIndexs = (from x in context.PlanActionIndex
                               join y in context.ActionIndex on x.ActionIndexID equals y.ActionIndexID
                               join o in context.PlanTargetAction on x.PlanTargetActionID equals o.PlanTargetActionID
                               join z in context.PlanTarget on o.PlanTargetID equals z.PlanTargetID
                               join w in context.Plan on z.PlanID equals w.PlanID
                               where w.PeriodID == PeriodID && z.TargetID == TargetID
                               select new PlanActionIndexModel
                               {
                                   PlanActionIndexID = x.PlanActionIndexID,
                                   ActionIndexID = x.ActionIndexID,
                                   PlanTargetActionID = x.PlanTargetActionID,
                                   IndexValue = x.IndexValue,
                                   IndexName = y.IndexName,
                                   IndexUnitName = y.UnitName,
                                   PlanTargetID = x.PlanTargetID
                               }).Distinct();
            return IQSubIndexs.OrderByDescending(x => x.PlanActionIndexID).ToList();
        }
        private void SaveSummaryPlan(string Target, int PeriodID, int PlanID = 0)
        {
            Period period = context.Period.Find(PeriodID);
            if(period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            try
            {
                UserInfo objUser = SessionManager.GetUserInfo();
                Plan entity = context.Plan.Where(a => a.PeriodID == PeriodID && a.DepartmentID == objUser.DepartmentID).FirstOrDefault();
                var objDepartment = context.Department.Find(objUser.DepartmentID);
                if (entity != null)
                {
                    DeleteAllPlan(entity.PlanID);
                }
                else
                {
                    //Xu ly voi ke hoach tong the
                    entity = new Plan();
                    entity.PeriodID = PeriodID;
                    entity.DepartmentID = objUser.DepartmentID.Value;
                    entity.PlanTarget = Target;
                    entity.Status = Constants.PLAN_APPROVED;
                    entity.Type = Constants.MAIN_TYPE;
                    entity.RepresentPersonName = objDepartment.RepresentPersonName;
                    entity.RepresentPosition = objDepartment.RepresentPosition;
                    entity.RepresentAddress = objDepartment.RepresentAddress;
                    entity.RepresentEmail = objDepartment.RepresentEmail;
                    entity.RepresentPhone = objDepartment.RepresentPhone;
                    entity.ContactPersonName = objDepartment.ContactPersonName;
                    entity.ContactPosition = objDepartment.ContactPosition;
                    entity.ContactAddress = objDepartment.ContactAddress;
                    entity.ContactEmail = objDepartment.ContactEmail;
                    entity.ContactPhone = objDepartment.ContactPhone;
                    entity.FromDate = period.FromDate;
                    entity.ToDate = period.ToDate;
                    context.Plan.Add(entity);
                    context.SaveChanges();
                }
                //Xu ly moi muc tieu cua ke hoach tong
                PlanBusiness bus = new PlanBusiness(context);
                bus.SynTarget(entity.PlanID);
                bus.SynActionAndIndex(entity);
                bus.InitTime(entity.PlanID);
                context.SaveChanges();

            }
            catch (Exception e)
            {
                throw new BusinessException("Tổng hợp kế hoạch không thành công, Có lỗi xảy ra: " + e.Message);
            }
        }
        private void DeleteAllPlan(int PlanID)
        {
            try
            {
                //Lay len ke hoach
                Plan Planed = context.Plan.Find(PlanID);
                if (Planed == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch đang chọn");
                }
                List<PlanTarget> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == PlanID).ToList();
                PlanBusiness bus = new PlanBusiness(context);
                bus.DelPlanTarget(listPlanTarget, PlanID);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new BusinessException("Xử lý Tổng hợp kế hoạch không thành công, Có lỗi xảy ra: " + e.Message);
            }
        }
    }
}