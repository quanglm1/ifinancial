﻿using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class PeriodController : Controller
    {
        private Entities context = new Entities();
        [BreadCrumb(ControllerName = "Giai đoạn", ActionName = "Giai đoạn", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult SearchPeriod(string PeriodName = null, int page = 1)
        {
            if (string.IsNullOrWhiteSpace(PeriodName))
            {
                PeriodName = null;
            }
            else
            {
                PeriodName = PeriodName.Trim().ToLower();
            }

            IQueryable<PeriodModel> iqPeriod =
                from x in this.context.Period
                where x.Status != Constants.PERIOD_DEACTIVED
                && (PeriodName == null || x.PeriodName.ToLower().Contains(PeriodName))
                select new PeriodModel()
                {
                    PeriodID = x.PeriodID,
                    Description = x.Description,
                    PeriodName = x.PeriodName,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Status = x.Status,
                    CreateDate = x.CreateDate
                };
            Paginate<PeriodModel> paginate = new Paginate<PeriodModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqPeriod.OrderBy(x => x.PeriodName).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = iqPeriod.Count();
            ViewData["listPeriod"] = paginate;
            return PartialView("_ListPeriod");
        }
        public JsonResult Delete(int[] periodIds)
        {
            List<Period> LstPeriod = context.Period.Where(x => periodIds.Contains(x.PeriodID) && x.Status == Constants.IS_ACTIVE).ToList();
            if (LstPeriod == null)
            {
                throw new BusinessException("Không tồn tại kỳ lập kế hoạch đã chọn");
            }
            int countPlan = (from pl in this.context.Plan
                             join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                             where pl.Status == Constants.IS_ACTIVE && periodIds.Contains(pr.PeriodID)
                             select pl.PlanID).Count();
            if (countPlan > 0)
            {
                throw new BusinessException("Đã có kế hoạch của kỳ, không thể xóa dữ liệu");
            }
            LstPeriod.ForEach(x =>
            {
                x.Status = Constants.PERIOD_DEACTIVED;
            });
            this.context.SaveChanges();
            return Json("Xóa kỳ lập kế hoạch thành công", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(PeriodModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                var isNew = false;
                Period entity = null;
                if (model.PeriodID > 0)
                {
                    entity = this.context.Period.Where(x => x.PeriodID == model.PeriodID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại kỳ lập kế hoạch đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật kỳ lập kế hoạch thành công";
                }
                else
                {
                    isNew = true;
                    entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    this.context.Period.Add(entity);
                    msg = "Thêm mới kỳ lập kế hoạch thành công";

                }
                this.context.SaveChanges();
                if (isNew)
                {
                    // Them moi luong xu ly
                    var lastPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).OrderByDescending(x => x.ToDate).FirstOrDefault();
                    if (lastPeriod != null)
                    {
                        List<FlowProcess> listFp = context.FlowProcess.Where(x => x.PeriodID == lastPeriod.PeriodID).ToList();
                        foreach(var fp in listFp)
                        {
                            FlowProcess newFP = new FlowProcess();
                            Utils.Copy(fp, newFP);
                            newFP.PeriodID = entity.PeriodID;
                            this.context.FlowProcess.Add(newFP);
                        }
                        this.context.SaveChanges();
                    }
                }
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            PeriodModel model = new PeriodModel();
            model.FromDate = DateTime.Now;
            model.FromDate = DateTime.Now.AddYears(1);
            if (id.HasValue && id.Value > 0)
            {
                model = this.context.Period.Where(x => x.PeriodID == id && x.Status != Constants.PERIOD_DEACTIVED).Select(x => new PeriodModel
                {
                    PeriodID = x.PeriodID,
                    PeriodName = x.PeriodName,
                    Description = x.Description,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Status = x.Status
                }).FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại kỳ lập kế hoạch đã chọn");
                }
            }
            return PartialView("_AddOrEdit", model);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}