﻿using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReportMissionPlanCostController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Báo cáo tổng hợp chi phí", ActionName = "Báo cáo tổng hợp chi phí", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            List<Period> listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            ViewData["listPeriod"] = listPeriod;
            return View();
        }
        public PartialViewResult GetListAction(int periodID)
        {
            // Lay danh sach nhiem vu
            List<MissionModel> listMission = context.Mission.Where(x => x.Status == Constants.IS_ACTIVE)
                .Select(x => new MissionModel
                {
                    MissionID = x.MissionID,
                    MissionName = x.MissionName
                }).ToList();

            var rootDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE && x.ParentID == null).First();
            // Tong hop theo hoat dong
            List<MissionModel> listMissionCost = (from m in context.Mission
                                                  join a in context.MainAction on m.MissionID equals a.MissionID
                                                  join pa in context.PlanTargetAction on a.MainActionID equals pa.MainActionID
                                                  join pt in context.PlanTarget on pa.PlanTargetID equals pt.PlanTargetID
                                                  join pl in context.Plan on pt.PlanID equals pl.PlanID
                                                  where pl.DepartmentID != rootDept.DepartmentID && pl.PeriodID == periodID
                                                  && a.Status == Constants.IS_ACTIVE && pl.Status == Constants.PLAN_APPROVED
                                                  group pa by new { m.MissionID, m.MissionCode, m.MissionName, m.MissionContent } into g
                                                  orderby g.Key.MissionID
                                                  select new MissionModel
                                                  {
                                                      MissionID = g.Key.MissionID,
                                                      MissionCode = g.Key.MissionCode,
                                                      MissionContent = g.Key.MissionContent,
                                                      MissionName = g.Key.MissionName,
                                                      PlanMoney = g.Select(x => x.TotalBudget == null ? 0 : x.TotalBudget.Value).DefaultIfEmpty(0).Sum()
                                                  }).ToList();
            int idx = 0;
            bool isHasData = false;
            Dictionary<int, string> dicColor = Utils.GetColorCode();
            for (int i = 1; i <= listMission.Count; i++)
            {
                MissionModel mission = listMission[i - 1];
                mission.MissionCode = "Nhiệm vụ " + i;
                if (!dicColor.ContainsKey(idx))
                {
                    idx = 0;
                }
                mission.Color = dicColor[idx];
                idx++;
                List<MissionModel> listCost = listMissionCost.Where(x => x.MissionID == mission.MissionID).ToList();
                if (listCost != null && listCost.Any())
                {
                    mission.PlanMoney = listCost.Sum(x => x.PlanMoney);
                    if (mission.PlanMoney > 0)
                    {
                        isHasData = true;
                    }
                }
            }
            if (!isHasData)
            {
                throw new BusinessException("Chưa có thông tin chi phí");
            }
            ViewData["listMission"] = listMission;
            return PartialView("_LstResult");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}