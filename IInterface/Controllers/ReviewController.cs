﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReviewController : Controller
    {
        private Entities Context = new Entities();
        // GET: Review
        [BreadCrumb(ControllerName = "Xem xét kế hoạch", ActionName = "Xem xét kế hoạch", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            ViewData["listDepartment"] = GetlistDepartment();
            var listPeriod = Context.Period.Where(x => x.Status == Constants.IS_ACTIVE).OrderByDescending(x => x.ToDate)
                .Select(x => new SelectListItem
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID.ToString()
                }).ToList();
            ViewData["listPeriod"] = listPeriod;
            return View();
        }
        public PartialViewResult Search(int SearchDepartmentID, int PeriodId = 0, int? Status = null, int page = 1)
        {
            ViewData["listResult"] = SearchPlan(SearchDepartmentID, PeriodId, Status, page);
            return PartialView("_LstResult");
        }
        public PartialViewResult ViewReviewHistory(int PlanID, int page = 1)
        {
            ViewData["listResult"] = SearchHistory(PlanID, null, page);
            return PartialView("_ReviewHistory");
        }
        public PartialViewResult ViewHisPlanApprove(int PlanID, int page = 1)
        {
            ViewData["listResult"] = SearchHistory(PlanID, Constants.PLAN_APPROVED, page);
            return PartialView("_PlanApproveHistory");
        }

        public JsonResult ApprovePlan(int PlanID)
        {
            Plan ObjectPlan = Context.Plan.Find(PlanID);
            ObjectPlan.Status = Constants.PLAN_APPROVED;
            Context.SaveChanges();
            return Json("Phê duyệt kế hoạch thành công", JsonRequestBehavior.AllowGet);
        }
        private List<SelectListItem> GetlistDepartment()
        {
            UserInfo user = SessionManager.GetUserInfo();
            return new PlanBusiness(Context).GetlistDepartment(user.UserID);
        }
        private Paginate<PlanReviewHistoryModel> SearchHistory(int PlanID, int? Status, int page)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IEnumerable<PlanReviewHistoryModel> IQSearch = (from a in Context.PlanReviewHistory
                                                            join b in Context.User on a.UserID equals b.UserID
                                                            join c in Context.PlanHis on a.PlanHisID equals c.ID
                                                            where a.PlanID == PlanID
                                                            && (Status == null || Status == 0 || c.Status == Status)
                                                            select new PlanReviewHistoryModel
                                                            {
                                                                UserID = a.UserID,
                                                                Comment = a.Comment,
                                                                CreateDate = a.CreateDate,
                                                                PlanID = a.PlanID,
                                                                PlanReviewHistoryID = a.PlanReviewHistoryID,
                                                                UpdateDate = a.UpdateDate,
                                                                UserName = b.UserName,
                                                                PlanHisID = a.PlanHisID,
                                                                IsFirstApprove = c.FirstApprove
                                                            }).AsEnumerable();
            Paginate<PlanReviewHistoryModel> paginate = new Paginate<PlanReviewHistoryModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = IQSearch.OrderBy(x => x.CreateDate).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            if (Status.GetValueOrDefault() == Constants.PLAN_APPROVED)
            {
                for (int i = 1; i <= paginate.List.Count; i++)
                {
                    var item = paginate.List[i - 1];
                    item.Comment = "Phê duyệt kế hoạch lần " + i;
                    if (paginate.List.Count > 1 && item.IsFirstApprove.GetValueOrDefault() == Constants.IS_ACTIVE)
                    {
                        item.Comment += " (Kế hoạch phê duyệt trước thay đổi)";
                    }
                }
            }
            paginate.Count = IQSearch.Count();
            return paginate;
        }
        private Paginate<PlanModel> SearchPlan(int SearchDepartmentID, int PeriodId, int? Status, int page)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            List<int> listDeptManID = this.Context.UserManageDepartment.Where(x => x.UserID == objUser.UserID).Select(x => x.DepartmentID).ToList();
            IEnumerable<PlanModel> IQSearch = (from a in Context.Plan
                                               join b in Context.Department on a.DepartmentID equals b.DepartmentID
                                               join c in Context.Period on a.PeriodID equals c.PeriodID
                                               where c.Status == Constants.IS_ACTIVE && a.Status != Constants.PERIOD_DEACTIVED
                                               && b.Status == Constants.IS_ACTIVE
                                               && (objUser.DepartmentID == Constants.HEAD_DEPT_ID || listDeptManID.Contains(a.DepartmentID))
                                               select new PlanModel
                                               {
                                                   PlanID = a.PlanID,
                                                   DepartmentID = a.DepartmentID,
                                                   DepartmentName = b.DepartmentName,
                                                   PeriodID = a.PeriodID,
                                                   FromDate = c.FromDate,
                                                   ToDate = c.ToDate,
                                                   Status = a.Status,
                                                   PeriodName = c.PeriodName,
                                                   StepNow = a.StepNow == null ? 0 : a.StepNow,
                                                   LastOpsDate = Context.PlanReviewHistory.Where(x => x.PlanID == a.PlanID).OrderByDescending(x => x.PlanReviewHistoryID).Select(x => x.CreateDate).FirstOrDefault(),
                                                   LastFlowProcess = Context.FlowProcess.Where(x => x.PeriodID == a.PeriodID
                                                                        && x.ObjectType == Constants.COMMENT_TYPE_PLAN_TARGET && x.Step == a.StepNow).FirstOrDefault()
                                               });
            if (PeriodId > 0)
            {
                IQSearch = IQSearch.Where(x => x.PeriodID == PeriodId);
            }
            if (SearchDepartmentID != 0)
            {
                IQSearch = IQSearch.Where(x => x.DepartmentID == SearchDepartmentID);
            }
            if (Status.GetValueOrDefault() > 0)
            {
                IQSearch = IQSearch.Where(x => x.Status == Status);
            }
            List<PlanModel> listPlan = IQSearch.ToList();
            List<int> listPlanID = listPlan.Where(x => x.PlanID != null).Select(x => x.PlanID.Value).ToList();
            Dictionary<int, ObjectRoleBO> dicRole = new FlowProcessBusiness(this.Context).GetRoleForObject(objUser.UserID, listPlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            listPlan.RemoveAll(x => !dicRole.ContainsKey(x.PlanID.Value) || dicRole[x.PlanID.Value].Role == Constants.ROLE_NO_VIEW);
            foreach (PlanModel plan in listPlan)
            {
                plan.Role = dicRole[plan.PlanID.Value].Role;
                plan.PL = dicRole[plan.PlanID.Value].PL;
            }
            Paginate<PlanModel> paginate = new Paginate<PlanModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = listPlan.OrderByDescending(x => x.StepNow).ThenByDescending(x => x.LastOpsDate).ThenByDescending(x => x.PlanID).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            foreach (var item in paginate.List)
            {
                var process = item.LastFlowProcess;
                if (item.StepNow > 0 && process != null)
                {
                    if (process.ProcessorType == Constants.FLOW_DEPT)
                    {
                        var dept = Context.Department.Where(x => x.DepartmentID == process.ProcessorID).FirstOrDefault();
                        var users = Context.User.Where(x => x.DepartmentID == process.ProcessorID);
                        if (process.IsRepresent.GetValueOrDefault() == Constants.IS_ACTIVE)
                        {
                            users = users.Where(x => Context.UserManageDepartment.Where(d => d.UserID == x.UserID && d.DepartmentID == item.DepartmentID).Any());
                        }
                        if (process.IsHeadOffice.GetValueOrDefault() == Constants.IS_ACTIVE)
                        {
                            users = users.Where(x => x.IsHeadOffice == Constants.IS_ACTIVE);
                        }
                        var user = users.FirstOrDefault();
                        string reviewInfo = string.Empty;
                        if (user != null)
                        {
                            reviewInfo += user.FullName + " - ";
                        }
                        if (dept != null)
                        {
                            reviewInfo += dept.DepartmentName + " ";
                        }
                        item.LastReviewInfo = reviewInfo;
                    }
                    else if (process.ProcessorType == Constants.FLOW_ROLE)
                    {
                        var role = Context.Role.Where(x => x.RoleID == process.ProcessorID).FirstOrDefault();
                        var users = Context.User.Where(x => x.Status != Constants.IS_DELETED_USER && Context.UserRole.Where(r => r.UserID == x.UserID && r.RoleID == process.ProcessorID).Any());
                        if (process.IsRepresent.GetValueOrDefault() == Constants.IS_ACTIVE)
                        {
                            users = users.Where(x => Context.UserManageDepartment.Where(d => d.UserID == x.UserID && d.DepartmentID == item.DepartmentID).Any());
                        }
                        if (process.IsHeadOffice.GetValueOrDefault() == Constants.IS_ACTIVE)
                        {
                            users = users.Where(x => x.IsHeadOffice == Constants.IS_ACTIVE);
                        }
                        var user = users.FirstOrDefault();
                        string reviewInfo = string.Empty;
                        if (user != null)
                        {
                            reviewInfo += user.FullName + " - ";
                        }
                        if (role != null)
                        {
                            reviewInfo += role.RoleName;
                        }
                        item.LastReviewInfo = reviewInfo;
                    }
                    if (item.Status == Constants.PLAN_APPROVED)
                    {
                        item.LastReviewInfo = string.IsNullOrEmpty(item.LastReviewInfo) ? "Đã phê duyệt" : item.LastReviewInfo + "<br />đã phê duyệt";
                    }
                    else if (item.Status == Constants.PERIOD_NEXT_LEVER)
                    {
                        item.LastReviewInfo = string.IsNullOrEmpty(item.LastReviewInfo) ? "Đang xem xét" : item.LastReviewInfo + "<br />đang xem xét";
                    }
                }
                if (item.StepNow == 0)
                {
                    if (item.Status == Constants.PERIOD_NEXT_LEVER)
                    {
                        item.LastReviewInfo = "Đơn vị đang chỉnh sửa";
                    }
                    else if (item.Status == Constants.PERIOD_APPROVED)
                    {
                        item.LastReviewInfo = "Đã phê duyệt";
                    }
                    else
                    {
                        item.LastReviewInfo = "Dự thảo";
                    }
                }
            }
            paginate.Count = listPlan.Count();
            return paginate;
        }
        public JsonResult Delete(int id)
        {
            PlanReviewHistory obj = Context.PlanReviewHistory.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn lịch sử đã chọn");
            }
            Context.PlanReviewHistory.Remove(obj);
            this.Context.SaveChanges();
            return Json("Xóa lịch sử thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}