﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;

namespace IInterface.Controllers
{
    public class PaySheetController : Controller
    {
        private List<SelectListItem> GetType(int type = 0)
        {
            List<SelectListItem> listType = new List<SelectListItem>();
            listType.Add(new SelectListItem { Value = Constants.TYPE_BDH.ToString(), Text = "Ban điều hành" });
            listType.Add(new SelectListItem { Value = Constants.TYPE_BKS.ToString(), Text = "Ban kiểm soát" });
            foreach (var item in listType)
            {
                if (item.Value.Equals(type.ToString()))
                {
                    item.Selected = true;
                }
            }
            return listType;
        }
        // GET: PaySheet

        private readonly Entities _context = new Entities();
        [BreadCrumb(ActionName = "Bảng lương", ControllerName = "Bảng lương", AreaName = "Tài chính")]
        public ActionResult Index()
        {
            int year = DateTime.Now.Year;
            int type = Constants.TYPE_BDH;
            List<PaySheetModel> paySheet =
                _context.PaySheet.Where(o => o.Year == year && o.Type == type).Select(o => new PaySheetModel
                {
                    PaySheetId = o.PaySheetId,
                    ActuallyReceived = o.ActuallyReceived,
                    BasicSalary = o.BasicSalary,
                    Coefficient = o.Coefficient,
                    IncreasedSalary = o.IncreasedSalary,
                    InsurranceDepartmentPay = o.InsurranceDepartmentPay,
                    InsurranceEmplyeePay = o.InsurranceEmplyeePay,
                    NumOfEmployee = o.NumOfEmployee,
                    TotalPerMonth = o.TotalPerMonth,
                    TotalPerYear = o.TotalPerYear,
                    TotalSalary = o.TotalSalary,
                }).ToList();
            ViewData["PaySheetResult"] = paySheet;

            PaySheetModel totalPs = new PaySheetModel();
            if (_context.PaySheet.Any(o => o.Year == year && o.Type == type))
            {
                var qPay = _context.PaySheet.Where(o => o.Year == year);
                totalPs.BasicSalary = qPay.Sum(o => o.BasicSalary);
                totalPs.ActuallyReceived = qPay.Sum(o => o.ActuallyReceived);
                totalPs.IncreasedSalary = qPay.Sum(o => o.IncreasedSalary);
                totalPs.InsurranceDepartmentPay = qPay.Sum(o => o.InsurranceDepartmentPay);
                totalPs.InsurranceEmplyeePay = qPay.Sum(o => o.InsurranceEmplyeePay);
                totalPs.TotalPerMonth = qPay.Sum(o => o.TotalPerMonth);
                totalPs.TotalPerYear = qPay.Sum(o => o.TotalPerYear);
                totalPs.TotalSalary = qPay.Sum(o => o.TotalSalary);
            }
            ViewData["PaySheetTotal"] = totalPs;

            List<AllowanceModel> allowance =
                _context.Allowance.Where(o => o.Year == year && o.Type == type).Select(o => new AllowanceModel
                {
                    BasicSalary = o.BasicSalary,
                    Coefficient = o.Coefficient,
                    NumOfEmployee = o.NumOfEmployee,
                    TotalPerYear = o.TotalPerYear,
                    AllowanceId = o.AllowanceId,
                    AllowancePerMonth = o.AllowancePerMonth,
                }).ToList();
            ViewData["AllowanceResult"] = allowance;

            AllowanceModel totalAl = new AllowanceModel();
            if (_context.Allowance.Any(o => o.Year == year && o.Type == type))
            {
                var qAllowance = _context.Allowance.Where(o => o.Year == year);
                totalAl.BasicSalary = qAllowance.Sum(o => o.BasicSalary);
                totalAl.AllowancePerMonth = qAllowance.Sum(o => o.AllowancePerMonth);
                totalAl.TotalPerYear = qAllowance.Sum(o => o.TotalPerYear);
            }

            ViewData["AllowanceTotal"] = totalAl;
            List<SelectListItem> listYear = this.GetListYear(year);
            ViewData["ListYear"] = listYear;
            ViewData["YearView"] = year;
            ViewData["ListType"] = this.GetType(type);
            return View();
        }

        public PartialViewResult SearchPaySheet(int year, int type)
        {
            List<PaySheetModel> paySheet =
                _context.PaySheet.Where(o => o.Year == year && o.Type == type).Select(o => new PaySheetModel
                {
                    PaySheetId = o.PaySheetId,
                    ActuallyReceived = o.ActuallyReceived,
                    BasicSalary = o.BasicSalary,
                    Coefficient = o.Coefficient,
                    IncreasedSalary = o.IncreasedSalary,
                    InsurranceDepartmentPay = o.InsurranceDepartmentPay,
                    InsurranceEmplyeePay = o.InsurranceEmplyeePay,
                    NumOfEmployee = o.NumOfEmployee,
                    TotalPerMonth = o.TotalPerMonth,
                    TotalPerYear = o.TotalPerYear,
                    TotalSalary = o.TotalSalary,
                }).ToList();
            ViewData["PaySheetResult"] = paySheet;

            PaySheetModel total = new PaySheetModel();
            if (_context.PaySheet.Any(o => o.Year == year && o.Type == type))
            {
                var pay = _context.PaySheet.Where(o => o.Year == year && o.Type == type);
                total.BasicSalary = pay.Sum(o => o.BasicSalary);
                total.ActuallyReceived = pay.Sum(o => o.ActuallyReceived);
                total.IncreasedSalary = pay.Sum(o => o.IncreasedSalary);
                total.InsurranceDepartmentPay = pay.Sum(o => o.InsurranceDepartmentPay);
                total.InsurranceEmplyeePay = pay.Sum(o => o.InsurranceEmplyeePay);
                total.TotalPerMonth = pay.Sum(o => o.TotalPerMonth);
                total.TotalPerYear = pay.Sum(o => o.TotalPerYear);
                total.TotalSalary = pay.Sum(o => o.TotalSalary);
            }

            ViewData["PaySheetTotal"] = total;
            ViewData["YearView"] = year;
            return PartialView("_PaySheet");
        }

        public PartialViewResult SearchAllowance(int year, int type)
        {
            List<AllowanceModel> allowance =
                _context.Allowance.Where(o => o.Year == year && o.Type == type).Select(o => new AllowanceModel
                {
                    BasicSalary = o.BasicSalary,
                    Coefficient = o.Coefficient,
                    NumOfEmployee = o.NumOfEmployee,
                    TotalPerYear = o.TotalPerYear,
                    AllowanceId = o.AllowanceId,
                    AllowancePerMonth = o.AllowancePerMonth,
                }).ToList();
            ViewData["AllowanceResult"] = allowance;

            AllowanceModel totalAl = new AllowanceModel();
            if (_context.Allowance.Any(o => o.Year == year && o.Type == type))
            {
                var all = _context.Allowance.Where(o => o.Year == year && o.Type == type);
                totalAl.BasicSalary = all.Sum(o => o.BasicSalary);
                totalAl.AllowancePerMonth = all.Sum(o => o.AllowancePerMonth);
                totalAl.TotalPerYear = all.Sum(o => o.TotalPerYear);
            }

            ViewData["AllowanceTotal"] = totalAl;
            return PartialView("_Allowance");
        }

        public PartialViewResult PrepareAddOrEditPaySheet(int? id, int year, int type)
        {
            PaySheetModel model = new PaySheetModel();

            if (id.HasValue && id.Value > 0)
            {
                model = _context.PaySheet
                    .Where(x => x.PaySheetId == id)
                    .Select(x => new PaySheetModel
                    {
                        PaySheetId = x.PaySheetId,
                        NumOfEmployee = x.NumOfEmployee,
                        Coefficient = x.Coefficient,
                        Year = x.Year,
                        Type = x.Type
                    })
                    .FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại bảng lương đã chọn");
                }
            }
            else
            {
                model.Year = year;
            }
            List<SelectListItem> listYear = this.GetListYear(year);
            ViewData["ListYear"] = listYear;
            ViewData["ListType"] = this.GetType();
            return PartialView("_AddOrEditPaySheet", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SavePaySheet(PaySheetModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            string msg;
            if (ModelState.IsValid)
            {
                string basic = ConfigurationManager.AppSettings["PAYSHEET_BASIC_SALARY"];
                string deptPay = ConfigurationManager.AppSettings["PAYSHEET_DEPARTMENT_PAY"];
                string empPay = ConfigurationManager.AppSettings["PAYSHEET_EMPLOYEE_PAY"];


                int basicSalary = 0;
                double deptPayAmount = 0;
                double empPayAmount = 0;
                if (basic != null)
                {
                    basicSalary = Convert.ToInt32(basic);
                }

                if (deptPay != null)
                {
                    deptPayAmount = Convert.ToDouble(deptPay);
                }

                if (empPay != null)
                {
                    empPayAmount = Convert.ToDouble(empPay);
                }

                if (model.PaySheetId > 0)
                {
                    PaySheet entity = _context.PaySheet.FirstOrDefault(x => x.PaySheetId == model.PaySheetId);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại bảng lương đã chọn");
                    }
                    entity.Year = model.Year;
                    entity.Type = model.Type;
                    entity.NumOfEmployee = model.NumOfEmployee;
                    entity.Coefficient = model.Coefficient;
                    entity.BasicSalary = model.NumOfEmployee * model.Coefficient * basicSalary;
                    entity.IncreasedSalary = entity.BasicSalary;
                    entity.TotalPerMonth = 2 * entity.BasicSalary;
                    entity.TotalPerYear = 12 * entity.TotalPerMonth;

                    entity.InsurranceDepartmentPay = entity.BasicSalary * deptPayAmount * 12;
                    entity.InsurranceEmplyeePay = entity.BasicSalary * empPayAmount * 12;

                    entity.ActuallyReceived = entity.TotalPerYear - entity.InsurranceEmplyeePay;
                    entity.TotalSalary = entity.TotalPerYear;

                    msg = "Cập nhật thành công";
                }
                else
                {
                    PaySheet entity = new PaySheet();
                    entity.NumOfEmployee = model.NumOfEmployee;
                    entity.Coefficient = model.Coefficient;
                    entity.BasicSalary = model.NumOfEmployee * model.Coefficient * basicSalary;
                    entity.IncreasedSalary = entity.BasicSalary;
                    entity.TotalPerMonth = 2 * entity.BasicSalary;
                    entity.TotalPerYear = 12 * entity.TotalPerMonth;

                    entity.InsurranceDepartmentPay = entity.BasicSalary * deptPayAmount * 12;
                    entity.InsurranceEmplyeePay = entity.BasicSalary * empPayAmount * 12;

                    entity.ActuallyReceived = entity.TotalPerYear - entity.InsurranceEmplyeePay;
                    entity.TotalSalary = entity.TotalPerYear;
                    entity.Year = model.Year;
                    entity.Type = model.Type;
                    _context.PaySheet.Add(entity);
                    msg = "Thêm mới thành công";

                }
                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public PartialViewResult PrepareAddOrEditAllowance(int? id, int year, int type)
        {
            AllowanceModel model = new AllowanceModel();

            if (id.HasValue && id.Value > 0)
            {
                model = _context.Allowance
                    .Where(x => x.AllowanceId == id)
                    .Select(x => new AllowanceModel
                    {
                        AllowanceId = x.AllowanceId,
                        NumOfEmployee = x.NumOfEmployee,
                        Coefficient = x.Coefficient,
                        Year = x.Year,
                        Type = x.Type
                    })
                    .FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại bảng lương đã chọn");
                }
            }
            else
            {
                model.Year = year;
                model.Type = type;
            }
            List<SelectListItem> listYear = this.GetListYear(year);
            ViewData["ListYear"] = listYear;
            ViewData["ListType"] = this.GetType();
            return PartialView("_AddOrEditAllowance", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveAllowance(AllowanceModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                string basic = ConfigurationManager.AppSettings["PAYSHEET_BASIC_SALARY"];
                string perMonth = ConfigurationManager.AppSettings["ALLOWANCE_PER_MONTH"];

                int basicSalary = 0;
                double perMonthAmount = 0;
                if (basic != null)
                {
                    basicSalary = Convert.ToInt32(basic);
                }

                if (perMonth != null)
                {
                    perMonthAmount = Convert.ToDouble(perMonth);
                }

                if (model.AllowanceId > 0)
                {
                    Allowance entity = _context.Allowance.FirstOrDefault(x => x.AllowanceId == model.AllowanceId);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại phụ cấp đã chọn");
                    }
                    entity.Year = model.Year;
                    entity.Type = model.Type;
                    entity.NumOfEmployee = model.NumOfEmployee;
                    entity.Coefficient = model.Coefficient;
                    entity.BasicSalary = model.NumOfEmployee * model.Coefficient * basicSalary;
                    entity.AllowancePerMonth = entity.BasicSalary * perMonthAmount;
                    entity.TotalPerYear = 12 * entity.AllowancePerMonth;

                    msg = "Cập nhật thành công";
                }
                else
                {
                    Allowance entity = new Allowance();
                    entity.NumOfEmployee = model.NumOfEmployee;
                    entity.Coefficient = model.Coefficient;
                    entity.BasicSalary = model.NumOfEmployee * model.Coefficient * basicSalary;
                    entity.AllowancePerMonth = entity.BasicSalary * perMonthAmount;
                    entity.TotalPerYear = 12 * entity.AllowancePerMonth;
                    entity.Year = model.Year;
                    entity.Type = model.Type;
                    _context.Allowance.Add(entity);
                    msg = "Thêm mới thành công";
                }
                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public JsonResult DeleteAllowance(int id)
        {
            Allowance obj = _context.Allowance.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại phụ cấp đã chọn");
            }

            _context.Allowance.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa nhiệm vụ phụ cấp", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePaySheet(int id)
        {
            PaySheet obj = _context.PaySheet.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại bảng lương đã chọn");
            }

            _context.PaySheet.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa nhiệm vụ bảng lương", JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetListYear(int selectYear)
        {
            List<SelectListItem> res = new List<SelectListItem>();
            int nowYear = DateTime.Now.Year;
            int fromYear = nowYear - 5;
            int toYear = nowYear + 5;
            List<Period> listPeriod = _context.Period.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            if (listPeriod.Any())
            {
                fromYear = listPeriod.Select(x => x.FromDate.Year).Min();
                toYear = listPeriod.Select(x => x.ToDate.Year).Max();
            }

            for (int year = fromYear; year <= toYear; year++)
            {
                res.Add(new SelectListItem
                {
                    Text = "Năm " + year,
                    Value = year.ToString(),
                    Selected = (selectYear == year)
                });
            }
            res = res.OrderByDescending(o => o.Value).ToList();
            return res;
        }
    }
}