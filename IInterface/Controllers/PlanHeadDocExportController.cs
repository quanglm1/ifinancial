﻿using DocumentFormat.OpenXml.Wordprocessing;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System.Linq;
using System.Collections.Generic;
using IInterface.Common.OpenXML;
using System.Web.Mvc;
using System;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using IInterface.Business;

namespace IInterface.Controllers
{
    public class PlanHeadDocExportController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        public FileResult ExportPLanPL01(int planID)
        {
            PlanModel plan = (from p in this.context.Plan
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              where p.PlanID == planID && p.Status != Constants.PLAN_DEACTIVED
                              select new PlanModel
                              {
                                  PeriodID = p.PeriodID,
                                  PlanID = p.PlanID,
                                  DepartmentID = p.DepartmentID,
                                  DepartmentName = d.DepartmentName,
                                  Address = d.Address,
                                  PhoneNumber = d.PhoneNumber,
                                  Fax = d.Fax,
                                  AccountNumber = d.AccountNumber,
                                  BankName = d.BankName,
                                  RepresentPersonName = p.RepresentPersonName,
                                  RepresentPosition = p.RepresentPosition,
                                  RepresentAddress = p.RepresentAddress,
                                  RepresentPhone = p.RepresentPhone,
                                  RepresentEmail = p.RepresentEmail,
                                  ContactPersonName = p.ContactPersonName,
                                  ContactPosition = p.ContactPosition,
                                  ContactAddress = p.ContactAddress,
                                  ContactPhone = p.ContactPhone,
                                  ContactEmail = p.ContactEmail,
                                  PlanContent = p.PlanContent,
                                  FromDate = pr.FromDate,
                                  ToDate = pr.ToDate,
                                  PlanTarget = p.PlanTarget,
                                  Implementation = p.Implementation
                              }).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch được chọn");
            }
            List<PlanTargetModel> listPlanTarget = this.GetListAction(planID);
            List<Paragraph> listParagraph = new List<Paragraph>();
            listParagraph.AddRange(this.GenGeneralInfo(plan));
            listParagraph.AddRange(this.GenActionContent(plan, listPlanTarget));
            byte[] data = this.GenDocPL01(listParagraph, this.GenSignArea(plan));
            return File(data, System.Net.Mime.MediaTypeNames.Application.Octet, "PL01_KH_Donvi_" + plan.DepartmentName + ".docx");
        }

        private byte[] GenDocPL01(List<Paragraph> listParagraph, Table tableSign)
        {
            // Tao file moi tu file cu
            string newFile = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".docx";
            string newFilePath = Path.Combine(Server.MapPath("~/TempFile"), newFile);
            string templateFilePath = Path.Combine(Server.MapPath("~/Template"), "PL01_DonVi.docx");
            System.IO.File.Copy(templateFilePath, newFilePath);
            using (WordprocessingDocument doc = WordprocessingDocument.Open(newFilePath, true))
            {
                MainDocumentPart mainPart = doc.MainDocumentPart;
                Paragraph firstP = mainPart.Document.Body.GetFirstChild<Paragraph>();
                if (firstP != null)
                {
                    firstP.Remove();
                }
                foreach (Paragraph p in listParagraph)
                {
                    mainPart.Document.Body.AppendChild(p);
                }
                Paragraph pNewLine = new Paragraph();
                mainPart.Document.Body.AppendChild(pNewLine);
                mainPart.Document.Body.AppendChild(tableSign);

                SectionProperties sectionProperties = new SectionProperties() { RsidRPr = "006647C9", RsidR = "003E08D4", RsidSect = "00965A25" };
                FooterReference footerReference = new FooterReference() { Type = HeaderFooterValues.Default, Id = "rId8" };
                PageSize pageSize = new PageSize() { Width = (int)11907U, Height = (int)16840U, Code = (int)9U };
                PageMargin pageMargin = new PageMargin() { Top = 864, Right = (int)994U, Bottom = 432, Left = (int)1642U, Header = (int)0U, Footer = (int)346U, Gutter = (int)0U };
                Columns columns = new Columns() { Space = "720" };
                DocGrid docGrid = new DocGrid() { LinePitch = 360 };

                sectionProperties.Append(footerReference);
                sectionProperties.Append(pageSize);
                sectionProperties.Append(pageMargin);
                sectionProperties.Append(columns);
                sectionProperties.Append(docGrid);
                mainPart.Document.Body.AppendChild(sectionProperties);
            }
            return System.IO.File.ReadAllBytes(newFilePath);
        }

        private Table GenSignArea(PlanModel plan)
        {
            Table table = new Table();
            TableProperties tableProperties = new TableProperties();
            TableWidth tableWidth = new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct };
            TableLook tableLook = new TableLook() { Val = "04A0", FirstRow = true, LastRow = false, FirstColumn = true, LastColumn = false, NoHorizontalBand = false, NoVerticalBand = true };

            tableProperties.Append(tableWidth);
            tableProperties.Append(tableLook);

            TableGrid tableGrid = new TableGrid();
            GridColumn gridColumn1 = new GridColumn() { Width = "4714" };
            GridColumn gridColumn2 = new GridColumn() { Width = "4557" };

            tableGrid.Append(gridColumn1);
            tableGrid.Append(gridColumn2);
            table.Append(tableProperties);
            table.Append(tableGrid);

            TableRow tr = new TableRow();
            TableCell c1 = new TableCell();
            c1.AddColWidth(33);
            Paragraph p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "Nơi nhận:", isBold: true, isItalic: true);
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Bộ Y tế;");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Thành viên Hội đồng quản lý Quỹ;");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Ban Kiểm soát;");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Ban Tư vấn;");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Cơ quan Điều hành Quỹ;");
            c1.Append(p);
            p = new Paragraph();
            p.AddSpacing(line: "276");
            p.AddText(text: "- Lưu VT.");
            c1.Append(p);
            tr.Append(c1);
            TableCell c2 = new TableCell();
            p = new Paragraph();
            p.AddCenter();
            p.AddSpacing(line: "276");
            p.AddText(text: "CHỦ TỊCH HỘI ĐỒNG", isBold: true, isNewLine: true, fontSize: "28");
            p.AddNewLine();
            p.AddText(text: "QUẢN LÝ LIÊN NGÀNH QUỸ", isBold: true, fontSize: "28");
            c2.Append(p);
            tr.Append(c2);
            table.Append(tr);
            return table;
        }

        private List<Paragraph> GenActionContent(PlanModel plan, List<PlanTargetModel> listPlanTarget)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            listParagraph.Add(this.CreateBasicParagraph(text: "I. MỤC TIÊU", isBold: true, left: "360"));
            listParagraph.Add(this.CreateBasicParagraph(text: "1. Mục tiêu chung:", isBold: true, left: "360"));
            listParagraph.AddRange(this.GenListParagraphFromContent(plan.PlanTarget));
            listParagraph.Add(this.CreateBasicParagraph(text: "2. Mục tiêu cụ thể:", isBold: true, left: "360"));
            Paragraph p;
            for (int i = 0; i < listPlanTarget.Count; i++)
            {
                int order = i + 1;
                PlanTargetModel pt = listPlanTarget[i];
                string title = "2." + order + ". " + pt.PlanTargetContent;
                listParagraph.Add(this.CreateBasicParagraph(text: title, isBold: true, isItalic: true));
                listParagraph.AddRange(this.GenListParagraphFromContent(pt.Description));
                listParagraph.Add(this.CreateBasicParagraph(text: "Chỉ số:", isBold: true, left: "360"));
                List<TargetIndexModel> listIndex = pt.ListPlanTargetIndex;
                if (listIndex != null)
                {
                    for (int j = 1; j <= listIndex.Count; j++)
                    {
                        TargetIndexModel index = listIndex[j - 1];
                        p = new Paragraph();
                        p.AddBasicSpacing("360");
                        double? value = index.ValueIndex;
                        string contentIndex = index.ContentIndex.Trim();
                        if (contentIndex.StartsWith("-") || contentIndex.StartsWith("+"))
                        {
                            contentIndex = contentIndex.Substring(1).Trim();
                        }
                        if (value != null)
                        {
                            if (index.TypeIndex.GetValueOrDefault() != Constants.TYPE_INDEX_CHOICE)
                            {
                                if (index.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_INPUT_AVERAGE)
                                {
                                    if (contentIndex.StartsWith("%"))
                                    {
                                        contentIndex = contentIndex.Substring(1).Trim();
                                    }
                                    contentIndex = value.Value.ToString() + "% " + contentIndex;
                                }
                                else
                                {
                                    string firtChar = contentIndex.First().ToString().ToLower();
                                    contentIndex = value.Value.ToString() + " " + firtChar + contentIndex.Substring(1);
                                }
                            }
                        }
                        p.AddText(text: "+ " + contentIndex, fontSize: "28");
                        listParagraph.Add(p);
                    }
                }
            }

            p = new Paragraph();
            p.AddNewLine();
            listParagraph.Add(p);
            listParagraph.Add(this.CreateBasicParagraph(text: "II. NỘI DUNG HOẠT ĐỘNG", isBold: true, left: "360"));

            for (int i = 1; i <= listPlanTarget.Count; i++)
            {
                PlanTargetModel target = listPlanTarget[i - 1];
                string title = "Mục tiêu " + i + ": " + target.PlanTargetContent;
                p = new Paragraph();
                p.AddBasicSpacing("360");
                p.AddText(text: title, isBold: true, fontSize: "28");
                listParagraph.Add(p);

                List<PlanTargetActionModel> listPlanTargetAction = target.ListPlanTargetActionModel;
                List<int> listPlanTargetActionID = listPlanTargetAction.Select(x => x.PlanTargetActionID).ToList();
                PlanHistoryBusiness bus = new PlanHistoryBusiness(context);
                Dictionary<int, string> dicTime = bus.ActionTimes(listPlanTargetActionID, plan.PlanID.Value, true);
                if (listPlanTargetAction != null)
                {
                    for (int j = 1; j <= listPlanTargetAction.Count; j++)
                    {
                        title = i + "." + j;
                        PlanTargetActionModel targetAction = listPlanTargetAction[j - 1];
                        targetAction.ActionCode = title;
                        targetAction.ActionTime = dicTime[targetAction.PlanTargetActionID];
                        listParagraph.AddRange(this.GenOnePlanTargetAction(targetAction));
                    }
                }
            }
            Dictionary<int, double> dicCost = new PlanBusiness(context).GetPlanCostByYear(plan.PlanID.Value);
            List<int> listYear = dicCost.Keys.ToList();
            p = new Paragraph();
            p.AddBasicSpacing(left: "360");
            p.AddNewLine();
            p.AddText(text: "III. KẾ HOẠCH KINH PHÍ", isBold: true, fontSize: "28");
            listParagraph.Add(p);
            int startYear = listYear[0];
            int endYear = listYear[listYear.Count - 1];
            long totalMoney = 0;
            foreach (int year in listYear)
            {
                long cost = Convert.ToInt64((dicCost[year] / 1000));
                p = new Paragraph();
                p.AddBasicSpacing(left: "360");
                p.AddText(text: "Kinh phí năm " + year + ": " + Utils.FormatMoney(cost) + " tỷ đồng", fontSize: "28");
                listParagraph.Add(p);
                totalMoney += cost;
            }
            p = new Paragraph();
            p.AddBasicSpacing(left: "360");
            p.AddText(text: "Tổng kinh phí giai đoạn " + startYear + "-" + endYear + Utils.FormatMoney(totalMoney) + " tỷ đồng", fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddBasicSpacing(left: "360");
            p.AddText(text: "(" + endYear + Utils.ChuyenSo(totalMoney.ToString()) + ")", fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "(Dự toán kinh phí kèm theo)", fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddBasicSpacing(left: "360");
            p.AddNewLine();
            p.AddText(text: "IV. TỔ CHỨC THỰC HIỆN", isBold: true, fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddBasicSpacing(left: "360");
            p.AddNewLine();
            p.AddText(text: "IV. TỔ CHỨC THỰC HIỆN", isBold: true, fontSize: "28");
            listParagraph.Add(p);
            listParagraph.AddRange(GenListParagraphFromContent(plan.Implementation));
            return listParagraph;
        }

        private List<Paragraph> GenGeneralInfo(PlanModel plan)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            Paragraph p = new Paragraph();
            p.AddCenter();
            p.AddNewLine();
            p.AddText(text: "KẾ HOẠCH HOẠT ĐỘNG GIAI ĐOẠN " + plan.FromDate.Year + " – " + plan.ToDate.Year, isBold: true, fontSize: "28");
            p.AddNewLine();
            p.AddText(text: "CỦA" + plan.DepartmentName.ToUpper(), isBold: true, fontSize: "28");
            listParagraph.Add(p);
            p = new Paragraph();
            p.AddCenter();
            p.AddText(text: "(Ban hành kèm theo Quyết định số …./QĐ-HĐQL ngày …/……/2017  của Chủ tịch Hội đồng quản lý liên ngành Quỹ Phòng, chống tác hại của thuốc lá)",
                fontSize: "27");
            p.AddNewLine();
            listParagraph.Add(p);
            return listParagraph;
        }

        private Paragraph CreateBasicParagraph(string text, bool isBold = false, bool isItalic = false, string left = null, string fontSize = "28")
        {
            Paragraph p = new Paragraph();
            p.AddBasicSpacing(left);
            p.AddText(text: text, isBold: isBold, isItalic: isItalic, fontSize: fontSize);
            return p;
        }

        private Paragraph AddBasicBullet(string title, string text)
        {
            Paragraph p = new Paragraph();
            p.AddBasicBullet(val: 3);
            p.AddBasicSpacing();
            p.AddText(text: title, isItalic: true);
            p.AddText(text: (string.IsNullOrWhiteSpace(text) ? string.Empty : text.Trim()));
            return p;
        }

        private List<Paragraph> GenOnePlanTargetAction(PlanTargetActionModel targetAction)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            if (!string.IsNullOrWhiteSpace(targetAction.ActionContent))
            {
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_TITLE));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_CONTENT));
            }
            if (targetAction.ListSubActionPlan != null && targetAction.ListSubActionPlan.Any())
            {
                List<int> listSubActionID = targetAction.ListSubActionPlan.Select(x => x.SubActionPlanID.GetValueOrDefault()).ToList();
                Dictionary<int, string> dicSubActionTime = new PlanHistoryBusiness(context).SubActionTimes(targetAction.PlanTargetID.Value, targetAction.PlanID.Value, true);
                for (int i = 1; i <= targetAction.ListSubActionPlan.Count; i++)
                {
                    SubActionPlanModel sub = targetAction.ListSubActionPlan[i - 1];
                    string title = targetAction.ActionCode + "." + i;
                    PlanTargetActionModel pta = new PlanTargetActionModel();
                    pta.ActionCode = title;
                    pta.ActionContent = sub.SubActionContent;
                    pta.ContentPropaganda = sub.ContentPropaganda;
                    pta.ObjectScope = sub.SubActionObject;
                    pta.ActionMethod = sub.SubActionMethod;
                    pta.Result = sub.Result;
                    if (dicSubActionTime.ContainsKey(sub.SubActionPlanID.Value))
                    {
                        pta.ActionTime = dicSubActionTime[sub.SubActionPlanID.Value];
                    }
                    listParagraph.AddRange(this.GenOnePlanTargetAction(pta));
                }
            }
            if (!string.IsNullOrWhiteSpace(targetAction.ActionContent))
            {
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_OBJECT));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_METHOD));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_RESULT));
                listParagraph.AddRange(this.GenOnePlanTargetActionContent(targetAction, Constants.PARAGRAPH_TARGET_ACTION_TIME));
            }
            return listParagraph;
        }

        private List<Paragraph> GenOnePlanTargetActionContent(PlanTargetActionModel targetAction, int type)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            Paragraph p = new Paragraph();
            p.AddBasicSpacing();
            p.AddIndentation("360");
            bool isItalic = (type == Constants.PARAGRAPH_TARGET_ACTION_TITLE);
            string content = string.Empty;
            string title = string.Empty;
            switch (type)
            {
                case Constants.PARAGRAPH_TARGET_ACTION_TITLE:
                    content = (string.IsNullOrWhiteSpace(targetAction.ActionCode) ? string.Empty : targetAction.ActionCode.Trim()) + ". " + targetAction.ActionContent.Trim();
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_CONTENT:
                    title = "Nội dung: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ContentPropaganda) ? string.Empty : targetAction.ContentPropaganda.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_OBJECT:
                    title = "Đối tượng, phạm vi: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ObjectScope) ? string.Empty : targetAction.ObjectScope.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_METHOD:
                    title = "Phương thức thực hiện: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.ActionMethod) ? string.Empty : targetAction.ActionMethod.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_RESULT:
                    title = "Kết quả dự kiến: ";
                    content = (string.IsNullOrWhiteSpace(targetAction.Result) ? string.Empty : targetAction.Result.Trim());
                    break;
                case Constants.PARAGRAPH_TARGET_ACTION_TIME:
                    if (!string.IsNullOrWhiteSpace(targetAction.ActionTime))
                    {
                        title = "Thời gian: ";
                        content = targetAction.ActionTime;
                    }
                    break;
            }
            if (!string.IsNullOrWhiteSpace(content) && !content.EndsWith("."))
            {
                content += ".";
            }
            if (!string.IsNullOrWhiteSpace(title))
            {
                if (!isItalic && (content.Contains("\r\n") || content.Contains("\n")))
                {
                    p.AddText(text: title.Trim(), isUnderline: true);
                }
                else
                {
                    p.AddText(text: title, isUnderline: true);
                }
                listParagraph.Add(p);
            }
            listParagraph.Add(p);

            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!isItalic && (content.Contains("\r\n") || content.Contains("\n")))
                {
                    if (type == Constants.PARAGRAPH_TARGET_ACTION_RESULT)
                    {
                        listParagraph.AddRange(this.GenListParagraphFromContent(content, true));
                    }
                    else
                    {
                        listParagraph.AddRange(this.GenListParagraphFromContent(content));
                    }
                }
                else
                {
                    if (type == Constants.PARAGRAPH_TARGET_ACTION_RESULT)
                    {
                        content = FormatResult(content);
                    }
                    else
                    {
                        content = FormatHead(content);
                    }
                    p.AddText(text: content, isItalic: isItalic, fontSize: "28");
                }
            }
            return listParagraph;
        }

        private string FormatHead(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return string.Empty;
            }
            if (s.Length == 1)
            {
                return s.ToUpper();
            }
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        private string FormatResult(string s)
        {
            s = FormatHead(s);
            var arr = s.Split(' ');
            double test = -1;
            if (double.TryParse(arr[0], out test))
            {
                s = arr[0];
                arr[0] = string.Empty;
                var content = string.Join(" ", arr).Trim();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    if (content.Length == 1)
                    {
                        return s + " " + content.ToLower();
                    }
                    return s + " " + char.ToLower(content[0]) + content.Substring(1);
                }
            }
            else
            {
                var nums = "0123456789";
                var fNum = string.Empty;
                int idx = 0;
                foreach (var c in arr[0])
                {
                    if (nums.Contains(c.ToString()))
                    {
                        fNum += c.ToString();
                    }
                    else
                    {
                        break;
                    }
                    idx++;
                }
                s = (fNum + " " + s.Substring(idx)).Trim();
                return FormatResult(s);
            }
            return s;
        }

        /// <summary>
        /// Truong hop nguoi dung nhap noi dung co dau xuong dong thi tach thanh cac Paragraph
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private List<Paragraph> GenListParagraphFromContent(string content, bool isAddHead = false)
        {
            List<Paragraph> listParagraph = new List<Paragraph>();
            if (!string.IsNullOrWhiteSpace(content))
            {
                string[] arr = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                foreach (string s in arr)
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        Paragraph p = new Paragraph();
                        p.AddBasicSpacing();
                        p.AddIndentation("360");
                        var value = s.Trim();
                        if (isAddHead)
                        {
                            value = "- " + FormatResult(s.Trim().Replace("  ", " "));
                        }
                        else
                        {
                            value = FormatHead(s.Trim().Replace("  ", " "));
                        }
                        p.AddText(text: value, fontSize: "28");
                        listParagraph.Add(p);
                    }
                }
            }
            return listParagraph;
        }

        private List<PlanTargetModel> GetListAction(int planID)
        {
            List<PlanTargetModel> listPlanTarget = this.context.PlanTarget.Where(x => x.PlanID == planID)
                .OrderBy(x => x.TargetOrder)
                .ThenBy(x => x.PlanTargetTitle)
                .Select(x => new PlanTargetModel
                {
                    PlanTargetID = x.PlanTargetID,
                    PlanTargetContent = x.PlanTargetContent,
                    Description = x.Description
                }).ToList();
            foreach (PlanTargetModel target in listPlanTarget)
            {
                List<PlanTargetActionModel> listAction = (from pt in this.context.PlanTargetAction
                                                          join p in this.context.PlanTarget on pt.PlanTargetID equals p.PlanTargetID
                                                          join s in this.context.SubActionPlan on pt.PlanTargetActionID equals s.PlanTargetActionID into sg
                                                          from sj in sg.DefaultIfEmpty()
                                                          where pt.PlanID == planID && pt.PlanTargetID == target.PlanTargetID
                                                          orderby pt.ActionOrder
                                                          group new { pt, sj } by new { p.PlanID, p.PlanTargetID, pt.PlanTargetActionID, pt.ActionContent, pt.ActionCode, pt.ActionOrder, pt.ContentPropaganda, pt.ObjectScope, pt.ActionMethod, pt.Result } into g
                                                          orderby g.Key.ActionOrder, g.Key.PlanTargetActionID
                                                          select new PlanTargetActionModel
                                                          {
                                                              PlanID = g.Key.PlanID,
                                                              PlanTargetID = g.Key.PlanTargetID,
                                                              PlanTargetActionID = g.Key.PlanTargetActionID,
                                                              ActionContent = g.Key.ActionContent,
                                                              ActionCode = g.Key.ActionCode,
                                                              ContentPropaganda = g.Key.ContentPropaganda,
                                                              ObjectScope = g.Key.ObjectScope,
                                                              ActionMethod = g.Key.ActionMethod,
                                                              Result = g.Key.Result,
                                                              ListSubActionPlan = g.Where(x => x.sj != null).OrderBy(x => x.sj.SubActionOrder).Select(x =>
                                                              new SubActionPlanModel
                                                              {
                                                                  SubActionPlanID = x.sj.SubActionPlanID,
                                                                  SubActionCode = x.sj.SubActionCode,
                                                                  SubActionContent = x.sj.SubActionContent,
                                                                  ContentPropaganda = x.sj.ContentPropaganda,
                                                                  SubActionObject = x.sj.SubActionObject,
                                                                  SubActionMethod = x.sj.SubActionMethod,
                                                                  Result = x.sj.Result
                                                              }).ToList()
                                                          }).ToList();
                target.ListPlanTargetActionModel = listAction;
                // Index
                List<TargetIndexModel> listPlanTargetIndex = (from pi in this.context.PlanTargetIndex
                                                              where pi.PlanID == planID && pi.PlanTargetID == target.PlanTargetID
                                                              orderby pi.PlanTargetIndexID
                                                              select new TargetIndexModel
                                                              {
                                                                  TypeIndex = pi.TypeIndex,
                                                                  ValueIndex = pi.ValueIndex,
                                                                  ContentIndex = pi.ContentIndex
                                                              }).ToList();
                target.ListPlanTargetIndex = listPlanTargetIndex;
            }
            return listPlanTarget;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}