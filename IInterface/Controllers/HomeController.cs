﻿using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IInterface.Business;
using Microsoft.Extensions.Caching.Memory;
using IInterface.Filter;
using System.IO;

namespace IInterface.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        [BreadCrumb(AreaName = "Màn hình chính")]
        public ActionResult Index()
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            if (objUser == null)
            {
                return RedirectToAction("LogOn", "Home");
            }
            bool isWarning = false;
            List<int> listMonthWarning = new List<int> { 1, 4, 7, 10 };
            // Kiem tra thoi gian canh bao
            int reportDay = Constants.REPORT_DAY;
            int dayBefore = Constants.REPORT_WARNING_BEFORE_DAY;
            DateTime now = DateTime.Now;
            int quarter = 0;
            int countDay = 0;
            int year = now.Year;
            int reportYear = now.Year;
            if (now.Month == 12)
            {
                year++;
            }
            if (now.Month <= 2)
            {
                reportYear--;
            }
            foreach (int reportMonth in listMonthWarning)
            {
                DateTime reportDate = new DateTime(year, reportMonth, reportDay);
                if (reportDate <= now.AddDays(dayBefore))
                {
                    quarter = (reportMonth / 3);
                    if (quarter == 0)
                    {
                        quarter = 4;
                    }
                    isWarning = true;
                    countDay = Convert.ToInt32((reportDate - now).TotalDays);
                    if (countDay < -dayBefore)
                    {
                        isWarning = false;
                    }
                }
            }
            ViewData["isWarning"] = isWarning;
            ViewData["countDay"] = countDay;
            bool isInFlow = true;
            using (Entities context = new Entities())
            {
                List<int> listRole = objUser.Role;
                if (!context.FlowProcess.Any(x => x.ProcessorType == Constants.FLOW_ROLE && listRole.Contains(x.ProcessorID))
                    && !context.FlowProcess.Any(x => x.ProcessorType == Constants.FLOW_DEPT && x.ProcessorID == objUser.DepartmentID))
                {
                    isInFlow = false;
                }
            }

            if (objUser.DeptType == Constants.SUB_TYPE || !isInFlow)
            {
                using (Entities context = new Entities())
                {
                    List<WarningBO> listWarning = new List<WarningBO>();
                    // Chi phi
                    Plan plan = context.Plan.Where(x => x.DepartmentID == objUser.DepartmentID && x.Status != Constants.IS_NOT_ACTIVE
                    && context.Period.Any(p => p.PeriodID == x.PeriodID && p.FromDate.Year <= reportYear && p.ToDate.Year >= reportYear
                    && p.Status == Constants.IS_ACTIVE)).FirstOrDefault();
                    if (plan != null)
                    {
                        // Bao cao hoat dong
                        // Quy
                        ActionReport actionReportQuarter = context.ActionReport.Where(x => x.Year == reportYear && x.Quarter == quarter && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                        listWarning.Add(new WarningBO
                        {
                            Quarter = quarter,
                            Year = reportYear,
                            Type = Constants.REPORT_TYPE_ACTION,
                            Status = actionReportQuarter == null ? Constants.PLAN_DEACTIVED : actionReportQuarter.Status
                        });
                        // Nam              
                        if (quarter == 4)
                        {
                            ActionReport actionReportYear = context.ActionReport.Where(x => x.Year == reportYear && x.Quarter == null && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                            listWarning.Add(new WarningBO
                            {
                                Quarter = null,
                                Year = reportYear,
                                Type = Constants.REPORT_TYPE_ACTION,
                                Status = actionReportYear == null ? Constants.PLAN_DEACTIVED : actionReportYear.Status
                            });
                        }
                        // Bao cao quyet toan
                        SettleSynthesis settleReportQuarter = context.SettleSynthesis.Where(x => x.Year == reportYear && x.Quarter == quarter && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                        listWarning.Add(new WarningBO
                        {
                            Quarter = quarter,
                            Year = reportYear,
                            Type = Constants.REPORT_TYPE_SETTLE,
                            Status = settleReportQuarter == null ? Constants.PLAN_DEACTIVED : settleReportQuarter.Status
                        });
                        // Bao cao tai chinh
                        // Quy
                        FinancialReport finReportQuarter = context.FinancialReport.Where(x => x.Year == reportYear && x.Quarter == quarter && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                        listWarning.Add(new WarningBO
                        {
                            Quarter = quarter,
                            Year = reportYear,
                            Type = Constants.REPORT_TYPE_FINANCIAL,
                            Status = finReportQuarter == null ? Constants.PLAN_DEACTIVED : finReportQuarter.Status
                        });
                        // Nam     
                        if (quarter == 4)
                        {
                            FinancialReport finReportYear = context.FinancialReport.Where(x => x.Year == reportYear && x.Quarter == null && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                            listWarning.Add(new WarningBO
                            {
                                Quarter = null,
                                Year = reportYear,
                                Type = Constants.REPORT_TYPE_FINANCIAL,
                                Status = finReportYear == null ? Constants.PLAN_DEACTIVED : finReportYear.Status
                            });
                        }
                        ViewData["listWarning"] = listWarning;

                        List<PlanTargetModel> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == plan.PlanID)
                            .Select(x => new PlanTargetModel
                            {
                                PlanTargetID = x.PlanTargetID,
                                PlanTargetTitle = x.PlanTargetTitle,
                                TotalBudget = x.TotalBuget,
                                TotalSettleMoney = 0
                            }).ToList();
                        // Lay thong tin chi phi tu bao cao quyet toan
                        List<SettleSynthesisCost> listSettleCost = (from a in context.SettleSynthesisCost
                                                                    join r in context.SettleSynthesis on a.SettleSynthesisID equals r.SettleSynthesisID
                                                                    where r.PlanID == plan.PlanID && a.PlanTargetActionID == null
                                                                    select a).ToList();

                        foreach (var pt in listPlanTarget)
                        {
                            pt.TotalSettleMoney = listSettleCost.Where(x => x.PlanTargetID == pt.PlanTargetID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                        }
                        ViewData["listPlanTarget"] = listPlanTarget;
                    }
                }
                return View("IndexDept");
            }
            else
            {
                using (Entities context = new Entities())
                {
                    Period period = context.Period.Where(x => x.FromDate.Year <= reportYear && x.ToDate.Year >= reportYear
                    && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                    if (period != null)
                    {
                        List<WarningBO> listWarning = new List<WarningBO>();
                        List<WarningBO> listTask = new List<WarningBO>();
                        int countDept = context.UserManageDepartment.Where(x => x.UserID == objUser.UserID).Count();
                        bool isManDept = true;
                        if (countDept == 0)
                        {
                            isManDept = false;
                            countDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE
                            && x.DepartmentType != Constants.DEPT_TYPE_PBQUY).Count();
                        }
                        bool isWarningYear = (isWarning && (quarter == 4));
                        FlowProcessBusiness flowBus = new FlowProcessBusiness(context);
                        // Hoat dong
                        #region Tong hop thong tin tu bao cao hoat dong
                        var listActionReport = (from a in context.ActionReport
                                                join p in context.Plan on a.PlanID equals p.PlanID
                                                where p.PeriodID == period.PeriodID
                                                && (!isManDept || context.UserManageDepartment.Any(d => d.UserID == objUser.UserID && d.DepartmentID == p.DepartmentID))
                                                && a.Year == reportYear
                                                select new
                                                {
                                                    a.ActionReportID,
                                                    a.Year,
                                                    a.Quarter,
                                                    a.Status
                                                }).ToList();
                        List<int> listActionReportID = listActionReport.Select(x => x.ActionReportID).ToList();
                        Dictionary<int, ObjectRoleBO> dicActionReportRole = flowBus.GetRoleForObject(objUser.UserID, listActionReportID, Constants.COMMENT_TYPE_ACTION);
                        listActionReport.RemoveAll(x => !dicActionReportRole.ContainsKey(x.ActionReportID) || dicActionReportRole[x.ActionReportID] == null || dicActionReportRole[x.ActionReportID].Role == Constants.ROLE_NO_VIEW);
                        listWarning.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_ACTION,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = countDept - listActionReport.Where(x => x.Status >= Constants.PLAN_NEXT_LEVER && x.Quarter == quarter).Count()
                        });
                        listTask.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_ACTION,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = listActionReport.Where(x => x.Status == Constants.PLAN_NEXT_LEVER && x.Quarter == quarter
                            && dicActionReportRole.ContainsKey(x.ActionReportID) && dicActionReportRole[x.ActionReportID] != null && dicActionReportRole[x.ActionReportID].Role >= Constants.ROLE_EDIT).Count()
                        });
                        if (isWarningYear)
                        {
                            listWarning.Add(new WarningBO
                            {
                                Type = Constants.REPORT_TYPE_ACTION,
                                Year = reportYear,
                                Count = countDept - listActionReport.Where(x => x.Status >= Constants.PLAN_NEXT_LEVER && x.Quarter == null).Count()
                            });
                            listTask.Add(new WarningBO
                            {
                                Type = Constants.REPORT_TYPE_ACTION,
                                Year = reportYear,
                                Count = listActionReport.Where(x => x.Status == Constants.PLAN_NEXT_LEVER && x.Quarter == null
                                && dicActionReportRole.ContainsKey(x.ActionReportID) && dicActionReportRole[x.ActionReportID] != null && dicActionReportRole[x.ActionReportID].Role >= Constants.ROLE_EDIT).Count()
                            });
                        }
                        #endregion

                        // Quyet toan
                        #region Tong hop bao cao quyet toan
                        var listSettleReport = (from a in context.SettleSynthesis
                                                join p in context.Plan on a.PlanID equals p.PlanID
                                                where p.PeriodID == period.PeriodID
                                                && (!isManDept || context.UserManageDepartment.Any(d => d.UserID == objUser.UserID && d.DepartmentID == p.DepartmentID))
                                                && a.Year == reportYear
                                                select new
                                                {
                                                    a.SettleSynthesisID,
                                                    a.Year,
                                                    a.Quarter,
                                                    a.Status
                                                }).ToList();
                        List<int> listlistSettleReportID = listSettleReport.Select(x => x.SettleSynthesisID).ToList();
                        Dictionary<int, ObjectRoleBO> dicSettleReportRole = flowBus.GetRoleForObject(objUser.UserID, listlistSettleReportID, Constants.COMMENT_TYPE_SETTLE);
                        listSettleReport.RemoveAll(x => !dicSettleReportRole.ContainsKey(x.SettleSynthesisID) || dicSettleReportRole[x.SettleSynthesisID] == null || dicSettleReportRole[x.SettleSynthesisID].Role == Constants.ROLE_NO_VIEW);
                        listWarning.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_SETTLE,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = countDept - listSettleReport.Where(x => x.Status >= Constants.PLAN_NEXT_LEVER && x.Quarter == quarter).Count()
                        });
                        listTask.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_SETTLE,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = listSettleReport.Where(x => x.Status == Constants.PLAN_NEXT_LEVER && x.Quarter == quarter
                            && dicSettleReportRole.ContainsKey(x.SettleSynthesisID) && dicSettleReportRole[x.SettleSynthesisID] != null && dicSettleReportRole[x.SettleSynthesisID].Role >= Constants.ROLE_EDIT).Count()
                        });
                        #endregion

                        // Tai chinh
                        #region Tong hop bao cao tai chinh
                        var listFinancialReport = (from a in context.FinancialReport
                                                   join p in context.Plan on a.PlanID equals p.PlanID
                                                   where p.PeriodID == period.PeriodID
                                                   && (!isManDept || context.UserManageDepartment.Any(d => d.UserID == objUser.UserID && d.DepartmentID == p.DepartmentID))
                                                   && a.Year == reportYear
                                                   select new
                                                   {
                                                       a.FinancialReportID,
                                                       a.Year,
                                                       a.Quarter,
                                                       a.Status
                                                   }).ToList();
                        List<int> listFinancialReportID = listFinancialReport.Select(x => x.FinancialReportID).ToList();
                        Dictionary<int, ObjectRoleBO> dicFinancialReportRole = flowBus.GetRoleForObject(objUser.UserID, listFinancialReportID, Constants.COMMENT_TYPE_FINANCIAL);
                        listFinancialReport.RemoveAll(x => !dicFinancialReportRole.ContainsKey(x.FinancialReportID) || dicFinancialReportRole[x.FinancialReportID] == null || dicFinancialReportRole[x.FinancialReportID].Role == Constants.ROLE_NO_VIEW);
                        listWarning.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_FINANCIAL,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = countDept - listFinancialReport.Where(x => x.Status >= Constants.PLAN_NEXT_LEVER && x.Quarter == quarter).Count()
                        });
                        listTask.Add(new WarningBO
                        {
                            Type = Constants.REPORT_TYPE_FINANCIAL,
                            Year = reportYear,
                            Quarter = quarter,
                            Count = listFinancialReport.Where(x => x.Status == Constants.PLAN_NEXT_LEVER && x.Quarter == quarter
                            && dicFinancialReportRole.ContainsKey(x.FinancialReportID) && dicFinancialReportRole[x.FinancialReportID] != null && dicFinancialReportRole[x.FinancialReportID].Role >= Constants.ROLE_EDIT).Count()
                        });
                        if (isWarningYear)
                        {
                            listWarning.Add(new WarningBO
                            {
                                Type = Constants.REPORT_TYPE_FINANCIAL,
                                Year = reportYear,
                                Count = countDept - listFinancialReport.Where(x => x.Status >= Constants.PLAN_NEXT_LEVER && x.Quarter == null).Count()
                            });
                            listTask.Add(new WarningBO
                            {
                                Type = Constants.REPORT_TYPE_FINANCIAL,
                                Year = reportYear,
                                Count = listFinancialReport.Where(x => x.Status == Constants.PLAN_NEXT_LEVER && x.Quarter == null
                                && dicFinancialReportRole.ContainsKey(x.FinancialReportID) && dicFinancialReportRole[x.FinancialReportID] != null && dicFinancialReportRole[x.FinancialReportID].Role >= Constants.ROLE_EDIT).Count()
                            });
                        }
                        #endregion

                        // Don vi quan ly
                        var listDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE
                        && context.UserManageDepartment.Any(u => u.UserID == objUser.UserID && u.DepartmentID == x.DepartmentID)).ToList();
                        ViewData["isWarningYear"] = isWarningYear;
                        ViewData["listWarning"] = listWarning;
                        ViewData["listTask"] = listTask;
                        ViewData["listDept"] = listDept;
                    }
                    return View("IndexReview");
                }
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult LogOn()
        {
            if (User.Identity.IsAuthenticated)
            {
                //chuyen tu cookie vao session
                try
                {
                    var res = SessionManager.TransferSession(User.Identity.Name);
                    if (res)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View();
                    }
                }
                catch
                {
                    return View(new ModelLogOn());
                }
            }
            return View(new ModelLogOn());
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult LogOn(ModelLogOn objLogin)
        {
            if (ModelState.IsValid)
            {
                string encriptPass = StringCipher.Encrypt(objLogin.Password);
                //Check validate
                using (Entities context = new Entities())
                {
                    if (IsNeedCheckCaptcha(objLogin.UserName) && !CaptchaBusiness.IsValidCaptcha(objLogin.Captcha))
                    {
                        return Json(new { Type = "CAPTCHAFAIL", Message = "Sai mã xác nhận" });
                    }
                    var user = context.User.FirstOrDefault(a => a.UserName.Equals(objLogin.UserName) && a.Status != Constants.IS_DELETED_USER
                                && a.Password.Equals(encriptPass));
                    if (user != null)
                    {
                        RemoveInvalidLogIn(objLogin.UserName);

                        if (user.Status == Constants.IS_NOT_ACTIVE)
                        {
                            throw new BusinessException("Tài khoản đã bị khóa, yêu cầu liên hệ với quản trị hệ thống");
                        }

                        UserInfo objUser = new UserInfo
                        {
                            UserName = user.UserName,
                            UserID = user.UserID,
                            FullName = user.FullName
                        };

                        var role = context.UserRole.Where(o => o.UserID == user.UserID).Select(o => o.RoleID);
                        if (role.Any())
                        {
                            objUser.Role = role.ToList();
                        }
                        objUser.DepartmentID = user.DepartmentID;

                        //Kiem tra co phai don vi quy hay la don vi cap duoi
                        var dept = context.Department.Find(user.DepartmentID);
                        objUser.DepartmentName = dept.DepartmentName;
                        if (dept.ParentID == null || dept.DepartmentType == Constants.DEPT_TYPE_PBQUY)
                        {
                            objUser.DeptType = Constants.MAIN_TYPE;
                        }
                        else
                        {
                            objUser.DeptType = Constants.SUB_TYPE;
                        }

                        objUser.ListMenu = SessionManager.GetListMenu(objUser.Role, context);
                        objUser.IsHeadOffice = user.IsHeadOffice.GetValueOrDefault();
                        user.LastLoginDate = DateTime.Now;
                        context.SaveChanges();

                        SessionManager.SetUserInfo(objUser);
                        FormsAuthentication.SetAuthCookie(objUser.UserName, true);
                        return Json(new { Type = "SUCCESS", RedirectUrl = Url.Action("Index", "Home") });
                    }

                    UpdateInvalidLogIn(objLogin.UserName);
                    if (IsNeedCheckCaptcha(objLogin.UserName))
                    {
                        return Json(new { Type = "CAPTCHA", Message = "Nhập captcha" });
                    }
                    return Json(new { Type = "LOGINFAIL", Message = "Sai thông tin đăng nhập" });
                }
            }
            throw new BusinessException("Sai thông tin đăng nhập");
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult LogOff()
        {
            SessionManager.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("LogOn", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public PartialViewResult ChangePassword()
        {
            ChangePasswordModel objModel = new ChangePasswordModel();
            var userInfo = SessionManager.GetUserInfo();

            objModel.FullName = userInfo.UserName;
            return PartialView(objModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            {
                using (Entities context = new Entities())
                {
                    int userId = SessionManager.GetUserInfo().UserID;
                    User entity = context.User.FirstOrDefault(x => x.UserID == userId);
                    if (entity == null)
                    {
                        throw new BusinessException("Dữ liệu không hợp lệ");
                    }

                    //truong hop doi mat khau
                    if (!string.IsNullOrWhiteSpace(model.NewPassword))
                    {
                        //Check pass 
                        if (!StringCipher.Encrypt(model.Password).Equals(entity.Password))
                        {
                            throw new BusinessException("Mật khẩu cũ không đúng");
                        }
                        //if (!Utils.IsStrongPassword(model.NewPassword, out Message))
                        //{
                        //    throw new BusinessException(Message);
                        //}
                        if (model.NewPassword.Equals(model.Password))
                        {
                            throw new BusinessException("Mật khẩu mới trùng mật khẩu cũ, vui lòng nhập lại");
                        }
                        if (!model.NewPassword.Equals(model.ConfirmNewPassword))
                        {
                            throw new BusinessException("Xác nhận lại mật khẩu không đúng");
                        }

                        entity.Password = StringCipher.Encrypt(model.NewPassword);
                    }

                    context.SaveChanges();
                }
                return Json(new { Type = "SUCCESS", Message = "Đổi mật khẩu thành công" });
            }

        }

        public long GetInvalidLogIn(string userName)
        {
            object count = CacheManagement.MemoryCache.Get("financialUser_" + userName.ToUpper());
            if (count == null)
            {
                return 0;
            }
            else
            {
                return (long)count;
            }
        }

        public long UpdateInvalidLogIn(string userName)
        {
            long updateCount = GetInvalidLogIn(userName) + 1;
            CacheManagement.MemoryCache.Set("financialUser_" + userName.ToUpper(), updateCount,
                new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(5)));
            return updateCount;
        }

        public bool IsNeedCheckCaptcha(string userName)
        {
            long count = GetInvalidLogIn(userName);
            return count >= Constants.LOGIN_FAIL_COUNT;
        }

        public void RemoveInvalidLogIn(string userName)
        {
            CacheManagement.MemoryCache.Remove("financialUser_" + userName.ToUpper());
        }

        [BreadCrumb(AreaName = "Trang lỗi")]
        public ActionResult RoleErrorBusiness(string msg)
        {
            if (!string.IsNullOrWhiteSpace(msg))
            {
                ViewBag.ErrorContent = msg;
            }
            return View();
        }
        [BreadCrumb(AreaName = "Trang lỗi")]
        public ActionResult RoleError()
        {
            return View();
        }
        [AllowAnonymous]
        [BreadCrumb(AreaName = "Lỗi hệ thống")]
        public ActionResult Error()
        {
            return View();
        }
        [AllowAnonymous]
        [BreadCrumb(AreaName = "Không tìm thấy đường dẫn")]
        public ActionResult NotFound()
        {
            return View("404");
        }

        [AllowAnonymous]
        public FileContentResult UserPhotos()
        {
            if (User.Identity.IsAuthenticated)
            {
                UserInfo objUser = SessionManager.GetUserInfo();
                if (objUser.Avatar == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Content/Images/avatar-default.jpg");

                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    var imageData = br.ReadBytes((int)imageFileLength);

                    return File(imageData, "image/jpeg");

                }
                // to get the user details to load user Image
                return new FileContentResult(objUser.Avatar, "image/jpeg");
            }
            {
                string fileName = HttpContext.Server.MapPath(@"~/Content/Images/avatar-default.jpg");

                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                var imageData = br.ReadBytes((int)imageFileLength);
                return File(imageData, "image/jpeg");
            }
        }
    }
}