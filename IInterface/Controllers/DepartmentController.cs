﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using IInterface.Extensions;
using IInterface.Filter;

//using MvcBreadCrumbs;

namespace IInterface.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly Entities _context = new Entities();

        // GET: Departments
        [BreadCrumb(ControllerName = "Mô hình tổ chức", ActionName = "Mô hình tổ chức", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ReloadParent()
        {
            List<SelectListItem> listParent = GetListParent();
            listParent.Insert(0, new SelectListItem { Value = "0", Text = "---Chọn đơn vị cha---" });
            ViewData["listParent"] = listParent;
            return PartialView("_ListParent");
        }

        public PartialViewResult ChooseTargetAction(int? id)
        {
            string str = "";
            var lst = _context.DepartmentAction.Where(o => o.DepartmentID == id).ToList();
            if (lst.Any())
            {
                foreach (var obj in lst)
                {
                    str += obj.TargetID + "-" + obj.ActionID + ",";
                }

                if (!string.IsNullOrEmpty(str))
                {
                    str = str.TrimEnd(',');
                }
            }
            //List<SelectListItem> listParent = GetListParent();
            //listParent.Insert(0, new SelectListItem { Value = "0", Text = "---Chọn đơn vị cha---" });
            ViewData["oldTargetAction"] = str;
            return PartialView("_ListTargetAction");
        }

        [BreadCrumb(ControllerName = "Mô hình tổ chức", ActionName = "Cập nhật", AreaName = "Hệ thống")]
        public ActionResult DepartmentInfo(int? id)
        {
            DepartmentModel model = new DepartmentModel();

            if (id.HasValue && id.Value > 0)
            {
                Department entity = _context.Department.FirstOrDefault(x => x.DepartmentID == id && x.Status == Constants.IS_ACTIVE);
                if (entity == null)
                {
                    throw new BusinessException("Không tồn tại đơn vị đã chọn");
                }
                model = new DepartmentModel();
                model.EntityTo(entity);
                List<MainActionModel> lstAction = (from ma in _context.MainAction
                                                   join ta in _context.Target on ma.TargetID equals ta.TargetID
                                                   join da in _context.DepartmentAction on ma.MainActionID equals da.ActionID
                                                   where ma.Status == Constants.IS_ACTIVE && ma.Type == Constants.MAIN_TYPE && da.DepartmentID == id
                                                   orderby ta.TargetOrder, ma.MainActionID
                                                   select new MainActionModel
                                                   {
                                                       TargetID = ta.TargetID,
                                                       MainActionID = ma.MainActionID,
                                                       ActionCode = ma.ActionCode,
                                                       ActionContent = ma.ActionContent,
                                                       TargetName = ta.TargetContent
                                                   }).ToList();
                ViewData["lstAction"] = lstAction;
            }

            // Lay don vi cha
            List<SelectListItem> listParent = GetListParent(id);
            listParent.Insert(0, new SelectListItem { Value = "0", Text = "---Chọn đơn vị cha---", Selected = (id.HasValue && model.ParentID == null) });
            ViewData["listParent"] = listParent;
            ViewData["listTarget"] = GetLstTarget();
            ViewData["listDeptType"] = Utils.GetDepartmentType();
            return View("AddOrEdit", model);
        }

        public PartialViewResult LoadActionChosen(List<string> actionIDs)
        {
            UserInfo user = SessionManager.GetUserInfo();
            if (actionIDs != null && actionIDs.Count > 0)
            {
                List<int> listMainActionID = new List<int>();
                foreach (string sActionID in actionIDs)
                {
                    int actionID;
                    string[] arr = sActionID.Split('-');
                    if (arr.Length > 1)
                    {
                        if (int.TryParse(arr[1], out actionID))
                        {
                            listMainActionID.Add(actionID);
                        }
                    }
                }
                List<MainActionModel> lstAction = (from ma in _context.MainAction
                                                   join ta in _context.Target on ma.TargetID equals ta.TargetID
                                                   where ma.Status == Constants.IS_ACTIVE && ma.Type == Constants.MAIN_TYPE && listMainActionID.Contains(ma.MainActionID)
                                                   orderby ta.TargetOrder, ma.MainActionID
                                                   select new MainActionModel
                                                   {
                                                       TargetID = ta.TargetID,
                                                       MainActionID = ma.MainActionID,
                                                       ActionCode = ma.ActionCode,
                                                       ActionContent = ma.ActionContent,
                                                       TargetName = ta.TargetContent
                                                   }).ToList();
                ViewData["lstAction"] = lstAction;
            }
            return PartialView("_ListActionChosen");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrEdit(DepartmentModel model)
        {
            string msg;
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(model.DepartmentCode))
                {
                    string deptCode = model.DepartmentCode.Trim().ToLower();
                    if (_context.Department.Any(x => x.Status == Constants.IS_ACTIVE
                    && (model.DepartmentID == 0 || x.DepartmentID != model.DepartmentID) && x.DepartmentCode.ToLower().Equals(deptCode)))
                    {
                        throw new BusinessException("Đã tồn tại đơn vị có mã là " + deptCode);
                    }
                }
                Department entity;
                if (model.DepartmentID > 0)
                {
                    entity = _context.Department.FirstOrDefault(x => x.DepartmentID == model.DepartmentID && x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại đơn vị đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật đơn vị thành công";
                }
                else
                {
                    model.Creator = SessionManager.GetUserInfo().UserName;
                    entity = model.ToEntity();
                    _context.Department.Add(entity);
                    msg = "Thêm mới đơn vị thành công";

                }
                _context.SaveChanges();
                //if (entity.DepartmentType.GetValueOrDefault() == Constants.DEPT_TYPE_PBQUY)
                //{
                //    if (model.TargetID != null && model.TargetID > 0)
                //    {
                //        List<DepartmentAction> listDeptAction = _context.DepartmentAction.Where(x => x.DepartmentID == entity.DepartmentID
                //        && x.TargetID == model.TargetID
                //        ).ToList();
                //        if (!listDeptAction.Any())
                //        {
                //            DepartmentAction da = new DepartmentAction();
                //            da.DepartmentID = entity.DepartmentID;
                //            da.TargetID = model.TargetID.Value;
                //            _context.DepartmentAction.Add(da);
                //        }
                //    }
                //}
                //else
                //{
                //    List<DepartmentAction> listDeptAction = _context.DepartmentAction.Where(x => x.DepartmentID == entity.DepartmentID).ToList();
                //    if (listDeptAction.Any())
                //    {
                //        _context.DepartmentAction.RemoveRange(listDeptAction);
                //    }
                //}

                if (!string.IsNullOrEmpty(model.ListTargetAction))
                {
                    List<DepartmentAction> listDeptAction = _context.DepartmentAction.Where(x => x.DepartmentID == entity.DepartmentID).ToList();
                    if (listDeptAction.Any())
                    {
                        _context.DepartmentAction.RemoveRange(listDeptAction);
                    }
                    var lst = model.ListTargetAction.Split(',');
                    if (lst.Length > 0)
                    {
                        List<DepartmentAction> lstIn = new List<DepartmentAction>();
                        foreach (string obj in lst)
                        {
                            int target = Convert.ToInt32(obj.Split('-')[0]);
                            int action = Convert.ToInt32(obj.Split('-')[1]);
                            if (!lstIn.Any(x => x.ActionID == action))
                            {
                                lstIn.Add(new DepartmentAction
                                {
                                    TargetID = target,
                                    ActionID = action,
                                    DepartmentID = entity.DepartmentID
                                });
                            }
                        }

                        _context.DepartmentAction.AddRange(lstIn);
                    }
                }
                else
                {
                    List<DepartmentAction> listDeptAction = _context.DepartmentAction.Where(x => x.DepartmentID == entity.DepartmentID).ToList();
                    if (listDeptAction.Any())
                    {
                        _context.DepartmentAction.RemoveRange(listDeptAction);
                    }
                }

                _context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }

        public JsonResult Delete(int id)
        {
            Department entity = _context.Department.FirstOrDefault(x => x.DepartmentID == id && x.Status == Constants.IS_ACTIVE);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại đơn vị đã chọn");
            }
            entity.Status = Constants.IS_NOT_ACTIVE;
            _context.SaveChanges();
            return Json("Xóa đơn vị thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SearchDepartment(string Keyword = null, int? DeptType = null, int page = 1)
        {
            UserInfo user = SessionManager.GetUserInfo();
            if (string.IsNullOrWhiteSpace(Keyword))
            {
                Keyword = null;
            }
            else
            {
                Keyword = Keyword.Trim().ToLower();
            }
            IQueryable<DepartmentModel> iqDepartment = _context.Department
                .Where(x => x.Status == Constants.IS_ACTIVE
                && (DeptType == null || x.DepartmentType == DeptType)
                && (Keyword == null || x.DepartmentName.ToLower().Contains(Keyword) || x.DepartmentCode.ToLower().Contains(Keyword)
                || x.Address.ToLower().Contains(Keyword) || x.PhoneNumber.ToLower().Contains(Keyword) || x.Fax.ToLower().Contains(Keyword)
                || x.AccountNumber.ToLower().Contains(Keyword) || x.BankName.ToLower().Contains(Keyword)
                || x.RepresentPersonName.ToLower().Contains(Keyword) || x.ContactPersonName.ToLower().Contains(Keyword))).Select(x => new DepartmentModel()
                {
                    DepartmentID = x.DepartmentID,
                    DepartmentCode = x.DepartmentCode,
                    DepartmentName = x.DepartmentName,
                    Address = x.Address,
                    PhoneNumber = x.PhoneNumber,
                    Fax = x.Fax,
                    AccountNumber = x.AccountNumber,
                    BankName = x.BankName,
                    RepresentPersonName = x.RepresentPersonName,
                    RepresentPosition = x.RepresentPosition,
                    RepresentAddress = x.RepresentAddress,
                    RepresentPhone = x.RepresentPhone,
                    RepresentEmail = x.RepresentEmail,
                    ContactPersonName = x.ContactPersonName,
                    ContactPosition = x.ContactPosition,
                    ContactAddress = x.ContactAddress,
                    ContactPhone = x.ContactPhone,
                    ContactEmail = x.ContactEmail,
                    IsUpDept = (x.IsUpDept == Constants.IS_ACTIVE ? true : false),
                    IsHasAdiction = (x.IsHasAdiction == Constants.IS_ACTIVE ? true : false),
                });
            Paginate<DepartmentModel> paginate = new Paginate<DepartmentModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = iqDepartment.OrderBy(x => x.DepartmentName).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = iqDepartment.Count();
            ViewData["listDepartment"] = paginate;
            return PartialView("_ListDepartment");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult LoadListAction(int? id)
        {
            List<SelectListItem> res = new List<SelectListItem>();
            res = GetLstMainAction(id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetListParent(int? id = null)
        {
            List<Department> listDepartment = _context.Department.Where(x => x.Status == Constants.IS_ACTIVE
                                                                                  && (id == null || x.DepartmentID != id)).OrderBy(x => x.ParentID).ThenBy(x => x.DepartmentName).ToList();
            List<Department> listRoot = listDepartment.Where(x => x.ParentID == null).ToList();
            Dictionary<int, List<Department>> dicRoot1 = new Dictionary<int, List<Department>>();
            listRoot.ForEach(x =>
            {
                dicRoot1[x.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x.DepartmentID).ToList();
            });

            Dictionary<int, List<Department>> dicRoot2 = new Dictionary<int, List<Department>>();
            dicRoot1.Values.ToList().ForEach(x =>
            {
                x.ForEach(x1 =>
                {
                    dicRoot2[x1.DepartmentID] = listDepartment.Where(c => c.ParentID != null && c.ParentID.Value == x1.DepartmentID).ToList();
                });
            });
            List<SelectListItem> listParent = new List<SelectListItem>();
            bool hasSelected = false;
            listRoot.ForEach(x =>
            {
                SelectListItem item = new SelectListItem
                {
                    Value = x.DepartmentID.ToString(),
                    Text = x.DepartmentName,
                    Selected = (!id.HasValue && !hasSelected && (x.ParentID == null))
                };
                if (x.ParentID == null)
                {
                    hasSelected = true;
                }
                listParent.Add(item);
                // cap 2
                if (dicRoot1.ContainsKey(x.DepartmentID))
                {
                    dicRoot1[x.DepartmentID].ForEach(x1 =>
                    {
                        SelectListItem item1 = new SelectListItem
                        {
                            Value = x1.DepartmentID.ToString(),
                            Text = "... " + x1.DepartmentName
                        };
                        listParent.Add(item1);
                        // cap 3
                        if (dicRoot2.ContainsKey(x1.DepartmentID))
                        {
                            dicRoot2[x1.DepartmentID].ForEach(x2 =>
                            {
                                SelectListItem item2 = new SelectListItem
                                {
                                    Value = x2.DepartmentID.ToString(),
                                    Text = "...... " + x2.DepartmentName
                                };
                                listParent.Add(item2);
                            });
                        }
                    });
                }
            });
            return listParent;
        }

        private List<SelectListItem> GetLstTarget()
        {
            var res = new List<SelectListItem>();
            res.Add(new SelectListItem
            {
                Text = "-- Chọn --",
                Value = ""
            });
            var lst = _context.Target.Where(o => o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.TargetTitle,
                Value = o.TargetID.ToString()
            }).ToList();

            if (lst.Count > 0)
            {
                res.AddRange(lst);
            }

            return res;
        }

        private List<SelectListItem> GetLstMainAction(int? targetId)
        {
            var res = new List<SelectListItem>();

            res = _context.MainAction.Where(o => o.TargetID == targetId && o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.ActionCode + "-" + o.ActionContent,
                Value = o.TargetID.ToString()
            }).ToList();

            return res;
        }


        public PartialViewResult SearchTarget(string targetName = null)
        {
            if (string.IsNullOrWhiteSpace(targetName))
            {
                targetName = null;
            }
            else
            {
                targetName = targetName.Trim().ToLower();
            }
            List<TargetModel> listTarget = _context.Target.Where(x => x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE
            && (targetName == null || x.TargetContent.ToLower().Contains(targetName))).Select(x => new TargetModel
            {
                TargetTitle = x.TargetTitle,
                TargetContent = x.TargetContent,
                TargetID = x.TargetID,
                TargetOrder = x.TargetOrder,
            }).ToList();
            ViewData["listTarget"] = listTarget;
            return PartialView("_ListTarget");
        }

        public PartialViewResult ViewListActionByTarget(int? targetId, int? departmentId, List<string> actionIDs)
        {
            Target entity = _context.Target.FirstOrDefault(x => x.TargetID == targetId
            && x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại mục tiêu đã chọn");
            }
            List<int?> listAction = _context.DepartmentAction.Where(x => x.DepartmentID == departmentId && x.TargetID == targetId)
                .Select(x => x.ActionID).ToList();
            if (actionIDs != null && actionIDs.Count > 0)
            {
                List<int> listMainActionID = new List<int>();
                foreach (string sActionID in actionIDs)
                {
                    int actionID;
                    string[] arr = sActionID.Split('-');
                    if (arr.Length > 1)
                    {
                        if (int.TryParse(arr[1], out actionID))
                        {
                            listAction.Add(actionID);
                        }
                    }
                }
            }
            List<MainActionModel> lstAction = _context.MainAction.Where(x => x.TargetID == targetId
             && x.Status == Constants.IS_ACTIVE && x.Type == Constants.MAIN_TYPE)
            .Select(x => new MainActionModel
            {
                TargetID = x.TargetID,
                MainActionID = x.MainActionID,
                ActionCode = x.ActionCode,
                ActionContent = x.ActionContent,
                IsSelected = listAction.Contains(x.MainActionID)
            }).ToList();

            ViewData["lstAction"] = lstAction;
            ViewData["targetId"] = targetId;
            ViewData["targetTitle"] = entity.TargetTitle;
            return PartialView("_ListAction");
        }
    }
}