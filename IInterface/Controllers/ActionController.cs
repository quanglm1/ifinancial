﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class ActionController : Controller
    {
        // GET: Action
        private readonly Entities _context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Hoạt động", AreaName = "Hoạt động")]
        public ActionResult Index(int? type)
        {
            ViewData["typeDept"] = type;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SearchCode"></param>
        /// <param name="SearchContent"></param>
        /// <param name="typeDept">sub or main</param>
        /// <param name="page"></param>
        /// <returns></returns>
        public PartialViewResult Search(string SearchCode = null, string SearchContent = null, int? typeDept = 0, int TargetID = 0, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            IQueryable<MainActionModel> IQSearch =
                from x in this._context.MainAction
                join y in _context.Target on x.TargetID equals y.TargetID
                where x.Status != Constants.IS_NOT_ACTIVE
                && x.Type == typeDept
                && (TargetID == 0 || y.TargetID == TargetID)
                select new MainActionModel
                {
                    TypeFor = x.Type,
                    MainActionID = x.MainActionID,
                    MissionID = x.MissionID,
                    ParentID = x.ParentID,
                    Status = x.Status,
                    TargetID = x.TargetID,
                    ActionCode = x.ActionCode,
                    ParentName = _context.MainAction.FirstOrDefault(o => o.MainActionID == x.ParentID).ActionCode,
                    TargetName = y.TargetTitle,
                    ActionContent = x.ActionContent,
                    CrateUser = x.CrateUser,
                    CreatDate = x.CreatDate.Value,
                    Level = x.Level
                };

            if (!string.IsNullOrWhiteSpace(SearchContent))
            {
                IQSearch = IQSearch.Where(x => x.ActionContent.ToLower().Contains(SearchContent.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(SearchCode))
            {
                IQSearch = IQSearch.Where(x => x.ActionCode.ToLower().Contains(SearchCode.Trim().ToLower()));
            }

            Paginate<MainActionModel> paginate = new Paginate<MainActionModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = IQSearch.OrderBy(o => o.TargetID).ThenBy(o => o.ActionCode).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList(),
                Count = IQSearch.Count()
            };
            ViewData["listResult"] = paginate;
            return PartialView("_LstResult");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeDept">sub or main</param>
        /// <param name="type">hành động thêm mới, sửa, thêm con</param>
        /// <returns></returns>
        public PartialViewResult PrepareAddOrEdit(int? id, int? typeDept, int? type)
        {
            var model = new MainActionModel();

            var listTarget = GetListTarget(typeDept).ToList();
            ViewData["listTarget"] = listTarget;
            ViewData["ListMission"] = GetListMission();
            if (type == Constants.ACTION_ADD_NEW)
            {
                model.TypeFor = typeDept;
                model.ActionType = type;
            }

            if (type == Constants.ACTION_ADD_CHILD)
            {
                var parent = _context.MainAction.Find(id);
                if (parent != null)
                {
                    var count = _context.MainAction.Count(o => o.ParentID == parent.MainActionID) + 1;
                    model.ParentID = id;
                    model.ActionCode = parent.ActionCode + "." + count;
                    model.ParentName = parent.ActionContent;
                    model.TargetID = parent.TargetID;
                    model.MissionID = parent.MissionID;
                    model.TypeFor = parent.Type;
                    model.ActionType = type;
                }
                else
                {
                    throw new BusinessException("Hoạt động cha không hợp lệ");
                }
            }

            var listAddedDeptID = new List<int>();
            var listAddedDeptType = new List<int>();
            if (type == Constants.ACTION_EDIT)
            {
                model = this._context.MainAction
                      .Where(x => x.MainActionID == id && x.Status != Constants.IS_NOT_ACTIVE)
                      .Select(x => new MainActionModel
                      {
                          TypeFor = x.Type,
                          MainActionID = x.MainActionID,
                          MissionID = x.MissionID,
                          ParentID = x.ParentID,
                          ParentName = _context.MainAction.FirstOrDefault(o => o.MainActionID == x.ParentID).ActionContent,
                          Status = x.Status,
                          TargetID = x.TargetID,
                          ActionCode = x.ActionCode,
                          ActionContent = x.ActionContent,
                          CrateUser = x.CrateUser,
                          CreatDate = x.CreatDate.Value,
                          ActionType = type,
                          IsOther = (x.IsOther == Constants.IS_ACTIVE)
                      }).FirstOrDefault();

                if (model == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động đã chọn");
                }
                listAddedDeptID.AddRange(_context.MappingActionDepartment.Where(x => x.MainActionID == model.MainActionID && x.DepartmentID != null)
                    .Select(x => x.DepartmentID.Value).ToList());
                listAddedDeptType.AddRange(_context.MappingActionDepartment.Where(x => x.MainActionID == model.MainActionID)
                    .Select(x => x.DepartmentType).ToList());
            }
            if (model.TargetID > 0)
            {
                ViewData["listParent"] = GetListParent(model.TargetID, model.TypeFor);
            }
            else
            {
                ViewData["listParent"] = GetListParent(Convert.ToInt32(listTarget[0].Value), model.TypeFor);
            }
            model.listDeptID = listAddedDeptID.ToArray();
            model.DeptType = listAddedDeptType.ToArray();
            return PartialView("_AddOrEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(MainActionModel model)
        {
            if (ModelState.IsValid)
            {
                string msg = null;
                MainAction entity = null;
                if (model.ActionType == Constants.ACTION_EDIT)
                {
                    entity = this._context.MainAction.FirstOrDefault(x => x.MainActionID == model.MainActionID &&
                                                                     x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại hoạt động đã chọn");
                    }
                    model.UpdateEntity(entity);

                    var listDept = _context.MappingActionDepartment.Where(x => x.MainActionID == entity.MainActionID).ToList();
                    _context.MappingActionDepartment.RemoveRange(listDept);
                    msg = "Cập nhật thành công";
                }
                else
                {
                    var parent = _context.MainAction.Find(model.ParentID);
                    if (parent != null)
                    {
                        model.Level = parent.Level == null ? 1 : parent.Level + 1;
                        model.MissionID = parent.MissionID;
                    }

                    model.CreatDate = DateTime.Now;
                    model.Status = Constants.IS_ACTIVE;

                    entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    entity.MissionID = model.MissionID;
                    this._context.MainAction.Add(entity);
                    msg = "Thêm mới thành công";
                }
                if (model.TypeFor == Constants.SUB_TYPE && model.IsOther)
                {
                    var query = _context.MainAction.Where(x => x.IsOther == Constants.IS_ACTIVE);
                    if (model.ActionType == Constants.ACTION_EDIT)
                    {
                        query = query.Where(x => x.MainActionID != model.MainActionID);
                    }
                    var otherAction = query.FirstOrDefault();
                    if (otherAction != null)
                    {
                        throw new BusinessException("Đã có hoạt động khác với mã '" + otherAction.ActionCode + "' và nội dung '" + otherAction.ActionContent + "'");
                    }
                }
                _context.SaveChanges();
                if (entity.Type != Constants.SUB_TYPE && model.DeptType != null && model.DeptType.Any())
                {
                    var listDept = new List<Department>();
                    if (model.listDeptID != null && model.listDeptID.Any())
                    {
                        listDept = _context.Department.Where(x => x.Status == Constants.IS_ACTIVE && model.listDeptID.Contains(x.DepartmentID)).ToList();
                    }
                    foreach (var deptType in model.DeptType)
                    {
                        var listDeptByType = listDept.Where(x => x.DepartmentType == deptType).ToList();
                        if (listDeptByType != null && listDeptByType.Any())
                        {
                            foreach (var dept in listDeptByType)
                            {
                                var mappingActionDept = new MappingActionDepartment
                                {
                                    MainActionID = entity.MainActionID,
                                    DepartmentType = deptType,
                                    DepartmentID = dept.DepartmentID
                                };
                                _context.MappingActionDepartment.Add(mappingActionDept);
                            }
                        }
                        else
                        {
                            var mappingActionDept = new MappingActionDepartment
                            {
                                MainActionID = entity.MainActionID,
                                DepartmentType = deptType
                            };
                            _context.MappingActionDepartment.Add(mappingActionDept);
                        }
                    }
                    _context.SaveChanges();
                }
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        public JsonResult Delete(int id)
        {
            MainAction obj = _context.MainAction.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại hoạt động đã chọn");
            }
            //if (_context.PlanTargetAction.Any(x => x.MainActionID == id) || _context.SubActionPlan.Any(x => x.SubActionID == id))
            //{
            //    throw new BusinessException("Hoạt động đã được sử dụng trong kế hoạch, không thể xóa");
            //}
            obj.Status = Constants.IS_NOT_ACTIVE;
            this._context.SaveChanges();
            return Json("Xóa hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadListParentAction(int? targetId, int? deptType)
        {
            List<SelectListItem> res = new List<SelectListItem>();
            res = GetListParent(targetId, deptType).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadListDept(int?[] deptType, int id = 0)
        {
            List<SelectListItem> res = new List<SelectListItem>();
            if (deptType != null)
            {
                foreach (var t in deptType)
                {
                    res.AddRange(this.GetListDept(t, id));
                }
            }
            ViewData["listDept"] = res;
            return PartialView("_listDept");
        }

        private IEnumerable<SelectListItem> GetListParent(int? targetId, int? deptType)
        {
            var res = _context.MainAction.Where(o => o.TargetID == targetId && o.Type == deptType
            && o.Status == Constants.IS_ACTIVE).Select(
                o => new SelectListItem
                {
                    Text = o.ActionCode + " - " + o.ActionContent,
                    Value = o.MainActionID + ""
                }).ToList();

            res.Insert(0, new SelectListItem
            {
                Text = "-- Chọn --",
                Value = ""
            });
            return res;
        }

        private IEnumerable<SelectListItem> GetListTarget(int? typeDept)
        {
            List<SelectListItem> res;

            if (typeDept == Constants.MAIN_TYPE)
            {
                res = _context.Target.Where(o => o.Type == Constants.MAIN_TYPE && o.Status == Constants.IS_ACTIVE).Select(
                      o => new SelectListItem
                      {
                          Text = o.TargetTitle + " - " + o.TargetContent,
                          Value = o.TargetID.ToString()
                      }).ToList();
            }
            else
            {
                res = _context.Target.Where(o => o.Status == Constants.IS_ACTIVE && o.Type == Constants.SUB_TYPE).Select(
                    o => new SelectListItem
                    {
                        Text = o.TargetTitle + " - " + o.TargetContent,
                        Value = o.TargetID.ToString()
                    }).ToList();
            }

            return res;
        }

        private List<SelectListItem> GetListDept(int? deptType, int id = 0)
        {
            List<SelectListItem> res = new List<SelectListItem>();
            var listAddedDeptID = new List<int>();
            if (id > 0)
            {
                listAddedDeptID.AddRange(_context.MappingActionDepartment.Where(x => x.MainActionID == id && x.DepartmentID != null)
                    .Select(x => x.DepartmentID.Value).ToList());
            }
            res.Add(new SelectListItem
            {
                Text = "-- Chọn --",
                Value = ""
            });
            if (deptType != null)
            {
                res.AddRange(this._context.Department.Where(x => x.ParentID != null
                && x.Status == Constants.IS_ACTIVE && x.DepartmentType == deptType)

                .Select(
                o => new SelectListItem
                {
                    Text = o.DepartmentName,
                    Value = o.DepartmentID + "",
                    Selected = listAddedDeptID.Contains(o.DepartmentID)
                }).ToList());

            }
            return res;
        }

        private List<SelectListItem> GetListMission()
        {
            var res = _context.Mission.Where(o => o.Status == Constants.IS_ACTIVE).Select(o => new SelectListItem
            {
                Text = o.MissionCode + "-" + o.MissionName,
                Value = o.MissionID.ToString()
            }).ToList();

            return res;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}