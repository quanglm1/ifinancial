﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class ActionLogController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Giám sát hệ thống", ActionName = "Giám sát hệ thống", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult Search(ActionLogModel model, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            IQueryable<ActionLogModel> IQSearch =
                from x in this.context.ActionLog
                join y in context.SystemAction on new { x.ActionName, x.ControllerName } equals new { y.ActionName, y.ControllerName }
                select new ActionLogModel
                {
                    UserName = x.UserName,
                    ActionTime = x.ActionTime,
                    ActionDescription = y.Description
                };

            if (!string.IsNullOrWhiteSpace(model.ActionName))
            {
                IQSearch = IQSearch.Where(x => x.ActionDescription.ToLower().Contains(model.ActionName.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(model.UserName))
            {
                IQSearch = IQSearch.Where(x => x.UserName.ToLower().Contains(model.UserName.Trim().ToLower()));
            }
            if (model.FromDate != null)
            {
                IQSearch = IQSearch.Where(x => x.ActionTime >= model.FromDate);
            }
            if (model.ToDate != null)
            {
                IQSearch = IQSearch.Where(x => x.ActionTime <= model.ToDate);
            }

            Paginate<ActionLogModel> paginate = new Paginate<ActionLogModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = IQSearch.OrderBy(o => o.ActionTime).ThenBy(o => o.UserName).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList(),
                Count = IQSearch.Count()
            };
            ViewData["listResult"] = paginate;
            return PartialView("_ListResult");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}