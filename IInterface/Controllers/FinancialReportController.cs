﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class FinancialReportController : ReportController
    {
        [BreadCrumb(ControllerName = "Báo cáo tài chính", ActionName = "Báo cáo tài chính", AreaName = "Báo cáo")]
        public ActionResult IndexDept()
        {
            return BaseIndexDept();
        }

        [BreadCrumb(ControllerName = "Báo cáo tài chính", ActionName = "Báo cáo tài chính", AreaName = "Xem xét báo cáo")]
        public ActionResult IndexReview()
        {
            return BaseIndexReview();
        }

        [BreadCrumb(ControllerName = "Báo cáo tài chính", ActionName = "Báo cáo tài chính", AreaName = "Báo cáo")]
        public ActionResult AddOrEdit(int? id, int periodID = 0, int year = 0, int? quarter = 0, int isReview = 0)
        {
            FinancialReportInfoModel model = null;
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            Plan plan = null;
            if (id == null || id == 0)
            {
                if (periodID <= 0 || year <= 0)
                {
                    RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Dữ liệu không hợp lệ" });
                }
                if (isReview == 0)
                {
                    if (quarter == 0)
                    {
                        quarter = null;
                    }
                    UserInfo user = SessionManager.GetUserInfo();
                    plan = context.Plan.Where(x => x.DepartmentID == user.DepartmentID && x.PeriodID == periodID).FirstOrDefault();
                    if (plan == null)
                    {
                        RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Kế hoạch chưa được phê duyệt" });
                    }
                    FinancialReport report = context.FinancialReport.Where(x => x.DepartmentID == user.DepartmentID && x.Year == year && x.Quarter == quarter
                    && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                    if (report == null)
                    {
                        report = new FinancialReport();
                        report.CreateDate = DateTime.Now;
                        report.Creator = user.UserName;
                        report.DepartmentID = user.DepartmentID;
                        report.PlanID = plan.PlanID;
                        report.Quarter = quarter;
                        report.Status = Constants.PLAN_NEW;
                        report.Type = user.DeptType;
                        report.Year = year;
                        context.FinancialReport.Add(report);
                        context.SaveChanges();
                    }
                    id = report.FinancialReportID;
                }
            }
            if (id != null)
            {
                model = bus.GetFinancialReportInfo(id.Value);
                year = model.Year;
                plan = this.context.Plan.Find(model.PlanID);
                // Lay tong chi phi
                double sumMoneyReport = bus.GetTotalSettleMoney(context.FinancialReport.Find(id), FinancialReportBusiness.GET_TOTAL);
                model.MoneyInQuarter = sumMoneyReport;
            }
            double planMoney = SetAddOrEditData(id, plan, year, isReview, true);
            if (model != null)
            {
                model.PlanMoney = planMoney;
            }
            return View("AddOrEdit", model);
        }
        [BreadCrumb(ControllerName = "Báo cáo tài chính", ActionName = "Chi tiết", AreaName = "Báo cáo")]
        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }
        public JsonResult SaveGeneralInfo(FinancialReportData data)
        {
            FinancialReport FinancialReport = null;
            int deptID = SessionManager.GetUserInfo().DepartmentID.Value;
            Plan plan = this.context.Plan.Where(x => x.PeriodID == data.PeriodID && x.DepartmentID == deptID && x.Status == Constants.PLAN_APPROVED).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            if (data.FinancialReportID > 0)
            {
                FinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == data.FinancialReportID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                if (FinancialReport == null)
                {
                    throw new BusinessException("Không tồn tại báo cáo tài chính, vui lòng kiểm tra lại");
                }
                FinancialReport.PlanID = plan.PlanID;
                FinancialReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    FinancialReport.Quarter = data.Quarter;
                }
                this.context.SaveChanges();
            }
            else
            {
                // Check xem bao cao da tao chua
                FinancialReport = this.context.FinancialReport.Where(x => x.PlanID == plan.PlanID && x.Year == data.Year && x.Quarter == data.Quarter).FirstOrDefault();
                if (FinancialReport != null)
                {
                    throw new BusinessException("Đã tồn tại báo cáo tài chính cho thời gian và kế hoạch vừa chọn, vui lòng kiểm tra lại");
                }
                FinancialReport = new FinancialReport();
                FinancialReport.PlanID = plan.PlanID;
                FinancialReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    FinancialReport.Quarter = data.Quarter;
                }
                FinancialReport.CreateDate = DateTime.Now;
                FinancialReport.Status = Constants.PERIOD_NEW;
                FinancialReport.Creator = SessionManager.GetUserInfo().UserID.ToString();
                FinancialReport.DepartmentID = SessionManager.GetUserInfo().DepartmentID;
                this.context.FinancialReport.Add(FinancialReport);
                this.context.SaveChanges();
            }
            this.context.SaveChanges();
            return Json(FinancialReport.FinancialReportID, JsonRequestBehavior.AllowGet);
        }

        private void ValidateEditRole(int id)
        {
            var user = SessionManager.GetUserInfo();
            var busRole = new FlowProcessBusiness(this.context);
            var role = busRole.GetRoleForObject(user.UserID, id, Constants.COMMENT_TYPE_FINANCIAL);
            if (role == null || role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
        }

        public JsonResult DeleteReport(int id)
        {
            FinancialReport entity = this.context.FinancialReport.Where(x => x.FinancialReportID == id && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo tài chính");
            }
            var user = SessionManager.GetUserInfo();
            Plan plan = this.context.Plan.Where(x => x.PlanID == entity.PlanID && x.DepartmentID == user.DepartmentID).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            ValidateEditRole(id);
            this.context.Database.ExecuteSqlCommand("delete from FinancialReportDetail where FinancialReportID = " + entity.FinancialReportID);
            this.context.Database.ExecuteSqlCommand("delete from ReportReview where ReportID = " + entity.FinancialReportID + " and ReportType = " + Constants.REPORT_TYPE_FINANCIAL);
            this.context.FinancialReport.Remove(entity);
            this.context.SaveChanges();
            return Json("Xóa báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendReport(int id)
        {
            new ReportReviewBusiness(this.context).SendReport(id, Constants.REPORT_TYPE_FINANCIAL);
            return Json("Gửi báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadActionInfo(int FinancialReportID, int planTargetID, int isView = 0)
        {
            FinancialReport entity = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo tài chính hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == planTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            List<FinancialReportDetailModel> listDetail = bus.GetListAction(0, planTargetID, entity);
            ViewData["financialReport"] = entity;
            ViewData["listDetail"] = listDetail;
            FinancialReportDetail detail = this.context.FinancialReportDetail.Where(x => x.FinancialReportID == FinancialReportID
                    && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.FinancialReportDetailID && c.Type == Constants.COMMENT_TYPE_FINANCIAL
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.FinancialReportID;
            }
            if (isView == 0)
            {
                return PartialView("_ActionInfo");
            }
            else
            {
                return PartialView("_ActionInfoView");
            }
        }

        public PartialViewResult Search(int PeriodID, int Year, int DeptID = 0, int Status = 0, int Quarter = 0, int IsReview = 0, int page = 1)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (IsReview == Constants.IS_ACTIVE)
            {
                ViewData["listResult"] = new FinancialReportBusiness(this.context).GetListFinancialReport(userInfo.UserID, PeriodID, DeptID, Status, Quarter, Year, page); ;
            }
            else
            {
                ViewData["listResult"] = new FinancialReportBusiness(this.context).GetListFinancialReport(userInfo.UserID, PeriodID, userInfo.DepartmentID.Value, Status, Quarter, Year, page); ;
            }
            ViewData["isReview"] = IsReview;
            return PartialView("_ListResult");
        }

        public PartialViewResult LoadReportDetail(int planTargetActionID, int planTargetID, int FinancialReportID, int isView = 0)
        {
            FinancialReport entityFinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entityFinancialReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            List<FinancialReportDetailModel> listDetail = bus.GetListAction(planTargetActionID, planTargetID, entityFinancialReport);
            ViewData["listDetail"] = listDetail;
            ViewData["planTargetID"] = planTargetID;
            string postFix = string.Empty;
            // Bao cao tai chinh chi xem
            isView = 1;
            if (isView == 1)
            {
                postFix = "View";
            }
            if (entityFinancialReport.Quarter != null)
            {
                return PartialView("_ListDetailQuarter" + postFix);
            }
            else
            {
                ViewData["financialReport"] = entityFinancialReport;
                return PartialView("_ListDetailYear" + postFix);
            }
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, int FinancialReportID)
        {
            FinancialReport entityFinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entityFinancialReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID
            && x.ActionContent != null && x.ActionContent.Trim() != "")
            .OrderBy(x => x.ActionOrder)
                .Select(x => new PlanTargetActionModel
                {
                    PlanID = x.PlanID.Value,
                    PlanTargetID = x.PlanTargetID.Value,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }

        public PartialViewResult LoadDisb(int periodID, int reportID = 0, int? type = null)
        {
            FinancialReport report = null;
            int isView = 1;
            int planID = 0;
            if (reportID > 0)
            {
                if (type.GetValueOrDefault() == Constants.REPORT_TYPE_SETTLE)
                {
                    UserInfo user = SessionManager.GetUserInfo();
                    ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, reportID, Constants.COMMENT_TYPE_SETTLE);
                    if (role != null && role.Role >= Constants.ROLE_EDIT)
                    {
                        isView = 0;
                    }
                    SettleSynthesis settleReport = context.SettleSynthesis.Find(reportID);
                    if (settleReport != null)
                    {
                        // Lay bao cao tai chinh
                        report = context.FinancialReport.Where(x => x.PlanID == settleReport.PlanID && x.Year == settleReport.Year && x.Quarter == settleReport.Quarter).FirstOrDefault();
                        planID = settleReport.PlanID;
                        Plan plan = context.Plan.Find(planID);
                        if (plan == null || plan.DepartmentID != user.DepartmentID)
                        {
                            isView = 1;
                        }
                    }
                }
                else
                {
                    report = context.FinancialReport.Find(reportID);
                    planID = report.PlanID;
                }
            }
            else
            {
                UserInfo user = SessionManager.GetUserInfo();
                planID = context.Plan.Where(x => x.PeriodID == periodID && x.DepartmentID == user.DepartmentID).Select(x => x.PlanID).FirstOrDefault();
            }
            ViewData["isView"] = isView;
            List<DisbursementModel> listDisb = context.Disbursement.Where(x => x.PlanID == planID).OrderBy(x => x.Number)
                .Select(x => new DisbursementModel
                {
                    DisbursementID = x.DisbursementID,
                    Money = x.Money,
                    PlanID = x.PlanID,
                    Number = x.Number
                }).ToList();
            ViewData["listDisb"] = listDisb;
            ViewData["planID"] = planID;
            // Thong tin giai ngan
            if (report != null)
            {
                double totalSettleMoney = new FinancialReportBusiness(context).GetTotalSettleMoney(report, FinancialReportBusiness.GET_TOTAL);
                ViewData["totalSettleMoney"] = totalSettleMoney;
            }
            return PartialView("_DisbView");
        }


        public PartialViewResult LoadDisbEdit(long id, int planID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            Plan plan = context.Plan.Find(planID);
            if (plan == null || plan.DepartmentID != user.DepartmentID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            DisbursementModel model = new DisbursementModel { PlanID = planID };
            if (id > 0)
            {
                Disbursement disb = context.Disbursement.Find(id);
                if (disb == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                model.DisbursementID = disb.DisbursementID;
                model.Money = disb.Money;
                model.SMoney = Utils.FormatMoney(disb.Money);
                model.Number = disb.Number;
                model.PlanID = disb.PlanID;
            }
            else
            {
                model.Number = context.Disbursement.Where(x => x.PlanID == planID).Select(x => x.Number).DefaultIfEmpty(0).Max() + 1;
            }
            return PartialView("_DisbAdd", model);
        }

        public JsonResult SaveDisb(DisbursementModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            Plan plan = context.Plan.Find(model.PlanID);
            if (plan == null || plan.DepartmentID != user.DepartmentID)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Disbursement disb = null;
            string msg = string.Empty;
            double money = 0;
            if (!string.IsNullOrWhiteSpace(model.SMoney))
            {
                if (!double.TryParse(model.SMoney.Trim(), out money))
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
            }
            if (model.DisbursementID > 0)
            {
                disb = context.Disbursement.Find(model.DisbursementID);
                disb.Number = model.Number;
                disb.Money = money;
                msg = "Cập nhật thông tin tạm ứng thành công";
            }
            else
            {
                disb = new Disbursement();
                disb.PlanID = model.PlanID;
                disb.Number = model.Number;
                disb.Money = money;
                context.Disbursement.Add(disb);
                msg = "Thêm thông tin tạm ứng thành công";
            }
            context.SaveChanges();
            List<Disbursement> listDisb = context.Disbursement.Where(x => x.PlanID == disb.PlanID).OrderBy(x => x.Number).ThenByDescending(x => x.DisbursementID).ToList();
            for (int i = 1; i <= listDisb.Count; i++)
            {
                Disbursement item = listDisb[i - 1];
                item.Number = i;
            }
            context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelDisb(long id)
        {
            Disbursement disb = context.Disbursement.Find(id);
            if (disb == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            List<Disbursement> listDisb = context.Disbursement.Where(x => x.PlanID == disb.PlanID && x.Number > disb.Number).ToList();
            foreach (Disbursement item in listDisb)
            {
                item.Number = item.Number - 1;
            }
            context.Disbursement.Remove(disb);
            context.SaveChanges();
            return Json("Xóa thông tin tạm ứng thành công", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}