﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReviewFinancialReportController : Controller
    {
        // GET: Action
        private Entities context = new Entities();
        public ActionResult Index()
        {
            var listPlan = (from pr in this.context.Period
                            where pr.Status == Constants.IS_ACTIVE
                            orderby pr.FromDate descending
                            select new
                            {
                                pr.PeriodID,
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listPlanItem = new List<SelectListItem>();
            listPlanItem.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            listPlan.ForEach(x =>
            {
                listPlanItem.Add(new SelectListItem { Value = x.PeriodID.ToString(), Text = x.PeriodName });
            });
            ViewData["listPlan"] = listPlanItem;
            List<SelectListItem> listYear = new List<SelectListItem>();
            listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i < maxYear; i++)
                {
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                }
            }
            ViewData["listYear"] = listYear;
            // Lay danh sach don vi quan ly
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<SelectListItem> listDept = (from m in this.context.UserManageDepartment
                                             join d in this.context.Department on m.DepartmentID equals d.DepartmentID
                                             where m.UserID == userID && d.Status == Constants.IS_ACTIVE
                                             orderby d.DepartmentName
                                             select new SelectListItem
                                             {
                                                 Value = d.DepartmentID + "",
                                                 Text = d.DepartmentName
                                             }).ToList();
            listDept.Insert(0, new SelectListItem { Value = "0", Text = "---Tất cả---" });
            ViewData["listDept"] = listDept;
            return View();
        }

        public ActionResult AddOrEdit(int? id)
        {
            FinancialReportInfoModel model = null;
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            if (id != null)
            {
                model = bus.GetFinancialReportInfo(id.Value);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            UserInfo userInfo = SessionManager.GetUserInfo();

            if (id != null)
            {
                List<TargetModel> listTarget = (from pt in this.context.PlanTarget
                                                join t in this.context.Target on pt.TargetID equals t.TargetID
                                                where pt.PlanID == model.PlanID && t.Status == Constants.IS_ACTIVE && pt.Status == Constants.IS_ACTIVE
                                                orderby t.TargetOrder
                                                select new TargetModel
                                                {
                                                    TargetID = t.TargetID,
                                                    TargetTitle = t.TargetTitle,
                                                    TargetContent = t.TargetContent
                                                }).ToList();
                ViewData["listTarget"] = listTarget;
            }
            return View("AddOrEdit", model);
        }
        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }
        public JsonResult SaveGeneralInfo(FinancialReportData data)
        {
            FinancialReport FinancialReport = null;
            if (data.FinancialReportID > 0)
            {
                FinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == data.FinancialReportID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                if (FinancialReport == null)
                {
                    throw new BusinessException("Không tồn tại báo cáo tài chính, vui lòng kiểm tra lại");
                }
                FinancialReport.PlanID = data.PlanID;
                FinancialReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    FinancialReport.Quarter = data.Quarter;
                }
                this.context.SaveChanges();
            }
            else
            {
                // Check xem bao cao da tao chua
                FinancialReport = this.context.FinancialReport.Where(x => x.PlanID == data.PlanID && x.Year == data.Year && x.Quarter == data.Quarter).FirstOrDefault();
                if (FinancialReport != null)
                {
                    throw new BusinessException("Đã tồn tại báo cáo tài chính cho thời gian và kế hoạch vừa chọn, vui lòng kiểm tra lại");
                }
                FinancialReport = new FinancialReport();
                FinancialReport.PlanID = data.PlanID;
                FinancialReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    FinancialReport.Quarter = data.Quarter;
                }
                FinancialReport.CreateDate = DateTime.Now;
                FinancialReport.Status = Constants.IS_ACTIVE;
                FinancialReport.Type = Constants.PERIOD_PLANNED;
                FinancialReport.Creator = SessionManager.GetUserInfo().UserID.ToString();
                FinancialReport.DepartmentID = SessionManager.GetUserInfo().DepartmentID;
                this.context.FinancialReport.Add(FinancialReport);
                this.context.SaveChanges();
            }
            this.context.SaveChanges();
            return Json(FinancialReport.FinancialReportID, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Approve(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(reportID, Constants.REPORT_TYPE_FINANCIAL, Constants.IS_ACTIVE);
            return Json("Phê duyệt báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Agree(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(reportID, Constants.REPORT_TYPE_FINANCIAL, Constants.IS_ACTIVE);
            return Json("Gửi báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult Unlock(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionAgree(reportID, Constants.REPORT_TYPE_FINANCIAL, Constants.IS_NOT_ACTIVE);
            return Json("Mở khóa báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult UnApprove(int reportID)
        {
            new ReportReviewBusiness(this.context).ActionApprove(reportID, Constants.REPORT_TYPE_FINANCIAL, Constants.IS_NOT_ACTIVE);
            return Json("Hủy phê duyệt báo cáo tài chính thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadActionInfo(int FinancialReportID, int targetID, int isView = 0)
        {
            FinancialReport entity = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID
            && x.Status == Constants.IS_ACTIVE && x.Type != Constants.PLAN_APPROVED).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo tài chính hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.TargetID == targetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            List<FinancialReportDetailModel> listDetail = bus.GetListAction(0, targetID, entity);
            ViewData["financialReport"] = entity;
            ViewData["listDetail"] = listDetail;
            FinancialReportDetail detail = this.context.FinancialReportDetail.Where(x => x.FinancialReportID == FinancialReportID
                    && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.FinancialReportDetailID && c.Type == Constants.COMMENT_TYPE_FINANCIAL
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.FinancialReportID;
            }
            if (isView == 0)
            {
                return PartialView("_ActionInfo");
            }
            else
            {
                return PartialView("_ActionInfoView");
            }
        }

        public PartialViewResult Search(int PlanID, int Year, int DeptID, int page = 1)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<int> listDeptID = this.context.UserManageDepartment.Where(x => x.UserID == userID)
                .Select(x => x.DepartmentID).ToList();
            IQueryable<FinancialReportInfoModel> IQSearch =
                from s in this.context.FinancialReport
                join pl in this.context.Plan on s.PlanID equals pl.PlanID
                join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                join d in this.context.Department on pl.DepartmentID equals d.DepartmentID
                where (userInfo.IsHeadOffice == Constants.IS_ACTIVE || listDeptID.Contains(pl.DepartmentID)) && pl.Status == Constants.PLAN_APPROVED
                && s.Status >= Constants.PLAN_NEXT_LEVER
                select new FinancialReportInfoModel
                {
                    CreateDate = s.CreateDate,
                    Creator = s.Creator,
                    PlanID = s.PlanID,
                    PlanName = pr.PeriodName,
                    Quarter = s.Quarter,
                    Year = s.Year,
                    FinancialReportID = s.FinancialReportID,
                    Type = s.Type,
                    Status = s.Status,
                    DeptID = d.DepartmentID,
                    DeptName = d.DepartmentName,
                    SendDate = s.SendDate
                };
            if (PlanID > 0)
            {
                IQSearch = IQSearch.Where(x => x.PlanID == PlanID);
            }
            if (Year > 0)
            {
                IQSearch = IQSearch.Where(x => x.Year == Year);
            }
            if (DeptID > 0)
            {
                IQSearch = IQSearch.Where(x => x.DeptID == DeptID);
            }

            Paginate<FinancialReportInfoModel> paginate = new Paginate<FinancialReportInfoModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = IQSearch.OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter)
                .Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = IQSearch.Count();
            ViewData["listResult"] = paginate;
            return PartialView("_ListResult");
        }

        public PartialViewResult LoadReportDetail(int planTargetActionID, int targetID, int FinancialReportID, int isView = 0)
        {
            FinancialReport entityFinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entityFinancialReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            FinancialReportBusiness bus = new FinancialReportBusiness(this.context);
            List<FinancialReportDetailModel> listDetail = bus.GetListAction(planTargetActionID, targetID, entityFinancialReport);
            ViewData["listDetail"] = listDetail;
            string postFix = string.Empty;
            if (isView == 1)
            {
                postFix = "View";
            }
            if (entityFinancialReport.Quarter != null)
            {
                return PartialView("_ListDetailQuarter" + postFix);
            }
            else
            {
                ViewData["financialReport"] = entityFinancialReport;
                return PartialView("_ListDetailYear" + postFix);
            }
        }

        public JsonResult LoadYear(int planID)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            var listPlan = (from pl in this.context.Plan
                            join pr in this.context.Period on pl.PeriodID equals pr.PeriodID
                            where pl.DepartmentID == deptID && pl.Status == Constants.IS_ACTIVE
                            && pl.Type == Constants.PLAN_APPROVED && (planID == 0 || pl.PlanID == planID)
                            orderby pr.FromDate descending
                            select new
                            {
                                pl.PlanID,
                                pr.PeriodName,
                                pr.FromDate,
                                pr.ToDate
                            }).ToList();
            List<SelectListItem> listYear = new List<SelectListItem>();
            listYear.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            if (listPlan.Any())
            {
                int minYear = listPlan.Select(x => x.FromDate.Year).Min();
                int maxYear = listPlan.Select(x => x.ToDate.Year).Max();
                for (int i = minYear; i < maxYear; i++)
                {
                    listYear.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                }
            }
            ViewData["listYear"] = listYear;
            return Json(listYear);
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, long FinancialReportID)
        {
            FinancialReport entityFinancialReport = this.context.FinancialReport.Where(x => x.FinancialReportID == FinancialReportID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
            if (entityFinancialReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID
            && x.ActionContent != null && x.ActionContent.Trim() != "")
                .Select(x => new PlanTargetActionModel
                {
                    PlanID = x.PlanID.Value,
                    PlanTargetID = x.PlanTargetID.Value,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}