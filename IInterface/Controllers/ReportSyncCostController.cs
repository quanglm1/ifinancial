﻿using ClosedXML.Excel;
using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonExcel;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class ReportSyncCostController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Bảng theo dõi báo cáo tài chính", ActionName = "Tổng hợp", AreaName = "Báo cáo")]
        public ActionResult Index()
        {
            List<Period> listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            ViewData["listPeriod"] = listPeriod;
            int periodID = 0;
            if (listPeriod.Any())
            {
                periodID = listPeriod[0].PeriodID;
                int year = DateTime.Now.Year;
                Period nowPeriod = listPeriod.Where(x => x.FromDate.Year <= year && x.ToDate.Year >= year).FirstOrDefault();
                if (nowPeriod != null)
                {
                    periodID = nowPeriod.PeriodID;
                }
            }
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listYear = bus.GetListYearByPeriod(periodID, false, true);
            ViewData["listYear"] = listYear;
            return View();
        }

        [BreadCrumb(ControllerName = "Chi phí thực tế theo nhiệm vụ", ActionName = "Tổng hợp", AreaName = "Báo cáo")]
        public ActionResult IndexSync()
        {
            List<Period> listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).ToList();
            ViewData["listPeriod"] = listPeriod;
            int periodID = 0;
            if (listPeriod.Any())
            {
                periodID = listPeriod[0].PeriodID;
                int year = DateTime.Now.Year;
                Period nowPeriod = listPeriod.Where(x => x.FromDate.Year <= year && x.ToDate.Year >= year).FirstOrDefault();
                if (nowPeriod != null)
                {
                    periodID = nowPeriod.PeriodID;
                }
            }
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listYear = bus.GetListYearByPeriod(periodID, false, true);
            ViewData["listYear"] = listYear;
            return View();
        }

        public PartialViewResult SysChartCost(int periodID, int year, int? quarter = null)
        {
            // Lay danh sach nhiem vu
            List<MissionModel> listMission = context.Mission.Where(x => x.Status == Constants.IS_ACTIVE)
                .Select(x => new MissionModel
                {
                    MissionID = x.MissionID,
                    MissionName = x.MissionName
                }).ToList();

            // Tong hop theo hoat dong
            List<MissionModel> listMissionPlanCost = (from m in context.Mission
                                                      join a in context.MainAction on m.MissionID equals a.MissionID
                                                      join pa in context.PlanTargetAction on a.MainActionID equals pa.MainActionID
                                                      join pt in context.PlanTarget on pa.PlanTargetID equals pt.PlanTargetID
                                                      join pl in context.Plan on pt.PlanID equals pl.PlanID
                                                      where pl.Type == Constants.SUB_TYPE && pl.PeriodID == periodID
                                                      && a.Status == Constants.IS_ACTIVE && pl.Status == Constants.PLAN_APPROVED
                                                      group pa by new { m.MissionID, m.MissionCode, m.MissionName, m.MissionContent } into g
                                                      orderby g.Key.MissionID
                                                      select new MissionModel
                                                      {
                                                          MissionID = g.Key.MissionID,
                                                          MissionCode = g.Key.MissionCode,
                                                          MissionContent = g.Key.MissionContent,
                                                          MissionName = g.Key.MissionName,
                                                          PlanMoney = g.Select(x => x.TotalBudget == null ? 0 : x.TotalBudget.Value).DefaultIfEmpty(0).Sum()
                                                      }).ToList();

            List<MissionModel> listMissionActualCost = (from fn in context.SettleSynthesis
                                                        join fd in context.SettleSynthesisCost on fn.SettleSynthesisID equals fd.SettleSynthesisID
                                                        join pl in context.Plan on fn.PlanID equals pl.PlanID
                                                        join pta in context.PlanTargetAction on fd.PlanTargetActionID equals pta.PlanTargetActionID
                                                        join ma in context.MainAction on pta.MainActionID equals ma.MainActionID
                                                        join m in context.Mission on ma.MissionID equals m.MissionID
                                                        where pl.PeriodID == periodID && pl.Type == Constants.SUB_TYPE && fn.Status == Constants.PLAN_APPROVED
                                                        && fd.SubPlanActionID == null && fd.SubActionSpendItemID == null && fd.ParentID == null
                                                        group fd by new { m.MissionID, m.MissionCode, m.MissionName, m.MissionContent } into g
                                                        select new MissionModel
                                                        {
                                                            MissionID = g.Key.MissionID,
                                                            MissionCode = g.Key.MissionCode,
                                                            MissionContent = g.Key.MissionContent,
                                                            MissionName = g.Key.MissionName,
                                                            ActualMoney = g.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum()
                                                        }).ToList();
            int idx = 1;
            foreach (MissionModel mission in listMission)
            {
                mission.MissionCode = "Nhiệm vụ " + idx++;
                List<MissionModel> listPlanCost = listMissionPlanCost.Where(x => x.MissionID == mission.MissionID).ToList();
                if (listPlanCost != null && listPlanCost.Any())
                {
                    mission.PlanMoney = listPlanCost.Sum(x => x.PlanMoney);
                }
                List<MissionModel> listActualCost = listMissionActualCost.Where(x => x.MissionID == mission.MissionID).ToList();
                if (listActualCost != null && listActualCost.Any())
                {
                    mission.ActualMoney = listActualCost.Sum(x => x.PlanMoney);
                }
            }
            ViewData["listMission"] = listMission;
            return PartialView("_LstResultSync");
        }

        public FileResult SynCost(int periodID, int year, int? quarter = null)
        {
            // Lay tat ca hoat dong cap 1 cua cac don vi
            List<PlanTargetActionModel> listPlanTargetAction = (from pl in context.Plan
                                                                join d in context.Department on pl.DepartmentID equals d.DepartmentID
                                                                join pr in context.Period on pl.PeriodID equals pr.PeriodID
                                                                join fn in context.SettleSynthesis on pl.PlanID equals fn.PlanID
                                                                join fd in context.SettleSynthesisCost on fn.SettleSynthesisID equals fd.SettleSynthesisID
                                                                join pta in context.PlanTargetAction on fd.PlanTargetActionID equals pta.PlanTargetActionID
                                                                join pt in context.PlanTarget on pta.PlanTargetID equals pt.PlanTargetID
                                                                join t in context.Target on pt.TargetID equals t.TargetID into tg
                                                                from tj in tg.DefaultIfEmpty()
                                                                join ma in context.MainAction on pta.MainActionID equals ma.MainActionID into mag
                                                                from maj in mag.DefaultIfEmpty()
                                                                where pr.PeriodID == periodID && pl.Type == Constants.SUB_TYPE && fn.Status == Constants.PLAN_APPROVED
                                                                && fd.SubPlanActionID == null && fd.ParentID == null && fd.SubActionSpendItemID == null
                                                                group fd by new
                                                                {
                                                                    d.DepartmentID,
                                                                    d.DepartmentType,
                                                                    d.DepartmentName,
                                                                    pt.TargetID,
                                                                    pt.PlanTargetID,
                                                                    Content = (tj != null ? tj.TargetContent : pt.PlanTargetContent)
                                                                } into g
                                                                select new PlanTargetActionModel
                                                                {
                                                                    TargetID = g.Key.TargetID,
                                                                    PlanTargetID = g.Key.PlanTargetID,
                                                                    DeptID = g.Key.DepartmentID,
                                                                    DeptType = g.Key.DepartmentType,
                                                                    DeptName = g.Key.DepartmentName,
                                                                    TargetContent = g.Key.Content,
                                                                    TotalSettleMoney = g.Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum()
                                                                }).ToList();

            List<PlanTargetModel> listTarget = listPlanTargetAction
                .Where(x => x.TargetID != null).Select(x => new
                {
                    x.TargetID,
                    x.TargetContent
                }).Distinct()
               .Select(x => new PlanTargetModel
               {
                   TargetID = x.TargetID,
                   PlanTargetContent = x.TargetContent
               }).OrderBy(x => x.TargetID).ToList();


            List<PlanTargetModel> listTargetDept = listPlanTargetAction
                .Where(x => x.TargetID == null && x.PlanTargetID != null).Select(x => new
                {
                    PlanTargetID = x.PlanTargetID.Value,
                    x.TargetContent,
                    x.DeptID,
                    x.DeptType
                }).Distinct()
               .Select(x => new PlanTargetModel
               {
                   PlanTargetID = x.PlanTargetID,
                   PlanTargetContent = x.TargetContent,
                   DepartmentID = x.DeptID,
                   DepartmentType = x.DeptType
               }).OrderBy(x => x.PlanTargetContent).ToList();

            foreach (PlanTargetModel target in listTarget)
            {
                target.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.TargetID != null && x.TargetID == target.TargetID).ToList();
            }
            foreach (PlanTargetModel target in listTargetDept)
            {
                target.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.TargetID != null && x.TargetID == target.TargetID).ToList();
            }
            string fileName = "Bang theo doi bao cao tai chinh.xlsx";
            string filePath = Server.MapPath("~/Template/") + fileName;
            using (MemoryStream ms = new MemoryStream())
            {
                using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);

                    IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                    IXVTWorksheet sheet1 = oBook.GetSheet(1);
                    IXVTWorksheet sheet2 = oBook.GetSheet(2);
                    IXVTWorksheet sheet3 = oBook.GetSheet(3);
                    this.SetExportTargetData(sheet1, 1, listTarget, listTargetDept);
                    this.SetExportTargetData(sheet2, 2, listTarget, listTargetDept);
                    this.SetExportTargetData(sheet3, 3, listTarget, listTargetDept);

                    Stream streamToWrite = oBook.ToStream();
                    byte[] bytesToWrite = new byte[streamToWrite.Length];
                    streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                    string subFix = year.ToString();
                    if (quarter != null)
                    {
                        subFix += " - quy " + quarter.Value;
                    }
                    string newFile = "Bang theo doi bao cao tai chinh nam " + subFix + ".xlsx";
                    return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                }
            }
        }

        private void SetExportTargetData(IXVTWorksheet sheet, int orderSheet, List<PlanTargetModel> listPlanTarget, List<PlanTargetModel> listPlanTargetDept)
        {
            var qDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE);
            List<PlanTargetModel> listTargetByDept;
            string sheetName = string.Empty;
            if (orderSheet == 1)
            {
                qDept = qDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_TINH_TP || x.DepartmentType == Constants.DEPT_TYPE_TP_DU_LICH);
                listTargetByDept = listPlanTargetDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_TINH_TP || x.DepartmentType == Constants.DEPT_TYPE_TP_DU_LICH).ToList();
                sheetName = "Tỉnh thành phố";
            }
            else if (orderSheet == 2)
            {
                qDept = qDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BO_NGANH);
                listTargetByDept = listPlanTargetDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BO_NGANH).ToList();
                sheetName = "Bộ ngành";
            }
            else
            {
                qDept = qDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BV_CAI_NGHIEN);
                listTargetByDept = listPlanTargetDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BV_CAI_NGHIEN).ToList();
                sheetName = "Bệnh viện";
            }
            List<Department> listDept = qDept.ToList();
            sheet.SheetName = listDept.Count + " " + sheetName;
            IXVTRange rangeHeader = sheet.GetRange(1, 3, 3, 3);
            for (int i = 0; i < listDept.Count; i++)
            {
                rangeHeader.CopyTo(sheet.GetRange(1, 3 + i, 3, 3 + i));
                sheet.SetCellValue(1, 3 + i, (i + 1).ToString());
                sheet.SetCellValue(2, 3 + i, listDept[i].DepartmentName);
            }
            int lastCol = 2 + listDept.Count;
            sheet.SetRangeFont(2, 1, 2, lastCol, "Times New Roman", 12, true, false);
            IXVTRange rangeData = sheet.GetRange(3, 1, 3, lastCol);
            int startData = 3;
            listTargetByDept.InsertRange(0, listPlanTarget);
            Dictionary<int, double> dicDeptCost = new Dictionary<int, double>();
            for (int i = 0; i < listTargetByDept.Count; i++)
            {
                int orderTarget = i + 1;
                var target = listPlanTarget[i];
                List<PlanTargetActionModel> listActionDept = target.ListPlanTargetActionModel;
                var listAction = listActionDept.Select(x => new
                {
                    x.ActionContent,
                    x.MainActionID
                }).Distinct().ToList();
                int fromData = startData;
                foreach (var action in listAction)
                {
                    rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                    sheet.SetCellValue(startData, 1, "Mục tiêu " + orderTarget + ": " + target.PlanTargetContent);
                    if (action.MainActionID != null || target.TargetID == null)
                    {
                        sheet.SetCellValue(startData, 2, action.ActionContent);
                    }
                    else
                    {
                        sheet.SetCellValue(startData, 2, "Chi phí khác");
                    }
                    for (int j = 0; j < listDept.Count; j++)
                    {
                        var dept = listDept[j];
                        var cost = listActionDept.Where(x => x.MainActionID == action.MainActionID && x.DeptID == dept.DepartmentID).Select(x => x.TotalSettleMoney.Value).DefaultIfEmpty(0).Sum();
                        if (cost > 0)
                        {
                            if (dicDeptCost.ContainsKey(dept.DepartmentID))
                            {
                                dicDeptCost[dept.DepartmentID] = dicDeptCost[dept.DepartmentID] + cost;
                            }
                            else
                            {
                                dicDeptCost[dept.DepartmentID] = cost;
                            }
                            sheet.SetCellValue(startData, 3 + j, cost.ToString());
                        }
                    }
                    startData++;
                }
                sheet.GetRange(1, fromData, 1, startData - 1).Range.Merge();
            }
            sheet.SetRangeFont(3, 2, startData - 1, lastCol, "Times New Roman", 12, false, false);
            sheet.SetCellValue(startData, 1, "Tổng cộng");
            for (int i = 0; i < listDept.Count; i++)
            {
                var dept = listDept[i];
                if (dicDeptCost.ContainsKey(dept.DepartmentID))
                {
                    sheet.SetCellValue(startData, 3 + i, dicDeptCost[dept.DepartmentID].ToString());
                }
                else
                {
                    sheet.SetCellValue(startData, 3 + i, "0");
                }
            }
            sheet.SetRangeFont(1, 1, startData, 1, "Times New Roman", 12, true, false);
            sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
            sheet.SetSheetWrapText();
        }

        private void SetExportMissionData(IXVTWorksheet sheet, List<Department> listDept, List<Mission> listMission, List<PlanTargetActionModel> listPlanTargetAction)
        {
            IXVTRange rangeHeader = sheet.GetRange(2, 3, 4, 4);
            int countDept = listDept.Count;
            for (int i = 0; i < countDept; i++)
            {
                rangeHeader.CopyTo(sheet.GetRange(2, 3 + (i * 2), 3, 4 + (i * 2)));
                sheet.SetCellValue(2, 3 + (i * 2), listDept[i].DepartmentName);
            }
            rangeHeader.CopyTo(sheet.GetRange(2, 3 + (countDept * 2), 3, 4 + (countDept * 2)));
            sheet.SetCellValue(2, 3 + (countDept * 2), "TỔNG CỘNG");
            int lastCol = 4 + (countDept * 2);
            int startData = 4;
            IXVTRange rangeData = sheet.GetRange(startData, 1, startData, lastCol);
            for (int i = 0; i < listMission.Count; i++)
            {
                int orderMission = i + 1;
                var mission = listMission[i];
                rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                sheet.SetCellValue(startData, 1, orderMission.ToString());
                sheet.SetCellValue(startData, 2, mission.MissionName);
                string sumPlanMission = string.Empty;
                string sumActualMission = string.Empty;
                string sumMission = string.Empty;
                for (int j = 0; j < listDept.Count; j++)
                {
                    var dept = listDept[j];
                    var cost = listPlanTargetAction.Where(x => x.MissionID == mission.MissionID && x.DeptID == dept.DepartmentID).FirstOrDefault();
                    if (cost != null)
                    {
                        sheet.SetCellValue(startData, 3 + (j * 2), cost.TotalBudget.GetValueOrDefault().ToString());
                        sheet.SetCellValue(startData, 4 + (j * 2), cost.TotalSettleMoney.GetValueOrDefault().ToString());
                    }
                    sumPlanMission += ", " + Utils.GetExcelColumnName(3 + (j * 2)) + startData;
                    sumActualMission += ", " + Utils.GetExcelColumnName(4 + (j * 2)) + startData;
                }
                if (!string.IsNullOrWhiteSpace(sumPlanMission))
                {
                    sumPlanMission = "SUM(" + sumPlanMission.Substring(2) + ")";
                }
                if (!string.IsNullOrWhiteSpace(sumActualMission))
                {
                    sumActualMission = "SUM(" + sumActualMission.Substring(2) + ")";
                }
                sheet.SetCellFormula(startData, 3 + (countDept * 2), sumPlanMission);
                sheet.SetCellFormula(startData, 4 + (countDept * 2), sumActualMission);
                startData++;
            }
            sheet.SetRangeFont(4, 1, startData - 1, lastCol, "Times New Roman", 12, false, false);
            sheet.SetCellValue(startData, 2, "Tổng cộng");
            for (int i = 0; i < listDept.Count + 1; i++)
            {
                string colNamePlan = Utils.GetExcelColumnName(3 + (i * 2));
                string colNameActual = Utils.GetExcelColumnName(4 + (i * 2));
                sheet.SetCellFormula(startData, 3 + (i * 2), "SUM(" + colNamePlan + "4:" + colNamePlan + (startData - 1) + ")");
                sheet.SetCellFormula(startData, 4 + (i * 2), "SUM(" + colNameActual + "4:" + colNameActual + (startData - 1) + ")");
            }
            //sheet.SetRangeFont(2, 1, startData, 1, "Times New Roman", 12, true, false);
            sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
            sheet.SetRangeBorderThin(startData, 1, startData, lastCol);
            sheet.SetSheetWrapText();
        }

        private void SetTotalMission(IXVTWorksheet sheet, int planID, List<Department> listDept, List<Mission> listMission, List<PlanTargetActionModel> listPlanTargetAction)
        {
            int startData = 4;
            int lastCol = 14;
            IXVTRange rangeData = sheet.GetRange(startData, 1, startData, lastCol);
            for (int i = 0; i < listMission.Count; i++)
            {
                int orderMission = i + 1;
                var mission = listMission[i];
                rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
                sheet.SetCellValue(startData, 1, orderMission.ToString());
                sheet.SetCellValue(startData, 2, mission.MissionName);
                string sumPlanMission = string.Empty;
                string sumActualMission = string.Empty;
                string sumMission = string.Empty;
                for (int j = 0; j < 4; j++)
                {
                    List<Department> listDeptByType = new List<Department>();
                    if (j == 0)
                    {
                        listDeptByType = listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_TINH_TP || x.DepartmentType == Constants.DEPT_TYPE_TP_DU_LICH).ToList();
                    }
                    else if (j == 1)
                    {
                        listDeptByType = listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BO_NGANH).ToList();
                    }
                    else if (j == 2)
                    {
                        listDeptByType = listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BV_CAI_NGHIEN).ToList();
                    }
                    else if (j == 3)
                    {
                        listDeptByType = listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_SANG_KIEN).ToList();
                    }
                    var listPlanTargetActionByType = listPlanTargetAction.Where(x => x.MissionID == mission.MissionID && listDeptByType.Any(d => d.DepartmentID == x.DeptID)).ToList();
                    sheet.SetCellValue(startData, 3 + (j * 2), listPlanTargetActionByType.Select(x => x.TotalBudget.GetValueOrDefault()).DefaultIfEmpty(0).Sum().ToString());
                    sheet.SetCellValue(startData, 4 + (j * 2), listPlanTargetActionByType.Select(x => x.TotalSettleMoney.GetValueOrDefault()).DefaultIfEmpty(0).Sum().ToString());
                    sumPlanMission += ", " + Utils.GetExcelColumnName(3 + (j * 2)) + startData;
                    sumActualMission += ", " + Utils.GetExcelColumnName(4 + (j * 2)) + startData;
                }
                if (!string.IsNullOrWhiteSpace(sumPlanMission))
                {
                    sumPlanMission = "SUM(" + sumPlanMission.Substring(2) + ")";
                }
                if (!string.IsNullOrWhiteSpace(sumActualMission))
                {
                    sumActualMission = "SUM(" + sumActualMission.Substring(2) + ")";
                }
                sheet.SetCellFormula(startData, 13, sumPlanMission);
                sheet.SetCellFormula(startData, 14, sumActualMission);
                startData++;
            }
            int startBold = startData;
            List<CostEstimateSpend> listCost = context.CostEstimateSpend.Where(x => x.PlanID == planID && x.ParentID == null && x.PlanTargetID == null).ToList();
            sheet.SetRangeFont(4, 1, startData - 1, lastCol, "Times New Roman", 12, false, false);
            rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
            sheet.SetCellValue(startData, 1, string.Empty);
            sheet.SetCellValue(startData, 2, "Chi phí dự phòng");
            sheet.SetCellValue(startData, 11, listCost.Where(x => x.OtherType == Constants.OTHER_TYPE_DU_PHONG).Select(x => x.TotalPrice == null ? 0 : x.TotalPrice.Value).DefaultIfEmpty(0).Sum().ToString());
            startData++;
            rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
            sheet.SetCellValue(startData, 1, string.Empty);
            sheet.SetCellValue(startData, 2, "Chi phí hành chính");
            sheet.SetCellValue(startData, 11, listCost.Where(x => x.OtherType == Constants.OTHER_TYPE_HC).Select(x => x.TotalPrice == null ? 0 : x.TotalPrice.Value).DefaultIfEmpty(0).Sum().ToString());
            startData++;
            rangeData.CopyTo(sheet.GetRange(startData, 1, startData, lastCol));
            sheet.SetCellValue(startData, 1, string.Empty);
            sheet.SetCellValue(startData, 2, "Tổng cộng");
            for (int i = 0; i < 6; i++)
            {
                string colNamePlan = Utils.GetExcelColumnName(3 + (i * 2));
                string colNameActual = Utils.GetExcelColumnName(4 + (i * 2));
                sheet.SetCellFormula(startData, 3 + (i * 2), "SUM(" + colNamePlan + "4:" + colNamePlan + (startData - 1) + ")");
                sheet.SetCellFormula(startData, 4 + (i * 2), "SUM(" + colNameActual + "4:" + colNameActual + (startData - 1) + ")");
            }
            sheet.SetRangeFont(startBold, 1, startData, 2, "Times New Roman", 12, true, false);
            sheet.SetRangeFont(startData, 1, startData, lastCol, "Times New Roman", 12, true, false);
            sheet.SetSheetWrapText();
        }

        public FileResult SynCostByMission(int periodID, int year, int? quarter = null)
        {
            // Lay Ke hoach tong
            int planID = context.Plan.Where(x => x.PeriodID == periodID && x.DepartmentID == Constants.HEAD_DEPT_ID).Select(x => x.PlanID).FirstOrDefault();
            // Lay tat ca hoat dong cap 1 cua cac don vi
            List<PlanTargetActionModel> listPlanTargetAction = (from pl in context.Plan
                                                                join pr in context.Period on pl.PeriodID equals pr.PeriodID
                                                                join pta in context.PlanTargetAction on pl.PlanID equals pta.PlanID
                                                                join ma in context.MainAction on pta.MainActionID equals ma.MainActionID
                                                                join ms in context.Mission on ma.MissionID equals ms.MissionID
                                                                join fnt in context.SettleSynthesis on pl.PlanID equals fnt.PlanID into fng
                                                                from fn in fng.DefaultIfEmpty()
                                                                join fdt in context.SettleSynthesisCost on new { fn.SettleSynthesisID, pta.PlanTargetActionID } equals new { fdt.SettleSynthesisID, PlanTargetActionID = fdt.PlanTargetActionID.Value } into fdg
                                                                from fd in fdg.DefaultIfEmpty()
                                                                where pr.PeriodID == periodID && pl.Type == Constants.SUB_TYPE && fn.Status == Constants.PLAN_APPROVED
                                                                && fd.SubPlanActionID == null && fd.ParentID == null && fd.SubActionSpendItemID == null
                                                                group new { fd, pta } by new { ms.MissionID, pl.DepartmentID } into g
                                                                select new PlanTargetActionModel
                                                                {
                                                                    MissionID = g.Key.MissionID,
                                                                    DeptID = g.Key.DepartmentID,
                                                                    TotalBudget = g.Select(x => x.pta.TotalBudget == null ? 0 : x.pta.TotalBudget.Value).Sum(),
                                                                    TotalSettleMoney = g.Select(x => x.fd.SetttleMoney).DefaultIfEmpty(0).Sum()
                                                                }).ToList();

            List<Department> listDept = context.Department.Where(x => x.Status == Constants.IS_ACTIVE && x.DepartmentType != Constants.DEPT_TYPE_PBQUY).ToList();

            List<Mission> listMission = context.Mission.Where(x => x.Status == Constants.IS_ACTIVE).ToList();

            string fileName = "BCTC theo 9nv.xlsx";
            string filePath = Server.MapPath("~/Template/") + fileName;
            using (MemoryStream ms = new MemoryStream())
            {
                using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);

                    IXVTWorkbook oBook = XVTExcel.OpenWorkbook(ms);
                    IXVTWorksheet sheet1 = oBook.GetSheet(1);
                    IXVTWorksheet sheet2 = oBook.GetSheet(2);
                    IXVTWorksheet sheet3 = oBook.GetSheet(3);
                    IXVTWorksheet sheet4 = oBook.GetSheet(4);
                    this.SetExportMissionData(sheet1, listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_TINH_TP || x.DepartmentType == Constants.DEPT_TYPE_TP_DU_LICH).OrderBy(x => x.DepartmentName).ToList(),
                        listMission, listPlanTargetAction);
                    this.SetExportMissionData(sheet2, listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BO_NGANH).OrderBy(x => x.DepartmentName).ToList(),
                        listMission, listPlanTargetAction);
                    this.SetExportMissionData(sheet3, listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_BV_CAI_NGHIEN).OrderBy(x => x.DepartmentName).ToList(),
                        listMission, listPlanTargetAction);
                    this.SetExportMissionData(sheet4, listDept.Where(x => x.DepartmentType == Constants.DEPT_TYPE_SANG_KIEN).OrderBy(x => x.DepartmentName).ToList(),
                        listMission, listPlanTargetAction);
                    // Phan tong hop
                    IXVTWorksheet sheet5 = oBook.GetSheet(5);
                    this.SetTotalMission(sheet5, planID, listDept, listMission, listPlanTargetAction);

                    Stream streamToWrite = oBook.ToStream();
                    byte[] bytesToWrite = new byte[streamToWrite.Length];
                    streamToWrite.Read(bytesToWrite, 0, bytesToWrite.Length);

                    string subFix = year.ToString();
                    if (quarter != null)
                    {
                        subFix += " - quy " + quarter.Value;
                    }
                    string newFile = "BCTC theo 9nv " + subFix + ".xlsx";
                    return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}