﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public class CostEstimateController : Controller
    {
        // GET: Action
        private Entities context = new Entities();

        [BreadCrumb(ControllerName = "Dự toán quỹ", ActionName = "Dự toán quỹ", AreaName = "Kế hoạch")]
        public ActionResult Index(int periodId, int isView = 0)
        {
            Period period = context.Period.Where(x => x.PeriodID == periodId).FirstOrDefault();
            if (period != null)
            {
                ViewData["Period"] = period;

                Plan plan = context.Plan.Where(x => x.PeriodID == periodId && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
                ViewData["Plan"] = plan;
                if (plan != null)
                {
                    CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
                    if (isView == 0)
                    {
                        bus.Init(plan.PlanID);
                        context.SaveChanges();
                    }
                    List<PlanTargetModel> lstPlanTarget = bus.GetListPlanTargetCost(plan.PlanID);
                    // Lay them thong tin ve chi phi tong hop tu don vi
                    ViewData["PlanTarget"] = lstPlanTarget;
                    List<CostEstimateSpendModel> listCostSpend = bus.GetListRootSpend(plan.PlanID);
                    ViewData["listSpend "] = listCostSpend;
                }
                else
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
            }
            return View();
        }

        public PartialViewResult SyncCostEstimate(int periodID)
        {
            Period period = context.Period.Where(x => x.PeriodID == periodID).FirstOrDefault();
            if (period != null)
            {
                ViewData["Period"] = period;

                Plan plan = context.Plan.Where(x => x.PeriodID == periodID && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
                if (plan != null)
                {
                    CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
                    List<PlanTargetModel> lstPlanTarget = bus.GetListPlanTargetCost(plan.PlanID, true);
                    // Lay them thong tin ve chi phi tong hop tu don vi
                    ViewData["PlanTarget"] = lstPlanTarget;
                    List<CostEstimateSpendModel> listCostSpend = bus.GetListRootSpend(plan.PlanID);
                    ViewData["listSpend "] = listCostSpend;
                }
                else
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
            }
            else
            {
                throw new BusinessException("Không tồn tại giai đoạn, vui lòng kiểm tra lại");
            }
            return PartialView("_ListEstimateCost");
        }

        public JsonResult DelSpendItem(int id)
        {
            CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
            bus.DelSpendCost(id);
            return Json(new { Type = "SUCCESS", Message = "Xóa hạng mục chi thành công" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCostEstimate(List<string> TotalPrice, List<long> SubCostID, List<string> ArrDesc, int periodId)
        {
            Plan plan = this.context.Plan.Where(x => x.PeriodID == periodId && x.Status != Constants.IS_NOT_ACTIVE && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch chưa được lập, vui lòng kiểm tra lại");
            }
            CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
            bus.SaveCostEstimate(TotalPrice, SubCostID, ArrDesc, plan.PlanID);
            context.SaveChanges();
            return Json(new { Type = "SUCCESS", Message = "Cập nhật thông tin dự toán quỹ thành công" });

        }

        public PartialViewResult AddSpendItem(int planID, int? id, string type, string[] spendCodes, string[] spendContents)
        {
            Plan plan = context.Plan.Find(planID);
            if (plan == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch đã chọn");
            }
            Period period = context.Period.Find(plan.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            int userID = SessionManager.GetUserInfo().UserID;
            int? planTargetID = null;
            int? planTargetActionID = null;
            int? subActionPlanID = null;
            int? parentID = null;
            int? MainActionID = null;
            if ("targetaction".Equals(type))
            {
                PlanTargetAction pta = this.context.PlanTargetAction.Find(id);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                planTargetID = pta.PlanTargetID.Value;
                MainActionID = pta.MainActionID.Value;
            }
            else if ("subaction".Equals(type))
            {
                var subAction = (from s in this.context.SubActionPlan
                                 join p in this.context.PlanTargetAction on s.PlanTargetActionID equals p.PlanTargetActionID
                                 select new
                                 {
                                     p.PlanTargetID,
                                     p.PlanTargetActionID,
                                     s.SubActionPlanID,
                                     s.SubActionID
                                 }).FirstOrDefault();
                if (subAction == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = subAction.PlanTargetActionID;
                subActionPlanID = subAction.SubActionPlanID;
                planTargetID = subAction.PlanTargetID.Value;
                MainActionID = subAction.SubActionID.Value;
            }
            else if ("subcost".Equals(type))
            {
                CostEstimateSpend subItem = this.context.CostEstimateSpend.Find(id);
                if (subItem == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                PlanTargetAction pta = this.context.PlanTargetAction.Find(subItem.PlanTargetActionID);
                if (pta == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                planTargetActionID = pta.PlanTargetActionID;
                subActionPlanID = subItem.SubActionPlanID;
                planTargetID = pta.PlanTargetID.Value;
                MainActionID = subItem.MainActionID;
                parentID = id;
            }
            // Luu muc chi cao nhat lam cha
            int startIdx = 0;
            List<CostEstimateSpend> listSubSpendItem = new List<CostEstimateSpend>();
            List<CostEstimateSpendModel> listModel = new List<CostEstimateSpendModel>();
            for (int i = startIdx; i < spendContents.Length; i++)
            {
                CostEstimateSpend spendItem = new CostEstimateSpend();
                spendItem.PlanTargetID = planTargetID;
                spendItem.ParentID = parentID;
                spendItem.PlanTargetActionID = planTargetActionID;
                spendItem.SpendContent = spendContents[i];
                spendItem.SpendItemTitle = spendCodes[i];
                spendItem.SubActionPlanID = subActionPlanID;
                spendItem.MainActionID = MainActionID;
                spendItem.PlanID = planID;
                this.context.CostEstimateSpend.Add(spendItem);
                listSubSpendItem.Add(spendItem);
            }
            context.SaveChanges();
            // Insert phan cost
            int fromYear = period.FromDate.Year;
            int toYear = period.ToDate.Year;
            List<CostEstimateDetail> listCost = new List<CostEstimateDetail>();
            for (int year = fromYear; year <= toYear; year++)
            {
                foreach (CostEstimateSpend item in listSubSpendItem)
                {
                    CostEstimateDetail cost = new CostEstimateDetail();
                    cost.PlanTargetActionID = planTargetActionID;
                    cost.PlanTargetID = planTargetID.Value;
                    cost.PlanYear = year;
                    cost.SubActionPlanID = subActionPlanID;
                    cost.MainActionID = item.MainActionID;
                    cost.CostEstimateSpendID = item.CostEstimateSpendID;
                    cost.PlanID = planID;
                    this.context.CostEstimateDetail.Add(cost);
                    listCost.Add(cost);
                }
            }
            context.SaveChanges();
            for (int i = 0; i < listSubSpendItem.Count; i++)
            {
                CostEstimateSpendModel model = new CostEstimateSpendModel();
                model.CostEstimateSpendID = listSubSpendItem[i].CostEstimateSpendID;
                model.SpendContent = listSubSpendItem[i].SpendContent;
                model.SpendItemTitle = listSubSpendItem[i].SpendItemTitle;
                model.SubActionPlanID = subActionPlanID;
                model.PlanTargetActionID = planTargetActionID;
                model.PlanTargetID = planTargetID;
                model.MainActionID = listSubSpendItem[i].MainActionID;
                model.ListDetail = listCost.Where(x => x.CostEstimateSpendID == model.CostEstimateSpendID)
                    .Select(x => new CostEstimateDetailModel
                    {
                        CostEstimateDetailID = x.CostEstimateDetailID,
                        CostEstimateSpendID = x.CostEstimateSpendID,
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetActionID = x.PlanTargetActionID,
                        SubActionPlanID = x.SubActionPlanID,
                        PlanYear = x.PlanYear,
                        TotalMoney = x.TotalMoney,
                        Description = x.Description,
                        MainActionID = x.MainActionID
                    }).ToList();
                listModel.Add(model);
            }
            ViewData["Period"] = period;
            ViewData["RangeYear"] = toYear - fromYear;
            ViewData["listSubSpendItem"] = listModel;
            return PartialView("_ListSubCostInsert");
        }

        public JsonResult SyncSalary(int id)
        {
            CostEstimateSpend spend = this.context.CostEstimateSpend.Find(id);
            if (spend == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            Plan plan = context.Plan.Find(spend.PlanID);
            Period period = context.Period.Find(plan.PeriodID);
            int startYear = period.FromDate.Year;
            int endYear = period.ToDate.Year;
            List<double> listCost = new List<double>();
            for (int year = startYear; year <= endYear; year++)
            {
                double cost = 0;
                if (spend.IsSalary.GetValueOrDefault() == Constants.IS_ACTIVE)
                {
                    if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_CHINH_TYPE)
                    {
                        cost = context.PaySheet.Where(x => x.Year == year && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                    }
                    else if (spend.SalaryType.GetValueOrDefault() == Constants.QUY_SPEND_HC_LUONG_PHU_CAP_TYPE)
                    {
                        cost = context.Allowance.Where(x => x.Year == year && x.TotalPerYear != null).Select(x => x.TotalPerYear.Value).DefaultIfEmpty(0).Sum();
                    }
                }
                listCost.Add(cost);
            }
            return Json(listCost, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportCostReport(int periodID)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            Period period = context.Period.Where(x => x.PeriodID == periodID).FirstOrDefault();
            if (period != null)
            {
                Plan plan = context.Plan.Where(x => x.PeriodID == periodID && x.DepartmentID == Constants.HEAD_DEPT_ID).FirstOrDefault();
                if (plan == null)
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
                if (plan != null)
                {
                    CostEstimateBusiness bus = new CostEstimateBusiness(this.context);
                    string fileName = "Du_Toan_Quy.xlsx";
                    string filePath = Server.MapPath("~/Template/") + fileName;

                    byte[] bytesToWrite = bus.SetExportData(period, plan, filePath);
                    string newFile = "Du_Toan_Kinh_Phi_" + period.FromDate.Year + "_" + period.ToDate.Year + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    return File(bytesToWrite.ToArray(), MimeMapping.GetMimeMapping(fileName), newFile);

                }
                else
                {
                    throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
                }
            }
            else
            {
                throw new BusinessException("Không tồn tại kế hoạch, vui lòng kiểm tra lại");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}