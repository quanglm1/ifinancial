﻿using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace IInterface.Controllers
{
    public class FlowProcessController : Controller
    {
        private Entities context = new Entities();
        // GET: FlowProcess
        [BreadCrumb(ControllerName = "Luồng dữ liệu", ActionName = "Luồng dữ liệu", AreaName = "Hệ thống")]
        public ActionResult Index()
        {
            var listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).OrderByDescending(x => x.ToDate)
                .Select(x => new SelectListItem
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID.ToString()
                }).ToList();
            ViewData["listPeriod"] = listPeriod;
            return View();
        }

        public JsonResult Save(FlowProcessModel FlowProcess)
        {
            FlowProcess entity = null;
            string msg;
            if (FlowProcess.FlowProcessID > 0)
            {
                entity = this.context.FlowProcess.Where(x => x.FlowProcessID == FlowProcess.FlowProcessID).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Thông tin không tồn tại trong hệ thống");
                }
                FlowProcess.UpdateEntity(entity);
                msg = "Cập nhật thông tin thành công";
            }
            else
            {
                entity = FlowProcess.ToEntity();
                context.FlowProcess.Add(entity);
                msg = "Thêm mới thông tin thành công";
            }
            context.SaveChanges();
            List<FlowProcess> listFlow = this.context.FlowProcess.Where(x => x.ObjectType == FlowProcess.ObjectType).OrderBy(x => x.Step).ToList();
            for (int i = 0; i < listFlow.Count; i++)
            {
                listFlow[i].Step = i + 1;
            }
            context.SaveChanges();
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult OpenAddOrEditFlowProcess(int? FlowProcessId)
        {
            FlowProcessModel model = null;
            List<SelectListItem> listProcess = null;
            if (FlowProcessId.HasValue && FlowProcessId.Value > 0)
            {
                FlowProcess entity = this.context.FlowProcess.Where(x => x.FlowProcessID == FlowProcessId).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Thông tin không tồn tại trong hệ thống");
                }
                model = new FlowProcessModel();
                model.EntityTo(entity);
                if (entity.ProcessorType == 1)
                {
                    listProcess = this.context.Role.Where(x => x.Status == Constants.IS_ACTIVE)
                        .Select(x => new SelectListItem
                        {
                            Text = x.RoleName,
                            Value = x.RoleID.ToString()
                        }).ToList();
                }
                else
                {
                    List<Department> listDept = this.GetListDepartment();
                    listProcess = listDept
                        .Select(x => new SelectListItem
                        {
                            Text = x.DepartmentName,
                            Value = x.DepartmentID.ToString()
                        }).ToList();
                }
            }
            else
            {
                listProcess = this.context.Role.Where(x => x.Status == Constants.IS_ACTIVE)
                .Select(x => new SelectListItem
                {
                    Text = x.RoleName,
                    Value = x.RoleID.ToString()
                }).ToList();
            }
            ViewData["listProcess"] = listProcess;
            var listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE).OrderByDescending(x => x.ToDate)
                .Select(x => new SelectListItem
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID.ToString()
                }).ToList();
            ViewData["listPeriod"] = listPeriod;
            return PartialView("_AddOrEdit", model);
        }

        public JsonResult LoadObect(int ProcessorType)
        {
            List<SelectListItem> listProcess = null;
            if (ProcessorType == 1)
            {
                listProcess = this.context.Role.Where(x => x.Status == Constants.IS_ACTIVE)
                    .Select(x => new SelectListItem
                    {
                        Text = x.RoleName,
                        Value = x.RoleID.ToString()
                    }).ToList();
            }
            else
            {
                List<Department> listDept = this.GetListDepartment();
                listProcess = listDept
                    .Select(x => new SelectListItem
                    {
                        Text = x.DepartmentName,
                        Value = x.DepartmentID.ToString()
                    }).ToList();
            }
            return Json(listProcess);
        }


        public JsonResult DeleteFlowProcess(int id)
        {
            FlowProcess entity = context.FlowProcess.Where(x => x.FlowProcessID == id).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Thông tin không tồn tại trong hệ thống");
            }
            context.FlowProcess.Remove(entity);
            context.SaveChanges();
            return Json("Xóa Thông tin thành công", JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult SearchFlowProcess(int objectType, int periodId)
        {
            ViewData["objectType"] = objectType;
            List<FlowProcessModel> listFlowProcess = (from x in this.context.FlowProcess
                                                      join r in this.context.Role on x.ProcessorID equals r.RoleID
                                                      where r.Status == Constants.IS_ACTIVE
                                                      && x.ObjectType == objectType && x.ProcessorType == 1 && x.PeriodID == periodId
                                                      orderby x.Step
                                                      select new FlowProcessModel
                                                      {
                                                          FlowProcessID = x.FlowProcessID,
                                                          IsHeadOffice = x.IsHeadOffice,
                                                          ObjectType = x.ObjectType,
                                                          PLNumber = x.PLNumber,
                                                          ProcessorID = x.ProcessorID,
                                                          ProcessorName = r.RoleName,
                                                          ProcessorType = x.ProcessorType,
                                                          Step = x.Step,
                                                          WorkType = x.WorkType
                                                      }).ToList();

            listFlowProcess.AddRange((from x in this.context.FlowProcess
                                      join d in this.context.Department on x.ProcessorID equals d.DepartmentID
                                      where d.Status == Constants.IS_ACTIVE
                                      && x.ObjectType == objectType && x.ProcessorType == 2 && x.PeriodID == periodId
                                      orderby x.Step
                                      select new FlowProcessModel
                                      {
                                          FlowProcessID = x.FlowProcessID,
                                          IsHeadOffice = x.IsHeadOffice,
                                          ObjectType = x.ObjectType,
                                          PLNumber = x.PLNumber,
                                          ProcessorID = x.ProcessorID,
                                          ProcessorName = d.DepartmentName,
                                          ProcessorType = x.ProcessorType,
                                          Step = x.Step,
                                          WorkType = x.WorkType
                                      }).ToList());

            ViewData["listFlowProcess"] = listFlowProcess.OrderBy(x => x.Step).ToList();
            return PartialView("_LstFlow");
        }

        private List<Department> GetListDepartment()
        {
            List<Department> listDepartment = this.context.Department.Where(x => x.Status == Constants.IS_ACTIVE
            && x.DepartmentType == Constants.DEPT_TYPE_PBQUY).OrderBy(x => x.ParentID).ThenBy(x => x.DepartmentName).ToList();
            return listDepartment;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}