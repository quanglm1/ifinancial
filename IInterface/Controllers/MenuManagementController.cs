﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class MenuManagementController : Controller
    {
        // GET: MenuManagement
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Chức năng", ActionName = "Chức năng", AreaName = "Danh mục")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Search(string MissionCodeSearch = null, string MissionNameSearch = null, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            List<MenuModel> listMenu = _context.Menu.Where(x => x.ParentID == null)
               .Select(x => new MenuModel
               {
                   MenuID = x.MenuID,
                   MenuName = x.MenuName,
                   IconClass = x.IconClass,
                   OrderNumber = x.OrderNumber
               }).ToList();
            // Lay danh sach menu con
            List<int> listMenuId = listMenu.Select(x => x.MenuID).ToList();
            List<Menu> listChildMenu = _context.Menu.Where(x => listMenuId.Contains(x.ParentID.Value)).ToList();
            listMenu.ForEach(x =>
            {
                x.ListChildMenu = listChildMenu.Where(c => c.ParentID.Value == x.MenuID)
                .Select(c => new MenuModel
                {
                    MenuID = c.MenuID,
                    MenuName = c.MenuName,
                    ActionPath = c.ActionPath,
                    OrderNumber = x.OrderNumber,
                    IconClass = c.IconClass
                }).OrderBy(o=>o.MenuName).ToList();
            });

            ViewData["listMenu"] = listMenu;

            return PartialView("_ListResult");
        }
        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            MenuModel model = new MenuModel();
            if (id == -1)
            {
                model.Type = -1;
            }

            if (id.HasValue && id.Value > 0)
            {
                var x = this._context.Menu.Find(id);

                if (x != null)
                {
                    if (x.ParentID == null)
                    {
                        model.Type = 2;
                        model.ParentID = x.MenuID;
                        model.ParentName = x.MenuName;
                        model.OrderNumber = x.OrderNumber;
                    }
                    else
                    {
                        model.ActionPath = x.ActionPath;
                        model.MenuID = x.MenuID;
                        model.IconClass = x.IconClass;
                        model.MenuName = x.MenuName;
                        model.Status = x.Status;
                        model.ParentID = x.ParentID;
                        model.OrderNumber = x.OrderNumber;
                        model.Type = 3;
                    }
                }
                else
                {
                    throw new BusinessException("Không tồn tại chức năng đã chọn");
                }
            }

            ViewBag.LstStatus = GetStatusLst();
            ViewBag.LstParent = GetLstParent();
            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(MenuModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            string msg;
            if (ModelState.IsValid)
            {
                if (model.MenuID > 0)
                {
                    Menu entity = this._context.Menu.Where(x => x.MenuID == model.MenuID && x.Status == Constants.IS_ACTIVE).FirstOrDefault();
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại nhiệm vụ đã chọn");
                    }

                    entity.MenuName = model.MenuName;
                    entity.IconClass = model.IconClass;
                    entity.ActionPath = model.ActionPath;
                    entity.Status = model.Status;
                    entity.ParentID = model.ParentID;
                    entity.OrderNumber = model.OrderNumber;
                    _context.SaveChanges();

                    msg = "Cập nhật thành công";
                }
                else
                {
                    Menu entity = new Menu();
                    entity.MenuName = model.MenuName;
                    entity.IconClass = model.IconClass;
                    entity.ActionPath = model.ActionPath;
                    entity.Status = model.Status;
                    entity.ParentID = model.ParentID;
                    entity.OrderNumber = model.OrderNumber;
                    entity.Status = Constants.IS_ACTIVE;
                    this._context.Menu.Add(entity);
                    _context.SaveChanges();
                    msg = "Thêm mới thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            Menu obj = _context.Menu.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại nhiệm vụ đã chọn");
            }

            _context.Menu.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa nhiệm vụ thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        private IEnumerable<SelectListItem> GetStatusLst()
        {
            List<SelectListItem> res = new List<SelectListItem>();
            res.Add(new SelectListItem { Text = "Hiệu lực", Value = "1" });
            res.Add(new SelectListItem { Text = "Hết hiệu lực", Value = "0" });

            return res;
        }

        private IEnumerable<SelectListItem> GetLstParent()
        {
            List<SelectListItem> res = new List<SelectListItem>();
            var lst = _context.Menu.Where(o => o.ParentID == null && o.Status
            == Constants.IS_ACTIVE);

            if (lst.Any())
            {
                foreach (var item in lst.ToList())
                {
                    res.Add(new SelectListItem { Text = item.MenuName, Value = item.MenuID.ToString() });
                }
            }

            return res;
        }
    }
}