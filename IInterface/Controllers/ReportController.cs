﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class ReportController : Controller
    {
        protected Entities context = new Entities();

        public ActionResult BaseIndexDept()
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listPeriod = bus.GetListPeriod(deptID, false);
            List<SelectListItem> listYear;
            if (listPeriod.Any())
            {
                SelectListItem seletedItem = listPeriod.Where(x => x.Selected).OrderBy(x => x.Value).FirstOrDefault();
                if (seletedItem == null)
                {
                    seletedItem = listPeriod[0];
                }
                listYear = bus.GetListYearByPeriod(int.Parse(seletedItem.Value), true, true);
            }
            else
            {
                listYear = new List<SelectListItem>();
            }
            ViewData["listPeriod"] = listPeriod;
            ViewData["listYear"] = listYear;
            return View();
        }

        public ActionResult BaseIndexReview()
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listPeriod = bus.GetListPeriod(isAll: false);
            List<SelectListItem> listYear;
            if (listPeriod.Any())
            {
                SelectListItem seletedItem = listPeriod.Where(x => x.Selected).FirstOrDefault();
                if (seletedItem == null)
                {
                    seletedItem = listPeriod[0];
                }
                listYear = bus.GetListYearByPeriod(int.Parse(seletedItem.Value), true, true);
            }
            else
            {
                listYear = new List<SelectListItem>();
            }
            int userID = userInfo.UserID;
            // Lay danh sach don vi phan cong
            List<SelectListItem> listDept = (from m in this.context.UserManageDepartment
                                             join d in this.context.Department on m.DepartmentID equals d.DepartmentID
                                             where m.UserID == userID && d.Status == Constants.IS_ACTIVE
                                             orderby d.DepartmentName
                                             select new SelectListItem
                                             {
                                                 Value = d.DepartmentID + "",
                                                 Text = d.DepartmentName
                                             }).ToList();
            if (!listDept.Any())
            {
                listDept = (from d in this.context.Department
                            where d.Status == Constants.IS_ACTIVE
                            orderby d.DepartmentName
                            select new SelectListItem
                            {
                                Value = d.DepartmentID + "",
                                Text = d.DepartmentName
                            }).ToList();
            }
            listDept.Insert(0, new SelectListItem { Value = "0", Text = "---Tất cả---" });
            List<SelectListItem> listQuarter = new List<SelectListItem>();
            listQuarter.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            listQuarter.Add(new SelectListItem { Value = "1", Text = "Quý 1" });
            listQuarter.Add(new SelectListItem { Value = "2", Text = "Quý 2" });
            listQuarter.Add(new SelectListItem { Value = "3", Text = "Quý 3" });
            listQuarter.Add(new SelectListItem { Value = "4", Text = "Quý 4" });
            List<SelectListItem> listStatus = new List<SelectListItem>();
            listStatus.Add(new SelectListItem { Value = "0", Text = "---Tất cả---" });
            listStatus.Add(new SelectListItem { Value = Constants.REPORT_NEW.ToString(), Text = "Chưa gửi báo cáo" });
            listStatus.Add(new SelectListItem { Value = Constants.REPORT_SENT.ToString(), Text = "Đã gửi báo cáo" });
            ViewData["listStatus"] = listStatus;
            ViewData["listQuarter"] = listQuarter;
            ViewData["listDept"] = listDept;
            ViewData["listPeriod"] = listPeriod;
            ViewData["listYear"] = listYear;
            ViewData["isReview"] = Constants.IS_ACTIVE;
            return View();
        }

        public JsonResult LoadYear(int PeriodID)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listYear = bus.GetListYearByPeriod(PeriodID);
            return Json(listYear);
        }

        public double SetAddOrEditData(int? reportID, Plan plan, int year, int isReview = 0, bool hasChoice = true)
        {
            UserInfo userInfo = SessionManager.GetUserInfo();
            int deptID = userInfo.DepartmentID.Value;
            if (isReview == Constants.IS_ACTIVE)
            {
                deptID = 0;
            }
            ReportReviewBusiness bus = new ReportReviewBusiness(this.context);
            List<SelectListItem> listPeriod = bus.GetListPeriod(deptID, false);
            ViewData["listPeriod"] = listPeriod;
            List<SelectListItem> listYear = new List<SelectListItem>();
            if (plan != null)
            {
                listYear = bus.GetListYearByPeriod(plan.PeriodID, false);
            }
            else if (listPeriod.Any())
            {
                listYear = bus.GetListYearByPeriod(int.Parse(listPeriod.First().Value), false);
            }
            ViewData["listYear"] = listYear;
            List<SelectListItem> listQuarter = new List<SelectListItem>();
            if (hasChoice)
            {
                listQuarter.Add(new SelectListItem { Value = "0", Text = "---Lựa chọn---" });
            }
            listQuarter.Add(new SelectListItem { Value = "1", Text = "Quý 1" });
            listQuarter.Add(new SelectListItem { Value = "2", Text = "Quý 2" });
            listQuarter.Add(new SelectListItem { Value = "3", Text = "Quý 3" });
            listQuarter.Add(new SelectListItem { Value = "4", Text = "Quý 4" });
            ViewData["listQuarter"] = listQuarter;
            double planMoney = 0;
            if (reportID != null && plan != null)
            {
                List<PlanTargetModel> listTarget = (from pt in this.context.PlanTarget
                                                    where pt.PlanID == plan.PlanID
                                                    orderby pt.TargetOrder
                                                    select new PlanTargetModel
                                                    {
                                                        PlanTargetID = pt.PlanTargetID,
                                                        PlanTargetTitle = pt.PlanTargetTitle,
                                                        TargetCode = pt.TargetCode,
                                                        PlanTargetContent = pt.PlanTargetContent,
                                                        TotalBudget = pt.TotalBuget
                                                    }).ToList();

                planMoney = (from sc in this.context.SubActionPlanCost
                             join pt in this.context.PlanTarget on sc.PlanTargetID equals pt.PlanTargetID
                             where pt.PlanID == plan.PlanID && sc.PlanYear == year && sc.PlanTargetActionID == null
                             select sc.TotalPrice == null ? 0 : sc.TotalPrice.Value
                                  ).DefaultIfEmpty(0).Sum();
                ViewData["listTarget"] = listTarget;
            }
            if (plan != null && plan.DepartmentID != userInfo.DepartmentID)
            {
                ViewData["isReview"] = Constants.IS_ACTIVE;
            }
            else
            {
                ViewData["isReview"] = Constants.IS_NOT_ACTIVE;
            }
            return planMoney;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}