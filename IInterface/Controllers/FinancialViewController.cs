﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class FinancialViewController : Controller
    {
        // GET: Action
        private readonly Entities context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Chi phí", AreaName = "Thống kê")]
        public ActionResult Index()
        {
            var listPeriod = context.Period.Where(x => x.Status == Constants.IS_ACTIVE)
                .OrderBy(x => x.FromDate).Select(x => new
                {
                    Text = x.PeriodName,
                    Value = x.PeriodID + "",
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Selected = false
                }).ToList();
            int yearNow = DateTime.Now.Year;
            List<SelectListItem> listSelect = listPeriod.Select(x => new SelectListItem
            {
                Text = x.Text,
                Value = x.Value
            }).ToList();
            foreach (var period in listPeriod)
            {
                if (period.FromDate.Year <= yearNow && period.ToDate.Year >= yearNow)
                {
                    SelectListItem item = listSelect.Where(x => x.Value.Equals(period.Value)).FirstOrDefault();
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
            }

            ViewData["listPeriod"] = listSelect;
            return View();
        }

        public PartialViewResult ViewFinancialNow(int PeriodID)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            Plan plan = context.Plan.Where(x => x.PeriodID == PeriodID && x.DepartmentID == objUser.DepartmentID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (plan != null)
            {

                FinancialReport lastReport = context.FinancialReport.Where(x => x.PlanID == plan.PlanID)
                    .OrderByDescending(x => x.Year).ThenByDescending(x => x.Quarter == null ? 100 : x.Quarter.Value).FirstOrDefault();
                int year = 0;
                int? quarter = 0;
                if (lastReport != null)
                {
                    year = lastReport.Year;
                    quarter = lastReport.Quarter.GetValueOrDefault() > 0 ? lastReport.Quarter.GetValueOrDefault() : 4;
                }
                var nowSettle = context.SettleSynthesis.Where(x => x.PlanID == plan.PlanID && x.Year == year && x.Quarter == quarter).FirstOrDefault();
                List<SettleSynthesisCost> listSetCost;
                if (nowSettle != null)
                {
                    listSetCost = (from a in this.context.SettleSynthesisCost
                                   join r in this.context.SettleSynthesis on a.SettleSynthesisID equals r.SettleSynthesisID
                                   where r.PlanID == plan.PlanID
                                   && a.SubActionSpendItemID == null && a.ParentID == null
                                   && ((r.Year < nowSettle.Year)
                                   || (r.Year == nowSettle.Year && r.Quarter <= quarter))
                                   select a).ToList();
                }
                else
                {
                    listSetCost = new List<SettleSynthesisCost>();
                }
                List<PlanTargetModel> listPlanTarget = context.PlanTarget.Where(x => x.PlanID == plan.PlanID).OrderBy(x => x.PlanTargetID)
                    .Select(x => new PlanTargetModel
                    {
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetTitle = x.PlanTargetTitle,
                        TargetCode = x.TargetCode,
                        PlanTargetContent = x.PlanTargetContent,
                        TotalBudget = x.TotalBuget
                    }).ToList();

                List<PlanTargetActionModel> listPlanTargetAction = context.PlanTargetAction.Where(x => context.PlanTarget.Any(t => t.PlanID == plan.PlanID && t.PlanTargetID == x.PlanTargetID))
                    .OrderBy(x => x.PlanTargetActionID)
                    .Select(x => new PlanTargetActionModel
                    {
                        PlanTargetID = x.PlanTargetID,
                        PlanTargetActionID = x.PlanTargetActionID,
                        ActionCode = x.ActionCode,
                        ActionContent = x.ActionContent,
                        TotalBudget = x.TotalBudget
                    }).ToList();

                List<SubActionPlanModel> listSubAction = context.SubActionPlan.Where(x => x.PlanID == plan.PlanID && x.ParentID == null)
                    .OrderBy(x => x.SubActionPlanID)
                    .Select(x => new SubActionPlanModel
                    {
                        SubActionPlanID = x.SubActionPlanID,
                        PlanTargetActionID = x.PlanTargetActionID,
                        SubActionCode = x.SubActionCode,
                        SubActionContent = x.SubActionContent,
                        TotalBudget = x.TotalBudget
                    }).ToList();
                foreach (SubActionPlanModel sub in listSubAction)
                {
                    sub.TotalSettleMoney = listSetCost.Where(x => x.SubPlanActionID == sub.SubActionPlanID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                }
                foreach (PlanTargetActionModel pta in listPlanTargetAction)
                {
                    pta.ListSubActionPlan = listSubAction.Where(x => x.PlanTargetActionID == pta.PlanTargetActionID).ToList();
                    pta.TotalSettleMoney = listSetCost.Where(x => x.SubPlanActionID == null && x.PlanTargetActionID == pta.PlanTargetActionID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                }
                foreach (PlanTargetModel pt in listPlanTarget)
                {
                    pt.ListPlanTargetActionModel = listPlanTargetAction.Where(x => x.PlanTargetID == pt.PlanTargetID).ToList();
                    pt.TotalSettleMoney = listSetCost.Where(x => x.PlanTargetActionID == null && x.PlanTargetID == pt.PlanTargetID).Select(x => x.SetttleMoney).DefaultIfEmpty(0).Sum();
                }
                ViewData["listPlanTarget"] = listPlanTarget;
            }
            return PartialView("_ListDetailCost");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}