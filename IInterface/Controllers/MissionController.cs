﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class MissionController : Controller
    {
        // GET: Mission
        private readonly Entities _context = new Entities();
        // GET: Target
        [BreadCrumb(ControllerName = "Nhiệm vụ", ActionName = "Nhiệm vụ", AreaName = "Danh mục")]
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult Search(string MissionCodeSearch = null, string MissionNameSearch = null, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
            IQueryable<MissionModel> IQSearch =
                from x in this._context.Mission
                where x.Status != Constants.IS_NOT_ACTIVE
                select new MissionModel
                {
                    MissionCode = x.MissionCode,
                    MissionName = x.MissionName,
                    CreateTime = x.CreateTime,
                    Creator = x.Creator,
                    Status = x.Status,
                    MissionContent = x.MissionContent,
                    MissionID = x.MissionID
                };
            if (!string.IsNullOrWhiteSpace(MissionCodeSearch))
            {
                IQSearch = IQSearch.Where(x => x.MissionCode.ToLower().Contains(MissionCodeSearch.Trim().ToLower()));
            }
            if (!string.IsNullOrWhiteSpace(MissionNameSearch))
            {
                IQSearch = IQSearch.Where(x => x.MissionName.ToLower().Contains(MissionNameSearch.Trim().ToLower()));
            }


            Paginate<MissionModel> paginate = new Paginate<MissionModel>();
            paginate.PageSize = Constants.PAGE_SIZE;
            paginate.List = IQSearch.OrderBy(x => x.MissionCode).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList();
            paginate.Count = IQSearch.Count();
            ViewData["listMission"] = paginate;
            return PartialView("_ListResult");
        }
        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            MissionModel model = new MissionModel();

            if (id.HasValue && id.Value > 0)
            {
                model = this._context.Mission
                    .Where(x => x.MissionID == id && x.Status != Constants.IS_NOT_ACTIVE)
                    .Select(x => new MissionModel
                    {
                        MissionCode = x.MissionCode,
                        MissionName = x.MissionName,
                        CreateTime = x.CreateTime,
                        Creator = x.Creator,
                        Status = x.Status,
                        MissionContent = x.MissionContent,
                        MissionID = x.MissionID
                    })
                    .FirstOrDefault();
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại nhiệm vụ đã chọn");
                }
                else
                {
                    ViewBag.LstStatus = Utils.GetStatusLst();
                }
            }
            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(MissionModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            string msg;
            if (ModelState.IsValid)
            {
                if (model.MissionID > 0)
                {
                    Mission entity = this._context.Mission.FirstOrDefault(x => x.MissionID == model.MissionID && x.Status == Constants.IS_ACTIVE);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại nhiệm vụ đã chọn");
                    }
                    model.Creator = entity.Creator;
                    model.UpdateEntity(entity);
                    msg = "Cập nhật thành công";
                }
                else
                {
                    Mission entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    entity.CreateTime = DateTime.Now;
                    entity.Status = 1;
                    entity.Creator = user != null ? user.UserName : "";
                    this._context.Mission.Add(entity);
                    msg = "Thêm mới thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            Mission obj = _context.Mission.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại nhiệm vụ đã chọn");
            }

            _context.Mission.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa nhiệm vụ thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}