﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Common.CommonBO;
using IInterface.Filter;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IInterface.Controllers
{
    public partial class PeriodDepartmentController : Controller
    {
        public JsonResult SaveImplementation(PlanModel model)
        {
            Plan entity = context.Plan.Find(model.PlanID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            if (entity.DepartmentID == Constants.HEAD_DEPT_ID)
            {
                entity.Implementation = model.Implementation;
            }
            else
            {
                entity.Monitor = model.Monitor;
                entity.CostManagement = model.CostManagement;
            }
            this.context.SaveChanges();
            return Json("Cập nhật thông tin thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult PrepareImplementation(int PlanID)
        {
            UserInfo user = SessionManager.GetUserInfo();
            PlanModel model = new PlanModel();
            Plan entity = context.Plan.Find(PlanID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            model.DepartmentID = entity.DepartmentID;
            model.PlanID = entity.PlanID;
            model.Implementation = entity.Implementation;
            model.Monitor = entity.Monitor;
            model.CostManagement = entity.CostManagement;
            ObjectRoleBO role = new FlowProcessBusiness(context).GetRoleForObject(user.UserID, PlanID, Constants.COMMENT_TYPE_PLAN_TARGET);
            ViewData["role"] = role;
            return PartialView("_Implementation", model);
        }

        public PartialViewResult CostInfo(int PlanID)
        {
            Plan entity = context.Plan.Find(PlanID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại kế hoạch");
            }
            Period period = context.Period.Find(entity.PeriodID);
            ViewData["period"] = period;
            PlanBusiness bus = new PlanBusiness(context);
            ViewData["listCost"] = bus.GetPlanCostByYear(PlanID);
            return PartialView("_CostInfo");
        }
    }
}