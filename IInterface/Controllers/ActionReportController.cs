﻿using IInterface.Business;
using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public partial class ActionReportController : ReportController
    {
        [BreadCrumb(ControllerName = "Báo cáo hoạt động", ActionName = "Báo cáo hoạt động", AreaName = "Báo cáo")]
        public ActionResult IndexDept()
        {
            return BaseIndexDept();
        }

        [BreadCrumb(ControllerName = "Báo cáo hoạt động", ActionName = "Báo cáo hoạt động", AreaName = "Xem xét báo cáo")]
        public ActionResult IndexReview()
        {
            return BaseIndexReview();
        }

        [BreadCrumb(ControllerName = "Báo cáo hoạt động", ActionName = "Báo cáo hoạt động", AreaName = "Báo cáo")]
        public ActionResult AddOrEdit(int? id, int periodID = 0, int year = 0, int? quarter = 0, int isReview = 0)
        {
            ActionReportInfoModel model = null;
            Plan plan = null;
            if (id == null || id == 0)
            {
                if (periodID <= 0 || year <= 0)
                {
                    RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Dữ liệu không hợp lệ" });
                }
                if (isReview == 0)
                {
                    if (quarter == 0)
                    {
                        quarter = null;
                    }
                    UserInfo user = SessionManager.GetUserInfo();
                    plan = context.Plan.Where(x => x.DepartmentID == user.DepartmentID && x.PeriodID == periodID).FirstOrDefault();
                    if (plan == null)
                    {
                        RedirectToAction("RoleErrorBusiness", "Home", new { msg = "Kế hoạch chưa được phê duyệt" });
                    }
                    ActionReport report = context.ActionReport.Where(x => x.DepartmentID == user.DepartmentID && x.Year == year && x.Quarter == quarter
                    && x.PlanID == plan.PlanID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                    if (report == null)
                    {
                        report = new ActionReport();
                        report.CreateDate = DateTime.Now;
                        report.Creator = user.UserName;
                        report.DepartmentID = user.DepartmentID;
                        report.PlanID = plan.PlanID;
                        report.Quarter = quarter;
                        report.Status = Constants.PLAN_NEW;
                        report.Type = user.DeptType;
                        report.Year = year;
                        context.ActionReport.Add(report);
                        context.SaveChanges();
                    }
                    id = report.ActionReportID;
                }
            }
            if (id != null)
            {
                var entity = (from s in this.context.ActionReport
                              join p in this.context.Plan on s.PlanID equals p.PlanID
                              join d in this.context.Department on p.DepartmentID equals d.DepartmentID
                              join pr in this.context.Period on p.PeriodID equals pr.PeriodID
                              where s.ActionReportID == id && s.Status != Constants.IS_NOT_ACTIVE
                              select new
                              {
                                  pr.PeriodID,
                                  s.ActionReportID,
                                  s.Year,
                                  s.Quarter,
                                  s.PlanID,
                                  s.Status,
                                  pr.PeriodName,
                                  d.DepartmentName,
                                  s.Advantage,
                                  s.Difficulty,
                                  s.Propose,
                                  s.SendDate
                              }).FirstOrDefault();
                if (entity == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                model = new ActionReportInfoModel();
                model.ActionReportID = entity.ActionReportID;
                model.Year = entity.Year;
                model.Quarter = entity.Quarter;
                model.PlanID = entity.PlanID;
                model.PeriodID = entity.PeriodID;
                model.PlanName = entity.PeriodName;
                model.Status = entity.Status;
                model.DeptName = entity.DepartmentName;
                model.Advantage = entity.Advantage;
                model.Difficulty = entity.Difficulty;
                model.Propose = entity.Propose;
                model.SendDate = entity.SendDate;
                plan = this.context.Plan.Find(entity.PlanID);
                year = entity.Year;
                // File dinh kem
                List<ActionReportFileModel> listActionReportFile = this.context.ActionReportFile.Where(x => x.ActionReportID == id.Value)
                .Select(x => new ActionReportFileModel
                {
                    ActionReportFileID = x.ActionReportFileID,
                    ActionReportID = x.ActionReportID,
                    DocName = x.DocName,
                    FileName = x.FileName,
                    FileSize = x.FileSize
                }).ToList();
                ViewData["listActionReportFile"] = listActionReportFile;
            }
            double planMoney = SetAddOrEditData(id, plan, year, isReview, true);
            ViewData["planMoney"] = planMoney;
            return View("AddOrEdit", model);
        }

        public PartialViewResult LoadTarget(int PeriodID, int? ActionReportID = null)
        {
            UserInfo user = SessionManager.GetUserInfo();
            Plan plan;
            if (ActionReportID != null && ActionReportID > 0)
            {
                ActionReport report = context.ActionReport.Find(ActionReportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                plan = context.Plan.Find(report.PlanID);
            }
            else
            {
                plan = context.Plan.Where(x => x.PeriodID == PeriodID && x.DepartmentID == user.DepartmentID).FirstOrDefault();
            }
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch không tồn tại");
            }
            List<PlanTargetModel> listTarget = (from pt in this.context.PlanTarget
                                                where pt.PlanID == plan.PlanID
                                                orderby pt.TargetOrder, pt.PlanTargetTitle
                                                select new PlanTargetModel
                                                {
                                                    PlanTargetID = pt.PlanTargetID,
                                                    PlanTargetTitle = pt.PlanTargetTitle,
                                                    TargetCode = pt.TargetCode,
                                                    PlanTargetContent = pt.PlanTargetContent,
                                                    TotalBudget = pt.TotalBuget
                                                }).ToList();
            double planMoney = listTarget.Select(x => x.TotalBudget.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
            ViewData["listTarget"] = listTarget;
            ViewData["planMoney"] = planMoney;
            ViewData["plan"] = plan;
            return PartialView("_Target");
        }

        public PartialViewResult LoadCostInfo(int PeriodID, int Year, int Quarter = 0, int? ActionReportID = null)
        {
            UserInfo user = SessionManager.GetUserInfo();
            Plan plan;
            if (ActionReportID != null && ActionReportID > 0)
            {
                ActionReport report = context.ActionReport.Find(ActionReportID);
                if (report == null)
                {
                    throw new BusinessException("Dữ liệu không hợp lệ");
                }
                plan = context.Plan.Find(report.PlanID);
            }
            else
            {
                plan = context.Plan.Where(x => x.PeriodID == PeriodID && x.DepartmentID == user.DepartmentID).FirstOrDefault();
            }
            if (plan == null)
            {
                throw new BusinessException("Kế hoạch không tồn tại");
            }
            List<PlanTargetModel> listTarget = (from pt in this.context.PlanTarget
                                                where pt.PlanID == plan.PlanID
                                                orderby pt.PlanTargetID
                                                select new PlanTargetModel
                                                {
                                                    PlanTargetID = pt.PlanTargetID,
                                                    PlanTargetTitle = pt.PlanTargetTitle,
                                                    TargetCode = pt.TargetCode,
                                                    PlanTargetContent = pt.PlanTargetContent,
                                                    TotalBudget = pt.TotalBuget
                                                }).ToList();
            double planMoney = listTarget.Select(x => x.TotalBudget.GetValueOrDefault()).DefaultIfEmpty(0).Sum();
            ViewData["planMoney"] = planMoney;
            int reportType = Constants.REPORT_TYPE_YEAR;
            if (Quarter > 0)
            {
                reportType = Constants.REPORT_TYPE_QUARTER;
            }
            ViewData["reportType"] = reportType;
            // Lay thong tin bao cao tai chinh
            FinancialReport financialReport = this.context.FinancialReport.Where(x => x.PlanID == plan.PlanID
             && x.Year == Year && (Quarter == 0 || x.Quarter == Quarter)).FirstOrDefault();
            var finalBus = new FinancialReportBusiness(this.context);
            ViewData["totalMoneyInReport"] = finalBus.GetTotalSettleMoney(financialReport, FinancialReportBusiness.GET_TOTAL);
            return PartialView("_CostInfo");
        }

        public JsonResult SaveTargetResult(int reportID, int planTargetID, string targetResult)
        {
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == reportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            ValidateEditRole(entity.ActionReportID);
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == planTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            ActionReportDetail detail = this.context.ActionReportDetail.Where(x => x.ActionReportID == reportID
            && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (detail != null)
            {
                detail.TargetResult = string.IsNullOrWhiteSpace(targetResult) ? null : targetResult.Trim();
            }
            this.context.SaveChanges();
            return Json("Cập nhật kết quả hoạt động chính thành công", JsonRequestBehavior.AllowGet);
        }

        [BreadCrumb(ControllerName = "Báo cáo hoạt động", ActionName = "Báo cáo hoạt động", AreaName = "Báo cáo")]
        public ActionResult View(int id)
        {
            ActionResult res = this.AddOrEdit(id);
            ViewData["isView"] = true;
            return res;
        }

        public JsonResult SaveGeneralInfo(ActionReportData data)
        {
            ActionReport actionReport = null;
            int deptID = SessionManager.GetUserInfo().DepartmentID.Value;
            Plan plan = this.context.Plan.Where(x => x.PeriodID == data.PeriodID && x.DepartmentID == deptID && x.Status == Constants.PLAN_APPROVED).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            if (data.ActionReportID > 0)
            {
                actionReport = this.context.ActionReport.Where(x => x.ActionReportID == data.ActionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
                if (actionReport == null)
                {
                    throw new BusinessException("Không tồn tại báo cáo hoạt động, vui lòng kiểm tra lại");
                }
                ValidateEditRole(data.ActionReportID);

                actionReport.PlanID = plan.PlanID;
                actionReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    actionReport.Quarter = data.Quarter;
                }
                actionReport.Advantage = data.Advantage;
                actionReport.Difficulty = data.Difficulty;
                actionReport.Propose = data.Propose;
                string[] arrAttId = Request.Form.GetValues("ActionReportFileID");
                // Xoa du lieu tai lieu
                List<int> listExistAttID = new List<int>();
                if (arrAttId != null && arrAttId.Length > 0)
                {
                    foreach (string sAttId in arrAttId)
                    {
                        int attId = 0;
                        if (int.TryParse(sAttId, out attId))
                        {
                            listExistAttID.Add(attId);
                        }
                    }
                }
                List<ActionReportFile> listAttFile = this.context.ActionReportFile.Where(x => x.ActionReportID == actionReport.ActionReportID).ToList();
                listAttFile.RemoveAll(x => listExistAttID.Contains(x.ActionReportFileID));
                if (listAttFile.Count > 0)
                {
                    listAttFile.ForEach(x =>
                    {
                        this.context.ActionReportFile.Remove(x);
                    });
                }
                this.context.SaveChanges();
            }
            else
            {
                // Check xem bao cao da tao chua
                actionReport = this.context.ActionReport.Where(x => x.PlanID == plan.PlanID && x.Year == data.Year && x.Quarter == data.Quarter).FirstOrDefault();
                if (actionReport != null)
                {
                    throw new BusinessException("Đã tồn tại báo cáo hoạt động cho thời gian và kế hoạch vừa chọn, vui lòng kiểm tra lại");
                }
                actionReport = new ActionReport();
                actionReport.PlanID = plan.PlanID;
                actionReport.Year = data.Year;
                if (data.Quarter != null && data.Quarter > 0)
                {
                    actionReport.Quarter = data.Quarter;
                }
                actionReport.CreateDate = DateTime.Now;
                actionReport.Status = Constants.PERIOD_NEW;
                actionReport.Creator = SessionManager.GetUserInfo().UserID.ToString();
                actionReport.Advantage = data.Advantage;
                actionReport.Difficulty = data.Difficulty;
                actionReport.Propose = data.Propose;
                actionReport.DepartmentID = SessionManager.GetUserInfo().DepartmentID;
                this.context.ActionReport.Add(actionReport);
                this.context.SaveChanges();
                // Tong hop chi so trong truong hop la bao cao nam (tong hop tu cac nam, quy truoc do)
            }
            HttpFileCollectionBase files = Request.Files;
            if (files.Count > 0)
            {
                foreach (string key in files.AllKeys)
                {
                    HttpPostedFileBase file = files[key];
                    string fname;

                    // Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }
                    byte[] fileData;
                    using (Stream inputStream = file.InputStream)
                    {
                        MemoryStream memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        fileData = memoryStream.ToArray();
                    }
                    ActionReportFile docAtt = new ActionReportFile();
                    docAtt.ActionReportID = actionReport.ActionReportID;
                    docAtt.FileName = fname;
                    docAtt.DocName = key.Split('_')[1];
                    docAtt.FileContent = fileData;
                    docAtt.FileSize = fileData.LongLength;
                    this.context.ActionReportFile.Add(docAtt);
                }
            }
            this.context.SaveChanges();
            return Json(actionReport.ActionReportID, JsonRequestBehavior.AllowGet);
        }

        private void ValidateEditRole(int id)
        {
            var user = SessionManager.GetUserInfo();
            var busRole = new FlowProcessBusiness(this.context);
            var role = busRole.GetRoleForObject(user.UserID, id, Constants.COMMENT_TYPE_ACTION);
            if (role == null || role.Role < Constants.ROLE_EDIT)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
        }

        public JsonResult DeleteReport(int id)
        {
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == id && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            var user = SessionManager.GetUserInfo();
            Plan plan = this.context.Plan.Where(x => x.PlanID == entity.PlanID && x.DepartmentID == user.DepartmentID).FirstOrDefault();
            if (plan == null)
            {
                throw new BusinessException("Bạn không có quyền thực hiện thao tác này");
            }
            ValidateEditRole(id);
            this.context.Database.ExecuteSqlCommand("delete from ActionReportFile where ActionReportID = " + entity.ActionReportID);
            this.context.Database.ExecuteSqlCommand("delete from ActionReportActionIndex where ActionReportID = " + entity.ActionReportID);
            this.context.Database.ExecuteSqlCommand("delete from ActionReportIndex where ActionReportID = " + entity.ActionReportID);
            this.context.Database.ExecuteSqlCommand("delete from ActionReportDetail where ActionReportID = " + entity.ActionReportID);
            this.context.Database.ExecuteSqlCommand("delete from ReportReview where ReportID = " + entity.ActionReportID + " and ReportType = " + Constants.REPORT_TYPE_ACTION);
            this.context.ActionReport.Remove(entity);
            this.context.SaveChanges();
            return Json("Xóa báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult OpenIndexPopup(int actionReportID, int planTargetID, int page = 1)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == planTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            Paginate<ActionReportIndexModel> paginate = bus.GetReportIndex(actionReportID, planTargetID, page);
            ViewData["listTargetIndex"] = paginate;
            ViewData["targetName"] = pt.PlanTargetContent;
            return PartialView("_PopupTargetIndex");
        }

        public PartialViewResult OpenActionIndexPopup(int actionReportID, int planTargetID, int page = 1)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == planTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            Paginate<ActionReportActionIndexModel> paginate = bus.GetReportActionIndex(actionReportID, planTargetID, page);
            ViewData["listActionIndex"] = paginate;
            ViewData["targetName"] = pt.PlanTargetContent;
            return PartialView("_PopupActionIndex");
        }

        public PartialViewResult LoadReportIndex(int actionReportID, int planTargetID)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<TargetIndexModel> lstTargetIndex = bus.GetActionReportTargetIndex(actionReportID, planTargetID)
                    .Select(x => new TargetIndexModel
                    {
                        PlanTargetIndexID = x.PlanTargetIndexID,
                        TypeIndex = x.TypeIndex,
                        ValueIndex = x.ValueIndex,
                        ContentIndex = x.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_CHOICE ?
                        x.ContentIndex : x.TypeIndex == Constants.TYPE_INDEX_INPUT ? x.PlanValueIndex + " " + x.ContentIndex :
                        x.PlanValueIndex + "% " + (x.ContentIndex.StartsWith("%") ? x.ContentIndex.Substring(1) : x.ContentIndex)
                    }).ToList();
            ViewData["listTargetIndexChoosen"] = lstTargetIndex;
            return PartialView("_TargetIndexView");
        }

        public PartialViewResult LoadReportActionIndex(int actionReportID, int planTargetID)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<ActionReportActionIndexModel> lstActionIndex = bus.GetActionReportActionIndex(actionReportID, planTargetID).ToList();
            ViewData["lstActionIndex"] = lstActionIndex;
            return PartialView("_ActionIndex");
        }

        public JsonResult RemoveTargetIndex(int planTargetID, int actionReportID, long planTargetIndexID)
        {
            ValidateEditRole(actionReportID);
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.RemoveReportTargetIndex(planTargetID, actionReportID, planTargetIndexID);
            return Json("Xóa chỉ số mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveActionIndex(int planTargetID, int actionReportID, long actionReportActionIndexID)
        {
            ValidateEditRole(actionReportID);
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.RemoveReportActionIndex(planTargetID, actionReportID, actionReportActionIndexID);
            return Json("Xóa chỉ số hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChoiceIndex(List<IndexMappingBO> TargetIndexIDs, int PlanTargetID, int ActionReportID, int Page = 1)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.SaveReportTargetIndex(TargetIndexIDs, PlanTargetID, ActionReportID, Page);
            return Json("Cập nhật chỉ số mục tiêu thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChoiceActionIndex(List<IndexMappingBO> ActionIndexIDs, int PlanTargetID, int ActionReportID, int Page)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.SaveReportActionIndex(ActionIndexIDs, PlanTargetID, ActionReportID, Page);
            return Json("Cập nhật chỉ số hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelTargetResult(int PlanTargetID, int ActionReportID)
        {
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == ActionReportID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            ValidateEditRole(entity.ActionReportID);
            ActionReportDetail targetDetail = this.context.ActionReportDetail.Where(x => x.ActionReportID == ActionReportID && x.PlanTargetID == PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            if (targetDetail != null)
            {
                targetDetail.TargetResult = null;
            }
            this.context.SaveChanges();
            return Json("Xóa thông tin hoạt động chính thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadActionInfo(int actionReportID, int planTargetID, int isView = 0)
        {
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            ActionReport entity = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID
            && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo hoạt động hoặc chưa được phê duyệt, vui lòng kiểm tra lại");
            }
            PlanTarget pt = this.context.PlanTarget.Where(x => x.PlanID == entity.PlanID && x.PlanTargetID == planTargetID).FirstOrDefault();
            if (pt == null)
            {
                throw new BusinessException("Mục tiêu chưa được gắn với kế hoạch");
            }
            List<ActionReportDetailModel> listDetail = bus.GetListAction(0, planTargetID, actionReportID);
            ViewData["listDetail"] = listDetail;
            ActionReportDetail detail = this.context.ActionReportDetail.Where(x => x.ActionReportID == actionReportID
                    && x.PlanTargetID == pt.PlanTargetID && x.PlanTargetActionID == null).FirstOrDefault();
            List<TargetIndexModel> lstTargetIndex = bus.GetActionReportTargetIndex(actionReportID, planTargetID)
                    .Select(x => new TargetIndexModel
                    {
                        PlanTargetIndexID = x.PlanTargetIndexID,
                        TypeIndex = x.TypeIndex,
                        ValueIndex = x.ValueIndex,
                        ContentIndex = x.TypeIndex.GetValueOrDefault() == Constants.TYPE_INDEX_CHOICE ?
                        x.ContentIndex : x.TypeIndex == Constants.TYPE_INDEX_INPUT ? x.PlanValueIndex + " " + x.ContentIndex :
                        x.PlanValueIndex + "% " + (x.ContentIndex.StartsWith("%") ? x.ContentIndex.Substring(1) : x.ContentIndex)
                    }).ToList();
            ViewData["listTargetIndexChoosen"] = lstTargetIndex;
            ViewData["lstActionIndex"] = bus.GetActionReportActionIndex(actionReportID, planTargetID).ToList();
            // Neu la bao cao nam lay them thong tin chi so
            if (entity.Quarter == null)
            {
                if (detail != null)
                {
                    ViewData["mainResult"] = detail.TargetResult;
                }

            }
            ViewData["actionReport"] = entity;
            if (detail != null)
            {
                // comment
                List<CommentModel> listComment = (from c in this.context.Comment
                                                  join u in this.context.User on c.CommentUserID equals u.UserID
                                                  where c.ObjectID == detail.ActionReportDetailID && c.Type == Constants.COMMENT_TYPE_ACTION
                                                  select new CommentModel
                                                  {
                                                      CommentDate = c.CommentDate,
                                                      CommentFullName = u.FullName,
                                                      CommentID = c.CommentID,
                                                      CommentUserID = u.UserID,
                                                      CommentUserName = u.UserName,
                                                      Content = c.Content,
                                                      ObjectID = c.ObjectID,
                                                      Type = c.Type
                                                  }).ToList();
                ViewData["listComment"] = listComment;
                ViewData["planTargetID"] = detail.ActionReportDetailID;
            }
            if (isView == 0)
            {
                return PartialView("_ActionInfo");
            }
            else
            {
                return PartialView("_ActionInfoView");
            }
        }

        public PartialViewResult Search(int PeriodID, int Year, int Status = 0, int Quarter = 0, int DeptID = 0, int IsReview = 0, int page = 1)
        {
            if (PeriodID <= 0)
            {
                throw new BusinessException("Bạn chưa chọn giai đoạn lập kế hoạch");
            }
            UserInfo userInfo = SessionManager.GetUserInfo();
            if (IsReview == Constants.IS_ACTIVE)
            {
                ViewData["listResult"] = new ActionReportBusiness(this.context).GetListActionReport(userInfo.UserID, PeriodID, DeptID, Status, Quarter, Year, page); ;
            }
            else
            {
                ViewData["listResult"] = new ActionReportBusiness(this.context).GetListActionReport(userInfo.UserID, PeriodID, userInfo.DepartmentID.Value, Status, Quarter, Year, page); ;
            }
            ViewData["isReview"] = IsReview;
            return PartialView("_ListResult");
        }

        public JsonResult SendReport(int id)
        {
            new ReportReviewBusiness(this.context).SendReport(id, Constants.REPORT_TYPE_ACTION);
            return Json("Gửi báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LoadReportDetail(int planTargetActionID, int planTargetID, long actionReportID, int isView = 0)
        {
            // report
            ActionReport entity = context.ActionReport.Find(actionReportID);
            if (entity == null)
            {
                throw new BusinessException("Không tồn tại báo cáo đã chọn, vui lòng kiểm tra lại");
            }
            ViewData["actionReport"] = entity;
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            List<ActionReportDetailModel> listDetail = bus.GetListAction(planTargetActionID, planTargetID, actionReportID);
            ViewData["listDetail"] = listDetail;
            ViewData["planTargetActionID"] = planTargetActionID;
            if (isView == 0)
            {
                return PartialView("_ListDetail");
            }
            else
            {
                return PartialView("_ListDetailView");
            }
        }

        public PartialViewResult LoadActionByTarget(int planTargetID, long actionReportID)
        {
            ActionReport entityactionReport = this.context.ActionReport.Where(x => x.ActionReportID == actionReportID && x.Status != Constants.IS_NOT_ACTIVE).FirstOrDefault();
            if (entityactionReport == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ, vui lòng kiểm tra lại");
            }
            List<PlanTargetActionModel> listAction = this.context.PlanTargetAction.Where(x => x.PlanTargetID == planTargetID
            && x.ActionContent != null && x.ActionContent.Trim() != "").OrderBy(x => x.ActionOrder)
                .Select(x => new PlanTargetActionModel
                {
                    PlanID = x.PlanID.Value,
                    PlanTargetID = x.PlanTargetID.Value,
                    PlanTargetActionID = x.PlanTargetActionID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            listAction.Insert(0, new PlanTargetActionModel { PlanTargetActionID = 0, ActionContent = "---Tất cả---", ActionCode = "Tất cả" });
            ViewData["listAction"] = listAction;
            return PartialView("_ListAction");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ActionReportData data)
        {
            ValidateEditRole(data.ActionReportID);
            ActionReportBusiness bus = new ActionReportBusiness(this.context);
            bus.Save(data);
            return Json("Cập nhật báo cáo hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadActionReportFile(int actionReportFileID)
        {
            ActionReportFile att = this.context.ActionReportFile.Find(actionReportFileID);
            if (att == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
            return File(att.FileContent, System.Net.Mime.MediaTypeNames.Application.Octet, att.FileName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}