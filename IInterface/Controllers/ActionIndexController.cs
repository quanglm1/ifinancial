﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IInterface.Filter;
using UserInfo = IInterface.Common.UserInfo;

namespace IInterface.Controllers
{
    public class ActionIndexController : Controller
    {
        // GET: Action
        private readonly Entities _context = new Entities();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">type sub or main</param>
        /// <returns></returns>
        [BreadCrumb(ControllerName = "Hoạt động", ActionName = "Chỉ số hoạt động", AreaName = "Hoạt động")]
        public ActionResult Index(int? type)
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SearchContent"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public PartialViewResult Search(string SearchContent = null, int TargetID = 0, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            IQueryable<ActionIndexModel> IQSearch =
                from x in this._context.ActionIndex
                join y in _context.MainAction on x.ActionID equals y.MainActionID
                join z in _context.Target on y.TargetID equals z.TargetID
                where y.Status != Constants.IS_NOT_ACTIVE && z.Status != Constants.IS_NOT_ACTIVE
                && (TargetID == 0 || z.TargetID == TargetID)
                select new ActionIndexModel
                {
                    ActionIndexID = x.ActionIndexID,
                    ActionID = x.ActionID,
                    ActionName = y.ActionContent,
                    TargetName = z.TargetContent,
                    IndexName = x.IndexName,
                    UnitName = x.UnitName,
                    CreateDate = x.CreateDate
                };

            if (!string.IsNullOrWhiteSpace(SearchContent))
            {
                IQSearch = IQSearch.Where(x => x.IndexName.ToLower().Contains(SearchContent.Trim().ToLower()));
            }

            Paginate<ActionIndexModel> paginate = new Paginate<ActionIndexModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = IQSearch.OrderBy(o => o.ActionName).ThenBy(o => o.IndexName).Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE).ToList(),
                Count = IQSearch.Count()
            };
            ViewData["listResult"] = paginate;
            return PartialView("_LstResult");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="typeDept">sub or main</param>
        /// <param name="type">hành động thêm mới, sửa, thêm con</param>
        /// <returns></returns>
        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            ActionIndexModel model = null;

            List<SelectListItem> listTarget = GetListTarget().ToList();
            ViewData["listTarget"] = listTarget;
            int targetID = 0;
            if (listTarget.Any())
            {
                targetID = int.Parse(listTarget.First().Value);
            }

            if (id != null && id > 0)
            {
                model = (from x in this._context.ActionIndex
                         join a in this._context.MainAction on x.ActionID equals a.MainActionID
                         where x.ActionIndexID == id && a.Status == Constants.IS_ACTIVE && a.Type == Constants.SUB_TYPE
                         select new ActionIndexModel
                         {
                             ActionIndexID = x.ActionIndexID,
                             ActionID = x.ActionID,
                             IndexName = x.IndexName,
                             UnitName = x.UnitName,
                             TargetID = a.TargetID.Value,
                             CreateDate = x.CreateDate
                         }).FirstOrDefault();
                MainAction action = _context.MainAction.Where(x => x.MainActionID == model.ActionID && x.Status == Constants.IS_ACTIVE
                && x.Type == Constants.SUB_TYPE).FirstOrDefault();
                if (action == null)
                {
                    throw new BusinessException("Không tồn tại hoạt động đã chọn hoặc đã bị xóa");
                }
                targetID = action.TargetID.Value;
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại chỉ số hoạt động đã chọn");
                }
            }

            ViewData["listAction"] = this.GetListActionByTarget(targetID);
            return PartialView("_AddOrEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ActionIndex model)
        {
            if (ModelState.IsValid)
            {
                string msg = null;
                if (model.ActionIndexID > 0)
                {
                    ActionIndex entity =
                        this._context.ActionIndex.FirstOrDefault(x => x.ActionIndexID == model.ActionIndexID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại hoạt động đã chọn");
                    }
                    entity.ActionID = model.ActionID;
                    entity.IndexName = model.IndexName;
                    entity.UnitName = model.UnitName;
                    msg = "Cập nhật thành công";
                }
                else
                {
                    ActionIndex entity = new ActionIndex();
                    entity.ActionID = model.ActionID;
                    entity.IndexName = model.IndexName;
                    entity.UnitName = model.UnitName;
                    entity.CreateDate = DateTime.Now;
                    entity.CreateID = SessionManager.GetUserInfo().UserID;
                    this._context.ActionIndex.Add(entity);
                    msg = "Thêm mới thành công";
                }

                this._context.SaveChanges();

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }
        }

        public JsonResult Delete(int id)
        {
            ActionIndex obj = _context.ActionIndex.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại chỉ số hoạt động đã chọn");
            }

            if (_context.PlanActionIndex.Any(o => o.ActionIndexID == id))
            {
                throw new BusinessException("Chỉ số hoạt động đã được sử dụng trong kế hoạch, không thể xóa");
            }
            _context.ActionIndex.Remove(obj);
            this._context.SaveChanges();
            return Json("Xóa chỉ số hoạt động thành công", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadListAction(int targetID)
        {
            List<SelectListItem> res = GetListActionByTarget(targetID);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetListActionByTarget(int targetID)
        {
            List<MainActionModel> listAction = _context.MainAction.Where(x => x.ParentID == null && x.Type == Constants.SUB_TYPE && x.Status == Constants.IS_ACTIVE && x.TargetID == targetID)
                .Select(x => new MainActionModel
                {
                    TypeFor = x.Type,
                    MainActionID = x.MainActionID,
                    MissionID = x.MissionID,
                    ParentID = x.ParentID,
                    Status = x.Status,
                    TargetID = x.TargetID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            List<int> listActionID = listAction.Select(x => x.MainActionID).ToList();
            List<MainActionModel> listActionChild = _context.MainAction.Where(x => x.Type == Constants.SUB_TYPE && x.Status == Constants.IS_ACTIVE
            && x.TargetID == targetID && x.ParentID != null && listActionID.Contains(x.ParentID.Value))
                .Select(x => new MainActionModel
                {
                    TypeFor = x.Type,
                    MainActionID = x.MainActionID,
                    MissionID = x.MissionID,
                    ParentID = x.ParentID,
                    Status = x.Status,
                    TargetID = x.TargetID,
                    ActionCode = x.ActionCode,
                    ActionContent = x.ActionContent
                }).ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (MainActionModel action in listAction)
            {
                string actionTile = action.ActionContent;
                if (!string.IsNullOrWhiteSpace(action.ActionCode))
                {
                    actionTile = action.ActionCode + ". " + actionTile;
                }
                List<MainActionModel> listChild = listActionChild.Where(x => x.ParentID == action.MainActionID).ToList();
                if (listChild.Any())
                {
                    SelectListGroup group = new SelectListGroup { Name = actionTile };
                    foreach (MainActionModel child in listChild)
                    {
                        string childTile = child.ActionContent;
                        if (!string.IsNullOrWhiteSpace(action.ActionCode))
                        {
                            childTile = child.ActionCode + ". " + childTile;
                        }
                        SelectListItem item = new SelectListItem();
                        item.Group = group;
                        item.Value = child.MainActionID.ToString();
                        item.Text = childTile;
                        selectList.Add(item);
                    }
                }
                else
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = action.MainActionID.ToString();
                    item.Text = actionTile;
                    selectList.Add(item);
                }
            }

            selectList.Insert(0, new SelectListItem
            {
                Text = "-- Chọn --",
                Value = ""
            });
            return selectList;
        }

        private IEnumerable<SelectListItem> GetListTarget()
        {
            List<SelectListItem> res = _context.Target.Where(o => o.Status == Constants.IS_ACTIVE && o.Type == Constants.SUB_TYPE).Select(
                    o => new SelectListItem
                    {
                        Text = o.TargetTitle + " - " + o.TargetContent,
                        Value = o.TargetID.ToString()
                    }).ToList();

            return res;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}