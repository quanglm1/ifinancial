﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class ContributeController : Controller
    {
        // GET: Contribute
        private readonly Entities _context = new Entities();
        [BreadCrumb(ControllerName = "Quản lý thu", AreaName = "Kế hoạch")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Search(int? year, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();

            IQueryable<ContributeModel> iqSearch =
                from x in this._context.Contribute
                select new ContributeModel
                {
                    ContributeID = x.ContributeID,
                    Year = x.Year,
                    DBPlanMoney = x.PlanMoney,
                    DBActualMoney = x.ActualMoney,
                    PlanMoney = "" + x.PlanMoney,
                    ActualMoney = "" + x.ActualMoney
                };
            if (year != null)
            {
                iqSearch = iqSearch.Where(x => x.Year == year);
            }
            Paginate<ContributeModel> paginate = new Paginate<ContributeModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = iqSearch.OrderBy(x => x.Year).Skip((page - 1) * Constants.PAGE_SIZE)
                    .Take(Constants.PAGE_SIZE).ToList(),
                Count = iqSearch.Count()
            };
            ViewData["listContribute"] = paginate;
            return PartialView("_ListResult");
        }

        public PartialViewResult PrepareAddOrEdit(int? id)
        {
            var model = new ContributeModel();
            if (id != null && id > 0)
            {
                model = this._context.Contribute
                    .Where(x => x.ContributeID == id)
                    .Select(x => new ContributeModel
                    {
                        ContributeID = x.ContributeID,
                        Year = x.Year,
                        DBPlanMoney = x.PlanMoney,
                        DBActualMoney = x.ActualMoney
                    }).FirstOrDefault();
                model.PlanMoney = model.PlanMoneyFormat;
                model.ActualMoney = model.ActualMoneyFormat;
                if (model == null)
                {
                    throw new BusinessException("Không tồn tại khoản thu bắt buộc đã chọn");
                }
            }
            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ContributeModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            if (ModelState.IsValid)
            {
                string msg;
                if (model.ContributeID > 0)
                {
                    Contribute entity = this._context.Contribute.FirstOrDefault(x => x.ContributeID == model.ContributeID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại khoản thu bắt buộc đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật khoản thu bắt buộc thành công";
                }
                else
                {
                    Contribute entity = model.ToEntity();
                    this._context.Contribute.Add(entity);
                    this._context.SaveChanges();
                    msg = "Thêm mới khoản thu bắt buộc thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            Contribute obj = _context.Contribute.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại khoản thu bắt buộc đã chọn");
            }

            _context.Contribute.Remove(obj);

            this._context.SaveChanges();
            return Json("Xóa khoản thu bắt buộc thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}