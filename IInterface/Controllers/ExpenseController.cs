﻿using IInterface.Common;
using IInterface.Models;
using IModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IInterface.Filter;

namespace IInterface.Controllers
{
    public class ExpenseController : Controller
    {
        // GET: Expense
        private readonly Entities _context = new Entities();
        [BreadCrumb(ActionName = "Danh mục chi", ControllerName = "Danh mục chi", AreaName = "Tài chính")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Search(string price = null, string missionNameSearch = null, int page = 1)
        {
            UserInfo objUser = SessionManager.GetUserInfo();
      
            IQueryable<ExpenseModel> iqSearch =
                from x in this._context.SpendItem
                    //where x.Status != Constants.IS_NOT_ACTIVE
                select new ExpenseModel
                {
                    Description = x.Description,
                    Price = x.Price,
                    SpenContent = x.SpenContent,
                    SpendItemID = x.SpendItemID,
                    Status = x.Status,
                    ParentID = x.ParentID,
                    PathTraversal = x.PathTraversal,
                    Level = x.Level
                };

            Paginate<ExpenseModel> paginate = new Paginate<ExpenseModel>
            {
                PageSize = Constants.PAGE_SIZE,
                List = iqSearch.OrderBy(x => x.PathTraversal).Skip((page - 1) * Constants.PAGE_SIZE)
                    .Take(Constants.PAGE_SIZE).ToList(),
                Count = iqSearch.Count()
            };
            ViewData["listExpense"] = paginate;
            return PartialView("_ListResult");
        }

        public PartialViewResult PrepareAddOrEdit(int? id, int? type)
        {
            var model = new ExpenseModel();

            if (type == Constants.ACTION_ADD_NEW)
            {
                model.ActionType = type;
            }

            if (type == Constants.ACTION_ADD_CHILD)
            {
                var parent = _context.SpendItem.Find(id);
                if (parent != null)
                {
                    model.ParentID = id;
                    model.ParentName = parent.SpenContent;
                    model.ActionType = type;
                }
                else
                {
                    throw new BusinessException("Khoản chi không hợp lệ");
                }
            }

            if (type == Constants.ACTION_EDIT)
            {
                model = this._context.SpendItem
                    .Where(x => x.SpendItemID == id && x.Status != Constants.IS_NOT_ACTIVE)
                    .Select(x => new ExpenseModel
                    {
                        Description = x.Description,
                        Price = x.Price,
                        SpenContent = x.SpenContent,
                        SpendItemID = x.SpendItemID,
                        Status = x.Status,
                        ParentID = x.ParentID,
                        PathTraversal = x.PathTraversal,
                        Level = x.Level
                    })
                    .FirstOrDefault();

                if (model == null)
                {
                    throw new BusinessException("Không tồn tại khoản chi đã chọn");
                }

                model.ActionType = type;
            }



            ViewBag.LstStatus = GetStatusLst();

            return PartialView("_AddOrEdit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(ExpenseModel model)
        {
            UserInfo user = SessionManager.GetUserInfo();
            if (ModelState.IsValid)
            {
                string msg;
                if (model.ActionType == 3)
                {
                    SpendItem entity = this._context.SpendItem.FirstOrDefault(x => x.SpendItemID == model.SpendItemID);
                    if (entity == null)
                    {
                        throw new BusinessException("Không tồn tại khoản chi đã chọn");
                    }
                    model.UpdateEntity(entity);
                    msg = "Cập nhật thành công";
                }
                else
                {
                    SpendItem entity = model.ToEntity(SessionManager.GetUserInfo().UserName);
                    entity.Status = 1;

                    var parent = _context.SpendItem.Find(entity.ParentID);
                    if (model.ActionType == 2)
                    {
                        if (parent != null)
                        {
                            if (parent.Price < model.Price)
                            {
                                throw new BusinessException("Giá không được lớn hơn khoản chi cha");
                            }
                            else
                            {
                                var total = _context.SpendItem.Where(o => o.ParentID == parent.SpendItemID)
                                    .Sum(o => o.Price);
                                if (parent.Price < model.Price + total)
                                {
                                    throw new BusinessException("Tổng các khoản chi con không được lớn hơn khoản chi cha");
                                }
                            }
                        }
                        else
                        {
                            throw new BusinessException("Dữ liệu không hợp lệ");
                        }
                    }

                    this._context.SpendItem.Add(entity);
                    this._context.SaveChanges();

                    entity.PathTraversal = entity.SpendItemID.ToString();

                    if (model.ActionType == 2)
                    {
                        if (entity.ParentID > 0)
                        {
                            if (parent != null)
                            {
                                entity.PathTraversal = parent.PathTraversal + "/" + entity.SpendItemID.ToString();
                                entity.Level = parent.Level == null ? 1 : parent.Level + 1;
                            }
                        }
                    }
                    msg = "Thêm mới thành công";

                }
                this._context.SaveChanges();
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

        }
        public JsonResult Delete(int id)
        {
            SpendItem obj = _context.SpendItem.Find(id);
            if (obj == null)
            {
                throw new BusinessException("Không tồn tại khoản chi đã chọn");
            }

            _context.SpendItem.RemoveRange(_context.SpendItem.Where(o => o.ParentID == id));
            _context.SpendItem.Remove(obj);

            this._context.SaveChanges();
            return Json("Xóa khoản chi thành công", JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
        private IEnumerable<SelectListItem> GetStatusLst()
        {
            List<SelectListItem> res = new List<SelectListItem>
            {
                new SelectListItem {Text = "Hiệu lực", Value = "1"},
                new SelectListItem {Text = "Hết hiệu lực", Value = "0"}
            };

            return res;
        }
    }
}